﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using MASNET.DataAccess.Model.Custom;
using MASNET.DataAccess.Model.Settings;
using Masnet3.Common;
using Masnet3.Common.Filters;
using Masnet3.Common.Utilities;
using System;
using System.Linq;

namespace Masnet3.Model.ViewModel
{
    

    public class LayoutViewModel
    {

        private string[] breadcumbPages = new string[] { "contest/details", "contest/match", "contest/submit", "contest/newsubmit", "news/detail", "contestant/details",
                                                "myaccount/edit", "myaccount/changepassword", "myaccount/media", "myaccount/points", "myaccount/accountsummary", "myaccount/deposit",
                                                "myaccount/entries", "myaccount/friends", "myaccount/activities", "myaccount/votes", "contest/round", "myaccount/aboutme",
                                                "myaccount/messages", "report/actions", "myaccount/profile"
                                            };

        public string SiteName { get; set; }

        public int EditThemeId { get; set; }
        public string EditSiteId { get; set; }
        public string EditCampaignId { get; set; }
        public int ContestID { get; set; }
        public int ApplicationID { get; set; }

        public string Controller { get; set; }
        public string Action { get; set; }
        public string PageName { get; set; }

        public bool IsValid { get; set; }
        public bool IsLoggedIn { get; set; }
        public bool IsLogin { get; set; }
        public bool IsAdminSite { get; set; }
        public bool IsAdminPage { get; set; }
        public bool IsSuperAdmin { get; set; }
        public bool ShowLeftMenuCondition { get; set; }

        public bool MasterContainer { get; set; } = Convert.ToInt32(Commons.Config["ContainerType"]) == 1;
        public bool HasBreadcrumb {
            get
            {
                return breadcumbPages.Contains(PageName.ToLower());
            }
        }

        public bool ShowLanguges
        {
            get
            {
                return Commons.Config["Header_ShowLanguages"] == "1";
            }
        }

        public string LogoUrl { get; set; }

        public bool IsDebug { get; set; }
        public bool ShowPreviewButton { get; set; }
        public bool DisplayLeftMenu { get; set; }
        public bool IsHeaderContainer { get; set; }
        public bool IsFullWidth { get; set; }
        public bool UsingHeaderMenuLinks { get; set; }

        public string ShareID { get; set; }
        public bool UseValidate { get; set; }

        public bool TriviaContest { get; set; }
        public bool IsGreaterGoodContext { get; set; }
        public bool IsSyngentaContext { get; set; }
        public bool IsAARP2017Context { get; set; }
        public bool IsContestEnd { get; set; }
        public bool IsPageContestended { get; set; }
        public string IsShowLeaderBoard
        {
            get
            {
                return Commons.Config["Quiz_IsShowLeaderBoard"];
            }
        }

        public string SpinAllow
        {
            get
            {
                return Commons.Config["Quiz_PointsToPlayIWG"];
            }
        }

        public dynamic ExtraSetting
        {
            get
            {
                return Commons.ExtraSetting();
            }
        }

        public string IsNumberAnsweredQuestions
        {
            get
            {
                return Commons.Config["Quiz_DisplayNumberAnsweredQuestions"];
            }
        }

        public string IsNumberCorrectAnswered
        {
            get
            {
                return Commons.Config["Quiz_DisplayNumberCorrectAnswered"];
            }
        }

        public string IsShowPoints
        {
            get
            {
                return Commons.Config["Quiz_IsShowPoints"];
            }
        }

        public int SpinCount { get; set; } = 0;


        //User
        public int NumberAnswered { get; set; } = 0;
        public int CorrectAnswered { get; set; } = 0;
        public int Point { get; set; } = 0;

        //object
        public LoggedUser loggedUser { get; set; } = new LoggedUser();
        public AppConfiguration appConfiguration { get; set; } = new AppConfiguration();
        public SiteInformation SiteInformation { get; set; }
        public PageBannerDelivery PageBannerDelivery { get; set; } = new PageBannerDelivery();
        public Config CustomizeButtonConfig = new Config();


    }
}
