﻿using MASNET.DataAccess.Model.Custom;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Model.ViewModel
{
    public class RetailerLocationViewModel
    {
        public IEnumerable<RetailerLocation> retailerLocation { get; set; }
    }
}
