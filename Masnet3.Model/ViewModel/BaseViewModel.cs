﻿using Masnet3.Model.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.ViewModel
{
    public class BaseViewModel : LayoutViewModel
    {
        public string Title { get; set; }

    }
}
