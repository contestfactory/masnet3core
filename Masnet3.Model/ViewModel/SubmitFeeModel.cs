﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Model.ViewModel
{
    public class SubmitFeeModel
    {
        public int FeeID { get; set; }
        public int FeeType { get; set; }
        public int ContestID { get; set; }
        public double Amount { get; set; }
        public string PromoCode { get; set; }
        public int ContestantEntryID { get; set; }
    }
}
