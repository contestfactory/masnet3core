﻿using Masnet3.Common.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Masnet3.Model.ViewModel
{
    public class LoginViewModel : IValidatableObject
    {
        public string UsernameLogin { get; set; }
        public string PasswordLogin { get; set; }
        public bool RememberMeLogin { get; set; }
        public string ReturnUrl { get; set; }

        public string username { get; set; }
        public string password { get; set; }
        public int RegistrationType { get; set; }
        public string SSToken { get; set; }
        public string TokenSecret { get; set; }
        public string SS_ID { get; set; }
        public int loginType { get; set; }
        public string Code { get; set; }
        public string RepID { get; set; }
        public int PrizeID { get; set; }
        public int ContestID { get; set; }
        public int Application { get; set; }
        public string Message { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (string.IsNullOrWhiteSpace(UsernameLogin))
            {
                yield return new ValidationResult(DbLanguage.GetText("gValidationRequired"), new string[] { "UsernameLogin" });
            }

            if (string.IsNullOrWhiteSpace(PasswordLogin))
            {
                yield return new ValidationResult(DbLanguage.GetText("gValidationRequired"), new string[] { "PasswordLogin" });
            }
        }
    }
}
