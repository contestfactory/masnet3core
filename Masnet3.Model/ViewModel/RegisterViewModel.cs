﻿using MASNET.DataAccess;
using MASNET.DataAccess.Model.Custom;
using MASNET.DataAccess.Model.ViewModel;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Masnet3.Model.ViewModel
{
    public class RegisterViewModel : ConfigRegister
    {
        public bool DisplayLogin { get; set; }
        public string ReturnUrl { get; set; }
        public Registration Registration { get; set; } = new Registration();
        public ICollection<DisplayItem> StandardFields { get; set; }
        public List<CustomFields> CustomFields { get; set; } = new List<CustomFields>();
        public virtual ExtensionField ExtensionField { get; set; } = new ExtensionField();
        public List<BookingIntentionViewModel> BookingIntentions { get; set; } = new List<BookingIntentionViewModel>
        {
            new BookingIntentionViewModel(),
            new BookingIntentionViewModel(),
            new BookingIntentionViewModel()
        };

    }

    public class ConfigRegister : BaseViewModel
    {
        public bool HasBeginForm { get; set; }
        public bool ShowTrackingCode { get; set; }
        public bool ShowLoginForm { get; set; }
        public string RegistrationAutoConfirm { get; set; }
        
        public List<string> ElementRequires { get; set; }  = new List<string>();
        public Dictionary<string, string> ElementRules { get; set; } = new Dictionary<string, string>();
        public Dictionary<string, Dictionary<string, object>> StandardElements { get; set; } = new Dictionary<string, Dictionary<string, object>>();

        public KeystandardElements keyStandardElements { get; set; } = new KeystandardElements();

        // Sygenta
        public RetailerLocationViewModel RetailerLocation { get; set; } = new RetailerLocationViewModel();
        public IEnumerable<SelectListItem> varietyArray { get; set; }
        public IEnumerable<SelectListItem> ProvinceArray { get; set; }
        public IEnumerable<string> RetailerNameArray { get; set; }
        public IEnumerable<string> RetailerCityArray { get; set; }
        
    }

    public class KeystandardElements
    {
        public KeyValuePair<string,Dictionary<string, object>> FirstName { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> LastName { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> City { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> Provine { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> Zip { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> EmailAddress { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> Phone { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> Retailer { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
        public KeyValuePair<string, Dictionary<string, object>> RetailerLocation { get; set; } = new KeyValuePair<string, Dictionary<string, object>>();
    }
}
