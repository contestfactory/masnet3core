﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class MessageMetadata
    {

    }

    public partial class Message
    {
        public long MessageID { get; set; }
        public long MessageSessionID { get; set; }
        public string MessageText { get; set; }
        public long SenderID { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual string Username { get; set; }
        public virtual string SenderPhoto { get; set; }
    }
}
