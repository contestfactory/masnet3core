//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    [Serializable()]
    public class RegistrationProfile
    {           
        public int ProfileID { get; set; }
        public long UserID {get;set;}
        public string Profile { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
        public string Email { get; set; }
        public DateTime? LastUpdate { get; set; }
    }
}
