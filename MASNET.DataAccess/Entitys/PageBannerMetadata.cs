﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class PageBanner
    {
        public string BannerName { get; set; }
        public Nullable<int> Width { get; set; }
        public Nullable<int> Height { get; set; }
    }
}
