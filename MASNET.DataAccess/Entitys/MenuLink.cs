﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    [Serializable]
    public class MenuLink
    {
        public int MenuLinkID { get; set; }
        private string _Id = null;
        public string Id
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_Id))
                {
                    _Id = "Menu_" + Guid.NewGuid().ToString().Replace("-", "_");
                }
                return _Id.Replace(" ", "_");
            }
            set { _Id = value; }
        }
        public string Url { get; set; }
        public string Name { get; set; }
        private string _PageNames = string.Empty;
        public string PageNames
        {
            get
            {
                if (string.IsNullOrWhiteSpace(_PageNames))
                {
                    _PageNames = string.Empty;
                }

                _PageNames = _PageNames.Trim().Replace(" ", "").ToLower();
                if (_PageNames.StartsWith("/") == false)
                {
                    _PageNames = "/" + _PageNames;
                }
                return _PageNames;
            }
            set
            {
                _PageNames = value;
            }
        }
        public bool IsDisplay { get; set; }
        public bool IsDisplayForAgent { get; set; }
        public string Icon { get; set; }
        public int Priority { get; set; }
        public string OnClick { get; set; }
        public string HelpTextId { get; set; }
        public string HelpText { get; set; }
        public bool IsBuilderSteps { get; set; }
        public bool IsSelected { get; set; }
        public List<MenuLink> MenuLinks { get; set; }
        public string Style { get; set; }
        public int MenuItemID { get; set; }
        public int ParentID { get; set; }
        public int Level { get; set; }
        public string ChildIDs { get; set; }
        public int MenuType { get; set; }
        public int PrevID { get; set; }
        public int NextID { get; set; }
        public bool IsLeaf { get; set; }
        public int PageID { get; set; }
        public int MenuBarID { get; set; }
        public virtual MenuLink Parent { get; set; }
        public string Pers { get; set; }              //1,2    
        public string CustomFunction { get; set; }    //Commons.SessionsUtilities.CustomValidate(1)  
        public string Display_Config { get; set; }

        public MenuLink()
        {
            IsDisplay = true;
            Priority = 1;
            IsBuilderSteps = false;
            IsSelected = false;
            Style = string.Empty;
            Level = 0;
            PageID = -1;
            MenuPositionType = 0;
        }

        public virtual string FullName
        {
            get
            {
                string value = this.Name;

                MenuLink parent = this.Parent;
                while (parent != null && parent.MenuLinkID > 0)
                {
                    value = parent.Name + @"/" + value;
                    parent = parent.Parent;
                }
                return value;
            }
        }

        public bool ShowHide { get; set; }
        public int? MenuLinkKey { get; set; }
        public int UseType { get; set; }
        public int? ApplicationID { get; set; }
        public int? ContestID { get; set; }
        public string ResourceKey { get; set; }
        public bool IsDisplayEnd { get; set; }
        public bool IsDefaultEnd { get; set; }
        public int MenuPositionType { get; set; }
        public bool IsHideToAgent { get; set; }

        public int LeafCount {
            get
            {
                if (this.MenuLinks == null || this.MenuLinks.Count == 0) return this.IsDisplay ? 1 : 0;
                else return this.MenuLinks.Sum(x => x.LeafCount);
            }
        }
    }

    public class FooterMenuLink
    {
        public string ItemName { get; set; }
        public bool ShowHide { get; set; }
        public bool IsDisplay { get; set; }
        public string ItemUrl { get; set; }
        public int LanguageID { get; set; }
        public int MenuBarID { get; set; }
        public string Target { get; set; }
        public int Priority { get; set; }
        public string Position { get; set; }
    }
    public class FooterMenu
    {
        public FooterMenu()
        {
            items = new List<FooterMenuLink>();
        }
        public int UseType { get; set; }
        public int ApplicationID { get; set; }
        public int ContestID { get; set; }
        public int TemplateID { get; set; }
        public string CustomFooter { get; set; }
        public List<FooterMenuLink> items { get; set; }

        public string ViewName { get; set; }
        public string Position { get; set; }
        public string Heading { get; set; }
        public string SiteName { get; set; }
    }
}
