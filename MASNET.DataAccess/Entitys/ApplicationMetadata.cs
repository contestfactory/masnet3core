﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ApplicationMetadata
    {
    }

    public partial class Application
    {
        public string Theme { get; set; }
        public string CreatedByUser { get; set; }

        private int _DefaultLanguageID = 0;
        public int DefaultLanguageID
        {
            get 
            {
                if (_DefaultLanguageID <= 0 && ApplicationLanguages != null && ApplicationLanguages.Count > 0)
                {
                    ApplicationLanguage applicationLanguage = ApplicationLanguages.Where(a => a.Default == true).FirstOrDefault();
                    if (applicationLanguage != null)
                    {
                        _DefaultLanguageID = applicationLanguage.LanguageID;
                    }
                    else
                    {
                        _DefaultLanguageID = ApplicationLanguages.FirstOrDefault().LanguageID;
                    }
                }
                return _DefaultLanguageID;
            }
            set
            {
                _DefaultLanguageID = value;
            }
        }

        private int _SecondLanguageID = 0;
        public int SecondLanguageID
        {
            get
            {
                if (_SecondLanguageID <= 0 && ApplicationLanguages != null && ApplicationLanguages.Count > 0)
                {
                    ApplicationLanguage applicationLanguage = ApplicationLanguages.Where(a => a.Default == false).FirstOrDefault();
                    if (applicationLanguage != null)
                    {
                        _SecondLanguageID = applicationLanguage.LanguageID;
                    }
                    else
                    {
                        _SecondLanguageID = ApplicationLanguages.FirstOrDefault().LanguageID;
                    }
                }
                return _SecondLanguageID;
            }
            set
            {
                _SecondLanguageID = value;
            }
        }
    }
}
