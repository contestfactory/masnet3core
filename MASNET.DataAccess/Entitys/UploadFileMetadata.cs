﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{    
    public partial class UploadFile
    {

        public int VoteStatus { get; set; }
        public long VoteID { get; set; }
    }

    public partial class uploadfilemetadata
    {       
        public string path { get; set; }
    }
}
