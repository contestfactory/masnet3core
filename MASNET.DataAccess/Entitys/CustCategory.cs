﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class CustCategory
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public bool Display { get; set; }
        public int ApplicationID { get; set; }
        public int CampaignID { get; set; }
        public long CreatedBy { get; set; }
        public int UseType { get; set; }

        public int? QuestionCount { get; set; }
        public int? QuestionLevel { get; set; }
        public string QuestionLevelText { get; set; }
        public int? RowIndex { get; set; }
    }


}
