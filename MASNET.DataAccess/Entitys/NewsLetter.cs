﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class NewsLetter
    {
        public int NewsLetterID { get; set; }
        public int ContestID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string IpAddress { get; set; }
        public string Opt_IN { get; set; }
        public string VerifyCode { get; set; }
        public byte? Status { get; set; }
        public string ApprovedBy { get; set; }
        public int RowNum { get; set; }
    }
}
