﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class APICredentials
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Signature { get; set; }
        public bool IsSandbox { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
    }
}
