﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ViralDNAVote
    {
        //RowNum,VotedDate,NumOfVote
        public long RowNum { get; set; }
        public DateTime VotedDate { get; set; }
        public int NumOfVote { get; set; }
        public int NumOfFanVotes { get; set; }
        public int NumOfJudgeVotes { get; set; }
        public int NumOfRegisteredVotes { get; set; }
        public int NumOfGuestVotes { get; set; }
    }
}
