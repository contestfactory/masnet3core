﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class VoteCriteria {

        public int SelectedValue { get; set; }
        public string UserComment { get; set; }
    }    
}
