﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class Money
    {
        public int ID { get; set; }
        public decimal Value { get; set; }
        public bool Display { get; set; }
        public int Priority { get; set; }
    }
}
