﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class FAQ_Info
    {
        public int FAQID { get; set; }
        public string ItemTitle { get; set; }
        public string Item { get; set; }
        public Nullable<System.DateTime> ItemDate { get; set; }
        public int DisplayOrder { get; set; }
        public int ParentID { get; set; }
        public int Depth { get; set; }
    }
}
