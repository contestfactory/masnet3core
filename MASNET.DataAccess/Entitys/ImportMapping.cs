﻿using System.Collections.Generic;

namespace MASNET.DataAccess
{
    public class ImportMapping
    {
        public string FieldName { get; set; }
        public string MappingField { get; set; }
        public byte Type { get; set; }        
    }

    public class ImportResult
    {
        public int QueueID { get; set; }
        public int UserImportedCount { get; set; }
        public bool hasMultipleSubmission { get; set; }
        public bool hasDuplicatedSubmission { get; set; }
        public int SubmissionImportedCount { get; set; }    
        public Dictionary<long, int> UserSubmissionCount { get; set; }
        public int DuplicateSubmissionCount { get; set; } 
               
        public List<ImportResultItem> Items { get; set; }

        public ImportResult()
        {
            Items = new List<ImportResultItem>();
            UserSubmissionCount = new Dictionary<long, int>();
        }
    }

    public class ImportResultItem
    {
        public int Row { get; set; }
        public List<string> Messages { get; set; }  
        public dynamic Data { get; set; }
        public bool IsDuplicated { get; set; }
        public bool IsMultiple { get; set; }
        public long EntryID { get; set; }
        public long MediaID { get; set; }
        public long UserID { get; set; }
        public string Essay { get; set; }
        public string HashCode { get; set; }
        public ImportResultItem()
        {
            Messages = new List<string>();
        }
    }
}
