﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ReportTeam
    {
        public long RowNum { get; set; }
        public DateTime VotedDate { get; set; }
        public int CompletedTeams { get; set; }
    }
}
