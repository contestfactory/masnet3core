﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class CustomClass
    {
        public int ClassID { get; set; }
        public string ClassName { get; set; }
        public string DisplayName { get; set; }
    }
}
