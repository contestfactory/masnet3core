﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class AARPLoginCallBack
    {
        public string ut { get; set; }
        public string code { get; set; }
        public string state { get; set; }
    }
}
