﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    public partial class News
    {
        public int NewsID { get; set; }
        public string Title { get; set; }
        public string TitleSecond { get; set; }
        public string ShortDesc { get; set; }
        public string ShortDescSecond { get; set; }
        public string DetailDesc { get; set; }
        public string DetailDescSecond { get; set; }
        public string Photo { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<int> CategoryID { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<bool> IsDisplay { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> ContestID { get; set; }
        public int IsInTheBiz { get; set; }

        public virtual Registration Registration { get; set; }
        public virtual Category Category { get; set; }
        public virtual List<Comment> Comments { get; set; }
    }
}
