﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{

    public partial class ContestantEntry
    {
        public Nullable<bool> IsDisplay { get; set; }
        public Dictionary<string, List<UploadFile>> Media = null;
        public Nullable<int> FeeID { get; set; }
        public string PromoCode { get; set; }
        public Nullable<DateTime> RejectedDate { get; set; }
        public string Username { get; set; }
        public string PhotoUser { get; set; }
        public string FirstNameUser { get; set; }
        public string LastNameUser { get; set; }
        public long PrevID { get; set; }
        public long NextID { get; set; }
        public List<UploadFile> Photos
        {
            get
            {
                InitMedia();
                return Media["Photos"];
            }
            set
            {
                InitMedia();
                if (value != null)
                {
                    Media["Photos"] = value;
                }
                else
                {
                    Media["Photos"] = new List<UploadFile>();
                }
            }
        }
        public List<UploadFile> Audios
        {
            get
            {
                InitMedia();
                return Media["Audios"];
            }
            set
            {
                InitMedia();
                if (value != null)
                {
                    Media["Audios"] = value;
                }
                else
                {
                    Media["Audios"] = new List<UploadFile>();
                }
            }
        }
        public List<UploadFile> Videos
        {
            get
            {
                InitMedia();
                return Media["Videos"];
            }
            set
            {
                InitMedia();
                if (value != null)
                {
                    Media["Videos"] = value;
                }
                else
                {
                    Media["Videos"] = new List<UploadFile>();
                }
            }
        }
        public List<UploadFile> Documents
        {
            get
            {
                InitMedia();
                return Media["Documents"];
            }
            set
            {
                InitMedia();
                if (value != null)
                {
                    Media["Documents"] = value;
                }
                else
                {
                    Media["Documents"] = new List<UploadFile>();
                }
            }
        }
        public UploadFile Upload { get; set; }

        public string SweepstakesPrize { get; set; }
        public DateTime? SweepstakesWinnerDate { get; set; }
        public int? SweepstakesPriority { get; set; }

        private void InitMedia()
        {
            if (Media == null)
            {
                Media = new Dictionary<string, List<UploadFile>>();

                Media.Add("Photos", new List<UploadFile>());
                Media.Add("Audios", new List<UploadFile>());
                Media.Add("Videos", new List<UploadFile>());
                Media.Add("Documents", new List<UploadFile>());

                if (ContestantFiles != null)
                {
                    foreach (var ContestantFile in ContestantFiles)
                    {
                        if (ContestantFile.UploadFile != null)
                        {
                            switch (ContestantFile.UploadFile.MediaTypeID)
                            {
                                case 1: //Photo
                                    Media["Photos"].Add(ContestantFile.UploadFile);
                                    break;
                                case 2: //Audio
                                    Media["Audios"].Add(ContestantFile.UploadFile);
                                    break;
                                case 3: //Video
                                    Media["Videos"].Add(ContestantFile.UploadFile);
                                    break;
                                case 4: //Document
                                    Media["Documents"].Add(ContestantFile.UploadFile);
                                    break;
                            }
                        }
                    }
                }
            }
        }

        public bool IsVideo { get { return CategoryID == 3; } }
        public bool IsEssay { get { return CategoryID == 4; } }
    }

}
