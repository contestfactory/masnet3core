﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    public partial class Traffic
    {
        public Traffic()
        {
            PageViews = 0;
            TrafficDetails = new List<TrafficDetails>();
        }

        public int TrafficID { get; set; }
        public int ContestID { get; set; }
        public int ApplicationID { get; set; }
        public string PageName { get; set; }
        public int PageViews { get; set; }

        public virtual List<TrafficDetails> TrafficDetails { get; set; }
    }
}
