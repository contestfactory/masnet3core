﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class FAQ
    {
        public int ID { get; set; }
        public int FAQID { get; set; }
        public string QuestionNo { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public bool IsDisplay { get; set; }
    }
}
