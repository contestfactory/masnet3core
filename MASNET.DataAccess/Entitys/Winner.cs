﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class Winner : Registration
    {
        public int ID { get; set; }
        public Nullable<long> ContestantID { get; set; }
        public long ContestantDetailID { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ViewCnt { get; set; }
        public int WinnerType { get; set; }

        public int SweepstakesPriority { get; set; }
        public int PrizeID { get; set; }
        public int WinnerStatus { get; set; }

        #region foreign
        // User 
        public new long UserID { get; set; }
        public new string SweepstakesPrize { get; set; }

        // Prize Properties
        public string PrizeCode { get; set; }
        public string PrizeName { get; set; }
        public string PrizeDescription { get; set; }
        public string PrizePhoto { get; set; }

        // Contestant Entry Properties 
        public string ContestantName { get; set; }
        public string ContestantThumbnail { get; set; }
        public string ContestantDetailDesc { get; set; }
        #endregion


    }
}
