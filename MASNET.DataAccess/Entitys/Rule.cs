﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    [Serializable()]
    public partial class Rule
    {
        public string RuleKey { get; set; }
        public string ResourceKey { get; set; }
        public string Value { get; set; }        
        public bool IsBuiltIn { get; set; }
        public int PageID { get; set; }
        public string ResourceMessage { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Message { get; set; }
        public bool IsRequireValue { get; set; }
    }
}
