﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess.SocialApi
{
    public class Taggbox
    {
        public WallDetail WallDetail { get; set; } = new WallDetail();
        public List<Feeds> Feeds { get; set; } = new List<SocialApi.Feeds>();
     }

    public class WallDetail
    {
        public string WallName { get; set; }
        public string WallUrl { get; set; }
        public string WallProfanity { get; set; }
        public string WallCreatedAt { get; set; }
        public int count { get; set; }
    }

    public class Feeds
    {
        public string FeedName { get; set; }
        public string NetworkName { get; set; }
        public string FeedId { get; set; }
        public string PostId { get; set; }
        public string PostContent { get; set; }
        public string PostFile { get; set; }
        public string PostContentPicture { get; set; }
        public string PostLinkUrl { get; set; }
        public string PostPin { get; set; }
        public string PostHighlight { get; set; }
        public string PostAuthorName { get; set; }
        public string PostUserName { get; set; }
        public string PostUserPicture { get; set; }
        public string PostPicture { get; set; }
        public string PostTags { get; set; }
        public string PostCreatedAt { get; set; }
        public string PostMediaFile { get; set; }
        public string PostUpdatedAt { get; set; }
        public string Location { get; set; }
        public string CTAcolor { get; set; }
        public string CTAbackground { get; set; }
        public string CTAurl { get; set; }
        public string CTAtext { get; set; }
    }
}
