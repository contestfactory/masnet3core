﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
   
    public partial class Registration
    {
        public Nullable<bool> IsPublicEmail { get; set; }
        public Nullable<bool> IsPublicFirstName { get; set; }
        public Nullable<bool> IsPublicLastName { get; set; }
        public Nullable<bool> IsPublicCity { get; set; }
        public Nullable<bool> IsPublicStateID { get; set; }
        public Nullable<bool> IsPublicCountryID { get; set; }
        public Nullable<bool> IsPublicProvine { get; set; }
        public Nullable<bool> IsPublicZip { get; set; }
        public Nullable<bool> IsPublicGender { get; set; }
        public Nullable<bool> IsPublicBirthday { get; set; }
        public string ApplicationName { get; set; }
        public string RoleName { get; set; }
        public Nullable<byte> UserType { get; set; }
        public Nullable<int> ContestID { get; set; }
        public string CompanyName { get; set; }
        public virtual ICollection<Application> Applications { get; set; }
        public long Entries { get; set; }
        public long Lower { get; set; }
        public long Upper { get; set; }
        public int ModerateNo { get; set; }

        public string UserFolder
        {
            get
            {
                string shortenUsername = Username;
                if (!string.IsNullOrEmpty(shortenUsername))
                {
                    if (shortenUsername.Length > 50)
                        shortenUsername = string.Format("{0}{1}", shortenUsername.Substring(0, 50), UserID);
                }

                return shortenUsername;
            }
        }

        //public List<SelectListItem> FoldersList {
        //    get {
        //        return new List<SelectListItem>();
        //        //using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["dmasConnection"].ConnectionString))
        //        //{
        //        //    conn.Open();

        //        //    return conn.Query<Folder>("SELECT * FROM Folder WHERE CreatedBy=@UserID AND FolderName='Default'", new { UserID = this.UserID })
        //        //                                .ToList()
        //        //                                .Select(t => new SelectListItem()
        //        //                                {
        //        //                                    Text = t.FolderName,
        //        //                                    Value = t.FolderID.ToString()

        //        //                                })
        //        //                                .ToList();
        //        //}
        //    }
        //}

        public List<Folder> Folders
        {
            get
            {
                return new List<Folder>();
                //using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["dmasConnection"].ConnectionString))
                //{
                //    conn.Open();

                //    return conn.Query<Folder>("SELECT * FROM Folder WHERE CreatedBy=@UserID", new { UserID = this.UserID }).ToList();                                               
                //}
            }
        }

        public double Balance 
        { 
            get 
            {
                return 0;
                // move to registrationBL.UpdateBalance(UserID);
                //using (var conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["dmasConnection"].ConnectionString))
                //{
                //    conn.Open();

                //    string sql = String.Format(@"SELECT (
                //                     (SELECT ISNULL(SUM(Amount),0) FROM accountdetail WHERE UserID=@UserID AND Transtype=1 AND (Status > 0)) 
                //                     - 
                //                     (SELECT ISNULL(SUM(Amount),0) FROM accountdetail WHERE UserID=@UserID AND Transtype=2 AND (Status > 0))
                //                     ) As TotalAmounts");


                //    var TotalAmounts = conn.Query<dynamic>(sql, new { UserID = this.UserID }).SingleOrDefault();

                //    double result = (double)TotalAmounts.TotalAmounts;

                //    return result;
                //}
            }
        }

        public double BalanceToDate { get; set; }
    }
}
