﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
#if DEBUG
    [Serializable()]
#endif
    public partial class Item
    {
        public int ItemID { get; set; }
        public Nullable<int> ItemType { get; set; }
        public string DisplayName { get; set; }
        public string HelpText { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<bool> IsDisplay { get; set; }
        public string Icon { get; set; }
        public string Description { get; set; }
        public int KeyID { get; set; }
        public Nullable<int> ParentKeyID { get; set; }
        public int ContestID { get; set; }
        public Nullable<int> ItemCount { get; set; }
        public int RowNum { get; set; }
        public long UserID { get; set; }
        public int Type { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
