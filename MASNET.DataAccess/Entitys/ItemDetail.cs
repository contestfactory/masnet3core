﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
#if DEBUG
    [Serializable()]
#endif
    public partial class ItemDetail
    {
        public int ItemDetailID { get; set; }
        public Nullable<int> ItemID { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> TemplateID { get; set; }
        public Nullable<int> UseType { get; set; }
        public Nullable<int> ShowHide { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public virtual Item Item { get; set; }
    }
}
