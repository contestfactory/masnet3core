﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class MessageSessionMetadata
    {
    }

    public partial class MessageSession
    {
        public long MessageSessionID { get; set; }
        public long LatestMessageID { get; set; }
        public string LatestMessage { get; set; }
        public DateTime LatestDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual ICollection<MessageMember> MessageMembers { get; set; }
        public virtual ICollection<Message> Messages { get; set; }
        public virtual int MessageStatus { get; set; }

        public virtual string MessageSessionName { get; set; }
        public virtual string MessageSessionPhoto { get; set; }

        public MessageSession()
        {
            this.MessageMembers = new HashSet<MessageMember>();
            this.Messages = new HashSet<Message>();
        }
    }
}
