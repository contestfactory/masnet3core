//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

#if DEBUG
    [Serializable()]
#endif
    public partial class ContestType
    {
        public ContestType()
        {
            this.Contests = new HashSet<Contest>();
        }
    
        public byte ContestTypeID { get; set; }
        public string ContestTypeName { get; set; }
        public string Display { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<bool> IsDeleted { get; set; }
        public bool CheckRegisteredDateBeforeVoting { get; set; }
    
        public virtual ICollection<Contest> Contests { get; set; }
    }
}
