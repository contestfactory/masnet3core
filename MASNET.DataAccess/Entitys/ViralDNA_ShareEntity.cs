﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ViralDNA_ShareEntity
    {
        public int RowNum { get; set; }
        public DateTime Date { get; set; }
        public int Facebook { get; set; }
        public bool Facebook_SH { get; set; }
        public int GooglePlus { get; set; }
        public bool GooglePlus_SH { get; set; }
        public int Twitter { get; set; }
        public bool Twitter_SH { get; set; }
        public int Email { get; set; }
        public bool Email_SH { get; set; }

        public string Title
        {
            get
            {
                return string.Format("{0:MM/dd/yyyy}", this.Date);
            }
        }
        public long Total 
        {
            get
            {
                long total = 0;
                total += Facebook_SH ? Facebook : 0;
                total += GooglePlus_SH ? GooglePlus : 0;
                total += Twitter_SH ? Twitter : 0;
                total += Email_SH ? Email : 0;
                return total;
            }
        }
    }
}