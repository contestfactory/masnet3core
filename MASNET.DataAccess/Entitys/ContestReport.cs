﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ContestAnalytics {
        public ContestAnalytics()
        {
            TimeLine = new ContestReport();
            Gender = new ContestReport();
            State = new ContestReport();
            Country = new ContestReport();
            AgeGroup = new ContestReport();
        }
        public ContestReport TimeLine { get; set; }
        public ContestReport Gender { get; set; }
        public ContestReport State { get; set; }
        public ContestReport Country { get; set; }
        public ContestReport AgeGroup { get; set; }
        
    }

    public class ContestReport {
        public ContestReport() {
            ReportData = new List<ChartData>();
            ReportDataHeader = new List<string>();
            Display = true;
        }
        public string ReportTitle { get; set; }
        public string XTitle { get; set; }
        public string YTitle { get; set; }
        public double MaxValue { get; set; }
        public List<string> ReportDataHeader { get; set; }
        public List<ChartData> ReportData { get;set; }
        public List<ChartDataDynamic> ReportDataDynamic { get; set; }
        public bool Display { get; set; }
    }
    public class ChartDataDynamic
    {
        public string Title { get; set; }
        public string Value { get; set; }
        public int Priority { get; set; }

        public ChartDataDynamic()
        {
            Priority = 1;
        }
    }

    public class ChartData {
        public string Title { get; set; }
        public double Value { get; set; }
        public int Priority { get; set; }

        public ChartData()
        {
            Priority = 1;
        }
    }
}
