﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class SearchItem
    {
        public string SearchKey { get; set; }
        public string SearchValue { get; set; }
        public string SearchType { get; set; }
        public int SeachBoolDefault { get; set; } 
    }
}
