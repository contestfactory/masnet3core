﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ContestPartition
    {
        public int ContestID {get;set;}
        public string DetailDesc {get;set;}
        public string PrizeText {get;set;}
        public string DetailDescForContestant {get;set;}
        public string DetailDescForVoter { get; set; }
    }
}
