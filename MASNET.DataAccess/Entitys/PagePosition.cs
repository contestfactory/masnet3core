﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class PagePosition
    {
        public int ID { get; set; }
        public Nullable<int> PageID { get; set; }
        public Nullable<int> PositionID { get; set; }
        public Nullable<bool> UseGlobal { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public bool IsDisplay { get; set; }
        public int ShowHide { get; set; }
        public string PageName { get; set; }
    }
}
