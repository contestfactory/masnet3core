﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class Abuse
    {
        public Abuse(){}
        public int RowNum { get; set; }
        public long AbuseID { get; set; }
        public int? ApplicationID { get; set; }
        public int? ContestID { get; set; }
        public int? UseType { get; set; }
        public long? ContestantEntryID { get; set; }
        public long? ContestantID { get; set; }
        public DateTime AbuseDate { get; set; }
        public string Email { get; set; }
        public long? UserID { get; set; }
        public int Status { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public long? ApprovedBy { get; set; }
        public DateTime? ActionDate { get; set; }
        public string Thumbnail { get; set; }
        public string Username { get; set; }
        public string ContetantName { get; set; }
        public bool IsHideContestantEntry { get; set; }

        public string Language { get; set; }
        public string SubmissionID { get; set; }
        public string Country { get; set; }
        public string Destination { get; set; }
        public string ExperienceDate { get; set; }
        public string Essay { get; set; }
        public int CharacterCount { get; set; }
        public string AccountCreatedDate { get; set; }
        public string CreatedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
