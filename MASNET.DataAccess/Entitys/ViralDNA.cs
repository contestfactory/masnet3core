﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MASNET.DataAccess;

namespace MASNET.DataAccess
{
    public class ViralDNA
    {
        public int ID { get; set; }
        public string Date { get; set; }
        public int RegistrationCount { get; set; }
        public int EntryCount { get; set; }
        public int PendingEntryCount { get; set; }
        public int RejectedEntryCount { get; set; }
        public int ApprovedEntryCount { get; set; }
        public int ContestID { get; set; }
    }

    public class ViralDNASearchFilter
    {
        const string _dateFormat = "MM/dd/yyyy";

        public int applicationID {get;set;}
        public int contestID { get; set; }
        public int activeRoundID { get; set; }
        public bool isBracket { get; set; }
        public List<MASNET.DataAccess.RegistrationType> sourceTypes { get; set; }

        //public List<SelectListItem> Countries { get; set; }
        //public List<SelectListItem> States { get; set; }
        public bool forceDisplayState { get; set; }
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public string searchFunctionName { get; set; }
        public string downloadFunctionName { get; set; }
        public string importFunctionName { get; set; }        
        //public List<SelectListItem> searchKeys { get; set; }
        public string keyword { get; set; }
        public bool showDisplayState { get; set; }
        public bool showStatus { get; set; }
        //public List<SelectListItem> submissionCategories { get; set; }

        public bool showMediaType { get; set; }

        public bool showSourceType { get; set; }
        public bool showCountry { get; set; }
        public bool showState { get; set; }
        public bool showCategory { get; set; }
        public bool showSearchKey { get; set; }
        public bool showImportButton { get; set; }
        public string getFromDate()
        {
            return this.fromDate == null ? "" : this.fromDate.GetValueOrDefault().ToString(_dateFormat);
        }
        public string getToDate()
        {
            return this.toDate == null ? "" : this.toDate.GetValueOrDefault().ToString(_dateFormat);
        }
        
        public string reportType { get; set; }
        public int PageReportType { get; set; }
    }
}
