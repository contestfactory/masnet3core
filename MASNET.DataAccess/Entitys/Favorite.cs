﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class Favorite
    {
        public int Id { get; set; }
        public long UserID { get; set; }
        public long ContestantID { get; set; }
        public int ContestID { get; set; }
        public int ShelterID { get; set; }
        public int ApplicationID { get; set; }
        public int FavoriteType { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CompanyName { get; set; }
        public string Thumbnail { get; set; }
    }
}
