﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class UseTypeInfo
    {
        public UseTypeInfo()
        {
            editSiteId = -1;
            editCampaignId = -1;
        }
        public int ID { get; set; }
        public int UseType { get; set; }
        public int editSiteId { get; set; }
        public int editCampaignId { get; set; }
        public int ContestID { get; set; }
        public int editTemplateId { get; set; }
        public int AppTempleteID { get; set; }
        public int ThemeID { get; set; }
        public int ApplicationID { get; set; }
        public bool IsCustomOAuth { get; set; }
        public long UserID { get; set; }

    }

    [Serializable()]
    public class Result
    {
        public int Status { get; set; }
        public string AccessToken { get; set; }
        public string Message { get; set; }
    }
    public class RegUserType{
        public string Name { get; set; }
        public int UserType { get; set; }
        public int RoleID { get; set; }
    }
}
