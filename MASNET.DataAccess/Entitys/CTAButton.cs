﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class CTAButton
    {
        public int CTAButtonID { get; set; }
        public string Title { get; set; }
        public string ButtonLabel { get; set; }
        public string Image { get; set; }
        public string Link { get; set; }
        public string PromoBox { get; set; }
        public Nullable<int> PageID { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public int Position { get; set; }
        public Nullable<int> UseType { get; set; }
        public Nullable<int> TemplateID { get; set; }
        public CTAButtonDisplayItem CTAButtonDisplayItem { get; set; }
        public Nullable<int> SH_Title { get; set; }
        public Nullable<int> SH_ButtonLabel { get; set; }
        public Nullable<int> SH_Image { get; set; }
        public Nullable<int> SH_Link { get; set; }
        public Nullable<int> SH_Copy { get; set; }
        public Nullable<int> SH_PromoBox { get; set; }
        public int CTAButtonKeyID { get; set; }
        public bool Display { get; set; }
        public string OldImage { get; set; }
        public int ShowHide { get; set; }
    }
}
