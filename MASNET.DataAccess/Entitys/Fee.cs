﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    [Serializable()]
    public partial class Fee
    {
        public int FeeId { get; set; }
        public int FeeType { get; set; }
        public string FeeName { get; set; }
        public Nullable<decimal> Price { get; set; }
        public Nullable<int> LimitedWeek { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<bool> AdvancedDesign { get; set; }
        public Nullable<bool> LikeGate { get; set; }
        public Nullable<bool> MediaExport { get; set; }
        public Nullable<bool> EmailSupport { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string ShortDesc { get; set; }
        public string DetailDesc { get; set; }
        public Nullable<int> LimitedContest { get; set; }
        public bool Display { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<decimal> MinPrice { get; set; }
        public Nullable<decimal> MaxPrice { get; set; }
        public Nullable<byte> Type { get; set; }
        
        public int ReviewNumber { get; set; }
        public double Amount { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public double DiscountPercent { get; set; }
        public long MediaID { get; set; }
        public int UseType { get; set; }
        public int FeeKeyID { get; set; }
        public Nullable<int> ExpireDay { get; set; }
        public Nullable<int> Hour { get; set; }
        public Nullable<int> Minute { get; set; }
        public bool InUsed { get; set; }
    }
}
