﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class CTAButtonDisplayItem
    {
        public int DisplayItemID { get; set; }
        public Nullable<int> CTAButtonID { get; set; }
        public Nullable<int> SH_Title { get; set; }
        public Nullable<int> SH_ButtonLabel { get; set; }
        public Nullable<int> SH_Image { get; set; }
        public Nullable<int> SH_Link { get; set; }
        public Nullable<int> SH_Copy { get; set; }
        public Nullable<int> SH_PromoBox { get; set; }
        public Nullable<int> PageID { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<int> Position { get; set; }
        public Nullable<int> UseType { get; set; }
        public Nullable<int> TemplateID { get; set; }
    }
}
