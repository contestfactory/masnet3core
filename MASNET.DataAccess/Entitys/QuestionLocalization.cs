﻿using System;
using System.Collections.Generic;

namespace MASNET.DataAccess
{
    [Serializable]
    public class QuestionLocalization
    {
        public int ID { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
        public string LanguageQuestion { get; set; }
        public string LanguageDescription { get; set; }
        public string LanguageImage { get; set; }
        public int QuestionID { get; set; }

        public virtual Question Question { get; set; }
    }
}
