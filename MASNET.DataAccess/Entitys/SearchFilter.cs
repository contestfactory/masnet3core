﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class SearchFilter
    {
       public SearchFilter()
        {
           this.keyword = "";
           this.sortby = "";
           this.page = 1;
           this.pageSize = 8;
           this.IsMinify = false;
           this.TieBreak = false;
           this.ShowWinner = 0;
           this.displayView = "";
           this.searchBy = "";
           this.IsSweepstakesWinner = 0;
           this.returnFormat = 0;
           this.ContestantsLayout = 1;
           this.categoryId = 0;
           this.tagId = 1;
           this.randomKey = "";
           this.applicationID = 0;
           this.contestID = 0;
           this.isPartial = false;
           this.WinnerType = 1; // Contestant 
           IsSweepstakesWinner = 0;
           sortDirection = "";
           Email = "";
        }

        public int applicationID { get; set; }
        public int contestID { get; set; }
        public string keyword { get; set; }
        public string searchBy { get; set; }
        public string sortby { get; set; }
        public string sortDirection { get; set; }
        public int page { get; set; }
        public int pageSize { get; set; }
        public int returnFormat { get; set; }
        public int ContestantsLayout { get; set; }
        public int tagId { get; set; }
        public int categoryId { get; set; }
        public string randomKey { get; set; }        
        public int IsSweepstakesWinner { get; set; }
        public bool IsMinify { get; set; }
        public bool TieBreak { get; set; }
        public int ShowWinner { get; set; }
        public string displayView { get; set; }
        public long ParentID {get; set;}  
        public int MatchViewType { get;set;} 

        public long shelterID { get; set; }

        public int queueID { get; set; }

        public bool isPartial { get; set; }
        public string PrizeName { get; set; }
        public string PrizeCode { get; set; }

        public int WinnerType { get; set; }
        public int WinnerStatus { get; set; }
        public long UserID { get; set; }
        public string Email { get; set; }

        // Greater Good
        public string EntryName { get; set; }
        public string ShelterName { get; set; }
        public string SubmittedBy { get; set; }
        public int CountryID { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }

        public string targetUpdateId { get; set; }
    }
}
