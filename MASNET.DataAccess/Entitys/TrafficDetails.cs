﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    public partial class TrafficDetails
    {
        public TrafficDetails()
        {

        }

        public long TrafficDetailsID { get; set; }
        public string RequestUrl { get; set; }
        public int TrafficID { get; set; }
        public long UserID { get; set; }
        public string IPAddress { get; set; }
        public string ReferrerUrl { get; set; }
        public string UserAgent { get; set; }
        public string OS { get; set; }
        public string Browser { get; set; }
        public string RequestInfors { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual Traffic Traffic { get; set; }
    }
}
