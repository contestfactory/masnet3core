﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class CFUpload
    {
        public string uploadID { get; set; }
        public string fileName { get; set; }
        public int block { get; set; }
        public bool isFirst { get { return block == 0; } set { } }
        public bool isLast { get; set; }
        public int bufferSize { get; set; }
        public byte[] data { get; set; }

        public string tempFolder { get; set; }
        public string tempFilePath { get { return string.Format(@"{0}\{1}.tmp", this.tempFolder, this.uploadID); } set { } }
        public string uploadFolderPath { get; set; }
        public string filePath { get { return string.Format(@"{0}\{1}", this.uploadFolderPath, this.fileName); } set { } }
        public string uploadFolderUrl { get; set; }
        public string fileUrl { get { return string.Format(@"{0}/{1}", this.uploadFolderUrl, this.fileName); } set { } }
    }

    public partial class CFUploadResult
    {
        public string uploadID { get; set; }
        public int block { get; set; }
        public byte status { get; set; }
        public string message { get; set; }
    }
}
