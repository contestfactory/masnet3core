﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class MessageMemberMetadata
    {
    }

    public partial class MessageMember
    {
        public long MessageMemberID { get; set; }
        public long MessageSessionID { get; set; }
        public long UserID { get; set; }
        public long MessageStatus { get; set; }

        public virtual string Username { get; set; }
        public virtual string UserPhoto { get; set; }
    }
}
