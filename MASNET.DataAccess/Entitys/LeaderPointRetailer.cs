﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class LeaderPointRetailer
    {
        public int RetailerID { get; set; }
        public string RetailerName { get; set; }
        public int RetailerStoreID { get; set; }
        public string StoreName { get; set; }

        public decimal AvgPoints { get; set; }
        public string FormattedAvgPoints { get { return this.AvgPoints.ToString("N1"); } set { } }

        public int TotalRespondent { get; set; }
        public string FormattedTotalRespondents { get { return this.TotalRespondent.ToString("N1"); } set { } }

        public int TotalMember { get; set; }
        public string FormattedTotalMembers { get { return this.TotalMember.ToString("N1"); } set { } }
    }
}
