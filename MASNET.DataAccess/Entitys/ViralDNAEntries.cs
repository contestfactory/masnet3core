﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ViralDNAEntries
    {
        public long RowNum { get; set; }
        public long ContestantID { get; set; }
        public int ContestID { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FormattedCreateDate { get; set; }
        public string ContetantName { get; set; }
        public Nullable<int> TotalVotes { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Username { get; set; }
        public string Thumbnail { get; set; }
        public Nullable<double> AvgScore { get; set; }
        public string DisplayAvgScore { get; set; }
        public Nullable<double> JudgeAvgScore { get; set; }
        public string DisplayJudgeAvgScore { get; set; }
        public Nullable<int> FanCnt { get; set; }
        public Nullable<int> JudgeCnt { get; set; }
        public Nullable<byte> RegistrationType { get; set; }
        public Nullable<int> SS_RegistrationType { get; set; }
        public string SS_RegistrationTypeName { get; set; }
        public byte Status { get; set; }
        public string DisplayStatus { get; set; }
        public int IsSweepstakesWinner { get; set; }
        public string Comment { get; set; }
        public string ApprovedByDisplayName { get; set; }
        public string SweepstakesPrize { get; set; }
        public DateTime? SweepstakesWinnerDate { get; set; }
        public int? SweepstakesPriority { get; set; }

        public string ContestantFirstName { get; set; }
        public string ContestantEmail { get; set; }
        public string ContestantPhone { get; set; }
        public string ContestantWebsite { get; set; }
        public string ContestantState { get; set; }

        public string ContestDomain { get; set; }
        public string DetailDesc { get; set; }
        public int CountryID { get; set; }
        public string CountryName { get; set; }

        public string SubmissionType { get; set; }

        public string Language { get; set; }
        public string SS_SubmissionID { get; set; }
        public string Country { get; set; }
        public string Destination { get; set; }
        public string ExperienceDate { get; set; }
        public string Essay { get; set; }
        public int CharacterCount { get; set; }
        public Nullable<System.DateTime> SS_CreatedDate { get; set; }
        public int GuestVotes { get; set; }
        public int UserVotes { get; set; }
        public int PrizeID { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ShelterName { get; set; }
        public string ShelterEmail { get; set; }
        public string ShelterCity { get; set; }
        public string ShelterState { get; set; }
        public string ShelterCountry { get; set; }

        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
    }
}
