﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{    
    partial class Config
    {
        public Nullable<bool> IsDisplay { get; set; }

        public Nullable<bool> ValidationRequired { get; set; }
        public Nullable<int> ValidationMinLength { get; set; }
        public Nullable<int> ValidationMaxLength { get; set; }
        public Nullable<int> ValidationMin { get; set; }
        public Nullable<int> ValidationMax { get; set; }
        public Nullable<bool> ValidationEmail { get; set; }
        public Nullable<bool> ValidationUrl { get; set; }
        public Nullable<bool> ValidationDate { get; set; }
        public Nullable<bool> ValidationNumber { get; set; }
        public Nullable<bool> ValidationDigits { get; set; }
    }    
}
