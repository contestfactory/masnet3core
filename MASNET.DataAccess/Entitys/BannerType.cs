﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class BannerType
    {
        public int ID { get; set; }
        public string TypeName { get; set; }
        public int MaxWidth { get; set; }
        public int MaxHeight { get; set; }
    }
}
