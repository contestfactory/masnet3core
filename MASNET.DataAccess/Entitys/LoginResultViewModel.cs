﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MASNET.OAuth.Models
{
    public class LoginResultViewModel
    {
        public LoginResultViewModel(bool success, string returnUrl)
        {
            Success = success;
            ReturnUrl = returnUrl;
        }

        public bool Success { get; set; }
        public string ReturnUrl { get; set; }
    }
}