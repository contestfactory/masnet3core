﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class MailArchive
    {
        public int ID { get; set; }
        public string from_alias { get; set; }
        public string mail_from { get; set; }
        public string mail_to { get; set; }
        public string mail_cc { get; set; }
        public string mail_bcc { get; set; }
        public string mail_subject { get; set; }
        public string mail_body { get; set; }
        public Nullable<bool> mail_IsBodyHtml { get; set; }
        public Nullable<byte> mail_status { get; set; }
        public Nullable<System.DateTime> mail_senddate { get; set; }
        public Nullable<byte> mail_priority { get; set; }
        public string mail_description { get; set; }
        public Nullable<short> mail_retried { get; set; }
        public Nullable<System.DateTime> mail_updated { get; set; }
        public string mail_attachments { get; set; }
        public byte Type { get; set; }
        public string memberid { get; set; }
        public int contestantDetailID { get; set; }
        public string TemplateName { get; set; }
        public string ToCSVFile { get; set; }
        public int CSVFileCount { get; set; }

        public int? ApplicationID { get; set; }
        public int? ContestID { get; set; }
        public int? UseType { get; set; }

        public string adhocStats { get; set; }
        public string adhocStatsJSON { get; set; }
        public DateTime? adhocStatsLastUpdate { get; set; }
    }
}
