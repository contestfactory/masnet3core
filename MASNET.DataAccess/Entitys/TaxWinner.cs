﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class TaxWinner
    {
        public int RowNum { get; set; }
        public int ID { get; set; }
        public int ContestID { get; set; }
        public string SSN { get; set; }
        public string PrizeName { get; set; }
        public string ContestName { get; set; }
        public Nullable<decimal> ARV { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string MailingAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<System.DateTime> WinDate { get; set; }
        public Nullable<bool> ShowFlag { get; set; }
        public string Comment { get; set; }

    }
}
