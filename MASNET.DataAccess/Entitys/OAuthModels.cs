﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MASNET.OAuth.Models
{
    public class OAuthModels
    {
        public string SS_ID { get; set; }

        //[Required(ErrorMessage = "The Screen Name field is required.")]        
        //[RegularExpression("^[A-Za-z0-9_-]*$", ErrorMessage = "Screen Name may only include alphanumeric characters, dashes and underscores.")]
        //[MaxLength(15, ErrorMessage = "Screen Name cannot be longer than 15 characters.")]
        [Display(Name = "Screen Name")]
        public string UserName { get; set; }
        
        //[EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        public string OAuthProvider { get; set; }

        public string ExternalLoginData { get; set; }
        public string SiteName { get; set; }
        public int? CampaignId { get; set; }
        public int? UserType { get; set; }
        public string ReturnUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime RegisteredDate { get; set; }
        public Nullable<int> RoleID { get; set; }
        public Nullable<int> ContestID { get; set; }
        public string Token { get; set; }
        public string ProviderName { get; set; }
    }
}