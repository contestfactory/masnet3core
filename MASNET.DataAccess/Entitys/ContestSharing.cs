﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    public partial class ContestSharing
    {
        public ContestSharing()
        {
            
        }

        public int ContestSharingID { get; set; }
        public int ContestID { get; set; }
        public string FaceBookTitle { get; set; }
        public string FaceBookDescription { get; set; }
        public string FaceBookImage { get; set; }
        public string TwitterDescription { get; set; }

        public string Contestant_FaceBookTitle { get; set; }
        public string Contestant_FaceBookDescription { get; set; }
        public string Contestant_FaceBookImage { get; set; }
        public string Contestant_TwitterDescription { get; set; }

        public Nullable<int> ActionID { get; set; }
        public Nullable<int> ApplicationID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public long ContestantID { get; set; }
        public Nullable<int> PageID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Thumbnail { get; set; }

        public Nullable<int> TemplateID { get; set; }
        public Nullable<int> UseType { get; set; }
        public Nullable<int> ShowHide { get; set; }

        public Nullable<int> FBSettingShowHide { get; set; }
        public virtual Contest Contest { get; set; } 
        public Nullable<bool> IsActive { get; set; }
        public int PointType { get; set; }
    }
}
