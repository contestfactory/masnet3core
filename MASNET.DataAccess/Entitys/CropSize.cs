﻿namespace MASNET.DataAccess
{
    public class CropSize
    {
        public int W { get; set; }

        public int H { get; set; }

        public string P { get; set; }

        public int Type { get; set; }
    }
}
