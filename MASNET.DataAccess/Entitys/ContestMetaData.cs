﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess {
    partial class Contest {
        public string CreatedByUser { get; set; }
        public Nullable<int> ParentCategoryID { get; set; }
        public virtual Clock SubmissionClock { get; set; }
        public virtual Clock VoteClock { get; set; }
        public virtual ContestDesign ContestDesign { get; set; }
        private Round _ActiveRound = null;
        public Round ActiveRound {
            get {
                if (this.Rounds != null && this.Rounds.Count > 0) {
                    _ActiveRound = this.Rounds.Where(r => r.RoundID == this.ActiveRoundID).FirstOrDefault();
                }

                return _ActiveRound;
            }
            set {
                _ActiveRound = value;
            }
        }

        public bool IsValidContestantDesc() {

            return

                (this.ShortDescForContestant == null || this.ShortDescForContestant.Length < 1550)
                && (this.ShortDescForContestantSecond == null || this.ShortDescForContestantSecond.Length < 1550)
                && (this.DetailDescForContestant == null || this.DetailDescForContestant.Length < 1000000)
                && (this.DetailDescForContestantSecond == null || this.DetailDescForContestantSecond.Length < 1000000);

        }

        public bool IsValidVoterDesc() {
            return
                (this.ShortDescForVoter == null || this.ShortDescForVoter.Length < 1550)
                && (this.ShortDescForVoterSecond == null || this.ShortDescForVoterSecond.Length < 1550)
                && (this.DetailDescForVoter == null || this.DetailDescForVoter.Length < 1000000)
                && (this.DetailDescForVoterSecond == null || this.DetailDescForVoterSecond.Length < 1000000);
        }


        public bool IsValidRule() {
            return
                this.IsShowDemographics.HasValue
                && this.IsAutoClone.HasValue
                && this.JudgeWeight.HasValue && this.JudgeWeight.Value >= 0 && this.JudgeWeight.Value <= 100
                && this.FanWeight.HasValue && this.FanWeight.Value >= 0 && this.FanWeight.Value <= 100
                && this.VotingResult.HasValue
                && this.DisplayVoter.HasValue
                && this.PhotoMin.HasValue
                && this.PhotoMax.HasValue
                && this.PhotoMin.Value <= this.PhotoMax.Value
                && this.AudioMin.HasValue
                && this.AudioMax.HasValue
                && this.AudioMin.Value <= this.AudioMax.Value
                && this.VideoMin.HasValue
                && this.VideoMax.HasValue
                && this.VideoMin.Value <= this.VideoMax.Value
                && this.DocMin.HasValue
                && this.DocMax.HasValue
                && this.DocMin.Value <= this.DocMax.Value;

        }
        public virtual string ContestTemplateName { get; set; }
    }

}
