﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class IntegrateResponse
    {
        public IntegrateResponse()
        {
            Message = "";
        }
        public string Result { get; set; }
        public object Message { get; set; }
        public object Data { get; set; }
        public string RedirectUrl { get; set; }
        public string ReturnUrl { get; set; }
        public string ErrorType { get; set; }
        public string ErrorCode { get; set; }
    }
}
