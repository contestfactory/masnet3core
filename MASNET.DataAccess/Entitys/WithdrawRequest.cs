﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class WithdrawRequest
    {
        public int WithdrawID { get; set; }
        public Nullable<int> TranID { get; set; }
        public int? UserId { get; set; }
        public string TypeOfVoter { get; set; }
        public Nullable<decimal> TransAmount { get; set; }
        public int? Status { get; set; }
        public Nullable<System.DateTime> TransDate { get; set; }
        public int TransCode { get; set; }
        public string Details { get; set; }
        public Nullable<int> ContestantID { get; set; }
        public int? ApplicationID { get; set; }
        public int? CampaignID { get; set; }
    }
}
