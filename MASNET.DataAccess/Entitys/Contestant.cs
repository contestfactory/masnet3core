﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class Contestant 
    {
        public Contestant(){}

        public long ContestantID { get; set; }
        public string Thumbnail { get; set; }
        public string Path { get; set; }
        public long EntryID { get; set; }
        public string ContestantName { get; set; }        
        public string Description { get; set; }   
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserShortName { get; set; }
        public string UserPhoto { get; set; }
        public byte MediaTypeID { get; set; }
        public byte SourceType { get; set; }
        public string TagName { get; set; }
        public int tagId { get; set; }
        public int KeyID {get; set; }
        public int CategoryID { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ContestantCreatedDate { get; set; }
        public string Url { get; set; }
        public int ViewCnt { get; set; }
        public int FanCnt { get; set; }
        public int DislikeCnt { get; set; }
        public int VoteNumber { get;set;}
        public int NumOfVotePerUser { get;set;}  
        public int userFanCnt { get;set;}
        public int userDislikeCnt { get;set;}
        public bool Votable { get;set;}
        public int ContestID { get; set; }
        public int MatchID { get; set; }
        public bool IsHighlight { get; set; }
        public int Type { get; set; }
        public int ActionID { get; set;}
        public long UserID { get; set;}
        public byte Converted { get; set; }
        public string DisplayCreatedDate { get; set; }
        public long ParentID { get; set; }

        public string ContestantFirstName { get; set; }
        public string ContestantLastName { get; set; }
        public int RantBackCnt { get; set; }
        public Vote RecentVote { get; set; }
        public string RandomKey { get; set; }

        public string MatchName {get;set;}
        public string MatchFullName { get; set; }
        public long VoteID { get; set; }
    }
}
