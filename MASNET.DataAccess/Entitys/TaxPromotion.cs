﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class TaxPromotion
    {
        public int RowNum { get; set; }
        public int ContestID { get; set; }
        public string ContestName { get; set; }
        public Nullable<bool> IsDisplay { get; set; }
    }
}
