﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    [Serializable]
    public class Question
    {        
        public string id { get; set; }
        public int QuestionID { get; set; }
        public int AnswerID { get; set; }
        public int CorrectAnswerID { get; set; }
        public string answer { get; set; }
        public string category { get; set; }
        public string type { get; set; }
        public string difficulty { get; set; }
        public string question { get; set; }
        public string correct_answer { get; set; }
        public List<string> incorrect_answers { get; set; }
        public int CategoryID { get; set; }
        public int LevelID { get; set; }
        public bool ShowHide { get; set; }
        public bool isDelete { get; set; }
        public int QuestionType { get; set; }
        public bool AllowComment { get; set; }
        public int ApplicationID { get; set; }
        public int ContestID { get; set; }
        public int? AmountComment { get; set; }
        public string Email { get; set; }
        public int ResponseId { get; set; }
        public string Response_Content { get; set; }
        public bool IsResponseCorrect { get; set; }
        public DateTime? ResponseDate { get; set; }
        public int RowNum { get; set; }

        public string CategoryName { get; set; }
        public string LevelName { get; set; }
        public int TotalResponses { get; set; }
        public int CorrectResponses { get; set; }
        public int WrongResponses { get; set; }
        public int Priority { get; set; }
        public string Thumbnail { get; set; }
        public int DisplayType { get; set; }
        public string Description { get; set; }

        public Question()
        {
            id = Guid.NewGuid().ToString();
            isDelete = false;
            ShowHide = true;
            ResponseId = 0;
            IsResponseCorrect = false;
        }

        public List<Answer> ListAnswers { get; set; }
        public List<QuestionLocalization> ListLocalizations { get; set; }
    }

    public class APIResponse
    {        
        public string response_code { get; set; }
        public List<Question> results { get; set; }        
    }

    public class Answer
    {        
        public int AnswerID { get; set; }
        public int QuestionID { get; set; }
        public string answer { get; set; }
        public bool IsCorrectAnswer { get; set; }
        public int FanCnt { get; set; }
        public float FanAvgScore { get; set; }
        public float AvgScore { get; set; }
        public float FanPercent { get; set; }
        public int QuestionType { get; set; }
        public string Thumbnail { get; set; }
        public int DisplayType { get; set; }
        public int ContestID { get; set; }
        public int ApplicationID { get; set; }
        public List<AnswerLocalization> AnswerLocalizations { get; set; }
    }

    public class AnswerLocalization
    {
        public int ID { get; set; }
        public int AnswerID { get; set; }
        public int QuestionID { get; set; }
        public string LocalAnswer { get; set; }
        public string LocalThumbnail { get; set; }
        public int LanguageID { get; set; }
        public string LanguageName { get; set; }
    }

    public class QuestionExport
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        public int FanCnt { get; set; }
        public float FanAvgScore { get; set; }
        public float AvgScore { get; set; }
        public string FanPercentDisplay { get; set; }
        public string RowNumDisplay { get; set; }
    }

    public class ResponsesAllQuestions
    {
        public long UserID { get; set; }
        public long TotalQuestionsAvailable { get; set; }
        public long TotalRegistrants { get; set; }
        public long TotalRespondents { get; set; }
        public long TotalResponses { get; set; }
        public long CorrectResponses { get; set; }
        public long WrongResponses { get; set; }
        //public long MyTotalResponses { get; set; }
        //public long MyCorrectResponses { get; set; }
        //public long MyWrongResponses { get; set; }
    }

    public class SpinWinnersExport
    {
        public int No { get; set; }
        public string RegisteredOn { get; set; }
        public string LastWin { get; set; }
        public int NumberOfWins { get; set; }
        public string DisplayNumberOfWins { get; set; }
        public string PrizeList { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Email { get; set; }

    }

    public class SpinPrizesExport
    {
        public int No { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int Quantity { get; set; }
        public int Awarded { get; set; }
        public int Remaining { get; set; }
    }
}
