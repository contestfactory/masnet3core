namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    #if DEBUG
        [Serializable()]
    #endif
    public partial class ReportDisplayItem
    {
        public ReportDisplayItem()
        {
            items = new List<ReportItem>();
        }
        public int UseType { get; set; }
        public int ReportType { get; set; }
        public int ApplicationID { get; set; }
        public int ContestID { get; set; }
        public int TemplateID { get; set; }
        public string CustomValue { get; set; }
        public List<ReportItem> items { get; set; }
        public bool IsReportDisplayItem { get; set; }
        public bool IsUpdated { get; set; }
    }
    public partial class ReportItem
    {
        public string ItemName { get; set; }
        public string DisplayName { get; set; }
        public bool IsDisplay { get; set; }
        public bool IsDownload { get; set; }
        public int Priority { get; set; }
    }
}
