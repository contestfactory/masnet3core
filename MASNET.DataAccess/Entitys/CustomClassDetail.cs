﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
#if DEBUG
    [Serializable()]
#endif
    public partial class CustomClassDetail
    {
        public int ID { get; set; }
        public Nullable<int> ClassID { get; set; }
        public Nullable<int> ThemeID { get; set; }
        public string color { get; set; }
        public Nullable<int> font_size { get; set; }
              
        public string ClassName { get; set; }
        public string DisplayName { get; set; }
    }
}
