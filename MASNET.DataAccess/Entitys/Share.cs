﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class Share
    {
        public Share()
        {
            customView = "_Sharing";
            contestantentryid = 0;
            contestantID = 0;
            returnFormat = 0;
            bFlag = false;
            isNotDeActivate = false;
            partialView = true;
            shareURL = "";
            ToolTipDataPlacement = "bottom";
            Title = "Share";
            IconType = "icon";
            NumOfEmailShare = 1;
            ShareGUID = Guid.NewGuid().ToString();
        }
        public string customView { get; set; }
        public List<int> actions { get; set; }
        public string shareURL { get; set; }
        public int? sourceID { get; set; }
        public bool isNotDeActivate { get; set; }
        public int? ContestID { get; set; }
        public int? ApplicationID { get; set; }
        public long contestantID { get; set; }
        public long contestantentryid { get; set; }
        public int returnFormat { get; set; }
        public bool bFlag { get; set; }
        public bool partialView { get; set; }

        public int ActionID { get; set; }
        public long UserID { get; set; }
        public string AnoUserID { get; set; }
        public string Title { get; set; }
        public string ToolTipDataPlacement { get; set; }
        public string IconType { get; set; }
        public int NumOfEmailShare { get; set; }
        public string ShareGUID { get; set; }
        public int SourceType { get; set; }
        public string Exclude { get; set; }

        public int MediaId { get; set; }
        public string VideoPath { get; set; }
        public string ShareLinkType { get; set; }
        public string ThumbnailUrl { get; set; }
        public string ThumbnailDefaultUrl { get; set; }
    }
}
