﻿namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public partial class PromotionalCode
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public Nullable<System.DateTime> GeneratedDate { get; set; }
        public Nullable<System.DateTime> ExpirationDate { get; set; }
        public string Comments { get; set; }
        public int Used { get; set; }
        public Nullable<int> Limited { get; set; }
        public int? ApplicationId { get; set; }
        public int? CampaignId { get; set; }
        public int PrizeId { get; set; }

        public string Prefix { get; set; }
               

        public int LanguageID { get; set; }
        public int IsValidCode { get; set; }

        
        //Prize
        public string PrizeName { get; set; }
        public string Description { get; set; }
        public string Description_2 { get; set; }
        public string Description_3 { get; set; }
        public string Description_4 { get; set; }
        public string Photo { get; set; }
        //End Prize
    }
}
