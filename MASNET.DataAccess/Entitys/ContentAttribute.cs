﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ContentAttribute
    {
        public string ContentField { get; set; }
        public string IDField { get; set; }
        public string Maxlength { get; set; }
        public bool Required { get; set; }
        public bool Multiline { get; set; }
        public string ValidateHandler { get; set; }

        public ContentAttribute()
        {
            Required = true;
            Multiline = true;
        }
    }
}
