﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class RegistrationType
    {
        public int ID { get; set; }
        public Nullable<byte> RegistrationTypeID { get; set; }
        public string RegistrationTypeName { get; set; }
        public int? ContestID { get; set; }
        public int? ApplicationID { get; set; }
        public int? UseType { get; set; }
        public int? Priority { get; set; }
        public string HashToken { get; set; }
        public long SinceId { get; set; }
        public int APIType { get; set; }
        public string APIURL { get; set; }
    }
}
