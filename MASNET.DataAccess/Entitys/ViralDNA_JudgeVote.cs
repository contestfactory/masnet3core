﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ViralDNA_JudgeVote
    {
        public int ID { get; set; }
        public int ContestID { get; set; }
        public string VoteDate { get; set; }
        public long UserID { get; set; }
        public int VoteCount { get; set; }

    }
}
