﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    [Serializable()]
    public partial class ContestDesign
    {
        public ContestDesign()
        {

        }

        public int ContestDesignID { get; set; }
        public int ContestID { get; set; }
        public string Theme { get; set; }
        public string HeaderSlogan { get; set; }
        public string HeaderImage { get; set; }
        public string FooterText { get; set; }
        public string FooterImage { get; set; }
        public string HeaderView { get; set; }
        public string FooterView { get; set; }
        public int? ThemeID { get; set; }
    }
}
