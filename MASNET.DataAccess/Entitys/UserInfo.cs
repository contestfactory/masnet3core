﻿namespace MASNET.DataAccess
{
    public class UserInfo {
        public string Id;
        public string UserName;
        public string Token;
    }
}
