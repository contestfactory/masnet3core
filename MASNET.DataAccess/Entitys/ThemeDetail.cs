﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
#if DEBUG
    [Serializable()]
#endif
    public partial class ThemeDetail
    {
        public int ID { get; set; }
        public int TemplateID { get; set; }
        public int ThemeID { get; set; }
        public int ShowHide { get; set; }
        public int UserType { get; set; }
        public int ContestID { get; set; }
        public int ApplicationID { get; set; }
        public virtual Theme theme { get; set; }

    }
}
