﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace MASNET.DataAccess
{
    public class DemoSliderModel
    {

        public string imageFolder { get; set; }
        public string outputFile { get; set; }
        public string rootUrl{ get; set; }
        public string returnUrl { get; set; }

        public string appName { get; set; }
        public string viewName { get; set; }

        //public HttpPostedFileBase uploadFile {get; set; }

        public string txtName { get; set; }
        public string txtShortDescription { get; set; }
        public string txtCFRole { get; set; }
        public string txtUrl { get; set; }
        public string ddlSite { get; set; }
        public string ddlTitleNames { get; set; }
        public string txtPriority { get; set; }
        public string action { get; set; }
        public string siteUrl { get; set; }

        public bool isUpdateSite { get; set; }
    }
}
