﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
#if DEBUG
    [Serializable()]
#endif
    public partial class Theme
    {
        public Theme()
        {
            ThemeName   = "Default";
            Layout      = "~/Views/Shared/Layouts/_Default.cshtml";
            Css         = "~/Content/themes/Default/site.css";
            Status      = 1;
            CustomClasses = new List<CustomClassDetail>();            
        }

        public int ID { get; set; }
        public string ThemeName { get; set; }
        public string Layout { get; set; }
        public string Css { get; set; }
        public int Status { get; set; }
        public string Photo { get; set; }
        public bool IsDisplay { get; set; }
        
        public int Type { get; set; }
        public List<CustomClassDetail> CustomClasses { get; set; }
        public string FilePath { get; set; }
        public int ParentID { get; set; }
        public long UserID { get; set; }
        public string ClassDetail { get; set; }
        public string ThemeFolder { get; set; }

        public int ShowHide { get; set; }
        public int InUsed { get; set; }

        public string HeaderView { get; set; }
        public string FooterView { get; set; }
        public int Priority { get; set; }
        public string Name { get; set; }
        public int ApplicationID { get; set; }
        public int ContestID { get; set; }
        public string ThemeNameFile { 
            get {
                if (string.IsNullOrEmpty(ThemeName))
                    return "";
                return ThemeName.Replace(" ", "");
            }
        }
        public int OldID { get; set; }
    }
}
