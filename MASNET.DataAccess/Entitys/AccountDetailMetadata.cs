﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using MvcPaging;

namespace MASNET.DataAccess
{
    public class AccountDetailMetadata
    {
    }

    public partial class AccountDetail
    {
        public int No { get; set; }
        public string Username { get; set; }
        public string UserPhoto { get; set; }
        public string ActionName { get; set; }
        public string ActionNameSecond { get; set; }
        public Nullable<int> PublicLevel { get; set; }
        public int ActionType { get; set; }
        public string ContestName { get; set; }
        public Nullable<decimal> Balance { get; set; }


        public virtual Registration Registration { get; set; }
        public virtual List<Comment> Comments { get; set; }
        public string PrizeName { get; set; }
        public string PrizeCode { get; set; }        
        public string Email { get; set; }
        public string IFSCatalogItem { get; set; }
        public string ItemSourceCode { get; set; }
        public string ItemShipMethod { get; set; }

        //User info
        public string Registration_Field_1 { get; set; }
        public string Registration_Field_2 { get; set; }
        public string Registration_Field_3 { get; set; }
        public string Registration_Field_4 { get; set; }
        public string Zip { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string NewsLetter { get; set; }

    }
}
