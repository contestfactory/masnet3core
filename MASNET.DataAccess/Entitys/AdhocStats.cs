﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class AdhocStats
    {
        public int Sent { get; set; }
        public int Delivered { get; set; }
        public int Opened { get; set; }
        public int Clicked { get; set; }
        public int Unsubscribed { get; set; }
        public int Complained { get; set; }
        public int SupressBounce { get; set; }
        public int SupressUnsubscribe { get; set; }
        public int SupressComplaint { get; set; }
        public int Bounce { get; set; }
        public int PermanentFail { get; set; }
        public int TemporaryFail { get; set; }
        public int TotalBounced
        {
            get { return this.SupressBounce + this.Bounce; }
        }
        public int TotalFailed
        {
            get { return this.PermanentFail + this.TemporaryFail; }
        }

        public DateTime? LastUpdated { get; set; }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
        public static AdhocStats FromJson(int sent_count, string data = "", string updatedData = "", DateTime? lastUpdated = null)
        {
            AdhocStats result = new AdhocStats() { Sent = sent_count };
            try
            {
                if (string.IsNullOrEmpty(data))
                {
                    var model = JsonConvert.DeserializeObject<dynamic>(updatedData);

                    if (model != null)
                    {
                        var stats = model["stats"] != null ? model["stats"][0] : null;
                        if (stats!= null)
                        {
                            result = new AdhocStats()
                            {
                                Clicked = stats["clicked"]["total"],
                                Delivered = stats["delivered"]["total"],
                                Opened = stats["opened"]["total"],
                                Sent = sent_count,
                                Unsubscribed = stats["unsubscribed"]["total"],
                                Complained = stats["complained"]["total"],
                                SupressBounce = stats["failed"]["permanent"]["suppress-bounce"],
                                SupressUnsubscribe = stats["failed"]["permanent"]["suppress-unsubscribe"],
                                SupressComplaint = stats["failed"]["permanent"]["suppress-complaint"],
                                Bounce = stats["failed"]["permanent"]["bounce"],
                                PermanentFail = stats["failed"]["permanent"]["total"],
                                TemporaryFail = stats["failed"]["temporary"]["espblock"],
                                LastUpdated = lastUpdated
                            };
                        }
                    }
                }
                else
                {
                    var model = JsonConvert.DeserializeObject<AdhocStats>(data);
                }
            }
            catch (Exception ex)
            {

            }

            return result;
        }
    }
}
