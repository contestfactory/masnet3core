﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class StaticPage
    {
        public StaticPage()
        {
            IsDisplay = true;
        }

        public int ID { get; set; }
        public int ApplicationID { get; set; }
        public int CampaignID { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public bool IsDisplay { get; set; }
    }
}
