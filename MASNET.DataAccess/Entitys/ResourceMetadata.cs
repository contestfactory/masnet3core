﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ResourceMetadata
    {
    }

    public partial class Resource
    {
        public string PageName { get; set; }
        public string ApplicationName { get; set; }
        public string LanguageCode { get; set; }
        public string LanguageName { get; set; }
    }
}
