//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MASNET.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class Folder
    {
        public Folder()
        {
            this.UploadFile = new HashSet<UploadFile>();
        }
    
        public int FolderID { get; set; }
        public string FolderName { get; set; }
        public string ShortDesc { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public string Photo { get; set; }
        public Nullable<long> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<bool> IsDisplay { get; set; }
        public Nullable<int> PhotoCnt { get; set; }
        public Nullable<int> AudioCnt { get; set; }
        public Nullable<int> VideoCnt { get; set; }
        public Nullable<int> DocumentCnt { get; set; }
    
        public virtual ICollection<UploadFile> UploadFile { get; set; }
    }
}
