﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public partial class OAuthenticate
    {
        public int OAuthenticateId { get; set; }
        public long UserId { get; set; }
        public string SS_ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string OAuthProvider { get; set; }
        public string ToKen { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int ApplicationId { get; set; }
        public int CampaignId { get; set; }
    }
}
