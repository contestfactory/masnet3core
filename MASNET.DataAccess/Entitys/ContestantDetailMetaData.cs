﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess {

    partial class ContestantDetail {
        public Nullable<double> AvgScore { get; set; }
        public Nullable<double> FanPercent { get; set; }
        public Nullable<double> JudgePercent { get; set; }
        public Nullable<double> FanAvgScore { get; set; }
        public Nullable<double> JudgeAvgScore { get; set; }
        public Nullable<int> FanCnt { get; set; }
        public Nullable<int> JudgeCnt { get; set; }
        public Nullable<int> ViewCnt { get; set; }
        
        public int AnoFanCnt { get; set; }
        public int UserFanCnt { get; set; }

        

        public double ShowPercent(double? JudgeWeight, double? FanWeight) {
            JudgeWeight = JudgeWeight ?? 0;
            FanWeight = FanWeight ?? 0;
            JudgePercent = JudgePercent ?? 0;
            FanPercent = FanPercent ?? 0;

            return (double)(JudgePercent * (JudgeWeight / 100.0) + FanPercent * (FanWeight / 100.0));
        }

        public double ShowAvgScore(double? JudgeWeight, double? FanWeight) {
            JudgeWeight = JudgeWeight ?? 0;
            FanWeight = FanWeight ?? 0;
            JudgeAvgScore = JudgeAvgScore ?? 0;
            FanAvgScore = FanAvgScore ?? 0;


            return (double)(JudgeAvgScore * (JudgeWeight / 100.0) + FanAvgScore * (FanWeight / 100.0));
        }
        
    }
}
