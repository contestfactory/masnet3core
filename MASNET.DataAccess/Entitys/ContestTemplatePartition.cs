﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class ContestTemplatePartition
    {
        public int ID {get;set;}
        public string DetailDesc {get;set;}
        public string DetailDescForContestant { get; set; }
        public string DetailDescForVoter { get; set; }
    }
}
