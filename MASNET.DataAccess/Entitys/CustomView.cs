﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MASNET.DataAccess
{
    public class CustomView
    {
        public int CustomViewId { get; set; }
        public Nullable<int> PageId { get; set; }
        public string PageName { get; set; }
        public Nullable<int> ApplicationId { get; set; }
        public Nullable<int> CampaignId { get; set; }
        public string ViewName { get; set; }
        public int UseType { get; set; }
        public int ThemeID { get; set; }
        public string ThemeName { get; set; }
        public string Description{ get; set; }
    }
}
