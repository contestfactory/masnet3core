﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.Custom
{
    public class CustomRouteSetting
    {
        public int ID { get; set; }
        public string domainValue { get; set; }
        public string siteExtension { get; set; }
        public string controllerValue { get; set; }
        public string actionValue { get; set; }
        public bool isDisplay { get; set; }
    }
}
