﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.Custom
{
    public class BookingIntention
    {
        public long? UserId { get; set; }
        public string Variety { get; set; }
        public int? Amount { get; set; }
    }
}
