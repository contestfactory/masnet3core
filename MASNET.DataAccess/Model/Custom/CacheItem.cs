﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.Custom
{
    public class CacheItem
    {
        public string ResourceKey { get; set; }
        public int UseType { get; set; }
        public string CacheName { get; set; }
        public string PageName { get; set; }
        public int CacheType { get; set; } // 1: PageID > 0, 0: PageID = 0
        public int LanguageID { get; set; }
        public int CacheIndex { get; set; }
    }

    public class CacheResource
    {
        public string Value { get; set; }
        public string Help { get; set; }
        public long ID { get; set; }
    }
}
