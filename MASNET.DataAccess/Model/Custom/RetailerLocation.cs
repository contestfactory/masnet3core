﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.Custom
{
    public class RetailerLocation
    {
        public string Name { get; set; }

        public string City { get; set; }

        public string StreetAddress { get; set; }

        public string Province { get; set; }

        public string PostalCode { get; set; }

        public string Phone { get; set; }
    }
}
