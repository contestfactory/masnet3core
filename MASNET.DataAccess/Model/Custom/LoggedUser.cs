﻿using MASNET.DataAccess;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.Custom
{
    public class LoggedUser
    {
        public LoggedUser() { }
        public LoggedUser(Registration reg)
        {
            if (reg != null && reg.UserID > 0)
            {
                this.UserID = reg.UserID;
                this.UserType = reg.UserType;
                this.Username = reg.Username;
                this.ValidationKey = reg.ValidationKey;
                this.ApplicationID = reg.ApplicationID;
                this.ContestID = reg.ContestID;
                this.Email = reg.Email;
                this.FirstName = reg.FirstName;
                this.LastName = reg.LastName;
                this.IAB = reg.IAB;
                this.Photo = reg.Photo;
                this.CountryID = reg.CountryID;
                this.Token = reg.Token;
                this.MongoID = reg.MongoID;
                this.LanguageID = reg.LanguageID;
                this.ReturnUrl = reg.ReturnUrl;
                this.CompanyName = reg.Registration_Field_4;
                this.RetailerID = reg.RetailerID;
                this.RetailerStoreID = reg.RetailerStoreID;
            }
        }

        public long UserID { get; set; }
        public int? UserType { get; set; }
        public string Username { get; set; }
        public string ValidationKey { get; set; }
        public int? ApplicationID { get; set; }
        public int? ContestID { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public bool? IAB { get; set; }
        public string LastName { get; set; }
        public string Photo { get; set; }
        public int? CountryID { get; set; }
        public string Token { get; set; }
        public string IntergratedToken { get; set; }
        public string MongoID { get; set; }
        public int? LanguageID { get; set; }
        public string AnoUserID { get; set; }
        public string ReturnUrl { get; set; }
        public string CompanyName { get; set; }
        public int? RetailerID { get; set; }
        public int? RetailerStoreID { get; set; }
        public string RandomKey { get; set; }
        public string UserFolder
        {
            get
            {
                string shortenUsername = Username;
                if (!string.IsNullOrEmpty(shortenUsername))
                {
                    if (shortenUsername.Length > 50)
                        shortenUsername = $"{shortenUsername.Substring(0, 50)}{UserID}";
                }
                return shortenUsername;
            }
        }
        public List<SelectListItem> FoldersList = new List<SelectListItem>();
        public double Balance { get; set; }
        public int ByPassFBLike { get; set; }
        public bool TwoFactorVirified { get; set; }
        public string CodeVerify { get; set; }
    }
}
