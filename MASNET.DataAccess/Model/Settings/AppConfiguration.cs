﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Model.Settings
{
    public class AppConfiguration
    {
        public int AgentRoleID { get; set; }
        public string ServerDomain { get; set; }
        public string SubDomain { get; set; }
        public string Debug { get; set; }
        public string IsBigData { get; set; }
        public string AARP_ServiceURL { get; set; }
        public string AARP_LoginURL { get; set; }
        public string AARP_TrackingURL { get; set; }
        public string RouteSettings { get; set; }
        public string SiteInfo_Type { get; set; }
        public string UsingHeaderMenuLinks { get; set; }

        public string ShowBanner { get; set; }

        public string FBAppId { get; set; }

        public string Integrated { get; set; }
        public string IntegrateGetToken { get; set; }
        public string SessionTimeout { get; set; }
        public string Kickbox_APIKey { get; set; }

    }
}
