﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum ApplicationStatus
        {
            UnPublished = 0,
            Published = 1,
            Pending = 0,
            Active = 1,
        }

        public enum ApplicationTemplateType
        {
            DRTV = 1
        }
    }
}
