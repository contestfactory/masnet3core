﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum UseType
        {
            Global = 1,
            Site = 2,
            Campaign = 3,
            ContestTemplate = 4,
            SiteContest = 5,
            ApplicationTemplate = 6,
            Theme = 7
        }
    }
}
