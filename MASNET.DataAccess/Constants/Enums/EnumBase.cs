﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum TimeZone
        {
            PST = 151
        }

        public enum PageID  //table Page.PageID
        {
            CountdownPage = 188,
            LandingPage = 189,
            SubmissionPage = 8,
            SubmissionThankPage = 92,
            ContestEndedPage = 202,
            RegisterPage = 80,
            RegisterThankPage = 91,
            MyEntriesPage = 7,
            MatchPage = 9,
            ContestantPage = 39,
            ChangePassword = 19,
            SpinPage = 204,
            ResultPage = 205,
            FacebookSetting = 206,
            Login = 81,
            ContestDetails = 37,
            ContestantListWithMedia = 13,
            ContestantEntriesList = 209,
            RoundPage = 212,
            HomePage = 16,
            FEJudges = 231,
            FEJudgeList = 232,
            FEJudgeDetail = 78,
            PointLeader = 93,
            GetPointLeader = 94,
            ContestDirectory = 10,
            MediaList = 233,
            ContestantEntriesForUser = 213,
            Quiz_Index = 274,
            Quiz_GetQuestion = 272,
            Application_EditSite = 97,
            ProfilePage = 5,
            Registration_Register = 1,
            ContestShare = 275,
            ReportContestants = 25,
            ReportUsers = 280,
            ViralDNA = 115,
            Winnners = 253,
            _winners = 254
        }

        public enum CacheMode
        {
            Normal = 1,
            Private = 2
        }

        public enum DisplayItemOption
        {
            Nope = 0,
            Edit = 1,
            View = 2,
            Hide = 3
        }

        public enum ShowHideType
        {
            Template = 1,
            Resource = 2,
            Config = 3,
            DisplayItem = 4,
            CustomFieldSite = 5,
            ContestDesign = 6,
            CustomFields = 7,
            VoteCriteriaSet = 8,
            Period = 9,
            Contest = 10,
            Contestant = 11,
            SweepStake = 12,
            Prize = 13,
            ExtensionFieldDisplayItem = 14,
            CTAButtonDisplayItem = 15,
            ContestTemplate = 16,
            ThemeDetail = 17,
            CTAButton = 18,
            ContestSharing = 19,
            BannerPosition = 20
        }

        public enum RestrictStateType
        {
            AllStates = 1,
            ContiguosAllStates = 2,
            SpecificStates = 3
        }

        public enum ProductMode
        {
            SelfServe = 1,
            StandAlone
        }

        public enum ConfigKeyID
        {
            DefaultRoleID = 1,
            AutoFill_MaxContestants = 2,
            ChangePassword_MaxRetries = 3,
            CustomerService = 4,
            HomePage_NumOfContests = 5,
            ImageQuality = 6,
            Red5Folder = 7,
            RmtpRrl = 8,
            TieBreakJudgeID = 9,
            UnsubscribeEmail = 10,
            VirtualFolderPath = 11,
            VirtualUploadPath = 12,
            VirtualUploadPath1 = 13,
            VirtualUploadPath2 = 14,
            PointLeaders_MaxNumber = 15,
            ShowSecondDataField = 16,
            ShowTimeout = 17,
            ShowTimeOutInSeconds = 18,
            LoginStyle = 19,
            ShowNews = 20,
            Cat_SubCat_Separator = 21,
            CountViewByRefresh = 22,
            TimeBetweenVotes = 23,
            MaxPhotoSize = 24,
            MaxAudioSize = 25,
            MaxVideoSize = 26,
            MaxDocumentSize = 27,
            VideoDimension = 28,
            DisplayFilledContests = 29,
            LoginOAuthUrl = 30,
            ShowAdminLeftMenu = 31,
            DefaultCountry = 32,
            ShowMediaStyle = 33,
            ShowMediaSize = 34,
            DefaultTheme = 35,
            DesignTheme = 36,
            ViralDNADisplay_Option = 37,
            Submission_Layout = 38,
            ViralDNA_ReportName = 39,
            ViralDNA_ReportRename = 40,
            AdminContestLayout = 41,
            TopMenu_Competition = 42,
            TopMenu_Competition_Enter = 43,
            TopMenu_Competition_Vote = 44,
            TopMenu_Public = 45,
            TopMenu_Public_Media = 46,
            TopMenu_Public_Judges = 47,
            TopMenu_Public_PointLeader = 48,
            TopMenu_Public_Winners = 49,
            TopMenu_InviteFriends = 50,
            Media_BackgroundColor = 51,
            UGC_ContestTemplates_CategoryDefault = 52,
            CustomerRedirectAfterLogin = 53,
            UserRedirectAfterLogin = 54,
            JudgeRedirectAfterLogin = 55,
            SuperAdminRedirectAfterLogin = 56,
            AllowJudgeVoteOneByOne = 57,
            Registration_AutoConfirm = 58,
            DisplayMyStuff_Profile = 59,
            DisplayMyStuff_Media = 60,
            DisplayMyStuff_Points = 61,
            DisplayMyStuff_Account = 62,
            DisplayMyStuff_Entries = 63,
            DisplayMyStuff_Friends = 64,
            DisplayMyStuff_Activities = 65,
            DisplayMyStuff_Messages = 66,
            DisplayMyStuff_Votes = 67,
            DisplayContestantReviewStatus = 68,
            FeeId_Upgrade = 69,
            FeeId_UpgradeOptions = 70,
            UseJudgeIAB = 71,
            DRTV_DefaultContestID = 72,
            ShowVoteDetailButton = 73,
            Contestant_ShowVoteComment = 74,
            Contest_Votes_ShowVotePerRound = 75,
            SiteName = 76,
            Login_ShowOpenId = 77,
            Account_ShowSubmissionFee = 78,
            Reports_ShowSubmissionPendingIRB = 79,
            Account_ShowPromotionalCodes = 80,
            Media_DefaultImage = 81,
            Registration_ShowLoginForm = 82,
            SubmitContestant_ShowRegisterStep = 83,
            Contest_ShowInfomation = 84,
            User_RedirectAfteRegister = 85,
            Submit_PhoneReviewFeeIDs = 86,
            Submit_RedirectAfterComplete = 87,
            Contest_ShowInFE = 88,
            Match_ShowInFE = 89,
            Contestant_ShowOrtherEntriesPopup = 90,
            Contestant_DeleteInterval = 91,
            Media_ShowMediaTitle = 92,
            Report_ShowJudgeInventorCommunications = 93,
            Account_ShowWithdrawRequest = 94,
            Comment_MessagePerPage = 95,
            Comment_CharactersPerMessage = 96,
            Comment_IntervalRefresh = 97,
            Account_ShowDepositToMembers = 98,
            Account_ShowDepositToJudges = 99,
            Account_ShowApiCredentials = 100,
            Account_ShowPointsToMembers = 101,
            ContestBuilder_ShowOldNextPre = 102,
            Contest_DemoContestID = 104,
            VimeoPlayerEmbed = 105,
            Reports_ShowSubmissions = 106,
            Reports_ShowVotes = 107,
            Reports_ShowSpins = 108,
            DisplayMyStuff_Spins = 109,
            SharingURL = 112,
            Register_Birthday_Format = 114,
            CustomerIFrame = 122,
            Email_FromAddress = 121,
            AllowAnonymousVote = 139,
            ContestEndedPageExpire = 184,
            Vote_MaxJudges = 195,
            Contest_ShowButtons = 198,

            FBClientSecret = 145,
            GoogleAppId = 146,
            GoogleClientSecret = 147,
            TWAppId = 148,
            TWClientSecret = 149,
            FBAppId = 150,
            PerVoteType = 192,
            Contest_ShowCustomizeButton = 200,
            ViralDNA_Submisison_DefaultSort = 204,
            Submit_AllowEitherMedia = 205,
            Submit_UploadDocThumbnail = 206,
            Vote_NextSubmissionAfterVote = 217,
            ContestTemplate_SourceType = 225,
            Submit_UseShelter = 229,
            Submit_AllowSubmit = 230,
            WinnerType = 231,
            Winners_PrizeControlType = 232,
            ContestHeaderExcludePages = 235,
            StaticPage_AllowCreate = 237,
            AnonyousType = 244,
            Match_ShowFirstName = 245
        }

        public enum Action
        {
            Facebook = 1,
            Twitter = 6,
            Submit = 22,
            Vote = 23,
            CorrectVote = 24,
            PayFee = 25,
            PostComment = 26,
            Deposit = 27,
            AssignPoint = 28,
            EmailShare = 29,
            Login = 30,
            Spin = 31,
            Registration = 32,
            AMOERegister = 33,
            UploadPhoto = 45,
            UploadAudio = 46,
            UploadVideo = 47,
            ShareReturn = 42,

            ReportAbuse = 48,
            Favorite = 49,
            UnFavorite = 50,
            VotedSubmission = 51,
            UnVote = 52,
            Dislike = 53,
            UnDislike = 54,
            Pinterest = 55,
            SecuredEmailShare = 56,
            Donation = 57,
            UploadText = 58,

            Reject = 59,
            ClearFlag = 60,
            ApproveContestant = 61,
            JugdeVote = 62,
            ShareRegistration = 63,

            FacebookShareRegistration = 64,
            TwitterShareRegistration = 65,
            EmailShareRegistration = 66,

            FacebookShareReturn = 67,
            TwitterShareReturn = 68,
            EmailShareReturn = 69,
            CreateTeam = 70,

            Link = 71,
            Embed = 72
        }

        public enum TemplateKey
        {
            Action_Share = 1,
            SEM_ForgotPassword = 2,
            SEM_JudgesNextRound = 3,
            SEM_InvitationfromYourFriend = 4,
            SEM_Register = 5,
            SEM_Successfulsubmission = 6,
            SEM_CongratulationsYouwonFree = 7,
            SEM_UserChangeEmailAddress = 8,
            SEM_CommentApproval = 9,
            SEM_ManualContestantApproval = 10,
            SEM_ContestYourcontesthasstarted = 11,
            Action_Vote = 12,
            Action_Submit = 13,
            Action_Comment = 14,
            SEM_AdminContactUs = 15,
            SEM_JudgeNewSubmission = 16,
            SEM_JudgeNewDRTVSubmission = 17,
            SEM_DRTVRejectionLetterGeneral = 18,
            SEM_RegisterAutoAuthentication = 19,
            SEM_AdminRegistration = 43,
            SEM_SuccessfulSubmissionforPhoneReview = 20,
            SEM_SuccessfulSubmissionforPromoCode = 21,
            SEM_AdminPendingSubmission = 22,
            SEM_SuccessfulSubmissionPendingApproval = 23,
            SEM_SpinYouarethewinner = 24,
            Action_Spin = 25,
            SEM_DRTVContactInventor = 26,
            AutoGenerateRule = 27, SEM_SubmissionLoser = 28,
            SEM_ReviewerNewSongSubmission = 30,
            SEM_ReportAbuse = 33,
            SEM_Sponsorship = 34,
            SEM_EmailShare = 35,
            SEM_SubmissionReject = 36,
            SEM_SubmissionRantBack = 37,
            SEM_SecuredInvitationfromYourFriend = 38,
            SEM_FriendAcceptedYourInvitation = 39,
            SEM_Admin_PendingProfile = 40,

            SEM_AdminPendingRegistration = 44,
            SEM_AdminRegistrationApproved = 45,
            SEM_EmailShare_Team = 46,
        }

        public enum ReturnFormat
        {
            Html = 0,
            Json = 1,
            JsonP = 2
        }

        public enum ActionType
        {
            Share = 0,
            UserAction = 1,
            AdminAction = 2
        }

        public enum PointFrequencyType
        {
            WholePromotion = 1,
            SeperatePromotionAndSubmission = 2,
        }

        public enum SubmissionPayFeeState
        {
            Success = 0,
            InvalidPromoCode = 1,
            Redirect = 2
        }

    }
}
