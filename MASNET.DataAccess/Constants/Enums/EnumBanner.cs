﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum BannerOpenType
        {
            SameWindow = 1,
            NewWindow
        }

        public enum BannerType
        {
            Image = 1,
            //Audio = 2,
            Video = 3,
            HTML = 4,
            GoogleBanner = 5
        }
        public enum BannerPosition
        {
            H1 = 1,
            H2 = 2,
            R1 = 3,
            R2 = 4,
            R3 = 5,
            R4 = 6,
            B1 = 7,
            B2 = 8
        }
    }
}
