﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {

        public enum TranType
        {
            Credit = 1,
            Debit = 2
        }

        public enum CookieType
        {
            Login,
            lang,
            dir,
            ShareId,
            Token
        }


        public enum RegistrationType
        {
            Site = 1,
            Mongo = 2,
            Twitter = 3,
            Instagram = 4,
            AMOE = 5,
            Google = 6,
            Facebook = 7,
            Tumblr = 8,
            LinkedIn = 9
        }

        public enum RegistrationStatus
        {
            Pending = 0,
            Activated = 1,
            Blocked = 2,
            Disabled = 3
        }

        public enum RegistrationUserType
        {
            [Description("Customer")]
            Customer = 0,
            [Description("Registrants")]
            User = 1,
            [Description("Judges")]
            Judge = 2,
            [Description("ContestUser")]
            ContestUser = 3,
            [Description("SuperAdmin")]
            SuperAdmin = 4,
            [Description("Moderators")]
            Moderator = 5,
            [Description("Reviewers")]
            Reviewer = 6,
            [Description("Shelters")]
            Shelter = 7,
            [Description("Admin")]
            Admin = 8
        }

        public enum LoginStatus
        {
            Success,
            InvalidUserNameOrPassword,
            Pending,
            Blocked,
            Disabled,
            RequiredLogin
        }

        public enum LoginType
        {
            Admin = 1,
            User = 2
        }

        public enum AccountDetailStatus
        {
            New = 0,
            Shared = 1
        }

        public enum MailStatus
        {
            Fail = 0,
            Success = 1,
            Pending = 2,
            Running = 3,
            Stopped = 5,
            Draft = 4
        }
        public enum AdhocMailMode
        {
            Draft,
            Pending,
            Sent
        }

    }

}
