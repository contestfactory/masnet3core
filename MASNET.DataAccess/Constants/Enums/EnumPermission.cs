﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum AdminShowHide
        {
            SuperAdmin = 1,
            CustomerSiteAdmin = 2
        }

        public enum PermissionType
        {
            Site = 1,
            Campaign
        }

        public enum Permissions
        {
            Admin_Parameters = 1,
            Admin_Parameters_Configs = 2,
            Admin_Parameters_Design = 3,
            Admin_Parameters_EmailTemplates = 4,
            Admin_Parameters_Forms = 5,
            Admin_Parameters_Languages = 6,
            Admin_Parameters_VoteMethod = 7,
            Admin_Parameters_TimelineVoting = 8,
            Admin_Parameters_Sharing = 9,
            Admin_Reports = 10,
            Admin_Reports_Reports = 11,
            Admin_Reports_ViralDNA = 12,
            Admin_Sweepstakes = 13,
            Admin_Sweepstakes_GenerateReport = 14,
            Admin_Sweepstakes_Periods = 15,
            Admin_Sweepstakes_SelectWinners = 16,
            Admin_Contests = 17,
            Admin_Members = 18,
            Admin_Members_Customers = 19,
            Admin_Members_Admins = 153,
            Admin_Members_Judges = 20,
            Admin_Members_Users = 21,
            Admin_Members_Permissions = 22,
            Admin_Reports_Responses = 125,
            Sweepstakes_SelectWinner_AllowUpload = 126,
            Admin_Retailer_Leaderboard = 154,

            #region Admin_Campaign_
            Admin_Campaign_GetStarted = 23,
            Admin_Campaign_Timeline = 24,
            Admin_Campaign_Votes = 25,
            Admin_Campaign_SignUpForm = 26,
            Admin_Campaign_SocialShare = 27,
            Admin_Campaign_SystemEmails = 28,
            Admin_Campaign_Banners = 52,
            Admin_Campaign_Design = 29,
            Admin_Campaign_Publish = 30,
            Admin_Campaign_MessageBoard = 31,
            Admin_Campaign_SocialSites = 32,
            Admin_Campaign_Configuration = 33,
            Admin_Campaign_Languages = 34,
            Admin_Campaign_GenerateReport = 35,
            Admin_Campaign_Periods = 36,
            Admin_Campaign_SelectWinner = 37,
            Admin_Campaign_Moderate = 38,
            Admin_Campaign_ReportActivity = 39,
            Admin_Campaign_ReportMessageBoard = 40,
            Admin_Campaign_ReportSubmissions = 41,
            Admin_Campaign_ReportVotes = 42,
            Admin_Campaign_ReportViralDNA = 43,
            Admin_Campaign_ReportsVoteResult = 112,
            Admin_Campaign_Registrants = 44,
            Admin_Campaign_Judges = 45,
            Admin_Campaigns = 46,
            Admin_Campaigns_Parameters = 47,
            Admin_Campaigns_Reports = 48,
            Admin_Campaigns_Sweepstakes = 49,
            Admin_Campaigns_Members = 50,
            Admin_Campaign_Submission = 51,

            Admin_Campaign_Parameters_ContestPages = 97,
            Admin_Campaign_Parameters_Legal = 98,
            Admin_Campaign_Parameters_SocialMedia = 99,
            Admin_Campaign_Parameters_ContestEmails = 100,
            Admin_Campaign_Parameters_Analytics = 101,
            Admin_Campaign_Parameters_Publish = 102,
            Admin_Campaign_Members_Moderators = 103,
            Admin_Campaign_ViralDNA_ImportSubmissions = 150,
            #endregion

            #region Admin_Site_
            Admin_Site_Details = 60,
            Admin_Site_SignupForms = 61,
            Admin_Site_Languages = 62,
            Admin_Site_Configuration = 63,
            Admin_Site_SystemEmails = 64,
            Admin_Site_Banners = 89,
            Admin_Site_Design = 65,
            Admin_Site_Publish = 66,
            Admin_Site_News = 67,
            Admin_Site_Contests = 68,
            Admin_Site_Sweepstakes = 69,
            Admin_Site_Registrants = 70,
            Admin_Site_Judges = 71,
            Admin_Site_ReportsActivity = 72,
            Admin_Site_ReportsMessageBoard = 73,
            Admin_Site_ReportsSubmissions = 74,
            Admin_Site_ReportsVotes = 75,
            Admin_Site_ViralDNA = 76,
            Admin_Site_Parameters = 77,
            Admin_Site_Reports = 78,
            Admin_Site_Members = 79,
            Admin_Site_SweepstakesGenerateReport = 80,
            Admin_Site_SweepstakesPeriods = 81,
            Admin_Site_SweepstakesSelectWinners = 82,
            Admin_Site_Contest_TimeLine = 83,
            Admin_Site_Contest_Submission = 84,
            Admin_Site_Contest_Votes = 85,
            Admin_Site_Contest_MessageBoard = 86,
            Admin_Site_Contest_Details = 87,
            Admin_Site_Contest_Publish = 88,
            Admin_Site_Contest_Questionnaire = 90,
            Admin_Site_SocialMedia_FBSetting = 91,
            Admin_Site_AdhocEmail = 113,

            //Moderations
            Moderation_Submissions = 92,
            Moderation_PendingSubmissions = 93,
            Moderation_Registrants = 94,
            Moderation_Comments = 95,
            Moderation_Votes = 96


            #endregion
        }

        public enum OwnerType
        {
            Profile = 1,
            Contest = 2,
            Product = 3
        }

        public enum BelongToSiteType
        {
            User = 1,
            Contest = 2
        }

        public enum CheckPermissionResponseType
        {
            Redirect = 1,
            Message = 2
        }
    }
}
