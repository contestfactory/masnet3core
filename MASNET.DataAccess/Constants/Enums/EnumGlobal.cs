﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum Countries
        {
            Australia = 14,
            Canada = 39,
            UK = 230,
            US = 231
        }

        public enum Language
        {
            English = 1,
            Farsi = 2,
            Spanish = 3,
            French = 4,
            Vietnamese = 5
        }
    }
}
