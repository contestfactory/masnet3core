﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum ContestStatuses
        {
            [Description("In Process")]
            InProcess = 1,
            Open = 2,
            [Description("Open Expired")]
            OpenExpired = 3,
            [Description("Pending Approval")]
            PendingApproval = 4,
            [Description("Pending Active")]
            PendingActive = 5,
            Active = 6,
            Over = 7,
            Tied = 8,
            Completed = 9,
            [Description("Pending Open")]
            PendingOpen = 10,
            OpenActive = 11,
            Filled
        }

        public enum ContestCreatedFrom
        {
            Admin = 1,
            Campaign
        }

        public enum ContestTemplateType
        {
            [Description("Stand Alone")]
            StandAlone = 1,
            [Description("Brackets")]
            Brackets = 2,
            [Description("Self Serve")]
            SelfServe = 3,
            [Description("Elections")]
            Elections = 4,
            [Description("Sweepstakes")]
            Sweepstakes = 5,
            [Description("SpinZone")]
            SpinZone = 6,
            Contests = 7
        }
    }
}
