﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MASNET.DataAccess.Constants.Enums
{
    public partial class Enums
    {
        public enum SiteType
        {
            Main,
            Customer,
            Campaign,
            SiteContest
        }

        public enum SiteStatus
        {
            Valid, //Active
            InvalidSite, //Not existed
            Unpublished, //Draft
            Hidden,
            Over,
            Expired,
            PendingStart
        }

        public enum MenuEditSite
        {
            Details,
            Voting,
            Sharing,
            Forms,
            Resources,
            Configs,
            EmailTemplates,
            Design,
            Publish,
            Moderate,
            Analytics,
            News,
            Contests,
            Users,
            Judges,
            Reports,
            Customers,
            Criteria,
            SweepStake,
            Timeline,
            Submissions,
            MessageBoard,
            ReviewQA,
            ReviewFiles
        }

        public enum SweepType
        {
            Period = 1,
            Contest = 2
        }

        public enum FavoriteType
        {
            Contestant = 1,
            Shelter = 2
        }

        public enum PointType
        {
            ShareComplete = 1,
            ClickOnIcon = 2,
            FriendReturn = 3,
            FriendReturnAndRegister = 4
        }
    }

}
