﻿using MASNET.DataAccess;
using Masnet3.Business.Repository.CustomViews;
using Masnet3.Business.Repository.Layout;
using Masnet3.Common.Caching;
using Masnet3.Common.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Masnet3.Business.Repository.Applications;
using Masnet3.Business.Repository.Contests;
using Masnet3.Business.Repository;
using MASNET.DataAccess.Constants.Enums;
using Microsoft.Extensions.Configuration;
using System.IO;
using Masnet3.Common.Helper;
using Masnet3.Common.Statups;
using System.Web;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Http.Features;
using Newtonsoft.Json;
using System.Net;
using MASNET.DataAccess.Model.Settings;

namespace Masnet3.Common
{
    public class Commons
    {


        public static AppConfiguration GetConfigFromAppsettings()
        {
            try
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
                var config = builder.Build();
                var appConfiguration = new AppConfiguration();
                config.GetSection("AppConfiguration").Bind(appConfiguration);
                return appConfiguration;
            }
            catch
            {
                return new AppConfiguration(); ;
            }
        }

        public static string GetConfigKeyFromAppsettings(string key)
        {
            try
            {
                var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
                var config = builder.Build();
                return config.GetValue<string>($"AppConfiguration:{key}");
            }
            catch
            {
                return string.Empty;
            }
        }

        public static string Username(Registration user)
        {
            string result = user.FirstName;
            if (!string.IsNullOrEmpty(user.LastName))
                result += " " + user.LastName.Substring(0, 1) + ".";
            if (string.IsNullOrEmpty(result))
                result = user.Username;
            return result;
        }

        public static string Username(string FirstName, string LastName, string Username)
        {
            Registration reg = new Registration()
            {
                FirstName = FirstName,
                LastName = LastName,
                Username = Username
            };
            return Commons.Username(reg);
        }

        public static string AreaName()
        {
            return "mas";
        }

        public static bool IsPreview
        {
            get
            {
                if (GlobalVariables.httpContextObj.HttpContext.Items["IsPreview"] != null)
                {
                    return (GlobalVariables.httpContextObj.HttpContext.Items["IsPreview"].ToString()  == "1");
                }
                else
                {
                    return false;
                }
            }
        }

        public static string GetConfig(string key)
        {
            try
            {
                string result = Config[key];
                return result;
            }
            catch (Exception e)
            {
                return "";
            }

        }

        public static string GetConfig(string key, int ApplicationID, int CampaignID, bool useCurrentConfig = false, bool autoCreate = false)
        {
            try
            {
                if (useCurrentConfig)
                {
                    return Commons.Config[key];
                }
            }
            catch (Exception ex) { }

            var keyValue = GetConfigKeyFromAppsettings(key);
            if (GlobalVariables.httpContextObj.HttpContext != null && !string.IsNullOrEmpty(keyValue))
            {
                return keyValue;
            }

            int ConfigKeyID = 0;
            try
            {
                ConfigKeyID = (int)Enum.Parse(typeof(Enums.ConfigKeyID), key);
            }
            catch (ArgumentException)
            {
                ConfigKeyID = 0;
            }
            string strWhere = " WHERE ConfigKey = @ConfigKey ";
            if (ConfigKeyID > 0)
            {
                strWhere = string.Format(" WHERE  ConfigKeyID = {0} ", ConfigKeyID);
            }

            string sql = "";
            //Auto create config
            if (autoCreate)
            {
                sql = string.Format(@"
                        IF NOT EXISTS (SELECT 1 FROM Config WHERE ConfigKey = @ConfigKey {0})
                        INSERT INTO Config(ConfigKeyID,ConfigKey,[Value],IsDisplay,IsCustom, ApplicationID, ContestID, UseType)
                        SELECT ConfigKeyID,ConfigKey,[Value],IsDisplay,IsCustom, @ApplicationID, @ContestID, @UseType
                        FROM   Config " +
                    Commons.Join(new
                    {
                        LeftTable = "Config",
                        Table = "Config",
                        OutTable = "NewConfig",
                        IDField = "ConfigID",
                        JoinFields = "ConfigKeyID",
                        editApplicationID = ApplicationID,
                        editCampaignID = CampaignID,
                        editTemplateID = 0
                    }) + strWhere, FilterUilities.GetFilter(ApplicationID, CampaignID, 0));

                StartupStaticDI.repositoryBasic.ExecuteSql(sql, new
                {
                    ApplicationID,
                    ContestID = CampaignID,
                    UseType = Commons.GetUseType(ApplicationID, CampaignID, 0),
                    ConfigKey = key
                });
            }

            sql = @"  SELECT *
                          FROM	Config " +
                    Commons.Join(new
                    {
                        LeftTable = "Config",
                        Table = "Config",
                        OutTable = "NewConfig",
                        IDField = "ConfigID",
                        JoinFields = "ConfigKeyID",
                        editApplicationID = ApplicationID,
                        editCampaignID = CampaignID,
                        editTemplateID = 0
                    }) + strWhere;

            var objConfig = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<Config>(sql, new { ApplicationID = ApplicationID, CampaignID = CampaignID, ConfigKey = key })
                                .DefaultIfEmpty(new Config() { Value = "" })
                                .FirstOrDefault();
            if (key.ToLower() == "VirtualFolderPath".ToLower())
            {
                objConfig.Value = string.Format(objConfig.Value, System.Net.Dns.GetHostName());
            }
            return objConfig.Value;
        }

        public static Dictionary<string, string> Config
        {
            get
            {
                var siteinfo = GlobalVariables.SiteInformation;
                int contestID = siteinfo.GetContestID();
                string cachename = String.Format("Config_{0}_{1}", siteinfo.ApplicationID, contestID);
                var cached = CacheUilities.cacheManager.Get<Dictionary<string, string>>(cachename);
                if (cached != null)
                {
                    return cached;
                }
                //No Cache
                {
                    var dic = new Dictionary<string, string>();
                    List<Config> configs;
                    string sql = "";
                    string JoinCommon = Commons.Join(new
                    {
                        LeftTable = "Config",
                        Table = "Config",
                        OutTable = "NewConfig",
                        IDField = "ConfigID",
                        JoinFields = "ConfigKeyID",
                        editApplicationID = siteinfo.ApplicationID,
                        editCampaignID = contestID,
                        editTemplateID = 0
                    });
                    sql = @" SELECT	Config.ConfigID, ConfigKey,Value
                                 FROM	Config " +
                                 JoinCommon + @"
                                 WHERE	ISNULL(GroupName,'') <> 'MediaServer'
                                 UNION
                                     SELECT	Config.ConfigID, ConfigKey,Value
                                     FROM	Config " +
                                     JoinCommon + @" WHERE	ISNULL(GroupName,'') = 'MediaServer' AND IsDisplay = 1";

                    configs = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<Config>(sql, new { ApplicationID = siteinfo.ApplicationID, CampaignID = contestID }).ToList();
                    foreach (var item in configs)
                    {
                        string temp = !dic.TryGetValue(item.ConfigKey, out temp) ? "0" : "1";
                        if (temp == "0")
                        {
                            dic.Add(item.ConfigKey, item.ConfigKey == "VirtualFolderPath" ? item.Value.Replace("{0}", System.Net.Dns.GetHostName()) : item.Value);
                            CacheUilities.cacheManager.Set(cachename, dic);
                        }
                    }
                    return dic;
                }
            }
        }

        public static Config GetConfigObject(string key, int ApplicationID, int CampaignID, int TemplateID = 0)
        {
            string sql = "";
            string sqlWhere = "";
            int ConfigKeyID = 0;
            try { ConfigKeyID = (int)Enum.Parse(typeof(Enums.ConfigKeyID), key); }
            catch (ArgumentException) { ConfigKeyID = 0; }
            if (ConfigKeyID > 0)
                sqlWhere = string.Format(" WHERE 1=1 AND ConfigKeyID = {0} ", ConfigKeyID);
            else
                sqlWhere = string.Format(" WHERE 1=1 AND ConfigKey = @ConfigKey ", key);

            sql = @"SELECT * FROM Config " +
                    Commons.Join(new
                    {
                        LeftTable = "Config",
                        Table = "Config",
                        OutTable = "NewConfig",
                        IDField = "ConfigID",
                        JoinFields = "ConfigKeyID",
                        editApplicationID = ApplicationID,
                        editCampaignID = CampaignID,
                        editTemplateID = TemplateID
                    }) + sqlWhere;

            var objConfig = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<Config>(sql, new { ConfigKey = key })
                                .DefaultIfEmpty(new Config() { Value = "" })
                                .FirstOrDefault();

            if (key.ToLower() == "VirtualFolderPath".ToLower())
            {
                objConfig.Value = string.Format(objConfig.Value, System.Net.Dns.GetHostEntry("").HostName);
            }

            return objConfig ?? new Config() { Value = "" };
        }


        public static string GetCustomView(int themeID, int pageID, int? applicationId = null, int? campaignId = null)
        {
            string cachename = "Theme_CustomView";
            string cachetheme = "Theme_Item";
            string ViewPath = "";
            List<CustomView> customViews = CacheUilities.cacheManager.Get<List<CustomView>>(cachename);
            Theme objTheme = CacheUilities.cacheManager.Get<Theme>(cachetheme);
            CustomView customView = new CustomView();
            if (customViews == null)
            {
                customViews = StartupStaticDI.customViewRepository.GetList().ToList();
                CacheUilities.cacheManager.Set(cachename, customViews);
            }

            int selectedThemeId = themeID;
            if (objTheme == null || objTheme.ID != themeID)
            {
                objTheme = StartupStaticDI.themeRepository.GetThemeById(themeID);
                CacheUilities.cacheManager.Set(cachetheme, objTheme);
            }
            if (objTheme != null && objTheme.Type == 2 && objTheme.ParentID > 0)
            {
                selectedThemeId = objTheme.ParentID;
            }
            if (customViews != null)
            {
                if (applicationId >= 0 || campaignId >= 0)
                {
                    SiteInformation siteInformation = GlobalVariables.SiteInformation;
                    applicationId = applicationId ?? siteInformation.ApplicationID;
                    campaignId = campaignId ?? siteInformation.CampaignID;

                    customView = customViews.Where(n => n.ThemeID == selectedThemeId && n.PageId == pageID && (n.ApplicationId == applicationId || n.ApplicationId == null) && n.CampaignId == campaignId).FirstOrDefault();
                }
                if (customView == null || customView.CustomViewId == 0)
                {
                    customView = customViews.Where(n => n.ThemeID == selectedThemeId && n.PageId == pageID).FirstOrDefault();
                }

                if (customView != null)
                {
                    ViewPath = customView.ViewName;
                }
            }
            return ViewPath;
        }

        public static double GetSpanTime(int? TimeZoneID = 0)
        {
            double result = 0;
            return result;
        }

        public static Enums.UseType GetUseType(int? applicationID = null, int? campaignID = null, int? contestTemplateID = null)
        {
            //ContestTemplate
            if (contestTemplateID > 0)
            {
                return Enums.UseType.ContestTemplate;
            }
            try
            {
                Enums.UseType? usetype = GetUseTypeByCurrentUrl();
                if (usetype.HasValue)
                {
                    return usetype.Value;
                }
                if (applicationID == null && campaignID == null && contestTemplateID == null)
                {
                    SiteInformation siteInfo = GlobalVariables.SiteInformation;
                    applicationID = applicationID ?? siteInfo.ApplicationID;
                    campaignID = campaignID ?? siteInfo.CampaignID;
                }
                else
                {
                    applicationID = applicationID ?? 0;
                    campaignID = campaignID ?? 0;
                }
            }
            catch { }

            //Campaign, SiteContest
            if (campaignID > 0)
            {
                if (applicationID > 0)
                {
                    return Enums.UseType.SiteContest;
                }
                else
                {
                    return Enums.UseType.Campaign;
                }
            }

            //Global
            if (applicationID == -1 && campaignID == -1)
            {
                return Enums.UseType.Global;
            }

            //ApplicationTemplate, Site           
            if (applicationID > 0)
            {
                if (GlobalVariables.httpContextObj.HttpContext != null)
                {
                    var app = StartupCommon.DicApplications.Where(c => c.Value.ApplicationID == applicationID).FirstOrDefault();
                    if (app.Key != null)
                    {
                        return Enums.UseType.Site;
                    }
                    else
                    {
                        return Enums.UseType.ApplicationTemplate;
                    }
                }
                else
                {
                    var app2 = StartupStaticDI.applicationRepository.GetApplicationById(applicationID.Value);
                    if (app2 != null)
                    {
                        return app2.IsTemplate ? Enums.UseType.ApplicationTemplate : Enums.UseType.Site;
                    }
                }
            }
            return Enums.UseType.Site;
        }

        public static Enums.UseType? GetUseTypeByCurrentUrl()
        {
            if (GlobalVariables.httpContextObj.HttpContext != null)
            {
                try
                {
                    HttpRequest request = GlobalVariables.httpContextObj.HttpContext.Request;
                    string currentUrl = request.GetRawUrl().ToString().ToLower();
                    if (currentUrl.Contains("customercontest/details") && string.IsNullOrWhiteSpace(request.Query["isCreate"]) == false)
                    {
                        if (request != null && request.Query["CreatedFrom"] == "1")
                        {
                            return Enums.UseType.SiteContest;
                        }
                        return Enums.UseType.Campaign;
                    }
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        public static Dictionary<string, string> ConvertToTokensDictionary(object value, int convertType = 0)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (value != null && value != DBNull.Value)
            {
                Type type = value.GetType();
                List<PropertyInfo> properties = type.GetProperties().ToList();
                foreach (var item in properties)
                {
                    string strValue = string.Empty;
                    object obj = item.GetValue(value);
                    strValue = (obj == null || obj == DBNull.Value) ? string.Empty : obj.ToString();

                    result.Add(string.Format(convertType.Equals(0) ? "{0}{1}{0}" : "{1}", GlobalVariables.SeparatorTemplateToken, item.Name), strValue);
                }
            }
            return result;
        }

        public static bool IsPrivateCache()
        {
            string key = "Resource_CacheMode";
            bool result = false;
            try
            {
                result = Config[key] == ((int)Enums.CacheMode.Private).ToString();
            }
            catch (Exception ex)
            {
                // do not thing
            }
            return result;
        }

        public static string AddParameter(string url, string paramName, object paramValue)
        {
            var uriBuilder = new UriBuilder(url);
            var query = HttpUtility.ParseQueryString(uriBuilder.Query);
            query[paramName] = paramValue.ToString();
            uriBuilder.Query = query.ToString();
            return uriBuilder.ToString();
        }

        public static bool IsShowContestRule(SiteInformation site)
        {
            return site.SiteType == Enums.SiteType.Campaign;
        }

        #region Check Context

        public static bool IsCollette100DaysContext(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("collette100days");
        }

        public static bool isAvonContext(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("maritz-avon");
        }

        public static bool isSyngentaContext(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("syngenta");
        }

        public static bool IsAARP2017Context(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("aarpdream-25daysofheroes") || IsAARPDreamPhase2(site);
        }

        public static bool IsAARPDreamPhase2(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain == "aarpdream2018phase2" || domain == "aarpdreamphase2" || (domain == "aarpdream" && site.EndDate != null && DateTime.Now > site.EndDate);
        }
        public static bool IsAirbnb(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("airbnb");
        }

        public static bool isGreaterGoodContext(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("greatergood") || domain.Contains("shelterchallenge");
        }

        public static bool ColletteFinal(SiteInformation site, string contestDomain = "")
        {
            string domain = !string.IsNullOrEmpty(contestDomain) ? contestDomain.ToLower() : site.ContextName.ToLower();
            return domain.Contains("comewithmephase");
        }


        #endregion

        #region Dynamic SQL

        public static string Join(dynamic attributes)
        {
            Dictionary<string, string> dic = ConvertToTokensDictionary(attributes, 1);
            string leftTable = dic["LeftTable"];
            string table = dic["Table"];        //table working on e.g. "Template"
            string outTable = "OutTable";       //out temp table e.g.   "NewTemplate"
            string IDField = dic["IDField"];    //ID field          
            string JoinFields = dic["JoinFields"]; string[] arr = JoinFields.Split(',');
            int editApplicationID = -1;
            int editCampaignID = -1;
            int editTemplateID = -1;
            bool useHidded = false;
            if (dic.ContainsKey("UseHidded")) Boolean.TryParse(dic["UseHidded"], out useHidded);
            if (dic.ContainsKey("editCampaignID")) Int32.TryParse(dic["editCampaignID"], out editCampaignID);
            if (dic.ContainsKey("editApplicationID")) Int32.TryParse(dic["editApplicationID"], out editApplicationID);
            if (dic.ContainsKey("editTemplateID")) Int32.TryParse(dic["editTemplateID"], out editTemplateID);
            if (dic.ContainsKey("OutTable")) outTable = dic["OutTable"];
            var TemplateIDField = table.Equals("Template", StringComparison.CurrentCultureIgnoreCase) ? "ContestTemplateID" : "TemplateID";
            var ContestIDField = table.Equals("CTAButton", StringComparison.CurrentCultureIgnoreCase) || table.Equals("Period", StringComparison.CurrentCultureIgnoreCase) ? "CampaignID" : "ContestID";

            string extraTable = "";
            int ContestTemplateID = 0;
            Enums.UseType UserType = GetUseType(editApplicationID, editCampaignID, editTemplateID);

            if (table.ToLower() == "contestsharing")
            {
                if (UserType.Equals((Enums.UseType.Global)))
                {
                    extraTable = string.Format(" JOIN (SELECT {0} FROM {1} WHERE UseType=1) {2} ON {3}.{0} = {2}.{0}", IDField, table, outTable, leftTable);
                }
                if (UserType.Equals((Enums.UseType.Site)))
                {
                    string ApplicationTemplateID = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID).ApplicationTemplateID.ToString();
                    extraTable = string.Format(@"JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE TempResource.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {3} ) Private                                                       
                                                       FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {4}) TempResource ON " + GetJoinFields(arr, "Private", "TempResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, ApplicationTemplateID, outTable, leftTable);

                }

                if (UserType.Equals((Enums.UseType.Campaign)))
                {
                    ContestTemplateID = StartupStaticDI.contestRepository.GetContestByContestId(editCampaignID).TemplateID.GetValueOrDefault();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE TempResource.{0} END {0}
                                                    FROM (SELECT {0}, {1} FROM {2} WHERE UseType=3 AND ContestID = {3} ) Private                                                    
                                                    FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND TemplateID = {4}) TempResource ON " + GetJoinFields(arr, "Private", "TempResource") + @"                                                                                                                                                                                
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable);
                }

                if (UserType.Equals((Enums.UseType.SiteContest)))
                {
                    string ApplicationTemplateID = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID).ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN SiteResource.{0} > 0 THEN SiteResource.{0} ELSE TempResource.{0} END {0}
                                                    FROM (SELECT {0}, {1} FROM {2} WHERE UseType=5 AND ContestID = {3} ) Private                                                    
                                                    FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {4}) SiteResource ON " + GetJoinFields(arr, "Private", "SiteResource") + @"                                                                                                                                                                                
                                                    FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {7}) TempResource ON " + GetJoinFields(arr, "Private", "TempResource") + @"                                                                                                                                                                                 
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);
                }


                if (UserType.Equals((Enums.UseType.ContestTemplate)))
                {
                    extraTable = string.Format(@" JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND TemplateID = {3}) 
                                                  {4} ON {5}.{0} = {4}.{0}
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable);
                }


                if (UserType.Equals((Enums.UseType.ApplicationTemplate)))
                {
                    extraTable = string.Format(@" JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {3}) 
                                                      {4} ON {5}.{0} = {4}.{0}                                
                                                    ", IDField, JoinFields, table, editApplicationID, outTable, leftTable);
                }


            }
            else if (table.ToLower() == "sweepstake")
            {
                int PeriodID = -1;
                if (dic.ContainsKey("OutTable") && dic.ContainsKey("PeriodID"))
                {
                    Int32.TryParse(dic["PeriodID"], out PeriodID);
                }
                string filter = PeriodID > 0 ? string.Format(" PeriodID = {0}", PeriodID) : " IsActive = 1 ";

                if (UserType.Equals((Enums.UseType.Global)))
                {
                    extraTable = string.Format(@" JOIN (SELECT {0} 
                                                        FROM {1} 
                                                        JOIN Period ON Period.PeriodID = {1}.ReferID
                                                        WHERE {4} AND UseType=1  
                                                        ) {2} ON {3}.{0} = {2}.{0} 
                                                ", IDField, table, outTable, leftTable, filter);
                }

                if (UserType.Equals((Enums.UseType.Site)))
                {
                    extraTable = string.Format(@"JOIN (SELECT {0}, {1} FROM {2} 
                                                       JOIN Period ON Period.PeriodID = {2}.ReferID 
                                                       WHERE {7} AND UseType=2 AND ApplicationID = {3}                                                        
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, 0, outTable, leftTable, filter);

                }

                if (UserType.Equals((Enums.UseType.Campaign)))
                {
                    ContestTemplateID = StartupStaticDI.contestRepository.GetContestByContestId(editCampaignID).TemplateID.GetValueOrDefault();
                    extraTable = string.Format(@" JOIN (SELECT {0}, {1} FROM {2} 
                                                        JOIN Period ON Period.PeriodID = {2}.ReferID 
                                                        WHERE {7} AND UseType=3 AND CampaignID = {3}  
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable, filter);
                }

                if (UserType.Equals((Enums.UseType.SiteContest)))
                {
                    string aplicationTemplateID = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID).ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE SiteResource.{0} END {0}
                                                    FROM (SELECT {0}, {1} FROM {2} 
                                                          JOIN Period ON Period.PeriodID = {2}.ReferID
                                                          WHERE {8} AND UseType=5 AND CampaignID = {3} ) Private                                                    
                                                    FULL JOIN 
                                                         (SELECT {0}, {1} FROM {2} 
                                                          JOIN Period ON Period.PeriodID = {2}.ReferID
                                                          WHERE IsActive = 1 AND UseType=2 AND ApplicationID = {4}) SiteResource ON " + GetJoinFields(arr, "Private", "SiteResource") + @"                                                                                                                                                                                                                                   
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, aplicationTemplateID, filter);
                }


                if (UserType.Equals((Enums.UseType.ContestTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT {0} FROM {2} 
                                                        JOIN Period ON Period.PeriodID = {2}.ReferID
                                                        WHERE {6} AND UseType=4 AND TemplateID = {3}
                                                       ) {4} ON {5}.{0} = {4}.{0}
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable, filter);


                if (UserType.Equals((Enums.UseType.ApplicationTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT {0} FROM {2} 
                                                        JOIN Period ON Period.PeriodID = {2}.ReferID
                                                        WHERE {6} AND UseType=6 AND ApplicationID = {3}) 
                                                      {4} ON {5}.{0} = {4}.{0}                                
                                                    ", IDField, JoinFields, table, editApplicationID, outTable, leftTable, filter);


            }
            else if (table.ToLower() == "period")
            {
                if (UserType.Equals((Enums.UseType.SiteContest)))
                {

                    string ApplicationTemplateID = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID).ApplicationTemplateID.ToString();
                    extraTable = string.Format(@"   JOIN (SELECT {0}, {1}, 1 Priority FROM {2} WHERE UseType=5 AND " + ContestIDField + @" = {3}
                                                    UNION ALL
                                                    SELECT {0}, {1}, 2 Priority FROM {2} WHERE UseType=2 AND ApplicationID = {4}                                                        
                                                    UNION ALL                                                   
                                                    SELECT {0}, {1}, 3 Priority FROM {2} WHERE UseType=6 AND ApplicationID = {7}                                                                                                                                                                                
                                                    UNION ALL
                                                    SELECT {0}, {1}, 4 Priority FROM {2} WHERE UseType = 1) {5} ON {5}.{0} = {6}.{0}  
                                                    ORDER BY {5}.Priority               
                                                ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);

                }
            }
            else if (table.ToLower() == "categorymapping")
            {
                if (UserType.Equals((Enums.UseType.Global)))
                    extraTable = string.Format(" JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=1) {3} ON {4}.{1} = {3}.{1}", IDField, JoinFields, table, outTable, leftTable);

                if (UserType.Equals((Enums.UseType.Site)))
                {
                    string ApplicationTemplateID = "0";
                    Application app = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID);
                    if (app != null)
                    {
                        ApplicationTemplateID = app.ApplicationTemplateID.ToString();
                    }

                    extraTable = string.Format(@"JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} ELSE Global.{1} END {1}, CASE WHEN Private.{1} > 0 THEN Private.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=2 AND ApplicationID = {3} ) Private
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=1) Global ON " + GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=6 AND ApplicationID = {4}) TempResource ON " + GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{1} = {5}.{1}                                
                                                ", IDField, JoinFields, table, editApplicationID, ApplicationTemplateID, outTable, leftTable);
                }

                if (UserType.Equals((Enums.UseType.Campaign)))
                {
                    var firstOrDefault = StartupStaticDI.contestRepository.GetContestByContestId(editCampaignID);
                    if (firstOrDefault != null)
                    {
                        ContestTemplateID = firstOrDefault.TemplateID.GetValueOrDefault();
                    }

                    extraTable = string.Format(@" 
                        JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN AppResource.{0} > 0 THEN AppResource.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} WHEN TempResource.{1}>0 THEN TempResource.{1} ELSE Global.{1} END {1},
                        CASE WHEN Private.ID > 0 THEN Private.isDisplay  WHEN AppResource.ID > 0 THEN AppResource.isDisplay WHEN TempResource.ID>0 THEN TempResource.isDisplay ELSE Global.isDisplay END isDisplay
                        FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=3 AND CampaignID = {3} ) Private
                        FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType = 1) Global ON " + GetJoinFields(arr, "Private", "Global") + @"  
                        FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {4}) TempResource ON " + GetJoinFields(arr, "Global", "TempResource") + @"    
                        FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=2 AND ApplicationID = {7}) AppResource ON " + GetJoinFields(arr, "Global", "AppResource") + @"                                                                                                                                                                                 
                        ) {5} ON {6}.{1} = {5}.{1}                                
                       ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable, editApplicationID);

                }

                if (UserType.Equals((Enums.UseType.SiteContest)))
                {

                    string ApplicationTemplateID = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID).ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN AppResource.{0}>0 THEN AppResource.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} WHEN AppResource.{1}>0 THEN AppResource.{1} WHEN TempResource.{1}>0 THEN TempResource.{1} ELSE Global.{1} END {1}, CASE WHEN Private.ID > 0 THEN Private.isDisplay WHEN AppResource.ID>0 THEN AppResource.isDisplay WHEN TempResource.ID>0 THEN TempResource.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=5 AND CampaignID = {3} ) Private
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType = 1) Global ON " + GetJoinFields(arr, "Private", "Global") + @"                                                         
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=6 AND ApplicationID = {7}) TempResource ON " + GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                  
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=2 AND ApplicationID = {4}) AppResource ON " + GetJoinFields(arr, "Global", "AppResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{1} = {5}.{1}                                
                                                ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);

                }

                if (UserType.Equals((Enums.UseType.ContestTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN TempResource.{0} > 0 THEN TempResource.{0} ELSE Global.{0} END {0}, CASE WHEN TempResource.{1} > 0 THEN TempResource.{1} ELSE Global.{1} END {1}, CASE WHEN TempResource.ID > 0 THEN TempResource.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {3}) TempResource                                                   
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType = 1) Global ON " + GetJoinFields(arr, "TempResource", "Global") + @"                                                                                                         
                                                       ) {4} ON {5}.{1} = {4}.{1}                                
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable);


                if (UserType.Equals((Enums.UseType.ApplicationTemplate)))

                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} ELSE Global.{1} END {1}, CASE WHEN Private.ID > 0 THEN Private.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=6 AND ApplicationID = {3}  ) Private
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=1 ) Global ON " + GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       ) {4} ON {5}.{1} = {4}.{1}                                
                                                ", IDField, JoinFields, table, editApplicationID, outTable, leftTable);
            }
            //Config
            else
            {
                if (UserType.Equals((Enums.UseType.Global)))
                {
                    extraTable = string.Format(" JOIN (SELECT {0} FROM {1} WHERE UseType=1) {2} ON {3}.{0} = {2}.{0}", IDField, table, outTable, leftTable);
                }

                if (UserType.Equals((Enums.UseType.Site)))
                {
                    string ApplicationTemplateID = "0";
                    Application app = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID);
                    if (app != null)
                    {
                        ApplicationTemplateID = app.ApplicationTemplateID.ToString();
                    }
                    extraTable = string.Format(@"JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {3} ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=1) Global ON " + GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {4}) TempResource ON " + GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, ApplicationTemplateID, outTable, leftTable);
                }

                if (UserType.Equals((Enums.UseType.Campaign)))
                {
                    var firstOrDefault = StartupStaticDI.contestRepository.GetContestByContestId(editCampaignID);
                    if (firstOrDefault != null)
                    {
                        ContestTemplateID = firstOrDefault.TemplateID.GetValueOrDefault();
                    }
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=3 AND " + ContestIDField + @" = {3} ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType = 1) Global ON " + GetJoinFields(arr, "Private", "Global") + @"  
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {4}) TempResource ON " + GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable);

                }
                if (UserType.Equals((Enums.UseType.SiteContest)))
                {

                    string ApplicationTemplateID = StartupStaticDI.applicationRepository.GetApplicationById(editApplicationID).ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN AppResource.{0}>0 THEN AppResource.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=5 AND " + ContestIDField + @" = {3} ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType = 1) Global ON " + GetJoinFields(arr, "Private", "Global") + @"                                                         
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {7}) TempResource ON " + GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                  
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {4}) AppResource ON " + GetJoinFields(arr, "Global", "AppResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);

                }

                if (UserType.Equals((Enums.UseType.ContestTemplate)))
                {
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN TempResource.{0} > 0 THEN TempResource.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {3}) TempResource                                                   
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType = 1) Global ON " + GetJoinFields(arr, "TempResource", "Global") + @"                                                                                                         
                                                       ) {4} ON {5}.{0} = {4}.{0}                                
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable);
                }


                if (UserType.Equals((Enums.UseType.ApplicationTemplate)))
                {
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {3}  ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=1 ) Global ON " + GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       ) {4} ON {5}.{0} = {4}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, outTable, leftTable);
                }

            }

            return extraTable;
        }

        public static string GetJoinFields(string[] arr, string leftTable, string outTable)
        {
            string result = "";
            foreach (var field in arr)
                result += (result == "" ? "" : " AND ") + string.Format("{0}.{2} = {1}.{2}", leftTable, outTable, field);
            return result;
        }

        #endregion

        #region Default Value

        public static List<string> GetSyngentaVarieties()
        {
            return new List<string>
            {
                "S007-Y4",
                "S0009-M2",
                "S006-W5",
                "S008-N2",
                "S0007-B7X",
                "S006-M4X",
                "S0009-D6",
                "S003-L3"
            };
        }

        #endregion

        public static Config GetPrivateConfig(int applicationID, int contestID, int configKeyID, bool useCurrent = false, string configKeyName = "")
        {
            Config config = null;
            var site = GlobalVariables.SiteInformation;
            if (string.IsNullOrEmpty(configKeyName))
                configKeyName = ((Enums.ConfigKeyID)configKeyID).ToString();
            string key = CacheUilities.GetCacheKeyByUseType(configKeyName, applicationID, contestID, prefix: "cfg");
            var cacheResult = CacheUilities.cacheManager.Get<Config>(key);
            if (cacheResult == null)
            {
                var useType = GetUseType(applicationID, contestID);
                config = StartupStaticDI.configRepository.GetConfigByConfigKeyID(useType, applicationID, contestID, configKeyID);
                if (config == null)
                {
                    config = GetConfigObject(configKeyName, applicationID, contestID);
                }
                CacheUilities.cacheManager.Set(key, config);
            }
            else
            {
                config = cacheResult;
            }
            return config;
        }

        public static string AutoGenerateRule
        {
            get
            {
                return "";
            }
        }
        public static string VirtualUploadDomain(string subdomain)
        {
             return GlobalVariables.VirtualUploadDomain(subdomain);
        }

        public static string VirtualUploadPath
        {
            get
            {
                return GlobalVariables.VirtualUploadPath;
            }
        }


        public static string UploadFolder
        {
            get { return GlobalVariables.UploadFolder; }
        }

        public static bool CanEditShowHide()
        {
            return CanEditShowHide(true);
        }
        public static bool CanEditShowHide(bool useAdminShowHide)
        {
            bool isSuperAdminMode = true;
            if (useAdminShowHide && GlobalVariables.httpContextObj.HttpContext.Session.Get<int>("AdminShowHide") != 0)
            {
                isSuperAdminMode = GlobalVariables.httpContextObj.HttpContext.Session.Get<int>("AdminShowHide") == ((int)Enums.AdminShowHide.SuperAdmin);
            }
            return SessionUtilities.IsSuperAdmin() && isSuperAdminMode;
        }
        public static bool CanEditMenuShowHide(SiteInformation site, HttpRequest request = null, bool useAdminShowHide = true)
        {
            bool isCustomerSiteOrContest = site.SiteType != Enums.SiteType.Main;
            if (!isCustomerSiteOrContest && request != null)
            {
                string _editSiteId = request.Query["editSiteId"];
                string _editCampaignId = request.Query["editCampaignId"];

                int editSiteId = int.Parse(_editSiteId ?? "0");
                int editCampaignId = int.Parse(_editCampaignId ?? "0");
                isCustomerSiteOrContest = editSiteId > 0 || editCampaignId > 0;
            }
            return CanEditShowHide(useAdminShowHide) && isCustomerSiteOrContest;
        }

        public static HtmlString ShowHide(int? flag)
        {
            flag = flag ?? 1;
            return ShowHide(flag, "");
        }
        public static HtmlString ShowHide(int? flag, string AppendedStyle)
        {
            StringBuilder Attr = new StringBuilder();
            foreach (var item in ShowHideHtmlAttribute(flag, AppendedStyle))
                Attr.AppendFormat("{0}='{1}' ", item.Key, item.Value.ToString());
            return new HtmlString(Attr.ToString());
        }
        public static Dictionary<string, object> ShowHideHtmlAttribute(int? flag, string AppendedStyle = "", string OtherAttrs = "")
        {
            bool canEditShowHide = Commons.CanEditShowHide();
            Enums.DisplayItemOption enumflag = (Enums.DisplayItemOption)flag.GetValueOrDefault(1);

            Dictionary<string, object> dic = new Dictionary<string, object>();

            string appended = AppendedStyle;
            if (!String.IsNullOrWhiteSpace(appended))
            {
                if (!appended.EndsWith(";"))
                    appended += ";";
            }
            else
            {
                appended = "";
            }

            if (enumflag == Enums.DisplayItemOption.Edit || canEditShowHide)
            {
                dic.Add("style", appended);
            }
            else if (enumflag == Enums.DisplayItemOption.Nope || enumflag == Enums.DisplayItemOption.Hide)
                dic.Add("style", String.Format("{0}display:none", appended));
            else if (enumflag == Enums.DisplayItemOption.View)
            {
                dic.Add("style", String.Format("{0}display:;border-style:none;box-shadow:none", appended));
                dic.Add("readonly", "readonly");
                dic.Add("onfocus", "this.blur();");
            }

            string pattern = "(?<left>[^=]*)[ ]*=[ ]*(['\"]*)(?<right>[^'\"]*)['\"][ ]*";
            Regex reg = new Regex(pattern, RegexOptions.Multiline);
            foreach (System.Text.RegularExpressions.Match item in reg.Matches(OtherAttrs))
            {
                dic.Add(item.Groups["left"].Value, item.Groups["right"].Value);
            }

            return dic;
        }

        public static HtmlString ContentEdittable(string keyName, string editClass, ViewContext viewcontext = null, dynamic attributes = null, int editType = 1)
        {
            return ContentEdittable(Enums.ShowHideType.Resource, 0, keyName, editClass, viewcontext, attributes, null, editType);
        }
        public static HtmlString ContentEdittable(Enums.ShowHideType contentType, int contentID, string editClass, dynamic attributes = null, int editType = 1)
        {
            return ContentEdittable(contentType, contentID, "", editClass, null, attributes, null, editType);
        }
        public static HtmlString ContentEdittable(Enums.ShowHideType contentType, int id, string keyName, string editClass, ViewContext viewcontext = null, object handlers = null, object messages = null, int editType = 1)
        {

            if (GlobalVariables.httpContextObj.HttpContext.Items["IsPreview"] != null && string.IsNullOrWhiteSpace(GlobalVariables.httpContextObj.HttpContext.Items["IsPreview"].ToString()) == false && GlobalVariables.httpContextObj.HttpContext.Items["PreviewMode"].ToString() != "readonly")
            {
                StringBuilder attrs = new StringBuilder("{rules:{");
                Dictionary<string, string> dicRules = ConvertToTokensDictionary(handlers, 1);
                Dictionary<string, string> dicMessages = ConvertToTokensDictionary(messages, 1);
                foreach (var item in dicRules)
                {
                    if (!item.Key.Equals("ContentField") && !item.Key.Equals("IDField") && !String.IsNullOrEmpty(item.Value) && !item.Key.Equals("required") && !(item.Key.Equals("multiline") && item.Value == "True"))
                        attrs.Append(String.Format("{0}:{1},", item.Key.ToLower(), (item.Value.Equals("True") || (item.Value).Equals("False")) ? item.Value.ToLower() : item.Value));
                }
                attrs.Append("},messages:{");
                foreach (var item in dicMessages)
                {
                    attrs.Append(String.Format("{0}:{1},", item.Key.ToLower(), item.Value));
                }
                attrs.Append("}}");

                if (contentType.Equals(Enums.ShowHideType.Resource))
                {
                    id = DbLanguage.GetIDByKey(keyName, viewcontext);
                }

                return new HtmlString(
                    String.Format((editType.Equals(1) ? " contenteditable=\"true\" " : " csseditable=\"true\" ")
                    + " onclick=\"Content.SelectText(this) \" "
                    + (editType.Equals(1) ? " onblur=\"Content.UpdateText(this)\"" : "")
                    + " data-id=\"{3}\" data-contentype=\"{0}\" data-contentField=\"{1}\" data-IDField=\"{2}\" ", (int)contentType, dicRules.ContainsKey("ContentField") ? dicRules["ContentField"] : "", dicRules.ContainsKey("IDField") ? dicRules["IDField"] : "", id)
                    + (String.IsNullOrWhiteSpace(editClass) ? "" : String.Format(" data-editClass='{0}' ", editClass))
                    + String.Format(" handlers='{0}' ", attrs.ToString())
                    + String.Format(" data-editSiteId='{0}' data-editCampaignId='{1}' ", GlobalVariables.SiteInformation.ApplicationID, GlobalVariables.SiteInformation.CampaignID));

            }
            else
            {
                return new HtmlString("");
            }
        }

        public static string Object_GetSize(object o)
        {
            long size = 0;
            try
            {
                using (Stream s = new MemoryStream())
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    formatter.Serialize(s, o);
                    size = s.Length;
                    return Math.Round((double)size / 1024, 3).ToString();
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static HtmlString Object_PropertiesSize(object o)
        {
            var result = @"<table class=""table-gridview"" style=""margin-bottom: 20px;background-color:white"">";
            result += string.Format(@"
                                <tr>
                                    <th>{0}</th>
                                    <th>{1}</th>
                                </tr> ", o.GetType().ToString(), "");
            result += @"        <tr>
                                    <th>Property</th>
                                    <th>Size (KB)</th>
                                </tr>";
            double total = 0;
            foreach (System.Reflection.PropertyInfo propertyInfo in o.GetType().GetProperties())
            {
                //if (propertyInfo.CanRead)
                {
                    string size = Commons.Object_GetSize(propertyInfo.GetValue(o));
                    //if (size.Equals("") || size > 0)
                    result += string.Format(@"
                                <tr>
                                    <td>{0}</td>
                                    <td> {1}</td>
                                </tr> ", propertyInfo.ToString(), size);
                    //total += size > 0 ? size : 0 ;
                }
            }
            result += string.Format(@"
                                <tr>
                                    <td>{0}</td>
                                    <td>{1}</td>
                                </tr> ", "Total", total);
            result += "</table>";
            return new HtmlString(result);
        }

        public static string IPAddress(HttpRequest Request)
        {
            return Request.IsLocal() ? "127.0.0.1" :
                GlobalVariables.httpContextObj.HttpContext.Features.Get<IHttpConnectionFeature>()?.RemoteIpAddress.ToString();
        }

        public static List<Page> PagePermission
        {
            get
            {
                string cachename = "PagePermission";
                var cached = CacheUilities.cacheManager.Get<List<Page>>(cachename);
                if (cached != null)
                    return cached;
                else
                {
                    var lstPage = new List<Page>();
                    CacheUilities.cacheManager.Set(cachename, lstPage);
                    return lstPage;
                }
            }
            set
            {
                string cachename = "PagePermission";
                CacheUilities.cacheManager.Remove(cachename);
                CacheUilities.cacheManager.Set(cachename, value);
            }
        }

        public static bool IsURL(string path, bool isStrict = false)
        {
            if (string.IsNullOrEmpty(path))
                return false;
            return path.ToLower().StartsWith("http://") || path.ToLower().StartsWith("https://")
                || (!isStrict && path.ToLower().StartsWith("//"));
        }
        public static string GetYoutubeID(string url)
        {
            if (string.IsNullOrEmpty(url))
                return "";

            Regex YoutubeVideoRegex = new Regex(@"vi/([A-Za-z0-9_\-]+)/([A-Za-z0-9_\-]+)\.jpg$", RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Match youtubeMatch = YoutubeVideoRegex.Match(url);
            string id = string.Empty;

            if (youtubeMatch.Success)
            {
                id = youtubeMatch.Groups[1].Value;
            }
            return id;
        }
        public static string GetVimeoImageID(string url)
        {
            //https://i.vimeocdn.com/video/273946728_1920x960.jpg?r=pad
            if (string.IsNullOrEmpty(url))
                return "";

            Regex YoutubeVideoRegex = new Regex(@"video/([0-9]+)(_|\.jpg)", RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Match youtubeMatch = YoutubeVideoRegex.Match(url);
            string id = string.Empty;

            if (youtubeMatch.Success)
            {
                id = youtubeMatch.Groups[1].Value;
            }
            return id;
        }
        public static bool IsYoutubeImage(string path)
        {
            if (string.IsNullOrEmpty(path))
                return false;

            return (path.IndexOf("img.youtube.com") > -1 || path.IndexOf(".ytimg.com") > -1);
        }
        public static bool IsVimeoImage(string path)
        {
            if (string.IsNullOrEmpty(path))
                return false;

            return (path.IndexOf("i.vimeocdn.com") > -1 || path.IndexOf("i.vimeocdn.com") > -1);
        }
        public static string GetFullName(string path)
        {
            if (string.IsNullOrEmpty(path)
                || path.StartsWith("http:", StringComparison.CurrentCultureIgnoreCase)
                || path.StartsWith("//", StringComparison.CurrentCultureIgnoreCase)
                || path.StartsWith("https:", StringComparison.CurrentCultureIgnoreCase))
                return path;

            string mediaserver = Commons.VirtualUploadPath;

            if (!mediaserver.EndsWith("/"))
                mediaserver += "/";

            return String.Format(@"{0}{2}/{1}",
                                      mediaserver,
                                      path.Replace("\\", "/"), Commons.UploadFolder);
        }

        public static string ImageURL(string path, string pre = "", string imgType = "d", bool foreHttp = false)
        {

            string newpath = path;
            string result = "";
            pre = pre.Replace("o", "");

            if (!string.IsNullOrEmpty(path))
            {
                if (!string.IsNullOrEmpty(pre))
                {
                    if (IsURL(path))
                    {
                        if (IsYoutubeImage(path))
                        {
                            string id = GetYoutubeID(path);
                            newpath = String.Format("https://i1.ytimg.com/vi/{0}/{1}default.jpg",
                                                id,
                                                pre.Replace("f", "m").Replace("d", "maxres").Replace("l", "hq").Replace("m", "mq").Replace("t", "mq").Replace("s", ""));

                        }
                        else if (IsVimeoImage(path))
                        {
                            string id = GetVimeoImageID(path);
                            newpath = String.Format("https://i.vimeocdn.com/video/{0}_{1}.jpg",
                                                id,
                                                pre.Replace("d", "").Replace("l", "480x360").Replace("m", "320x180").Replace("t", "160x120").Replace("s", "120x90"));
                        }
                        else if (path.Contains("s3-p.animoto.com"))
                        {
                            newpath = path;
                        }
                        else
                        {
                            string name = Path.GetFileName(path);
                            newpath = path.Replace(name, String.Format("{0}_{1}", pre, name));
                        }
                    }
                    else
                    {
                        var VirtualFolderPath = GlobalVariables.VirtualFolderPath;
                        newpath = String.Format(@"{0}\{1}_{2}",
                                            Path.GetDirectoryName(path),
                                            pre,
                                            Path.GetFileName(path));

                    }
                }
                result = Commons.GetFullName(newpath);
            }
            else
                result = Commons.DefaultImage(pre, imgType);

            //force Http
            if (foreHttp && !result.StartsWith("http"))
                result = "http:" + result;

            return result;
        }

        public static string DefaultImage(string pre = "", string imgType = "d")
        {
            //type:d default, u: user
            if (imgType == "u")
            {
                return "https://cdn2.hubspot.net/hubfs/447141/Images_(just_about_all_images)/icons/default.jpg";
            }
            else
            {
                string Media_DefaultImage = Commons.Config["Media_DefaultImage"];
                string fileName = Path.GetFileName(Media_DefaultImage);
                if (Media_DefaultImage.ToLower().StartsWith("https://") || Media_DefaultImage.ToLower().StartsWith("http://"))
                {
                    return Media_DefaultImage.Replace(fileName, string.Format("{0}_{1}", pre, fileName));
                }
                else
                {
                    string mediaserver = Commons.VirtualUploadPath;
                    if (!mediaserver.EndsWith("/"))
                        mediaserver += "/";

                    string defaultimage = String.Format(@"{0}images/{1}_{2}",
                                                mediaserver,
                                                pre,
                                                imgType == "a" ? "audioDefault.png" : Commons.Config["Media_DefaultImage"]);
                    return defaultimage;
                }

                //return "http://il3.picdn.net/shutterstock/videos/925060/thumb/5.jpg";
            }
        }

        public static dynamic ExtraSetting()
        {
            var quizExtraSetting = Commons.Config["Quiz_ExtraSettings"];
            return JsonConvert.DeserializeObject<dynamic>(quizExtraSetting);
        }

        public static string Gravatar2Url(string name, string size = "medium", string queryString = null)
        {
            string url = "";
            try
            {
                var response = new WebClient().DownloadString(string.Format("https://randomuser.me/api/?seed={0}", name));
                var data = JsonConvert.DeserializeObject<Dictionary<string, object>>(response);
                dynamic results = data["results"];
                string strSize = (size == "l" ? "large" : (size == "s" ? "thumbnail" : "medium"));
                url = results[0]["picture"][strSize];
            }
            catch (Exception ex)
            {
                url = Commons.DefaultImage();
            }
            return url;
        }


        public static string RemoveQueryStringByKey(string url, params string[] keys)
        {
            if (!String.IsNullOrWhiteSpace(url))
            {
                foreach (var item in keys)
                {
                    var uri = new Uri(url);
                    var newQueryString = HttpUtility.ParseQueryString(uri.Query);
                    newQueryString.Remove(item);
                    string pagePathWithoutQueryString = uri.GetLeftPart(UriPartial.Path);
                    url = newQueryString.Count > 0 ? String.Format("{0}?{1}", pagePathWithoutQueryString, newQueryString) : pagePathWithoutQueryString;
                }
            }
            return url;
        }

        public static string SharingURL(SiteInformation siteinfo = null, HttpRequest Request = null, string pageShareURL = "")
        {
            if (siteinfo == null)
            {
                siteinfo = GlobalVariables.SiteInformation;
            }
            if (Request == null)
            {
                Request = GlobalVariables.httpContextObj.HttpContext.Request;
            }
            string sharedURL = string.IsNullOrEmpty(pageShareURL) ? Commons.Config["SharingURL"] : pageShareURL;

            string strPathAndQuery = Request.GetRawUrl();
            var absoluteUri = Request.GetAbsoluteUriSelf();
            string strUrl = absoluteUri.ToString().Replace(strPathAndQuery, "");

            if (Request.PathBase.ToString().StartsWith("/"))
            {
                strUrl += Request.PathBase.ToString();
            }
            else
            {
                strUrl += "/" + Request.ToString();
            }
            strUrl += "/" + siteinfo.ContextName;
            string pageURL = Commons.RemoveQueryStringByKey(Request.GetAbsoluteUriSelf().ToString(), "showshare");

            if (siteinfo.SiteType == Enums.SiteType.Campaign)
            {
                if (siteinfo.ContextName.ToLower() == "rantchamp")
                {
                    strUrl += "/contest/match/" + siteinfo.CampaignID;
                }
                else
                {
                    string homeURL = Commons.Config["HomePage"];
                    if (!string.IsNullOrEmpty(homeURL))
                    {
                        strUrl += homeURL.Replace("~", ""); ;
                    }
                    else
                    {
                        strUrl += "/contest/details/" + siteinfo.CampaignID;
                    }
                }
            }
            else
            {
                strUrl += "/home/index";
            }
            sharedURL = sharedURL.Replace("%CurrentPage%", pageURL).Replace("%CurrentSite%", strUrl);
            return sharedURL;
        }

        private static readonly Random Random = new Random();
        public static string PasswordGenerator(int length, bool includeSpecialCharacters, bool isCodeVerify = false, string specialCharacters = @"!#$%&'()*+,-./:;<=>?@[\]_")
        {
            int seed = Random.Next(1, int.MaxValue);
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            if (isCodeVerify)
            {
                allowedChars = "0123456789";
            }
            var chars = new char[length];
            var rd = new Random(seed);
            for (var i = 0; i < length; i++)
            {
                // If we are to use special characters
                if (includeSpecialCharacters && i % Random.Next(3, length) == 0)
                {
                    chars[i] = specialCharacters[rd.Next(0, specialCharacters.Length)];
                }
                else
                {
                    chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
                }
            }
            return new string(chars);
        }

        public static bool CanUpdateShare(SiteInformation site, AccountDetail detail)
        {
            return site.SiteType == Enums.SiteType.Customer && detail.ApplicationID == site.ApplicationID ||
                site.SiteType == Enums.SiteType.Campaign && detail.ContestID == site.CampaignID;
        }
    }
}
