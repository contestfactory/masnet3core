﻿using Microsoft.AspNetCore.Html;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace Masnet3.Common.Helper
{
    public static class MvcHtmlString
    {
        public static string RemoveHtmlTag(this string source)
        {
            if (string.IsNullOrWhiteSpace(source))
            {
                return string.Empty;
            }

            return Regex.Replace(source, "<.*?>", string.Empty);
        }
        public static string RemoveStartAndEndWith(this string value)
        {
            return RemoveStartAndEndWith(value, new string[] { " ", "," });
        }
        public static string RemoveStartAndEndWith(this string value, string removedValue)
        {
            if (string.IsNullOrEmpty(value) || string.IsNullOrEmpty(removedValue))
            {
                return value;
            }

            return RemoveStartAndEndWith(value, new string[] { removedValue });
        }
        public static string RemoveStartAndEndWith(this string value, string[] removedValues = null)
        {
            if (string.IsNullOrEmpty(value) || removedValues == null || removedValues.Length == 0)
            {
                return value;
            }

            bool firstFound = false, lastFound = false;
            string firstString = "", lastString = "";

            do
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    return value;
                }

                firstFound = lastFound = false;
                firstString = lastString = "";

                if (removedValues.Contains(" "))
                {
                    value = value.Trim();
                }

                for (int index = 0; index < removedValues.Length; index++)
                {
                    if (!firstFound && value.StartsWith(removedValues[index]))
                    {
                        firstFound = true;
                        firstString = removedValues[index];
                    }

                    if (!lastFound && value.EndsWith(removedValues[index]))
                    {
                        lastFound = true;
                        lastString = removedValues[index];
                    }
                }

                if (firstFound)
                {
                    value = value.Remove(0, firstString.Length);
                }

                if (lastFound)
                {
                    value = value.Remove(value.Length - lastString.Length);
                }
            } while (firstFound == true || lastFound == true);

            return value;
        }

        public static HtmlString HandleMissing(string pre, string type = "d")
        {
            return new HtmlString(String.Format("onerror=\"this.onerror=null;this.src='{0}'\"", DefaultImage(pre, type)));
        }
        public static string DefaultImage(string pre = "", string imgType = "d")
        {
            //type:d default, u: user
            if (imgType == "u")
            {
                return "https://cdn2.hubspot.net/hubfs/447141/Images_(just_about_all_images)/icons/default.jpg";
            }
            else
            {
                string Media_DefaultImage = Commons.Config["Media_DefaultImage"];
                string fileName = Path.GetFileName(Media_DefaultImage);
                if (Media_DefaultImage.ToLower().StartsWith("https://") || Media_DefaultImage.ToLower().StartsWith("http://"))
                {
                    return Media_DefaultImage.Replace(fileName, string.Format("{0}_{1}", pre, fileName));
                }
                else
                {
                    string mediaserver = Commons.VirtualUploadPath;
                    if (!mediaserver.EndsWith("/"))
                        mediaserver += "/";

                    string defaultimage = String.Format(@"{0}images/{1}_{2}",
                                                mediaserver,
                                                pre,
                                                imgType == "a" ? "audioDefault.png" : Commons.Config["Media_DefaultImage"]);
                    return defaultimage;
                }
                //return "http://il3.picdn.net/shutterstock/videos/925060/thumb/5.jpg";
            }
        }
    }
}