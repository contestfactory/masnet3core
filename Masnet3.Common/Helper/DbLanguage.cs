﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Common.Statups;
using Masnet3.Common.Utilities;
using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Masnet3.Common.Helper
{
    public class DbLanguage
    {

        public static string CurrentDir
        {
            get
            {
                if (GlobalVariables.SiteInformation.SiteType == Enums.SiteType.Campaign || GlobalVariables.SiteInformation.SiteType == Enums.SiteType.Main)
                {
                    return GlobalVariables.SiteInformation.LanguageID == (int)Enums.Language.Farsi ? "rtl" : "ltr";
                }
                else
                {
                    return SessionUtilities.CurrentLanguageID == (int)Enums.Language.Farsi ? "rtl" : "ltr";
                }
            }
            set
            {
                GlobalVariables.httpContextObj.HttpContext.Session.SetStringObject(SessionUtilities.AreaName() + "CurrentDir", value);
            }
        }

        static int GetCaheIndex(int LanguageID)
        {
            if (LanguageID.Equals((int)Enums.Language.English))
                return 0;
            else if (LanguageID.Equals((int)Enums.Language.French))
                return 1;
            else if (LanguageID.Equals((int)Enums.Language.Farsi))
                return 2;
            return 1;
        }

        public static string GetText(string Key, int KeyType = 0)
        {
            return GetTextByCache(Key.Trim(), GlobalVariables.PageName()) + "";
        }

        public static string GetText(string Key, string pagename, int intPage = 0)
        {
            return GetTextByCache(Key.Trim(), pagename) + "";
        }

        public static string GetText(string Key, string partialView, bool isPartial = false)
        {
            if (isPartial)
            {
                return GetTextByCache(Key.Trim(), partialView) + "";
            }
            else
            {
                return GetTextByCache(Key.Trim(), SessionUtilities.PageName()) + "";
            }
        }

        public static HtmlString GetHelpText(string key = null, bool IsShowIcon = true, ViewContext ViewContext = null, string helptext = null, int KeyType = 0)
        {
            string strHelp = string.Empty;
            if (!string.IsNullOrWhiteSpace(helptext) || !string.IsNullOrWhiteSpace(key))
            {
                if (string.IsNullOrWhiteSpace(helptext))
                {
                    strHelp = GetTextByCache(key.Trim(), GlobalVariables.PageName(ViewContext), true);
                }
                else
                {
                    strHelp = helptext;
                }

                if (!String.IsNullOrWhiteSpace(strHelp) && IsShowIcon)
                {
                    strHelp = DrawHelpIcon(key, 600, 350, "", "", 1, strHelp);
                }

            }
            return new HtmlString(strHelp);
        }

        public static string GetTextByCache(string Key, string CachedName, bool isHelpText = false, bool isGetID = false, int KeyType = 0)
        {
            //Init data
            //MASNET3.DebugClock DC3 = new MASNET3.DebugClock();
            //DC3.Start("Init Variables");
            var showLanguageKey = (string)GlobalVariables.httpContextObj.HttpContext.Session.Get<string>("showlanguages") == "1";
            bool isPrivateCache = Commons.IsPrivateCache();
            if (showLanguageKey) return "[" + Key + "]";
            //var count = 1;

            SiteInformation siteInformation = GlobalVariables.SiteInformation;
            int contestID = siteInformation.CampaignID;
            int applicationID = siteInformation.ApplicationID;
            int siteContestID = siteInformation.SiteContestID;
            Enums.SiteType siteType = siteInformation.SiteType;
            int templateID = siteInformation.TemplateID;

            int cacheIndex = GetCaheIndex(SessionUtilities.CurrentLanguageID);
            string globalCacheKey1 = String.Format("r0/{0}", CachedName);
            string globalCacheKey2 = "r0/0";
            string templateCacheKey1 = String.Format("r4_{0}/{1}", templateID, CachedName);
            string templateCacheKey2 = String.Format("r4_{0}/0", templateID);
            string appCacheKey1 = "", appCacheKey2 = "", siteContestCacheKey1 = "", siteContestCacheKey2 = "";
            string value = "";
            Dictionary<string, List<CacheResource>> resources;
            Dictionary<string, string> contestResource = null;

            string cacheKeyPattern = "r_userType{0}_{1}/{2}/{3}";
            //string useType = siteType == Enums.SiteType.Campaign ? "3" : "2";
            string useType = "";
            if (applicationID > 0)
            {
                useType = Commons.GetUseType(applicationID, siteContestID).ToString();
                siteType = siteContestID > 0 ? Enums.SiteType.SiteContest : siteType;
            }
            else
            {
                useType = Commons.GetUseType(applicationID, contestID).ToString();
            }

            string cacheKey = string.Format(cacheKeyPattern, siteType, contestID, CachedName, Key);

            if (isPrivateCache && StartupCommon.ContestResources.ContainsKey(cacheKey))
            {
                contestResource = StartupCommon.ContestResources[cacheKey];
                value = contestResource[Key];
            }
            else
            {
                if (isPrivateCache)
                {
                    contestResource = new Dictionary<string, string>();
                    StartupCommon.ContestResources.Add(cacheKey, contestResource);
                }

                CacheItem cache;

                CacheItem[] arr = new CacheItem[6];
                switch (siteType)
                {
                    case Enums.SiteType.Campaign:
                        arr = new CacheItem[6];
                        appCacheKey1 += string.Format("r_userType3_{0}/{1}", contestID, CachedName);
                        appCacheKey2 += string.Format("r_userType3_{0}/0", contestID);
                        arr[0] = new CacheItem() { UseType = 3, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = appCacheKey2 };
                        arr[1] = new CacheItem() { UseType = 3, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = appCacheKey1 };
                        arr[2] = new CacheItem() { UseType = 4, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = templateCacheKey2 };
                        arr[3] = new CacheItem() { UseType = 4, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = templateCacheKey1 };
                        arr[4] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = globalCacheKey2 };
                        arr[5] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = globalCacheKey1 };

                        break;
                    case Enums.SiteType.Customer:
                        arr = new CacheItem[4];
                        appCacheKey1 += string.Format("r_userType2_{0}/{1}", applicationID, CachedName);
                        appCacheKey2 += string.Format("r_userType2_{0}/0", applicationID);
                        arr[0] = new CacheItem() { UseType = 2, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = appCacheKey2 };
                        arr[1] = new CacheItem() { UseType = 2, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = appCacheKey1 };
                        arr[2] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = globalCacheKey2 };
                        arr[3] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = globalCacheKey1 };

                        break;
                    case Enums.SiteType.SiteContest:
                        siteContestCacheKey1 += string.Format("r_userType5_{0}/{1}", siteContestID, CachedName);
                        siteContestCacheKey2 += string.Format("r_userType5_{0}/0", siteContestID);
                        appCacheKey1 += string.Format("r_userType2_{0}/{1}", applicationID, CachedName);
                        appCacheKey2 += string.Format("r_userType2_{0}/0", applicationID);

                        arr = new CacheItem[6];
                        arr[0] = new CacheItem() { UseType = 5, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = siteContestCacheKey2 };
                        arr[1] = new CacheItem() { UseType = 5, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = siteContestCacheKey1 };
                        arr[2] = new CacheItem() { UseType = 2, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = appCacheKey2 };
                        arr[3] = new CacheItem() { UseType = 2, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = appCacheKey1 };
                        arr[4] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = globalCacheKey2 };
                        arr[5] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = globalCacheKey1 };

                        break;
                    default:
                        arr = new CacheItem[4];
                        appCacheKey1 += string.Format("r_userType2_{0}/{1}", applicationID, CachedName);
                        appCacheKey2 += string.Format("r_userType2_{0}/0", applicationID);
                        arr[0] = new CacheItem() { UseType = 2, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = appCacheKey2 };
                        arr[1] = new CacheItem() { UseType = 2, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = appCacheKey1 };
                        arr[2] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 0, CacheName = globalCacheKey2 };
                        arr[3] = new CacheItem() { UseType = 1, ResourceKey = Key, PageName = CachedName, CacheType = 1, CacheName = globalCacheKey1 };
                        break;
                }
                //Update caches
                //DC3.Restart("Update caches");
                for (var j = 0; j < arr.Length; j++)
                {
                    cache = arr[j];
                    cache.LanguageID = SessionUtilities.CurrentLanguageID;
                    cache.CacheIndex = cacheIndex;
                    UpdateCache(cache, true);
                }

                //Get values
                //DC3.Restart("Get values");
                for (var j = 0; j < arr.Length; j++)
                {
                    cache = arr[j];
                    resources = CacheUilities.cacheManager.Get<Dictionary<string, List<CacheResource>>>(cache.CacheName.ToLower());
                    if (Contains(resources, cache, out value, isHelpText, isGetID))
                    {
                        break;
                    }
                }
                if (isPrivateCache)
                {
                    if (!contestResource.ContainsKey(Key))
                    {
                        contestResource.Add(Key, null);
                    }
                    contestResource[Key] = value;
                }
            }

            return value;

        }

        static void UpdateCache(CacheItem cache, bool useSiteContest = false)
        {
            SiteInformation siteInformation = GlobalVariables.SiteInformation;
            Dictionary<string, List<CacheResource>> resources = CacheUilities.cacheManager.Get<Dictionary<string, List<CacheResource>>>(cache.CacheName.ToLower());
            List<CacheResource> keyResources;
            bool update = false;
            bool addNew = false;

            if (resources == null)
            {
                resources = new Dictionary<string, List<CacheResource>>();
                update = true;
                addNew = true;
            }
            else if (!resources.Count.Equals(0) && resources.First().Value[cache.CacheIndex] == null)
            {
                update = true;
            }
            if (update)
            {
                int contestId = cache.UseType == (int)Enums.UseType.SiteContest ? siteInformation.SiteContestID : siteInformation.CampaignID;
                string cacheFilter = CacheUilities.GetCacheFilter(cache.UseType, "ApplicationID", "ContestID", "TemplateID", siteInformation.Application.ApplicationID, contestId, 0, "", true, cache.LanguageID, useSiteContest: useSiteContest);
                var source = StartupStaticDI.resourceRepository.GetResourceFromCache(cache.CacheType, cacheFilter, siteInformation.Application.ApplicationID, contestId, cache.PageName, (int)Commons.GetUseType(siteInformation.ApplicationID, contestId, null));
                foreach (var item in source)
                {
                    if (addNew)
                    {
                        keyResources = new List<CacheResource>() { null, null, null };
                        keyResources[cache.CacheIndex] = new CacheResource() { Value = item.Value, Help = item.HelpText, ID = item.ID };
                        if (resources.ContainsKey(item.ResourceKey) == false)
                        {
                            resources.Add(item.ResourceKey, keyResources);
                        }
                    }
                    else
                    {
                        keyResources = null;
                        try { keyResources = resources[item.ResourceKey]; } catch { }
                        if (keyResources != null)
                        {
                            keyResources[cache.CacheIndex] = new CacheResource() { Value = item.Value, Help = item.HelpText, ID = item.ID, };
                        }
                    }
                }
                CacheUilities.cacheManager.Set(cache.CacheName.ToLower(), resources);
            }
        }

        static bool Contains(Dictionary<string, List<CacheResource>> resources, CacheItem cache, out string value, bool isHelpText = false, bool isGetID = false)
        {
            bool result = true;
            try
            {
                value = !isHelpText ? resources[cache.ResourceKey][cache.CacheIndex].Value : resources[cache.ResourceKey][cache.CacheIndex].Help;
                if (isGetID)
                {
                    value = resources[cache.ResourceKey][cache.CacheIndex].ID.ToString();
                }
            }
            catch
            {
                value = cache.ResourceKey;
                if (isGetID)
                {
                    value = "0";
                }
                result = false;
            }
            return result;
        }

        public static int GetIDByKey(string Key, ViewContext viewcontext = null)
        {
            int ResourceID = 0;
            string result = "";
            if (!Commons.IsPreview)
                return 0;
            if (viewcontext == null)
            {
                result = GetTextByCache(Key.Trim(), GlobalVariables.PageName(), false, true);
            }
            else
            {
                result = GetTextByCache(Key, viewcontext.RouteData.GetRequiredString("Controller") + '/' + viewcontext.RouteData.GetRequiredString("Action"), false, true);
            }

            if (int.TryParse(result, out ResourceID) == false)
            {
                return 0;
            }
            else
            {
                return ResourceID;
            }

        }

        // Icon
        public static string DrawHelpIcon(string resourceKey = "", int width = 600, int height = 350, string objectId = "", string style = "", int ShowType = 0, string HelpText = "")
        {
            StringBuilder HtmlHelpIcon = new StringBuilder("");
            string divWidth = "auto";
            if (String.IsNullOrWhiteSpace(HelpText))
            {
                HelpText = DbLanguage.GetText(resourceKey).Trim();
            }
            if (string.IsNullOrWhiteSpace(resourceKey))
            {
                resourceKey = Guid.NewGuid().ToString().Replace("-", "_");
            }

            if (!String.IsNullOrWhiteSpace(HelpText))
            {
                HelpText = HelpText.Trim();
                if (HelpText.Length >= 50)
                {
                    divWidth = "400";
                }
                HtmlHelpIcon.Append("<span style='" + style + "'> ");
                if (ShowType == 0)
                {
                    HtmlHelpIcon.Append("<span onclick='ShowHelp(\"dvHelpIcon_" + resourceKey + "\"," + width + "," + height + ");' id='lnkHelpIcon_" + resourceKey + "' style='cursor:pointer' ><img src='/Content/img/helpicon.png' border='0' /></span> ");
                    HtmlHelpIcon.Append("<div id='dvHelpIcon_" + resourceKey + "'   style='display:none' >" + HelpText + "</div>");
                }
                else
                {
                    HtmlHelpIcon.Append("<span  id='lnkHelpIcon_" + resourceKey + "' style='cursor:pointer' onmouseover='ShowHelp(\"dvHelpIcon_" + resourceKey + "\"," + width + "," + height + ",1,\"lnkHelpIcon_" + resourceKey + "\",this);' onmouseout='HideHelp(\"dvHelpIcon_" + resourceKey + "\");' ><img src='/Content/img/helpicon.png' border='0' /></span> ");
                    HtmlHelpIcon.Append("<div id='dvHelpIcon_" + resourceKey + "'   style='display:none;width: " + divWidth + "px;word-wrap: break-word; background-color:lightyellow; border: 1px solid rgb(204, 204, 204); padding: 5px; position: absolute; z-index: " + DateTime.Now.Ticks + ";color:black;font-size:13px' >" + HelpText + "</div>");
                }
                HtmlHelpIcon.Append("</span>");
            }
            return HtmlHelpIcon.ToString();
        }

        public static string GetShareText(MASNET.DataAccess.Action action)
        {
            if (action.SourceID == (int)Enums.PageID.HomePage)
                return GetText("Share_ShareSite");
            else if (action.ActionID == (int)Enums.Action.Facebook)
                return GetText("Share_ThisEntry");
            else if (action.ActionID == (int)Enums.Action.Twitter)
                return GetText("Share_ThisEntry");
            else if (action.ActionID == (int)Enums.Action.EmailShare)
                return GetText("Share_ThisEntry");
            else if (action.ActionID == (int)Enums.Action.Embed)
                return GetText("Share_GetEmbed");
            else if (action.ActionID == (int)Enums.Action.Link)
                return GetText("Share_GetLink");
            else
                return GetText("Share_DefaultTooltip");
        }

        public static string CssLeft
        {
            get
            {
                if (CurrentDir == "ltr")
                    return "left";
                else
                    return "right";
            }
        }

    }
}
