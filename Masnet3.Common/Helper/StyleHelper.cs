﻿using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common.Utilities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Helper
{
    public class StyleHelper
    {
        public static string CurrentDir
        {
            get
            {
                if (GlobalVariables.SiteInformation.SiteType == Enums.SiteType.Campaign || GlobalVariables.SiteInformation.SiteType == Enums.SiteType.Main)
                {
                    return GlobalVariables.SiteInformation.LanguageID == (int)Enums.Language.Farsi ? "rtl" : "ltr";
                }
                else
                {
                    return SessionUtilities.CurrentLanguageID == (int)Enums.Language.Farsi ? "rtl" : "ltr";
                }
            }
            set
            {
                GlobalVariables.httpContextObj.HttpContext.Session.SetStringObject(Commons.AreaName() + "CurrentDir", value);
            }
        }

        public static string ReverseForRTL(string CssStr)
        {
            if (CurrentDir == "rtl")
            {
                return CssStr.Replace("left", "tmpRight").Replace("right", "left").Replace("tmpRight", "right");
            }
            else
                return CssStr;

        }
    }
}
