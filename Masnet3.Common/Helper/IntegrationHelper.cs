﻿using MASNET.DataAccess;
using Masnet3.Common.Statups;
using Masnet3.Common.Utilities;
using Masnet3.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Masnet3.Common.Helper
{
    public class IntegrationHelper
    {
        public static string IntegrateURL
        {
            get
            {
                string integrateURL = Commons.GetConfigKeyFromAppsettings("IntegrateURL");
                if (!string.IsNullOrWhiteSpace(integrateURL))
                {
                    integrateURL = integrateURL.Trim().Replace(@"\", @"/");
                    if (!integrateURL.EndsWith(@"/"))
                    {
                        integrateURL += @"/";
                    }
                }

                return integrateURL;
            }
        }

        public static string GetToken(Registration registration = null)
        {
            string token = string.Empty;

            if (SessionUtilities.IsLoggedIn())
            {
                if (registration == null)
                {
                    registration = new Registration();
                    registration = StartupStaticDI.registrationRepository.GetRegistrationByUserId(SessionUtilities.CurrentUser.UserID.ToString());
                }
                string password = AESEncryption.Decrypt(registration.Password, registration.ValidationKey, registration.PasswordSalt);
                string username = registration.Username;

                string integrateGetToken = Commons.GetConfigKeyFromAppsettings("IntegrateGetToken");
                if (!string.IsNullOrWhiteSpace(integrateGetToken) && !string.IsNullOrWhiteSpace(registration.MongoID))
                {
                    integrateGetToken = string.Format(integrateGetToken, username, password).Replace(@"&amp;", @"&");

                    integrateGetToken = integrateGetToken.Replace("%MongoID%", registration.MongoID);
                    integrateGetToken = integrateGetToken.Replace("%SecretKey%", Commons.GetConfigKeyFromAppsettings("SecretKey"));

                    IntegrateResponse integrateResponse = GetIntegrateResponse(IntegrateURL + integrateGetToken);

                    if (integrateResponse.Result.ToLower() == "ok")
                    {
                        token = ((dynamic)integrateResponse.Data).Token;

                        SessionUtilities.CurrentUser.IntergratedToken = token;
                    }
                }
            }

            return token;
        }

        public static IntegrateResponse GetIntegrateResponse(string url)
        {
            IntegrateResponse response = new IntegrateResponse() { Data = "", Message = "", Result = "" };
            url = url.Replace("%Token%", SessionUtilities.CurrentUser.IntergratedToken);
            using (WebClient client = new WebClient())
            {
                try
                {
                    string content = client.DownloadString(url);
                    response = (IntegrateResponse)JsonConvert.DeserializeObject(content, response.GetType());
                }
                catch (Exception ex)
                {
                    response.Result = "error";
                    response.Message = ex.Message.ToString();
                }
            }
            return response;
        }
    }
}
