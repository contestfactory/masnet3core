﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MASNET.DataAccess.Model.Settings;

namespace Masnet3.Common.Helper
{
    public class SettingHelper
    {
        public static AppConfiguration GetAppConfiguration()
        {
            var appConfig = new AppConfiguration();
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");

            var config = builder.Build();
            config.GetSection("AppConfiguration").Bind(appConfig);
            return appConfig;

        }
    }
}
