﻿using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Routing;
using System;
using Microsoft.AspNetCore.WebUtilities;
using System.Text;
using System.Net;
using Microsoft.AspNetCore.Mvc;

namespace Masnet3.Common.Helper
{
    public static class HtmlExtension
    {
        public static string SZAction(this IUrlHelper url, string action, string controller)
        {
            return SZAction(url, action, controller, null);
        }
        public static string SZAction(this IUrlHelper url, string action, string controller, object routeValue)
        {

            StringBuilder sb = new StringBuilder();

            string apppath = url.ActionContext.HttpContext.Request.PathBase.Value.ToLower();
            if (apppath != "/")
            {
                sb.Append(apppath);
                sb.Append("/");
            }
            else
            {
                sb.Append("/");
            }

            if (url.ActionContext.RouteData.Values["iframe"] != null)
            {
                sb.Append("iframe_");
            }

            sb.Append(url.ActionContext.RouteData.Values["SiteName"]);
            sb.Append("/");
            sb.Append(controller);
            sb.Append("/");
            sb.Append(action);

            var param = new RouteValueDictionary(routeValue);
            if (param.ContainsKey("id"))
            {
                sb.Append("/");
                sb.Append(param["id"]);
            }

            bool firstparam = true;
            string key = "";
            foreach (string tmpkey in param.Keys)
            {
                key = tmpkey.ToLower();
                if (key != "id" && param[key] != null)
                {
                    if (firstparam)
                    {
                        firstparam = false;
                        sb.Append("?");
                    }
                    else
                    {
                        sb.Append("&");
                    }

                    sb.Append(key);
                    sb.Append("=");
                    sb.Append(WebUtility.UrlEncode(param[key].ToString()));
                }
            }

            return sb.ToString();
        }
    }
}
