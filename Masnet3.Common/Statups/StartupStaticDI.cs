﻿using Masnet3.Business.Repository;
using Masnet3.Business.Repository.Applications;
using Masnet3.Business.Repository.Configs;
using Masnet3.Business.Repository.Contests;
using Masnet3.Business.Repository.Customer;
using Masnet3.Business.Repository.CustomRouteSettings;
using Masnet3.Business.Repository.CustomViews;
using Masnet3.Business.Repository.Global;
using Masnet3.Business.Repository.Languages;
using Masnet3.Business.Repository.Layout;
using Masnet3.Business.Repository.Periods;
using Masnet3.Business.Repository.Resources;
using Masnet3.Business.Repository.Rules;
using Masnet3.Business.Repository.Users;
using Masnet3.Common.Caching;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Statups
{
    public static class StartupStaticDI
    {
        //DI
        static public IRepositoryBasic repositoryBasic { get; set; }
        static public IUserRepository userRepository { get; set; }
        static public IContestRepository contestRepository { get; set; }
        static public ILanguageRepostirory languageRepostirory { get; set; }
        static public IApplicationLanguageRepository applicationLanguageRepository { get; set; }
        static public IApplicationRepository applicationRepository { get; set; }
        static public IThemeRepository themeRepository { get; set; }
        static public ICustomRouteSettingRepository customRouteSettingRepository { get; set; }
        static public IResourceRepository resourceRepository { get; set; }
        static public ICountryRepository countryRepository { get; set; }
        static public IRuleRepository ruleRepository { get; set; }
        static public ICustomViewRepository customViewRepository { get; set; }
        static public IConfigRepository configRepository { get; set; }
        static public IPageRepository pageRepository { get; set; }
        static public IPeriodRepository periodRepository { get; set; }
        static public IRegistrationRepository registrationRepository { get; set; }

    }
}
