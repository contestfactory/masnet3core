﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository.Applications;
using Masnet3.Business.Repository.Contests;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Masnet3.Common.Statups
{
    public class StartupCommon
    {
        public static Dictionary<string, MinifiedContest> DicContests = new Dictionary<string, MinifiedContest>();
        public static Dictionary<string, MinifiedApplication> DicApplications = new Dictionary<string, MinifiedApplication>();
        public static Dictionary<string, Dictionary<string, string>> ContestResources = new Dictionary<string, Dictionary<string, string>>();
        public static void RebuildAppAndContestDictionary()
        {
            DicApplications.Clear();
            DicContests.Clear();

            int[] userTypes = new[] { (int)Enums.UseType.Global, (int)Enums.UseType.Site };
            var applicationLanguages = StartupStaticDI.applicationLanguageRepository.GetApplicationLanguagesByUserTypes(userTypes).ToList();
            applicationLanguages = applicationLanguages ?? new List<ApplicationLanguage>();

            var lstApp = StartupStaticDI.applicationRepository.GetMinifiedApplicationsIsTemplateNull().ToList();
            foreach (var ma in lstApp)
            {
                if (!DicApplications.Keys.Contains(ma.ApplicationName.ToLower()))
                {
                    ma.ApplicationLanguages = applicationLanguages.Where(a => a.ApplicationID == ma.ApplicationID).ToList();
                    DicApplications.Add(ma.ApplicationName.ToLower(), ma);
                }
            }
            var lstContests = StartupStaticDI.contestRepository.GetMinifiedContestsByCreatedFrom(2).ToList();
            foreach (var mc in lstContests)
            {
                if (!DicContests.Keys.Contains(mc.ContestDomain.ToLower()))
                {
                    DicContests.Add(mc.ContestDomain.ToLower(), mc);
                }
            }
        }

    }
}
