﻿using Masnet3.Common.Helper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Builder;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Business.Repository.CustomRouteSettings;
using Microsoft.Extensions.Configuration;
using MASNET.DataAccess.Model.Settings;

namespace Masnet3.Common.Statups
{
    public static class StartupRouteSetting
    {
        private static AppConfiguration appConfiguration = null;
        static StartupRouteSetting()
        {
            appConfiguration = SettingHelper.GetAppConfiguration();
        }

        public static void InitCustomRoute(IRouteBuilder routes, bool reload = true)
        {
            //LogHelper.WriteLog("InitCustomRoute: Start.");
            List<CustomRouteSetting> list = new List<CustomRouteSetting>();
            if (reload)
            {
                list.AddRange(StartupStaticDI.customRouteSettingRepository.GetCustomRouteSettings());
            }
            //LogHelper.WriteLog("DB route: " + list.Count);
            string routeSettings = appConfiguration.RouteSettings;
            List<CustomRouteSetting> listConfigRoutes = new List<CustomRouteSetting>();
            if (!string.IsNullOrEmpty(routeSettings))
            {
                try
                {
                    listConfigRoutes = JsonConvert.DeserializeObject<List<CustomRouteSetting>>(routeSettings);
                    if (listConfigRoutes == null)
                    {
                        listConfigRoutes = new List<CustomRouteSetting>();
                        //LogHelper.WriteLog("WebConfig route: " + listConfigRoutes.Count);
                    }

                }
                catch (Exception ex)
                {
                    //LogHelper.WriteLog("InitCustomRoute:" + ex.Message);
                }
            }
            if (listConfigRoutes.Count > 0)
            {
                list.AddRange(listConfigRoutes.Where(x => !list.Any(y => y.domainValue.Equals(x.domainValue))).ToList());
            }
            //LogHelper.WriteLog("Total unique route: " + list.Count);

            foreach (var item in list)
            {
                //LogHelper.WriteLog("- Register route: " + item.domainValue);
                if (item.domainValue == "defaultRoute")
                {
                    routes.MapRoute(
                        name: "Main_CustomerSite",
                        template: "{SiteName}/{Controller}/{Action}/{Id?}",
                        defaults: new { SiteName = "contestfactory", Controller = "home", Action = "index" }
                    );
                }
                else
                {
                    routes.MapRoute(
                        name: "CustomerSite_" + item.ID,
                        template: "{SiteName}/{Controller}/{Action}/{Id?}",
                        defaults: new { SiteName = item.siteExtension, Controller = item.controllerValue, Action = item.actionValue },
                        constraints: new { host = new HostConstraint("^" + item.domainValue + "$") }
                    );
                }
            }

            if (string.IsNullOrEmpty(routeSettings))
            {
                routes.MapRoute(
                    name: "Main_CustomerSite",
                    template: "{SiteName}/{Controller}/{Action}/{Id?}",
                    defaults: new { SiteName = "contestfactory", Controller = "home", Action = "index" }
                );
            }
        }
    }

    public class HostConstraint : IRouteConstraint
    {
        private readonly Regex _host;

        public HostConstraint(string pattern)
        {
            _host = new Regex(pattern,
                    RegexOptions.Compiled | RegexOptions.IgnoreCase);
        }

        public bool Match(HttpContext httpContext, IRouter route, string routeKey, RouteValueDictionary values, RouteDirection routeDirection)
        {
            return _host.IsMatch(httpContext.Request.Host.Value);
        }
    }
}
