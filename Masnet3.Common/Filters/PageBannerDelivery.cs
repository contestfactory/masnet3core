﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common.Statups;
using Masnet3.Common.Utilities;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Filters
{
    public class PageBannerDelivery
    {
        public Banner H1 { get; set; }
        public Banner H2 { get; set; }
        public Banner R1 { get; set; }
        public Banner R2 { get; set; }
        public Banner R3 { get; set; }
        public Banner R4 { get; set; }
        public Banner B1 { get; set; }
        public Banner B2 { get; set; }

        public void LoadBannersByPage(string pagename)
        {
            var objPage = StartupStaticDI.pageRepository.GetPageByName(pagename);
            if (objPage == null)
                objPage = new Page() { PageID = 0 };
            LoadBannersByPage(objPage.PageID);
        }

        public void LoadBannersByPage(int PageID)
        {
            var objPage = StartupStaticDI.pageRepository.GetPageById(PageID)
                                  .DefaultIfEmpty(new Page()
                                  {
                                      ShowBanner = false
                                  })
                                  .FirstOrDefault();
            if (!objPage.ShowBanner.GetValueOrDefault())
            {
                this.H1
                    = this.H2
                    = this.R1 = this.R2 = this.R3 = this.R4
                    = this.B1 = this.B2 = null;
                return;
            }

            // PagePosition
            var PagePosition = StartupStaticDI.pageRepository.GetPagePosition(PageID, GlobalVariables.SiteInformation.ApplicationID, GlobalVariables.SiteInformation.CampaignID).ToList();

            // H1 & H2
            var H1Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.H1).FirstOrDefault();
            if (H1Pos == null)
                H1Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.H1, UseGlobal = true };

            var H2Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.H2).FirstOrDefault();
            if (H2Pos == null)
                H2Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.H2, UseGlobal = true };

            // R1 & R2 & R3 & R4
            var R1Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.R1).FirstOrDefault();
            if (R1Pos == null)
                R1Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.R1, UseGlobal = true };

            var R2Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.R2).FirstOrDefault();
            if (R2Pos == null)
                R2Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.R2, UseGlobal = true };

            var R3Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.R3).FirstOrDefault();
            if (R3Pos == null)
                R3Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.R3, UseGlobal = true };

            var R4Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.R4).FirstOrDefault();
            if (R4Pos == null)
                R4Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.R4, UseGlobal = true };

            // B1 & B2
            var B1Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.B1).FirstOrDefault();
            if (B1Pos == null)
                B1Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.B1, UseGlobal = true };

            var B2Pos = PagePosition.Where(p => p.PositionID.GetValueOrDefault() == (int)Enums.BannerPosition.B2).FirstOrDefault();
            if (B2Pos == null)
                B2Pos = new PagePosition() { PositionID = (int)Enums.BannerPosition.B2, UseGlobal = true };

            // Header
            var objHeaderBanner = GetShownBannerInSharedPosition(PageID, Pos1: H1Pos, Pos2: H2Pos);
            if (objHeaderBanner != null)
            {
                if (objHeaderBanner.ShownPosition.GetValueOrDefault() == H1Pos.PositionID)
                {
                    if (objHeaderBanner.Width.GetValueOrDefault() == GlobalVariables.BannerPositions["HeaderFull"][0]) // Full
                    {
                        this.H1 = objHeaderBanner;
                        this.H2 = null;
                    }
                    else
                    { // Half
                        this.H1 = objHeaderBanner;
                        this.H2 = GetShownBannerInPostion(PageID, H2Pos);
                    }
                }
                else
                {
                    this.H2 = objHeaderBanner;
                    this.H1 = GetShownBannerInPostion(PageID, H1Pos, GlobalVariables.BannerPositions["HeaderHalf"][0]);
                }
            }
            else
            {
                this.H1 = null;
                this.H2 = null;
            }

            // Right
            this.R1 = GetShownBannerInPostion(PageID, R1Pos);
            this.R2 = GetShownBannerInPostion(PageID, R2Pos);
            this.R3 = GetShownBannerInPostion(PageID, R3Pos);
            this.R4 = GetShownBannerInPostion(PageID, R4Pos);

            // Bottom
            var objBottomBanner = GetShownBannerInSharedPosition(PageID, Pos1: B1Pos, Pos2: B2Pos);
            if (objBottomBanner != null)
            {
                if (objBottomBanner.ShownPosition.GetValueOrDefault() == B1Pos.PositionID)
                {
                    if (objBottomBanner.Width.GetValueOrDefault() == GlobalVariables.BannerPositions["BottomFull"][0]) // Full
                    {
                        this.B1 = objBottomBanner;
                        this.B2 = null;
                    }
                    else
                    { // Half
                        this.B1 = objBottomBanner;
                        this.B2 = GetShownBannerInPostion(PageID, B2Pos);
                    }
                }
                else
                {
                    this.B2 = objBottomBanner;
                    this.B1 = GetShownBannerInPostion(PageID, B1Pos, GlobalVariables.BannerPositions["BottomHalf"][0]);
                }
            }
            else
            {
                this.B1 = null;
                this.B2 = null;
            }
        }


        public Banner GetShownBannerInSharedPosition(int PageID, PagePosition Pos1, PagePosition Pos2)
        {
            return StartupStaticDI.pageRepository.GetShownBannerInSharedPosition(PageID, Pos1, Pos2, GlobalVariables.SiteInformation.ApplicationID,
               GlobalVariables.SiteInformation.CampaignID).FirstOrDefault();
        }

        public Banner GetShownBannerInPostion(int PageID, PagePosition Pos1, int WidthConstrainst = 0)
        {
            return StartupStaticDI.pageRepository.GetShownBannerInPostion(PageID, Pos1, WidthConstrainst, GlobalVariables.SiteInformation.ApplicationID,
               GlobalVariables.SiteInformation.CampaignID).FirstOrDefault();
        }

        public void IncreaseViews()
        {
            if (H1 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(H1.BannerID);
            if (H2 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(H2.BannerID);
            if (R1 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(R1.BannerID);
            if (R2 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(R2.BannerID);
            if (R3 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(R3.BannerID);
            if (R4 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(R4.BannerID);
            if (B1 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(B1.BannerID);
            if (B2 != null)
                StartupStaticDI.pageRepository.UpdateBannerSetView(B2.BannerID);
        }
    }
}
