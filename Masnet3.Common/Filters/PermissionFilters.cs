﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using Microsoft.Extensions.DependencyInjection;
using Masnet3.Common.Utilities;
using MASNET.DataAccess.Constants.Enums;
using MASNET.DataAccess;
using Masnet3.Business.Repository.Contests;
using Masnet3.Business.Repository.Applications;
using Masnet3.Business.Repository.Users;
using System.Linq;
using Masnet3.Business.Repository.Layout;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc;
using Masnet3.Common.Helper;
using Masnet3.Business.Repository;
using Masnet3.Business.Repository.Customer;

namespace Masnet3.Common.Filters
{
    public class PermissionFilters : IActionFilter
    {
        bool isCustomer, hasPermission;
        private readonly IContestRepository contestRepository;
        private readonly IApplicationRepository applicationRepository;
        private readonly IUserRepository userRepository;
        private readonly IPageRepository pageRepository;
        private readonly IRepositoryBasic repositoryBasic;
        private readonly IRegistrationRepository registrationRepository;

        public PermissionFilters(
            IContestRepository contestRepository, 
            IApplicationRepository applicationRepository,
            IPageRepository pageRepository,
            IRepositoryBasic repositoryBasic,
            IRegistrationRepository registrationRepository,
            IUserRepository userRepository)
        {
            this.repositoryBasic = repositoryBasic;
            this.pageRepository = pageRepository;
            this.userRepository = userRepository;
            this.applicationRepository = applicationRepository;
            this.contestRepository = contestRepository;
            this.registrationRepository = registrationRepository;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            //do not thing
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            string controller = context.RouteData.Values["controller"].ToString();
            string action = context.RouteData.Values["action"].ToString();
            string pagename = String.Format("{0}/{1}", controller, action);
            bool valid = true;
            Page objPage = null;
            isCustomer = true;
            hasPermission = true;
            bool hasPermissionChecking = false;

            if (context.HttpContext.Items["isPreview"] != null)
            {
                if (!string.IsNullOrEmpty(context.HttpContext.Items["isPreview"].ToString()))
                {
                    //base.OnActionExecuting(context);
                    return;
                }
            }

            if (context.HttpContext.Request.Headers["x-requested-with"] == "XMLHttpRequest") // isAjaxCall 
            {
                context.HttpContext.Response.Headers.Add("HasSession", SessionUtilities.IsLoggedIn().ToString());
            }

            string[] noCheckingPermissionPages = new string[] {
                "home/index", "news/_news",
                "customer/register", "home/customerlogin", "registration/forgotpassword", "action/getsharelist",
                "contest/details", "contestant/_contestantentries", "contestpages/_getctabutton",
                "navigation/_superadminnavigation", "navigation/_leftnavigation",
                "contest/submit", "uploadfile/_youtube", "uploadfile/_record", "uploadfile/library", "uploadfile/upload",
                    "uploadfile/_textmessage", "fileextension/getfileextension", "uploadfile/medialist","contestant/_media",
                "publishedcontest/contestantentrydetail",
                "contest/match", "contestant/_contestantlistwithmedia", "contestant/details", "contestant/_othercontestants",
                "contestant/contestantvote", "contestant/contestantavg",
                "home/logout",
                "home/contestended"
            };

            string currentPage = string.Format("{0}/{1}", controller.ToLower().Trim(), action.ToLower().Trim());
            if (noCheckingPermissionPages.Contains(currentPage))
            {
                //base.OnActionExecuting(filterContext);
                return;
            }

            //Uan commented
            try
            {
                objPage = Commons.PagePermission.Where(p => p.PageName.ToLower() == pagename.ToLower()).FirstOrDefault();
            }
            catch (Exception e)
            {

            }

            if (objPage == null)
            {
                objPage = this.pageRepository.GetPageByName(pagename);
                if (objPage != null)
                {
                    Commons.PagePermission.Add(objPage);
                }
                else
                {
                    objPage = new Page()
                    {
                        PageName = pagename,
                        CheckOwner = false,
                        CheckBelongToSite = false,
                        CheckPermission = false,
                        OnlySuperAdmin = false,
                        IsCustomer = false
                    };
                    Commons.PagePermission.Add(objPage);
                }
            }

            string httpMethod = context.HttpContext.Request.Method;
            if (httpMethod == "GET")
            {

                if (objPage.OnlySuperAdmin.GetValueOrDefault()
                    || objPage.IsCustomer.GetValueOrDefault()
                    || objPage.CheckPermission
                    || objPage.CheckOwner
                    || objPage.CheckBelongToSite)
                {
                    valid = false;
                    hasPermissionChecking = true;

                    //OR
                    if (objPage.OnlySuperAdmin.GetValueOrDefault())
                    {
                        valid = SessionUtilities.IsSuperAdmin();
                    }

                    //OR
                    if (objPage.IsCustomer.GetValueOrDefault() && !valid)
                    {
                        isCustomer = (SessionUtilities.CurrentUser.UserType == (int)Enums.RegistrationUserType.Customer);
                        valid = isCustomer;

                    }

                    //OR
                    if (objPage.CheckPermission && !valid)
                    {
                        hasPermission = HasPermission(objPage.PermissionList);
                        valid = hasPermission;
                    }

                    //OR
                    if (objPage.CheckOwner && !valid)
                    {
                        valid = IsOwner(objPage.OwnerType.Value, context, objPage);
                    }

                    //AND
                    if (objPage.CheckBelongToSite && valid)
                    {
                        valid = BelongToSite(objPage.BelongToSiteType.Value, GetRouteData(context, objPage.BelongToSiteRouteKey));
                    }
                }
                else
                {
                    valid = true;
                    hasPermissionChecking = false;
                }

                // Check Page Contrains
                if (!string.IsNullOrWhiteSpace(objPage.PageContrainst) && !SessionUtilities.IsSuperAdmin())
                {
                    hasPermissionChecking = true;
                    string pagecontrainst = objPage.PageContrainst;

                    string functionpattern = "@@(?<full>function.(?<functionname>[a-zA-Z0-9_]*)[(](?<params>[^)]*)[)])";
                    Regex reg = new Regex(functionpattern, RegexOptions.Multiline | RegexOptions.IgnoreCase);
                    foreach (System.Text.RegularExpressions.Match item in reg.Matches(pagecontrainst))
                        pagecontrainst = pagecontrainst.Replace(item.Value, item.Result(GetFunctionData(item.Groups["functionname"].Value, item.Groups["params"].Value)));

                    string pattern = "@@(?<param>[^ =;\t\r\n)']*)";
                    reg = new Regex(pattern, RegexOptions.Multiline);
                    foreach (System.Text.RegularExpressions.Match item in reg.Matches(pagecontrainst))
                        pagecontrainst = pagecontrainst.Replace(item.Value, item.Result(GetParamValue(context, item.Groups["param"].Value)));


                    if (context.HttpContext.Items["debugcontrainst"] != null)
                    {
                        if (!string.IsNullOrWhiteSpace(context.HttpContext.Items["debugcontrainst"].ToString()))
                        {
                            context.Result = new ContentResult()
                            {
                                Content = pagecontrainst.Replace("\n", "<br />")
                            };
                            return;
                        }
                    }
                    valid = valid && this.repositoryBasic.GetItemsDynamicSql<Boolean>(pagecontrainst).FirstOrDefault();
                }

                if (!valid)
                {
                    if (hasPermissionChecking)
                    {
                        if (!SessionUtilities.IsLoggedIn())
                        {
                            context.Result = new RedirectToRouteResult(
                                new RouteValueDictionary { { "controller", "Home" }, { "action", "CustomerLogin" }, { "returnUrl", context.HttpContext.Request.GetRawUrl() } });
                            return;
                        }
                    }

                    var responseType = (Enums.CheckPermissionResponseType)objPage.ResponseType.Value;
                    if (responseType == Enums.CheckPermissionResponseType.Redirect)
                    {
                        if (!string.IsNullOrWhiteSpace(objPage.ResponseRedirect))
                        {

                            string[] arr = objPage.ResponseRedirect.Split('/');

                            context.Result = new RedirectToRouteResult(
                                new RouteValueDictionary { { "controller", arr[0] }, { "action", arr[1] } });
                        }
                        else
                        {
                            context.Result = new RedirectToRouteResult(
                                new RouteValueDictionary { { "controller", "home" }, { "action", "index" } });
                        }
                    }
                    else
                    {
                        context.Result = new ContentResult()
                        {
                            Content = string.IsNullOrWhiteSpace(objPage.ResponseMessage) ? DbLanguage.GetText("gHasNoPermission") : objPage.ResponseMessage
                        };
                    }
                }
                else
                {
                    //base.OnActionExecuting(filterContext);
                }
            }
            else if (httpMethod == "POST")
            {
                if (objPage.OnlySuperAdmin.GetValueOrDefault()
                    || objPage.IsCustomer.GetValueOrDefault()
                    || objPage.CheckPermission
                    || objPage.CheckOwner
                    || objPage.CheckBelongToSite
                    || !string.IsNullOrWhiteSpace(objPage.PageContrainst)
                    )
                {
                    hasPermissionChecking = true;
                    if (!SessionUtilities.IsLoggedIn() && context.HttpContext.Request.Headers["Referer"].ToString() != null)
                    {
                        context.Result = new RedirectToRouteResult(
                            new RouteValueDictionary { { "controller", "Home" }, { "action", "CustomerLogin" }, { "returnUrl", context.HttpContext.Request.Headers["Referer"].ToString() } });
                    }
                }
                else
                {
                    //base.OnActionExecuting(filterContext);
                }
            }
            else
            {
                //base.OnActionExecuting(filterContext);
            }
        }


        public bool HasPermission(string permissions)
        {
            if (SessionUtilities.IsSuperAdmin())
            {
                return true;
            }
            if (!SessionUtilities.IsLoggedIn())
            {
                return false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(permissions))
                {
                    return true;
                }
                else
                {
                    foreach (var p in permissions.Split(','))
                    {
                        var permission = (Enums.Permissions)p.ToInt();
                        if (SessionUtilities.HasPermission(permission))
                        {
                            return true;
                        }
                    }
                    return false;
                }
            }
        }

        public bool IsOwner(int OwnerType, ActionExecutingContext filterContext, Page objPage)
        {
            if (SessionUtilities.IsSuperAdmin())
            {
                return true;
            }
            bool IsOwnerPage = false;
            var ownertype = (Enums.OwnerType)OwnerType;
            switch (ownertype)
            {
                case Enums.OwnerType.Profile:
                    IsOwnerPage =(Convert.ToInt64(GetRouteData(filterContext, objPage.OwnerRouteKey)) == SessionUtilities.CurrentUser.UserID);
                    break;
                case Enums.OwnerType.Contest:
                    var contestId = GetRouteData(filterContext, objPage.OwnerRouteKey);
                    if(string.IsNullOrEmpty(contestId))
                    {
                        IsOwnerPage = false;
                    }
                    var objContest = this.contestRepository.GetContestByContestId(Convert.ToInt32(contestId));
                    if (objContest != null)
                    {
                        IsOwnerPage =(objContest.CreatedBy == SessionUtilities.CurrentUser.UserID);
                    }
                    else
                    {
                        IsOwnerPage = false;
                    }
                    break;
                case Enums.OwnerType.Product:
                    int editSiteId = Convert.ToInt32(GetRouteData(filterContext, "editSiteId"));
                    int editCampaignId = Convert.ToInt32(GetRouteData(filterContext, "editCampaignId"));
                    if (editSiteId == 0 && editCampaignId == 0)
                    {
                        bool isCreate = (GetRouteData(filterContext, "isCreate") == "True");
                        if (isCreate)
                        {
                            IsOwnerPage = true;
                        }
                        else
                        {
                            IsOwnerPage = SessionUtilities.IsSuperAdmin();
                        }
                    }
                    else
                    {
                        if (editCampaignId > 0)
                        {

                            var proContest = this.contestRepository.GetContestByContestId(editCampaignId);
                            if (proContest != null)
                            {
                                IsOwnerPage = (proContest.CreatedBy == SessionUtilities.CurrentUser.UserID);
                            }
                            else
                            {
                                IsOwnerPage = false;
                            }
                        }
                        else if (editSiteId > 0)
                        {
                            var objApplication = this.applicationRepository.GetApplicationById(editSiteId);
                            if (objApplication != null)
                            {
                                IsOwnerPage =(objApplication.CreatedBy == SessionUtilities.CurrentUser.UserID);
                            }
                            else
                            {
                                IsOwnerPage = false;
                            }
                        }
                        else
                        {
                            IsOwnerPage = false;
                        }
                    }
                    break;
                default:
                    IsOwnerPage = false;
                    break;
            }
            return IsOwnerPage;
        }

        public bool BelongToSite(int BelongToSizeType, string value)
        {
            
            if (SessionUtilities.IsSuperAdmin())
            {
                return true;
            }
            bool IsBelongToSite = false;
            var belongtosizetype = (Enums.BelongToSiteType)BelongToSizeType;
            var si = GlobalVariables.SiteInformation;
            switch (belongtosizetype)
            {
                case Enums.BelongToSiteType.User:
                    string userid = "";
                    if (value == "" || value == string.Empty)
                        userid = SessionUtilities.CurrentUser.UserID.ToString();
                    else
                        userid = value;

                    var objUser = this.registrationRepository.GetRegistrationByUserId(userid); ;
                    if (objUser != null)
                        IsBelongToSite =(objUser.ApplicationID == si.ApplicationID && objUser.ContestID == si.CampaignID);
                    else
                        IsBelongToSite = true;
                    break;
                case Enums.BelongToSiteType.Contest:
                    var objContest = this.contestRepository.GetContestByContestId(int.Parse(value));
                    IsBelongToSite = (objContest.ApplicationID == si.ApplicationID);
                    break;
                default:
                    IsBelongToSite = false;
                    break;
            }
            return IsBelongToSite;
        }

        public string GetRouteData(ActionExecutingContext filterContext, string key)
        {

            try
            {
                if (string.IsNullOrWhiteSpace(key))
                {
                    return "";
                }

                if (filterContext.RouteData.Values[key] != null)
                {
                    return filterContext.RouteData.Values[key].ToString();
                }

                if (filterContext.HttpContext.Items[key] != null)
                {
                    return filterContext.HttpContext.Items[key].ToString();
                }
                return "";

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public string GetSessionData(string key)
        {
            string re = "";

            switch (key.ToLower())
            {
                case "userid":
                    re = SessionUtilities.CurrentUser.UserID.ToString();
                    break;
                case "siteid":
                    re = GlobalVariables.SiteInformation.ApplicationID.ToString();
                    break;
                case "campaignid":
                    re = GlobalVariables.SiteInformation.CampaignID.ToString();
                    break;
                default:
                    break;
            }

            return re;
        }

        public string GetContextData(string key)
        {
            string re = "";

            switch (key.ToLower())
            {
                case "iscustomer":
                    re = Convert.ToInt32(isCustomer).ToString();
                    break;
                case "haspermission":
                    re = Convert.ToInt32(hasPermission).ToString();
                    break;
                default:
                    break;
            }

            return re;
        }

        public string GetFunctionData(string functionname, string parameters)
        {
            string re = "";

            switch (functionname.ToLower())
            {
                case "haspermission":
                    re = Convert.ToInt32(HasPermission(ConvertToPermissionIds(parameters))).ToString();
                    break;
                default:
                    break;
            }

            return re;
        }

        public string GetParamValue(ActionExecutingContext filterContext, string key)
        {
            string[] arr = key.Split(new char[] { '.' }, 2);
            string type = arr[0].ToLower();
            if (type == "request")
            {
                return GetRouteData(filterContext, arr[1]);
            }
            else if (type == "session")
            {
                return GetSessionData(arr[1]);
            }
            else
            {
                return GetContextData(arr[1]);
            }
        }

        public string ConvertToPermissionIds(string PermissionNames)
        {
            string re = "";
            foreach (string item in PermissionNames.Split(','))
            {
                if (!string.IsNullOrWhiteSpace(re))
                    re += ",";

                var objPermission = this.userRepository.GetPermissionByName(item);
                if (objPermission != null)
                    re += objPermission.PermissionID.ToString();
            }

            return re;
        }

    }
}
