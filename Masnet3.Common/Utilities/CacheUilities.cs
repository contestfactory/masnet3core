﻿using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common.Caching;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public class CacheUilities
    {
        static public MemoryCacheManager cacheManager
        {
            get
            {
               return new MemoryCacheManager(new MemoryCache(new MemoryCacheOptions()));
            }
        }

        public static string GetCacheKeyByUseType(string key, int applicationID, int contestID, int templateID = 0, string prefix = "")
        {
            string result = "";
            Enums.UseType useType = Commons.GetUseType(applicationID, contestID, templateID);
            string appType = useType.ToString();
            string appID = "";
            switch (useType)
            {
                case Enums.UseType.Site:
                case Enums.UseType.ApplicationTemplate:
                    appID = applicationID.ToString();
                    break;
                case Enums.UseType.SiteContest:
                    appType += applicationID.ToString();
                    appID = contestID.ToString();
                    break;
                case Enums.UseType.Campaign:
                    appID = contestID.ToString();
                    break;
                case Enums.UseType.ContestTemplate:
                    appID = templateID.ToString();
                    break;
                default: break;
            }
            result = string.Format("{0}_{1}_{2}_{3}", prefix, appType, appID, key);
            return result;
        }
        public static string GetCacheFilter(int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1, int useType = 1, bool useSiteContest = false)
        {
            return GetCacheFilter(useType, "ApplicationID", "ContestID", "TemplateID", editApplicationID, editCampaignID, editTemplateID, useSiteContest: useSiteContest);
        }
        public static string GetCacheFilter(int UserType, string applicationField, string campaignField, string templateField, int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1, string prefixForUseType = "", bool useHidded = false, int editLanguageID = 1, bool useSiteContest = false)
        {
            string where = "";
            if (UserType.Equals((int)Enums.UseType.Global))
            {
                where = string.Format("AND {1}UseType = {0}", (int)UserType, prefixForUseType);
            }
            if (
                UserType.Equals((int)Enums.UseType.Site) ||
                (UserType.Equals((int)Enums.UseType.SiteContest) && !useSiteContest)
               )
            {
                where = string.Format("AND {3}UseType = {0} AND {1} = {2}", (int)UserType, applicationField, editApplicationID, prefixForUseType);
            }
            if (UserType.Equals((int)Enums.UseType.SiteContest) && useSiteContest)
            {
                where = string.Format("AND {3}UseType = {0} AND {1} = {2} AND {4} = {5}", (int)UserType, applicationField, editApplicationID, prefixForUseType, campaignField, editCampaignID);
            }
            if (UserType.Equals((int)Enums.UseType.Campaign))
            {
                if (!useHidded)
                {
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} AND {4}ShowHide <> {3} ", (int)UserType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
                }
                else
                {
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} ", (int)UserType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
                }
            }
            if (UserType.Equals((int)Enums.UseType.ContestTemplate))
            {
                where = string.Format("AND {3}UseType = {0} AND {1} = {2}", (int)UserType, templateField, editTemplateID, prefixForUseType);
            }
            return where + String.Format(" AND {0}LanguageID={1} ", prefixForUseType, editLanguageID);
        }
    }
}
