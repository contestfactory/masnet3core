﻿using MASNET.DataAccess;
using Masnet3.Common.Statups;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Masnet3.Common.Utilities
{
    public class RuleUilities
    {

        private static List<Rule> _rules = null;
        public static List<Rule> rules
        {
            get
            {
                if(_rules == null)
                {
                    _rules = StartupStaticDI.ruleRepository.GetListRules().ToList();
                }
                return _rules;
            }
        }

        public static string Serialize(ICollection<Rule> data, bool fullFields = false)
        {
            string result = "";
            if (data != null)
            {
                if (data.Count > 0)
                {
                    if (fullFields)
                    {
                        ICollection<dynamic> list = new List<dynamic>();
                        foreach (var item in data)
                        {
                            list.Add(new { RuleKey = item.RuleKey, Value = item.Value, ResourceKey = item.ResourceKey, ResourceMessage = item.ResourceMessage, IsRequireValue = item.IsRequireValue, IsBuiltIn = item.IsBuiltIn, PageID = item.PageID });
                        }
                        result = JsonConvert.SerializeObject(list);
                    }
                    else
                    {
                        try
                        {
                            result = JsonConvert.SerializeObject(data);
                        }
                        catch (Exception ex) { }
                    }
                }
            }
            return result;
        }

        public static List<Rule> Deserialize(string jsonData, int applicationID = 0, int contestID = 0, int templateID = 0, bool ignoreMessage = false, int languageID = 1)
        {
            List<Rule> result = new List<Rule>();
            if (jsonData != "")
            {
                try
                {
                    var contestTemplateId = StartupStaticDI.contestRepository.GetContestByContestId(contestID).TemplateID.Value;
                    result = JsonConvert.DeserializeObject<List<Rule>>(jsonData).OrderBy(x => x.RuleKey).ToList();
                    List<Rule> ruleInfos = rules;
                    List<Resource> ruleResources = new List<Resource>();
                    if (!ignoreMessage && result != null && result.Count > 0)
                    {
                        var useType = Commons.GetUseType(applicationID, contestID, templateID);
                        ruleResources = StartupStaticDI.resourceRepository.GetResources(
                            useType,
                            StartupCommon.DicApplications,
                            StartupCommon.DicContests,
                            contestTemplateId,
                            applicationID,
                            contestID,
                            templateID,
                            languageID: languageID,
                            sortColumn: "Value",
                            listKeys: result.Select(x => new Resource()
                            {
                                ResourceKey = x.ResourceKey,
                                PageID = x.PageID
                            }).ToList(),
                            canEditShowHide: !Commons.CanEditShowHide()
                        ).ToList();
                    }

                    foreach (var rule in result)
                    {
                        var ruleInfo = ruleInfos.FirstOrDefault(x => x.RuleKey == rule.RuleKey);
                        //rule.PageID = ruleInfo.PageID;
                        rule.IsRequireValue = ruleInfo.IsRequireValue;
                        rule.Name = ruleInfo.Name;
                        rule.Description = ruleInfo.Description;
                        var resource = ruleResources.FirstOrDefault(x => x.ResourceKey == rule.ResourceKey);
                        rule.ResourceMessage = ignoreMessage ? "" : resource.Value;
                        //rule.ResourceMessage = DbLanguage.GetText(rule.ResourceKey, "", rule.PageID);
                    }
                }
                catch (Exception ex) { }
            }
            return result;
        }
    }
}
