﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public static class RequestUtilities
    {

        public static bool IsLocal(this HttpRequest req)
        {
            var connection = req.HttpContext.Connection;
            if (connection.RemoteIpAddress != null)
            {
                if (connection.LocalIpAddress != null)
                {
                    return connection.RemoteIpAddress.Equals(connection.LocalIpAddress);
                }
                else
                {
                    return IPAddress.IsLoopback(connection.RemoteIpAddress);
                }
            }
            // for in memory TestServer or when dealing with default connection info
            if (connection.RemoteIpAddress == null && connection.LocalIpAddress == null)
            {
                return true;
            }
            return false;
        }

        public static string Scheme(HttpRequest Request = null)
        {
            string scheme = "http";
            if (Request == null && GlobalVariables.httpContextObj.HttpContext != null)
            {
                Request = GlobalVariables.httpContextObj.HttpContext.Request;
            }
            if (Request != null)
            {
                bool isHttps = false;
                if(GlobalVariables.httpContextObj.HttpContext.Items["HTTP_X_FORWARDED_PROTO"] != null)
                {
                    isHttps = GlobalVariables.httpContextObj.HttpContext.Items["HTTP_X_FORWARDED_PROTO"].ToString() == "https";
                }
                scheme = (GetAbsoluteUri(Request).StartsWith("https://", StringComparison.OrdinalIgnoreCase) || isHttps) ? "https" : "http";
            }
            return scheme;
        }

        public static string GetAbsoluteUri(HttpRequest request)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.Host;
            uriBuilder.Path = request.Path.ToString();
            uriBuilder.Query = request.QueryString.ToString();
            return uriBuilder.Uri.ToString();
        }

        public static string GetRawUrl(this HttpRequest request)
        {
            var httpContext = request.HttpContext;
            return $"{httpContext.Request.Scheme}://{httpContext.Request.Host}{httpContext.Request.Path}{httpContext.Request.QueryString}";
        }

        public static Uri GetAbsoluteUriSelf(this HttpRequest request)
        {
            UriBuilder uriBuilder = new UriBuilder();
            uriBuilder.Scheme = request.Scheme;
            uriBuilder.Host = request.Host.Host;
            uriBuilder.Path = request.Path.ToString();
            uriBuilder.Query = request.QueryString.ToString();
            return uriBuilder.Uri;
        }


        private static void InitHeaders(HttpWebRequest request, WebHeaderCollection headers)
        {
            foreach (var key in headers.AllKeys)
            {
                if (WebHeaderCollection.IsRestricted(key))
                {
                    SetRestrictHeader(request, key, headers[key]);
                }
                else
                {
                    request.Headers.Add(key, headers[key]);
                }
            }
        }
        private static void SetRestrictHeader(HttpWebRequest request, string header, string value)
        {
            string headerName = header.ToLower();
            try
            {
                if (headerName == "accept") request.Accept = value;
                if (headerName == "connection") request.Connection = value;
                if (headerName == "content-length") request.ContentLength = long.Parse(value);
                if (headerName == "content-type") request.ContentType = value;
                if (headerName == "date") request.Date = DateTime.Parse(value);
                if (headerName == "expect") request.Expect = value;
                if (headerName == "host") request.Host = value;
                if (headerName == "if-modified-since") request.IfModifiedSince = DateTime.Parse(value);
                if (headerName == "keep-alive") request.KeepAlive = Boolean.Parse(value);
                if (headerName == "range") request.AddRange(int.Parse(value));
                if (headerName == "referer") request.Referer = value;
                if (headerName == "transfer-encoding") request.TransferEncoding = value;
                if (headerName == "user-agent") request.UserAgent = value;
            }
            catch (Exception ex)
            {
                //LogHelper.WriteLog("SetRestrictHeader: " + ex.Message);
            }

        }
        public static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            //LogHelper.WriteLog(string.Format("SslPolicyErrors: {0}", sslPolicyErrors));
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                return true;
            }
            return true; // ignore error 
        }

        public static HttpWebResponse MakeRequest(string url, string method, WebHeaderCollection headers)
        {
            //if (enableTLS) ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = method;
            request.AllowAutoRedirect = false;
            // default headers
            request.KeepAlive = true;
            // custom headers
            //request.Headers = headers;
            InitHeaders(request, headers);
            try
            {
                System.Net.ServicePointManager.ServerCertificateValidationCallback = new System.Net.Security.RemoteCertificateValidationCallback(ValidateServerCertificate);
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            }
            catch (Exception ex)
            {
                //LogHelper.WriteLog("Enable TLS: " + ex.Message);
            };
            return (HttpWebResponse)request.GetResponse();
        }

    }
}
