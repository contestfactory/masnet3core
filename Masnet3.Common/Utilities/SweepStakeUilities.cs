﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common.Statups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public class SweepStakeUilities
    {
        public static SweepStake GetByAction(int ActionID, int? editSiteId = null, int? editCampaignId = null, Period activePeriod = null)
        {
            if (activePeriod == null)
            {
                activePeriod = PeriodUilities.GetActivePeriod(editSiteId, editCampaignId);
            }
            SweepStake objSS = GetByAction(ActionID, Enums.SweepType.Period, activePeriod.PeriodID, editSiteId, editCampaignId);

            if (objSS != null)
                return objSS;
            else
            {
                return new SweepStake()
                {
                    UserActionID = ActionID,
                    ReferID = activePeriod.PeriodID,
                    SweepType = (int)Enums.SweepType.Period,
                    Point = 0,
                    Amount = 0,
                    Limited = 0,
                    PointLimit = 0,
                    PointFrequency = 0
                };
            }
        }
        public static SweepStake GetByAction(int ActionID, Enums.SweepType sweeptype, int ReferID, int? editSiteId = null, int? editCampaignId = null)
        {
            SweepStake result = null;
            if (!editSiteId.HasValue)
            {
                var site = GlobalVariables.SiteInformation;
                editSiteId = site.ApplicationID;
                editCampaignId = site.CampaignID;
            }
            string cacheName = string.Format("{0}_{1}_{2}", sweeptype.ToString(), ActionID.ToString(), ReferID.ToString());
            string cacheKey = CacheUilities.GetCacheKeyByUseType(cacheName, editSiteId.Value, editCampaignId.Value, prefix: "SA");

            if (!CacheUilities.cacheManager.HashKey(cacheKey))
            {
                result = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<SweepStake>(
                        @"SELECT s.*, a.ActionName UserActionName
                    FROM sweepstake s inner join Action a on s.[UserActionID] = a.[ActionID] " +
                        Commons.Join(new
                        {
                            LeftTable = "s",
                            Table = "SweepStake",
                            OutTable = "NewSweepStake",
                            IDField = "ID",
                            JoinFields = "UserActionID",
                            editCampaignID = editCampaignId,
                            editApplicationID = editSiteId,
                            editTemplateID = 0,
                            PeriodID = ReferID
                        }) +
                        @" WHERE ISNULL(a.[IsDeleted], 0) = 0 
                          AND s.UserActionID=@ActionID",
                        new
                        {
                            ActionID = ActionID,
                            ReferID = ReferID,
                            SweepType = (int)sweeptype
                        }).FirstOrDefault();

                CacheUilities.cacheManager.Set(cacheKey, result);
            }
            else
            {
                result = CacheUilities.cacheManager.Get<SweepStake>(cacheKey);
            }
            return result;

        }

        public static SweepStake GetSweepstakeDirect(int ActionID, int? editSiteId = null, int? editCampaignId = null)
        {
            SweepStake result = null;
            string cacheName = string.Format("actionID_{0}", ActionID.ToString());
            string cacheKey = CacheUilities.GetCacheKeyByUseType(cacheName, editSiteId.Value, editCampaignId.Value, prefix: "SAD");
            if (!CacheUilities.cacheManager.HashKey(cacheKey))
            {
                string query = @"
                        SELECT s.*, Action.ActionType FROM Action 
                        JOIN Sweepstake s ON Action.ActionID = s.UserActionID " +
                        Commons.Join(new
                        {
                            LeftTable = "s",
                            Table = "SweepStake",
                            OutTable = "NewSweepStake",
                            IDField = "ID",
                            JoinFields = "UserActionID",
                            editCampaignID = editCampaignId,
                            editApplicationID = editSiteId,
                            editTemplateID = 0
                        }) +
                        @" WHERE s.UserActionID = @ActionID";
                result = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<SweepStake>(query
                    , new
                    {
                        ApplicationID = editSiteId.Value,
                        ContestID = editCampaignId.Value,
                        ActionID = ActionID
                    }).FirstOrDefault();

                CacheUilities.cacheManager.Set(cacheKey, result);
            }
            else
            {
                result = CacheUilities.cacheManager.Get<SweepStake>(cacheKey);
            }
            return result;
        }

    }
}
