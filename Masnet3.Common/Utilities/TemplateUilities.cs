﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common.Statups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Masnet3.Common.Utilities
{
    public class TemplateUilities
    {
        public static Template GetTemplateByNameDirect(int TemplateKeyID, int ApplicationID = 0, int ContestID = 0, int LanguageID = 1, SiteInformation siteInfo = null)
        {
            Template template = null;
            string sql = "";
            string cacheKey = "";

            if (siteInfo != null)
            {
                int editApplicationID = ApplicationID;
                int editCampaignID = ContestID;
                int editTemplateID = 0;

                Enums.UseType useType = Commons.GetUseType(editApplicationID, editCampaignID, editTemplateID);
                string cacheName = useType.ToString();
                string cacheID = "";
                switch (useType)
                {
                    case Enums.UseType.Site:
                    case Enums.UseType.ApplicationTemplate:
                        cacheID = editApplicationID.ToString();
                        break;
                    case Enums.UseType.SiteContest:
                        cacheName += editApplicationID.ToString();
                        cacheID = editCampaignID.ToString();
                        break;
                    case Enums.UseType.Campaign:
                        cacheID = editCampaignID.ToString();
                        break;
                    case Enums.UseType.ContestTemplate:
                        cacheID = editTemplateID.ToString();
                        break;
                    default: break;
                }
                cacheKey = string.Format("Tmpl_{0}_{1}", cacheName, cacheID);
                template = CacheUilities.cacheManager.Get<Template>(cacheKey);
            }

            if (template == null)
            {
                sql = @"SELECT * FROM Template " +
                           Commons.Join(new
                           {
                               LeftTable = "Template",
                               Table = "Template",
                               IDField = "TemplateID",
                               JoinFields = "TemplateKeyID,LanguageID",
                               editCampaignID = ContestID,
                               editApplicationID = ApplicationID,
                               editTemplateID = 0
                           }) +
                          @"WHERE (IsDisplay = 1)  AND TemplateKeyID = @TemplateKeyID 
                        AND ISNULL(LanguageID,1)=@LanguageID ";
                template = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<Template>(sql, new
                {
                    TemplateKeyID = TemplateKeyID,
                    ApplicationID = ApplicationID,
                    ContestID = ContestID,
                    LanguageID = LanguageID
                }).FirstOrDefault();
                CacheUilities.cacheManager.Set(cacheKey, template);
            }
            return template;
        }

        public static string GetNiceBody(string text)
        {
            //string text = "abc<mmrtl>acceABCMyTextHereDCdkkkdikksllais<>";
            string result = "";
            Regex r1 = new Regex(@"<eb>((.|\s)*?)</eb>");
            System.Text.RegularExpressions.Match match = r1.Match(text);
            if (match.Success)
            {
                result = match.Groups[1].Value;
            }
            return result;
        }
    }
}
