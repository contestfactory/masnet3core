﻿using MASNET.DataAccess;
using Masnet3.Common.Caching;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using Masnet3.Business.Repository.Resources;
using Microsoft.AspNetCore.Http;
using System.Linq;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository.Contests;
using Masnet3.Common.Statups;

namespace Masnet3.Common.Utilities
{
    public class SiteInformation
    {
        public MinifiedApplication Application = new MinifiedApplication();
        public MinifiedContest Campaign = new MinifiedContest();
        public int SiteContestID { get; set; }
        public int CampaignID = 0;
        public int ApplicationID = 0;
        public Enums.SiteType SiteType = Enums.SiteType.Main;
        public Enums.SiteStatus SiteStatus = Enums.SiteStatus.InvalidSite;
        public string SiteTitle
        {
            get
            {
                return (this.SiteType == Enums.SiteType.Campaign) ? this.Campaign.ContestName : (string.IsNullOrWhiteSpace(this.Application.Title) ? this.Application.ApplicationName : this.Application.Title);
            }
        }
        public string Logo
        {
            get;set;
        }
        public string LogoUrl
        {
            get; set;
        }
        private Theme _Theme = null;
        public Theme Theme
        {
            get
            {
                if (IsPreview())
                {
                    if (GlobalVariables.httpContextObj.HttpContext != null && GlobalVariables.httpContextObj.HttpContext.Request != null)
                    {
                        if (GlobalVariables.httpContextObj.HttpContext.Items["CustomTheme"] != null)
                        {
                            if (!string.IsNullOrEmpty(GlobalVariables.httpContextObj.HttpContext.Items["CustomTheme"].ToString()))
                            {
                                string customTheme = GlobalVariables.httpContextObj.HttpContext.Items["CustomTheme"].ToString();
                                return GlobalVariables.Themes.Where(t => t.Value.ThemeName == customTheme).FirstOrDefault().Value;
                            }
                        }
                    }
                }

                if (GlobalVariables.httpContextObj.HttpContext != null && GlobalVariables.httpContextObj.HttpContext.Session.Get<int>("SelectedThemeId") != 0)
                {

                    int SSThemeId = GlobalVariables.httpContextObj.HttpContext.Session.Get<int>("SelectedThemeId");
                    _Theme = GlobalVariables.Themes[SSThemeId];
                }

                if (GlobalVariables.httpContextObj.HttpContext.Items["UrlThemeId"] != null)
                {
                    if (!string.IsNullOrEmpty(GlobalVariables.httpContextObj.HttpContext.Items["UrlThemeId"].ToString()))
                    {
                        int urlThemeId = 0;
                        if (int.TryParse(GlobalVariables.httpContextObj.HttpContext.Items["UrlThemeId"].ToString(), out urlThemeId))
                        {
                            return GlobalVariables.Themes[urlThemeId];
                        }
                    }
                }
                if (GlobalVariables.httpContextObj.HttpContext.Items["UrlTheme"] != null)
                {
                    if (!string.IsNullOrEmpty(GlobalVariables.httpContextObj.HttpContext.Items["UrlTheme"].ToString()))
                    {
                        string urlTheme = GlobalVariables.httpContextObj.HttpContext.Items["UrlTheme"].ToString();
                        return GlobalVariables.Themes.Where(t => t.Value.ThemeName == urlTheme).FirstOrDefault().Value;
                    }
                }

                if (this.CampaignID > 0)
                {
                    int _ThemeId = 0;
                    string key = "Theme_" + this.CampaignID;
                    _Theme = CacheUilities.cacheManager.Get<Theme>(key);
                    if (_Theme == null)
                    {
                        var dic = GlobalVariables.Themes;
                        _ThemeId = StartupStaticDI.contestRepository.GetContestByContestId(this.CampaignID).ThemeID;
                        _Theme = dic[_ThemeId];
                        CacheUilities.cacheManager.Set(key, _Theme);
                    }
                }
                else
                {
                    _Theme = GlobalVariables.Themes.ContainsKey(Application.ThemeID) ? GlobalVariables.Themes[Application.ThemeID] : new Theme();
                }

                if (_Theme == null)
                {
                    _Theme = new Theme();
                }

                if (string.IsNullOrEmpty(_Theme.ThemeFolder))
                    _Theme.ThemeFolder = "";

                return _Theme;
            }
        }
        public void ClearSelectedTheme()
        {
            _Theme = null;
        }
        private List<Resource> _Resources = null;
        public List<Resource> Resources
        {
            get
            {
                string key = string.Format("SiteInformation_Resources_{0}_{1}", this.ApplicationID, this.CampaignID);
                if (_Resources == null || _Resources.Count == 0)
                {
                    _Resources = CacheUilities.cacheManager.Get<List<Resource>>(key);
                    if (_Resources == null || _Resources.Count == 0)
                    {
                        _Resources = StartupStaticDI.resourceRepository.GetResource(this.ApplicationID, this.CampaignID).ToList();
                    }
                }
                return _Resources;
            }
        }
        public string MediaFolder
        {
            get
            {
                if (SiteType == Enums.SiteType.Campaign)
                {
                    return String.Format("Campaign{0}", CampaignID);
                }
                return Application.ApplicationName;
            }
        }
        public string ContextName
        {
            get
            {
                if (SiteType == Enums.SiteType.Campaign || (Campaign != null && Campaign.ContestID > 0))
                {
                    return Campaign.ContestDomain + "";
                }
                return Application != null ? Application.ApplicationName + "" : "";
            }
        }
        public string HideForCampaign
        {
            get
            {
                if (SiteType == Enums.SiteType.Campaign || (CampaignID > 0))
                {
                    return "display:none";
                }
                return "";
            }
        }
        public bool IFrameMode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string HeaderView
        {
            get
            {
                string headerView = Application.HeaderView;
                if (SiteType == Enums.SiteType.Campaign || (Campaign != null && Campaign.ContestID > 0))
                {
                    headerView = Theme.HeaderView;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(headerView))
                    {
                        headerView = Theme.HeaderView;
                    }
                    else
                    {
                        headerView = Application.HeaderView;
                    }
                }

                if (!string.IsNullOrWhiteSpace(headerView))
                {
                    headerView = headerView.ToLower();

                    if (headerView.Contains("/_contestfactory_header.cshtml") && (ApplicationID != 0 || CampaignID != 0))
                    {
                        headerView = null;
                    }
                }
                else
                {
                    headerView = null;
                }

                //if (!string.IsNullOrWhiteSpace(headerView))
                //{
                //    if (System.Configuration.ConfigurationManager.AppSettings["UsingHeaderMenuLinks"] == "1")
                //    {
                //        if (headerView.ToLower().EndsWith("_menulinks.cshtml") == false)
                //        {
                //            string tempHeaderView = headerView.Replace(".cshtml", "_MenuLinks.cshtml");
                //            if (HttpContext.Current != null && HttpContext.Current.Server != null)
                //            {
                //                if (System.IO.File.Exists(HttpContext.Current.Server.MapPath(tempHeaderView)))
                //                {
                //                    headerView = tempHeaderView;
                //                }
                //            }
                //        }
                //    }
                //}

                return headerView;
            }
        }
        public string FooterView
        {
            get
            {
                string footerView = Application.FooterView;
                if (SiteType == Enums.SiteType.Campaign || (Campaign != null && Campaign.ContestID > 0))
                {
                    footerView = Theme.FooterView;
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(footerView))
                    {
                        footerView = Theme.FooterView;
                    }
                    else
                    {
                        footerView = Application.FooterView;
                    }
                }

                if (!string.IsNullOrWhiteSpace(footerView))
                {
                    footerView = footerView.ToLower();

                    if (footerView.Contains("/_contestfactory_footer.cshtml") && (ApplicationID != 0 || CampaignID != 0))
                    {
                        footerView = null;
                    }
                }
                else
                {
                    footerView = null;
                }

                return footerView;
            }
        }
        public int TimeZoneID { get; set; }
        public double SpanTime { get; set; }
        public string GATrackingCode
        {
            get
            {
                string strResult = string.Empty;
                string keyContest = CacheUilities.GetCacheKeyByUseType("Tracking", ApplicationID, GetContestID(), prefix: "GA");
                var gObjCache = CacheUilities.cacheManager.Get<string>(keyContest);
                if (gObjCache != null)
                {
                    strResult = (string)gObjCache;
                }
                else
                {
                    var useType = Commons.GetUseType(ApplicationID, GetContestID());
                    strResult = StartupStaticDI.contestRepository.GetGATrackingForContest(useType, ApplicationID, GetContestID(), CampaignID).GATracking;
                    CacheUilities.cacheManager.Set(keyContest, string.IsNullOrEmpty(strResult) ? "" : strResult);

                }
                return strResult;
            }
        }
        public List<ItemDetail> SocialLinks
        {
            get
            {
                //string key = string.Format("SocialLinks_{0}_{1}", CampaignID, ApplicationID);
                //List<ItemDetail> _ListSocialLinks = (List<ItemDetail>)CachingUtilities.GetCachingValue(key);
                //if (_ListSocialLinks == null || _ListSocialLinks.Count == 0)
                //{
                //    _ListSocialLinks = (new ItemDetailBL()).GetItemDetails(ApplicationID, CampaignID, 0, Enums.ItemType.SocialMedia_SocialLinks, true);
                //    //tuning: Thanh-05/04
                //    //List<Item> items = new ItemBL().Get(itemType: Enums.ItemType.SocialMedia_SocialLinks);

                //    for (int index = 0; index < _ListSocialLinks.Count; index++)
                //    {
                //        var socialLink = _ListSocialLinks[index];
                //        //tuning: Thanh-05/04
                //        //Item item = items.FirstOrDefault(i => i.ItemID == socialLink.ItemID);
                //        //if (!string.IsNullOrWhiteSpace(socialLink.Value))
                //        //    socialLink.Value = string.Format("{0}{1}", item.Description, socialLink.Value);
                //        if (!string.IsNullOrWhiteSpace(socialLink.Value))
                //        {
                //            if (socialLink.Item.DisplayName == "Google+")
                //                socialLink.Value = "+" + socialLink.Value;
                //            socialLink.Value = string.Format("{0}{1}", socialLink.Item.Description, socialLink.Value);
                //        }
                //    }

                //    CachingUtilities.SetCachingValue(key, _ListSocialLinks);
                //}

                //return _ListSocialLinks;
                return new List<ItemDetail>();
            }
        }
        public bool IsPreview()
        {
            bool result = false;

            if (GlobalVariables.httpContextObj.HttpContext != null && GlobalVariables.httpContextObj.HttpContext.Request != null)
            {
                if (GlobalVariables.httpContextObj.HttpContext.Items["IsPreview"] != null)
                {
                    return (Convert.ToBoolean(string.IsNullOrWhiteSpace(GlobalVariables.httpContextObj.HttpContext.Items["IsPreview"].ToString())) == false);
                }
            }
            return result;
        }

        public string PreviewMode()
        {
            return "";
        }
        public int TemplateID { get; set; }
        public byte LanguageID { get; set; }
        public int ThemeID { get; set; }
        public TimeSpan GetCountDownSpan(SiteInformation site, Contest contest = null)
        {
            TimeSpan span;
            if (contest != null)
            {
                if (contest.CandiadateStartDate == null)
                {
                    span = new TimeSpan(0, 0, 0);
                }
                else
                {
                    span = contest.CandiadateStartDate.Value > DateTime.Now ? (contest.CandiadateStartDate.Value - DateTime.Now) : new TimeSpan(0, 0, 0);
                }
            }
            else
            {
                if (SiteType == Enums.SiteType.Campaign)
                {
                    if (site.Campaign.CandiadateStartDate == null)
                    {
                        span = new TimeSpan(0, 0, 0);
                    }
                    else
                    {
                        span = site.Campaign.CandiadateStartDate.Value > DateTime.Now ? (site.Campaign.CandiadateStartDate.Value - DateTime.Now) : new TimeSpan(0, 0, 0);
                    }
                }
                else
                {
                    if (site.StartDate == null)
                    {
                        span = new TimeSpan(0, 0, 0);
                    }
                    else
                    {
                        span = site.StartDate.Value > DateTime.Now ? (site.StartDate.Value - DateTime.Now) : new TimeSpan(0, 0, 0);
                    }
                }
            }

            return span;
        }
        public int GetContestID()
        {
            int result = this.CampaignID;
            if (this.ApplicationID > 0 && this.SiteContestID > 0)
            {
                result = this.SiteContestID;
            }
            return result;
        }
    }
}
