﻿using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Masnet3.Common.Utilities
{
    public class EmailUtilities
    {
        public static string ExternalURL(string URL, int editSiteId = -1, int editCampaignId = -1, bool appendURL = true)
        {

            string CustomerIFrame = Commons.GetConfig("CustomerIFrame");
            if (editSiteId != -1 && editCampaignId != -1)
                CustomerIFrame = Commons.GetConfig("CustomerIFrame", editSiteId, editCampaignId);

            if (!string.IsNullOrWhiteSpace(CustomerIFrame))
            {
                if (appendURL && URL != "")
                {
                    return String.Format("{0}{1}{2}", CustomerIFrame, (CustomerIFrame.Contains("?") ? "&" : "?"), "re=" + HttpUtility.UrlEncode(URL));
                }
                else
                {
                    return CustomerIFrame;
                }
            }
            return URL;

        }

        public static IRestResponse GetValidate(string email)
        {
            RestClient client = new RestClient();
            client.BaseUrl = new Uri("https://api.mailgun.net/v3");
            client.Authenticator = new HttpBasicAuthenticator("api", "pubkey-e12d0c8361d1ae76ff82038ddeaad17c");
            RestRequest request = new RestRequest();
            request.Resource = "/address/validate";
            request.AddParameter("address", email);
            request.AddParameter("mailbox_verification", true);
            var response = new RestResponse();
            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;

            }).Wait();
            return response;
        }

        public static IRestResponse GetValidateByKickbox(string email)
        {
            //string apiKey = string.IsNullOrEmpty(Commons.GetConfigFromAppsettings().Kickbox_APIKey) ? "63831417a5cbd5778d5c54d82d3f73f32353143e2b4d49ee71305eae82cbd966" : Commons.GetConfigFromAppsettings().Kickbox_APIKey;
            var client = new RestClient(string.Format("https://api.kickbox.io/v2/verify?timeout=6000&email={0}&apikey={1}", email, "63831417a5cbd5778d5c54d82d3f73f32353143e2b4d49ee71305eae82cbd966"));
            var request = new RestRequest(Method.GET);
            request.AddParameter("undefined", "{}", ParameterType.RequestBody);
            var response = new RestResponse();
            Task.Run(async () =>
            {
                response = await GetResponseContentAsync(client, request) as RestResponse;
            }).Wait();
            return response;

        }

        public static Task<IRestResponse> GetResponseContentAsync(RestClient theClient, RestRequest theRequest)
        {
            var tcs = new TaskCompletionSource<IRestResponse>();
            theClient.ExecuteAsync(theRequest, response => {
                tcs.SetResult(response);
            });
            return tcs.Task;
        }

    }
}
