﻿using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common.Statups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public class ActionUitilites
    {
        public static IEnumerable<MASNET.DataAccess.Action> GetActiveActions(Enums.SweepType sweeptype, int periodID, int? editSiteId = null, int? editCampaignId = null)
        {
            IEnumerable<MASNET.DataAccess.Action> result = new List<MASNET.DataAccess.Action>();
            if (!editSiteId.HasValue)
            {
                var site = GlobalVariables.SiteInformation;
                editSiteId = site.ApplicationID;
                editCampaignId = site.GetContestID();
            }

            string cacheName = "activeAction";
            string cacheKey = CacheUilities.GetCacheKeyByUseType(cacheName, editSiteId.GetValueOrDefault(), editCampaignId.GetValueOrDefault(), prefix: sweeptype.ToString());
            if (!CacheUilities.cacheManager.HashKey(cacheKey))
            {

                string sql = @"SELECT a.*
                    FROM Action a 
                    JOIN sweepstake s on s.[UserActionID] = a.[ActionID] " +
                        Commons.Join(new
                        {
                            LeftTable = "s",
                            Table = "SweepStake",
                            OutTable = "NewSweepStake",
                            IDField = "ID",
                            JoinFields = "UserActionID",
                            editCampaignID = editCampaignId,
                            editApplicationID = editSiteId,
                            editTemplateID = 0,
                            PeriodID = periodID
                        }) +
                        @" WHERE a.ShowHide = 1
	                    AND ISNULL(a.IsDeleted, 0) = 0
	                    AND a.ActionType = 0
	                    AND ISNULL(s.IsDisplay, 0) = 1 ";
                result = StartupStaticDI.repositoryBasic.GetItemsDynamicSql<MASNET.DataAccess.Action>(sql, new
                {
                    periodID = periodID,
                    SweepType = (int)sweeptype
                });
                CacheUilities.cacheManager.Set(cacheKey, result);
            }
            else
            {
                result = CacheUilities.cacheManager.Get<List<MASNET.DataAccess.Action>>(cacheKey);
            }

            return result;
        }

        public static int GetShareRegisterActionID(int ActionID)
        {
            int newActionID = (int)Enums.Action.ShareRegistration;
            if (ActionID == (int)Enums.Action.Facebook)
                newActionID = (int)Enums.Action.FacebookShareRegistration;
            else if (ActionID == (int)Enums.Action.Twitter)
                newActionID = (int)Enums.Action.TwitterShareRegistration;
            else if (ActionID == (int)Enums.Action.EmailShare)
                newActionID = (int)Enums.Action.EmailShareRegistration;

            return newActionID;
        }
    }
}
