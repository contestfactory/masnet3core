﻿using MASNET.DataAccess;
using Masnet3.Common.Statups;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public static class PeriodUilities
    {
        public static Period GetActivePeriod(int? ApplicationID = null, int? CampaignID = null, int? TemplateID = null, bool autoCreate = false, SiteInformation siteInformation = null)
        {

            Period result = new Period();
            var siteinfo = siteInformation == null ? GlobalVariables.SiteInformation : siteInformation;

            if (ApplicationID.GetValueOrDefault() == 0)
            {
                ApplicationID = siteinfo.ApplicationID;
            }
            if (CampaignID.GetValueOrDefault() == 0)
            {
                CampaignID = siteinfo.GetContestID();
            }

            string cacheName = "activePeriod";
            string cacheKey = CacheUilities.GetCacheKeyByUseType(cacheName, ApplicationID.GetValueOrDefault(), CampaignID.GetValueOrDefault(), prefix: "AP");
            if (!CacheUilities.cacheManager.HashKey(cacheKey))
            {
                var useType = Commons.GetUseType(ApplicationID, CampaignID);
                var useTypeTemplateID = Commons.GetUseType(ApplicationID, CampaignID, TemplateID);
                result = StartupStaticDI.periodRepository.GetActivePeriod(useType, useTypeTemplateID, ApplicationID.GetValueOrDefault(), CampaignID.GetValueOrDefault(), TemplateID.GetValueOrDefault(), autoCreate);
                CacheUilities.cacheManager.Set(cacheKey, result);
            }
            else
            {
                result = CacheUilities.cacheManager.Get<Period>(cacheKey);
            }
            return result;
        }
    }
}
