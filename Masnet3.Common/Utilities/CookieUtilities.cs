﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public class CookieUtilities
    {
        public static void Set(string cookieKey, string cookieValue, int? days = null)
        {
            CookieOptions option = new CookieOptions();
            if (days.HasValue)
            {
                option.Expires = DateTime.Now.AddDays(days.Value);
            }
            else
            {
                option.Expires = DateTime.Now.AddMinutes(1);
            }
            GlobalVariables.httpContextObj.HttpContext.Response.Cookies.Append(cookieKey, cookieValue, option);
        }

        public static void Remove(string key)
        {
            GlobalVariables.httpContextObj.HttpContext.Response.Cookies.Delete(key);
        }

        public static string GetCookie(string cookieKey)
        {
            if (GlobalVariables.httpContextObj.HttpContext != null && !string.IsNullOrWhiteSpace(cookieKey))
            {
                cookieKey = cookieKey.Trim().ToLower();
                return GlobalVariables.httpContextObj.HttpContext.Request.Cookies[cookieKey];
            }
            return null;
        }
    }
}
