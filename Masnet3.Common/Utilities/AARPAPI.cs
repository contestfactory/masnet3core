﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Masnet3.Common.Utilities
{
    public class AARPAPI
    {
        private string client_id = "0oad4tay3iEFwUCkC0h7";
        private string client_secret = "Qd4NmawsW2JNQ6_zLWLufny-Rg-iHxQDylJkDNDv";
        private string default_redirect_uri = "https://new.cfdevs.com/Masnet3Test/aarpdream2018/Customer/RegisterCallback";
        private string response_type = "code";
        private string default_state = "defaultState";
        public enum Scopes
        {
            offline_access,
            basic_user_info,
            basic_membership_info,
            basic_address,
            advanced_membership_info,
            advanced_user_info,
            advanced_address
        }        
        public class AccessToken
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public int expire_in { get; set; }
            public string scope{ get; set; }
            public string refresh_token { get; set; }
        }
        public class AARPUser
        {
            /// <summary>
            /// uid – a unique user ID, often expressed as a UUID with and without dashes, but sometimes can contain spaces, apostrophes  and underscore symbols
            /// </summary>
            public string uid { get; set; }

            public string firstName { get; set; }

            public string userName { get; set; }

            /// <summary>
            /// dateOfBirth - yyyy-mm-dd format
            /// </summary>
            public string dateOfBirth { get; set; }

            /// <summary>
            /// coiStatus – confirm opt-in status, i.e. would you like us to send you emails, values:
            /// <para>C - confirmed, globally opted-in for new letter subscription</para>
            /// <para>P - pending</para>
            /// <para>O - opted out, globally opted out from any non-transactional emails</para>
            /// <para>N - not opted-in, but signed up for select emails</para>
            /// </summary>
            public string coiStatus{ get; set; }

            /// <summary>
            /// membershipStatus
            ///<para>ACTIVE - active</para>
            ///<para>CANCEL - cancelled</para>
            ///<para>DECEASED_SUSPEND – suspended, member is deceased</para>
            ///<para>EXPIRE – expired, user has not paid to renew</para>
            ///<para>MEMBER_REQUEST_SUSPEND – member requested suspension</para>
            ///<para>UNDELIVERABLE_SUSPEND – suspended due to mail delivery exceptions</para>
            /// </summary>
            public string membershipStatus { get; set; }
            
            /// <summary>
            /// membershipExpirationDate - yyyy-mm-dd format
            /// </summary>
            public string membershipExpirationDate { get; set; }

            public string city { get; set; }

            /// <summary>
            /// state - a two character US state or Canadian province abbreviation
            /// </summary>
            public string state { get; set; }

            /// <summary>
            /// zip – a string of up to 10 characters containing US or international postal code
            /// </summary>
            public string zip { get; set; }

            /// <summary>
            /// country – a two characterISO 3166-1 country code
            /// </summary>
            public string country { get; set; }
            public string productName { get; set; }
            public string productType { get; set; }
            
            /// <summary>
            /// membershipType
            ///<para>Full</para>
            ///<para>Associate</para>
            ///<para>Fellowship</para>
            ///<para>Institution</para>
            /// </summary>
            public string membershipType { get; set; }
            public string membershipNumber { get; set; }
            public string email { get; set; }
            public string phone { get; set; }
            public string streetAddress { get; set; }
        }
        public class AARPUserResult
        {
            public string status { get; set; }
            public AARPUser user { get; set; }
        }

        public bool isLiveMode {
            get {
                bool result = false;
                //if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_LiveMode"]))
                //    result = System.Configuration.ConfigurationManager.AppSettings["AARP_LiveMode"] == "1";
                return result;
            } set { }
        }
        public string responseType { get { return this.response_type; } set { } }
        public string clientID { get { return this.client_id; } set { } }
        public string clientSecret { get { return this.client_secret; }  set { } }

        public string redirectUri { get; set; }
        public List<Scopes> scopes { get; set; }
        public string scope { 
            get{
                string result = "";
                if (this.scopes != null && this.scopes.Count > 0)
                {
                    result = string.Join("+", this.scopes.Select(x => x.ToString()));
                }
                return result;
            } 
        }
        public string state { get; set; }

        public string authorizationCode
        {
            get { 
                string result =  "";
                return result;
            }
            set { }
        }

        private string apiKey
        {
            get
            {
                string result = "";
                //if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_APIKey"]))
                //    result = System.Configuration.ConfigurationManager.AppSettings["AARP_APIKey"];
                return result;
            }
            set { }
        }

        private string accessTokenUrl
        {
            get
            {
                string result = "";
                //if (this.isLiveMode)
                //{
                //    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_AccessTokenUrlOnLive"]))
                //        result = System.Configuration.ConfigurationManager.AppSettings["AARP_AccessTokenUrlOnLive"];
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_AccessTokenUrlOnDev"]))
                //        result = System.Configuration.ConfigurationManager.AppSettings["AARP_AccessTokenUrlOnDev"];
                //}
                return result;
            }
            set { }
        }

        private string loginUrl
        {
            get
            {
                string result = "";
                //if (this.isLiveMode)
                //{
                //    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_LoginUrlOnLive"]))
                //        result = System.Configuration.ConfigurationManager.AppSettings["AARP_LoginUrlOnLive"];
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_LoginUrlOnDev"]))
                //        result = System.Configuration.ConfigurationManager.AppSettings["AARP_LoginUrlOnDev"];
                //}
                return result;
            }
            set { }
        }

        private string serviceUrl
        {
            get
            {
                string result = "";
                //if (this.isLiveMode)
                //{
                //    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_ServiceUrlLive"]))
                //        result = System.Configuration.ConfigurationManager.AppSettings["AARP_ServiceUrlLive"];
                //}
                //else
                //{
                //    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["AARP_ServiceUrlDev"]))
                //        result = System.Configuration.ConfigurationManager.AppSettings["AARP_ServiceUrlDev"];
                //}
                return result;
            }
            set { }
        }
        string getAccessTokenUrl(string authorizationCode)
        {
            string result = "";
            string url = string.Format(this.accessTokenUrl, this.apiKey);
            string query = string.Format(@"grant_type=authorization_code&redirect_uri={0}&code={1}&client_id={2}&client_secret={3}",
                                            System.Web.HttpUtility.UrlEncode(this.redirectUri),
                                            authorizationCode,
                                            this.clientID,
                                            this.clientSecret);

            result = string.Format("{0}?{1}", url, query);
            return result;
        }

        public AARPAPI()
        {
            //this.isLiveMode = false;
            this.scopes = new List<Scopes>() { Scopes.basic_user_info, Scopes.basic_membership_info};
            this.redirectUri = default_redirect_uri;
        }

        public AARPAPI(string redirect_uri)
        {
            //this.isLiveMode = false;
            this.scopes = new List<Scopes>() { Scopes.basic_user_info, Scopes.basic_membership_info };
            this.redirectUri = redirect_uri;
        }

        //public void saveAuthorizationCode(HttpContext httpContext, string code)
        //{
        //    if (httpContext != null) httpContext.Session["AARP_AuthorizationCode"] = code;
        //}

        //public string getAuthorizationCode(HttpContext httpContext)
        //{
        //    string result = "";
        //    if (httpContext != null) result = httpContext.Session["AARP_AuthorizationCode"].ToString();
        //    return result;
        //}

        public string getAARPLoginUrl() {

            string query = string.Format(@"response_type={0}&client_id={1}&redirect_uri={2}&scope={3}&state={4}",
                                            this.responseType,
                                            this.clientID,
                                            System.Web.HttpUtility.UrlEncode(this.redirectUri),
                                            this.scope,
                                            this.getState());
            return string.Format("{0}?{1}", this.loginUrl, query);
        }

        public string getState()
        {
            return string.IsNullOrEmpty(this.state) ? default_state : this.state;
        }

        private AccessToken getAccessToken(string authorizationCode)
        {
            AccessToken result = null;
            WebHeaderCollection headers = new WebHeaderCollection();
            headers["Accept"] = "application/json"; // restricted header name
            headers["Content-Type"] = "application/x-www-form-urlencoded";// restricted header name
            headers["Cache-Control"] = "no-cache";
            string url = getAccessTokenUrl(authorizationCode);
            //LogHelper.WriteLog(string.Format("getAccessToken URL: {0}", url));
            //using (var response = RequestUtilities.MakeRequest(url, "POST", headers))
            //{
            //    using (var stream = new StreamReader(response.GetResponseStream()))
            //    {
            //        string data = stream.ReadToEnd();
            //        LogHelper.WriteLog(string.Format("getAccessToken Result: {0}", data));
            //        result = Newtonsoft.Json.JsonConvert.DeserializeObject<AccessToken>(data);
            //    }
            //}
            return result;
        }

        public AARPUserResult getUserInfo(string authorizationCode)
        {
            AARPUserResult result = new AARPUserResult()
            {
                status = "",
                user = new AARPUser()
            };


            var accessToken = getAccessToken(authorizationCode);
            //if(accessToken!= null)
            //{
            //    var js = new JavaScriptSerializer();
            //    WebHeaderCollection headers = new WebHeaderCollection();
            //    headers["Accept"] = "application/json";// restricted header name
            //    headers["Cache-Control"] = "no-cache";
            //    headers["Authorization"] = string.Format("{0} {1}", accessToken.token_type, accessToken.access_token);
            //    LogHelper.WriteLog(string.Format("getUserInfo Url: {0}", this.serviceUrl));
            //    LogHelper.WriteLog(string.Format("Header Authorization: {0}", headers["Authorization"]));
            //    using (var response = RequestUtilities.MakeRequest(this.serviceUrl, "GET", headers))
            //    {                    
            //        using (var stream = new StreamReader(response.GetResponseStream()))
            //        {
            //            string data = stream.ReadToEnd();
            //            LogHelper.WriteLog(string.Format("getUserInfo Result: {0}", data));
            //            result = js.Deserialize<AARPUserResult>(data);
            //        }
            //    }
            //}
            return result;
        }

    }
}
