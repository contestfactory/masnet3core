﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository.ExtensionFields;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public static class ExtensionUilities
    {
        public static string GetRequiredString(this RouteData routeData, string keyName)
        {
            object value;
            if (!routeData.Values.TryGetValue(keyName, out value))
            {
                return string.Empty;
            }

            return value?.ToString();
        }

        public static ExtensionField GetExtensionField(IExtensionFieldRepository extensionFieldRepository, int? applicationID = null, int? contestID = null, int? templateID = null, SiteInformation siteInfo = null, bool isReload = false)
        {
            if (siteInfo == null)
            {
                siteInfo = GlobalVariables.SiteInformation;
            }
            int editApplicationID = applicationID ?? siteInfo.ApplicationID;
            int editCampaignID = contestID ?? siteInfo.CampaignID;
            int editTemplateID = templateID ?? 0;

            string cacheKey = CacheUilities.GetCacheKeyByUseType("", editApplicationID, editCampaignID, editTemplateID, "EF");
            Enums.UseType useType = Commons.GetUseType(editApplicationID, editCampaignID, editTemplateID);
            bool allowCache = useType == Enums.UseType.Site
                || useType == Enums.UseType.SiteContest
                || useType == Enums.UseType.Campaign;
            ExtensionField extensionField = CacheUilities.cacheManager.Get<ExtensionField>(cacheKey);
            if (isReload || !CacheUilities.cacheManager.HashKey(cacheKey))
            {
                string filter = FilterUilities.GetFilter("ApplicationID", "ContestID", "TemplateID", editApplicationID, editCampaignID, editTemplateID, "", true);
                extensionField = extensionFieldRepository.GetExtensionFieldWithDisplayItem(filter, (int)useType, editApplicationID, editCampaignID, editTemplateID);
                if (allowCache) CacheUilities.cacheManager.Set(cacheKey, extensionField);
            }
            return extensionField;
        }
    }
}
