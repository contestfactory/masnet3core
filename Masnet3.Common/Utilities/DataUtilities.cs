﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public static class DataUtilities
    {
        public static int ToInt(this object obj)
        {
            return Convert.ToInt32(obj);
        }

        public static int ToIntOrDefault(this object obj, int defaulValue = 0)
        {
            int result = 0;
            obj = obj ?? "";

            if (int.TryParse(obj.ToString(), out result))
                return result;
            else
                return defaulValue;
        }
    }
}
