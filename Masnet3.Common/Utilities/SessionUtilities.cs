﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Common.Statups;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Routing;
using System.Linq;
using Masnet3.Lib;

namespace Masnet3.Common.Utilities
{
    public static class SessionUtilities
    {

        public static string PageName(ViewContext ViewContext = null)
        {
            var requestContext = GlobalVariables.httpContextObj.HttpContext;
            if (ViewContext != null)
            {
                requestContext = ViewContext.HttpContext;
            }

            return requestContext.GetRouteData().Values["Controller"].ToString().ToLower() + "/" + requestContext.GetRouteData().Values["Action"].ToString().ToLower();
        }

        public static void SetStringObject(this ISession session, string key, string value)
        {
            session.SetString(key, value);
        }

        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        // User - sPermission
        public static bool IsLoggedIn()
        {
            if(CurrentUser != null)
            {
                if(CurrentUser.UserID > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static string AdminUserToken
        {
            get
            {
                if (!IsLoggedIn())
                {
                    return string.Empty;
                }
                return GlobalVariables.httpContextObj.HttpContext.Session.Get<string>(AreaName() + "AdminUserToken");
            }
            set
            {
                GlobalVariables.httpContextObj.HttpContext.Session.Set(AreaName() + "AdminUserToken", value);
            }
        }

        public static bool IsLoggedIn(LoggedUser currentUser)
        {
            if (currentUser != null)
            {
                if (currentUser.UserID > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static LoggedUser CurrentUser
        {
            get
            {
                if (GlobalVariables.httpContextObj.HttpContext == null)
                {
                    return new LoggedUser();
                }
                SiteInformation siteInfo = GlobalVariables.SiteInformation;
                string key = "CurrentUser";
                if (SuperAdminAccount != null && SuperAdminAccount.UserID > 0)
                {
                    key = "CurrentUser";
                }
                else
                {
                    switch (siteInfo.SiteType)
                    {
                        case Enums.SiteType.Campaign:
                            key = string.Format("CurrentUser_Campaign_{0}", siteInfo.CampaignID);
                            break;
                        case Enums.SiteType.Customer:
                            key = string.Format("CurrentUser_Customer_{0}", siteInfo.Application.ApplicationName);
                            break;
                        case Enums.SiteType.Main:
                            key = "CurrentUser";
                            break;
                    }
                }
                var userLogIn = GlobalVariables.httpContextObj.HttpContext.Session.Get<LoggedUser>(key);
                if (userLogIn == null)
                {
                    GlobalVariables.httpContextObj.HttpContext.Session.Set<LoggedUser>(key, new LoggedUser());
                }
                return userLogIn;
            }
            set
            {
                SiteInformation siteInfo = GlobalVariables.SiteInformation;
                string key = "CurrentUser";
                switch (siteInfo.SiteType)
                {
                    case Enums.SiteType.Campaign:
                        key = string.Format("CurrentUser_Campaign_{0}", siteInfo.CampaignID);
                        break;
                    case Enums.SiteType.Customer:
                        key = string.Format("CurrentUser_Customer_{0}", siteInfo.Application.ApplicationName);
                        break;
                    case Enums.SiteType.Main:
                        key = "CurrentUser";
                        break;
                }

                if (value != null && value.UserID > 0 && value.UserType == (int)Enums.RegistrationUserType.SuperAdmin)
                {
                    key = "CurrentUser";
                    SuperAdminAccount = value;
                }
                else
                {
                    SuperAdminAccount = new LoggedUser();
                }

                GlobalVariables.httpContextObj.HttpContext.Session.Set<LoggedUser>(key, value);
            }
        }

        public static LoggedUser SuperAdminAccount
        {
            get
            {
                var userLogIn = GlobalVariables.httpContextObj.HttpContext.Session.Get<LoggedUser>("SuperAdminAccount");
                if (userLogIn == null)
                {
                    userLogIn = new LoggedUser();
                    GlobalVariables.httpContextObj.HttpContext.Session.Set<LoggedUser>("SuperAdminAccount", new LoggedUser());
                }

                return userLogIn;
            }
            set
            {
                GlobalVariables.httpContextObj.HttpContext.Session.Set<LoggedUser>("SuperAdminAccount", value);
            }
        }

        public static bool IsSuperAdmin()
        {
            return (SuperAdminAccount != null && SuperAdminAccount.UserID > 0);
        }

        public static bool IsSuperAdmin(LoggedUser currentAdmin)
        {
            return (currentAdmin != null && currentAdmin.UserID > 0);
        }

        public static bool IsUserHasAdminPermission()
        {
            if (IsLoggedIn())
            {
                return !string.IsNullOrWhiteSpace(UserPermission);
            }
            return false;
        }

        public static bool IsJudge()
        {
            return IsLoggedIn() && (CurrentUser.UserType == (int)Enums.RegistrationUserType.Judge);
        }

        public static string UserPermission
        {
            get
            {
                SiteInformation siteInfo = GlobalVariables.SiteInformation;
                string sessionKey = string.Format("{0}_{1}_UserPermission", siteInfo.ApplicationID, siteInfo.CampaignID);
                string userPermission = GlobalVariables.httpContextObj.HttpContext.Session.Get<string>(sessionKey);
                string sesionUserLogin = string.Empty;
                if (string.IsNullOrEmpty(userPermission))
                {
                    if (IsLoggedIn())
                    {
                        sesionUserLogin = getUserPermission();
                        GlobalVariables.httpContextObj.HttpContext.Session.SetString(sessionKey, sesionUserLogin);
                        SetPermissionsForCurrentUser();
                    }
                    else
                    {
                        sesionUserLogin = string.Empty;
                        GlobalVariables.httpContextObj.HttpContext.Session.SetString(sessionKey, string.Empty);
                    }
                }
                else
                {
                    if (!IsLoggedIn())
                    {
                        sesionUserLogin = string.Empty;
                        GlobalVariables.httpContextObj.HttpContext.Session.SetString(sessionKey, string.Empty);
                    }
                }

                return sesionUserLogin;
            }

            set
            {
                SiteInformation siteInfo = GlobalVariables.SiteInformation;
                string sessionKey = string.Format("{0}_{1}_UserPermission", siteInfo.ApplicationID, siteInfo.CampaignID);
                GlobalVariables.httpContextObj.HttpContext.Session.SetString(sessionKey, value);
            }

        }

        public static string getUserPermission()
        {
            if (IsLoggedIn())
            {
                string strPermission = "";
                IEnumerable<string> UserPermissionList = StartupStaticDI.userRepository.GetUserPermissionList(CurrentUser.UserID);
                foreach (var item in UserPermissionList)
                {
                    if (string.IsNullOrEmpty(strPermission))
                    {
                        strPermission = item;
                    }
                    else
                    {
                        strPermission = strPermission + ";" + item;
                    }
                }
                return string.IsNullOrEmpty(strPermission) ? string.Empty : strPermission;
            }
            else
            {
                return null;
            }
        }

        private static void SetPermissionsForCurrentUser(ICollection<Permission> userPermissions = null)
        {
            bool needQueryPermission = true;
            List<Permission> permissions = null;
            if (IsLoggedIn())
            {
                bool isGetPermissionsByUser = false;
                if (CurrentUser.UserType != (int)Enums.RegistrationUserType.SuperAdmin)
                {
                    if (userPermissions == null)
                    {
                        isGetPermissionsByUser = true;
                        needQueryPermission = true;
                    }
                    else
                    {
                        needQueryPermission = false;
                        permissions = userPermissions.ToList();
                    }
                }
                if (needQueryPermission)
                {
                    permissions = StartupStaticDI.userRepository.GetPermissionBy(isGetPermissionsByUser, CurrentUser.UserID).ToList();
                }
                string strPermissions = "";
                if (permissions != null && permissions.Count > 0)
                {
                    strPermissions = ";";
                    bool hasPermission = true;
                    foreach (var item in permissions)
                    {
                        hasPermission = true;
                        if (SessionUtilities.CurrentUser.UserType == (int)Enums.RegistrationUserType.Customer)
                        {
                            if (item.PermissionID == (int)Enums.Permissions.Admin_Site_AdhocEmail)
                            {
                                hasPermission = false;
                            }
                        }
                        if (hasPermission)
                        {
                            strPermissions += item.PermissionID + ";";
                        }
                    }
                }
                UserPermission = strPermissions;
            }
        }

        public static bool HasPermission(Enums.Permissions permissionID)
        {

            if (UserPermission.ToString().IndexOf(";" + permissionID.ToInt().ToString() + ";") != -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsTwoFactor()
        {
            return Commons.Config["Login_TwoFA"] == "1";
        }

        public static bool IsPassedTwoFactor()
        {
            return (CurrentUser.TwoFactorVirified || Commons.Config["Login_TwoFA"] != "1");
        }

        public static bool IsPassedTwoFactor( string  isTwoFactor)
        {
            return (CurrentUser.TwoFactorVirified || isTwoFactor != "1");
        }

        public static void SetUserTokenInCookie(string token)
        {
            if (!string.IsNullOrEmpty(token))
            {
                try
                {
                    int days = 30;
                    string cipherToken = AESEncryption.EncryptToBase64(token);
                    CookieUtilities.Set(Enums.CookieType.Token.ToString(), cipherToken, days);
                }
                catch (Exception ex) { }
            }

        }

        // helper
        public static int CurrentLanguageID
        {
            get
            {
                string sessionKey = string.Format("Session_CurrentLanguageID_{0}", GlobalVariables.SiteInformation.CampaignID);
                string cookie = CookieUtilities.GetCookie(sessionKey);
                int result = string.IsNullOrEmpty(cookie) == false ? Int32.Parse(cookie) : GlobalVariables.SiteInformation.LanguageID;
                return result;
            }
            set
            {
                string sessionKey = string.Format("Session_CurrentLanguageID_{0}", GlobalVariables.SiteInformation.CampaignID);
                CookieUtilities.Set(sessionKey, value.ToString());
            }
        }

        public static int CurrentApplicationID
        {
            get
            {
                if (AreaName() != "")
                {
                    string sessionKey = AreaName() + "CurrentApplicationID";
                    var httpContext = GlobalVariables.httpContextObj.HttpContext;
                    if (httpContext != null && httpContext.Session != null)
                    {
                        if (httpContext.Session.Get<dynamic>(sessionKey) == null)
                        {
                            var app = StartupStaticDI.applicationRepository.GetApplicationByName(AreaName());
                            if (app != null && app.ApplicationID > 0)
                            {
                                httpContext.Session.Set(sessionKey, app.ApplicationID);
                                return app.ApplicationID;
                            }
                            return GlobalVariables.DefaultApplicationID;
                        }
                        else
                        {
                            return httpContext.Session.Get<int>(sessionKey);
                        }
                    }
                    else
                    {
                        return GlobalVariables.DefaultApplicationID;
                    }
                }
                else
                    return GlobalVariables.DefaultApplicationID;
            }
        }

        public static string AreaName()
        {
            return "mas";
        }
        public static Enums.ProductMode? ProductMode
        {
            get
            {
                var superAdminProductMode = CookieUtilities.GetCookie("SuperAdminProductMode");
                string value = superAdminProductMode == null ? null : superAdminProductMode;
                if (string.IsNullOrWhiteSpace(value))
                {
                    return null;
                }

                return (Enums.ProductMode?)Enum.Parse(typeof(Enums.ProductMode), value);
            }
            set
            {
                CookieUtilities.Set("SuperAdminProductMode", (value.HasValue ? value.ToString() : ""));
            }
        }

    }
}
