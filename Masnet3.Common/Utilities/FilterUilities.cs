﻿using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Utilities
{
    public class FilterUilities
    {
        public static string GetFilter(int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1)
        {
            return GetFilter("ApplicationID", "ContestID", "TemplateID", editApplicationID, editCampaignID, editTemplateID);
        }
        public static string GetFilter(int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1, bool useHidded = false)
        {
            return GetFilter("ApplicationID", "ContestID", "TemplateID", editApplicationID, editCampaignID, editTemplateID, "", useHidded);
        }
        public static string GetFilter(string applicationField, string campaignField, string templateField, int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1, string prefixForUseType = "", bool useHidded = false)
        {
            string where = "";
            Enums.UseType UserType = Commons.GetUseType(editApplicationID, editCampaignID, editTemplateID);

            if (UserType.Equals((Enums.UseType.Global)))
                where = string.Format("AND {1}UseType = {0}", (int)UserType, prefixForUseType);

            if (UserType.Equals(Enums.UseType.Site) || UserType.Equals(Enums.UseType.ApplicationTemplate))
            {
                where = string.Format("AND {3}UseType = {0} AND {1} = {2}", (int)UserType, applicationField, editApplicationID, prefixForUseType);
            }

            if (UserType.Equals(Enums.UseType.Campaign))
            {
                if (!useHidded)
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} AND {4}ShowHide <> {3} ", (int)UserType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
                else
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} ", (int)UserType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
            }

            if (UserType.Equals(Enums.UseType.SiteContest))
            {
                if (!useHidded)
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} AND {4}ShowHide <> {3} ", (int)UserType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
                else
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} ", (int)UserType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
            }

            if (UserType.Equals((Enums.UseType.ContestTemplate)))
                where = string.Format("AND {3}UseType = {0} AND {1} = {2}", (int)UserType, templateField, editTemplateID, prefixForUseType);

            return where;
        }
    }
}
