﻿using System;
using System.Collections.Generic;
using System.Linq;
using Masnet3.Common.Utilities;
using Microsoft.AspNetCore.Http;
using Masnet3.Common.Caching;
using Microsoft.Extensions.Caching.Memory;
using MASNET.DataAccess;
using Masnet3.Common;
using Masnet3.Business.Repository.Contests;
using Microsoft.AspNetCore.Routing;
using Masnet3.Business.Repository.Applications;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository.Layout;
using Masnet3.Common.Statups;
using Microsoft.AspNetCore.Mvc.Rendering;
using Masnet3.Common.Helper;

namespace Masnet3.Common.Utilities
{
    public class GlobalVariables
    {
        public static int DefaultCountryID = 231;
        public static int DefaultApplicationID = 0;
        public static int DefaultTenantID = 1;

        static public HttpContextAccessor httpContextObj
        {
            get
            {
                return new HttpContextAccessor();
            }
        }
        
        public static string SeparatorTemplateToken = "%";
        public static SiteInformation SiteInformation
        {
            get
            {
                try
                {
                    if (httpContextObj.HttpContext == null)
                    {
                        return new SiteInformation();
                    }
                    RouteData routeData = httpContextObj.HttpContext.GetRouteData();
                    string keySite = string.Format("SiteInformation_{0}", routeData.Values["SiteName"].ToString().Trim().ToLower());
                    var gObjCache = CacheUilities.cacheManager.Get<SiteInformation>(keySite);
                    if (gObjCache != null)
                    {
                        SiteInformation siteInfo = gObjCache;
                        int EditcampaignID = 0;
                        //campaign status                
                        if (siteInfo.SiteType.Equals(Enums.SiteType.Campaign))
                        {
                            MinifiedContest campaign = siteInfo.Campaign;
                            siteInfo.SiteStatus = GetSiteStatus(campaign);
                        }

                        //Application status
                        if (siteInfo.SiteType.Equals(Enums.SiteType.Customer))
                        {
                            MinifiedApplication campaign = siteInfo.Application;
                            siteInfo.SiteStatus = GetSiteStatus(null, campaign);
                            // check action is contest/match or contest/submit
                            siteInfo.SiteContestID = GetSitecontestID(routeData, httpContextObj.HttpContext);
                        }

                        //Update Mode                        
                        if (routeData.Values.Keys.Contains("iframe"))
                        {
                            string iFrame = routeData.Values["iframe"].ToString().Trim().ToLower();
                            siteInfo.IFrameMode = (iFrame == "iframe" || iFrame == "true");
                        }
                        else
                        {
                            siteInfo.IFrameMode = routeData.Values["SiteName"].ToString().ToLower().StartsWith("iframe_");
                        }

                        //Update Timezone ???                        
                        if (!string.IsNullOrWhiteSpace((string)httpContextObj.HttpContext.Items["editCampaignId"]))
                        {
                            int.TryParse((string)httpContextObj.HttpContext.Items["editCampaignId"].ToString().Trim(), out EditcampaignID);
                        }
                        if (EditcampaignID > 0)
                        {
                            var EditCampaign = StartupCommon.DicContests.Where(c => c.Value.ContestID == EditcampaignID).FirstOrDefault().Value;
                            if (EditCampaign != null && EditCampaign.ContestID > 0)
                            {
                                siteInfo.TimeZoneID = EditCampaign.TimeZoneID.GetValueOrDefault((int)Enums.TimeZone.PST);
                            }
                        }
                        else
                        {
                            siteInfo.TimeZoneID = siteInfo.SiteType.Equals(Enums.SiteType.Campaign) ? siteInfo.Campaign.TimeZoneID.GetValueOrDefault((int)Enums.TimeZone.PST) : siteInfo.Application.TimeZoneID.GetValueOrDefault((int)Enums.TimeZone.PST);
                        }
                        CacheUilities.cacheManager.Set(keySite, siteInfo);
                        return siteInfo;
                    }
                    else
                    {

                        SiteInformation siteInfo = new SiteInformation {
                            SiteStatus = Enums.SiteStatus.Valid,
                            TimeZoneID = (int)Enums.TimeZone.PST
                        };
                        string siteName = string.Empty;
                        int campaignID = 0;
                        int EditcampaignID = 0;
                        if (routeData != null)
                        {
                            if (routeData.Values.Keys.Contains("SiteName"))
                            {
                                siteName = routeData.Values["SiteName"].ToString().Trim().ToLower();

                                if (siteName.StartsWith("iframe_"))
                                {
                                    siteName = siteName.Substring(7);
                                }
                            }

                            if (!string.IsNullOrWhiteSpace((string)httpContextObj.HttpContext.Items["editCampaignId"]))
                            {
                                if (int.TryParse((string)httpContextObj.HttpContext.Items["editCampaignId"].ToString().Trim(), out EditcampaignID) == false)
                                {
                                    EditcampaignID = 0;
                                }
                                else
                                {
                                    if (EditcampaignID > 0)
                                    {
                                        var EditCampaign = Campaigns.Where(c => c.ContestID == EditcampaignID).FirstOrDefault();
                                        if (EditCampaign != null && EditCampaign.ContestID > 0)
                                        {
                                            siteInfo.TimeZoneID = EditCampaign.TimeZoneID ?? (int)Enums.TimeZone.PST;
                                        }
                                    }
                                }
                            }
                            if (routeData.Values.Keys.Contains("iframe"))
                            {
                                string iFrame = routeData.Values["iframe"].ToString().Trim().ToLower();
                                siteInfo.IFrameMode = (iFrame == "iframe" || iFrame == "true");
                            }
                            else
                            {
                                siteInfo.IFrameMode = routeData.Values["SiteName"].ToString().ToLower().StartsWith("iframe_");
                            }
                        }
                        if (string.IsNullOrWhiteSpace(siteName) && campaignID == 0)
                        {

                            MinifiedApplication application = StartupCommon.DicApplications["contestfactory"];
                            siteInfo.Application = application;
                            siteInfo.CampaignID = 0;
                            siteInfo.SiteType = Enums.SiteType.Main;
                            siteInfo.StartDate = application.StartDate;
                            siteInfo.EndDate = application.EndDate;
                            if (EditcampaignID == 0)
                                siteInfo.TimeZoneID = application.TimeZoneID ?? (int)Enums.TimeZone.PST;
                            siteInfo.ThemeID = application.ThemeID;

                        }
                        else
                        {

                            if (!string.IsNullOrWhiteSpace(siteName))
                            {
                                bool isApp = true;
                                MinifiedApplication application = new MinifiedApplication();
                                try { application = StartupCommon.DicApplications[siteName]; }
                                catch { isApp = false; }
                                if (isApp)
                                {

                                    if (application.ApplicationID > 0)
                                    {
                                        siteInfo.SiteType = Enums.SiteType.Customer;
                                    }
                                    else
                                    {
                                        siteInfo.SiteType = Enums.SiteType.Main;
                                    }
                                    if (application.Status != (int)Enums.ApplicationStatus.Published)
                                    {
                                        siteInfo.SiteStatus = Enums.SiteStatus.Unpublished;
                                    }
                                    else
                                    {

                                        if (application.ApplicationID > 0)
                                        {
                                            application.StartDate = application.StartDate ?? DateTime.Now;
                                            application.EndDate = application.EndDate ?? DateTime.Now;
                                            if (application.FeeId > 0
                                                && application.StartDate <= DateTime.Now
                                                && application.EndDate >= DateTime.Now
                                                && application.Status == (int)Enums.ApplicationStatus.Published
                                                && application.IsDisplay == true)
                                            {
                                                siteInfo.SiteType = Enums.SiteType.Customer;
                                            }
                                            else
                                            {
                                                siteInfo.SiteStatus = Enums.SiteStatus.Unpublished;
                                            }
                                        }
                                        else
                                        {
                                            siteInfo.SiteType = Enums.SiteType.Main;
                                        }
                                    }

                                    siteInfo.SiteStatus = GetSiteStatus(null, application);

                                    siteInfo.CampaignID = 0;
                                    siteInfo.Campaign = new MinifiedContest();
                                    siteInfo.Application = application;
                                    siteInfo.ApplicationID = application.ApplicationID;
                                    siteInfo.StartDate = application.StartDate;
                                    siteInfo.EndDate = application.EndDate;
                                    siteInfo.LanguageID = application.LanguageID.Equals(0) ? (byte)1 : application.LanguageID;
                                    if (EditcampaignID == 0)
                                        siteInfo.TimeZoneID = application.TimeZoneID ?? (int)Enums.TimeZone.PST;
                                }
                                else
                                {

                                    bool isCamp = true;
                                    MinifiedContest campaign = new MinifiedContest();
                                    try { campaign = StartupCommon.DicContests[siteName]; }
                                    catch { isCamp = false; };
                                    if (isCamp)
                                    {
                                        campaignID = campaign.ContestID;
                                        DateTime candiadateStartDate = campaign.CandiadateStartDate ?? DateTime.Now;
                                        DateTime endDate = campaign.EndDate ?? DateTime.Now;
                                        siteInfo.SiteType = Enums.SiteType.Campaign;
                                        if (campaign.IsDisplay == true)
                                        {
                                            if (candiadateStartDate <= DateTime.Now && endDate >= DateTime.Now)
                                            {
                                                siteInfo.SiteType = Enums.SiteType.Campaign;
                                            }
                                            else
                                            {
                                                if (endDate < DateTime.Now)
                                                {
                                                    if (!(campaign.Status == (byte)Enums.ContestStatuses.Over || campaign.Status == (byte)Enums.ContestStatuses.Completed))
                                                    {
                                                        //Campaigns.Clear();
                                                        //campaign = Campaigns.Where(c => c.ContestID == campaignID).FirstOrDefault();
                                                    }

                                                    if (campaign.Status == (byte)Enums.ContestStatuses.Over
                                                        || campaign.Status == (byte)Enums.ContestStatuses.Completed)
                                                    {
                                                        siteInfo.SiteStatus = Enums.SiteStatus.Over;
                                                    }
                                                    else
                                                    {
                                                        siteInfo.SiteStatus = Enums.SiteStatus.Unpublished;
                                                    }
                                                }
                                                else
                                                {
                                                    siteInfo.SiteStatus = Enums.SiteStatus.Unpublished;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            siteInfo.SiteStatus = Enums.SiteStatus.Hidden;
                                        }

                                        siteInfo.SiteStatus = GetSiteStatus(campaign);

                                        siteInfo.Application = new MinifiedApplication();
                                        siteInfo.CampaignID = campaignID;
                                        siteInfo.TemplateID = campaign.TemplateID ?? 0;
                                        siteInfo.Campaign = campaign;
                                        siteInfo.StartDate = campaign.CandiadateStartDate;
                                        siteInfo.EndDate = campaign.EndDate;
                                        siteInfo.LanguageID = campaign.LanguageID ?? 1;
                                        siteInfo.TimeZoneID = campaign.TimeZoneID ?? (int)Enums.TimeZone.PST;
                                    }
                                    else //not exist
                                    {

                                        siteInfo.Application = new MinifiedApplication();
                                        siteInfo.Campaign = new MinifiedContest();
                                        siteInfo.CampaignID = 0;
                                        siteInfo.SiteStatus = Enums.SiteStatus.InvalidSite;

                                        siteInfo.ApplicationID = 0;
                                        siteInfo.SiteType = Enums.SiteType.Main;
                                        siteInfo.LanguageID = 1;
                                    }
                                }
                            }
                            else  //empty domain
                            {

                                siteInfo.SiteStatus = Enums.SiteStatus.Hidden;
                                siteInfo.ApplicationID = 0;
                                siteInfo.SiteType = Enums.SiteType.Main;
                                siteInfo.LanguageID = 1;
                            }
                        }
                        siteInfo.SpanTime = Commons.GetSpanTime(siteInfo.TimeZoneID);
                        CacheUilities.cacheManager.Set(keySite, siteInfo);
                        if (siteInfo.Campaign == null)
                        {
                            siteInfo.Campaign = new MinifiedContest();
                        }
                        return siteInfo;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public static Enums.SiteStatus GetSiteStatus(MinifiedContest campaign = null, MinifiedApplication application = null)
        {
            Enums.SiteStatus result = Enums.SiteStatus.InvalidSite;
            try
            {
                DateTime? candiadateStartDate = null;
                DateTime? endDate = null;
                DateTime? expireDate = null;
                bool? isDisplay = null;
                int applicationID = application != null ? application.ApplicationID : 0;
                int contestID = campaign != null ? campaign.ContestID : 0;

                bool isCamp = campaign != null;

                if (isCamp)
                {
                    candiadateStartDate = campaign.CandiadateStartDate ?? DateTime.Now;
                    endDate = campaign.EndDate ?? DateTime.Now;
                    expireDate = campaign.ExpireDate;
                    isDisplay = campaign.IsDisplay;
                }
                else
                {
                    if (application != null)
                    {
                        candiadateStartDate = application.StartDate ?? DateTime.Now;
                        endDate = application.EndDate ?? DateTime.Now;
                        expireDate = application.ExpireDate;
                        isDisplay = application.IsDisplay;
                    }
                }

                if (isDisplay == true)
                {
                    if (candiadateStartDate <= DateTime.Now && endDate >= DateTime.Now)
                    {
                        result = Enums.SiteStatus.Valid;
                    }
                    else
                    {
                        if (endDate < DateTime.Now)
                        {
                            result = Enums.SiteStatus.Over;
                            if (expireDate.HasValue)
                            {
                                if (expireDate.Value <= DateTime.Now)
                                {
                                    // expired
                                    result = Enums.SiteStatus.Expired;
                                }
                            }
                        }
                        else
                        {
                            result = Enums.SiteStatus.Unpublished;
                        }
                    }
                }
                else
                {
                    result = Enums.SiteStatus.Hidden;
                }
            }
            catch (Exception ex)
            {
                // do not thing
            }
            return result;
        }

        static private int GetSitecontestID(RouteData routeData, HttpContext requestContext = null)
        {

            int result = 0;
            string action = routeData.Values["Action"].ToString().ToLower();
            string controller = routeData.Values["Controller"].ToString().ToLower();
            string[] listActions = new string[] { "contest/match", "contest/submit", "publishedcontest/contestantentrydetail" };
            string currentAction = string.Format("{0}/{1}", controller, action);
            string requestContestID = "0";
            if (!string.IsNullOrEmpty((string)requestContext.Items["contestid"]))
            {
                requestContestID = (string)requestContext.Items["contestid"];
            }
            else if (string.IsNullOrEmpty((string)requestContext.Items["editCampaignId"]))
            {
                requestContestID = (string)requestContext.Items["editCampaignId"];
            }
            // using route id
            if (listActions.Contains(currentAction))
            {
                string id = (routeData.Values["id"] ?? "0").ToString();
                if (!string.IsNullOrEmpty(id))
                {
                    int.TryParse(id, out result);
                }
            }
            // using request parameters
            else if (!string.IsNullOrEmpty(requestContestID))
            {
                if (!string.IsNullOrEmpty(requestContestID))
                {
                    int.TryParse(requestContestID, out result);
                }
            }
            return result;
        }

        // Items
        public static List<Contest> Campaigns
        {
            get
            {
                string key = "GlobalVariables_Campaigns";
                List<Contest> campaigns = CacheUilities.cacheManager.Get<List<Contest>>(key);

                if (campaigns == null || campaigns.Count == 0)
                {
                    campaigns = StartupStaticDI.contestRepository.GetContestsByCreatedFrom((int)Enums.ContestCreatedFrom.Campaign).ToList();
                    CacheUilities.cacheManager.Set(key, campaigns);
                }
                return campaigns;
            }
        }

        public static Dictionary<int, Theme> Themes
        {
            get
            {
                string key = "GlobalVariables_Themes";
                var dic = CacheUilities.cacheManager.Get<Dictionary<int, Theme>>(key);
                if (dic == null)
                {
                    var _Themes = StartupStaticDI.themeRepository.GetListTheme();
                    dic = _Themes.ToDictionary(x => x.ID, x => x);
                    CacheUilities.cacheManager.Set(key, dic);
                }
                return dic;
            }
        }

        public static List<Language> Languages
        {
            get
            {
                string key = "GlobalVariables_Languages";
                List<Language> languages = CacheUilities.cacheManager.Get<List<Language>>(key);

                if (languages == null || languages.Count == 0)
                {
                    languages = StartupStaticDI.languageRepostirory.GetLanguages(true);
                    CacheUilities.cacheManager.Set(key, languages);
                }

                return languages;
            }
        }

        public static string PageName(ViewContext viewContext = null)
        {
            var httpContext = GlobalVariables.httpContextObj.HttpContext;
            if (viewContext != null)
            {
                httpContext = viewContext.HttpContext;
            }
            return httpContext.GetRouteData().Values["Controller"].ToString().ToLower() + "/" + httpContext.GetRouteData().Values["Action"].ToString().ToLower();
        }

        public static List<Country> Countries
        {
            get
            {
                string key = "GlobalVariables_Countries";
                List<Country> countries = CacheUilities.cacheManager.Get<List<Country>>(key);
                if (countries == null || countries.Count == 0)
                {
                    countries = StartupStaticDI.countryRepository.GetCountriesByLanguageID(1).ToList();
                    CacheUilities.cacheManager.Set(key, countries);
                }

                return countries;
            }
        }
        public static List<StateOfUSA> States
        {
            get
            {
                string key = "GlobalVariables_States";
                List<StateOfUSA> states = CacheUilities.cacheManager.Get<List<StateOfUSA>>(key);

                if (states == null || states.Count == 0)
                {
                    states = StartupStaticDI.countryRepository.GetStateOfUSA().ToList();
                    CacheUilities.cacheManager.Set(key, states);
                }

                return states;
            }
        }

        private static List<DisplayItem> _ProfileStandardFields = null;
        public static List<DisplayItem> ProfileStandardFields
        {
            get
            {
                if (_ProfileStandardFields == null)
                {
                    _ProfileStandardFields = new List<DisplayItem>();
                    string[] itemNames = "Username,Password,Email,FirstName,LastName,Birthday,Gender,CountryID,City,StateID,Provine,Zip,Bio".Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var item in itemNames)
                    {
                        _ProfileStandardFields.Add(new DisplayItem() { ItemName = item.Trim() });
                    }
                }

                return _ProfileStandardFields;
            }
        }

        public static string VirtualUploadDomain(string SubDomains)
        {
            string domain = "";
            string subDomains = "www.,new.,test.,apps-ugc.,demos.";
            if (!string.IsNullOrEmpty(SubDomains))
            {
                subDomains = SubDomains;
            }
            if (httpContextObj.HttpContext != null)
            {
                domain = httpContextObj.HttpContext.Request.Host.Host.RemoveStartAndEndWith(subDomains.Split(','));
            }

            return domain;
        }
        public static string VirtualUploadPath
        {
            get
            {
                var request = httpContextObj.HttpContext.Request;
                string uploadpath = request.PathBase.Value.ToLower() == "/" ? request.PathBase.Value : request.PathBase.Value.ToLower() + "/";
                string scheme = (RequestUtilities.GetAbsoluteUri(request).StartsWith("https://", StringComparison.OrdinalIgnoreCase) || request.Headers["HTTP_X_FORWARDED_PROTO"] == "https") ? "https://" : "http://";
                uploadpath = scheme + request.Host.Host + uploadpath + "/Media/";
                return uploadpath;
            }
        }

        public static string UploadFolder
        {
            get { return "upload"; }
        }

        public static string VirtualFolderPath
        {
            get { return Commons.GetConfig("VirtualFolderPath"); }
        }

        public static Dictionary<string, int[]> BannerPositions
        {
            get
            {
                Dictionary<string, int[]> _BannerPositions = CacheUilities.cacheManager.Get<Dictionary<string, int[]>>("BannerPositions");
                if (_BannerPositions == null || _BannerPositions.Keys.Count == 0)
                {
                    _BannerPositions = new Dictionary<string, int[]>();
                    _BannerPositions.Add("HeaderFull", new int[] { 740, 90 });
                    _BannerPositions.Add("HeaderHalf", new int[] { 365, 90 });
                    _BannerPositions.Add("RightSmall", new int[] { 300, 120 });
                    _BannerPositions.Add("RightNormal", new int[] { 300, 240 });
                    _BannerPositions.Add("RightLarge", new int[] { 300, 360 });
                    _BannerPositions.Add("RightXLarge", new int[] { 300, 480 });
                    _BannerPositions.Add("BottomFull", new int[] { 1010, 90 });
                    _BannerPositions.Add("BottomHalf", new int[] { 500, 90 });
                    //_BannerPositions.Add("HomeCenterStage", new int[] { 460, 300 });
                    CacheUilities.cacheManager.Set("BannerPositions", _BannerPositions);
                }

                return _BannerPositions;
            }
        }

        public static int SessionTimeout
        {
            get
            {
                try
                {
                    string value = Commons.GetConfigFromAppsettings().SessionTimeout;
                    int sessionTimeout = 0;
                    int.TryParse(value, out sessionTimeout);

                    if (sessionTimeout <= 0)
                    {
                        sessionTimeout = 10;
                    }
                    return sessionTimeout;
                }
                catch
                {
                    return 10;
                }
            }
        }

        public static int DefaultRoleID
        {
            get
            {
                int defaultRoleID = 1;

                int.TryParse(Commons.Config["DefaultRoleID"].ToString(), out defaultRoleID);

                return defaultRoleID;
            }
        }

        public static bool IsInPublishContest()
        {
            var httpContext = GlobalVariables.httpContextObj.HttpContext;
            var url = httpContext.Request.GetRawUrl().ToLower();
            string isinpublishedcontest = httpContext.Request.Query["isinpublishedcontest"];
            url = url.Replace(@"\", @"/");
            if (url.Contains(@"/publishedcontest/") || isinpublishedcontest != null)
            {
                return true;
            }
            return false;
        }

        public static int? CurrentPublishedContestID
        {
            get
            {
                int? contestID = null;
                var httpContext = GlobalVariables.httpContextObj.HttpContext;
                if (IsInPublishContest())
                {
                    RouteData route = httpContext.GetRouteData();
                    string ContestID = httpContext.Request.Query["ContestID"];
                    string Id = httpContext.Request.Query["id"];
                    if (ContestID != null)
                    {
                        contestID = int.Parse(ContestID);
                    }
                    else if (route.Values["ContestID"] != null)
                    {
                        contestID = int.Parse(route.Values["ContestID"].ToString());
                    }
                    else if (route.Values["ID"] != null)
                    {
                        contestID = int.Parse(route.Values["ID"].ToString());
                    }
                    else if (Id != null)
                    {
                        int n;
                        var isNumeric = int.TryParse(Id, out n);
                        if (isNumeric)
                            contestID = Convert.ToInt32(Id);
                    }
                }
                return contestID;
            }
        }

        public static string DefaultAvatar
        {
            get
            {
                return string.Format("/Content/MainSite/images/default-avatar.gif");
            }
        }

    }
}
