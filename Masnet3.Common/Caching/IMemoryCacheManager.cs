﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Common.Caching
{
    public interface IMemoryCacheManager: IDisposable
    {
        T Get<T>(string key);
        void Set(string key, object data, int cacheTime);
        void Remove(string key);
        bool HashKey(string key);
        List<string> GetAllKey();
        void Clear();
    }
}
