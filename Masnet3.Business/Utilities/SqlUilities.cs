﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Dapper;
using Masnet3.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;

namespace Masnet3.Business.Utilities
{
    internal class SqlUilities
    {

        public static string GetUnionTable(
            Enums.UseType useType,
            Dictionary<string, MinifiedApplication> dicApplications,
            Dictionary<string, MinifiedContest> dicContests,
            int contestTemplateID,
            string leftTable,
            string Join,
            int editApplicationID = -1,
            int editCampaignID = -1,
            int editTemplateID = -1,
            bool useHidded = false,
            string LanguageList = "")
        {
            string extraTable = "";
            string editLanguageID = LanguageList;
            //Join = [Private.PageID = B.PageID AND Private.ResourceKeyID = B.ResourceKeyID]

            if (useType.Equals((Enums.UseType.Global)))
                extraTable = string.Format(@String.Format("JOIN (SELECT ID FROM Resource WHERE UseType=1) NewResource ON {0}.ID = NewResource.ID ", leftTable));

            if (useType.Equals((Enums.UseType.Site)))
            {
                var app = dicApplications.Where(a => a.Value.ApplicationID == editApplicationID).First().Value;
                if (app != null && LanguageList == "") editLanguageID = app.LanguageID.ToString();

                extraTable = string.Format(
                    @"JOIN (SELECT CASE WHEN Private.ID > 0 THEN Private.ID ELSE Global.ID END ID
                    FROM (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=2 AND ApplicationID = {0} AND LanguageID IN ({3}) ) Private
                    RIGHT JOIN (SELECT ID, ResourceKey, PageID,LanguageID,UseType FROM Resource WHERE UseType=1 AND LanguageID IN ({3})) Global ON " + String.Format(Join, "Private", "Global") + @"                                                                                                      
                    ) NewResource ON {2}.ID = NewResource.ID                                
                    ", editApplicationID, (int)useType, leftTable, editLanguageID);
            }

            if (useType.Equals((Enums.UseType.Campaign)))
            {
                var app = dicContests.Where(c => c.Value.ContestID == editCampaignID).First().Value;
                if (LanguageList == "" && app != null) editLanguageID = app.LanguageID.GetValueOrDefault().ToString();

                extraTable = string.Format(
                    @"JOIN (SELECT CASE WHEN Private.ID > 0 THEN Private.ID WHEN TempResource.ID>0 THEN TempResource.ID ELSE Global.ID END ID
                    FROM (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=3 AND ContestID = {0} AND LanguageID IN ({4})) Private
                    RIGHT JOIN (SELECT ID, ResourceKey, PageID,LanguageID,UseType FROM Resource WHERE UseType = 1 AND  LanguageID IN ({4})) Global ON " + String.Format(Join, "Private", "Global") + @"  
                    LEFT JOIN (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=4 AND TemplateID = {1} AND LanguageID IN ({4})) TempResource ON " + String.Format(Join, "Global", "TempResource") + @"                                                                                                                                                                                
                    ) NewResource ON {3}.ID = NewResource.ID                                
                    ", editCampaignID, contestTemplateID, (int)useType, leftTable, editLanguageID);

            }

            if (useType.Equals((Enums.UseType.SiteContest)))
            {
                if (LanguageList == "" || LanguageList == "0")
                {
                    var app = dicApplications.Where(c => c.Value.ApplicationID == editApplicationID).First().Value;
                    if (app != null)
                        editLanguageID = app.LanguageID.ToString();
                    if (LanguageList == "")
                        LanguageList = "1";
                }

                extraTable = string.Format(
                @" JOIN (SELECT CASE WHEN Private.ID > 0 THEN Private.ID WHEN AppResource.ID>0 THEN AppResource.ID ELSE Global.ID END ID
                   FROM (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=5 AND UseType=5 AND ContestID = {0} AND LanguageID IN ({2}) ) Private
                   RIGHT JOIN (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType = 1 AND LanguageID IN ({2})) Global ON " + String.Format(Join, "Private", "Global") + @"                                                                           
                   LEFT JOIN (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=2 AND ApplicationID = {1} AND LanguageID IN ({2})) AppResource ON " + String.Format(Join, "Global", "AppResource") + @"                                                                                                                                                                                  
                   ) NewResource ON {3}.ID = NewResource.ID                               
                    ", editCampaignID, editApplicationID, editLanguageID, leftTable);
            }

            if (useType.Equals((Enums.UseType.ApplicationTemplate)))
                extraTable = string.Format(
                    @"JOIN (SELECT CASE WHEN Private.ID > 0 THEN Private.ID ELSE Global.ID END ID
                      FROM (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=6 AND ApplicationID = {0}  ) Private
                      RIGHT JOIN (SELECT ID, ResourceKey, PageID,LanguageID,UseType FROM Resource WHERE UseType=1 ) Global ON " + String.Format(Join, "Private", "Global") + @"                                                                                                      
                    ) NewResource ON {2}.ID = NewResource.ID                                
                    ", editApplicationID, (int)useType, leftTable);

            if (useType.Equals((Enums.UseType.ContestTemplate)))
                extraTable = string.Format(
                    @"JOIN (SELECT CASE WHEN Private.ID > 0 THEN Private.ID ELSE Global.ID END ID
                      FROM (SELECT ID, ResourceKey, PageID,LanguageID FROM Resource WHERE UseType=4 AND TemplateID = {0}  ) Private
                      RIGHT JOIN (SELECT ID, ResourceKey, PageID,LanguageID,UseType FROM Resource WHERE UseType=1 ) Global ON " + String.Format(Join, "Private", "Global") + @"                                                                                                      
                    ) NewResource ON {2}.ID = NewResource.ID                                
                    ", editTemplateID, (int)useType, leftTable);

            return extraTable;
        }

        public static string Join(IDbConnection conn, dynamic attributes, Enums.UseType useType)
        {
            Dictionary<string, string> dic = SqlUilities.ConvertToTokensDictionary(attributes, 1);
            string leftTable = dic["LeftTable"];
            string table = dic["Table"];        //table working on e.g. "Template"
            string outTable = "OutTable";       //out temp table e.g.   "NewTemplate"
            string IDField = dic["IDField"];    //ID field          
            string JoinFields = dic["JoinFields"]; string[] arr = JoinFields.Split(',');
            int editApplicationID = -1;
            int editCampaignID = -1;
            int editTemplateID = -1;
            bool useHidded = false;
            if (dic.ContainsKey("UseHidded")) Boolean.TryParse(dic["UseHidded"], out useHidded);
            if (dic.ContainsKey("editCampaignID")) Int32.TryParse(dic["editCampaignID"], out editCampaignID);
            if (dic.ContainsKey("editApplicationID")) Int32.TryParse(dic["editApplicationID"], out editApplicationID);
            if (dic.ContainsKey("editTemplateID")) Int32.TryParse(dic["editTemplateID"], out editTemplateID);
            if (dic.ContainsKey("OutTable")) outTable = dic["OutTable"];
            var TemplateIDField = table.Equals("Template", StringComparison.CurrentCultureIgnoreCase) ? "ContestTemplateID" : "TemplateID";
            var ContestIDField = table.Equals("CTAButton", StringComparison.CurrentCultureIgnoreCase) || table.Equals("Period", StringComparison.CurrentCultureIgnoreCase) ? "CampaignID" : "ContestID";

            string extraTable = "";
            int ContestTemplateID = 0;

            if (table.ToLower() == "contestsharing")
            {
                if (useType.Equals((Enums.UseType.Global)))
                    extraTable = string.Format(" JOIN (SELECT {0} FROM {1} WHERE UseType=1) {2} ON {3}.{0} = {2}.{0}", IDField, table, outTable, leftTable);

                if (useType.Equals((Enums.UseType.Site)))
                {
                    string ApplicationTemplateID = "0";
                    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@"JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE TempResource.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {3} ) Private                                                       
                                                       FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {4}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Private", "TempResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, ApplicationTemplateID, outTable, leftTable);

                }

                if (useType.Equals((Enums.UseType.Campaign)))
                {
                    ContestTemplateID = conn.Query<Contest>("SELECT TemplateID FROM Contest WHERE ContestID = @ContestID", new { ContestID = editCampaignID }).FirstOrDefault().TemplateID.GetValueOrDefault();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE TempResource.{0} END {0}
                                                    FROM (SELECT {0}, {1} FROM {2} WHERE UseType=3 AND ContestID = {3} ) Private                                                    
                                                    FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND TemplateID = {4}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Private", "TempResource") + @"                                                                                                                                                                                
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable);
                }

                if (useType.Equals((Enums.UseType.SiteContest)))
                {
                    string ApplicationTemplateID = "0";
                    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN SiteResource.{0} > 0 THEN SiteResource.{0} ELSE TempResource.{0} END {0}
                                                    FROM (SELECT {0}, {1} FROM {2} WHERE UseType=5 AND ContestID = {3} ) Private                                                    
                                                    FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {4}) SiteResource ON " + SqlUilities.GetJoinFields(arr, "Private", "SiteResource") + @"                                                                                                                                                                                
                                                    FULL JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {7}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Private", "TempResource") + @"                                                                                                                                                                                 
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);
                }


                if (useType.Equals((Enums.UseType.ContestTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND TemplateID = {3}) 
                                                  {4} ON {5}.{0} = {4}.{0}
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable);


                if (useType.Equals((Enums.UseType.ApplicationTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {3}) 
                                                      {4} ON {5}.{0} = {4}.{0}                                
                                                    ", IDField, JoinFields, table, editApplicationID, outTable, leftTable);


            }
            else if (table.ToLower() == "sweepstake")
            {
                int PeriodID = -1;
                if (dic.ContainsKey("OutTable") && dic.ContainsKey("PeriodID"))
                    Int32.TryParse(dic["PeriodID"], out PeriodID);
                string filter = PeriodID > 0 ? string.Format(" PeriodID = {0}", PeriodID) : " IsActive = 1 ";

                if (useType.Equals((Enums.UseType.Global)))
                    extraTable = string.Format(@" JOIN (SELECT {0} 
                                                        FROM {1} 
                                                        JOIN Period ON Period.PeriodID = {1}.ReferID
                                                        WHERE {4} AND UseType=1  
                                                        ) {2} ON {3}.{0} = {2}.{0} 
                                                ", IDField, table, outTable, leftTable, filter);

                if (useType.Equals((Enums.UseType.Site)))
                {
                    //string ApplicationTemplateID = "0";
                    //using (var conn = Commons.DBContext)
                    //    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@"JOIN (SELECT {0}, {1} FROM {2} 
                                                       JOIN Period ON Period.PeriodID = {2}.ReferID 
                                                       WHERE {7} AND UseType=2 AND ApplicationID = {3}                                                        
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, 0, outTable, leftTable, filter);

                }

                if (useType.Equals((Enums.UseType.Campaign)))
                {
                    ContestTemplateID = conn.Query<Contest>("SELECT TemplateID FROM Contest WHERE ContestID = @ContestID", new { ContestID = editCampaignID }).FirstOrDefault().TemplateID.GetValueOrDefault();
                    extraTable = string.Format(@" JOIN (SELECT {0}, {1} FROM {2} 
                                                        JOIN Period ON Period.PeriodID = {2}.ReferID 
                                                        WHERE {7} AND UseType=3 AND CampaignID = {3}  
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable, filter);
                }

                if (useType.Equals((Enums.UseType.SiteContest)))
                {
                    string ApplicationTemplateID = "0";
                    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE SiteResource.{0} END {0}
                                                    FROM (SELECT {0}, {1} FROM {2} 
                                                          JOIN Period ON Period.PeriodID = {2}.ReferID
                                                          WHERE {8} AND UseType=5 AND CampaignID = {3} ) Private                                                    
                                                    FULL JOIN 
                                                         (SELECT {0}, {1} FROM {2} 
                                                          JOIN Period ON Period.PeriodID = {2}.ReferID
                                                          WHERE IsActive = 1 AND UseType=2 AND ApplicationID = {4}) SiteResource ON " + SqlUilities.GetJoinFields(arr, "Private", "SiteResource") + @"                                                                                                                                                                                                                                   
                                                    ) {5} ON {6}.{0} = {5}.{0}                                
                                            ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID, filter);
                }


                if (useType.Equals((Enums.UseType.ContestTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT {0} FROM {2} 
                                                        JOIN Period ON Period.PeriodID = {2}.ReferID
                                                        WHERE {6} AND UseType=4 AND TemplateID = {3}
                                                       ) {4} ON {5}.{0} = {4}.{0}
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable, filter);


                if (useType.Equals((Enums.UseType.ApplicationTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT {0} FROM {2} 
                                                        JOIN Period ON Period.PeriodID = {2}.ReferID
                                                        WHERE {6} AND UseType=6 AND ApplicationID = {3}) 
                                                      {4} ON {5}.{0} = {4}.{0}                                
                                                    ", IDField, JoinFields, table, editApplicationID, outTable, leftTable, filter);


            }
            else if (table.ToLower() == "period")
            {
                if (useType.Equals((Enums.UseType.SiteContest)))
                {

                    string ApplicationTemplateID = "0";
                    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@"   JOIN (SELECT {0}, {1}, 1 Priority FROM {2} WHERE UseType=5 AND " + ContestIDField + @" = {3}
                                                    UNION ALL
                                                    SELECT {0}, {1}, 2 Priority FROM {2} WHERE UseType=2 AND ApplicationID = {4}                                                        
                                                    UNION ALL                                                   
                                                    SELECT {0}, {1}, 3 Priority FROM {2} WHERE UseType=6 AND ApplicationID = {7}                                                                                                                                                                                
                                                    UNION ALL
                                                    SELECT {0}, {1}, 4 Priority FROM {2} WHERE UseType = 1) {5} ON {5}.{0} = {6}.{0}  
                                                    ORDER BY {5}.Priority               
                                                ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);

                }
            }
            else if (table.ToLower() == "categorymapping")
            {
                if (useType.Equals((Enums.UseType.Global)))
                    extraTable = string.Format(" JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=1) {3} ON {4}.{1} = {3}.{1}", IDField, JoinFields, table, outTable, leftTable);

                if (useType.Equals((Enums.UseType.Site)))
                {
                    string ApplicationTemplateID = "0";
                    Application app = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault();
                    if (app != null)
                        ApplicationTemplateID = app.ApplicationTemplateID.ToString();

                    extraTable = string.Format(@"JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} ELSE Global.{1} END {1}, CASE WHEN Private.{1} > 0 THEN Private.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=2 AND ApplicationID = {3} ) Private
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=1) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=6 AND ApplicationID = {4}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{1} = {5}.{1}                                
                                                ", IDField, JoinFields, table, editApplicationID, ApplicationTemplateID, outTable, leftTable);
                }

                if (useType.Equals((Enums.UseType.Campaign)))
                {
                    var firstOrDefault = conn.Query<Contest>("SELECT TemplateID FROM Contest WHERE ContestID = @ContestID", new { ContestID = editCampaignID }).FirstOrDefault();
                    if (firstOrDefault != null)
                        ContestTemplateID = firstOrDefault.TemplateID.GetValueOrDefault();

                    extraTable = string.Format(@" 
                        JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN AppResource.{0} > 0 THEN AppResource.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} WHEN TempResource.{1}>0 THEN TempResource.{1} ELSE Global.{1} END {1},
                        CASE WHEN Private.ID > 0 THEN Private.isDisplay  WHEN AppResource.ID > 0 THEN AppResource.isDisplay WHEN TempResource.ID>0 THEN TempResource.isDisplay ELSE Global.isDisplay END isDisplay
                        FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=3 AND CampaignID = {3} ) Private
                        FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType = 1) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"  
                        FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {4}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Global", "TempResource") + @"    
                        FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=2 AND ApplicationID = {7}) AppResource ON " + SqlUilities.GetJoinFields(arr, "Global", "AppResource") + @"                                                                                                                                                                                 
                        ) {5} ON {6}.{1} = {5}.{1}                                
                       ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable, editApplicationID);

                }

                if (useType.Equals((Enums.UseType.SiteContest)))
                {

                    string ApplicationTemplateID = "0";
                    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN AppResource.{0}>0 THEN AppResource.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} WHEN AppResource.{1}>0 THEN AppResource.{1} WHEN TempResource.{1}>0 THEN TempResource.{1} ELSE Global.{1} END {1}, CASE WHEN Private.ID > 0 THEN Private.isDisplay WHEN AppResource.ID>0 THEN AppResource.isDisplay WHEN TempResource.ID>0 THEN TempResource.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=5 AND CampaignID = {3} ) Private
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType = 1) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"                                                         
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=6 AND ApplicationID = {7}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                  
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=2 AND ApplicationID = {4}) AppResource ON " + SqlUilities.GetJoinFields(arr, "Global", "AppResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{1} = {5}.{1}                                
                                                ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);

                }

                if (useType.Equals((Enums.UseType.ContestTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN TempResource.{0} > 0 THEN TempResource.{0} ELSE Global.{0} END {0}, CASE WHEN TempResource.{1} > 0 THEN TempResource.{1} ELSE Global.{1} END {1}, CASE WHEN TempResource.ID > 0 THEN TempResource.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {3}) TempResource                                                   
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType = 1) Global ON " + SqlUilities.GetJoinFields(arr, "TempResource", "Global") + @"                                                                                                         
                                                       ) {4} ON {5}.{1} = {4}.{1}                                
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable);


                if (useType.Equals((Enums.UseType.ApplicationTemplate)))

                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}, CASE WHEN Private.{1} > 0 THEN Private.{1} ELSE Global.{1} END {1}, CASE WHEN Private.ID > 0 THEN Private.isDisplay ELSE Global.isDisplay END isDisplay
                                                       FROM (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=6 AND ApplicationID = {3}  ) Private
                                                       FULL JOIN (SELECT {0}, {1}, ISNULL(isDisplay,0) isDisplay FROM {2} WHERE UseType=1 ) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       ) {4} ON {5}.{1} = {4}.{1}                                
                                                ", IDField, JoinFields, table, editApplicationID, outTable, leftTable);
            }
            //Config
            else
            {
                if (useType.Equals((Enums.UseType.Global)))
                    extraTable = string.Format(" JOIN (SELECT {0} FROM {1} WHERE UseType=1) {2} ON {3}.{0} = {2}.{0}", IDField, table, outTable, leftTable);

                if (useType.Equals((Enums.UseType.Site)))
                {
                    string ApplicationTemplateID = "0";
                    Application app = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault();
                    if (app != null)
                        ApplicationTemplateID = app.ApplicationTemplateID.ToString();

                    extraTable = string.Format(@"JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {3} ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=1) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {4}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, ApplicationTemplateID, outTable, leftTable);
                }

                if (useType.Equals((Enums.UseType.Campaign)))
                {
                    var firstOrDefault = conn.Query<Contest>("SELECT TemplateID FROM Contest WHERE ContestID = @ContestID", new { ContestID = editCampaignID }).FirstOrDefault();
                    if (firstOrDefault != null)
                        ContestTemplateID = firstOrDefault.TemplateID.GetValueOrDefault();

                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=3 AND " + ContestIDField + @" = {3} ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType = 1) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"  
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {4}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editCampaignID, ContestTemplateID, outTable, leftTable);

                }
                if (useType.Equals((Enums.UseType.SiteContest)))
                {

                    string ApplicationTemplateID = "0";
                    ApplicationTemplateID = conn.Query<Application>("SELECT ApplicationTemplateID FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = editApplicationID }).FirstOrDefault().ApplicationTemplateID.ToString();
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} WHEN AppResource.{0}>0 THEN AppResource.{0} WHEN TempResource.{0}>0 THEN TempResource.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=5 AND " + ContestIDField + @" = {3} ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType = 1) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"                                                         
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {7}) TempResource ON " + SqlUilities.GetJoinFields(arr, "Global", "TempResource") + @"                                                                                                                                                                                  
                                                       LEFT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=2 AND ApplicationID = {4}) AppResource ON " + SqlUilities.GetJoinFields(arr, "Global", "AppResource") + @"                                                                                                                                                                                 
                                                       ) {5} ON {6}.{0} = {5}.{0}                                
                                                ", IDField, JoinFields, table, editCampaignID, editApplicationID, outTable, leftTable, ApplicationTemplateID);

                }

                if (useType.Equals((Enums.UseType.ContestTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN TempResource.{0} > 0 THEN TempResource.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=4 AND " + TemplateIDField + @" = {3}) TempResource                                                   
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType = 1) Global ON " + SqlUilities.GetJoinFields(arr, "TempResource", "Global") + @"                                                                                                         
                                                       ) {4} ON {5}.{0} = {4}.{0}                                
                                                ", IDField, JoinFields, table, editTemplateID, outTable, leftTable);


                if (useType.Equals((Enums.UseType.ApplicationTemplate)))
                    extraTable = string.Format(@" JOIN (SELECT CASE WHEN Private.{0} > 0 THEN Private.{0} ELSE Global.{0} END {0}
                                                       FROM (SELECT {0}, {1} FROM {2} WHERE UseType=6 AND ApplicationID = {3}  ) Private
                                                       RIGHT JOIN (SELECT {0}, {1} FROM {2} WHERE UseType=1 ) Global ON " + SqlUilities.GetJoinFields(arr, "Private", "Global") + @"                                                                                                      
                                                       ) {4} ON {5}.{0} = {4}.{0}                                
                                                ", IDField, JoinFields, table, editApplicationID, outTable, leftTable);

            }

            return extraTable;
        }


        public static string GetFilter(Enums.UseType useType, int editApplicationID = 0, int editCampaignID = 0, string table = "")
        {
            string where = "";
            table = string.IsNullOrEmpty(table) ? "" : table + ".";
            if (useType.Equals(Enums.UseType.Site))
            {
                where = string.Format("AND {0}ApplicationID = {1}", table, editApplicationID);
            }
            if (useType.Equals(Enums.UseType.Campaign))
            {
                where = string.Format("AND {0}ContestID = {1}", table, editCampaignID);
            }
            if (useType.Equals(Enums.UseType.SiteContest))
            {
                where = string.Format("AND {0}ApplicationID = {1} AND {0}ContestID = {2}", table, editApplicationID, editCampaignID);
            }
            return where;
        }
        public static string GetFilter(Enums.UseType useType, int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1)
        {
            return GetFilter(useType, "ApplicationID", "ContestID", "TemplateID", editApplicationID, editCampaignID, editTemplateID);
        }
        public static string GetFilter(Enums.UseType useType, int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1, bool useHidded = false)
        {
            return GetFilter(useType, "ApplicationID", "ContestID", "TemplateID", editApplicationID, editCampaignID, editTemplateID, "", useHidded);
        }
        public static string GetFilter(Enums.UseType useType, string applicationField, string campaignField, string templateField, int editApplicationID = -1, int editCampaignID = -1, int editTemplateID = -1, string prefixForUseType = "", bool useHidded = false)
        {
            string where = "";
            if (useType.Equals((Enums.UseType.Global)))
                where = string.Format("AND {1}UseType = {0}", (int)useType, prefixForUseType);

            if (useType.Equals(Enums.UseType.Site) || useType.Equals(Enums.UseType.ApplicationTemplate))
            {
                where = string.Format("AND {3}UseType = {0} AND {1} = {2}", (int)useType, applicationField, editApplicationID, prefixForUseType);
            }

            if (useType.Equals(Enums.UseType.Campaign))
            {
                if (!useHidded)
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} AND {4}ShowHide <> {3} ", (int)useType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
                else
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} ", (int)useType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
            }

            if (useType.Equals(Enums.UseType.SiteContest))
            {
                if (!useHidded)
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} AND {4}ShowHide <> {3} ", (int)useType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
                else
                    where = string.Format("AND {4}UseType = {0} AND {1} = {2} ", (int)useType, campaignField, editCampaignID, (int)Enums.DisplayItemOption.Hide, prefixForUseType);
            }

            if (useType.Equals((Enums.UseType.ContestTemplate)))
                where = string.Format("AND {3}UseType = {0} AND {1} = {2}", (int)useType, templateField, editTemplateID, prefixForUseType);

            return where;
        }

        public static string GetShowHide(Enums.UseType useType, string leftTable = "Resource.ShowHide", bool useHidded = false)
        {
            string where = "";
            if (useType.Equals(Enums.UseType.Global) || useType.Equals(Enums.UseType.ContestTemplate))
                where = string.Format(@"");
            if (useType.Equals((Enums.UseType.Site)) || useType.Equals(Enums.UseType.Campaign))
                where = useHidded ? string.Format("{0}=1") : "";
            return where;
        }
        public static string GetJoinFields(string[] arr, string leftTable, string outTable)
        {
            string result = "";
            foreach (var field in arr)
            {
                result += (result == "" ? "" : " AND ") + string.Format("{0}.{2} = {1}.{2}", leftTable, outTable, field);
            }
            return result;
        }
        public static Dictionary<string, string> ConvertToTokensDictionary(object value, int convertType = 0)
        {
            Dictionary<string, string> result = new Dictionary<string, string>();
            if (value != null && value != DBNull.Value)
            {
                Type type = value.GetType();
                List<PropertyInfo> properties = type.GetProperties().ToList();
                foreach (var item in properties)
                {
                    string strValue = string.Empty;
                    object obj = item.GetValue(value);
                    strValue = (obj == null || obj == DBNull.Value) ? string.Empty : obj.ToString();
                    result.Add(string.Format(convertType.Equals(0) ? "{0}{1}{0}" : "{1}", StaticKeyValue.SeparatorTemplateToken, item.Name), strValue);
                }
            }
            return result;
        }
    }
}
