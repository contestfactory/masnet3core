﻿using System;
using System.Linq;
using System.Data.SqlClient;
using Dapper;
using System.Collections.Generic;

namespace Masnet3.Business.Utilities
{
    public static class DapperPaging
    {
        const string SqlPagging =
            @"WITH ListPaged As
            ( SELECT    ROW_NUMBER() OVER ( ORDER BY {0}) AS RowNum, {1}
                      FROM      {2}
                      WHERE     {3}
            )
            SELECT * FROM ListPaged
            WHERE   RowNum > {4} 
                AND RowNum < {5}";
        
        const string sqlShowMore = @"SELECT TOP {0}  1 AS RowNum, {2}
                                FROM      {3}
                                WHERE     {4} AND {5}
                                ORDER BY    {1}";

        public static IEnumerable<T> QueryPaging<T>(this SqlConnection conn, string sqlSelect, string sqlFrom, string sqlWhere, string OrderBy, dynamic param = null, int pageindex = 1, int? pagesize = 10, string MaxId = "")
        {
            string querypaging = "";
            int totalRecord;
            pagesize = pagesize ?? 0;
            if (MaxId != "")
            {
                totalRecord = pagesize.Value * (pageindex + 2);
                querypaging = String.Format(sqlShowMore, pagesize, OrderBy, sqlSelect, sqlFrom, sqlWhere, MaxId);
            }
            else
            {

                totalRecord = conn.Query<int>(String.Format("SELECT COUNT(1) OVER() FROM {0} WHERE {1}", sqlFrom, sqlWhere), (object)param).FirstOrDefault();

                int from = pageindex * pagesize.Value;
                int to = (from + pagesize.Value + 1) <= 1 ? int.MaxValue : from + pagesize.Value + 1;
                if (pagesize == 0) to = int.MaxValue;

                querypaging = String.Format(SqlPagging, OrderBy, sqlSelect, sqlFrom, sqlWhere, from, to);
            }

            //return conn.Query<T>(querypaging, (object)param).ToPagedList(pageindex, (pagesize > 0 ? pagesize.Value : int.MaxValue), totalRecord);
            return conn.Query<T>(querypaging, (object)param);
        }


        public static T Cast<T>(object o)
        {
            return (T)o;
        }
    }
}
