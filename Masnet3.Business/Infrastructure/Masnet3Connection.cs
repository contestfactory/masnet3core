﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace Masnet3.Business.Infrastructure
{
    public class Masnet3Connection
    {
        public IConfiguration configuration = null;
        //public IDbConnection dbConnection = null;

        //public Masnet3Connection()
        //{
            //this.dbConnection = GetDbConectionBuild();

        //}
        public Masnet3Connection(IConfiguration configuration)
        {
            this.configuration = configuration;
            //this.dbConnection = GetDbConection();
        }

        protected SqlConnection GetDbConection()
        {
            return new SqlConnection(configuration.GetConnectionString("dmasConnection"));
        }

        protected SqlConnection GetDbConectionBuild()
        {
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            var config = builder.Build();
            return new SqlConnection(config.GetConnectionString("dmasConnection"));
        }
    }
}
