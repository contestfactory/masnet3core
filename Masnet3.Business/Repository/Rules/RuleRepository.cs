﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;

namespace Masnet3.Business.Repository.Rules
{
    public class RuleRepository : Masnet3Connection, IRuleRepository
    {
        public RuleRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<Rule> GetListRules()
        {
            IEnumerable<Rule> rules;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                rules = conn.Query<Rule>("SELECT * FROM ValidationRule Order By Name");
                conn.Close();
            }
            return rules;
        }
    }
}
