﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Rules
{
    public interface IRuleRepository
    {
        IEnumerable<Rule> GetListRules();
    }
}
