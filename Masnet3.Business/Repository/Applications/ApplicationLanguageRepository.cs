﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Dapper;

namespace Masnet3.Business.Repository.Applications
{
    public class ApplicationLanguageRepository: Masnet3Connection, IApplicationLanguageRepository
    {
        public ApplicationLanguageRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<ApplicationLanguage> GetApplicationLanguagesByUserTypes(int[] userTypes)
        {
            IEnumerable<ApplicationLanguage> applicationLanguages;
            string sqlRun = $"SELECT* FROM ApplicationLanguage WHERE UseType IN({string.Join(",", userTypes)})";
            using (var con = this.GetDbConection())
            {
                con.Open();
                applicationLanguages = con.Query<ApplicationLanguage>(sqlRun);
            }
            return applicationLanguages;
        }
    }
}
