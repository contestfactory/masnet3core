﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Applications
{
    public interface IApplicationLanguageRepository
    {
        IEnumerable<ApplicationLanguage> GetApplicationLanguagesByUserTypes(int[] userTypes);
    }
}
