﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using Dapper;
using System.Collections.Generic;

namespace Masnet3.Business.Repository.Applications
{
    public class ApplicationRepository : Masnet3Connection, IApplicationRepository
    {
        public ApplicationRepository(IConfiguration configuration) : base(configuration) { }

        public Application GetApplicationById(int ApplicationId)
        {
            Application application;
            using (var con = this.GetDbConection())
            {
                con.Open();
                application = con.QueryFirstOrDefault<Application>("SELECT * FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = ApplicationId });
            }
            return application;
        }

        public Application GetApplicationByName(string ApplicationName)
        {
            Application application;
            using (var con = this.GetDbConection())
            {
                con.Open();
                application = con.QueryFirstOrDefault<Application>("SELECT * FROM [application] WHERE ApplicationName = @ApplicationName", new { ApplicationName = ApplicationName });
            }
            return application;
        }

        public IEnumerable<MinifiedApplication> GetMinifiedApplicationsIsTemplateNull()
        {
            IEnumerable<MinifiedApplication> minifiedApplications;
            using (var con = this.GetDbConection())
            {
                con.Open();
                minifiedApplications = con.Query<MinifiedApplication>("SELECT * FROM Application WHERE ISNULL(IsTemplate, 0) = 0");
            }
            return minifiedApplications;
        }
    }
}
