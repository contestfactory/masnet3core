﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Applications
{
    public interface IApplicationRepository
    {
        Application GetApplicationById(int ApplicationId);
        Application GetApplicationByName(string ApplicationName);
        IEnumerable<MinifiedApplication> GetMinifiedApplicationsIsTemplateNull();
    }
}
