﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Masnet3.Business.Repository.Applications
{
    public interface IMinifiedApplicationRepository
    {
        Task<MinifiedApplication> GetMinifiedApplicationByApplicationId(int applicationId);
    }
}
