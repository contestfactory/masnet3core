﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Dapper;

namespace Masnet3.Business.Repository.Applications
{
    public class MinifiedApplicationRepository : Masnet3Connection, IMinifiedApplicationRepository
    {
        public MinifiedApplicationRepository(IConfiguration configuration) : base(configuration) { }

        public async Task<MinifiedApplication> GetMinifiedApplicationByApplicationId(int applicationId)
        {
            MinifiedApplication minifiedApplication;
            using (var con = this.GetDbConection())
            {
                con.Open();
                minifiedApplication = await con.QueryFirstAsync<MinifiedApplication>("SELECT * FROM Application WHERE ApplicationID = @ApplicationID", new { ApplicationID = applicationId });
            }
            return minifiedApplication;
        }
    }
}
