﻿using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Dapper;
using System.Threading.Tasks;

namespace Masnet3.Business.Repository
{
    public class RepositoryBasic : Masnet3Connection, IRepositoryBasic
    {
        public RepositoryBasic(IConfiguration configuration) : base(configuration) { }

        public void ExecuteSql(string sql, object parameter = null)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                if (parameter != null)
                {
                    conn.Execute(sql, parameter);
                }
                else
                {
                    conn.Execute(sql);
                }
                conn.Close();
            }
        }

        public IEnumerable<T> GetItemsDynamicSql<T>(string sql, object parameter = null)
        {
            IEnumerable<T> ts;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                if (parameter != null)
                {
                    ts = conn.Query<T>(sql, parameter);
                }
                else
                {
                    ts = conn.Query<T>(sql);
                }
                conn.Close();
            }
            return ts;
        }

        public async Task<IEnumerable<T>> GetItemsDynamicSql_Task<T>(string sql, object parameter = null)
        {
            IEnumerable<T> ts;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                if (parameter != null)
                {
                    ts = await conn.QueryAsync<T>(sql, parameter);
                }
                else
                {
                    ts = await conn.QueryAsync<T>(sql);
                }
                conn.Close();
            }
            return ts;
        }

        public int CheckExistsItem(string table, string filter, object paramter)
        {
            string sql = $"IF EXISTS (SELECT 1 FROM {table} WHERE 1=1 {(string.IsNullOrEmpty(filter) == true ? "" : filter)}) SELECT 1 ELSE SELECT 0";
            int result = -1;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                result = conn.QueryFirst<int>(sql, paramter);
                conn.Close();
            }
            return result;
        }
    }
}
