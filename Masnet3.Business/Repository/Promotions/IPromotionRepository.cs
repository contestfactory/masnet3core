﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Promotions
{
    public interface IPromotionRepository
    {
        void UpdateUsed(PromotionalCode code);
        PromotionalCode IsValidCode(PromotionalCode code);
    }
}
