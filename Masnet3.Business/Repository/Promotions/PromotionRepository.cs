﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Dapper;

namespace Masnet3.Business.Repository.Promotions
{
    public class PromotionRepository : Masnet3Connection, IPromotionRepository
    {
        public PromotionRepository(IConfiguration configuration) : base(configuration) { }

        public void UpdateUsed(PromotionalCode code)
        {
            string filter = " AND ApplicationId = @ApplicationId AND CampaignId = @CampaignId ";
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                conn.Execute(string.Format(@"
                    DECLARE @ProPrizeID INT;                    
                    UPDATE  PromotionalCode SET Used = 1 WHERE Code = @Code {0} ;                                       
                    SELECT  @ProPrizeID = PrizeID FROM PromotionalCode WHERE Code = @Code {0} ;                    
                    UPDATE  Prize
                    SET     Used = CASE WHEN ISNULL(Used,0) < Quantity THEN ISNULL(Used,0) + 1 ELSE Quantity END
                    WHERE   PrizeID = @ProPrizeID AND ContestID = @CampaignId 
                ", filter), code);
                conn.Close();
            }
        }

        public PromotionalCode IsValidCode(PromotionalCode code)
        {
            int result = 0;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                var promotionCode =
                    conn.QueryFirstOrDefault<PromotionalCode>(@"
                        SELECT promo.*, Description, Description_3, PrizeName, Prize.Photo
                        FROM PromotionalCode promo
                        LEFT JOIN Prize ON Prize.PrizeID = promo.PrizeID
                        WHERE promo.Code = @Code 
                        AND promo.ApplicationId = @ApplicationId 
                        AND promo.CampaignId = @CampaignId", code);
                try
                {
                    if (promotionCode == null) // Invalid Invitation Code                       
                        result = -3;
                    else
                    {
                        if (promotionCode.Used == 1) // Used                            
                            result = -2;
                        else
                        {
                            var expired = promotionCode.ExpirationDate.GetValueOrDefault() < DateTime.Now;
                            if (expired) // Expired                                
                                result = -1;
                            else //OK
                            {
                                result = 1;
                                code = promotionCode;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    result = -3;
                }

                code.IsValidCode = result;
            }

            //return result;
            return code;
        }
    }
}
