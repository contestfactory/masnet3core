﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Users
{
    public interface IUserRepository
    {
        IEnumerable<string> GetUserPermissionList(long userId);
        IEnumerable<Permission> GetPermissionBy(bool isGetAll, long userId);
        Permission GetPermissionByName(string name);

        void InsertAccountDetail(AccountDetail accountDetail);
        AccountDetail Insert(AccountDetail accountdetail);
        AccountDetail GetByAccountDetailTranId(object TranID);
        AccountDetail UpdateAccountDetailStatus(AccountDetail accountdetail, SweepStake sweep);
        AccountDetail UpdateAccountDetailAllowPoint(AccountDetail tran, SweepStake sweep, int frequency, int pointFrequencyType);

        string SetPermissionsForCurrentUser(ICollection<Permission> userPermissions = null, int userType = 0, int userId = 0);

    }
}
