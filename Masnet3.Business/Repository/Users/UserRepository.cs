﻿using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Collections.Generic;
using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System.Linq;
using Masnet3.Lib;
using System;

namespace Masnet3.Business.Repository.Users
{
    public class UserRepository : Masnet3Connection, IUserRepository
    {
        public UserRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<string> GetUserPermissionList(long userId)
        {
            IEnumerable<string> ListPermission;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                ListPermission = conn.Query<string>("SELECT cast(PermissionID as nvarchar) FROM dbo.fn_UserPermissions(@UserID)", new { UserID = userId });
                conn.Close();
            }
            return ListPermission;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isGetByUser">False: Get All Permission, True: Get By User Id</param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<Permission> GetPermissionBy(bool isGetByUser, long userId)
        {
            IEnumerable<Permission> permissions;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                if (isGetByUser)
                {
                    permissions = conn.Query<Permission>(@"
                                DECLARE @IsUserPermission bit
                                DECLARE @RoleID int
                                
                                SELECT @IsUserPermission = ISNULL(IsUserPermission, 0), @RoleID = RoleID
                                FROM [Registration]
                                WHERE [UserID] = @UserID

                                IF @IsUserPermission = 1
                                BEGIN
                                    SELECT p.*
                                    FROM UserPermission u INNER JOIN Permission p ON u.PermissionID = p.PermissionID
                                    WHERE u.UserID = @UserID
                                END
                                ELSE
                                BEGIN
                                    SELECT p.*
                                    FROM RolePermission r INNER JOIN Permission p ON r.PermissionID = p.PermissionID
                                    WHERE r.RoleID = @RoleID
                                END                                
                                ", new { UserID = userId });
                }
                else
                {
                    permissions = conn.Query<Permission>("SELECT * FROM Permission");
                }
                conn.Close();
            }
            return permissions;
        }

        public Permission GetPermissionByName(string name)
        {
            Permission permission = new Permission();
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                permission = conn.QueryFirstOrDefault<Permission>("SELECT * FROM Permission WHERE PermissionName=@PermissionName", new { PermissionName = name });
                conn.Close();
            }
            return permission;
        }

        public AccountDetail GetByAccountDetailTranId(object TranID)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();
                var accountdetail =
                    conn.Query<AccountDetail>(
                        @" SELECT * FROM AccountDetail WHERE TranID = @TranID",
                        new { TranID }
                    ).FirstOrDefault();
                conn.Close();
                return accountdetail;
            }
        }

        public AccountDetail UpdateAccountDetailStatus(AccountDetail accountdetail, SweepStake sweep)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();
                string sql = "";

                if (accountdetail.Status == 1)
                    sql = @" UPDATE AccountDetail SET ShareStatus = 1 
                             WHERE TranID = @TranID AND ShareStatus = 0 ";

                if (sweep.PointType == accountdetail.pt && accountdetail.Status == 1)
                    sql += @" UPDATE AccountDetail SET Status = @Status  
                             WHERE TranID = @TranID AND Status = 0 ";
                else if (accountdetail.Status == (int)Enums.AccountDetailStatus.New)
                    sql += @" UPDATE AccountDetail SET Status = @Status " +
                             (accountdetail.ActionID == (int)Enums.Action.Facebook ? ", ShareStatus = 1" : "") + @"
                             WHERE TranID = @TranID ";

                if (!string.IsNullOrEmpty(sql))
                    conn.Query<int>(sql, accountdetail);
                conn.Close();
                return accountdetail;
            }
        }

        public AccountDetail UpdateAccountDetailAllowPoint(AccountDetail tran, SweepStake sweep, int frequency, int pointFrequencyType)
        {
            if (frequency > 0 && (sweep.PointLimit > 0 || sweep.ActionType == (int)Enums.ActionType.Share))
            {

                string upperDate = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
                string lowerDate = DateTime.Now.ToString("yyyy-MM-dd");
                string sql = @"
                    SELECT COUNT(1) RowNum FROM AccountDetail 
                    WHERE ContestID = @ContestID AND ActionID = @ActionID 
                    AND UserID = @UserID ";

                if (tran.SourceID.HasValue && tran.SourceID.Value > 0 && pointFrequencyType == (int)Enums.PointFrequencyType.SeperatePromotionAndSubmission)
                {
                    sql += " AND SourceID = @SourceID ";
                }

                if (pointFrequencyType == (int)Enums.PointFrequencyType.SeperatePromotionAndSubmission)
                    sql += (tran.ContestantID > 0 ? " AND ContestantID > 0 " : " AND (ContestantID = 0 OR ContestantID IS NULL) ");

                using (var conn = GetDbConection())
                {
                    conn.Open();
                    switch(frequency)
                    {
                        case 1: // per Day
                            sql += " AND TransactionDate < @upperDate AND TransactionDate > @LowerDate ";
                            break;
                        case 2: // per Promotion = from promotion start to end date
                            sql += "";
                            break;
                        case 3: // per Hour
                            sql += " AND DATEDIFF (hour,TransactionDate,GETDATE()) = 0 ";
                            break;
                        case 4: // Round
                            sql += "";
                            break;
                        case 5: // Week
                            sql += @" AND TransactionDate > DATEADD(DAY, 1 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE() AS DATE))
                              AND TransactionDate < DATEADD(DAY, 7 - DATEPART(WEEKDAY, GETDATE()) + 1, CAST(GETDATE() AS DATE)) ";
                            break;
                        case 6: // 5 mins
                            sql += @" AND ABS(DATEDIFF (minute,TransactionDate,GETDATE())) < 5";
                            break;
                        case 7:
                            sql += "";
                            break;
                    }
                    var action = conn.Query<AccountDetail>(sql,
                        new
                        {
                            tran.ActionID,
                            UpperDate = upperDate,
                            LowerDate = lowerDate,
                            UserID = tran.UserID,
                            SourceID = tran.SourceID.GetValueOrDefault(),
                            ContestID = tran.ContestID.GetValueOrDefault()
                        }).FirstOrDefault();

                    conn.Close();

                    if (sweep.ActionType == (int)Enums.ActionType.Share)
                    {
                        if (action != null && action.RowNum > 0)
                            tran.Point = 0;
                    }
                    else
                        if (action != null && sweep.PointLimit > 0 && action.RowNum >= sweep.PointLimit)
                        tran.Point = 0;
                }
            }
            return tran;
        }

        public void InsertAccountDetail(AccountDetail accountDetail)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();

                string sql = @"INSERT INTO AccountDetail(UserID, ActionID, TransactionDate, Point, Amount, ReferID, ReferType, SourceID, Description, Transtype, ApplicationID, ContestID, ContestantID, TemplateToken, PublicLevel, Balance, TransCode, CampaignID, Code, RepID,PrizeID)
                                            VALUES (@UserID, @ActionID, @TransactionDate, @Point, @Amount, @ReferID, @ReferType, @SourceID, @Description, @Transtype, @ApplicationID, @ContestID, @ContestantID, @TemplateToken, @PublicLevel, @Balance, @TransCode, @CampaignID, @Code, @RepID,@PrizeID)";
                conn.Execute(sql, accountDetail);
                conn.Close();
            }
        }

        public AccountDetail Insert(AccountDetail accountdetail)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();

                if (accountdetail.Status == -1)
                {
                    accountdetail.Status = (int)Enums.AccountDetailStatus.Shared;
                }
                accountdetail.TranID = conn.Query<int>(@"INSERT INTO [AccountDetail]
                                                       ([UserID],AnoUserID                                                       
                                                       ,[ActionID]
                                                       ,[TransactionDate]
                                                       ,[Point]
                                                       ,[Amount]
                                                       ,[ReferID]
                                                       ,[ReferType]
                                                       ,[SourceID]
                                                       ,[Description]
                                                       ,[Transtype]
                                                       ,[ApplicationID]
                                                       ,[ContestID]
                                                       ,[ContestantID]
                                                       ,[MediaID]
                                                       ,[TemplateToken]
                                                       ,[Balance]
                                                       ,[TransCode]
                                                       ,IsSpinWinner
                                                       ,CampaignID
                                                       ,PrizeID
                                                       ,[ContestantEntryID]
                                                       ,[Status]
                                                       ,QuestionID
                                                       ,AnswerID
                                                       ,FriendFirstName
                                                       ,FriendLastName
                                                       ,FriendEmail, ShareStatus,Code,RepID)
                                                 VALUES
                                                       (@UserID, @AnoUserID
                                                       ,@ActionID 
                                                       ,@TransactionDate 
                                                       ,@Point 
                                                       ,@Amount 
                                                       ,@ReferID 
                                                       ,@ReferType 
                                                       ,@SourceID 
                                                       ,@Description 
                                                       ,@Transtype 
                                                       ,@ApplicationID 
                                                       ,@ContestID 
                                                       ,@ContestantID
                                                       ,@MediaID 
                                                       ,@TemplateToken
                                                       ,@Balance
                                                       ,@TransCode
                                                       ,@IsSpinWinner
                                                       ,@CampaignID
                                                       ,@PrizeID
                                                       ,@ContestantEntryID
                                                       ,@Status
                                                       ,@QuestionID
                                                       ,@AnswerID
                                                       ,@FriendFirstName
                                                       ,@FriendLastName
                                                       ,@FriendEmail, @ShareStatus,@Code,@RepID);

                                                SELECT CAST(SCOPE_IDENTITY() as int);", accountdetail).SingleOrDefault();

                conn.Close();
            }
            return accountdetail;
        }

        public string SetPermissionsForCurrentUser(ICollection<Permission> userPermissions = null, int userType = 0, int userId = 0)
        {
            bool needQueryPermission = true;
            List<Permission> permissions = null;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string query = "SELECT * FROM Permission";

                if (userType != (int)Enums.RegistrationUserType.SuperAdmin)
                {
                    if (userPermissions == null)
                    {
                        query = string.Format(@"
                                DECLARE @IsUserPermission bit
                                DECLARE @RoleID int
                                
                                SELECT @IsUserPermission = ISNULL(IsUserPermission, 0), @RoleID = RoleID
                                FROM [Registration]
                                WHERE [UserID] = {0}

                                IF @IsUserPermission = 1
                                BEGIN
                                    SELECT p.*
                                    FROM UserPermission u INNER JOIN Permission p ON u.PermissionID = p.PermissionID
                                    WHERE u.UserID = {0}
                                END
                                ELSE
                                BEGIN
                                    SELECT p.*
                                    FROM RolePermission r INNER JOIN Permission p ON r.PermissionID = p.PermissionID
                                    WHERE r.RoleID = @RoleID
                                END                                
                                ", userId);
                    }
                    else
                    {
                        needQueryPermission = false;
                        permissions = userPermissions.ToList();
                    }

                }
                if (needQueryPermission)
                {
                    permissions = conn.Query<Permission>(query).ToList();
                    conn.Close();
                }

                string strPermissions = "";
                if (permissions != null && permissions.Count > 0)
                {
                    strPermissions = ";";
                    bool hasPermission = true;
                    foreach (var item in permissions)
                    {
                        hasPermission = true;
                        if (userType == (int)Enums.RegistrationUserType.Customer)
                        {
                            if (item.PermissionID == (int)Enums.Permissions.Admin_Site_AdhocEmail)
                            {
                                hasPermission = false;
                            }
                        }

                        if (hasPermission)
                        {
                            strPermissions += item.PermissionID + ";";
                        }
                    }

                }
                return strPermissions;
            }
        }
    }
}
