﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Dapper;
using Masnet3.Business.Utilities;
using MASNET.DataAccess.Constants.Enums;
using System.Linq;

namespace Masnet3.Business.Repository.Languages
{
    public class LanguageRepostirory : Masnet3Connection, ILanguageRepostirory
    {
        public LanguageRepostirory(IConfiguration configuration) : base(configuration) { }

        public List<Language> GetLanguages(bool isActive = false)
        {
            List<Language> result;
            using (var conn = GetDbConection())
            {
                conn.Open();
                result = conn.Query<Language>(
                    @"SELECT * FROM Language 
                    WHERE 1=1 " + (isActive ? " AND isActive = 1" : "") +
                    "ORDER BY Priority").ToList();
                conn.Close();
            }
            return result;
        }

        public List<Language> GetLanguageList(Enums.UseType useType, int editSiteId = 0, int editCampaignId = 0)
        {
            List<Language> result;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                result = conn.Query<Language>(string.Format(
                    @"SELECT l.*, [Default]
                    FROM ApplicationLanguage
                    INNER JOIN [Language] l ON ApplicationLanguage.LanguageID = l.LanguageID
                    WHERE 1=1 {0}", SqlUilities.GetFilter(useType, editSiteId, editCampaignId, 0, true))).ToList();
                conn.Close();
            }
            return result ?? new List<Language>();
        }
    }
}
