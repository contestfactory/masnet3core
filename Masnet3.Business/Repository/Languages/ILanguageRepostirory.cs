﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Languages
{
    public interface ILanguageRepostirory
    {
        List<Language> GetLanguages(bool isActive = false);
        List<Language> GetLanguageList(Enums.UseType useType, int editSiteId = 0, int editCampaignId = 0);
    }
}
