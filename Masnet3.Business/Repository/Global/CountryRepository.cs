﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Collections.Generic;
using System.Linq;

namespace Masnet3.Business.Repository.Global
{
    public class CountryRepository : Masnet3Connection, ICountryRepository
    {
        public CountryRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<StateOfUSA> GetStateOfUSA(int USAState = 1)
        {
            IEnumerable<StateOfUSA> stateOfUSAs;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                IEnumerable<string> ColumList = conn.Query<string>("SELECT COLUMN_NAME FROM   information_schema.columns WHERE  table_name = 'StateOfUSA'");
                string ColumnString = "StateID,Contiguous,StateCode";
                foreach (var item in ColumList)
                {
                    if (item.IndexOf("State_") != -1)
                    {
                        ColumnString += "," + item;
                    }
                }
                stateOfUSAs = conn.Query<StateOfUSA>(@"Select " + ColumnString + " From StateOfUSA WHERE USAState=@USAState Order By [State]", new { USAState });
                conn.Close();
            }
            return stateOfUSAs;
        }

        public IEnumerable<StateOfUSA> GetStateOfUSAInStateID(string StateIDs)
        {
            IEnumerable<StateOfUSA> stateOfUSAs;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                stateOfUSAs = conn.Query<StateOfUSA>(string.Format("SELECT * FROM StateOfUSA WHERE StateID IN ({0})", StateIDs));
                conn.Close();
            }
            return stateOfUSAs;
        }

        public IEnumerable<Country> GetCountriesByLanguageID(int languageID)
        {
            IEnumerable<Country> countrys;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                List<string> ColumList = conn.Query<string>("SELECT COLUMN_NAME FROM  information_schema.columns WHERE  table_name = 'Country'").ToList();
                string ColumnString = "CountryID,CountryName,LanguageID,CountryCode";
                foreach (var item in ColumList)
                {
                    if (item.IndexOf("CountryName_") != -1)
                    {
                        ColumnString += "," + item;
                    }
                }
                countrys = conn.Query<Country>(@"Select " + ColumnString + @" From Country
                                            Where (LanguageID = @LanguageID) 
                                            ORDER BY CountryName",
                                        new { LanguageID = languageID });
                conn.Close();
            }
            return countrys;
        }
    }
}
