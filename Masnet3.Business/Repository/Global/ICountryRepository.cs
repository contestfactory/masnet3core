﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Global
{
    public interface ICountryRepository
    {
        IEnumerable<StateOfUSA> GetStateOfUSA(int USAState = 1);
        IEnumerable<StateOfUSA> GetStateOfUSAInStateID(string StateIDs);
        IEnumerable<Country> GetCountriesByLanguageID(int languageID);
    } 
}
