﻿using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Dapper;
namespace Masnet3.Business.Repository.MailArchives
{
    public class MailArchiveRepository : Masnet3Connection, IMailArchiveRepository
    {
        public MailArchiveRepository(
            IConfiguration configuration) : base(configuration)
        {
        }

        public byte GetMailStatus(string param)
        {
            var mailStoppedQty = 0;

            using (var conn = GetDbConection())
            {
                conn.Open();
                mailStoppedQty = conn.QueryFirstOrDefault<int>(@"
                    SELECT COUNT(*)
                    FROM TblMailQueue
                    WHERE [Param] = @param
                        AND Mail_Status = 5",
                    new
                    {
                        param
                    });
            }

            if (mailStoppedQty > 0)
            {
                return (byte)Enums.MailStatus.Stopped;
            }
            else
            {
                return (byte)Enums.MailStatus.Pending;
            }
        }
    }
}
