﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.MailArchives
{
    public interface IMailArchiveRepository
    {
        byte GetMailStatus(string param);
    }
}
