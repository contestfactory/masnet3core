﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Periods
{
    public interface IPeriodRepository
    {
        Period GetActivePeriod(Enums.UseType useType, Enums.UseType useTypeTemp, int ApplicationID = 0, int CampaignID = 0, int TemplateID = 0, bool autoCreate = false);
    }
}
