﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Infrastructure;
using Masnet3.Business.Utilities;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Linq;

namespace Masnet3.Business.Repository.Periods
{
    public class PeriodRepository : Masnet3Connection, IPeriodRepository
    {
        public PeriodRepository(IConfiguration configuration) : base(configuration) { }

        public Period GetActivePeriod(Enums.UseType useType, Enums.UseType useTypeTemp, int ApplicationID = 0, int CampaignID = 0, int TemplateID = 0, bool autoCreate = false)
        {
            Period result;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string sql = string.Format("SELECT * FROM Period WHERE (1=1) AND (IsActive = 1) ");
                sql += SqlUilities.GetFilter(useType, "ApplicationID", "CampaignId", "TemplateID", ApplicationID, CampaignID, TemplateID);

                result = conn.Query<Period>(sql, new
                {
                    ApplicationID = ApplicationID,
                    CampaignID = CampaignID,
                    TemplateID = TemplateID
                }).DefaultIfEmpty(new Period()).FirstOrDefault();

                if (result.PeriodID == 0 && CampaignID > 0 && ApplicationID > 0 && !autoCreate)
                {
                    result = GetActivePeriod(useType, useTypeTemp, ApplicationID, 0, TemplateID, autoCreate);
                }
                if (result.PeriodID <= 0 && autoCreate)
                {
                    sql = @"INSERT INTO Period( PeriodName,StartDate,EndDate,IsActive,ApplicationID,
                                    CampaignId,TemplateID,UseType,ShowHide,Date_Created, AutoCopy)
                            SELECT  PeriodName,StartDate,EndDate,IsActive,@ApplicationID,@CampaignID,
                                    @TemplateID,@UseType,ShowHide,Date_Created, AutoCopy 
                            FROM	Period 
                            WHERE	UseType = 1 AND isActive=1
                            SELECT  * FROM Period WHERE PeriodID = @@IDENTITY ";
                    result = conn.Query<Period>(sql, new { ApplicationID = ApplicationID, CampaignID = CampaignID, TemplateID = TemplateID, UseType = useTypeTemp }).FirstOrDefault();
                    conn.Close();
                }
                return result;
            }
        }
    }
}
