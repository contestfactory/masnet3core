﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Contestants
{
    public interface IContestantRepository
    {
        void FavoriteShelter(Favorite favorite);
        ContestantDetail GetNextHasNotVoted(int ContestID, int UserID, int applicationID, Enums.SiteType siteType, bool isJudge, string useJudgeIAB, bool? IAB, int vote_MaxJudges, bool isAirbnb);
    }
}
