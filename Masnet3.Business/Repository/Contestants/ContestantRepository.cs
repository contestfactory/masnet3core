﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Dapper;

namespace Masnet3.Business.Repository.Contestants
{
    public class ContestantRepository : Masnet3Connection, IContestantRepository
    {
        public ContestantRepository(IConfiguration configuration) : base(configuration) { }

        public void FavoriteShelter(Favorite favorite)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string sql = @"IF EXISTS (SELECT * FROM Favorite WHERE UserID = @UserID) 
                               BEGIN
                                  UPDATE Favorite SET ShelterID = @ShelterID
                                  WHERE UserID = @UserID AND FavoriteType = @FavoriteType
                               END
                               ELSE
                               BEGIN
                                  INSERT INTO Favorite(UserID,ShelterID,ApplicationID,CreatedDate,FavoriteType)
                                  VALUES (@UserID,@ShelterID,@ApplicationID,@CreatedDate,@FavoriteType)
                               END";
                conn.Execute(sql, favorite);
                conn.Close();
            }
        }

        public ContestantDetail GetNextHasNotVoted(int ContestID, int UserID, int applicationID, Enums.SiteType siteType, bool isJudge, string useJudgeIAB, bool? IAB, int vote_MaxJudges, bool isAirbnb)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string condition = " AND c.ContestID = @ContestID ";
                if (isJudge && siteType == Enums.SiteType.Customer)
                {
                    condition = " AND c.ApplicationID = @ApplicationID AND c.Status IN (6,7,11) AND round.JudgeWeight > 0 ";
                }

                string sql = @"  
                    ;WITH Voted AS(
                        SELECT * FROM Vote WHERE UserID = @UserID 
                    ) 
                    SELECT TOP 1 cd.* 
	                FROM ContestantDetail cd
                    LEFT JOIN Voted ON Voted.ContestantID = cd.ContestantID
                    JOIN ContestantEntry ce ON cd.ContestantEntryID = ce.ContestantID
                    JOIN Contest c ON cd.RoundID = c.ActiveRoundID
                    JOIN Round ON round.RoundID = cd.RoundID
	                WHERE 1=1 " + condition + @"                                    
                    AND c.IsDisplay = 1 AND ce.IsDisplay = 1 AND ce.IsDeleted = 0 AND ce.Status = 1 AND ce.IsFlagged = 0 ";

                if (isJudge)
                {
                    if (useJudgeIAB == "1")
                    {
                        if (!IAB.GetValueOrDefault())
                        {
                            sql += " AND ISNULL(ce.SendToIRB,0)=1";
                        }
                    }
                    //if (Commons.Config["Vote_MaxJudges"] != "0")
                    if (vote_MaxJudges > 0)
                    {
                        if (!IAB.GetValueOrDefault())
                        {
                            sql += " AND ISNULL(cd.JudgeCnt,0) < @Vote_MaxJudges ";
                        }
                    }
                    if (isAirbnb)
                    {
                        sql += " AND ce.CategoryID IN (SELECT CategoryID FROM membercat WHERE UserID = @UserID)";
                    }
                    sql += " AND (cd.InReview = 0 OR (cd.InReview = 1 AND ReviewBy = @UserID)) ";
                }

                sql += @" AND Voted.ContestantID IS NULL
                          ORDER BY ce.CategoryID, ce.CreatedDate ";

                var objContestantDetail = conn.QueryFirstOrDefault<ContestantDetail>(sql,
                    new
                    {
                        ContestID = ContestID,
                        ApplicationID = applicationID,
                        UserID = UserID,
                        vote_MaxJudges
                    });

                return objContestantDetail;
            }
        }
    }
}
