﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Linq;
using Masnet3.Lib;
using System;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository.Contestants;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Business.Repository.TblMailQueues;
using Masnet3.Business.Repository.Retailers;
using Masnet3.Business.Repository.Layout;
using System.Collections.Generic;

namespace Masnet3.Business.Repository.Customer
{
    public class RegistrationRepository : Masnet3Connection, IRegistrationRepository
    {
        private readonly ITblMailQueueRepository tblMailQueueRepository;
        private readonly IRetailerRepository retailerRepository;
        private readonly IContestantRepository contestantRepository;
        private readonly IThemeRepository themeRepository;

        public RegistrationRepository(
            IContestantRepository contestantRepository,
            IRetailerRepository retailerRepository,
            ITblMailQueueRepository tblMailQueueRepository,
            IThemeRepository themeRepository,
            IConfiguration configuration) : base(configuration) {
            this.tblMailQueueRepository = tblMailQueueRepository;
            this.retailerRepository = retailerRepository;
            this.themeRepository = themeRepository;
            this.contestantRepository = contestantRepository;
        }

        public Registration GetRegistrationByAppContestEmail(int? applicationID, int? contestID, string email)
        {
            Registration registration = new Registration();
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                registration = conn.QueryFirstOrDefault<Registration>("SELECT * FROM Registration WHERE ISNULL(ApplicationID, 0) = ISNULL(@ApplicationID, 0) AND ISNULL(ContestID, 0) = ISNULL(@ContestID, 0) AND Email = @Email", 
                    new { ApplicationID = applicationID, ContestID = contestID, Email = email });
                conn.Close();
            }
            return registration;
        }

        public bool EmailExists(string email, long? userId, int applicationId, int campaignId)
        {
            bool isEmailExists = false;

            if (!string.IsNullOrWhiteSpace(email))
            {
                using (var conn = this.GetDbConection())
                {
                    conn.Open();
                    string query = @"
                                    DECLARE @tempEmail varchar(255) = @Email
                                    SELECT TOP 1 *
                                    FROM (
                                        SELECT TOP 1 UserId
                                        FROM Registration
                                        WHERE ApplicationID = @ApplicationID AND ContestID = @ContestID 
                                        AND (Email = @tempEmail OR UserName = @tempEmail) 
                                        AND (UserId <> @UserId)
                                    UNION ALL
                                        SELECT TOP 1 UserId
                                        FROM Registration
                                        WHERE (Email = @tempEmail) AND UserType = 4 AND (UserId <> @UserId)
                                    ) r";

                    var values = conn.Query<dynamic>(query,
                        new
                        {
                            ApplicationID = applicationId,
                            ContestID = campaignId,
                            Email = email.Trim(),
                            UserId = userId ?? 0
                        });
                    conn.Close();
                    isEmailExists = (values.Count() == 0);
                }
            }

            return isEmailExists;
        }
        public bool IsUniqueUserName(string userName, int? applicationID = null)
        {
            int count = 0;
            using (var conn = GetDbConection())
            {
                conn.Open();
                count = conn.Query<int>("Select count(1) From Registration Where ((ApplicationID is NULL) AND (Username = @Username)) OR ((Username = @Username) AND (ApplicationID = @ApplicationID))", new { Username = userName, ApplicationID = applicationID }).FirstOrDefault();
                conn.Close();
            }
            return !(count > 0);
        }

        public bool IsUniqueEmail(string email, int UserID, int? CurrentPublishedContestID, bool IsInPublishContest, int? ContestID = null, int? applicationID = null)
        {
            int count = 0;
            using (var conn = GetDbConection())
            {
                conn.Open();
                string query = @"Select  count(1) 
                                From Registration 
                                Where (((ApplicationID is NULL) AND ((Email=@Email) OR (TempEmail=@Email))) 
                                    OR (((Email=@Email) OR (TempEmail=@Email)) AND (ApplicationID = @ApplicationID))) ";

                if (UserID == 0)
                {
                    if (IsInPublishContest || (ContestID > 0))
                    {
                        query += string.Format(" AND (ISNULL(ContestID, 0) = {0})", (CurrentPublishedContestID ?? ContestID));
                    }
                    else
                    {
                        query += string.Format(" AND (ISNULL(ContestID, 0) = 0)");
                    }

                    count = conn.Query<int>(query, new { Email = email, ApplicationID = applicationID }).FirstOrDefault();
                }
                else
                {
                    query += @" AND UserID <> @UserID ";
                    if (IsInPublishContest || (ContestID > 0))
                    {
                        query += string.Format(" AND (ContestID = {0})", (CurrentPublishedContestID ?? ContestID));
                    }
                    else
                    {
                        query += string.Format(" AND (ContestID = 0)");
                    }

                    count = conn.Query<int>(query, new { Email = email, ApplicationID = applicationID, UserID = UserID }).FirstOrDefault();
                }
            }

            return !(count > 0);
        }


        public Registration CreateRegistration(Registration registration, string registrationAutoConfirm, bool isBigData)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                bool reconn = false;
                if (string.IsNullOrWhiteSpace(registration.Password))
                {
                    registration.Password = Generator.PasswordGenerator(12, false);
                }
                if (string.IsNullOrWhiteSpace(registration.PasswordSalt))
                {
                    registration.PasswordSalt = Generator.PasswordGenerator(32, false);
                }
                if (string.IsNullOrWhiteSpace(registration.ValidationKey))
                {
                    registration.ValidationKey = Generator.PasswordGenerator(32, false);
                }
                if (string.IsNullOrWhiteSpace(registration.Email))
                {
                    registration.Email = "";
                }

                if (registration.RegisteredDate == null)
                {
                    registration.RegisteredDate = DateTime.Now;
                }

                string registration_AutoConfirm = registrationAutoConfirm;
                registration.Status = ((registration_AutoConfirm == "1" && registration.Under13 != "1" || (registration.RegistrationType != (int)Enums.RegistrationType.Site)) ? (byte)Enums.RegistrationStatus.Activated : (byte)Enums.RegistrationStatus.Pending);


                registration.Password = AESEncryption.Encrypt(registration.Password, registration.ValidationKey, registration.PasswordSalt);
                registration.IsPublicBirthday = true;
                registration.IsPublicCity = true;
                registration.IsPublicCountryID = true;
                registration.IsPublicEmail = true;
                registration.IsPublicFirstName = true;
                registration.IsPublicGender = true;
                registration.IsPublicLastName = true;
                registration.IsPublicProvine = true;
                registration.IsPublicStateID = true;
                registration.IsPublicZip = true;

                if (string.IsNullOrEmpty(registration.Phone))
                    registration.Phone = registration.Registration_Field_5;

                string query = @"INSERT INTO [Registration]([ApplicationID],[ContestID],[RetailerID],[RetailerStoreID],[Username],[Password],[PasswordSalt],[FirstName],[LastName],[Birthday],[Gender],[Address],[City],[StateID],[Provine],[Zip],[CountryID],[Email],[Phone],[Fax],[Photo],[Bio],[ValidationKey],[RegisteredDate],[ConfirmedDate],[IPAddress],[Status],[LanguageID],[IsUserPermission],[TenantID],[AlternativeEmail],[RoleID],[IsPublicEmail],[IsPublicFirstName],[IsPublicLastName],[IsPublicCity],[IsPublicCountryID],[IsPublicProvine],[IsPublicGender],[IsPublicBirthday],[IsPublicStateID],[IsPublicZip],[UserType],[CompanyName],[TempEmail], [HowHeard],[ShareId], [RegistrationType], [MongoID],[Newsletter],SS_ID,SS_CreatedDate, Registration_Field_4, CustUserType, Registration_Field_1, Registration_Field_2, Registration_Field_3, RepID)
                             VALUES(@ApplicationID,@ContestID,@RetailerID,@RetailerStoreID,@Username,@Password,@PasswordSalt,@FirstName,@LastName,@Birthday,@Gender,@Address,@City,@StateID,@Provine,@Zip,@CountryID,@Email,@Phone,@Fax,@Photo,@Bio,@ValidationKey,@RegisteredDate,@ConfirmedDate,@IPAddress,@Status,@LanguageID,@IsUserPermission,@TenantID,@AlternativeEmail,@RoleID,@IsPublicEmail,@IsPublicFirstName,@IsPublicLastName,@IsPublicCity,@IsPublicCountryID,@IsPublicProvine,@IsPublicGender,@IsPublicBirthday,@IsPublicStateID,@IsPublicZip,@UserType,@CompanyName,@TempEmail, @HowHeard,@ShareId, @RegistrationType,@MongoID,@Newsletter,@SS_ID,@SS_CreatedDate, @Registration_Field_4, @CustUserType, @Registration_Field_1, @Registration_Field_2, @Registration_Field_3,@RepID);
                             
                             SELECT CAST(SCOPE_IDENTITY() as bigint);";

                registration.UserID = conn.Query<long>(query, registration).FirstOrDefault();

                // Register Shelter
                if (registration.FavoriteID > 0)
                {
                    Favorite favorite = new Favorite
                    {
                        Id = 0,
                        CreatedDate = DateTime.Now,
                        ShelterID = registration.FavoriteID,
                        ApplicationID = registration.ApplicationID.GetValueOrDefault(),
                        UserID = registration.UserID,
                        FavoriteType = (int)Enums.FavoriteType.Shelter
                    };
                    this.contestantRepository.FavoriteShelter(favorite);
                }

                if (string.IsNullOrWhiteSpace(registration.Username))
                {
                    registration.Username = registration.UserID.ToString();
                    conn.Execute("UPDATE [Registration] SET [Username] = @Username WHERE [UserID]=@UserID", registration);
                }

                foreach (var regct in registration.RegistrationCustoms)
                {
                    regct.UserID = registration.UserID;
                    conn.Execute(@"INSERT INTO RegistrationCustom(UserID,CustomFieldID,Value) VALUES (@UserID,@CustomFieldID,@Value)", regct);
                }
                CreateDefaultFolder(registration.UserID);

                #region Insert data table ViralDNA when BigData
                if (isBigData)
                {
                    InsertViralDNA(registration.RegisteredDate.ToString("yyyy/MM/dd"), registration.ContestID.Value);
                }
                #endregion

                return registration;
            }
        }

        public Registration GetRegistrationByUserId(string userId)
        {
            Registration registration = new Registration();
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                registration = conn.QueryFirstOrDefault<Registration>("SELECT * FROM Registration WHERE UserID=@UserID", new { UserID = userId });
                conn.Close();
            }
            return registration;
        }

        public Registration GetPermissions(int userID)
        {
            if (userID <= 0)
            {
                return null;
            }
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string query = @"DECLARE @IsUserPermission bit
                                DECLARE @RoleID int
                                
                                SELECT @IsUserPermission = ISNULL(IsUserPermission, 0), @RoleID = RoleID
                                FROM [Registration]
                                WHERE [UserID] = @UserID

                                SELECT *
                                FROM [Registration]
                                WHERE [UserID] = @UserID

                                IF @IsUserPermission = 1
                                BEGIN
                                    SELECT p.*
                                    FROM UserPermission u INNER JOIN Permission p ON u.PermissionID = p.PermissionID
                                    WHERE u.UserID = @UserID
                                END
                                ELSE
                                BEGIN
                                    SELECT p.*
                                    FROM RolePermission r INNER JOIN Permission p ON r.PermissionID = p.PermissionID
                                    WHERE r.RoleID = @RoleID
                                END                                
                                
                                ";
                Registration registration;
                using (var grid = conn.QueryMultiple(query, new { UserID = userID }))
                {
                    registration = grid.ReadFirstOrDefault<Registration>();
                    if (registration != null)
                    {
                        if (registration.UserType == (int)Enums.RegistrationUserType.User
                            || registration.UserType == (int)Enums.RegistrationUserType.Judge)
                        {
                            if (registration.ContestID > 0)
                            {
                                registration.Permissions = grid.Read<Permission>().ToList();
                            }
                            else
                            {
                                registration.Permissions = grid.Read<Permission>().ToList();
                            }
                        }
                        else
                        {
                            registration.Permissions = grid.Read<Permission>().ToList();
                        }
                    }
                    conn.Close();
                }
                return registration;
            }
        }

        public Registration GetRegistrationByRepID(string repID)
        {
            Registration registration = null;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string sql = @"
                        SELECT r.*, ISNULL(r.FirstName,ar.FirstName) FirstName,ISNULL(r.LastName,ar.LastName) LastName
                        --,Email,Registration_Field_1,Registration_Field_2,Registration_Field_3,Registration_Field_4,
                        --Registration_Field_5,Address,City,StateID,Provine,Zip,CountryID
                        FROM Registration r 
                        RIGHT JOIN AvonRepID ar ON r.RepID = ar.RepID
                        WHERE ar.RepID = @repID ";

                registration = conn.QueryFirstOrDefault<Registration>(sql, new { repID });
                conn.Close();
            }
            return registration;
        }

        public void CreateDefaultFolder(long UserID)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();

                conn.Execute(@" INSERT INTO Folder(FolderName, ShortDesc, CreatedDate, CreatedBy, IsDisplay, PhotoCnt, AudioCnt, VideoCnt, DocumentCnt)
                                          VALUES ('Default', 'Default' ,GETDATE(), @UserID, 1, 0, 0, 0, 0)",
                             new
                             {
                                 UserID = UserID
                             });
                conn.Close();
            }
        }

        public void InsertViralDNA(string dCreatedDate, int contestId)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();
                conn.Execute(@"IF EXISTS
                                    (   SELECT 1 FROM ViralDNA WHERE DATE=@Date AND ContestID= @ContestID)
                                    UPDATE ViralDNA 
                                        SET RegistrationCount = RegistrationCount + 1 
                                        WHERE Date = @Date AND ContestID= @ContestID
                                ELSE
                                    INSERT INTO ViralDNA(Date, RegistrationCount, EntryCount, PendingEntryCount, RejectedEntryCount, ApprovedEntryCount, ContestID) 
                                    VALUES(@Date, 1, 0, 0, 0, 0, @ContestID)
                            ", new { Date = dCreatedDate, ContestID = contestId });
                conn.Close();
            }
        }

        public void CreateRegistrationToken(Registration registration)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();

                if (string.IsNullOrWhiteSpace(registration.Token) || DateTime.Now.CompareTo(registration.ExpiredDate ?? DateTime.Now.AddHours(-1)) > 0)
                {
                    
                    string sql = string.Format("UPDATE Registration SET Token = @Token, ExpiredDate = @ExpiredDate WHERE UserID = @UserID");
                    conn.Execute(sql, registration);
                }
                conn.Open();
            }
        }

        public void InsertMemberCat(int userID, int categoryId, int amount_per_vote)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();
                conn.Execute(@"
                        INSERT INTO MemberCat(UserID,CategoryID,amount_per_vote) 
                        VALUES (@UserID,@CategoryID,@amount_per_vote) ",
                        new
                        {
                            UserId = userID,
                            CategoryID = categoryId,
                            amount_per_vote = amount_per_vote
                        });
                conn.Close();
            }
        }

        public Enums.LoginStatus LoginMasNet3(string username, string password, int applicationID, int contestID, out Registration registration, bool? allowAnonymous = false, int loginType = 1, bool rememberLogin = false)
        {
            Enums.LoginStatus loginStatus = Enums.LoginStatus.InvalidUserNameOrPassword;

            using (var conn = this.GetDbConection())
            {
                conn.Open();
                //Admin login
                string query = string.Format(@"
                    SET ARITHABORT ON;
                    DECLARE @UserIDApplication INT, @UserIDContest INT, @TempUsername NVARCHAR(200) = @Username

                    SELECT TOP 1 @UserIDApplication = a.CreatedBy FROM [Application] a WHERE a.ApplicationID= @ApplicationID
                    SELECT TOP 1 @UserIDContest = c.CreatedBy FROM dbo.Contest c WHERE c.CreatedFrom={1} AND c.ContestID = @ContestID

                    SELECT *
                    FROM Registration 
                    WHERE (Username=@TempUsername OR Email=@TempUsername) AND ((UserID = @UserIDApplication)
	                        OR (UserID = @UserIDContest)
                            OR (UserType = {0}  OR (ContestID = @ContestID AND ApplicationID = @ApplicationID)))
                                                        
                    ", (int)Enums.RegistrationUserType.SuperAdmin, (int)Enums.ContestCreatedFrom.Campaign);

                //Normal login
                if (loginType == (int)Enums.LoginType.User)
                {
                    query = @"
                        SET ARITHABORT ON;
                        DECLARE @UserIDApplication INT, @UserIDContest INT, @TempUsername NVARCHAR(200) = @Username, @TempEmail VARCHAR(255) = @Username

                        SELECT UserID,UserType,Status,Password, ValidationKey, PasswordSalt
                        FROM Registration 
                        WHERE  ContestID = @ContestID 
                                AND ApplicationID = @ApplicationID 
                                AND Username=@Username   
                        UNION ALL
                        SELECT UserID,UserType,Status,Password, ValidationKey, PasswordSalt
                        FROM Registration 
                        WHERE  ContestID = @ContestID 
                                AND ApplicationID = @ApplicationID 
                                AND Email= @TempEmail   
                        ";

                }

                registration = conn.QueryFirstOrDefault<Registration>(query, new { Username = username.Trim(), ApplicationID = applicationID, ContestID = contestID });
                conn.Close();


                if (registration != null && registration.UserID > 0)
                {
                    if (allowAnonymous == true)
                    {
                        registration = GetPermissions((int)registration.UserID);
                        if (registration.UserType == (int)Enums.RegistrationUserType.User && (registration.Permissions == null || registration.Permissions.Count == 0))
                        {
                            switch (registration.Status)
                            {
                                case ((int)Enums.RegistrationStatus.Pending):
                                    loginStatus = Enums.LoginStatus.Pending;
                                    break;
                                case ((int)Enums.RegistrationStatus.Blocked):
                                    loginStatus = Enums.LoginStatus.Blocked;
                                    break;
                                case ((int)Enums.RegistrationStatus.Disabled):
                                    loginStatus = Enums.LoginStatus.Disabled;
                                    break;
                                default:
                                    loginStatus = Enums.LoginStatus.Success;
                                    break;
                            }
                        }
                        else
                        {
                            loginStatus = Enums.LoginStatus.RequiredLogin;
                        }
                    }
                    else
                    {
                        if (password == AESEncryption.Decrypt(registration.Password, registration.ValidationKey, registration.PasswordSalt))
                        {
                            switch (registration.Status)
                            {
                                case ((int)Enums.RegistrationStatus.Pending):
                                    loginStatus = Enums.LoginStatus.Pending;
                                    break;
                                case ((int)Enums.RegistrationStatus.Blocked):
                                    loginStatus = Enums.LoginStatus.Blocked;
                                    break;
                                case ((int)Enums.RegistrationStatus.Disabled):
                                    loginStatus = Enums.LoginStatus.Disabled;
                                    break;
                                default:
                                    loginStatus = Enums.LoginStatus.Success;
                                    registration = GetPermissions((int)registration.UserID);
                                    break;
                            }
                        }
                    }
                }
            }

            return loginStatus;
        }

        public void CreateBookingIntention(BookingIntention model)
        {
            using (var conn = GetDbConection())
            {
                conn.Open();

                var sql = @"
                    INSERT BookingIntention (
                        UserId,
                        Variety,
                        Amount
                    )
                    VALUES (
                        @userId,
                        @variety,
                        @amount
                    );";

                conn.Execute(sql,
                    new
                    {
                        userId = model.UserId,
                        variety = model.Variety,
                        amount = model.Amount
                    });
                conn.Close();
            }
        }

        public void SendAdminRegistration(Registration registration,
            int ApplicationID,
            int CampaignID,
            int LanguageID,
            string BouncedEmail,
            Enums.UseType useType)
        {
            if (registration.RetailerID > 0)
            {
                registration.Registration_Field_4 = this.retailerRepository.GetRetailerById(registration.RetailerID.GetValueOrDefault()).RetailerName;
            }

            string templatename = "Admin - Registration";
            Template template = this.themeRepository.GetTemplatesByName(useType, templatename, ApplicationID, CampaignID, 0, LanguageID);
            if (template != null && !string.IsNullOrEmpty(template.EmailTo))
            {
                Dictionary<string, string> tokens = new Dictionary<string, string>() {
                            {"%TemplateName%", templatename},
                            {"%ApplicationID%", registration.ApplicationID.ToString()},
                            {"%ContestID%", registration.ContestID.ToString()},
                            {"%LanguageID%", registration.LanguageID.ToString()},
                            {"%Email%", registration.Email},
                            {"%FirstName%", (registration.FirstName ?? registration.Username)},
                            {"%LastName%", registration.LastName},
                            {"%RetailerName%", registration.Registration_Field_4},
                            {"%RegistrationDate%", registration.RegisteredDate.ToShortDateString() }
                        };
                //EmailUtilities.ReplaceToken(template.Body, tokens);
                this.tblMailQueueRepository.Insert(new TblMailQueue()
                {
                    Mail_From = template.EmailFrom,
                    Mail_To = template.EmailTo,
                    Mail_IsBodyHtml = true,
                    Mail_Subject = StringToken.ReplaceToken(template.Subject, tokens),
                    Mail_Body = StringToken.ReplaceToken(template.Body, tokens),
                    Mail_Retried = 0,
                    Mail_Status = (int)Enums.MailStatus.Pending,
                    BouncedEmail = BouncedEmail
                });
            }
        }

        public void SendActivationEmail(Registration registration,
            int ApplicationID,
            int CampaignID,
            int LanguageID,
            string emailFromAddress,
            string CustomerService,
            string UnsubscribeEmail,
            Enums.UseType useType,
            string activationLink, string registrationAutoConfirm)
        {
            string templatename = "";
            if (registrationAutoConfirm == "1" && registration.Under13 != "1")
                templatename = "Register - Auto Authentication";
            else
                templatename = "Register";

            Template template = this.themeRepository.GetTemplatesByName(useType, templatename, ApplicationID, CampaignID, 0, LanguageID);
            if (template != null)
            {
                
                Dictionary<string, string> tokens = new Dictionary<string, string>() {
                            {"%TemplateName%", templatename},
                            {"%ApplicationID%", registration.ApplicationID.ToString()},
                            {"%ContestID%", registration.ContestID.ToString()},
                            {"%LanguageID%", registration.LanguageID.ToString()},
                            {"%UserID%", registration.UserID.ToString()},
                            {"%ActivationLink%", activationLink},
                            {"%Email%", registration.EmailToSend},
                            {"%FirstName%", (registration.FirstName ?? registration.Username)},
                            {"%CustomerService%", CustomerService},
                            {"%UnsubscribeEmail%", UnsubscribeEmail},
                            {"%LastName%", registration.LastName},
                            {"%Username%", registration.Username},
                            {"%ValidationKey%", registration.ValidationKey},
                            {"%BouncedEmail%", emailFromAddress},
                            {"%DateOfBirth%", registration.Birthday.HasValue ? registration.Birthday.Value.ToShortDateString() : "" }
                        };

                template.EmailTo = registration.EmailToSend;
                string BouncedEmail = emailFromAddress;
                themeRepository.SendEmail(template, tokens, ApplicationID, BouncedEmail);
            }
        }

    }
}
