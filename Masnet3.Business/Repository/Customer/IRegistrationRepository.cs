﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using MASNET.DataAccess.Model.Custom;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Masnet3.Business.Repository.Customer
{
    public interface IRegistrationRepository
    {
        Registration GetRegistrationByAppContestEmail(int? applicationID, int? contestID, string email);
        Registration GetPermissions(int userID);
        Registration GetRegistrationByUserId(string userId);
        Registration GetRegistrationByRepID(string repID);

        bool EmailExists(string email, long? userId, int applicationId, int campaignId);
        bool IsUniqueUserName(string userName, int? applicationID = null);
        bool IsUniqueEmail(string email, int UserID, int? CurrentPublishedContestID, bool IsInPublishContest, int? ContestID = null, int? applicationID = null);

        void CreateDefaultFolder(long UserID);
        void InsertViralDNA(string dCreatedDate, int contestId);
        Registration CreateRegistration(Registration registration, string registrationAutoConfirm, bool isBigData);
        void CreateRegistrationToken(Registration registration);

        void InsertMemberCat(int UserID, int categoryId, int amount_per_vote);

        Enums.LoginStatus LoginMasNet3(
            string username,
            string password,
            int applicationID,
            int contestID,
            out Registration registration, bool? allowAnonymous = false, int loginType = 1, bool rememberLogin = false);

        void CreateBookingIntention(BookingIntention model);

        void SendAdminRegistration(Registration registration,
            int ApplicationID,
            int CampaignID,
            int LanguageID,
            string BouncedEmail,
            Enums.UseType useType);
        void SendActivationEmail(Registration registration,
            int ApplicationID,
            int CampaignID,
            int LanguageID,
            string emailFromAddress,
            string CustomerService,
            string UnsubscribeEmail,
            Enums.UseType useType,
            string activationLink, string registrationAutoConfirm);
    }
}
