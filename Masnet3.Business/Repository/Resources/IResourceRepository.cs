﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Resources
{
    public interface IResourceRepository
    {
        IEnumerable<Resource> GetResource(int applicationID, int contestID);
        IEnumerable<Resource>  GetResources(
            Enums.UseType useType,
            Dictionary<string, MinifiedApplication> dicApplications,
            Dictionary<string, MinifiedContest> dicContests,
            int contestTemplateID, 
            int applicationId, 
            int contestId, 
            int templateId, 
            string resourceKey = null, 
            string value = null, 
            int? pageID = null, 
            string sortColumn = "", 
            string sortDirection = "", 
            int pageindex = 0, 
            int? pagesize = null, 
            bool? isContestPages = false, 
            int? languageID = null, 
            bool isFormPreview = false, 
            bool isPrivate = false, 
            List<Resource> listKeys = null,
            bool canEditShowHide = true);
        IEnumerable<Resource> GetResourceFromCache(int cacheType, string cacheFilter, int applicationID, int contestID, string pageName, int userType);
    }
}
