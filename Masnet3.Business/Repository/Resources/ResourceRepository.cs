﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using Dapper;
using System.Collections.Generic;
using System.Linq;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Utilities;

namespace Masnet3.Business.Repository.Resources
{
    public class ResourceRepository : Masnet3Connection, IResourceRepository
    {
        public ResourceRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<Resource> GetResource(int applicationID, int contestID)
        {
            IEnumerable<Resource> resources;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                resources = conn.Query<Resource>("SELECT * FROM Resource WHERE ApplicationID= @ApplicationID AND ContestID= @ContestID ORDER BY PageID, ResourceKey", new { ApplicationID = applicationID, ContestID = contestID });
                conn.Close();
            }
            return resources;
        }

        public IEnumerable<Resource> GetResources(
            Enums.UseType useType,
            Dictionary<string, MinifiedApplication> dicApplications,
            Dictionary<string, MinifiedContest> dicContests,
            int contestTemplateID,
            int applicationId, 
            int contestId, 
            int templateId,
            string resourceKey = null, 
            string value = null, 
            int? pageID = null, 
            string sortColumn = "", 
            string sortDirection = "", 
            int pageindex = 0, 
            int? pagesize = null, 
            bool? isContestPages = false, 
            int? languageID = null, 
            bool isFormPreview = false, 
            bool isPrivate = false, 
            List<Resource> listKeys = null,
            bool canEditShowHide = true)
        {
            if (string.IsNullOrWhiteSpace(sortColumn))
            {
                sortColumn = "[Page].PageName";
            }
            if (string.IsNullOrWhiteSpace(sortDirection))
            {
                sortDirection = "ASC";
            }
            if (pageindex < 0)
            {
                pageindex = 0;
            }
            if (pagesize.HasValue && pagesize.Value < 0)
            {
                pagesize = 20;
            }
            string selectClause = "r.*, p.PageName";
            string fromClause = "Resource r " + SqlUilities.GetUnionTable(useType, dicApplications, dicContests, contestTemplateID, "r", "{0}.PageID = {1}.PageID AND {0}.ResourceKey = {1}.ResourceKey AND {0}.LanguageID = {1}.LanguageID", applicationId, contestId, templateId, false, languageID.ToString()) +
                                " LEFT JOIN [Page] p ON r.PageID = p.PageID";

            string orderByClause = string.Format("{0} {1}", sortColumn, sortDirection);
            if (string.IsNullOrWhiteSpace(sortColumn) || sortColumn.ToLower().Contains("pagename"))
            {
                orderByClause = "Priority ASC, " + orderByClause;
            }

            orderByClause += ", ResourceKey ASC";

            string whereClause = "1 = 1 AND IsDeveloperKey = 0 ";
            whereClause += SqlUilities.GetShowHide(useType, "r.ShowHide");
            if (isFormPreview)
            {
                whereClause +=
                    string.Format(
                        @" AND (r.ResourceKey LIKE N'%ButtonRegister%' OR r.ResourceKey LIKE N'%Register_Instruction%' OR r.ResourceKey LIKE N'%Register_Header%')
	                                        AND (@Value IS NULL OR r.Value LIKE (N'%' + dbo.[fn_Escape](@Value) + '%') OR r.HelpText LIKE (N'%' + dbo.[fn_Escape](@Value) + '%'))
	                                        AND (@PageID IS NULL OR r.PageID = @PageID ");
            }
            else
            {
                whereClause +=
                    string.Format(
                        @" AND (@ResourceKey IS NULL OR r.ResourceKey LIKE N'%' + dbo.[fn_Escape](@ResourceKey) + '%' OR r.KeyLabel LIKE N'%' + dbo.[fn_Escape](@ResourceKey) + '%')
	                                        AND (@Value IS NULL OR r.Value LIKE (N'%' + dbo.[fn_Escape](@Value) + '%') OR r.HelpText LIKE (N'%' + dbo.[fn_Escape](@Value) + '%'))
	                                        AND (@PageID IS NULL OR r.PageID = @PageID ");
            }

            if (!applicationId.Equals(-1) || !contestId.Equals(-1))
                whereClause += " AND IsDisplay=1 ";

            // bad performance
            //if (pageID >= 0)
            //{
            //    whereClause += string.Format(@"OR (',' + RTRIM(LTRIM(ISNULL(ParentID,''))) + ',') LIKE N'%,{0},%'", pageID);
            //}

            whereClause += ")";

            if (languageID.GetValueOrDefault() > 0)
            {
                whereClause += string.Format(" AND r.LanguageID = {0} ", languageID ?? 0);
            }
            if ((useType == Enums.UseType.Site || useType == Enums.UseType.Campaign) && !canEditShowHide)
            {
                whereClause += string.Format(" AND r.ShowHide IN ({0},{1})", (int)Enums.DisplayItemOption.Edit, (int)Enums.DisplayItemOption.View);
            }

            if (isPrivate)
                whereClause += SqlUilities.GetFilter(useType, applicationId, contestId, templateId);

            //validation ruels
            List<string> resourceKeys = new List<String>();
            if (listKeys != null && listKeys.Count() > 0)
            {
                whereClause += string.Format("AND r.ResourceKey IN @ResourceKeys");
                resourceKeys = listKeys.Select(x => x.ResourceKey).ToList();
            }
            dynamic param = new
            {
                ApplicationID = applicationId,
                ContestID = (applicationId == -1) ? -1 : contestId,
                ResourceKey = resourceKey,
                Value = value,
                PageID = pageID,
                TemplateID = templateId,
                ResourceKeys = resourceKeys,
                UseType = useType
            };
            IEnumerable<Resource> resources = new List<Resource>();
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                resources = DapperPaging.QueryPaging<Resource>(conn, selectClause, fromClause, whereClause, orderByClause, param, pageindex, pagesize);
                //validation ruels
                if (listKeys != null && listKeys.Count() > 0)
                {
                    //resources = resources.Where(r => (listKeys.Where(k => (k.ResourceKey == r.ResourceKey && k.PageID == r.PageID)).FirstOrDefault() != null)).ToPagedList(query.PageIndex, query.PageSize, query.TotalItemCount);
                    resources = resources.Where(r => (listKeys.Where(k => (k.ResourceKey == r.ResourceKey && k.PageID == r.PageID)).FirstOrDefault() != null));
                }
                conn.Close();
            }
            return resources;
        }

        public IEnumerable<Resource> GetResourceFromCache(int cacheType, string cacheFilter, int applicationID, int contestID, string pageName, int userType)
        {
            IEnumerable<Resource> resources;
            string query = string.Format(
                                  @"SELECT * FROM [Resource]
                                    WHERE 1=1 AND (IsDeleted = 0) {0}
	                                AND " + (cacheType == 0 ? "ISNULL(PageID, 0) = 0" : " PageID = (SELECT TOP 1 PageID FROM [Page] WHERE PageName = @PageName) ") + @" 
                                    ORDER BY ResourceKey", cacheFilter);
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                resources = conn.Query<Resource>(query, new { ApplicationID = applicationID, ContestID = contestID, PageName = pageName, UseType = userType });
                conn.Close();
            }
            return resources;
        }
    }
}
