﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Layout
{
    public interface IThemeRepository
    {
        IEnumerable<Theme> GetListTheme();

        Template GetTemplatesByName(Enums.UseType useType, int TemplateKeyID, int ApplicationID = 0, int ContestID = 0, int editTemplateID = 0, int LanguageID = 1);
        Template GetTemplatesByName(Enums.UseType useType, Enums.TemplateKey templateKey, int ApplicationID = 0, int ContestID = 0, int editTemplateID = 0, int LanguageID = 1);
        Template GetTemplatesByName(Enums.UseType useType, string name, int ApplicationID = 0, int ContestID = 0, int editTemplateID = 0, int LanguageID = 1);
        Theme GetThemeById(int themeId);
        //
        Tuple<List<DisplayItem>, List<CustomFields>, List<CustomFieldValues>> Get_DisplayItem_CustomFields_CustomFieldValues(
            bool hasCustomFieldSite, string joinClause, string filters,
            string pageName, int applicationID, int contestID, int languageID);

        void SendEmail(Template template, Dictionary<string, string> tokensDictionary, int? applicationID = null, string BouncedEmail = "", string attachment = "", MailArchive adhoc = null);

    }
}
