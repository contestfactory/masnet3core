﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Collections.Generic;

namespace Masnet3.Business.Repository.Layout
{
    public class PageRepository : Masnet3Connection, IPageRepository
    {
        public PageRepository(IConfiguration configuration) : base(configuration) { }

        public Page GetPageByName(string pageName)
        {
            Page page = new Page();
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                page = conn.QueryFirstOrDefault<Page>("SELECT * FROM [Page] Where PageName = @PageName", new { PageName = pageName });
                conn.Close();
            }
            return page;
        }

        public IEnumerable<MASNET.DataAccess.Page> GetPageById(int pageId)
        {
            IEnumerable<MASNET.DataAccess.Page> pages;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                pages = conn.Query<Page>("SELECT * FROM Page WHERE PageID = @PageID", new { PageID = pageId });
                conn.Close();
            }
            return pages;
        }

        public IEnumerable<PagePosition> GetPagePosition(int pageID, int applicationId, int campaignID)
        {
            IEnumerable<PagePosition> pagePositions;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                pagePositions = conn.Query<PagePosition>("SELECT * FROM PagePosition WHERE PageID=@PageID AND ApplicationID=@ApplicationID AND CampaignID=@CampaignID", new { PageID = pageID, ApplicationID = applicationId, CampaignID = campaignID });
                conn.Close();
            }
            return pagePositions;
        }

        public IEnumerable<Banner> GetShownBannerInSharedPosition(int pageID, PagePosition pos1, PagePosition pos2, int applicationID, int campaignId)
        {
            IEnumerable<Banner> banners;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                string sql = @"
                    SELECT TOP 1 * FROM (
                        SELECT b.*,@Pos1 As ShownPosition
                        FROM PageBanner pb
                    INNER JOIN Banner b 
                        ON pb.BannerID = b.BannerID
                    LEFT JOIN PagePosition PP
                        ON pb.Position = PP.PositionID
                            AND b.ApplicationID = PP.ApplicationID
                            AND b.CampaignID = PP.CampaignID
                            AND PP.PageID = 0
                        WHERE 
                                (
                                    (@Pos1UseGlobal = 1 AND ( pb.PageID = @PageID OR pb.PageID = 0 )
                                        AND ISNULL(PP.IsDisplay, 0) = 1
                                    )
                                    OR
                                    (@Pos1UseGlobal = 0 AND pb.PageID = @PageID AND @Pos1IsDisplay = 1)
                                )
                                AND pb.Position = @Pos1
                                AND b.IsDisplay = 1  
                                AND b.ApplicationID = @ApplicationID
                                AND b.CampaignID = @CampaignID                              
                        UNION
                        SELECT b.*,@Pos2 As ShownPosition
                        FROM PageBanner pb
                    INNER JOIN Banner b 
                        ON pb.BannerID = b.BannerID
                    LEFT JOIN PagePosition PP
                        ON pb.Position = PP.PositionID
                            AND b.ApplicationID = PP.ApplicationID
                            AND b.CampaignID = PP.CampaignID
                            AND PP.PageID = 0
                        WHERE 
                                (
                                    (@Pos2UseGlobal = 1 AND ( pb.PageID = @PageID OR pb.PageID = 0 )
                                        AND ISNULL(PP.IsDisplay, 0) = 1
                                    )
                                    OR
                                    (@Pos2UseGlobal = 0 AND pb.PageID = @PageID AND @Pos2IsDisplay = 1)
                                )
                                AND pb.Position = @Pos2
                                AND b.IsDisplay = 1
                                AND b.ApplicationID = @ApplicationID
                                AND b.CampaignID = @CampaignID
                    ) AS BannersList
                    ORDER BY (ISNULL([Views],0) % (ISNULL([Weight],1)+1))/CAST(ISNULL([Weight],1)+1 As Float)";

                banners = conn.Query<Banner>(sql, new
                {
                    PageID = pageID,
                    Pos1 = pos1.PositionID,
                    Pos1IsDisplay = pos1.IsDisplay,
                    Pos1UseGlobal = pos1.UseGlobal.GetValueOrDefault(),
                    Pos2 = pos1.PositionID,
                    Pos2IsDisplay = pos2.IsDisplay,
                    Pos2UseGlobal = pos2.UseGlobal.GetValueOrDefault(),
                    ApplicationID = applicationID,
                    CampaignID = campaignId
                });
                conn.Close();
            }
            return banners;
        }

        public IEnumerable<Banner> GetShownBannerInPostion(int PageID, PagePosition Pos1, int WidthConstrainst = 0, int applicationID = 0, int campaignId = 0)
        {
            IEnumerable<Banner> banners;
            using (var conn = this.GetDbConection())
            {
                conn.Open();

                string sql = @"
                        SELECT TOP 1 b.*,@Pos1 As ShownPosition
                          FROM PageBanner pb
                    INNER JOIN Banner b ON pb.BannerID = b.BannerID
                         WHERE 
                                (
                                    (@Pos1UseGlobal = 1 AND ( PageID = @PageID OR PageID = 0 ))
                                    OR
                                    (@Pos1UseGlobal = 0 AND PageID = @PageID)
                                )
                                AND Position = @Pos1   
                                AND b.IsDisplay = 1   
                                AND b.ApplicationID = @ApplicationID
                                AND b.CampaignID = @CampaignID                  
                                AND ( @WidthConstrainst = 0 OR b.Width = @WidthConstrainst )
                      ORDER BY (ISNULL(b.[Views],0) % (ISNULL(b.[Weight],1)+1))/CAST(ISNULL(b.[Weight],1)+1 As Float)";

                banners = conn.Query<Banner>(sql, new { PageID = PageID, Pos1 = Pos1.PositionID, Pos1UseGlobal = Pos1.UseGlobal.GetValueOrDefault(), WidthConstrainst = WidthConstrainst, ApplicationID = applicationID, CampaignID = campaignId });
            }
            return banners;
        }

        public int UpdateBannerSetView(int bannerID)
        {
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                int rs = conn.Execute("UPDATE Banner SET [Views] = ISNULL([Views],0) + 1 WHERE BannerID = @BannerID", new { BannerID = bannerID });
                conn.Close();
                return rs;
            }
        }
    }
}
