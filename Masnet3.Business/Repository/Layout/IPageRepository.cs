﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Layout
{
    public interface IPageRepository
    {
        MASNET.DataAccess.Page GetPageByName(string pageName);
        IEnumerable<MASNET.DataAccess.Page> GetPageById(int pageId);
        IEnumerable<PagePosition> GetPagePosition(int pageID, int applicationId, int campaignID);
        IEnumerable<Banner> GetShownBannerInSharedPosition(int pageID, PagePosition pos1, PagePosition pos2, int applicationID, int campaignId);
        IEnumerable<Banner> GetShownBannerInPostion(int PageID, PagePosition Pos1, int WidthConstrainst = 0, int applicationID = 0, int campaignId = 0);
        int UpdateBannerSetView(int bannerID);
    }
}
