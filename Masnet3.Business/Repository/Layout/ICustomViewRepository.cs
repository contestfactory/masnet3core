﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.CustomViews
{
    public interface ICustomViewRepository
    {
        IEnumerable<CustomView> GetList();
    }
}
