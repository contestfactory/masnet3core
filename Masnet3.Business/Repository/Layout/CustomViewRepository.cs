﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Dapper;

namespace Masnet3.Business.Repository.CustomViews
{
    public class CustomViewRepository : Masnet3Connection, ICustomViewRepository
    {
        public CustomViewRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<CustomView> GetList()
        {
            IEnumerable<CustomView> customViews;
            using (var con = this.GetDbConection())
            {
                con.Open();
                customViews = con.Query<CustomView>("SELECT * FROM [CustomView]");
                con.Close();
            }
            return customViews;
        }
    }
}
