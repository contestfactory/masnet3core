﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Dapper;
using System.Collections.Generic;
using System;
using System.Linq;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Utilities;
using Masnet3.Business.Repository.TblMailQueues;
using Masnet3.Lib;
using Newtonsoft.Json;
using Masnet3.Business.Repository.MailArchives;

namespace Masnet3.Business.Repository.Layout
{
    public class ThemeRepository : Masnet3Connection, IThemeRepository
    {
        static Dictionary<string, Enums.TemplateKey> names = null;
        private readonly IMailArchiveRepository mailArchiveRepository;
        private readonly ITblMailQueueRepository tblMailQueueRepository;

        public ThemeRepository(IMailArchiveRepository mailArchiveRepository, ITblMailQueueRepository tblMailQueueRepository, IConfiguration configuration) : base(configuration) {

            this.mailArchiveRepository = mailArchiveRepository;
            this.tblMailQueueRepository = tblMailQueueRepository;
            if (names == null)
            {
                names = new Dictionary<string, Enums.TemplateKey>();
                names.Add("Share", Enums.TemplateKey.Action_Share);
                names.Add("ForgotPassword", Enums.TemplateKey.SEM_ForgotPassword);
                names.Add("Judges - Next Round", Enums.TemplateKey.SEM_JudgesNextRound);
                names.Add("Invitation from Your Friend", Enums.TemplateKey.SEM_InvitationfromYourFriend);
                names.Add("Register", Enums.TemplateKey.SEM_Register);
                names.Add("Successful submission", Enums.TemplateKey.SEM_Successfulsubmission);
                names.Add("Congratulations! You won - Free", Enums.TemplateKey.SEM_CongratulationsYouwonFree);
                names.Add("UserChangeEmailAddress", Enums.TemplateKey.SEM_UserChangeEmailAddress);
                names.Add("CommentApproval", Enums.TemplateKey.SEM_CommentApproval);
                names.Add("Manual Contestant Approval", Enums.TemplateKey.SEM_ManualContestantApproval);
                names.Add("Contest-Your contest has started", Enums.TemplateKey.SEM_ContestYourcontesthasstarted);
                names.Add("Vote", Enums.TemplateKey.Action_Vote);
                names.Add("Submit", Enums.TemplateKey.Action_Submit);
                names.Add("Comment", Enums.TemplateKey.Action_Comment);
                names.Add("Admin - Contact Us", Enums.TemplateKey.SEM_AdminContactUs);
                names.Add("Judge - New Submission", Enums.TemplateKey.SEM_JudgeNewSubmission);
                names.Add("Judge - New DRTV Submission", Enums.TemplateKey.SEM_JudgeNewDRTVSubmission);
                names.Add("DRTV Rejection Letter General", Enums.TemplateKey.SEM_DRTVRejectionLetterGeneral);
                names.Add("Register - Auto Authentication", Enums.TemplateKey.SEM_RegisterAutoAuthentication);
                names.Add("Admin - Registration", Enums.TemplateKey.SEM_AdminRegistration);
                names.Add("Successful Submission for Phone Review", Enums.TemplateKey.SEM_SuccessfulSubmissionforPhoneReview);
                names.Add("Successful Submission for Promo Code", Enums.TemplateKey.SEM_SuccessfulSubmissionforPromoCode);
                names.Add("Admin - Pending Submission", Enums.TemplateKey.SEM_AdminPendingSubmission);
                names.Add("Successful Submission – Pending Approval", Enums.TemplateKey.SEM_SuccessfulSubmissionPendingApproval);
                names.Add("Spin - You are the winner", Enums.TemplateKey.SEM_SpinYouarethewinner);
                names.Add("Spin", Enums.TemplateKey.Action_Spin);
                names.Add("DRTV Contact Inventor", Enums.TemplateKey.SEM_DRTVContactInventor);
            }
        }

        public IEnumerable<Theme> GetListTheme()
        {
            IEnumerable<Theme> themes;
            using (var con = this.GetDbConection())
            {
                con.Open();
                themes = con.Query<Theme>("SELECT * FROM Theme");
                con.Close();
            }
            return themes;
        }

        public Theme GetThemeById(int themeId)
        {
            Theme theme;
            using (var con = this.GetDbConection())
            {
                con.Open();
                theme = con.QueryFirst<Theme>("SELECT * FROM Theme WHERE ID=@ThemeID", new { ThemeID = themeId });
                con.Close();
            }
            return theme;
        }


        public Template GetTemplatesByName(Enums.UseType useType, int TemplateKeyID, int ApplicationID = 0, int ContestID = 0, int editTemplateID = 0, int LanguageID = 1)
        {
            Template template;
            string sql = "";
            using (var conn = GetDbConection())
            {
                conn.Open();
                sql = @"Select * from Template " +
                       SqlUilities.Join(conn, new
                       {
                           LeftTable = "Template",
                           Table = "Template",
                           IDField = "TemplateID",
                           JoinFields = "TemplateKeyID,LanguageID",
                           editCampaignID = ContestID,
                           editApplicationID = ApplicationID,
                           editTemplateID = editTemplateID
                       }, useType) +
                        @"where (IsDisplay = 1) 
                            AND TemplateKeyID = @TemplateKeyID AND ISNULL(LanguageID,1)=@LanguageID ";
                template = conn.QueryFirstOrDefault<Template>(sql,
                                                            new
                                                            {
                                                                TemplateKeyID = TemplateKeyID,
                                                                ApplicationID = ApplicationID,
                                                                ContestID = ContestID,
                                                                ContestTemplateID = editTemplateID,
                                                                LanguageID = LanguageID
                                                            });
                conn.Close();
            }
            return template;
        }
        public Template GetTemplatesByName(Enums.UseType useType, Enums.TemplateKey templateKey, int ApplicationID = 0, int ContestID = 0, int editTemplateID = 0, int LanguageID = 1)
        {
            return GetTemplatesByName(useType, (int)templateKey, ApplicationID, ContestID, editTemplateID, LanguageID);
        }
        public Template GetTemplatesByName(Enums.UseType useType, string name, int ApplicationID = 0, int ContestID = 0, int editTemplateID = 0, int LanguageID = 1)
        {
            if (names.ContainsKey(name))
            {
                return GetTemplatesByName(useType, names[name], ApplicationID, ContestID, editTemplateID, LanguageID);
            }
            return null;
        }

        public Tuple<List<DisplayItem>, List<CustomFields>, List<CustomFieldValues>> Get_DisplayItem_CustomFields_CustomFieldValues(
            bool hasCustomFieldSite, string joinClause, string filters,
            string pageName, int applicationID, int contestID, int languageID)
        {
            List<DisplayItem> displayItems = new List<DisplayItem>();
            List<CustomFields> customFields = new List<CustomFields>();
            List<CustomFieldValues> customFieldValues = new List<CustomFieldValues>();

            string sql = "";
            if(hasCustomFieldSite)
            {
                sql = @"DECLARE @PageID INT
                            SELECT @PageID = PageID FROM [Page] WHERE PageName = @PageName
                            SELECT * FROM dbo.DisplayItem "
                            + joinClause +
                            @"
                            WHERE 1=1 
                                  AND ISNULL(PageID, 0) = @PageID
                                  AND LanguageID = @LanguageID 
                            AND IsDisplay = 1
                            ORDER BY DisplayItem.DisplayOrder

                            SELECT cf.CustomFieldID,
                                   cf.FieldName,
                                   cf.HelpText,
                                   cfs.IsRequired,
                                   cf.ControlType,
                                   cfs.ContestID,
                                   cf.Priority,
                                   cfs.ApplicationID,
                                   cf.PageID,
                                   cfs.ShowInSignUp,
                                   cfs.ShowInProfile,
                                   cf.ContestTemplateID,
                                   cf.DisplayToAgent
                            FROM CustomFieldSite cfs
                            INNER JOIN CustomFields cf ON cfs.CustomFieldID = cf.CustomFieldID                             
                            WHERE 1=1 " +
                            filters +
                               @" AND ISNULL(cfs.ShowInSignUp,0) = 1    
                                  AND ISNULL(cf.QNID,0) = 0   ORDER BY cf.Priority,cf.CustomFieldID;

                            SELECT cfv.* 
                            FROM CustomFieldValues cfv INNER JOIN CustomFields cf ON cfv.CustomFieldID = cf.CustomFieldID
                            WHERE ISNULL(cf.ApplicationID, -1) = -1 
                                AND ISNULL(cf.ContestID, -1) = -1                                                                 
                             ORDER BY cfv.CustomFieldID, cfv.Priority";
            }
            else
            {
                sql =   @"DECLARE @PageID INT
                            SELECT @PageID = PageID FROM [Page] WHERE PageName = @PageName
                            SELECT * FROM dbo.DisplayItem " + joinClause +
                            @"
                            WHERE 1=1 
                               AND ISNULL(PageID, 0) = @PageID AND IsDisplay = 1
                               AND LanguageID = @LanguageID 
                            ORDER BY DisplayItem.DisplayOrder                            

                            SELECT * FROM dbo.CustomFields
                            WHERE 1=1 " +
                            @"  AND ISNULL(ApplicationID, -1) = -1
                                AND ISNULL(ContestID, -1) = -1 
                                AND ISNULL(ShowInSignUp,0) = 1
                                AND ISNULL(QNID,0) = 0
                            ORDER BY Priority,CustomFieldID;

                            SELECT cfv.* 
                            FROM CustomFieldValues cfv INNER JOIN CustomFields cf ON cfv.CustomFieldID = cf.CustomFieldID
                            WHERE ISNULL(cf.ApplicationID, -1) = -1 
                                AND ISNULL(cf.ContestID, -1) = -1                                       
                               AND ISNULL(cf.QNID,0) = 0                          
                            ORDER BY cfv.CustomFieldID, cfv.Priority";
            }
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                using (var grid = conn.QueryMultiple(sql, new { PageName = pageName, ApplicationID = applicationID, ContestID = contestID, LanguageID = languageID}))
                {
                    displayItems = grid.Read<DisplayItem>().ToList();
                    customFields = grid.Read<CustomFields>().ToList();
                    customFieldValues = grid.Read<CustomFieldValues>().ToList();
                    foreach (var customField in customFields)
                    {
                        customField.CustomFieldValues = customFieldValues.Where(c => c.CustomFieldID == customField.CustomFieldID).ToList();
                    }
                }
                conn.Close();
            }
            return Tuple.Create(displayItems, customFields, customFieldValues);
        }

        public void SendEmail(Template template, Dictionary<string, string> tokensDictionary, int? applicationID = null, string BouncedEmail = "", string attachment = "", MailArchive adhoc = null)
        {
            if (template == null || tokensDictionary == null)
                return;

            var dicParam = string.IsNullOrEmpty(template.Param) ? new Dictionary<string, string>() : JsonConvert.DeserializeObject<Dictionary<string, string>>(template.Param);
            if (adhoc != null && !dicParam.ContainsKey("adhocID"))
                dicParam.Add("adhocID", adhoc.ID.ToString());
            if (adhoc != null && !string.IsNullOrEmpty(adhoc.from_alias) && !dicParam.ContainsKey("from_alias"))
                dicParam.Add("from_alias", adhoc.from_alias.ToString());

            var param = JsonConvert.SerializeObject(dicParam);

            tblMailQueueRepository.Insert(new TblMailQueue()
            {
                Mail_From = StringToken.ReplaceToken(template.EmailFrom, tokensDictionary),
                Mail_To = StringToken.ReplaceToken(template.EmailTo, tokensDictionary),
                Mail_CC = template.CC,
                Mail_BCC = template.BCC,
                Mail_Subject = StringToken.ReplaceToken(template.Subject, tokensDictionary),
                Mail_Body = StringToken.ReplaceToken(template.Body, tokensDictionary),
                Mail_IsBodyHtml = true,
                Mail_Status = mailArchiveRepository.GetMailStatus(param),
                Mail_Retried = 0,
                BouncedEmail = BouncedEmail,
                Mail_Attachments = attachment,
                Param = param
            });
        }
    }
}
