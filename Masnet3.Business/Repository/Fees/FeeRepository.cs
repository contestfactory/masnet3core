﻿using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Dapper;
using System.Collections.Generic;
using MASNET.DataAccess;

namespace Masnet3.Business.Repository.Fees
{
    public class FeeRepository : Masnet3Connection, IFeeRepository
    {
        public FeeRepository(IConfiguration configuration) : base(configuration) {}

        public async Task<IEnumerable<Fee>> GetFeesByContestID(int contestID)
        {
            IEnumerable<Fee> fees;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                fees = await conn.QueryAsync<Fee>(@"SELECT f.* FROM Contestfee cf
                                                             INNER JOIN Fee f ON cf.FeeID = f.FeeID
                                                             WHERE cf.ContestID = @ContestID", new { ContestID = contestID });
                conn.Close();
            }
            return fees;
        }
    }
}
