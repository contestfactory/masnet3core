﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using MASNET.DataAccess;

namespace Masnet3.Business.Repository.Fees
{
    public interface IFeeRepository
    {
        Task<IEnumerable<Fee>> GetFeesByContestID(int contestID);
    }
}
