﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Dapper;
using Masnet3.Business.Utilities;
using MASNET.DataAccess.Constants.Enums;
using System.Linq;

namespace Masnet3.Business.Repository.Contests
{
    public class ContestRepository : Masnet3Connection, IContestRepository
    {
        public ContestRepository(IConfiguration configuration) : base(configuration) { }

        public Contest GetContestByContestId(int contestId)
        {
            Contest contest = new Contest();
            using (var con = this.GetDbConection())
            {
                con.Open();
                contest = con.QueryFirst<Contest>("SELECT * FROM Contest WHERE ContestID = @ContestID", new { ContestID = contestId });
                con.Close();
            }
            return contest;
        }

        public Contest GetGATrackingForContest(Enums.UseType useType, int applicationID, int getContestId, int contestId)
        {
            Contest contest = new Contest();
            using (var con = this.GetDbConection())
            {
                con.Open();
                contest = con.QueryFirstOrDefault<Contest>("SELECT GATracking FROM ExtensionField WHERE 1=1 " + SqlUilities.GetFilter(useType, applicationID, getContestId, ""), new { ContestID = contestId, ApplicationID = applicationID });
                con.Close();
            }
            return contest;
        }

        public IEnumerable<Contest> GetContestsByCreatedFrom(int createdFrom)
        {
            IEnumerable<Contest> contests;
            using (var con = this.GetDbConection())
            {
                con.Open();
                contests = con.Query<Contest>("SELECT * FROM Contest WHERE CreatedFrom = @CreatedFrom", new { CreatedFrom = createdFrom });
                con.Close();
            }
            return contests;
        }

        public IEnumerable<MinifiedContest> GetMinifiedContestsByCreatedFrom(int createdFrom)
        {
            IEnumerable<MinifiedContest> minifiedContests;
            using (var con = this.GetDbConection())
            {
                con.Open();
                minifiedContests = con.Query<MinifiedContest>(@"SELECT ContestName, IsDisplay, ThemeID, ContestDomain, TimeZoneID, ContestID, TemplateID,
                                                                CandiadateStartDate, EndDate, ExpireDate, LanguageID, Status, TemplateTypeID, Photo, RuleFileName, RuleAutoGen, AdminstratorName, RuleText FROM Contest WHERE CreatedFrom = @CreatedFrom", new { CreatedFrom = createdFrom });
                con.Close();
            }
            return minifiedContests;
        }

        public IEnumerable<ContestFee> GetListContestFeeByContestId(int contestID)
        {
            IEnumerable<ContestFee> contestFees;
            using (var con = this.GetDbConection())
            {
                con.Open();
                contestFees = con.Query<ContestFee>("SELECT * FROM ContestFee WHERE ContestID=@ContestID", new { ContestID = contestID });
                con.Close();
            }
            return contestFees;
        }
    }
}
