﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Contests
{
    public interface IContestRepository
    {
        Contest GetContestByContestId(int contestId);
        Contest GetGATrackingForContest(Enums.UseType useType, int ApplicationID, int getContestId, int contestId);
        IEnumerable<Contest> GetContestsByCreatedFrom(int createdFrom);
        IEnumerable<MinifiedContest> GetMinifiedContestsByCreatedFrom(int CreatedFrom);
        IEnumerable<ContestFee> GetListContestFeeByContestId(int contestID);
    }
}
