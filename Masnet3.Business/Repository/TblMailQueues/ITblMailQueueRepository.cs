﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.TblMailQueues
{
    public interface ITblMailQueueRepository
    {
        TblMailQueue Insert(TblMailQueue mailQueue, bool isSMTPMailgunDomain = false, string SMTP_MailgunDomain = "");
    }
}
