﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Dapper;

namespace Masnet3.Business.Repository.TblMailQueues
{
    public class TblMailQueueRepository : Masnet3Connection, ITblMailQueueRepository
    {
        public TblMailQueueRepository(IConfiguration configuration) : base(configuration) { }

        public TblMailQueue Insert(TblMailQueue mailQueue, bool isSMTPMailgunDomain = false, string SMTP_MailgunDomain = "")
        {
            try
            {
                var dicParam = new Dictionary<string, string>();
                if (!string.IsNullOrEmpty(mailQueue.Param))
                    dicParam = JsonConvert.DeserializeObject<Dictionary<string, string>>(mailQueue.Param + "");
                if (!isSMTPMailgunDomain)
                    dicParam.Add("SMTP_MailgunDomain", SMTP_MailgunDomain);
                mailQueue.Param = JsonConvert.SerializeObject(dicParam);
            }
            catch (Exception ex)
            { }

            if (mailQueue != null && mailQueue.ID == 0)
            {
                using (var conn = GetDbConection())
                {
                    conn.Open();

                    mailQueue.ID = conn.QueryFirstOrDefault<int>(@"
                        INSERT INTO TblMailQueue (Mail_From,Mail_To,Mail_CC,Mail_BCC,Mail_Subject,Mail_Body,Mail_IsBodyHtml,Mail_Status,Mail_Senddate,Mail_Priority,Mail_Description,Mail_Retried,Mail_Updated,UpdateBy,Mail_Attachments,BouncedEmail,Param)
					    VALUES (@Mail_From,@Mail_To,@Mail_CC,@Mail_BCC,@Mail_Subject,@Mail_Body,@Mail_IsBodyHtml,@Mail_Status,@Mail_Senddate,@Mail_Priority,@Mail_Description,@Mail_Retried,GETDATE(),@UpdateBy,@Mail_Attachments,@BouncedEmail,@Param);
					    SELECT CAST(SCOPE_IDENTITY() as int)",
                    mailQueue);
                    conn.Close();
                }

            }
            return mailQueue;

        }
    }
}
