﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using Dapper;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Utilities;

namespace Masnet3.Business.Repository.Configs
{
    public class ConfigRepository : Masnet3Connection, IConfigRepository
    {
        public ConfigRepository(IConfiguration configuration) : base(configuration) { }

        public Config GetConfigByConfigKeyID(Enums.UseType useType, int applicationID, int contestID, int configKeyID)
        {
            Config config = new Config();
            string filter = SqlUilities.GetFilter(useType, applicationID, contestID, 0, true);
            var sqlRun = $"SELECT * FROM Config WHERE ConfigKeyID = @ConfigKeyID {filter}";
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                config = conn.QueryFirstOrDefault<Config>(sqlRun, new { ConfigKeyID = configKeyID });
                conn.Close();
            }
            return config;
        }
    }
}
