﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Configs
{
    public interface IConfigRepository
    {
        Config GetConfigByConfigKeyID(Enums.UseType useType, int applicationID, int contestID, int configKeyID);
    }
}
