﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Masnet3.Business.Repository
{
    public interface IRepositoryBasic
    {
        void ExecuteSql(string sql, object parameter = null);
        IEnumerable<T> GetItemsDynamicSql<T>(string sql, object parameter = null);
        Task<IEnumerable<T>> GetItemsDynamicSql_Task<T>(string sql, object parameter = null);
        int CheckExistsItem(string table, string filter, object paramter);
    }
}
