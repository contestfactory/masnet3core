﻿using MASNET.DataAccess;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.ExtensionFields
{
    public interface IExtensionFieldRepository
    {
        ExtensionField GetExtensionFieldWithDisplayItem(string filter, int userType, int applicationID, int contestID, int templateID);
        ExtensionField GetExtensionField(int? applicationID = null, int? contestID = null, int? templateID = null, bool isReload = false);
    }
}
