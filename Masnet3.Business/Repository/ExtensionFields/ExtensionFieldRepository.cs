﻿using MASNET.DataAccess;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using Dapper;

namespace Masnet3.Business.Repository.ExtensionFields
{
    public class ExtensionFieldRepository : Masnet3Connection, IExtensionFieldRepository
    {
        public ExtensionFieldRepository(IConfiguration configuration) : base(configuration) { }

        public ExtensionField GetExtensionFieldWithDisplayItem(string filter, int userType, int applicationID , int contestID, int templateID)
        {
            ExtensionField extensionField;
            string sql = string.Format(@"SELECT * FROM ExtensionField WHERE (1 = 1) {0}
                                            SELECT * FROM ExtensionFieldDisplayItem WHERE (1 = 1) {0}", filter);
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                using (var grid = conn.QueryMultiple(sql))
                {
                    extensionField = grid.ReadFirstOrDefault<ExtensionField>() ?? InitExtensionField(userType, applicationID, contestID, templateID, false);
                    var displayItems = grid.ReadFirstOrDefault<ExtensionFieldDisplayItem>() ?? InitExtensionFieldDisplayItem(userType, applicationID, contestID, templateID);
                    extensionField.ExtensionFieldDisplayItem = displayItems;
                }
                conn.Close();
            }
            return extensionField;
        }

        public ExtensionField GetExtensionField(int? applicationID = null, int? contestID = null, int? templateID = null, bool isReload = false)
        {
            throw new NotImplementedException();
        }

        private ExtensionField InitExtensionField(int userType, int applicationID, int contestID, int templateID, bool initDisplay = true)
        {
            ExtensionField extensionField = new ExtensionField()
            {
                ApplicationID = applicationID,
                ContestID = contestID,
                TemplateID = templateID,
                UseType = userType
            };

            if (initDisplay)
            {
                extensionField.ExtensionFieldDisplayItem = InitExtensionFieldDisplayItem(userType, applicationID, contestID, templateID);
            }
            return extensionField;
        }
        private ExtensionFieldDisplayItem InitExtensionFieldDisplayItem(int userType, int applicationID, int contestID, int templateID)
        {
            ExtensionFieldDisplayItem extensionFieldDisplayItem = new ExtensionFieldDisplayItem()
            {
                ApplicationID = applicationID,
                ContestID = contestID,
                TemplateID = templateID,
                UseType = userType
            };
            foreach (var property in extensionFieldDisplayItem.GetType().GetProperties())
            {
                if (property.Name.StartsWith("SH_"))
                {
                    property.SetValue(extensionFieldDisplayItem, 1);
                }
            }
            return extensionFieldDisplayItem;
        }

    }
}
