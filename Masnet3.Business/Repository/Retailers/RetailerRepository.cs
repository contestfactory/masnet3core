﻿using Dapper;
using MASNET.DataAccess;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Retailers
{
    public class RetailerRepository : Masnet3Connection, IRetailerRepository
    {
        public RetailerRepository(IConfiguration configuration) : base(configuration) { }

        public Retailer GetRetailerById(int retailerID)
        {
            Retailer retailer;
            using (var conn = GetDbConection())
            {
                conn.Open();
                retailer = conn.QueryFirstOrDefault<Retailer>("SELECT * FROM Retailer WHERE RetailerID = @RetailerID ", new { RetailerID = retailerID });
                conn.Close();
            }
            return retailer;
        }

        public int GetRetailerId(string name, string city)
        {
            int retailerId = -1;
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                var sql = @"
                    SELECT RetailerID
                    FROM Retailer
                    WHERE RetailerName = @name
                        AND City = @city";
                retailerId = conn.QueryFirstOrDefault<int>(sql,
                    new
                    {
                        name,
                        city
                    });
                conn.Close();
            }
            return retailerId;
        }

        public IEnumerable<Retailer> GetRetailers(string name = null, bool? display = true)
        {
            IEnumerable<Retailer> retailers;
            DynamicParameters parameters = new DynamicParameters();
            string sql = @"SELECT * FROM Retailer WHERE (1 = 1) ";
            if (!string.IsNullOrWhiteSpace(name))
            {
                sql += "AND (RetailerName LIKE N'%' + @Name + '%') ";
                parameters.Add("Name", name.Trim());
            }
            if (display.HasValue)
            {
                sql += "AND (Display = @Display) ";
                parameters.Add("Display", display);
            }
            sql += " ORDER BY RetailerName";
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                retailers = conn.Query<Retailer>(sql, parameters);
                conn.Close();
            }
            return retailers;
        }

        public IEnumerable<string> GetRetailerNameAutocomplete(int contestId)
        {
            IEnumerable<string> retailerNames;
            var sql = @"
                    SELECT RetailerName
                    FROM Retailer
                    WHERE ContestId = @ContestId
                    GROUP BY RetailerName
                    ORDER BY RetailerName";
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                retailerNames = conn.Query<string>(sql, new { ContestId = contestId });
                conn.Close();
            }
            return retailerNames;
        }

        public IEnumerable<string> GetRetailerCityAutocomplete(int contestId, string retailerName)
        {
            IEnumerable<string> retailerCitys;
            var sql = @"
                    SELECT City
                    FROM Retailer
                    WHERE ContestId = @ContestId
                        AND (
                            @retailerName IS NULL 
                            OR RetailerName LIKE '%' + @RetailerName + '%'
                        )
                    GROUP BY City
                    ORDER BY City";
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                retailerCitys = conn.Query<string>(sql, new { ContestId = contestId, RetailerName = retailerName });
                conn.Close();
            }
            return retailerCitys;
        }

        public IEnumerable<RetailerLocation> GetRetailerLocations(int contestId)
        {
            IEnumerable<RetailerLocation> retailerLocations;
            var sql = @"
                    SELECT RetailerName AS Name,
                        City,
                        StreetAddress,
                        Province,
                        PostalCode,
                        Phone
                    FROM Retailer
                    WHERE ContestId = @ContestId
                    ORDER BY RetailerName, City";
            using (var conn = this.GetDbConection())
            {
                conn.Open();
                retailerLocations = conn.Query<RetailerLocation>(sql, new { ContestId = contestId });
                conn.Close();
            }
            return retailerLocations;
        }

    }
}
