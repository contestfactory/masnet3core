﻿using MASNET.DataAccess;
using MASNET.DataAccess.Model.Custom;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.Retailers
{
    public interface IRetailerRepository
    {
        Retailer GetRetailerById(int retailerID);
        int GetRetailerId(string name, string city);
        IEnumerable<Retailer> GetRetailers(string name = null, bool? display = true);
        IEnumerable<string> GetRetailerNameAutocomplete(int contestId);
        IEnumerable<string> GetRetailerCityAutocomplete(int contestId, string retailerName);

        IEnumerable<RetailerLocation> GetRetailerLocations(int contestId);
    }
}
