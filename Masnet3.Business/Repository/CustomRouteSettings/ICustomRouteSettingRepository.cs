﻿using MASNET.DataAccess.Model.Custom;
using System;
using System.Collections.Generic;
using System.Text;

namespace Masnet3.Business.Repository.CustomRouteSettings
{
    public interface ICustomRouteSettingRepository
    {
        IEnumerable<CustomRouteSetting> GetCustomRouteSettings(string searchKey = null, string searchValue = null, string sortBy = null, string sortDirection = null, int? page = 1, int pageSize = 20);
    }
}
