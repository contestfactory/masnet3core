﻿using MASNET.DataAccess.Model.Custom;
using Masnet3.Business.Infrastructure;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using Dapper;

namespace Masnet3.Business.Repository.CustomRouteSettings
{
    public class CustomRouteSettingRepository : Masnet3Connection, ICustomRouteSettingRepository
    {
        public CustomRouteSettingRepository(IConfiguration configuration) : base(configuration) { }

        public IEnumerable<CustomRouteSetting> GetCustomRouteSettings(string searchKey = null, string searchValue = null, string sortBy = null, string sortDirection = null, int? page = 1, int pageSize = 20)
        {
            IEnumerable<CustomRouteSetting> customRouteSettings;
            string where = "(1 = 1) ";
            if (!string.IsNullOrWhiteSpace(searchKey) && !string.IsNullOrWhiteSpace(searchValue))
            {
                where += string.Format(" AND ({0} LIKE N'%' + @SearchValue + '%') ", searchKey);
            }
            else
            {
                searchValue = null;
                where += string.Format(" AND (@SearchValue IS NULL) ");
            }
            if (string.IsNullOrWhiteSpace(sortBy))
            {
                sortBy = "ID";
            }
            if (string.IsNullOrWhiteSpace(sortDirection))
            {
                sortDirection = "asc";
            }
            var sqlRun = $"Select * From CustomRouteSetting Where {where} Order By {sortBy} {sortDirection} ";
            using (var con = this.GetDbConection())
            {
                con.Open();
                customRouteSettings = con.Query<CustomRouteSetting>(sqlRun, new { SearchValue = searchValue });
                con.Close();
            }
            return customRouteSettings;

        }
    }
}
