﻿using MASNET.DataAccess;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository;
using Masnet3.Common;
using Masnet3.Common.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Masnet3.ViewComponents
{
    public class ShareListViewComponent : ViewComponent
    {
        private readonly IRepositoryBasic repositoryBasic;
        public ShareListViewComponent(IRepositoryBasic repositoryBasic)
        {
            this.repositoryBasic = repositoryBasic;
        }
        public async Task<IViewComponentResult> InvokeAsync(Share model)
        {
            var site = GlobalVariables.SiteInformation;
            string customView = model.customView;
            string shareURL = model.shareURL;
            long contestantentryid = model.contestantentryid;
            long contestantID = model.contestantID;
            int returnFormat = model.returnFormat;
            bool bFlag = model.bFlag;
            bool isNotDeActivate = model.isNotDeActivate;
            int? contestID = model.ContestID;
            int? sourceID = model.sourceID;
            int mediaId = model.MediaId;
            string videoPath = model.VideoPath;
            string shareLinkType = model.ShareLinkType;
            string thumbnailUrl = model.ThumbnailUrl;
            string thumbnailDefaultUrl = model.ThumbnailDefaultUrl;
            List<int> actions = model.actions;

            if (sourceID == (int)Enums.PageID.HomePage && site.ApplicationID > 0)
                contestID = 0;

            var user = SessionUtilities.CurrentUser;
            var date = DateTime.Now.AddDays(1).ToString("yyyy-MM-dd");
            var lowerDate = DateTime.Now.ToString("yyyy-MM-dd");
            
            Period activeperiod = PeriodUilities.GetActivePeriod(site.ApplicationID, contestID);
            int ShareFrequency = Convert.ToInt32(Commons.Config["SharingFrequency"]);
            string sql = @"
                        SELECT ActionID FROM AccountDetail 
                        WHERE ContestID = @ContestID  AND [ShareStatus] = 1 ";
            sql += (SessionUtilities.IsLoggedIn() ? " AND UserID = @UserID " : " AND AnoUserID = @AnoUserID ");
            if (ShareFrequency == 1) // per Day
            {
                sql += " AND ContestantID = @ContestantID AND TransactionDate < @Date AND TransactionDate > @LowerDate ";
            }
            else if (ShareFrequency == 2) // per Promotion = from promotion start to end date
            {
                sql += "";
            }
            else if (ShareFrequency == 3) // per Hour
            {
                sql += " AND ContestantID = @ContestantID AND DATEDIFF (hour,TransactionDate,GETDATE()) = 0 ";
            }
            else if (ShareFrequency == 4)
            {   // Round
                sql += "";
            }
            else if (ShareFrequency == 5)
            { // Week
                sql += @" AND ContestantID = @ContestantID AND TransactionDate > DATEADD(DAY, 1 - DATEPART(WEEKDAY, GETDATE()), CAST(GETDATE() AS DATE))
                                      AND TransactionDate < DATEADD(DAY, 7 - DATEPART(WEEKDAY, GETDATE()) + 1, CAST(GETDATE() AS DATE))
                                ";
            }
            else if (ShareFrequency == 6)
            { // 5 mins
                sql += @" AND ContestantID = @ContestantID AND ABS(DATEDIFF (minute,TransactionDate,GETDATE())) < 5
                                ";
            }
            if (sourceID.HasValue && sourceID.Value > 0)
            {
                sql += " AND SourceID = @SourceID";
            }

            sql += " GROUP BY ActionID";
            var lsactions = await repositoryBasic.GetItemsDynamicSql_Task<int>(sql, new
            {
                PeriodID = activeperiod.PeriodID,
                Date = date,
                LowerDate = lowerDate,
                UserId = user.UserID,
                AnoUserID = CookieUtilities.GetCookie("id"),
                contestantentryid = contestantentryid,
                ContestantID = contestantID,
                ContestID = contestID.GetValueOrDefault(),
                SourceID = sourceID
            });
            actions = lsactions.ToList();

            var lstShare = ActionUitilites.GetActiveActions(Enums.SweepType.Period, activeperiod.PeriodID, site.ApplicationID, contestID);
            if (!string.IsNullOrEmpty(model.Exclude))
                lstShare = lstShare.Where(s => !model.Exclude.Split(',').Contains(s.ActionName));

            ViewBag.ActionID = actions;
            ViewBag.UseValidate = "1";
            ViewBag.ContestantentryID = contestantentryid;
            ViewBag.ContestantID = contestantID;
            ViewBag.shareURL = shareURL;
            ViewBag.isNotDeActivate = isNotDeActivate;
            ViewBag.sourceID = sourceID;
            ViewBag.contestID = contestID.GetValueOrDefault();
            ViewBag.MediaID = mediaId;
            ViewBag.VideoPath = videoPath;
            ViewBag.ShareLinkType = shareLinkType;
            ViewBag.ThumbnailUrl = thumbnailUrl;
            ViewBag.ThumbnailDefaultUrl = thumbnailDefaultUrl;

            model.actions = actions;
            ViewBag.Share = model;

            return View(customView, lstShare.ToList());
        }
    }
}
