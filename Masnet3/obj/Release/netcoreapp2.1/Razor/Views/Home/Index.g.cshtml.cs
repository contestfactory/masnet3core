#pragma checksum "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c24cca44a7796e144e6bce2ff6a225609eaaca3d"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Home/Index.cshtml", typeof(AspNetCore.Views_Home_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\NDex\Working\masnet3core\Masnet3\Views\_ViewImports.cshtml"
using Masnet3;

#line default
#line hidden
#line 2 "D:\NDex\Working\masnet3core\Masnet3\Views\_ViewImports.cshtml"
using Masnet3.Models;

#line default
#line hidden
#line 1 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
using Masnet3.Common.Helper;

#line default
#line hidden
#line 2 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
using Masnet3.Common.Utilities;

#line default
#line hidden
#line 3 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
using Masnet3.Common;

#line default
#line hidden
#line 4 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
using MASNET.DataAccess.Constants.Enums;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c24cca44a7796e144e6bce2ff6a225609eaaca3d", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62b5bd459d08c49c1754bc7ee28d415db29a5b57", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Masnet3.Model.ViewModel.LayoutViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 6 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
  
    var currentUser = SessionUtilities.CurrentUser;
    if (Model.SiteInformation.Application.TemplateTypeID.GetValueOrDefault() == (int)Enums.ApplicationTemplateType.DRTV)
    {
        ViewBag.Title = "";
    }

#line default
#line hidden
            BeginContext(406, 76, true);
            WriteLiteral("\r\n<style>\r\n    .content-inner {\r\n        display: none;\r\n    }\r\n</style>\r\n\r\n");
            EndContext();
#line 20 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
 if (Model.SiteInformation.Application.TemplateTypeID.GetValueOrDefault() == (int)Enums.ApplicationTemplateType.DRTV)
{

#line default
#line hidden
            BeginContext(604, 86, true);
            WriteLiteral("    <div id=\"dvHome\"></div>\r\n    <style>\r\n        #home_caption {\r\n            margin-");
            EndContext();
            BeginContext(691, 18, false);
#line 25 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
              Write(DbLanguage.CssLeft);

#line default
#line hidden
            EndContext();
            BeginContext(709, 23, true);
            WriteLiteral(": -493px;\r\n            ");
            EndContext();
            BeginContext(733, 18, false);
#line 26 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
       Write(DbLanguage.CssLeft);

#line default
#line hidden
            EndContext();
            BeginContext(751, 98, true);
            WriteLiteral(": 50%;\r\n        }\r\n        .content-inner {\r\n            display: none;\r\n        }\r\n    </style>\r\n");
            EndContext();
#line 32 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
}
else
{

}

#line default
#line hidden
            BeginContext(866, 2, true);
            WriteLiteral("\r\n");
            EndContext();
#line 38 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
 if (Model.SiteInformation.SiteType == Enums.SiteType.Main && (!SessionUtilities.IsLoggedIn(currentUser) || (SessionUtilities.IsLoggedIn(currentUser) && (SessionUtilities.IsSuperAdmin() || SessionUtilities.CurrentUser.UserType == (int)Enums.RegistrationUserType.Customer))))
{
    var createCampaignUrl = Url.Action("create", "customercontest");
    if (!SessionUtilities.IsLoggedIn(currentUser))
    {
        createCampaignUrl = Url.Action("register", "customer", new { userType = 0, returnUrl = createCampaignUrl });
    }

#line default
#line hidden
            BeginContext(1401, 848, true);
            WriteLiteral(@"    <style>
        #btnHomeCreateUGC {
            background: -webkit-linear-gradient(#FF770C, #BE2504); /* For Safari 5.1 to 6.0 */
            background: -o-linear-gradient(#FF770C, #BE2504); /* For Opera 11.1 to 12.0 */
            background: -moz-linear-gradient(#FF770C, #BE2504); /* For Firefox 3.6 to 15 */
            background: linear-gradient(#FF770C, #BE2504); /* Standard syntax (must be last) */
            border: medium none white;
            border-radius: 5px;
            /*box-shadow: 5px 5px 5px #e66500;*/
            font-size: 2em;
            font-weight: bold;
            padding: 10px 30px;
            font: 22px/28px ""OpenSansCondensedLight"",Arial,sans-serif;
            margin: 25px 0 10px;
        }
    </style>
    <div style=""text-align: center; width: 100%;"">
        <input type=""button""");
            EndContext();
            BeginWriteAttribute("onclick", " onclick=\"", 2249, "\"", 2303, 3);
            WriteAttributeValue("", 2259, "window.location.href=\'", 2259, 22, true);
#line 62 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
WriteAttributeValue("", 2281, createCampaignUrl, 2281, 20, false);

#line default
#line hidden
            WriteAttributeValue("", 2301, "\';", 2301, 2, true);
            EndWriteAttribute();
            BeginWriteAttribute("value", " value=\"", 2304, "\"", 2364, 1);
#line 62 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
WriteAttributeValue("", 2312, DbLanguage.GetText("Home_ButtonCreateUGCContest"), 2312, 52, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2365, 37, true);
            WriteLiteral(" id=\"btnHomeCreateUGC\">\r\n    </div>\r\n");
            EndContext();
#line 64 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
}

#line default
#line hidden
            BeginContext(2405, 29, true);
            WriteLiteral("<div class=\"clearer\"></div>\r\n");
            EndContext();
            DefineSection("Scripts", async() => {
                BeginContext(2451, 204, true);
                WriteLiteral("\r\n    <script>\r\n            $(document).ready(function () {\r\n                $(\"div[id^=\'counter\']\").each(function (key, value) {\r\n                    $(this).countdown({\r\n                        image: \'");
                EndContext();
                BeginContext(2656, 92, false);
#line 71 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                           Write(Url.Content(string.Format("~/Content/{0}/images/digits39.png", SessionUtilities.AreaName())));

#line default
#line hidden
                EndContext();
                BeginContext(2748, 499, true);
                WriteLiteral(@"',
                        format: ""dd:hh:mm:ss"",
                        startTime: $(""#timer_"" + this.getAttribute(""id"")).val(),
                        stepTime: 60,
                        digitImages: 6,
                        digitWidth: 27,
                        digitHeight: 39,
                        timerEnd: function () {
                            //alert('Candiate time ended.');
                        },
                    });
                });
               
");
                EndContext();
#line 84 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
             if (Model.SiteInformation.Application.TemplateTypeID.GetValueOrDefault() == (int)Enums.ApplicationTemplateType.DRTV)
            {
               

#line default
#line hidden
                BeginContext(3414, 89, true);
                WriteLiteral("\r\n                $.ajax({\r\n                    type: \"POST\",\r\n                    url: \"");
                EndContext();
                BeginContext(3504, 42, false);
#line 89 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                     Write(Url.Action("_getStaticPage", "staticpage"));

#line default
#line hidden
                EndContext();
                BeginContext(3546, 143, true);
                WriteLiteral("\",\r\n                    data: { ID: 16 },\r\n                    success: function (data) {\r\n                        $(\"#dvHome\").html(data);\r\n\r\n");
                EndContext();
#line 94 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                         if (ViewBag.CntContestFee > 0){

#line default
#line hidden
                BeginContext(3747, 28, true);
                WriteLiteral("                            ");
                EndContext();
                BeginContext(3777, 31, true);
                WriteLiteral("$(\"#dvHomePromoCode\").show();\r\n");
                EndContext();
#line 96 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                                                 }

#line default
#line hidden
                BeginContext(3860, 59, true);
                WriteLiteral("                    }\r\n                })\r\n                ");
                EndContext();
#line 99 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                       
            }

#line default
#line hidden
                BeginContext(3943, 197, true);
                WriteLiteral("            });\r\n\r\n            function SubmitPromoCode(txtPromoCode) {\r\n                var txt = $(\"#\" + txtPromoCode);\r\n                if (txt.val().trim() == \"\") {\r\n                    alert(\"");
                EndContext();
                BeginContext(4141, 43, false);
#line 106 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                      Write(DbLanguage.GetText("Text_RequirePromoCode"));

#line default
#line hidden
                EndContext();
                BeginContext(4184, 180, true);
                WriteLiteral("\");\r\n                    txt.focus();\r\n                    return false;\r\n                }\r\n                $.ajax({\r\n                    type: \"POST\",\r\n                    url: \"");
                EndContext();
                BeginContext(4365, 36, false);
#line 112 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                     Write(Url.Action("SubmitPromoCode","Home"));

#line default
#line hidden
                EndContext();
                BeginContext(4401, 204, true);
                WriteLiteral("\",\r\n                    data: {\r\n                        promocode: txt.val().trim()\r\n                    },\r\n                    success: function (obj) {\r\n                        if (obj.ReturnCode == \"");
                EndContext();
                BeginContext(4607, 40, false);
#line 117 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                                           Write((int)Enums.SubmissionPayFeeState.Success);

#line default
#line hidden
                EndContext();
                BeginContext(4648, 154, true);
                WriteLiteral("\") {\r\n                            window.location.href = obj.RedirectURL;\r\n                        }\r\n                        else if (obj.ReturnCode == \"");
                EndContext();
                BeginContext(4804, 49, false);
#line 120 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                                                Write((int)Enums.SubmissionPayFeeState.InvalidPromoCode);

#line default
#line hidden
                EndContext();
                BeginContext(4854, 41, true);
                WriteLiteral("\") {\r\n                            alert(\"");
                EndContext();
                BeginContext(4896, 43, false);
#line 121 "D:\NDex\Working\masnet3core\Masnet3\Views\Home\Index.cshtml"
                              Write(DbLanguage.GetText("Text_InvalidPromoCode"));

#line default
#line hidden
                EndContext();
                BeginContext(4939, 105, true);
                WriteLiteral("\");\r\n                        }\r\n                    }\r\n                })\r\n            }\r\n    </script>\r\n");
                EndContext();
            }
            );
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Masnet3.Model.ViewModel.LayoutViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
