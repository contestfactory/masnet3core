#pragma checksum "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "6dd892016dcc9a7d51c01840151576b91535fdea"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Navigation_Footer__Syngenta_Footer), @"mvc.1.0.view", @"/Views/Navigation/Footer/_Syngenta_Footer.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/Navigation/Footer/_Syngenta_Footer.cshtml", typeof(AspNetCore.Views_Navigation_Footer__Syngenta_Footer))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\NDex\Working\masnet3core\Masnet3\Views\_ViewImports.cshtml"
using Masnet3;

#line default
#line hidden
#line 2 "D:\NDex\Working\masnet3core\Masnet3\Views\_ViewImports.cshtml"
using Masnet3.Models;

#line default
#line hidden
#line 1 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
using Masnet3.Common.Helper;

#line default
#line hidden
#line 2 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
using Masnet3.Common;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6dd892016dcc9a7d51c01840151576b91535fdea", @"/Views/Navigation/Footer/_Syngenta_Footer.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"62b5bd459d08c49c1754bc7ee28d415db29a5b57", @"/Views/_ViewImports.cshtml")]
    public class Views_Navigation_Footer__Syngenta_Footer : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<Masnet3.Model.ViewModel.LayoutViewModel>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/Content/themes/syngenta/images/powered-by.png"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("class", new global::Microsoft.AspNetCore.Html.HtmlString("img-responsive"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("alt", new global::Microsoft.AspNetCore.Html.HtmlString("Powered by ContestFactory.com"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 4 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
   
    string FooterPrivacyLink = "";

#line default
#line hidden
            BeginContext(147, 1057, true);
            WriteLiteral(@"<div class=""main-footer"">
    <div class=""footer-container"">
        <div class=""container-fluid"">
            <div class=""row"">
                <div class=""col-sm-3 footer-copyright"">
                    &copy;2018 Syngenta.
                </div>
                <div class=""col-sm-6 social-links"">
                    <ul id=""footer-links"" class=""list-inline link-font"">
                        <li>
                            <a href=""http://response.syngenta.ca/cp-soymaster-pyb"" target=""_blank"" id=""footer-rules-btn"" class=""footer-menu"">Soy Masters</a>
                        </li>
                        <li class=""separator"">|</li>
                        <li>
                            <a href=""http://response.syngenta.ca/stewardship-agreement"" target=""_blank"" id=""footer-rules-btn"" class=""footer-menu"">Stewardship Agreement</a>
                        </li>
                        <li class=""separator"">|</li>

                        <li>
                            <a href=""javascript:");
            WriteLiteral(" void(0);\" id=\"footer-rules-btn\" ");
            EndContext();
            BeginContext(1205, 63, false);
#line 26 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                                                                            Write(Commons.ContentEdittable("gFooter_ContestRules", "footer-menu"));

#line default
#line hidden
            EndContext();
            BeginContext(1268, 54, true);
            WriteLiteral(" class=\"footer-menu\" onclick=\" contestRulesClick(); \">");
            EndContext();
            BeginContext(1323, 42, false);
#line 26 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                                                                                                                                                                                                  Write(DbLanguage.GetText("gFooter_ContestRules"));

#line default
#line hidden
            EndContext();
            BeginContext(1365, 121, true);
            WriteLiteral("</a>\r\n                        </li>\r\n                        <li class=\"separator\">|</li>\r\n                        <li>\r\n");
            EndContext();
#line 30 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                             if (!string.IsNullOrWhiteSpace(DbLanguage.GetText("gFooter_Privacy_Link")))
                            {
                                if (DbLanguage.GetText("gFooter_Privacy_Link").ToLower().IndexOf("https") != -1)
                                {
                                    FooterPrivacyLink = "https://" + DbLanguage.GetText("gFooter_Privacy_Link").ToLower().Replace("https://", "");
                                }
                                else if (DbLanguage.GetText("gFooter_Privacy_Link").ToLower().IndexOf("http") != -1)
                                {
                                    FooterPrivacyLink = "http://" + DbLanguage.GetText("gFooter_Privacy_Link").ToLower().Replace("http://", "");
                                }
                                else
                                {
                                    FooterPrivacyLink = "http://" + DbLanguage.GetText("gFooter_Privacy_Link").ToLower();
                                }

#line default
#line hidden
            BeginContext(2520, 74, true);
            WriteLiteral("                                <a id=\"footer-privacy-btn\" target=\"_blank\"");
            EndContext();
            BeginWriteAttribute("href", " href=\"", 2594, "\"", 2619, 1);
#line 44 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
WriteAttributeValue("", 2601, FooterPrivacyLink, 2601, 18, false);

#line default
#line hidden
            EndWriteAttribute();
            BeginContext(2620, 1, true);
            WriteLiteral(" ");
            EndContext();
            BeginContext(2622, 58, false);
#line 44 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                                                                                                Write(Commons.ContentEdittable("gFooter_Privacy", "footer-menu"));

#line default
#line hidden
            EndContext();
            BeginContext(2680, 59, true);
            WriteLiteral(" class=\"footer-menu\">\r\n                                    ");
            EndContext();
            BeginContext(2740, 37, false);
#line 45 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                               Write(DbLanguage.GetText("gFooter_Privacy"));

#line default
#line hidden
            EndContext();
            BeginContext(2777, 40, true);
            WriteLiteral("\r\n                                </a>\r\n");
            EndContext();
#line 47 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                            }
                            else
                            {

#line default
#line hidden
            BeginContext(2913, 93, true);
            WriteLiteral("                                <a id=\"footer-privacy-btn\" href=\"#\" onclick=\" return false \" ");
            EndContext();
            BeginContext(3007, 58, false);
#line 50 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                                                                                        Write(Commons.ContentEdittable("gFooter_Privacy", "footer-menu"));

#line default
#line hidden
            EndContext();
            BeginContext(3065, 59, true);
            WriteLiteral(" class=\"footer-menu\">\r\n                                    ");
            EndContext();
            BeginContext(3125, 37, false);
#line 51 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                               Write(DbLanguage.GetText("gFooter_Privacy"));

#line default
#line hidden
            EndContext();
            BeginContext(3162, 40, true);
            WriteLiteral("\r\n                                </a>\r\n");
            EndContext();
#line 53 "D:\NDex\Working\masnet3core\Masnet3\Views\Navigation\Footer\_Syngenta_Footer.cshtml"
                            }

#line default
#line hidden
            BeginContext(3233, 231, true);
            WriteLiteral("                        </li>\r\n                    </ul>\r\n                </div>\r\n                <div class=\"col-sm-3 poweredby\">\r\n                    <a href=\"http://contestfactory.com/\" target=\"_blank\">\r\n                        ");
            EndContext();
            BeginContext(3464, 120, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("img", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "e3dea1c16e3f4a7d94aac7fa1e004afb", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(3584, 108, true);
            WriteLiteral("\r\n                    </a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<Masnet3.Model.ViewModel.LayoutViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
