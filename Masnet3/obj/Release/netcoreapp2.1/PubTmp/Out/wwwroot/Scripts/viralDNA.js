﻿// class ViralDNA
function ViralDNA (params) {
    var _instance = this;
    // public readonly members
    Object.defineProperty(this,"instance", {
        get: function() { return _instance; }
    });
    Object.defineProperty(this,"applicationID", { value: params.applicationID });
    Object.defineProperty(this,"contestID", { value: params.contestID });
    Object.defineProperty(this, "roundId", { value: params.roundId ? params.roundId : 0 });
    Object.defineProperty(this, "reportUrl", { value: params.reportUrl });
    Object.defineProperty(this,"chartUrl", { value: params.chartUrl });
    // public writable members
    Object.defineProperty(this, "containerID", { value: params.containerID });
    Object.defineProperty(this,"filter", {
        value: {
            containerID: this.containerID,
            applicationID: this.applicationID,
            editSiteId: this.applicationID,
            contestId: this.contestID,
            editCampaignId: this.contestID,
            SortColumn: params.sortColumn,
            SortBy: params.sortBy,
            SortDirection: params.sortDirection,
            page: 1
        }, writable : true
    });
}

ViralDNA.prototype.init = function () {
    var _instance = this;
    // bind events
    if ($("[name=btn-filter-report]").size() > 0) {
        $("[name=btn-filter-report]").off("click").on("click", function () { _instance.openDownloadFiles(_instance.applicationID, _instance.contestID); });
    }
}

ViralDNA.prototype.getFilter = function(isReload) {
    var _instance = this.instance;
    _instance.filter.searchkey = $('#ddlSearchKey').length === 1 ? $('#ddlSearchKey option:selected').val() : "";
    _instance.filter.keyword = $('#txtKeyword').length === 1 ? $('#txtKeyword').val() : "";
    _instance.filter.FromDate = $("#txtFrom").val();
    _instance.filter.EndDate = $("#txtTo").val() + " 23:59:59";
    _instance.filter.startDate = $("#txtFrom").val();
    _instance.filter.endDate = $("#txtTo").val();

    _instance.filter.Status = $('#ddlStatus').length === 1 ? $('#ddlStatus option:selected').val() : ""
    _instance.filter.CategoryID = $("input[name=CategoryID]:checked", "#optCategoryID").val();
    _instance.filter.countryID = $('#ddlCountry').length === 1 ? $('#ddlCountry option:selected').val() : "";
    _instance.filter.stateID = $('#ddlState').length === 1 ? $('#ddlState option:selected').val() : "";
    _instance.filter.IsDisplay = $('#ddlDisplayState').length === 1 ? $('#ddlDisplayState option:selected').val() : "";
    _instance.filter.resetPaging = isReload;
    _instance.filter.roundId = $('#sub-tab').html() != undefined ? $('#sub-tab li.active').attr('data-value') : _instance.roundId;
    return _instance.filter;
}

ViralDNA.prototype.DrawLineChart = function (container, reportdata, sharingUsersData) {
    if(reportdata.ReportData.length || reportdata.ReportDataDynamic.length) {
        var arr = [];
        if (reportdata.ReportDataDynamic == null) {
            if (sharingUsersData != null) {
                arr.push(['Date', $("#hplPieCharts").text().trim(), $("#hplViralDnaTab1").text().trim()]);
            } else {
                //arr.push(['Date', getSelectedReport()]);
                arr.push(['Date', reportdata.ReportTitle]);
            }
        }

        for (var i = 0; i < reportdata.ReportData.length; i++) {
            if (sharingUsersData != null) {
                arr.push([reportdata.ReportData[i].Title, reportdata.ReportData[i].Value, sharingUsersData.ReportData[i].Value]);
            } else {
                arr.push([reportdata.ReportData[i].Title, reportdata.ReportData[i].Value]);
            }
        }

        if (reportdata.ReportDataDynamic) {
            arr.push(reportdata.ReportDataHeader);
            for (var i = 0; i < reportdata.ReportDataDynamic.length; i++) {
                var arrData = reportdata.ReportDataDynamic[i].Value.split(',').map(Number)
                arrData.unshift(reportdata.ReportDataDynamic[i].Title)
                arr.push(arrData);
            }
        }
        var data = google.visualization.arrayToDataTable(arr);

        var options = {
            title: "",
            vAxis: {
                minValue: 0,
                viewWindow: {
                    min: 0
                }
            },
            fontSize: 10,
            series: {
                0: { pointSize: 13 },
                1: { pointSize: 9 },
                2: { pointSize: 5 },
                3: { pointSize: 3 }
            },
            legend: {}
        };
        if (reportdata.ReportData && reportdata.ReportData.length > 0) {
            options.series[0].pointSize = 5;
        }
        var chart = new google.visualization.LineChart(document.getElementById(container));
        chart.draw(data, options);
    }
}

ViralDNA.prototype.openDownloadFiles = function (applicationID, contestID) {

    $.ajax({
        type: "post",
        url: SiteConfig.gSiteAdrs + "/" + SiteConfig.SiteName + "/Base/FileManagement",
        cache: false,
        data: {
            fileExtensions: "*.csv",
            folderName: "ViralDNA_Reports",
            allowUpload: true,
            allowDelete: true,
            editSiteId: applicationID,
            editCampaignId: contestID
        },
        success: function (html) {
            if ($("#dvFileManagement").length == 0) {
                $("#dgdialogUploadMedia").remove();
                $("body").append("<div style='display:none;' id='dvFileManagement'></div>");
            }

            $("#dvFileManagement").html(html);
            $("#dvFileManagement").dialog({
                modal: true,
                lgClass: true,
                autoOpen: false,
                height: 600,
                width: 800,
                title: "Viral DNA",
                close: function () {
                    delete sortTableColumnIndex;
                    delete sortTableColumnDirection;
                }
            }).dialog("open");

            initFileManagement();

            $("#LabeldgdvFileManagement").html("Viral DNA");
        }
    });
}
