
/*
jQuery fullsizable plugin v1.6
  - take full available browser space to show images

(c) 2011-2013 Matthias Schmidt <http://m-schmidt.eu/>

Example Usage:
  $('a.fullsizable').fullsizable();

Options:
  **detach_id** (optional, defaults to null) - id of an element that will be set to display: none after the curtain loaded.
  **navigation** (optional, defaults to false) - set to true to show next and previous links.
  **openOnClick** (optional, defaults to true) - set to false to disable default behavior which fullsizes an image when clicking on a thumb.
  **closeButton** (optional, defaults to false) - set to true to show a close link.
  **clickBehaviour** (optional, 'next' or 'close', defaults to 'close') - whether a click on an opened image should close the viewer or open the next image.
  **allowFullscreen** (optional, defaults to true) - enable native HTML5 fullscreen support in supported browsers.
*/

(function() {
  var $, $image_holder, closeFullscreen, closeViewer, container_id, current_image, hasFullscreenSupport, image_holder_id, images, keyPressed, makeFullsizable, nextImage, openViewer, options, preloadImage, prevImage, resizeImage, selector, showImage, spinner_class, stored_scroll_position, toggleFullscreen;
  var customNextImage,customPrevImage,alwayShowNavigation,slideShowInterval;
  
  $ = jQuery;

  container_id = '#jquery-fullsizable';

  image_holder_id = '#fullsized_image_holder';

  spinner_class = 'fullsized_spinner';

  $image_holder = $('<div id="jquery-fullsizable"><div id="fullsized_header"><span id="fullsized_name"></span></div><div id="fullsized_image_holder"></div></div>');

  selector = null;

  images = [];

  current_image = 0;

  options = null;

  stored_scroll_position = null;
  slideShowInterval = null;

  resizeImage = function() {
    var image;
    image = images[current_image];
    $(image_holder_id).css("visibility","hidden");

    
    image.ratio = (image.height / image.width).toFixed(2);

    if (image.height >= $(window).height() || image.width >= $(window).width()){
        if ($(window).height() / image.ratio > $(window).width()) {
          $(image).width($(window).width());
          $(image).height($(window).width() * image.ratio);
          $(image).css('margin-top', ($(window).height() - $(image).height()) / 2);
        } else {
          $(image).height($(window).height());
          $(image).width($(window).height() / image.ratio);
          $(image).css('margin-top', 0);
        }
    }
   
   $(image_holder_id)			
				.css({				
					"width" : $(window).width() + "px",
					"height": $(window).height() + "px",
                    "visibility" : "visible"
				})

   return $(image);
  };

  keyPressed = function(e) {
    if (e.keyCode === 27) closeViewer();
    if (e.keyCode === 37) prevImage();
    if (e.keyCode === 39) return nextImage();
  };

  prevImage = function() {
    if (current_image > 0) return showImage(images[current_image - 1], -1);
  };

  nextImage = function() {
    if (current_image < images.length - 1) {
      return showImage(images[current_image + 1], 1);
    }
  };

  showImage = function(image, direction) {
    if (direction == null) direction = 1;
    current_image = image.index;
    $(image_holder_id).hide();

    
	
	if (options.navigation) {		  
		  $('#fullsized_go_prev').toggle(current_image !== 0);
		  $('#fullsized_go_next').toggle(current_image !== images.length - 1);
		  $('#fullsized_slideshow').show();
	}
	else{
		$('#fullsized_go_prev').hide();
		$('#fullsized_go_next').hide();
		$('#fullsized_slideshow').hide();
	}
	
	$(image_holder_id)
          .removeAttr('style')
          .css({
              "display": "table-cell",
              "width": $(window).width() + "px",
              "height": $(window).height() + "px"
          })

	$("#fullsized_name")
        .attr("title", image.fulltitle)
        .html(image.shorttitle);

	$("#fullsized_shared")
        .attr({
            "shared-url": SiteConfig.gSiteAdrs + "/home/media?MediaId=" + image.mediaid,
            "shared-outcontentwrapper" : "true"
        })
        .unbind("hover")
        .hover(function () {
            BindShareable(this);
        }, function () {
            //Do nothing
        });

	if (image.filetype == null || image.filetype == "1") {
		$(image_holder_id).html(image);	
		
		if (image.loaded != null) {
		  $(container_id).removeClass(spinner_class);
		  resizeImage();
		  $(image_holder_id).show("200");
		  return preloadImage(direction);
		} else {
		  $(container_id).addClass(spinner_class);
          
		  $(image).load(function () {
		      resizeImage();
		      $(image_holder_id).show("200", function () {
		          return $(container_id).removeClass(spinner_class);
		      });
		      this.loaded = true;
		      return preloadImage(direction);
		  }).attr("src", image.buffer_src);
		  return image;
          
		}
	}
	else if (image.filetype == "2" || image.filetype == "3") {		
	    $(image_holder_id).html("<div id='fullsizedPlayer' style='width:" + $(window).width() * 0.7 + "px;height:" + $(window).height() * 0.7 + ";margin:0 auto;text-align:center;max-width:100%'></div>");
		
	    PlayMedia(image.filetype, image.filepath, image.thumbnail, "fullsizedPlayer");

	    $('#fullsizedPlayer').find('video').removeAttr('width');

		$(image_holder_id).show("200");
	}
	else if (image.filetype == '4') {

	    if (image.filepath == null || image.filepath == "") {

	        $(image_holder_id).html("<div id='fullsizedPlayer' style='width:70%;height:70%;margin:0 auto'></div>");

	        $("#fullsizedPlayer")
                .css({
                    "word-wrap": "break-word",
                    "background-color": "#fff",
                    "overflow": "auto",
                    "font-size" : "1.2em"
                })
                .html($("#dvDesc_" + image.mediaid).html());

	        $(image_holder_id).show("200");
        }
        else {
	        var downloadpath = image.filepath;
            var html = "<div style='text-align:center;padding:15px 5px;width:40%;margin:0 auto' class='shadowButton'  onclick=\"DownloadFile($(this).attr('downloadpath'));\" downloadpath='" + downloadpath + "'><a href='#' style='text-decoration:none' onclick=\"return false;\" ><i class='icon icon-download'></i> Download</a></div>";

            $(image_holder_id)
                .html(html)
                .show();
        }
	}
  };

  preloadImage = function(direction) {
    var preload_image;
    if (direction === 1 && current_image < images.length - 1) {
      preload_image = images[current_image + 1];
    } else if ((direction === -1 || current_image === (images.length - 1)) && current_image > 0) {
      preload_image = images[current_image - 1];
    } else {
      return;
    }
    preload_image.onload = function() {
      return this.loaded = true;
    };
    if (preload_image.src === '') {
      return preload_image.src = preload_image.buffer_src;
    }
  };

  openViewer = function(image) {
    $(window).bind('resize', resizeImage);
    showImage(image);
    return $(container_id).hide().show("200",function() {
      if (options.detach_id != null) {
        stored_scroll_position = $(window).scrollTop();
        $(options.detach_id).css('display', 'none');
        resizeImage();
        $(image_holder_id).show();
      }
      if (options.clickBehaviour === 'close') {
        $(container_id).bind('click', closeViewer);
      }
      else if (options.clickBehaviour === 'next') {
        $(container_id).bind('click', nextImage);
      }
      else {
          $(container_id).bind('click', function(){});
      }
      return $(document).bind('keydown', keyPressed);
    });
  };

  closeViewer = function() {
    if (options.detach_id != null) {
      $(options.detach_id).css('display', 'block');
      $(window).scrollTop(stored_scroll_position);
    }
    if (options.clickBehaviour === 'close') {
      $(container_id).unbind('click', closeViewer);
    }
    if (options.clickBehaviour === 'next') {
      $(container_id).unbind('click', nextImage);
    }
    
    if (slideShowInterval != null) {
        $("#fullsized_slideshow i")
            .removeClass("icon-pause")
            .addClass("icon-play");

        window.clearInterval(slideShowInterval);
        slideShowInterval = null;
    }

    options.navigation = true;

    $(container_id).fadeOut();
    closeFullscreen();
    $(container_id).removeClass(spinner_class);
    $(document).unbind('keydown', keyPressed);
    $("#dvSharing").hide();
    $('#fullsizedPlayer').html('');

    return $(window).unbind('resize', resizeImage);

  };

  makeFullsizable = function() {
    var sel;
    images.length = 0;
    if (options.dynamic) {
      sel = $(options.dynamic);
    } else {
      sel = selector;
    }
    return sel.each(function() {
      var image;
      image = new Image;
      image.buffer_src = $(this).attr('media-thumbnail');
      image.index = images.length;
      image.mediaid = $(this).attr('media-mediaid');
      image.thumbnail = $(this).attr('media-thumbnail');
      image.filetype = $(this).attr('media-filetype');
      image.filepath = $(this).attr('media-filepath');
      image.shorttitle = ($(this).attr('media-shorttitle') || "");
      image.fulltitle = ($(this).attr('title') || "");

	  image.object = $(this);
      images.push(image);
      if (options.openOnClick) {
          return $(this).unbind('click').click(function (e) {
          e.preventDefault();
          return openViewer(image);
        });
      }
    });
  };

  $.fn.fullsizable = function(opts) {
    options = $.extend({
      detach_id: null,
      navigation: false,
      openOnClick: true,
      closeButton: false,
      clickBehaviour: 'close',
      allowFullscreen: true,
      dynamic: null,
      slideshow : true
    }, opts || {});
    $('body').append($image_holder);
    if (options.navigation) {
      $image_holder.append('<a id="fullsized_go_prev" href="#prev"></a><a id="fullsized_go_next" href="#next"></a>');
      $('#fullsized_go_prev').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        return prevImage();
      });
      $('#fullsized_go_next').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        return nextImage();
      });
    }
    if (options.closeButton) {
        $("#fullsized_header").append('<a id="fullsized_close" href="#close" title="close"><i class="icon icon-times"></i></a>');
      $('#fullsized_close').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
        return closeViewer();
      });
    }
    if (options.allowFullscreen && hasFullscreenSupport()) {
      $("#fullsized_header").append('<a id="fullsized_fullscreen" href="#fullscreen" title="Fullscreen"><i class="icon icon-arrows-alt"></i></a>');
      $('#fullsized_fullscreen').click(function(e) {
        e.preventDefault();
        e.stopPropagation();	
        return toggleFullscreen();
      });
    }    
    if (options.navigation) {
        $("#fullsized_header").append('<a id="fullsized_slideshow" href="#slideshow" class="fullsized_slideshow_play" style="" title="Slideshow"><i class="icon icon-play"></i></a>');
        $('#fullsized_slideshow').click(function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (slideShowInterval == null) {
                $("#fullsized_slideshow i")
                    .removeClass("icon-play")
                    .addClass("icon-pause");

                slideShowInterval = window.setInterval(function () {

                    //if ($("." + spinner_class).is(":visible"))
                    //    return;

                    if ($("#fullsized_go_next").is(":visible")) {
                        //if (!$("." + spinner_class).is(":visible"))
                        $("#fullsized_go_next").trigger('click');
                    }
                    else {
                        $("#fullsized_slideshow i")
                            .removeClass("icon-pause")
                            .addClass("icon-play");

                        clearInterval(slideShowInterval);
                        slideShowInterval = null;
                    }
                }, 5000);
            }
            else {                
                $("#fullsized_slideshow i")
                    .removeClass("icon-pause")
                    .addClass("icon-play");

                clearInterval(slideShowInterval);
                slideShowInterval = null;
            }
        });
    }

    $("#fullsized_header").append('<a href="#" id="fullsized_shared" title="Share"><i class="icon icon-share-alt"></i></a>');
    $("#fullsized_header").append("<div class='clearer'></div>")


    selector = this;
    if (!options.dynamic) makeFullsizable();
    return options;
  };

  $.fullsizableDynamic = function(selector, opt) {
    if (opt == null) opt = {};
    return $(selector).fullsizable($.extend(opt, {
      dynamic: selector
    }));
  };

  $.fullsizableOpen = function(target) {
    var image, _i, _len, _results;    
    if (options.dynamic) makeFullsizable();
    _results = [];
    for (_i = 0, _len = images.length; _i < _len; _i++) {
      image = images[_i];
      if (image.mediaid === $(target).attr('media-mediaid')) {
        _results.push(openViewer(image));
      } else {
        _results.push(void 0);
      }
    }
    return _results;
  };

  hasFullscreenSupport = function() {
    var fs_dom;
    fs_dom = $image_holder.get(0);
    if (fs_dom.requestFullScreen || fs_dom.webkitRequestFullScreen || fs_dom.mozRequestFullScreen) {
      return true;
    } else {
      return false;
    }
  };

  closeFullscreen = function() {	
    return toggleFullscreen(true);
  };

  toggleFullscreen = function(force_close) {
    var fs_dom;
    fs_dom = $image_holder.get(0);
	
    if (fs_dom.requestFullScreen) {
      if (document.fullScreen || force_close) {
        return document.exitFullScreen();
      } else {
        return fs_dom.requestFullScreen();
      }
    } else if (fs_dom.webkitRequestFullScreen) {
      if (document.webkitIsFullScreen || force_close) {
        return document.webkitCancelFullScreen();
      } else {
        return fs_dom.webkitRequestFullScreen();
      }
    } else if (fs_dom.mozRequestFullScreen) {
      if (document.mozFullScreen || force_close) {
        return document.mozCancelFullScreen();
      } else {
        return fs_dom.mozRequestFullScreen();
      }
    }
  };

}).call(this);
