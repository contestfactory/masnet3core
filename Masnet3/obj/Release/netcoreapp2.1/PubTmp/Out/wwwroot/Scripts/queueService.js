﻿function QueueService(queueID, status, onUpdateQueueStatus) {
    var self = this;

    self.data = {
        status: status,
        queueID: queueID,
        IntervalId: 0,
        FileURL: "",
        onUpdateQueueStatus: onUpdateQueueStatus
    };

    self.CheckQueue = function () {
        if (self.data.IntervalId > 0)
            clearInterval(self.data.IntervalId);

        self.data.IntervalId = setInterval(self.GetQueueStatus, 5000);
    }

    self.GetQueueStatus = function () {
        $.ajax({
            type: "GET",
            url: SiteURL.Prefix + "/Report/GetQueue",
            data: {
                queueID: self.data.queueID
            },
            success: function(result) {
                self.data.status = result.status;
                self.data.FileURL = result.data;
                if (self.data.onUpdateQueueStatus) {
                    try{
                        self.data.onUpdateQueueStatus(self, result);
                    }
                    catch(ex)
                    {
                        console.log(ex.message);
                    }
                }                
            }
        });
    }

}