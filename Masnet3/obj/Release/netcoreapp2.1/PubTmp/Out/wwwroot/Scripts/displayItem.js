﻿// rule-wrapper template
/*
+ rule-wrapper: container of rule area.
+ data-fieldname: Name of the selected field.
Ex: 
<div class="rule-wrapper" data-fieldname="Field name">
    <a href="javascript:void(0);" onclick="ShowEditRule(this);">Add rule</a><br>    
</div>
*/

// get selected rule information
function GetItemRule(container, RuleKey) {
    if (container) {
        var itemRule = container.find("input[name='RuleKey'][value='" + RuleKey + "']");
        return itemRule.closest("div[name='RuleItemContainer']");
    }
    else {
        return null;
    }
}

// remove selected rule 
function RemoveRuleItem(el, RuleKey) {
    var container = $(el).closest(".rule-wrapper");
    var ruleItem = GetItemRule(container, RuleKey);
    ruleItem.remove();
}

function RebindSHOWHIDE(container) {
    $("select:not(#ddlPages,#slPositions,#Contest_SubmissionPerPeriod,#FeeIds,#VotePerType,#NumOfVotePerUser,#SubmissionPeriod,#RestrictedStates,[name^='Resources['],[data-inorge-showhide],[data_inorge_showhide],#LanguageID,#TemplateLanguageID), " +
      "textarea:not(#RuleText,#PrizeText,[name^='Resources['],[data-inorge-showhide],[data_inorge_showhide]),div#CheckUserCountry,#AllowPostMessage,#SendApproveMessageEmail," +
      "input:text:not(#ApplicationName,#Title,#Contest_Maxvotes,#WinnerSelectionDate,#ResourceKey,#SearchValue,[name^='Resources['],[name^='Rounds'],[data-inorge-showhide],[data_inorge_showhide],#PrizeName,#Quantity,#RetailValue,#txtFilterCountry,#s2id_autogen1,#MaxVideoMinute,#MaxVideoSecond,#MaxAudioMinute,#MaxAudioSecond,#MinVideoMinute,#MinVideoSecond,#MinAudioMinute,#MinAudioSecond,#MaxTextLength,#ExtMaxVideoMinute,#ExtMaxVideoSecond,#ExtMaxAudioMinute,#ExtMaxAudioSecond,#ExtMinVideoMinute,#ExtMinVideoSecond,#ExtMinAudioMinute,#ExtMinAudioSecond,#agentEmail,#ContestName)," +
      "input[type='number']:not([name^='Rounds']),input:radio[name='RuleType'],#hdAutoGen,#PrizeTextHolder,#PrizePhotoHolder,#VotePerType,#IsCustomRuleHolder,#MaxAudioLength,#MaxVideoLength,#MinAudioLength,#MinVideoLength,#hdMaxTextLength,#ExtensionField_MaxAudioLength,#ExtensionField_MaxVideoLength,#ExtensionField_MinAudioLength,#ExtensionField_MinVideoLength,#hdExtensionFieldMaxTextLength" +
      "#Logo,#HeaderBackground, #FooterBackground, [displayShowHide], .show-hide:not([data_inorge_showhide],[data-inorge-showhide]), #Photo", container).each(function( index ) {
        

            //<input id="IsCustomRuleHolder" type="hidden" showHideType="14" fieldName="SH_IsCustomRule" SH_Name="IsCustomRule" idField="DisplayItemID": 
            //SH_Name :  field,  fieldName: SH field name

            var field = $(this).attr("SH_Name") ? $(this).attr("SH_Name") : $(this).attr("SHName") ? $(this).attr("SHName") : $(this).attr("id");
            var prefix = $(this).attr("dataPrefix") ? $(this).attr("dataPrefix") : "ddl_";
            var content = $(this).attr("content") ? $(this).attr("content") : "";
            var value = $(this).attr("dataValue") ? $(this).attr("dataValue") : $(content + " [id='SH_" + field + "']:last ").val();
            var extraData = $(this).attr("extraData") ? $(this).attr("extraData") : "";
            var ddid = prefix + field;
            var showHideType = $(this).attr("showHideType") ? $(this).attr("showHideType") : "";            
            var idField = $(this).attr("idField") ?  $(this).attr("idField") : ""; 
            var fieldName = $(this).attr("fieldName") ? $(this).attr("fieldName") : "";           
            var dataid = $(this).attr("dataid") ? $(this).attr("dataid") : "";
            var onChange = $(this).attr("dataOnChange") ? $(this).attr("dataOnChange") : "ContestTemplate.UpdateDisplay(this);";
            var updateId = $(this).attr("updateId") ? $(this).attr("updateId") : "";
            var positionId = $(this).attr("positionId") ? $(this).attr("positionId") : "";
            var pageId = $(this).attr("pageId") ? $(this).attr("pageId") : "";            
            var editSiteId = $(this).attr("editSiteId") ? $(this).attr("editSiteId") : "";
            var editCampaignId = $(this).attr("editCampaignId") ? $(this).attr("editCampaignId") : "";
            var taget = $(this).closest(".showHideTaget").length > 0 ? $(this).closest(".showHideTaget") : $(this).parent();

            var ddl = $("<select name='" + ddid + "' id='" + ddid + "' title='Display to Agent?' data-inorge-showhide='1' " +
                (showHideType != "" ? "showHideType='" + showHideType + "'" : "") +
                (dataid != "" ? "dataid='" + dataid + "'" : "") +
                (idField != "" ? "idField='" + idField + "'" : "") +
                (fieldName != "" ? "fieldName='" + fieldName + "'" : "") +
                (extraData != "" ? "extraData='" + extraData + "'" : "") +
                (updateId != "" ? "updateId='" + updateId + "'" : "") +
                (positionId != "" ? "positionId='" + positionId + "'" : "") +
                (pageId != "" ? "pageId='" + pageId + "'" : "") +
                (editSiteId != "" ? "editSiteId='" + editSiteId + "'" : "") +
                (editCampaignId != "" ? "editCampaignId='" + editCampaignId + "'" : "") +
                " onchange='" + onChange + "' style='vertical-align:top;border:1px solid #BBFFFF;background-color:#DDFFFF'><option value='1'>SHOW</option><option value='3'>HIDE</option><option value='2'>VIEW</option></select>");
            if (value && value > 0) ddl.val(value);            
            if ($("#" + ddid).size() == 0)
                taget.append(ddl);

        });
}

ContestTemplate = {
    ShowHideAll: function (showHide, editSiteId, editTemplateId, editCampaignId, pageId) {
        var resource = {
            ShowHide: showHide,
            PageID: pageId,
            ApplicationID: editSiteId,
            TemplateID: editTemplateId,
            ContestID: editCampaignId
        };



        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: SiteURL.Prefix + "/Resource/ShowHideAll",
            data: JSON.stringify(resource),
            success: function (msg) {
                if (msg.result == "ok")
                    searchResources();
            }
        });
    },
    UpdateDisplay: function (obj) {
        var fieldName = $(obj).attr("showHideType") ? ($(obj).attr("fieldName") ? $(obj).attr("fieldName") : "ShowHide") : $(obj).attr("id").replace("ddl_", "SH_");  //ShowHide field
        var id = $(obj).attr("showHideType") ? ($(obj).attr("dataid") ? $(obj).attr("dataid") : $(obj).attr("id")) : $("#DisplayItemID").val();
        var idField = $(obj).attr("idField") ? $(obj).attr("idField") : "DisplayItemID";
        var LanguageID = $(obj).attr("languageid") ? $(obj).attr("languageid") : $("#LanguageID").val() ? $("#LanguageID").val() : "1";

        var extraData = $(obj).attr("extraData") ? $.parseJSON($(obj).attr("extraData")) : {};
        var positionID = $(obj).attr("positionId") ? $(obj).attr("positionId") : "1";
        var pageId = $(obj).attr("pageId") ? $(obj).attr("pageId") : "0";
        var editSiteId = $(obj).attr("editSiteId") ? $(obj).attr("editSiteId") : $("#ApplicationID").val();
        var editCampaignId = $(obj).attr("editCampaignId") ? $(obj).attr("editCampaignId") : $("#ContestID").val();

        $.ajax({
            type: "POST",
            url: SiteURL.Prefix + "/ContestTemplate/UpdateDisplay",
            data: $.extend(extraData, {
                id: id,
                name: fieldName,
                value: $(obj).is(':checkbox') ? ($(obj).is(":checked") ? 1 : 0) : $(obj).val(),
                type: $(obj).attr("showHideType"),
                idField: idField,
                editTemplateID: $("#ContestTemplateID").val(),
                editSiteId: editSiteId,
                editCampaignId: editCampaignId,
                LanguageID: LanguageID,
                position: positionID,
                pageId: extraData.PageID ? extraData.PageID :  pageId
            }),
            success: function (msg) {
                var objId = $(obj).attr("name");

                $($(obj).attr("id").replace("ddl_", "#SH_")).val($(obj).val());
                if (msg != "") {
                    $(obj).attr("dataid", msg);
                    if (window.UpdateDataId)
                        UpdateDataId(objId, msg);
                }
                if ($(obj).attr("updateId"))
                    $("#" + $(obj).attr("updateId")).val($(obj).val());

                if ($(obj).attr("showHideType") == "2" && window.searchResources)
                    searchResources();
            }
        });
    },
    UpdateMenuDisplay: function (obj, evt) {
        /* class="icon-eye" dataValue="1" dataid="188" style="cursor: pointer; color: darkgreen; */
        var LanguageID = $("#LanguageID").val() ? $("#LanguageID").val() : "1";
        var value = $(obj).attr("dataValue") == "1" ? "0" : "1";
        var editSiteID = $(obj).attr("editSiteID") ? $(obj).attr("editSiteID") : 0;
        var editCampaignID = $(obj).attr("editCampaignID") ? $(obj).attr("editCampaignID") : 0;
        var editTemplateID = $(obj).attr("templateID") ? $(obj).attr("templateID") : 0;

        if (!editSiteID) {
            editSiteID = $("#ApplicationID").val();
        }

        if (!editTemplateID) {
            editTemplateID = $("#ContestTemplateID").val();
        }
        $.ajax({
            type: "POST",
            url: SiteURL.Prefix + "/ContestTemplate/UpdateMenuDisplay",
            data: {
                menuKey: $(obj).attr("dataid"),
                value: value,
                editTemplateID: editTemplateID,
                editSiteId: editSiteID,
                editCampaignID: editCampaignID,
                LanguageID: LanguageID
            },
            success: function (msg) {
                $(obj).attr("dataValue", value).toggleClass("icon-eye icon-eye-slash");
            }
        });
                    
        if (evt) {
            evt.preventDefault();
            evt.stopPropagation();
        }
            
    }
}

function BindMenuShowHide(container, isLeftMenu) {
    $.each($(".dataMenuShowHide", container), function (index, el) {
        var item = $(el);
        var menuKeyID = item.attr("dataid");
        var menuValue = item.attr("dataValue");
        var editSiteID = item.attr("editsiteid") ? item.attr("editsiteid") : 0;
        var editCampaignID = item.attr("editcampaignid") ? item.attr("editcampaignid") : 0;
        var templateID = item.attr("templateid") ? item.attr("templateid") : 0;

        var isHidden = menuValue == "0";

        var icon = "<i title=' Show to Agent ? ' dataid='" + menuKeyID
            + "' editSiteID='" + editSiteID + "' editCampaignID='" + editCampaignID + "' templateID='" + templateID + "'"
            + "' fieldName='MenuDisplays_' dataValue='" + menuValue + "' class='icon-show-hide icon-eye" + (isHidden ? "-slash" : "")
            + "' onclick='ContestTemplate.UpdateMenuDisplay(this,event)'></i>";

        item.find('a:first').append(icon);
    });
}

function openSetting()
{
    if($("#dvDisplayItemSetting").length > 0)
    {
        $("#dvDisplayItemSetting").dialog({
            modal: true,
            width: 500,
            lgClass: true
        });
        $("#dvDisplayItemSetting").dialog("open");
        $("#dvDisplayItemSetting").dialog("option", "title", "Setting");
        $("input:checkbox[name='item-name']").bootstrapSwitch();
        $("input:checkbox[name='item-check-download']").bootstrapSwitch();
    }
}
