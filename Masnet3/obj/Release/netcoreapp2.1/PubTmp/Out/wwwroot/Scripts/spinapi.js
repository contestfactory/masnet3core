﻿var spinModel = {
    currentSpin: null,
    languages: {}
};

//loadLanguages("Spin_NoSpinsLeft", SiteConfig.pagename, spinModel);

function getSpinSound() {
    var url = SiteURL.Root + "Content/Themes/" + SiteConfig.ThemeFolder + "/Audio/Notification1.mp3";
    var audioElement = $("audio#audioWin");
    if (audioElement.size() == 0) {
        audioElement = $("<audio id='audioWin'></audio>").appendTo("body");
        audioElement.append("<source src='" + url + "' >");
        //audioElement.get(0).play();
        //audioElement.get(0).pause();
    }
    return audioElement;
}

// init Spin Sound when page load
getSpinSound();

function playSound() {    
    var audioElement = getSpinSound();
    audioElement.get(0).play();
}

function getSpins() {
    $.ajax({
        url: SiteURL.Prefix + "/quiz/getspins",
        async: false,
        success: function (result) {            
            if ($('#dvDialog').size() == 0)
                $("<div id='dvDialog' style='display:none'></div>").appendTo("body");

            if (result.result == "ok") {
                var numberSpins = result.numberSpins;

                if (spinModel.currentSpin != null && spinModel.currentSpin < numberSpins) {
                    playSound();
                    $(".spinnum").removeClass("zoominout").addClass("zoominout");
                }
                spinModel.currentSpin = numberSpins;

                if (numberSpins == 0) {
                    $(".icon-bell").hide();
                    $(".spinnum").hide();
                    $("#spintab").addClass("disabledtab");
                }
                else {
                    $("#spintab").removeClass("disabledtab");
                    $(".spinnum").html(numberSpins);
                    $(".spinnum").show(); $(".icon-bell").show();
                }
            } else {
                showDialog({
                    title: SiteContent.SiteTitle,
                    modal: true,
                    dSize: "modal-sm",
                    message: result.message
                });
            }
        }
    });
}

function getSpinsNotPlaySound() {
    $.ajax({
        url: SiteURL.Prefix + "/quiz/getspins",
        async: false,
        success: function (result) {
            if ($('#dvDialog').size() == 0)
                $("<div id='dvDialog' style='display:none'></div>").appendTo("body");

            if (result.result == "ok") {
                var numberSpins = result.numberSpins;

                if (spinModel.currentSpin != null && spinModel.currentSpin < numberSpins) {
                    $(".spinnum").removeClass("zoominout").addClass("zoominout");
                }
                spinModel.currentSpin = numberSpins;

                if (numberSpins == 0) {
                    $(".icon-bell").hide();
                    $(".spinnum").hide();
                    $("#spintab").addClass("disabledtab");
                }
                else {
                    $("#spintab").removeClass("disabledtab");
                    $(".spinnum").html(numberSpins);
                    $(".spinnum").show(); $(".icon-bell").show();
                }
            } else {
                showDialog({
                    title: SiteContent.SiteTitle,
                    modal: true,
                    dSize: "modal-sm",
                    message: result.message
                });
            }
        }
    });
}

function playSpin() {
    $.ajax({
        url: SiteURL.Prefix + "/quiz/playspin",
        success: function (result) {
            if (result.result == "ok") {
                if (result.numberSpins == 0) {
                    showDialog({
                        title: SiteContent.SiteTitle,
                        modal: true,
                        dSize: "modal-sm",
                        message: "Sorry, you don’t have any spins right now. Keep answering questions to earn spins."
                    });
                }
                else {
                    location.href = result.url;
                }
            } else {
                showDialog({
                    title: SiteContent.SiteTitle,
                    modal: true,
                    dSize: "modal-sm",
                    message: result.message
                });
            }
        }
    });
}

$(document).ready(function () {
    // Handler for .ready() called.
    $(".page-wrapper").after($(".footer-wrapper"));
    $(".main-footer").removeClass("hide");
});