﻿/// <reference path="~/Scripts/jquery/jquery-1.5.1.js" />
// Plugin for the Brettle.Web.NeatUpload .NET file uploading module
// Author: Abel Pérez Martínez
// Email: abelperezok@gmail.com
// Version: 0.5

(function ($) {
    var globalIndex = Math.round(Math.random() * 1000000000); //GLobal element index, it's different for each post.
    var process = {};    //List for store the status of each post.
    var sending = 1;     //Constant indicating that one process is being sent.
    var canceling = 2;   //Constant indicating that one process is being canceled.

    // Internal function that is responsible for add a new hidden block (form + iframe)
    function addFile(plugin, input) {
        var op = plugin.options;
        var currentIndex = ++globalIndex;

        // build the form and the group for put in the hidden div at the bottom of the page.
        var formId = "form" + op.containerId + "_" + currentIndex;
        var frameName = "frame" + op.containerId + "_" + currentIndex;
        var mainGroup = $('<div id="mainGroup"></div>');
        mainGroup.attr('data-neatupload-index', currentIndex);
        createInputFile(plugin);
        var form = $('<form id="' + formId + '" action="' + op.postDataUrl + '" method="post" enctype="multipart/form-data" target="' + frameName + '"></form>');
        input.appendTo(form)
        .attr("name", 'NeatUpload_' + currentIndex + '-' + op.fileField)
        .attr("id", "fileID");
        var frame = $('<iframe name="' + frameName + '" src="about:blank" width="1px" height="1px" frameborder="0"></iframe>')
        .load(function () {
            var html, reason;
            try {
                html = this.contentWindow.document.body.innerHTML;
                reason = "completed";
            } catch (e) {
                html = "ERROR";
                reason = "canceled";
            }
            // Avoid Access denied error message in some cases by a bad response from the server
            if (html != "") {
                removeFile(plugin, currentIndex, reason, html);
                submitForm(plugin);
            }
        });
        mainGroup.append(form).append(frame);
        plugin.container.append(mainGroup);

        // just pass as parameter the filename of the input type=file
        var arr = input.val().split("\\");
        op.fnAddFile(plugin, arr[arr.length - 1], form, currentIndex);
    }

    // Removes the internal block corresponding to 'index' and calls fnRemoveFile handler for custom client code.
    function removeFile(plugin, index, reason, html) {
        $('div[data-neatupload-index=' + index + ']').remove();
        plugin.options.fnRemoveFile(plugin, index, reason, html);
    }

    // Creates the Cancel link for each file in the list for upload
    function createCancelLink(plugin, index) {
        var cancel = $('<a href="javascript:">' + plugin.options.textCancel + '</a>')
        .click(function () { plugin.options.sendCancel(plugin, index); });
        return cancel;
    }

    // Adds a new Visual element (basic) for visual feedback to the user
    // May be overriden by the user via parameter fnAddFile in plugin constructor 
    function addVisualElemet(plugin, filename, form, index) {
        var cancel = plugin.options.createCancelLink(plugin, index);
        $('<div id="list" data-neatupload-index="' + index + '"></div>')
            .appendTo(plugin)
            .append(cancel)
            .append('<span>' + filename + '</span>')
            .append('<div id="progress"></div>');
    }

    // Internal function that creates an input type=file and appends to the container 
    function createInputFile(plugin) {
        $('<input id="newfield" type="file" name="NeatUpload_replacedByScript-' + plugin.options.fileField + '" accept="' + plugin.options.acceptExtensions + '" />')
        .appendTo($('div.filecont', plugin));
    }

    // Internal function that makes the actual submit to the server but before verifies the length of the queue
    function submitForm(plugin) {
        var list = $('div[id=mainGroup][data-neatupload-index]:eq(0)', plugin.container);
        var index = list.attr('data-neatupload-index');

        // return if already sending this item 
        if (process[index] && process[index] == sending) return;

        if (list.length > 0) {
            var form = $('form', list);
            form.get(0).submit();
            process[index] = sending;
            setTimeout(function () { loadData(plugin, index, 'iframe[name=' + $(form).attr("target") + ']'); }, plugin.options.pollInterval);
        }
        else {
            plugin.options.fnUploadComplete(plugin);
        }
    }

    // Internal function that responds to the load event for every iframe that makes a post to the server 
    function loadData(plugin, index, iframeSelector) {
        var url = plugin.options.statusUrl.replace(/\{0\}/, index).replace(/\{1\}/, plugin.options.fileField);
        $.getJSON(url, {}, function (data, textStatus) {
            // Call function that show visual progress (may be a cute progress bar or an ugly number and % symbol).
            plugin.options.fnShowStatus(plugin, index, data);
            // verify the status and then stop or continue polling the server
            if (data.Status == 'Cancelled' || data.Status == 'Rejected' || data.Status == 'Failed') {
                setTimeout(function () { stopData(plugin, iframeSelector, index); }, 1);
            }
            else {
                if (data.Status != "Completed")
                    setTimeout(function () { loadData(plugin, index, iframeSelector); }, plugin.options.pollInterval);
            }
        });
    }

    // Function that cancel the current uploading process indicated by index 
    function sendCancel(plugin, index) {
        if (process[index] && process[index] != canceling) {
            process[index] = canceling;
            var url = plugin.options.cancelUrl.replace(/\{0\}/, index);
            $.get(url);
        }
        else {
            removeFile(plugin, index, "canceled", "");
        }
    }

    // Internal function that stops the process of send data to server in the corresponding internal iframe
    function stopData(plugin, iframe, index) {
        var ifm = $(iframe);
        if (ifm.length) {
            var win = ifm[0].contentWindow;
            if (win.stop)
                win.stop();
            else if (win.document && win.document.execCommand)
                win.document.execCommand('Stop');
        }
        delete process[index];
        removeFile(plugin, index, "canceled", "");
        submitForm(plugin);
    }

    // Start up point of the plugin
    $.fn.neatupload = function (options) {
        this.options = $.extend({}, $.fn.neatupload.defaults, options);
        var plugin = this;
        if (plugin.length > 0) {
            // Search the container and if not found then create it.
            if (!$("div#" + plugin.options.containerId).length) {
                plugin.container = $('<div id="' + plugin.options.containerId + '"  style="display:none"></div>');
                $(document.body).append(plugin.container);
            }
            else {
                plugin.container = $("div#" + plugin.options.containerId);
            }
            plugin.empty().append('<div class="filecont"></div>');
            createInputFile(plugin);

            var btnAdd = $(plugin.options.selectorButtonAdd);
            if (!btnAdd.length) {
                btnAdd = $('<a href="javascript:" class="add">' + plugin.options.textAdd + '</a>');
                btnAdd.appendTo(plugin);
            }
            btnAdd.click(function () {
                var input = $('input[type=file][id=newfield]', plugin);
                if (input.val() == "" || input.val() == null) return;
                if (!plugin.options.fnValidate(plugin)) return;
                addFile(plugin, input);
            });

            var btnUpload = $(plugin.options.selectorButtonUpload);
            if (!btnUpload.length) {
                btnUpload = $('<a href="javascript:" class="upload">' + plugin.options.textUpload + '</a>');
                btnUpload.appendTo(plugin);
            }
            btnUpload.click(function () { submitForm(plugin); });
        }
        return plugin;
    };

    // Defaults for the plugin (works well if using the module's defaults).
    $.fn.neatupload.defaults = {
        containerId: "neatuploadContainer", // Just an id for HTML container (hidden).
        postDataUrl: "/StaticUploadHandler.ashx", //Path to handler/page/Controller who is responsible for receive the data.
        fileField: "fileField", // field name ending part (recomended not modify).
        submitSelector: "",   // for now is empty (recomended not modify).
        statusUrl: "/ProgressJsonHandler.ashx?postBackID={0}&controlID={1}", // Path to the handler/page/Controller who is responsible for returning the JSON with the status data.
        cancelUrl: "/ProgressJsonHandler.ashx?postBackID={0}&cancel=true", // Path to the handler/page/Controller who is responsible for set the cancel status on the server.
        pollInterval: 1000,  // The interval between poll and poll for request the status.
        fnAddFile: addVisualElemet, // Function that adds visual elements (DOM) for the corresponding hidden elements at the bottom of the page.
        fnShowStatus: function (data) { }, // Function that is called when the server sends a status chance notifycation.
        textAdd: "Add", // Text for the link/button Add, may be overriden using the parameter selectorButtonAdd.
        textUpload: "Upload", // Text for the link/button Upload, may be overriden using the parameter selectorButtonUpload.
        textCancel: "Cancel", // Text for the link/button Cancel, may be overriden using the parameter createCancelLink which is a function.
        selectorButtonAdd: "", // jQuery selector indicating how to get the DOM element that will click for the add action.
        selectorButtonUpload: "", // jQuery selector indicating how to get the DOM element that will click for the upload action.
        createCancelLink: createCancelLink, // Function responsible for creating the cancel link for every file in the list.
        fnRemoveFile: function (plugin, index, reason, html) { }, // Function that is called when a file finish the copy on the server.
        fnValidate: function (plugin) { return true; }, // Function that may execute some validation code, for example: validate the filename in one input text.
        fnUploadComplete: function (plugin) { }, // Function that is called when all uploads were done, may be used for visual feedback in the client.
        acceptExtensions: "", // for now is empty (recomended not modify).
        sendCancel: sendCancel // internal function that might be called from overriden methods.
    };

})(jQuery);