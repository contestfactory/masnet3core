﻿
if (ValidatorMessages) {
    $.extend($.validator.messages, {
        required: ValidatorMessages.required,
        email: ValidatorMessages.email,
        url: ValidatorMessages.url,
        date: ValidatorMessages.date,
        digits: ValidatorMessages.digits,
        safe: ValidatorMessages.safe
    });

    $.validator.addMethod("datevalidation", function (value, element) {
        return this.optional(element) || isDateTime(value);
    }, ValidatorMessages.datevalidation);
}