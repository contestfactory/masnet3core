﻿
var ExportService = {
    status: 0,
    queueID: 0,
    IntervalId: 0,
    FileURL: "",
    onSuccess: null
};

//Start Service
function CheckQueue(success) {

    if (ExportService.IntervalId > 0)
        clearInterval(ExportService.IntervalId);

    ExportService.onSuccess = success;
    ExportService.IntervalId = setInterval(GetQueueStatus, 5000);
    // show loading icon
    fadeIn();
}

function resetQueue() {
    ExportService.status = 0,
    ExportService.queueID = 0,
    ExportService.IntervalId = 0,
    ExportService.onSuccess = null;
}

function GetQueueStatus() {

    $.ajax({
        type: "GET",
        url: SiteURL.Prefix + "/Report/GetQueue", 
        data: {
            queueID: ExportService.queueID
        },
        success: function (result) {
            ExportService.status = result.status;
            ExportService.FileURL = result.data;
            if (result.status == 1 || result.status == 2) {
                // hide loading icon
                fadeOut();
                clearInterval(ExportService.IntervalId);                
            }
            if (ExportService.onSuccess)
                ExportService.onSuccess(result);
            
        }
    });
}