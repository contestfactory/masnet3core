function getParameterByName(name) {
    var referrerQuerystring = function () {
	  try {
		  var temp = document.referrer.split('?')[1] || '';
		  return '?' + temp;
	  }
	  catch (Err) {
		  return '';
	  }
	};

	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS, "i");
	var src = (window.self === window.top) ? window.location.search : referrerQuerystring();
	var results = regex.exec(src);
	if (results == null){
		return "";
	}else{
		return decodeURIComponent(results[1].replace(/\+/g, " "));
	}
}

// self executing function here
(function() {
   // your page initialization code here
   // the DOM will be available here
	var url = '';
	var re = getParameterByName("re");
	if (re){
		url = re;
		//$("#iframe-sweeps").attr('src', url);   
		document.getElementById('iframe-sweeps').src = url;
	}
	
	//var baseDomain = "https://apps-ugc.contestfactory.com";
	var baseDomain = window.location.origin;	
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];
	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";
	var iframe;

	// Listen to message from child window
	eventer(messageEvent,function(e) {
	  var key = e.message ? "message" : "data";
	  var data = e[key];

	  if (e.origin !== baseDomain && false) //checking which domain sent message
	  {
		//console.log("error: domain not allowed");
		//return;
	  }
	  else
	  {
		var msg = data.split(":")[0];
		var val = data.split(":")[1];

		switch( msg )
		{
		  case "scrollPage":
			scrollPage(val);
			break;
		  case "setIframeHeight":
			setIframeHeight(val);
			break;
		  case "scrollIframeTop":
			scrollPage(0);
			break;
		}

	  }
	},false);
	
	var scrollPage = function(h)
	{
		//$('html, body').stop().animate({
		//  scrollTop: h
		//}, 100);
		window.scrollTo(0, h);
	}
	var setIframeHeight = function(h)
	{
	  // console.log("setIframeHeight: " + h);
	  //$("#iframe-sweeps").css("height", h);
	  document.getElementById("iframe-sweeps").height = h; 
	}

	/** mobile orientation change listener (notify iframe when this happened)*/ 
	function doOnOrientationChange()
	{
	    if (!iframe)
	        iframe = document.getElementById("iframe-sweeps").contentWindow;

	    getIframe().postMessage(
			"orientationChanged",
			"*");
	}
	window.addEventListener('orientationchange', doOnOrientationChange);
	
	function getIframe() {
	    if (!iframe)
	        iframe = document.getElementById("iframe-sweeps").contentWindow;

	    return iframe;
	}

    //scroll event
	function onScrollEventHandler(ev) {
        //console.log(ev);
	    var doc = document.documentElement;
	    var left = (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0);
	    var top = (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0);
	    var frameTop = getOffsetTop(document.getElementById("iframe-sweeps"));
	    //console.log('top: ' + top);

	    getIframe().postMessage(
			"onScroll:" + (top - frameTop),
			"*");
	}
	var el = window;
	if (el.addEventListener)
	    el.addEventListener('scroll', onScrollEventHandler, false);
	else if (el.attachEvent)
	    el.attachEvent('onscroll', onScrollEventHandler);
	
    //get element offset top
	function getOffsetTop(elem) {
	    var offsetTop = 0;
	    do {
	        if (!isNaN(elem.offsetTop)) {
	            offsetTop += elem.offsetTop;
	        }
	    } while (elem = elem.offsetParent);
	    return offsetTop;
	}
})();