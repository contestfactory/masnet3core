
//--------------------------------------
//  PROPERTIES
//--------------------------------------
/**
/**
 *  
 */
var preloadedImageArr = new Array ();
var loadedIndex = 0;//loadedIndex
var _randomStartIndexes = [];

//--------------------------------------
//  IMAGE PRELOAD FUNCTIONS
//--------------------------------------
/**
/**
 *  
 */
function loadEvent(event)
{
	//alert("loaded: " + event.target.src);	
	
	//recording width, height
	prizeImgArr[loadedIndex][1] = preloadedImageArr[loadedIndex].width;
	prizeImgArr[loadedIndex][2] = preloadedImageArr[loadedIndex].height;
	
	if( loadedIndex + 1 < prizeImgArr.length )
	{
		//document.body.appendChild( preloadedImageArr[loadedIndex] );
		//$("#prize" + loadedIndex).html( $(preloadedImageArr[0]).clone() );
		
		loadedIndex++;
		preloadImage( prizeImgArr[loadedIndex][0] );
		$(".loadPerc").css("width", 147*(loadedIndex / (prizeImgArr.length - 1)));
	}
	else
	{
		//alert("all loaded");	
		prepareRandomStartIndexes();
		showSpinZone();
		$(".preloader").hide();
		$("#btnSpinTheWheel").show();
	}
}
function loadError()
{
	//alert("loadError");
}

function startLoading()
{
	preloadImage( prizeImgArr[loadedIndex][0] );
}

function preloadImage(url)
{
	//new Element("img", {id: "img0", src: url});	
	preloadedImageArr[loadedIndex]= new Image();
	preloadedImageArr[loadedIndex].src = url;
	preloadedImageArr[loadedIndex].onload = loadEvent;
	preloadedImageArr[loadedIndex].onerror = loadError;
}
//--------------------------------------
//  IMAGE UTILS FUNCTIONS
//--------------------------------------
/**
/**
 *  
 */
function prepareRandomStartIndexes()
{
	_randomStartIndexes = [];
	for (var i = 0; i < columnsWheels; i++) 
	{
		_randomStartIndexes.push( Math.floor( Math.random() * prizeImgArr.length) )
	}
	//Debugger.trace("PrizeLoader.totalPrizes: " + PrizeLoader.totalPrizes, 2, Images);
	//Debugger.trace("isUnique: " + checkUnique(_randomStartIndexes), 2, Images);
	
	//Debugger.traceObj(_randomStartIndexes);
	
	if ( checkUnique(_randomStartIndexes) ) 
	{
		var attempt = 1;
		do 
		{
			//_randomStartIndexes[ _randomStartIndexes.length - 1 ] = Math.floor( Math.random() * PrizeLoader.totalPrizes);
			attempt++;
		}
		while ( checkUnique(_randomStartIndexes) );
	}
	//debugMsg("_randomStartIndexes: " + _randomStartIndexes);
}


function checkUnique(data)
{
	var isUnique = true;
	var tmpValue = data[0];
	
	for (var i = 1; i < data.length; i++) 
	{
		if ( data[i] != tmpValue ) 
		{
			isUnique = false;
			break;
		}
	}
	return isUnique;
}
function getRandomPrize()
{
    var randomIndex = Math.floor(prizeImgArr.length * Math.random());
    var objectImg = $(preloadedImageArr[randomIndex]).clone(true);
	setAlignment(objectImg, randomIndex);
	return objectImg;
	//return [$(preloadedImageArr[randomIndex]).clone(true), randomIndex];
}

function getPrizeByID( index )
{
	var objectImg = $(preloadedImageArr[index]).clone(true);
	setAlignment(objectImg, index);
	return objectImg;
}

function setAlignment(objectImg, index)
{
    //var marginLeft = Math.round( ( 218 - prizeImgArr[index][1] ) / 2);
    //var marginTop = Math.round((179 - prizeImgArr[index][2]) / 2);
    var marginTop = Math.round((140 - prizeImgArr[index][2]) / 2);
    //$(objectImg).css({ "margin-top": marginTop });
}

