/*jQuery.fn.countdown=function(i){var h={stepTime:60,format:"dd:hh:mm:ss",startTime:"01:12:32:55",digitImages:6,digitWidth:53,digitHeight:77,timerEnd:function(){},image:"digits.png",};var d=[],e;var c=2;var a=function(y){var j=0;var o=0;var s=0;var w=0;if(typeof h.startTime=="object"){var v=new Date();if(h.startTime.getTime()<v.getTime()){h.startTime.setFullYear(h.startTime.getFullYear()+1)}var l=Math.ceil((h.startTime.getTime()-v.getTime())/1000);var m=Math.floor(l/86400);var p=Math.floor((l%86400)/3600);var t=Math.floor(((l%86400)%3600)/60);var x=((l%86400)%3600)%60;h.startTime=m+":"+p+":"+t+":"+x}_startTime=h.startTime.split("");cCounter=0;for(var q=0;q<_startTime.length;q++){if(isNaN(parseInt(_startTime[q]))){cCounter=cCounter+1}}var k=h.startTime.split(":");var u="";for(var q=0;q<k.length;q++){var r=59;if(k.length==3){if(q==0){r=23}display_ddTitle=false}if(k.length==4){if(q==0){r=9999}if(q==1){r=23}c=Math.min(Math.max(k[0].length,2),4)}if(k[q]>r){k[q]=r}if(k[q].length<2){k[q]="0"+k[q]}}h.startTime=k.join(":");switch(cCounter){case 3:if(h.startTime.split(":")[0].length==4){h.format="dddd:hh:mm:ss"}else{if(h.startTime.split(":")[0].length==3){h.format="ddd:hh:mm:ss"}else{h.format="dd:hh:mm:ss"}}break;case 2:h.format="hh:mm:ss";break;case 1:h.format="mm:ss";break;case 0:h.format="ss";break}h.startTime=h.startTime.split("");h.format=h.format.split("");for(var q=0;q<h.startTime.length;q++){if(parseInt(h.startTime[q])>=0){var n=jQuery('<div id="cnt_'+q+'" class="cntDigit" />').css({height:h.digitHeight*h.digitImages*10,"float":CountdownText.CssLeft,background:"url('"+h.image+"')",width:h.digitWidth});d.push(n);f(j,-((parseInt(h.startTime[q])*h.digitHeight*h.digitImages)));d[j].__max=9;switch(h.format[q]){case"h":if(o<1){d[j].__max=2;o=1}else{d[j].__condmax=3}break;case"d":d[j].__max=9;break;case"m":if(s<1){d[j].__max=5;s=1}else{d[j].__condmax=9}break;case"s":if(w<1){d[j].__max=5;w=1}else{d[j].__condmax=9}break}++j}else{n=jQuery('<div class="cntSeparator'+h.digitHeight+'"/>').css({"float":CountdownText.CssLeft}).text(h.startTime[q])}y.append("<div>");y.append(n);y.append("</div>")}};var f=function(j,l){try{if(l!==undefined){return d[j].css({marginTop:l+"px"})}return parseInt(d[j].css("marginTop").replace("px",""))}catch(k){return 0}};var g=function(k){d[k]._digitInitial=-(d[k].__max*h.digitHeight*h.digitImages);return function j(){mtop=f(k)+h.digitHeight;if(mtop==h.digitHeight){f(k,d[k]._digitInitial);if(k>0){g(k-1)()}else{clearInterval(e);for(var l=0;l<d.length;l++){f(l,0)}h.timerEnd();return}if((k>0)&&(d[k].__condmax!==undefined)&&(d[k-1]._digitInitial==f(k-1))){f(k,-(d[k].__condmax*h.digitHeight*h.digitImages))}return}f(k,mtop);if(f(k)/h.digitHeight%h.digitImages!=0){setTimeout(j,h.stepTime)}if(mtop==0){d[k].__ismax=true}}};$.extend(h,i);this.css({height:h.digitHeight,overflow:"hidden"});a(this);var b=function(k){if($.trim(h.startTime.toString().replace(/\,/g,"")).length<12){k.prepend("<div class='cntDigit' style='width:"+h.digitWidth+"px;float:"+CountdownText.CssLeft+"'>&nbsp;</div>")}var j="";j+='<div class="clockdesc'+h.digitHeight+'" style="margin: 0 3px;">';j+="   <div>"+CountdownText.Seconds+"</div>";j+="   <div>"+CountdownText.Minutes+"</div>";j+="   <div>"+CountdownText.Hours+"</div>";j+="   <div>"+CountdownText.Days+"</div>";j+='   <div style="clear:both;"></div>';j+="</div>";k.after(j)};b(this);e=setInterval(g(d.length-1),1000)};*/

/*
 * jquery-counter plugin
 *
 * Copyright (c) 2009 Martin Conte Mac Donell <Reflejo@gmail.com>
 * Dual licensed under the MIT and GPL licenses.
 * http://docs.jquery.com/License
 */
jQuery.fn.countdown = function(userOptions)
{
	// Default options
  	var options = {
    	stepTime: 1000,
		// startTime and format MUST follow the same format.
		// also you cannot specify a format unordered (e.g. hh:ss:mm is wrong)
		format: "dd:hh:mm:ss",
		startTime: "01:12:32:55",
		currentTime: new Date(),
		digitImages: 6,
		digitWidth: 53,
		digitHeight: 77,
		timerEnd: function(){},
		image: "digits.png"
	};
  	var digits = []; 
	var interval;
	var __max = [];
	var divisionRemainder;
	
	var parseDate = function()
	{
		// var timeLeft = Math.round((options.startTime - options.currentTime) / 1000);
		var secs;
		var mins;
		var hrs;		
		var days;
		if(timeLeft > 0)
		{
			mins = Math.floor(timeLeft / 60);
			hrs = Math.floor(mins / 60);
			days = Math.floor(hrs  / 24);
			secs = timeLeft - mins * 60;
			
			mins = mins - hrs * 60;
			hrs = hrs - days * 24;
			
			mins = addZero(mins);
			hrs = addZero(hrs);
			days = addZero(days);
			secs = addZero(secs);

			/*if(days > 0)
				options.startTime = days + ":" + hrs + ":" + mins + ":" + secs;
			else
			{
				options.startTime = hrs + ":" + mins + ":" + secs;
				$(".daysHours .days").css("display", "none");
			}*/
			options.startTime = days + ":" + hrs + ":" + mins + ":" + secs;
			
			$(".daysHours").css("display", "block");
			
			if(days > 99)
				options.format = "ddd:hh:mm:ss";
			if(days == 0)
				options.format = "hh:mm:ss";
			options.format = "dd:hh:mm:ss";


			divisionRemainder = options.startTime.split(":")[0].length % 2;
			options.startTime = options.startTime.split("");
			options.format = options.format.split("");
		}
		else
		{
			options.startTime = "00:00:00:00";
		}
		
		//options.startTime = "999:23:00:00";
		//options.format = "ddd:hh:mm:ss";
	}
	
	//function converts 1 to 01
	var addZero = function(obj)
	{
		if(Number(obj) < 10)
			obj = String("0" + obj);
		return obj;
	}
	
  	// Draw digits in given container
  	var createDigits = function(where) 
  	{
		//alert("createDigits");
		var c = 0;
		// Iterate each startTime digit, if it is not a digit
		// we'll asume that it's a separator
		
		// divisionRemainder = options.startTime.split(":")[0].length % 2;
		
		// options.startTime = options.startTime.split("");
		// options.format = options.format.split("");
		//options.startTime = replaceStrings(options.startTime, ":");
		//options.format = replaceStrings(options.format, ":");		
		// alert(options.startTime.length);
		var showLabel = options.showLabel === 1;
        if (showLabel) {
            var divContainer = $('<div class="cnt-digit-container"></div>');
            var whereContainer;

            where.html("");

            where.append(divContainer);
            whereContainer = $(where).children(".cnt-digit-container");

            for (var i = 0; i < options.startTime.length; i++) {


                if (parseInt(options.startTime[i]) >= 0) {
                    //create html for one digit
                    elem = $('<div id="cnt_' + c + '" class="cntDigit"><div>').css({
                        // background: 'url(\'' + options.image + '\')',
                        float: 'left',
                        width: options.digitWidth - 2,
                        height: options.digitHeight,
                        "line-height": options.digitHeight + "px"
                    });
                    //add new div into digits array
                    digits.push(elem);
                    //setting initial margin top
                    whereContainer.append(elem);
                    // margin(c, -((parseInt(options.startTime[i]) * options.digitHeight * options.digitImages)));
                    margin(c, (parseInt(options.startTime[i])));

                    // Add max digits, for example, first digit of minutes (mm) has 
                    // a max of 5. Conditional max is used when the left digit has reach
                    // the max. For example second "hours" digit has a conditional max of 4 

                    switch (options.format[i]) {
                        case 'h':
                            __max.push((c % 2 == divisionRemainder) ? 2 : 3);
                            if ($(whereContainer).children(".days").length == 0)
                                whereContainer.prepend($('<div class="days">Hours</div>'));
                            break;
                        case 'd':
                            __max.push(9);
                            if ($(whereContainer).children(".days").length == 0)
                                whereContainer.prepend($('<div class="days">Days</div>'));
                            break;
                        case 'm'://minutes will get same __max as seconds
                            if ($(whereContainer).children(".days").length == 0)
                                whereContainer.prepend($('<div class="days">Minutes</div>'));
                            break;
                        case 's':
                            __max.push((c % 2 == divisionRemainder) ? 5 : 9);
                            if ($(whereContainer).children(".days").length == 0)
                                whereContainer.prepend($('<div class="days">Seconds</div>'));
                            break;
                    }
                    ++c;
                }
                else {
                    elem = $('<div class="cntSeparator"/>').css({
                        float: 'left',
                        "line-height": options.digitHeight + "px",
                        "margin-top": "20px"
                    })
                            .html(":");
                    //.text(options.startTime[i]);
                    where.append(elem);
                    if (i < options.startTime.length)
                    {
                        where.append($('<div class="cnt-digit-container"></div>'));
                        whereContainer = $(where).children(".cnt-digit-container:last-child");
                    }
                }
                // alert( "digits.length: " + digits.length );
            }
        } else {
            for (var i = 0; i < options.startTime.length; i++) {
                if (parseInt(options.startTime[i]) >= 0) {
                    //create html for one digit
                    elem = $('<div id="cnt_' + c + '" class="cntDigit"><div>').css({
                        // background: 'url(\'' + options.image + '\')',
                        float: 'left',
                        width: options.digitWidth - 2,
                        height: options.digitHeight,
                        "line-height": options.digitHeight + "px"
                    });
                    //add new div into digits array
                    digits.push(elem);
                    //setting initial margin top
                    where.append(elem);
                    // margin(c, -((parseInt(options.startTime[i]) * options.digitHeight * options.digitImages)));
                    margin(c, (parseInt(options.startTime[i])));

                    // Add max digits, for example, first digit of minutes (mm) has 
                    // a max of 5. Conditional max is used when the left digit has reach
                    // the max. For example second "hours" digit has a conditional max of 4 

                    switch (options.format[i]) {
                        case 'h':
                            __max.push((c % 2 == divisionRemainder) ? 2 : 3);
                            break;
                        case 'd':
                            __max.push(9);
                            break;
                        case 'm'://minutes will get same __max as seconds
                        case 's':
                            __max.push((c % 2 == divisionRemainder) ? 5 : 9);
                    }
                    ++c;
                }
                else {
                    elem = $('<div class="cntSeparator"/>').css({
                        float: 'left',
                        "line-height": options.digitHeight + "px"
                    })
                            .html(":");
                    //.text(options.startTime[i]);
                    where.append(elem);
                }
                // alert( "digits.length: " + digits.length );
            }
        }
		
	};

	var updateText = function()
	{
	    var digitsIndex = 0;
		for (var i = 0; i < options.startTime.length; i++)
		{
			if( options.startTime[i] >= 0 )
			{
				$(digits[digitsIndex]).text( options.startTime[i] );
				digitsIndex++;
			}
		}
	}
  
    // Set or get element margin
    var margin = function (elem, val) {
        var cntDigitObj = $("#cnt_" + elem);
        if (val !== undefined) {
            // var r = $(cntDigitObj).css({ 'background-position': "0 " + val + 'px' });
            var r = $(cntDigitObj).text(val);
            return r;
        }
        // var returnValue = parseInt($(cntDigitObj).css('background-position').replace('0% ', '').replace('0px ', '').replace('px', ''), 10);
        var returnValue = parseInt($(cntDigitObj).text());
        return returnValue;
    };

	this.startInterval = function()
	{
		interval = setInterval(moveStep(digits.length - 1), 1000);
	}
	this.stopInterval = function()
	{
		//console.log("stopInterval");
		stopIntervalFunc();
	}
	var stopIntervalFunc = function()
	{
		if(interval)
			clearInterval(interval);
	}
	
	//init
	$.extend(options, userOptions);
	var timeLeft = Math.round((toDateValue(options.startTime) - toDateValue(options.currentTime)) / 1000);

	if (options.showLabel !== 1) {
	    this.css({ height: options.digitHeight, overflow: 'hidden' });
	}
	parseDate();		
	createDigits(this);
	$('#countdown').css("float", "right");

	function toDateValue(value) {
	    if (typeof value == 'object') {
            return value
	    }
	    
	    if ($.trim(value) == '') {
	        return new Date();
	    }

	    var chunks = value.split(":");
	    var current = new Date();

	    var yyyy = current.getFullYear(),
            mm = current.getMonth(),
            dd = current.getDate(),
            HH = current.getHours(),
            MM = current.getMinutes(),
            ss = current.getSeconds();

	    if (chunks.length >= 1) {
	        dd += parseInt(chunks[0]);
	    }

	    if (chunks.length >= 2) {
	        HH += parseInt(chunks[1]);
	    }
	    if (chunks.length >= 3) {
	        MM += parseInt(chunks[2]);
	    }
	    if (chunks.length >= 4) {
	        ss += parseInt(chunks[3]);
	    }
	    
	    return new Date(yyyy, mm, dd, HH, MM, ss);
	}

	var moveStepCF = function()
	{
		timeLeft--;

		parseDate();
		updateText();

		if(timeLeft <= 0)
		{	
			stopIntervalFunc();
			options.timerEnd();
		}
	}

	interval = setInterval(moveStepCF, 1000);

	return this;
};

