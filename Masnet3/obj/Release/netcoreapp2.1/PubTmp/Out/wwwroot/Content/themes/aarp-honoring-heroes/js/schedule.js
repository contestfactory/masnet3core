$(function() {

    //create div
	var dateDiv = '<div id="current-date" class="current-date"></div>'
	

    //test date
    $("#testDateSlct").on('change', function () {
        updateDate(this.value);
    });

    var d;

    function updateDate(date){

    	d = date;

        //parse
        var month = date.split('/')[0];
        var day = date.split('/')[1];

        // console.log('month: ' + month);
        // console.log('day: ' + day);

        //detect phase
        var phase;
        var perc;
        if(month == 11){
            if(day > 25){ //judge voting
                divRemoveClass();
                $('#current-date').detach();
                $('.phase1 .phase-title').append(dateDiv);

                perc = (day - 25) / 5;
                // console.log('perc: ' + perc);

                $('#current-date').text('November ' + day).css('left', $('.phase1 .timeline').width() + 67.5 * perc);
            }
            else{//phase 1
                $('#current-date').detach();
                $('.phase1 .phase-title').append(dateDiv);
                $('#current-date').addClass('p1');

                perc = (day - 1) / 24;
                // console.log('perc: ' + perc);


                $('#current-date').text('November ' + day).css('left', $('.phase1 .timeline').width() * perc);
            }
        }
        else if( month == 12 ){
            if(day < 4){ //judge voting
                divRemoveClass();
                $('#current-date').detach();
                $('.phase1 .phase-title').append(dateDiv);

                perc = day / 4;
                // console.log('perc: ' + perc);

                $('#current-date').text('December ' + day).css('left', $('.phase1 .timeline').width() + 67.5 + 40.5 * perc);

            }else if(day <= 8){ //phase 2
                $('#current-date').detach();
                $('.phase2 .phase-title').append(dateDiv);
                $('#current-date').addClass('p2');

                perc = (day - 4) / 4;
                $('#current-date').text('December ' + day).css('left', $('.phase2 .timeline').width() * perc);
            }else if(day > 8 && day < 11){
                divRemoveClass();
                $('#current-date').detach();
                $('.phase.ended .phase-title').append(dateDiv);

                perc = (day - 8) / 2;
                // console.log('perc: ' + perc);

                $('#current-date').text('December ' + day).css('left', -43 + 80 * perc);
            }else if(day == 11){
                divRemoveClass();
                $('#current-date').detach();
                $("#arrow-final").show();
                $(".phase.ended").addClass('final');
            }
            else
            {
                divRemoveClass();
                $('#current-date').detach();
                $("#arrow-final").show();
                $(".phase.ended").addClass('final');
            }
        }
        else{
            divRemoveClass();
            $('#current-date').detach();
        }
    }
    function divRemoveClass(){
        $('#current-date').removeClass('p1 p2');
    }
    window.updateDate = updateDate;

    $( window ).resize(function() {
	  	updateDate(d);
	});

});