var gulp = require('gulp'),
    sass = require('gulp-sass'),
    imagemin    = require('gulp-imagemin'), // image lib
    pngquant    = require('imagemin-pngquant'); // png lib

gulp.task('img', function() {
    return gulp.src('image/**/*') // take all jpg, png images
        .pipe(imagemin({ // optimise images with best settings
            interlaced: true,
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('../image')); // export
});

gulp.task('sass', function() { // Create task "sass"
  return gulp.src(['sass/**/site.sass']) // take source files
    // .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError)) // convert sass into css using galp-sass (minified CSS)
    .pipe(sass({outputStyle: 'uncompressed'}).on('error', sass.logError)) // convert sass into css using galp-sass (unminified CSS)
    .pipe(gulp.dest('../')) // export in css folder
  });

gulp.task('watch', function() {
  gulp.watch(['sass/**/*.sass', 'image/**/*'], ['sass']); // Watch for modified files in sass folder
});

gulp.task('default', ['watch']);