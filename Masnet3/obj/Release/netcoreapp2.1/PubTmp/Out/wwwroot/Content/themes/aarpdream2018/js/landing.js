$(function() {

	function autoPlay(videoID){
	    videojs(videoID).ready(function () {
          var myPlayer = this;
          // EXAMPLE: Start playing the video.
          myPlayer.play();
        });
    }

	$('#videoModal1').on('shown.bs.modal', function (event) {
		resizeYoutubeIframe1();
		setTimeout(function () { autoPlay("self_serve_video1"); }, 500);
	});
	$('#videoModal2').on('shown.bs.modal', function (event) {
	    resizeYoutubeIframe2();
	    setTimeout(function () { autoPlay("self_serve_video2"); }, 500);
	});
	$('#videoModal3').on('shown.bs.modal', function (event) {
	    resizeYoutubeIframe3();
	    setTimeout(function () { autoPlay("self_serve_video3"); }, 500);
	});
	$('#videoModal4').on('shown.bs.modal', function (event) {
	    resizeYoutubeIframe4();
	    setTimeout(function () { autoPlay("self_serve_video4"); }, 500);
	});

	$('#videoModal1').on('hide.bs.modal', function (event) {
		videojs("self_serve_video1").ready(function(){
          var myPlayer = this;
          myPlayer.pause();
        });
	});
	$('#videoModal2').on('hide.bs.modal', function (event) {
	    videojs("self_serve_video2").ready(function () {
	        var myPlayer = this;
	        myPlayer.pause();
	    });
	});
	$('#videoModal3').on('hide.bs.modal', function (event) {
	    videojs("self_serve_video3").ready(function () {
	        var myPlayer = this;
	        myPlayer.pause();
	    });
	});
	$('#videoModal4').on('hide.bs.modal', function (event) {
	    videojs("self_serve_video4").ready(function () {
	        var myPlayer = this;
	        myPlayer.pause();
	    });
	});

	function resizeYoutubeIframe1()
	{
	    var w = $("#videoModal1 .modal-body").width();
	    $("#videoModal .video-iframe-container").css({ width: w, height: w / 1.77 });
	}
	function resizeYoutubeIframe2() {
	    var w = $("#videoModal2 .modal-body").width();
	    $("#videoModal2 .video-iframe-container").css({ width: w, height: w / 1.77 });
	}
	function resizeYoutubeIframe3() {
	    var w = $("#videoModal3 .modal-body").width();
	    $("#videoModal3 .video-iframe-container").css({ width: w, height: w / 1.77 });
	}
	function resizeYoutubeIframe4() {
	    var w = $("#videoModal4 .modal-body").width();
	    $("#videoModal4 .video-iframe-container").css({ width: w, height: w / 1.77 });
	}
});