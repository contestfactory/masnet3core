﻿function InitCustomRules(formID) {
    $.each($("[custom-rule]"), function (idx, item) {
        try {
            var ruleObj = $(item);
            var customRule = JSON.parse(ruleObj.val());
            var fieldName = ruleObj.attr("data-name");
            var fieldObj = $("#" + formID + " [name=" + fieldName + "]");
            if (fieldObj.size() > 0) {
                ApplyRule(fieldObj, customRule);
            }
        }
        catch (ex) {
            console.log(ex.message);
        }
    });
}
// apply rules
function ApplyRule(el, rules) {

    if (el.size() > 0) {
        var customRule = { messages: {} };        
        $.each(rules, function (idx, rule) {
            // remove hard code rules
            if (rule.RuleKey == "dateDiffYear") {
                el.rules("remove", "dateMinAge");
            }

            var value = rule.Value == null || rule.Value == "false" ? (rule.IsBuiltIn ? true : false) : rule.Value;
            customRule[rule.RuleKey] = value;
            customRule.messages[rule.RuleKey] = rule.ResourceMessage ? rule.ResourceMessage : "invalid input";
        });
        el.rules("add", customRule);
    }
    else {
        console.log(el.get(0).id + " does not existed!");
    }
}

// DisplayItem custom validation methods
if ($.validator) {
    $.validator.addMethod("NameValidate", function (value, element) {
        if (value != '') {

            if (!(value.substr(0, 1).match(/[A-Za-z]/) != null))
                return false;

            if (!(value.match(/^[a-zA-Z '\.-]+$/) != null))
                return false;
        }
        return true;
    });

    $.validator.addMethod("dateMax", function (value, element, maxDateValue) {
        return this.optional(element) || (Date.parse(value) <= maxDateValue);
    });
    $.validator.addMethod("dateMaxToday", function (value, element) {
        return this.optional(element) || (Date.parse(value) <= new Date());
    });

    $.validator.addMethod("dateMin", function (value, element, minDateValue) {
        return this.optional(element) || (Date.parse(value) >= minDateValue);
    });
    $.validator.addMethod("dateMinToday", function (value, element) {
        return this.optional(element) || (Date.parse(value) >= new Date());
    });
    $.validator.addMethod("dateDiffYear", function (value, element, year) {

        if (year != '0') {
            var ValidateDate = new Date();
            var minYear = ValidateDate.getFullYear() - year;
            ValidateDate.setFullYear(minYear);
            if (value != "") {
                var SelectedDate = new Date(value);
                if (SelectedDate > ValidateDate)
                    return false;
                else
                    return true
            }
            return true
        }
        return true;
    });
    $.validator.addMethod("htmlRequired", function (value, element) {
        htmlValue = CKEDITOR.instances[element.id].getData();
        var result = $.validator.methods.required.call(this, htmlValue, element);
        return result;
    });

}

