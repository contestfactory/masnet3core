$(function() {
	
	var SliderOrientationChanged = function() {
        $(".slideshow-container").css("display", "none");
        ScaleSlider();
    }

	//winners
    var prizes = {};
    prizes.grandPrize       = { name: "$10,000 CASH", img: "10k" };
    prizes.appleWatch       = {name: "APPLE WATCH", img: "apple-watch"};
    prizes.ipad             = {name: "IPAD MINI", img: "ipad"};
    prizes.wordpress        = {name: "WORDPRESS PACKAGE", img: "wordpress"};
    prizes.cruiser          = {name: "BEACH CRUISER", img: "cruiser"};
    prizes.gopro            = {name: "GOPRO CAMERA", img: "gopro"};
    prizes.storePackage     = {name: "ONLINE STORE PACKAGE", img: "store-package"};
    prizes.cpanel           = {name: "CPANEL PACKAGE", img: "cpanel"};
    prizes.websiteBuilder   = {name: "WEBSITE BUILDER PACKAGE", img: "website-builder"};


    /*
     * Add winners to Array
     * Without winner photo: { name: "Winner 134", p: prizes.websiteBuilder }
     * With winner photo:    { name: "Winner 133", p: prizes.grandPrize, photo: "winner1.jpg" }
     *
     */

     /*
     * Winner photo dimensions: 282 x 184
     */

    var winners = [
        { name: "Paul F.", p: prizes.grandPrize, photo: "Paul_F.jpg" }
        , { name: "Mike D.", p: prizes.wordpress, photo: "Mike_D.jpg" }
        , { name: "Jessica A.", p: prizes.storePackage, photo: "Jessica_A.jpg" }
        , { name: "Shelley M.", p: prizes.appleWatch, photo: "Shelley_M.jpg" }
        , { name: "John F.", p: prizes.storePackage, photo: "JohnF.jpg" }

    ];

    var drawAllWinnersHTML = function()
    {
        var i = 0;
        var total = winners.length;
        var slidesContainer = "#slides-container";
        var containerStart = '<div>';
        var containerEnd = '</div>';
        var cntStr = "";

        if(total > 0)
        {
            for(i; i < total; i++)
            {
                cntStr = drawWinnerHTML( winners[i].name, winners[i].p, winners[i].photo );
                $(slidesContainer).append( cntStr );
            }
        }                    
    }
    var drawWinnerHTML = function(name, prize, photo)
    {
        if( !photo )
            photo = "winner-" + prize.img + ".png";
        if( prize ===  prizes.grandPrize)
            return '<div class="col-xs-3 winner-container"><div class="winner-container-grandprize"><div><img src="image/' + photo +'" class="img-responsive" alt="' + name + '" /></div><div class="text-center"><p class="grand-prize-winner">GRAND PRIZE WINNER</p><p class="name">' + name + '</p><p class="prize">' + prize.name +'</p></div></div></div>';
        else
            return '<div class="col-xs-3 winner-container"><div><img src="image/' + photo +'" class="img-responsive" alt="' + name + '" /></div><div class="text-center"><p class="name">' + name + '</p><p class="prize">' + prize.name +'</p></div></div>';
    }

    // drawAllWinnersHTML();
    
    var aspect = 3.323688;
    var slideW, slideH;

    slideW = $("body").width();
    slideH = $("body").width() / aspect;


    $("#slider1_container").css({
    	width: slideW,
    	height: slideH
    })

    var options = { 
                    $AutoPlay: true,
                    $AutoPlayInterval: 3000,
                    $AutoPlaySteps: 1,
                    $PauseOnHover: 1,
                    // $SlideDuration: 500,
                    // $SlideWidth: 243, 
                    $SlideWidth: slideW,                                
                    $SlideHeight: slideH,                                
                    $SlideSpacing: 0,
                    $DisplayPieces: 1,
                    $SlideDuration: 300,
                    // $Loop: 0,    

                    $ThumbnailNavigatorOptions:{
                        $Class: "winner-container",
                        $ChanceToShow: 2,
                        /*$Loop: 0,*/
                        $Lanes: 1,
                        $AutoCenter: 0,
                        $DisplayPieces: 1,
                        $ParkingPosition: 0
                    },
                    $BulletNavigatorOptions: {                          //[Optional] Options to specify and enable navigator or not
                        $Class: $JssorBulletNavigator$,                 //[Required] Class to create navigator instance
                        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                        $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                        $SpacingX: 0,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                        $SpacingY: 0,                                 //[Optional] Vertical space between each item in pixel, default value is 0
                        $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    },
                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$,                  //[Requried] Class to create arrow navigator instance
                        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always                                    
                        $Steps: 1
                    }
                };
    var jssor_slider1;
    //responsive code begin
    //you can remove responsive code if you don't want the slider scales while window resizes
    function ScaleSlider() {
        // var parentWidth = $(".green-bg").width();
        // var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
        // var parentWidth = jssor_slider1.$Elmt.parentNode.clientWidth;
        var parentWidth = $("body").width();
        // alert("parentWidth: " + parentWidth);

        if (parentWidth)
        {
            jssor_slider1.$ScaleWidth(parentWidth);                
        }
        else
        {
            window.setTimeout(ScaleSlider, 30);
        }
        $(".slideshow-container").show();
        // updatedHeight();           
    }


    jssor_slider1 = new $JssorSlider$('slider1_container', options);        

    ScaleSlider();

    $(window).bind("load", ScaleSlider);
    $(window).bind("resize", ScaleSlider);
    $(window).bind("orientationchange", SliderOrientationChanged);

    var isPositionChanging;
    var quoteIndex;
    function OnSlidePark(slideIndex, fromIndex) {
        console.log("OnSlidePark: " + slideIndex);
        isPositionChanging = false;
        if( slideIndex % 2 == 0 )
            quoteIndex = 1;
        else
            quoteIndex = 2;
        $("#quote" + quoteIndex).show();

    }    
    function OnPositionChange(position, fromPosition) {
        if( !isPositionChanging ){
            isPositionChanging = true;
            console.log("OnPositionChange: fromPosition - " + fromPosition);
        }
    }

    // jssor_slider1.$On($JssorSlider$.$EVT_PARK, OnSlidePark);

    // jssor_slider1.$On($JssorSlider$.$EVT_SLIDESHOW_START, OnSlideShowStart);
    // jssor_slider1.$On($JssorSlider$.$EVT_POSITION_CHANGE, OnPositionChange);
});