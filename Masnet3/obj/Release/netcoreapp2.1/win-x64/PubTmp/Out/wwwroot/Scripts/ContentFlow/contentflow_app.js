﻿
function msieversion() {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    if (msie > 0)      // If Internet Explorer, return version number
        return (parseInt(ua.substring(msie + 5, ua.indexOf(".", msie))));
    else                 // If another browser, return 0
        return 0

    return 0;
}

function preLoadImage(container,callback) {
    container.find('img').load(function () {
        $(this).attr('data-isloaded', '1');

        preLoadContestantImage(container, callback);
    })
}

function preLoadContestantImage(container, callback) {
    //$("#itemcontainer")
    var tmpItems = container;

    if (tmpItems.find("img[data-isloaded]").length == tmpItems.find("img").length) {

        if (callback != null)
            callback();
    }
}


function InitContentFlow(configs) {

    if (configs.container == null)
        configs.container = $("#slidercontainer");
    if (configs.contentFlowId == null)
        configs.contentFlowId = "ajax_cf";


    var html = "";
    html = '<div class="ContentFlow" id="' + configs.contentFlowId + '">                                                 ' +
           '    <div class="loadIndicator"><div class="indicator"></div></div>                                           ' +
           '    <div class="flow" >                                                                                      ' +
           '        <div class="item" href="#" data-loader="1">                                                          ' +
           '            <div class="content" style=""><div class="ajaxloading"></div></div>                              ' +
           '            <div class="caption"></div>                                                                      ' +
           '            <div class="label"></div>                                                                        ' +
           '        </div>                                                                                               ' +
           '    </div>                                                                                                   ' +
           '    <div class="globalCaption"></div>                                                                        ' +           
           '                                                                                                             ' +
           '    <div class="scrollbar next-prev-container" style="display:none">                                         ' +
           '        <div class="preButton" style="display:inline-block"><i class="icon-chevron-circle-left"></i></div>   ' +
           '        <div class="nextButton" style="display:inline-block"><i class="icon-chevron-circle-right"></i></div> ' +
           '    </div>                                                                                                   ' +
           '</div>                                                                                                       ';

    $(configs.container).html(html);

    var contentFlow = new ContentFlow(configs.contentFlowId, {
        circularFlow: false,
        loadingTimeout: 30000,
        flowDragFriction: 0,
        reflectionHeight: (msieversion() == 0 ? 0.5 : 0),
        fixItemSize: false,
        relativeItemPosition: "top",
        onMoveTo: function (obj) {
            if (!!configs.onMoveTo)
                configs.onMoveTo(obj);
        }
    });

    contentFlow.init();

    $("#" + configs.contentFlowId).swipe({
        swipe: configs.onSwipe,
        threshold: 60
    });

    return contentFlow;
}
