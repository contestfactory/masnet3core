﻿
$(document).ready(function () {
    $.ajaxSetup({ cache: false });

    if ($("#Topmenu li").length > 0)
        $("#Topmenu").show()

    if (isInPublishContestPreview) {
        initIconResourceChange(this);
    }

    if ($("#ddlControlTenants").length > 0) {
        $.ajax({
            type: "POST",
            url: urlGetJSONTenants,
            dataType: "JSON",
            success: function (msg) {
                $(msg).each(function (i) {
                    $("<option value='" + this.TenantID + "'>" + this.Name + "</option>")
                        .appendTo("#ddlControlTenants");
                })
            }
        });

        $("#ddlControlTenants").change(function () {
            if ($(this).val() == 0 || $(this).val() == "") {
                $("#ddlControlApps option").remove();
                $("#spControlApps").hide();
                ddlTenantAndAppCallback();
            }
            else {
                LoadApps($(this).val());
            }
        }).trigger("change");
    }

    $("#ddlControlApps").change(function () {
        ddlTenantAndAppCallback();
    })

    if (isTenant) {
        if ($("#ddlControlApps").length > 0) {
            LoadApps(currentUserTenantID);
        }
    }

    ShowRequireLogin();
});

//Jquery custorm function
$.validator.addMethod(
    "regex",
    function (value, element, regexp) {
        var re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "Please check your input."
);


$.validator.addMethod(
    "safe",
    function (value, element, regexp) {
        var re = new RegExp("^[^<>\'\"\\/]+$");
        return this.optional(element) || re.test(value);
    }
);

jQuery.validator.addClassRules('safe', {
    safe: true
});

function initIconResourceChange(id) {
    var resourceId = $(id).attr("resource-id");
    if (resourceId != "" && parseInt(resourceId) > 0) {
        var tagName = $(id).prop("tagName").toLowerCase();
        var resourceValue = "";
        switch (tagName) {
            case "input":
                resourceValue = $(id).val();
                break;
            default:
                resourceValue = $(id).text();
                break;
        }

        var html = "<div class='edit-resource-text' resource-value='" + resourceValue + "' resource-id-edit='" + resourceId + "'></div>";
        $(id).after(html);
        clickIconResourceChange("[resource-id-edit=" + resourceId + "]");
    }
}

function clickIconResourceChange(id) {
    $(id).click(function () {
        var resourceId = $(id).attr("resource-id-edit");
        var resourceValue = $(id).attr("resource-value").trim();

        $.ajax({
            type: "GET",
            url: urlModifyResource,
            data: { id: gCurrentPublishedContestID, resourceID: resourceId, value: resourceValue },
            success: function (data) {
                $(".ui-dialog-content").dialog("close");

                $("#dvModifyResource").html(data);
                $("#dvModifyResource").dialog();
                $("#dvModifyResource").dialog("open");
                $("#txtModifyResourceValue").focus();
                initChangeResourceValueDialog();
            }
        });
    });
}

function initChangeResourceValueDialog() {
    $("#btnModifyResourceCancel").click(function () {
        $("#dvModifyResource").dialog("close");
    });

    $("#btnModifyResourceSave").click(function () {
        var id = $("#hdfModifyResourceID").val();
        var value = $("#txtModifyResourceValue").val();

        $.ajax({
            type: "POST",
            url: urlSaveResource + "/" + $("#hdfModifyResourceContestID").val(),
            data: { resourceID: id, value: value, langcode: $("#hdfLanguageCode").val() },
            success: function (data) {
                if (data != null) {
                    var tagName = $("[resource-id=" + id + "]").prop("tagName").toLowerCase();
                    var resourceValue = "";
                    switch (tagName) {
                        case "input":
                            $("[resource-id=" + id + "]").val(value);
                            break;
                        default:
                            $("[resource-id=" + id + "]").text(value);
                            break;
                    }

                    $("[resource-id-edit=" + id + "]").attr("resource-value", value);

                    if (data != id) {
                        $("[resource-id-edit=" + id + "]").unbind("click");

                        $("[resource-id=" + id + "]").attr("resource-id", data);
                        $("[resource-id-edit=" + id + "]").attr("resource-id-edit", data);

                        clickIconResourceChange("[resource-id-edit=" + data + "]");
                    }

                    $("#dvModifyResource").dialog("close");
                }
            }
        });
    });

    $("#dvModifyResource").bind("dialogclose", function (event, ui) {
        $("#dvModifyResource").html("");
    });
}

function ShowMediaPopup(obj, playerid, param) {
    if (!playerid)
        playerid = "media_player";

    if (param != null) {
        param.playerid = playerid;
    }
    else {
        param = {
            mediaid: obj.getAttribute("media-mediaid"),
            thumbnail: obj.getAttribute("media-thumbnail"),
            filepath: obj.getAttribute("media-filepath"),
            filetype: obj.getAttribute("media-filetype"),
            playerid: playerid
        };
    }

    if ($("#" + playerid).length == 0)
        $("body").append("<div id='" + playerid + "' style='display:none'></div>");

    ShowMedia(param);

    $("#" + playerid).dialog({
        modal: true,
        width: 565,
        height: 385
    });

    $("#" + playerid).dialog("open");
}

function ShowMedia(param, obj) {
    /*
    param = {                
        mediaid
        thumbnail
        filepath
        filetype       
        playerid
    }
    */
    var w = 500;
    var url = "";
    var playerid = "";
    playerid = param.playerid == null ? "media_player" : param.playerid;

    if (obj) {
        $(obj).parent().find("img").css("border", "0px solid #ccc");
        $(obj).css("border", "2px solid #ccc");
    }


    if (param.filetype == '5') {
        if (param.filepath == "") {
            w = 900;
            $("#" + playerid).html($("#dvDesc_" + param.mediaid).html());
        }
        else {
            var downloadpath = "<%= FAQFolder %>/document/" + param.filepath;
            var html = "<table width='100%' style='height:100px'><tr><td align='center' valign='middle'><a href='#' onclick='downloadFile(\"" + downloadpath + "\");return false;'><img src='" + $("#imgIcon_" + faqvideoid).attr("src") + "' border='0' style='margin-bottom:-3px'></a>&nbsp;<a href='#' onclick='downloadFile(\"" + downloadpath + "\");return false;'>Download</a></td></tr></table>";

            $("#" + playerid).html(html);
        }
    }
    else if (param.filetype == '1') {
        if (param.thumbnail.indexOf("http://") == -1)
            url = gVirtualUploadPath + "showimage.aspx?img=/" + SiteConfig.UploadFolder + "/" + param.filepath + "&w=538&h=329";
        else
            url = param.thumbnail;
        $("#" + playerid).html("<img src='" + url + "' border='0' style='max-width:538px'>");
    }
    else {
        PlayMedia(param.filetype, param.filepath, param.thumbnail, playerid);
    }
}

function ShowIPhoneVideo(url, width, height, photo, controlName, fileType) {

    if (url.toLowerCase().indexOf("youtube") == -1) {
        if (fileType == "video") {
            var webmUrl = url.substring(0, url.lastIndexOf(".")) + ".webm";
            url = url.substring(0, url.lastIndexOf(".")) + ".mp4";
            var html5 = "<video width='" + width + "'  height='" + height + "' controls poster='" + photo + "'>"
            html5 = html5 + "<source src='" + url + "' type='video/mp4'  />"
            html5 = html5 + "<source src='" + webmUrl + "' type='video/webm'  />"
            html5 = html5 + "Your browser Your browser does not support the video tag.</video>"
        } else {
            var mp3Url = url.substring(0, url.lastIndexOf(".")) + ".mp3";
            url = url.substring(0, url.lastIndexOf(".")) + ".ogg";
            var html5 = "<audio controls>"
            html5 = html5 + "<source src='" + url + "' type='audio/ogg'  />"
            html5 = html5 + "<source src='" + mp3Url + "' type='audio/mpeg'  />"
            html5 = html5 + "Your browser Your browser does not support the video tag.</video>"
        }

        $("#" + controlName).html(html5)
    } else {

        var strHtml = "<object width='" + width + "'  height='" + height + "' >";
        strHtml = strHtml + "<param name='movie' value='" + url + "'></param>";
        strHtml = strHtml + "<param name='allowFullScreen' value='true'></param>";
        strHtml = strHtml + "<param name='allowscriptaccess' value='always'></param>";
        strHtml = strHtml + "<embed src='" + url + "' type='application/x-shockwave-flash'  allowscriptaccess='always' allowfullscreen='true' width='" + width + "'  height='" + height + "'></embed>";
        strHtml = strHtml + "</object>"
        $("#" + controlName).html(strHtml);
    }
}

function ImageURL(dThumnail, dWidth, dHeight) {
    if (!dWidth)
        dWidth = 70;
    if (!dHeight)
        dHeight = 70;

    if (dThumnail.toLowerCase().indexOf("http") == 0) {
        return dThumnail;
    }
    else {
        var src = gVirtualUploadPath
                      + "/ShowImage3.aspx?img=upload\\" + dThumnail
                      + "&w=" + dWidth
                      + "&h=" + dHeight;
        return src;
    }
}

function showLogonDialog() {
    if ($("#login_form").length == 0) {
        $("#login_dialog").load(urlLogin);
    }
    else {
        $("#UsernameLogin, #PasswordLogin").val('');
        $(".field-validation-error").attr("style", "display:none");
        $("#UsernameLogin, #PasswordLogin").removeClass("input-validation-error");
    }
    $("#login_dialog").dialog({
        autoOpen: false,
        height: 250,
        width: 350,
        modal: true,
        close: function (e, ui) {
            $("#login_dialog").dialog("close");
        }
    });
    $("#login_dialog").dialog("open");
}

function ActionShare(dActionID) {
    var link = urlShare;
    link = link.replace("&amp;", "&");
    link = link.replace("actionidholder", dActionID);
    link = link.replace("sharedurlholder", GSharing.CurrentSharedUrl);

    var w = 800;
    var h = 400;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);

    window.open(link, "", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
}

function ShowShareByEmail(dActionID) {
    $("#dvShareByEmail").dialog({
        modal: true,
        width: 700,
        height: 400,
        autoOpen: false,
        position: "center"
    }).dialog("open");

    $("#dvSharing").hide();

    $("#dvShareByEmail #btnSend").unbind("click").click(function () {
        if ($("#formShareByEmail").valid()) {

            var isvalid = true;
            var sendtolist = {};
            var hasfriend = false;
            $(".friendsemail > div").each(function (i) {
                var index = i;
                var fname = $.trim($("#txtFriendName" + index, $(this)).val());
                var femail = $.trim($("#txtFriendEmail" + index, $(this)).val());
                if (fname != "" || femail != "") {
                    if (fname != "" && femail == "") {
                        isvalid = false;
                        alert(gRequireFriendEmail + index);
                        $("#txtFriendEmail" + index, $(this)).focus();
                        return false;
                    }

                    if (fname == "" && femail != "") {
                        isvalid = false;
                        alert(gRequireFriendName + index);
                        $("#txtFriendName" + index, $(this)).focus();
                        return false;
                    }
                    hasfriend = true;
                    sendtolist[i] = { FriendName: fname, FriendEmail: femail };
                }
            })

            if (!hasfriend && isvalid) {
                alert(gRequireAtLeastFriend);
                return false;
            }

            if (isvalid) {

                var link = UrlAction(urlShareByEmail,
                                    {
                                        "actionidholder": 1,
                                        "sharedurlholder": GSharing.CurrentSharedUrl,
                                        "fromemailholder": $("#txtYourEmail").val()
                                    });

                $.ajax({
                    type: "POST",
                    url: link,
                    data: sendtolist,
                    success: function (msg) {
                        if (msg == "OK") {
                            alert(gSentSuccessfully);
                            $(".friendsemail input").val("");
                            $("#dvShareByEmail").dialog("close");

                        }
                    }
                });

            }
        }
    })

    $("#dvShareByEmail #btnClose").unbind("click").click(function () {
        $("#dvShareByEmail").dialog("close");
    })
}

function changeResourceKey(resourcekey, contestId) {
}

function postCommentByType(event, dControl) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == 13) {
        var ctl = $(dControl);
        if ($.trim(ctl.val()) != "") {

            var postedData = {
                ReferID: ctl.attr("param_referid"),
                CommentType: ctl.attr("param_commenttype"),
                Comment: ctl.val(),
                Container: ctl.attr("param_container")
            };

            ctl.val("");

            $.ajax({
                type: "POST",
                url: urlPostComment,
                data: postedData,
                success: function (msg) {
                    if (msg == "OK") {
                        onCommentPageChange(postedData.ReferID, postedData.CommentType, 1, postedData.Container, null);
                    }
                }
            });

        }
    }
}

function onCommentPageChange(dReferID, dCommentType, dPage, dContainer, dControl) {
    if (dControl)
        $(dControl).parent().remove();

    var configsection = $("#dvComments_" + dContainer + "_Config");

    var url = UrlAction(urlGetListComment,
                            {
                                "ReferIDHolder": dReferID,
                                "CommentTypeHolder": dCommentType,
                                "pageHolder": dPage
                            });
    $.ajax({
        type: "POST",
        url: url,
        data: {
            Container: dContainer,
            CommentWidth: $("input[name='CommentWidth']", configsection).val()
        },
        success: function (msg) {
            if (dPage == 1) {
                $("#dvComments_" + dContainer)
                .html(msg);
            }
            else {
                $("#dvComments_" + dContainer)
                .append(msg);
            }

        }
    });
}

function ddlTenantAndAppCallback() {
    var tenantid = 0;

    if (isTenant) {
        tenantid = currentUserTenantID;
    }
    else if (isSuperAdmin) {
        tenantid = $("#ddlControlTenants").val();
    }

    var appid = $("#ddlControlApps").val();
    if (appid == null)
        if (tenantid == 0)
            appid = "0";
        else
            appid = "-1";

    eval($("#hdControlOnchangeCallback").val() + "(" + tenantid + "," + appid + ");");
}

function LoadApps(dTenantId) {
    $("#ddlControlApps option").remove();

    var url = UrlAction(urlGetJSONApplications,
                        {
                            TenantIdHolder: dTenantId
                        });

    $.ajax({
        type: "POST",
        url: url,
        dataType: "JSON",
        success: function (msg) {
            $(msg).each(function (i) {
                $("<option value='" + this.ApplicationID + "'>" + this.ApplicationName + "</option>")
                    .appendTo("#ddlControlApps");
            })

            if ($("#ddlControlTenants").length > 0) {
                if ($("#ddlControlTenants").val() == 0 || $("#ddlControlTenants").val() == "")
                    $("#spControlApps").hide();
                else
                    $("#spControlApps").show();
            }
            else
                $("#spControlApps").show();

            ddlTenantAndAppCallback();
        }
    });
}

function SwitchUser(token) {
    $.ajax({
        type: "POST",
        url: UrlAction(urlLoginByToken, { token_holder: token }),
        success: function (msg) {
            if (msg != '') {
                window.location.href = msg;
            }
        }
    });
}

function changeLanguage(langCode) {
    var currentUrl = window.location.href;
    if (currentUrl.toLowerCase().indexOf("&lang") > 0) {
        window.location.href = currentUrl.replace(/&?lang=([^&]$|[^&]*)/i, "&lang=" + langCode)
    } else if (currentUrl.toLowerCase().indexOf("?lang") > 0)
        window.location.href = currentUrl.replace(/&?lang=([^&]$|[^&]*)/i, "lang=" + langCode)
    else {
        if (currentUrl.toLowerCase().indexOf("?") > 0)
            window.location.href = currentUrl + "&lang=" + langCode
        else
            window.location.href = currentUrl + "?lang=" + langCode
    }

}

function SpecifySharing(pLeft) {
    if ((pLeft + $("#dvSharing").width()) > $("#body > .content-wrapper").width()) {
        var dLeft = (pLeft - $("#dvSharing").width() + 70);
        return dLeft;
    }
    return pLeft;
}

function InitSharing(dTop, dLeft) {

    if (hasinitsharing)
        return;

    hasinitsharing = true;

    $("#loadingindicator")
        .css({ top: dTop + "px", left: dLeft + "px" })
        .show();

    var link = urlGetShareList;

    $.ajax({
        type: "GET",
        url: link,
        success: function (msg) {
            $($("body").get(0)).append(msg);
            $("#loadingindicator").hide();

            $("#dvSharing").css({ top: dTop + "px", left: SpecifySharing(dLeft) + "px" })
                   .show(200);


            $("#dvSharing").hover(function () {
                $(this).show();
            }, function () {
                $(this).hide();
            })


            $("#formShareByEmail").validate();

            var settings = $.data($("#formShareByEmail").get(0), 'validator').settings;
            settings.onfocusout = false;
            settings.onkeyup = false;
            settings.errorClass = "input-validation-error";
            settings.errorPlacement = function (error, element) {
                if ($(error).text() != "") {
                    alert($(error).text());
                    $(element).focus();
                }
            };
            settings.invalidHandler = function (event, validator) { };
            settings.success = function (label) { }
            settings.rules = {
                txtYourEmail: {
                    required: true,
                    email: true
                },
                txtFriendEmail1: { email: true },
                txtFriendEmail2: { email: true },
                txtFriendEmail3: { email: true },
                txtFriendEmail4: { email: true },
                txtFriendEmail5: { email: true }
            };
            settings.messages = {
                txtYourEmail: {
                    required: gRequireYourEmail
                }
            }

        }
    });
}

function PlayMedia(filetype, url, thumbnail, playerid) {
    var isIPhoneDetect = (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i) || (navigator.userAgent.match(/iPad/i)));

    var uAdultContent = "0";
    var flvType;
    var flv;

    var photo = "";
    var flvUrl = "";
    gVirtualUploadPath = gVirtualUploadPath + SiteConfig.UploadFolder + "/";

    if (thumbnail.indexOf("http://") == -1)
        photo = gVirtualUploadPath + thumbnail;
    else
        photo = thumbnail;

    if (url.indexOf("http://") == -1)
        flvUrl = gVirtualUploadPath + url;
    else
        flvUrl = url;

    if (filetype == '2') {	//audio
        photo = urlPhotoBackground;
        flvType = "audio";
    }
    else {
        flvType = "video";
    }

    if (!isIPhoneDetect) {
        var so = new SWFObject(urlFlvPlayer, "single", "538", "329", "9");
        so.addParam("allowfullscreen", "true");
        so.addParam("wmode", "transparent");
        so.addVariable("siteURL", urlsiteURL);
        so.addVariable("flvURL", flvUrl);
        so.addVariable("flvType", flvType);
        so.addVariable("adultContent", uAdultContent);
        so.addVariable("cName", "ContestFactory");
        so.addVariable("cPhotoUrl", photo);
        so.addVariable("cUrl", "");

        so.write(playerid);
    }
    else {
        ShowIPhoneVideo(flvUrl, '538', '329', photo, playerid, flvType);
    }
}


function drawMyFolder(config){        

    /*
    config:
        UserID,
        FileType,
        Page,        
        PageSize,
        Container,
        FileContainer,
        FileRenderOnSuccess,
        SearchControl,
        Callback
    */

    if (config.UserID == null)
        config.UserID = 0;

    var Container = null;
    if(typeof config.Container === "string")
        Container = $("#"+config.Container);
    else
        Container = $(config.Container);

    $.ajax({
        type: "POST",
        url: urlMyFolders,    
        data : {
            FileType: config.FileType,
            UserID : config.UserID,
            SearchControl: config.SearchControl,
            MyFolderContainer: config.Container,
            MyFilesContainer : config.FileContainer,
            FileRenderOnSuccess : config.FileRenderOnSuccess,
            page:1, 
            pageSize:config.PageSize
        },
        success: function (msg) {
            Container.html(msg);
            if(config.Callback){
                config.Callback();
            }
        }
    });        
}

function populateFolder(obj){
    $(".folder",$(obj).parent()).removeClass("selected");
    $(obj).addClass('selected');

    $(".folder img.openedfolder",$(obj).parent()).hide();
    $(".folder img.closedfolder",$(obj).parent()).show();

    $("img.closedfolder",$(obj)).hide();
    $("img.openedfolder",$(obj)).show();
        
}
    
function drawMyFiles(config){
    /*
    config:
        UserID,
        FolderID,
        FileType,
        Container,
        FileRenderOnSuccess,
        SearchControl
    */

    var Container = null;
    if(typeof config.Container === "string")
        Container = $("#"+config.Container);
    else
        Container = $(config.Container);


    if (config.UserID == null)
        config.UserID = 0;


    var search = "";
    if(config.SearchControl != ""){
        search = $("#"+config.SearchControl).val();
    }

    $.ajax({
        type: "POST",
        url: urlMyFiles,
        data : {                      
            fileType: config.FileType,
            UserID: config.UserID,
            FolderID : config.FolderID,
            FileRenderOnSuccess : config.FileRenderOnSuccess,
            search : search,
            page:1, 
            pageSize:8
            },
        success: function (msg) {
            Container.html(msg);
            if(config.FileRenderOnSuccess != ""){
                eval(config.FileRenderOnSuccess + "(" + config.FolderID + ")");
            }
        }
});        
}

function AddFolder(ddl,callback){
    if($("#dvAddFolder").length == 0){
        $("<div id='dvAddFolder'></div>").appendTo($("body").first());
        $("<div>Folder Name: <input type='text' id='txtAddFolder' maxlength='500'> <input id='btnAddFolder' type='button' value='Add'> <input id='btnCloseAddFolder' type='button' value='Close'></div>")
            .appendTo("#dvAddFolder");
    }

    $("#txtAddFolder").val("");
    $("#btnAddFolder")
        .unbind()
        .click(function(){
            if($("#txtAddFolder").val().trim() == ""){
                alert("Please enter folder name");
                return;
            }

            $.ajax({
                type: "POST",
                url: urlAddFolder,
                data : {                      
                    foldername : $("#txtAddFolder").val()
                },
                dataType: "Json",
                success: function (obj) {
                    $("#"+ddl + " option[selected]").removeAttr('selected');
                    $("<option value='" + obj.FolderID + "' selected='selected'>" + obj.FolderName + "</option>")
                        .appendTo("#"+ddl);

                    $("#dvAddFolder").dialog("close");

                    if(callback)
                        callback();
                    else {
                        if (window.RefreshAddFolder)
                            RefreshAddFolder();
                    }

                }
            });
        });

    $("#btnCloseAddFolder")
        .unbind()
        .click(function(){
            $("#dvAddFolder").dialog("close");        
        });

    $("#dvAddFolder").dialog({
        modal : true,
        width: 400,
        height : 150,
        title : "Add Folder"
    });

    $("#dvAddFolder").dialog("open");
}

function ShowRequireLogin() {

    if ($("#dvRequireLogin").length == 0) {
        $("<div id='dvRequireLogin' style='display:none;position:absolute;background-color:#fff;padding:30px 5px;border:1px solid gray'></div>").appendTo($("body").first());
        $("<div>Please <a href='" + urlContestLogin + "' style='font-weight:bold'>Login</a> or <a href='" + urlContestRegister + "' style='font-weight:bold'>Register</a> to continue.</div>")
            .appendTo("#dvRequireLogin");
    }

    $(".requirelogin")
        .unbind()
        .hover(
            function () {
                var os = $(this).offset();
                $("#dvRequireLogin").css({
                    top: (os.top + $(this).outerHeight() - 5) + 'px',
                    left: (os.left + ($(this).outerWidth() / 2) - ($("#dvRequireLogin").width()/2)) + 'px',
                }).show();
            }, function () {
                $("#dvRequireLogin").hide();
            })

    $("#dvRequireLogin")
        .unbind()
        .hover(
            function(){ $(this).show() },
            function () { $(this).hide() }
        )
    
}