$( document ).ready(function() {
	// Handler for .ready() called.
    $("#btn-login").click(function(){
        $("#login_dialog").toggle();
    });  
});


$(window).load(function()
{
    // $("body").css("background", "#fff");
    $("body").removeClass("loading");
    $("body, html").css("height", "auto");
    $(".page-wrapper, footer").show();
    
    resize();

    window.onscroll = function(){
        var scroll = window.scrollY;

        /*if(scroll < 0)
        {
          // $("footer img").css("opacity", 0);
        	$("footer img").css("display", "none");
        }
        else
        {
        	// $("footer img").css("opacity", 1);
          $("footer img").css("display", "block");
        }*/
    }
});
$( window ).resize(function() {
    resize();
});
var scale;
function resize()
{
  //$('.page-wrapper').css( 'margin-bottom', $('footer').height() );
  // alert( $('footer').height() );

  scale = $(window).width() / 1920;
  scale = ( scale > 1 ) ? 1 : scale ;
  if(scale > 0.3)
  {
  	$('.logo-header-container').css(
  		{
  			'width': 190 * scale,
  			'margin-left': - 190 / 2 * scale
  		}
  	);
  }	
  else
	$('.logo-header-container').css(
  		{
  			'width': 190 * 0.3,
  			'margin-left': - 190 / 2 * 0.3
  		}
  	);
	//makeSameHeight(".entry-description");

}
