﻿/*
** Just check and show vote form
*/
function CheckVoteForMedia(configs){
    /*
    configs:
    1. Button (JQuery)
    2. VoteFormContainer (JQuery) 
    4. onButtonClick : when vote form is ready
    5: SliderWidth    
    */

    var btn = configs.Button;
    var voteform = configs.VoteFormContainer;

    $.ajax({
        type: "POST",
        url: SiteURL.UploadFile_MediaVoteForm,
        data: {
            MediaID: btn.attr('data-mediaid'),
            VoteID : btn.attr('data-voteid'),
            SliderWidth: configs.SliderWidth            
        },
    success: function (msg) {
        voteform.hide();
        voteform.html(msg);

        var votetype = voteform.find('.formconfig > .VoteType').val();
        var voteable = voteform.find('.formconfig > .Voteable').val();
        var hasvoted = voteform.find('.formconfig > .Hasvoted').val();        

        if(voteable == '1'){            
            if (votetype == '0') {
                btn.show();
                voteform.hide();
                btn.unbind()
                   .click(function(){
                       voteform.find('form').submit();
                   })
            }
            else if (votetype == '1') {
                btn.hide();
                voteform.show();
                //btn.unbind()
                //   .click(function () {
                //       voteform.find('form').submit();
                //   })
            }
            else {
                //btn.show();
                //btn.unbind()
                //   .click(function(){
                       
                //   })

                voteform.show();
                InitMediaVoteSlider(voteform);
            }
        }
        else{
            btn.hide();
            voteform.show();
        }

        if (configs.onButtonClick != null) {
            configs.onButtonClick();
        }
    }
});
}

function InitMediaVoteSlider(container) {
    container.find(".votecriteria").each(function (i) {

        var slider = this;
        var dvVoteCriteria = $(slider).closest('.dvVoteCriteria');
        var curVal = dvVoteCriteria.find('.hdScore').val();

        $(slider).slider({
            value: curVal,
            min: parseInt($(slider).attr("slider-min")),
            max: parseInt($(slider).attr("slider-max")),
            step: 1,
            create: function (event, ui) {
                $(slider).slider().find("a")
                    .css({ "text-align": "center", "text-decoration": "none" })
                    .html($(slider).slider("value"));
            },
            slide: function (event, ui) {
                $(ui.handle).html(ui.value);
                $(ui.handle).parents(".dvVoteCriteria")
                            .find("#hdScore_" + $(ui.handle).parent().attr("id") + " > input:hidden")
                            .val(ui.value);

            }
        })
    });
}