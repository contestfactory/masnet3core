﻿

function initUploadFile(){

    $(".cmdbutton").parent().hide();
    $("#lnkUploadFile").click(function() {                
        $("#SubmitType").val("uploadfile");
        $(".tabmediaupload").removeClass("active");
        $(this).parent().addClass("active");
        var filetype = SelectedMediaType();

        ShowUploadFile(filetype, function(obj) {                                           
            AddMedia(SelectedMediaType(), obj.MediaID);                   
        }, 'dvUploadMediaFile');

        return false;
    })

    $("#lnkUploadYoutube").click(function() {
        $("#SubmitType").val("youtube")
        $("#txtVideoUrl").removeClass("input-validation-error")
        $(".field-validation-error").html("")
        $(".tabmediaupload").removeClass("active");
        $(this).parent().addClass("active");
        $("#dvYoutubeLink .upload-control-label").hide();
        $("#dvYoutubeLink #label-youtube-link").show();
        $("#VideoType").val("1");                
        ShowYoutube(function(obj) {
            AddMedia(SelectedMediaType(), obj.MediaID);                    
        }, 'dvUploadMediaUrl');

        return false;
    })

    $("#lnkUploadVimeo").click(function() {
        $("#SubmitType").val("vimeo")
        $("#txtVideoUrl").removeClass("input-validation-error")
        $(".field-validation-error").html("")
        $(".tabmediaupload").removeClass("active");
        $(this).parent().addClass("active");
        $("#dvYoutubeLink .upload-control-label").hide();
        $("#dvYoutubeLink #label-vimeo-link").show();
        $("#VideoType").val("2");
        ShowYoutube(function(obj) {
            AddMedia(SelectedMediaType(), obj.MediaID);                    
        }, 'dvUploadMediaUrl');

        return false;
    })

    //LoadCkEditorScript();
    //loadJS(SiteContent.CKEditor);
    setTimeout(function () { LoadCkEditorScript(); }, 300);

    $("#lnkUploadMessage").click(function() {
        $("#SubmitType").val("uploadmessage");
                
        //if (CKEDITOR) {
        //    if (CKEDITOR.instances.txtMessage)
        //        CKEDITOR.instances.txtMessage.destroy();

        //    CKEDITOR.replace('txtMessage', { language: "en" });
        //}
        $(".tabmediaupload").removeClass("active");
        $(this).parent().addClass("active");

        ShowTextMessage(function(obj) {
            AddMedia(SelectedMediaType(), obj.MediaID);
            //$("#dvUploadMedia").dialog("close");
        }, 'dvUploadMedia');

        return false;
    })

    $('[name="optUploadMedia"]').on('change', function(){
        $('.field-validation-error').empty();
        $('.input-validation-error').removeClass('input-validation-error');                
    });

    $("#lnkUploadFile, #lnkUploadYoutube,#lnkUploadMessage").trigger('click');       

    $.validator.addMethod("youtubeurl", function(value, element) {
        $.validator.messages.youtubeurl = SubmitMsg.Submit_InvalidYoutube;

        var txt = $(element).val();
        txt = txt.toLowerCase();
        if (txt.indexOf("vimeo") > -1)
        {
            $("#VideoType").val("2");
        }

        if ($("#VideoType").val() == "2") {
            return true;
        } else if (ContestSetting.Photo_IsAllowUrl == "True" || ContestSetting.Audio_IsAllowUrl == "True") {
            $.validator.messages.youtubeurl = SubmitMsg.Submit_InvalidMediaUrl;
            if($('#txtVideoUrl').size() > 0){
                $('#txtVideoUrl').val(wrapURL($('#txtVideoUrl').val()));
            }
            return  (ContestRules && ContestRules.PhotoMin == 0) || isURL(wrapURL($('#txtVideoUrl').val()));
        }

        var isOldTypeValid = value.toLowerCase().indexOf("youtube.com") > -1;
        var isNewTypeValid = value.toLowerCase().indexOf("youtu.be") > -1;

        if (value == null || (isOldTypeValid || isNewTypeValid) == false) {
            $.validator.messages.youtubeurl = SubmitMsg.Submit_InvalidYoutubeVimeo;
            return false;
        }               

        return true;
    });
    $.validator.addMethod("vimeo", function(value, element) {
        $.validator.messages.vimeo = SubmitMsg.Submit_InvalidMediaUrl;

        if ($("#VideoType").val() != "2")
            return true
        if ($("#txtVideoUrl").val() != "") {
            var theUrl = wrapURL($("#txtVideoUrl").val());
            var re = new RegExp("^https://vimeo.com/channels/staffpicks/page:[0-9]+$");
            if (re.test(theUrl)) {
                return true;
            } else {
                return Vimeo.ValidateUrl(theUrl);
            }
        }
        return true;
    });
            
    $.validator.addMethod(
        "safe",
        function(value, element, regexp) {
            var re = new RegExp("^[^<>\'\"\\/]+$");
            return this.optional(element) || re.test(value);
        }
    );

    var settings = $.data($("#frmUploadYoutube").get(0), 'validator').settings;
    settings.onkeyup = false;
    settings.onfocusin = false;
    settings.onfocusout = false;

    settings.rules = {
        title: {
            required: true,
            safe: true
        },
        videourl: {
            required: true,                    
            youtubeurl: true,
            vimeo: true
        },
        ddlFolder: {
            required: true
        }
    };

    settings.messages = {
        videourl: {
            required: function()
            {
                if($("#VideoType").val() == "1" || $("#VideoType").val() == "2")
                    return SubmitMsg.Submit_InvalidYoutubeVimeo;
                else if($("#VideoType").val() == "3" || $("#VideoType").val() == "4")
                    return SubmitMsg.Submit_InvalidMediaUrl;
            }                    
        }

    }

    /*Text Message*/
    initUploadTextMessage();
}

function initUploadTextMessage() {
    if (hasUploadTextMessageForm()) {
        var textsettings = $.data($("#frmUploadMessage").get(0), 'validator').settings;
        var messageRules = textsettings.rules;

        textsettings.onkeyup = false;
        textsettings.onfocusin = false;
        textsettings.onfocusout = false;
        
        // move to _TextMessage View
        //if ($("#dvUploadMessage[has-upload-photo]").size() > 0) {
        //    textsettings.ignore = ":hidden:not(#txtMessage,#txtPhotoThumbnailUrl)";
        //    textsettings.messages.txtPhotoThumbnailUrl = $.extend({
        //        "required": "Please select file to upload."
        //    }, textsettings.messages.txtPhotoThumbnailUrl);
        //}

        messageRules.title = $.extend({
            required: true,
            safe: true
        }, messageRules.title);

        messageRules.ddlFolder = $.extend({
            required: true
        }, messageRules.ddlFolder);

        messageRules.txtMessage = $.extend({
            required: true
        }, messageRules.txtMessage);
    }
}

function changeOptMedia(id){
    $('[name="optUploadMedia"][value='+ id + ']').click();    
}
function SetYoutubeType() {
    var Result = parseVideoURL($("#txtVideoUrl").val())

    if (Result.toLowerCase().indexOf("youtu") >= 0) {
        $("#VideoType").val("1")
        $("#SubmitType").val("youtube")
    } else if (Result.toLowerCase() == "vimeo") {
        $("#VideoType").val("2")
        $("#SubmitType").val("vimeo")
    } else if (ContestSetting.Photo_IsAllowUrl == "True") {
        $("#VideoType").val("3");
        $("#SubmitType").val("photoUrl");
    } else if (ContestSetting.Audio_IsAllowUrl == "True") {
        $("#VideoType").val("4");
        $("#SubmitType").val("audioUrl");
    }            
}
function parseVideoURL(url) {

    url = wrapURL(url);

    url = url.replace(/https/ig, 'https');
    url = url.replace(/http/ig, 'http');
    var provider = "";
    if (url.match(/(?:https|http):\/\/(:?www.|m.)?(\w*)/) && url.match(/(?:https|http):\/\/(:?www.|m.)?(\w*)/).length == 3)
        provider = url.match(/(?:https|http):\/\/(:?www.|m.)?(\w*)/)[2]
    return provider;
}    
function showMediaType(dvid, obj) {

    if (obj) {
        $(obj).parents("ul").find("li").removeClass("active");
        $(obj).parents("li").addClass("active");
    }

    $("#medialist > div").hide();
    $("#" + dvid).show();
    jMediaList.activeTab = dvid;
    if (dvid != 'dvDocs')
        $("#" + dvid + " img").css("border", "0px solid #ccc");
}
function RemoveMedia(id) {
    $("#" + id + " .mediaid").val(-1);
    $("#" + id).hide();
}
function AddMedia(type, mediaID) {           
    var data = $('#frmSubmit').serialize();
    if (isNaN(mediaID) == false) {
        $.ajax({
            type: "POST",
            url: SiteConfig.gSiteAdrs + "/Contestant/_Media",
            data: {
                mediaID:mediaID,
                type:type
            },
            success: function(msg) {
                $("#" + jMediaList.activeTab).html(msg);
                $("#frmSubmit").submit();
            }
        });
    } else {
        ShowTimeoutWarning();
    }
}
function SelectedMediaType() {

    if ($("[name=optUploadMedia]:checked").length > 0)    
        return mediaType = $("[name=optUploadMedia]:checked").attr("mediatypeid");

    if ($("#secondary-tabs .active").length == 0) {
        $("#secondary-tabs li:first").addClass('active');
    }
    return $("#secondary-tabs .active").attr("mediatype");
} 