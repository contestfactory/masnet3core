﻿var activePhoto;
var closePhotoDiv = "<div class='close-photo'>X</div>";

function openPhoto() {
    $("#lstContestants").on('click', ".ContestantWithMedia .entry-link[data-voteable='False']", function () {

        if ($(this).hasClass("active"))//need to close
        {
            closePhoto();
        }
        else //open photo
        {
            //close previously opened photo
            closePhoto();

            //open clicked photo
            $(this).addClass("active");
            activePhoto = $(this);
            $(activePhoto).closest(".ContestantWithMedia").css("z-index", "100");

            //append X close div
            $(this).prepend(closePhotoDiv);

            //bind close photo div
            // $( closePhotoDiv ).click(function() {
            //   closePhoto();
            // });  
        }

        return false;



    });
}
function closePhoto() {
    if (activePhoto) {
        $(activePhoto).removeClass("active");
        //console.log( "length: " + $(activePhoto).children(".close-photo").length );
        $(activePhoto).children(".close-photo").remove();
        $(activePhoto).closest(".ContestantWithMedia").css("z-index", "inherit");
    }
}

function makeSameHeight(selector) {
    var maxHeight = 0;
    $(selector).each(function (i) {
        $(this).css("height", "auto")
    });
    $(selector).each(function (i) {
        if ($(this).height() > maxHeight)
            maxHeight = $(this).height();
    });
    $(selector).each(function (i) {
        $(this).css("height", maxHeight + 16)
    });

    //console.log("maxHeight: " + maxHeight);
}