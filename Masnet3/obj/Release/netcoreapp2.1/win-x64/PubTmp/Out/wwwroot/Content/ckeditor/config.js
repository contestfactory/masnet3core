﻿/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';

    config.toolbar_OPUS =
	[
		['Source', 'Preview', 'Print', '-', 'Bold', 'Italic', 'Underline', 'Subscript', 'Superscript', 'SpecialChar', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Find', 'Replace', '-', 'BidiLtr', 'BidiRtl'],
		'/',
		['Font', 'FontSize', 'TextColor', 'BGColor', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Image', 'Link']
	];

    config.toolbar = 'OPUS';
	
    config.removePlugins = 'elementspath';
    config.extraPlugins = 'wordcount';	
	config.allowedContent = true;
	//config.extraAllowedContent = 'ul';	
	config.protectedSource.push(/<ul>(.|\n)*?<\/ul>/g);
	
	config.autoParagraph = false;
	config.enterMode = CKEDITOR.ENTER_BR // pressing the ENTER Key puts the <br/> tag
	config.shiftEnterMode = CKEDITOR.ENTER_P; //pressing the SHIFT + ENTER Keys puts the <p> tag
	
    
    config.wordcount = {

        // Whether or not you want to show the Word Count
        showWordCount: false,
        // Whether or not you want to show the Char Count
        showCharCount: true,
        // Whether or not to include Html chars in the Char Count
        countHTML: false
    };
	
	

};

	
