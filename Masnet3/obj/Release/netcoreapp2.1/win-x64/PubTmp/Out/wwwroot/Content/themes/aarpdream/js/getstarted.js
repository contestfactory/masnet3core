$(function() {

	var getStartedIndex = 1;
	var getStartedIndexOld = 1;
	$(".header-image-container .how-get-started a").on("click", function(){
		getStartedIndex = Number($(this).attr("data-attr"));
		showHideNextPrev();
		getStartedHide();
		getStartedShow();
		$("#getStartedModal").modal();
	});
	$("#getStartedModal .how-get-started a").on("click", function(){
		getStartedIndex = Number($(this).attr("data-attr"));
		showHideNextPrev();
		getStartedHide(300);
		getStartedShow(300);
	});
	function getStartedNext(){
		if(getStartedIndex + 1 <= 4){
			getStartedHide(300);
			getStartedIndex++;
			getStartedShow(300);
		}
		showHideNextPrev();
	}
	function getStartedPrev(){
		if(getStartedIndex - 1 > 0){
			getStartedHide(300);
			getStartedIndex--;
			getStartedShow(300);
		}
		showHideNextPrev();
	}
	function getStartedHide(time){
		if(!time)
			time = 0;
		if(getStartedIndexOld){
			$("#getStartedModal .modal-body .container-fluid:nth-child(" + getStartedIndexOld +")").hide(time);
			$("#getStartedModal .how-get-started .step-div:nth-child(" + (getStartedIndexOld + 1) +")").removeClass("active");
		}
	}
	function getStartedShow(time){
		if(!time)
			time = 0;
		$("#getStartedModal .modal-body .container-fluid:nth-child(" + getStartedIndex +")").show(time);
		getStartedIndexOld = getStartedIndex;
		$("#getStartedModal .how-get-started .step-div:nth-child(" + (getStartedIndexOld + 1) +")").addClass("active");
	}
	$("#getStartedModal .modal-footer .pull-left a").on("click", function(){
		getStartedPrev();
	});
	$("#getStartedModal .modal-footer .pull-right a").on("click", function(){
		getStartedNext();
	});
	function showHideNextPrev(){
		$("#getStartedModal .modal-footer .pull-left a").show();
		$("#getStartedModal .modal-footer .pull-right a").show();
		if(getStartedIndex == 1)
			$("#getStartedModal .modal-footer .pull-left a").hide();
		if(getStartedIndex == 4)
			$("#getStartedModal .modal-footer .pull-right a").hide();
	}

	// console.log("img: " + $("#getStartedModal .how-get-started .step-div:nth-child(2) img").attr("src"));
	// console.log("img: " + $("#getStartedModal .how-get-started .step-div:nth-child(3) img").attr("src"));
	// console.log("img: " + $("#getStartedModal .how-get-started .step-div:nth-child(4) img").attr("src"));
	// console.log("img: " + $("#getStartedModal .how-get-started .step-div:nth-child(5) img").attr("src"));

	// $("#getStartedModal").modal();
});