﻿function ShowRuleMessages(applicationID, contestID, templateId, itemName, isReload) {
    //dvRuleMessages
    $.ajax({
        type: "GET",
        url: SiteURL.Prefix + "/rule/_messagelist",
        data: {
            ApplicationID: applicationID,
            ContestID: contestID,
            TemplateId: templateId,
            ItemName: itemName
        },
        success: function (html) {
            if ($("#dvRuleMessages").size() == 0) {
                $("body").append("<div id='dvRuleMessages' style='display:none'></div>");
            }
            

            $("#dvRuleMessages").html(html);
            if (!isReload) {
                $("#dvRuleMessages").dialog({
                    width: 800,
                    title: "Alerts",
                    close: function () {

                    },
                    lgClass: true
                });
                $("#dvRuleMessages").dialog("open");
                $("#dvRuleMessages").closest(".modal-content").find(".modal-title").html("Alerts for \"" + itemName + "\"");
            }
        }
    });// End: $.ajax()
}

function SaveRuleMessage(editSiteId, editCampaignId, templateId) {
    if ($("#formRuleMessages").valid()) {
        var data = $("#formRuleMessages").serialize() + "&editSiteId=" + editSiteId + "&editCampaignId=" + editCampaignId + "&editTemplateId=" + templateId;
        $.ajax({
            type: "post",
            url: SiteURL.Prefix + "/ContestPages/_SaveResource",
            data: data,
            success: function (data) {
                alert("Data saved!");
            }
        });
    }
}

function ShowRuleList(applicationID, contestID, templateId, itemName, isReload) {
    $.ajax({
        type: "GET",
        url: SiteURL.Prefix + "/rule/_rulelist",
        data: {
            ApplicationID: applicationID,
            ContestID: contestID,
            TemplateId: templateId,
            ItemName: itemName
        },
        success: function (html) {
            if ($("#dvRuleList").size() == 0) {
                $("body").append("<div id='dvRuleList' style='display:none'></div>");
            }
            $("#dvRuleList").html(html);
            if (!isReload) {
                $("#dvRuleList").dialog({
                    width: 800,
                    title: "Rules",
                    close: function () {

                    },
                    lgClass: true
                });
                $("#dvRuleList").dialog("open");
                $("#dvRuleList").closest(".modal-content").find(".modal-title").html("Rules for \"" + itemName + "\"");
            }
        }
    });// End: $.ajax()
}
// open Edit rule dialog
function ShowEditRule(applicationID, contestID, templateId, itemName, ruleKey) {
    //var container = $(el).closest(".rule-wrapper");
    if (!ruleKey)
        ruleKey = '';

    var param = {
        ApplicationID: applicationID,
        ContestID: contestID,
        TemplateId: templateId,
        ItemName: itemName,
        RuleKey: ruleKey

    };
    if ($("#isDebug").size() > 0) {
        param.isDebug = $("#isDebug").val();
    }

    $.ajax({
        type: "GET",
        url: SiteURL.Prefix + "/rule/_editrule",
        cache: false,
        data: param,
        success: function (html) {
            if ($("#dvEditRule").size() == 0) {
                $("body").append("<div id='dvEditRule' style='display:none'></div>");
            }

            if ($("#dvEditRule").size() == 0) {
                var divEdit = '<div id="dvEditRule" style="display: none"></div>';
                $('body').append(divEdit);
            }
            $("#dvEditRule").html(html);
            var title = ruleKey == '' ? 'Add Rule for ' : 'Edit Rule for ';
            $("#dvEditRule").prop('itemName', itemName);
            $("#dvEditRule").dialog({
                width: 800,
                title: title + "\"" + itemName + "\"",
                close: function () {
                    if ($("#dvEditRule").prop("data-changed")) {
                        ShowRuleList(applicationID, contestID, templateId, $("#dvEditRule").prop('itemName'), true);
                        $("#dvEditRule").prop("data-changed", false);
                    }
                }
            });
            $("#dvEditRule").dialog("open");
            $("#dvEditRule").closest(".modal-content").find(".modal-title").html(title + itemName);
        }
    });// End: $.ajax()
}

