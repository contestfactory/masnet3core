﻿function ajaxSubmitMediaFile() {
    if (UploadControl.fn_Validate && !UploadControl.fn_Validate())
        return false;

    if (UploadControl.fn_DoUpload) {
        UploadControl.fn_DoUpload();
    }
}//End: ajaxSubmitMediaFile()

function DoUpdatingDataAfterUploading(errorCode, resultfileName) {
    if (errorCode == 0) {
        var obj = $("[data-upload-running]").first();
        $("#dialogUploadMedia").dialog("close");
        $(obj).attr("data-upload-running", "1");
        $(obj).attr("data-media-uploadedfileName", resultfileName);
        var callback = $(obj).attr("data-media-callback");
        if (callback != null && callback.trim() != "") {
            eval(callback);
        }
    }
    else if (errorCode == 1) alert(SiteLanguage.Media_SelectFile);
    else if (errorCode == -1) alert("Upload Fail: An exception occurred.");
}//End: DoUpdatingDataAfterUploading(errorCode, resultfileName)

function uploadLogoSuccess() {
    var obj = $("[data-upload-running]").first();

    var uploadFileName = SiteURL.VirtualUploadPath + "/" + SiteConfig.UploadFolder + "/" + $(obj).attr("data-media-appname") + "/" + $(obj).attr("data-media-username") + "/" + $(obj).attr("data-media-uploadedfileName");
    console.log(uploadFileName);

    //Create f thumbnail
    $.ajax({
        type: "post",
        url: SiteURL.Root + SiteConfig.SiteName + "/base/updatelogo",
        data: { filepath: uploadFileName },
        success: function (data) {
            window.parent.loadActivePage();
        }
    });
}//End: uploadSuccess