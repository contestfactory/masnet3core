//--------------------------------------
//  PROPERTIES
//--------------------------------------
/**
/**
 *  
 */
var columnsWheels = 3;
var wheel0 = new Wheel("wheel0");
var wheel1 = new Wheel("wheel1");
var wheel2 = new Wheel("wheel2");
var _animationInterval0;
var _animationInterval1;
var _animationInterval2;
var _prizesArr;

//--------------------------------------
//  Jquery ready
//--------------------------------------
/**
/**
 *  
 */
$(document).ready(function() 
{
	//wheel0._wheelID = "wheel0";
	//wheel1._wheelID = "wheel1";	
	//wheel2._wheelID = "wheel2";
	
	wheel0.initAddPrizes();
	wheel1.initAddPrizes();
	wheel2.initAddPrizes();
	
	//callWebservice();
	
});

function startAnimation0()
{
	debugMsg("core.startAnimation0");
	
	wheel0.initDefault();
	wheel1.initDefault();
	wheel2.initDefault();	
	clearInterval(_animationInterval0);
	clearInterval(_animationInterval1);
	clearInterval(_animationInterval2);
	
	
	_animationInterval0 = setInterval(tweenWheel0, 30);	
	setTimeout(startAnimation1, 200);
}
function startAnimation1()
{
	_animationInterval1 = setInterval(tweenWheel1, 30);	
	setTimeout(startAnimation2, 200);
}
function startAnimation2()
{
	_animationInterval2 = setInterval(tweenWheel2, 30);	
}
function tweenWheel0()
{
	wheel0.enterFrame();
	wheel0.interval = _animationInterval0;
}
function tweenWheel1()
{
	wheel1.enterFrame();
	wheel1.interval = _animationInterval1;
}
function tweenWheel2()
{
	wheel2.enterFrame();
	wheel2.interval = _animationInterval2;
}
//--------------------------------------
//  setPrizes - STOP ANIMATION
//--------------------------------------
/**
/**
 *  
 */
//function setPrizes( prizesArr, timeSpin )
function setPrizes( prizesArr )
{
	
	//var prizesArr = [0, 0, 0]
	debugMsg("core.setPrizes - prizesArr: " + prizesArr);
	debugMsg("prizesArr.length: " + prizesArr.length);
	
	_prizesArr = prizesArr;
	
	setTimeout(timeOutSpinPrize, 2000);
	
	var prizeCompare = prizesArr[0];
	for (i = 0; i < prizesArr.length; i++) 
	{
		if ( prizesArr[i] != prizeCompare ) 
		{
			_winner = false;
			break;
		}
		else _winner = true;
	}
	
	//if ( prize1 == prize2 && prize1 == prize3 && prize2 == prize3) _winner = true;
	//else _winner = false;
}
function timeOutSpinPrize()
{
	//alert("timeOutSpinPrize");	
	var timeSpin = 1000;
	
	debugMsg("timeOutSpinPrize");
	debugMsg("_prizesArr[0]: " + _prizesArr[0]);
	debugMsg("_prizesArr[1]: " + _prizesArr[1]);
	debugMsg("_prizesArr[2]: " + _prizesArr[2]);
	
	wheel0.setSpinPrize( _prizesArr[0], timeSpin );
	wheel1.setSpinPrize( _prizesArr[1], timeSpin );
	wheel2.setSpinPrize( _prizesArr[2], timeSpin );
	
}
function stopRotatingCore(wheelID, timeSpin)
{
	//debugMsg( "stopRotatingCore: " + wheelID);
	switch(wheelID)
	{
		case "wheel0":
			setTimeout( stopWheel0, this._timeStop);
			break;
		case "wheel1":
			setTimeout( stopWheel1, this._timeStop * 2);
			break;
		case "wheel2":
			setTimeout( stopWheel2, this._timeStop * 3);
			break;
	}
}
function stopWheel0()
{
	wheel0.stopRotating();	
}
function stopWheel1()
{
	wheel1.stopRotating();	
}
function stopWheel2()
{
	wheel2.stopRotating();	
}

function removeInterval(wheelID)
{
	// if (wheelID == "wheel0")
		// clearInterval(_animationInterval0);
	// if (wheelID == "wheel1")
		// clearInterval(_animationInterval1);
	// if (wheelID == "wheel2")
		// clearInterval(_animationInterval2);
}

//--------------------------------------
//  WEBSERVICES
//--------------------------------------
/**
/**
 *  
 */
function callWebservice(){
    var divToBeWorkedOn = '#debugger';
    //var webMethod = 'http://www.makeastar.com/Webservices/contest.asmx/CF_GetContest_Info'
	var webMethod = 'http://new.cfdevs.com/masnew/Webservices/contest.asmx/CF_GetContest_Info';
    var parameters = "ContestID=0&ContestType=&Params=";
	
	makePOSTRequest( webMethod, parameters, processReturn, true)
	
	/*
    $.ajax({
        type: "POST",
        url: webMethod,
        data: parameters,
        contentType: "application/x-www-form-urlencoded",
        dataType: "XML",
        success: function(msg) {    
            $(divToBeWorkedOn).html(msg.d);
        },
        error: function(e){
            $(divToBeWorkedOn).html("Unavailable");              
        }
    });
	*/
}
var prizeImgArr = new Array();
/*
*
* [url, width, height]
*
*/

function processPrizes(prizes)
{
	prizes = unescape(prizes);
	//alert( "processPrizes: " + prizes );
	var response = prizes; //hardcoded data
		
	//IE9
	var parser = new DOMParser();
	var doc = parser.parseFromString(response, "text/xml");
	
	for (var i = 0; i < doc.getElementsByTagName('prize').length; i++)
	{
		//alert( resultXML..prize[i].@descWeb );
		prizeImgArr[i] = [ doc.getElementsByTagName('prize')[i].getAttribute('descWeb'), 0, 0, doc.getElementsByTagName('prize')[i].getAttribute('name') ];
		//alert( doc.getElementsByTagName('prize')[i].getAttribute('descWeb') );
	}
	startLoading();
}

function processReturn() 
{
	if (http_request.readyState == 4) 
	{		
		if (http_request.status == 200) 
		{
			//var result = http_request.responseText;	
			
			//fix error "xml is a reserved identifier"
			//var response = http_request.responseText; //dynamic data
			var response = prizes; //hardcoded data
			//response = response.replace(/^<\?xml\s+version\s*=\s*(["'])[^\1]+\1[^?]*\?>/, ""); // bug 336551  
			//var resultXML = new XML(response);  
				
			//IE9
			var parser = new DOMParser();
			//var doc = parser.parseFromString(http_request.responseText, "text/xml");
			var doc = parser.parseFromString(response, "text/xml");
			
			//alert("XML: " + doc.getElementsByTagName('prize').length);
			
			
			for (var i = 0; i < doc.getElementsByTagName('prize').length; i++)
			{
				//alert( resultXML..prize[i].@descWeb );
				prizeImgArr[i] = [ doc.getElementsByTagName('prize')[i].getAttribute('descWeb') ];
				//alert( doc.getElementsByTagName('prize')[i].getAttribute('descWeb') );
				//
			}
			startLoading();
			
			
			/*
			if (result == "")
			{
				alert("This mashup is not available at this time.  Please try again later.")
			}
			else
			{			
				var objDownloadKaltura = document.getElementById("ifrDownloadKaltura");
				if (objDownloadKaltura 
					&& typeof(objDownloadKaltura) != 'undefined' 
					&& objDownloadKaltura != "")
				{
					objDownloadKaltura.src = result; 
				}
			}
			*/
		}
		else
		{
			alert("The download file is not available.");
		}
	}
}

function StringtoXML(text){
	if (window.ActiveXObject){
	  var doc=new ActiveXObject('Microsoft.XMLDOM');
	  doc.async='false';
	  doc.loadXML(text);
	} else {
	  var parser=new DOMParser();
	  var doc=parser.parseFromString(text,'text/xml');
	}
	return doc;
}
//--------------------------------------
//  UTILS FUNCTIONS
//--------------------------------------
/**
/**
 *  
 */
function debugMsg(msg)
{
	$("#debugger").html( $("#debugger").html() + "<br />" + msg);
}
//--------------------------------------
//  LAYOUTS
//--------------------------------------
/**
/**
 *  
 */
function showSpinZone()
{
	//alert("showSpinZone");
	
	
	for(var i = 0; i < columnsWheels; i++)
	{
		var clonedPrize = _randomStartIndexes;
		for(var y = 0; y < 4; y++)
		{
			//var clonedObjectArray = getRandomPrize();
			var clonedObject = getRandomPrize();
			//var randomIndex = clonedObjectArray[1];
			
			//var marginLeft = Math.round( ( 162 - prizeImgArr[randomIndex][1] ) / 2);
			//var marginTop = Math.round( ( 124 - prizeImgArr[randomIndex][2] ) / 2);			
			//$(clonedObject).css({"margin-top": marginTop});
			
			//debugMsg( "randomIndex: " + randomIndex + ", src: " + prizeImgArr[randomIndex][0] + ", marginTop: " + marginTop + ", marginLeft: " + marginLeft );
			
			//alert( $(clonedObject).css("margin-top") );
			$("#wheel" + i + "_prize" + y).html(clonedObject);
		}
	}
	$("#actionButtons").css("display", "block");
	
}











