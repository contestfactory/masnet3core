﻿var jsReady = false;
var iSiteID = 0;
var iCampaignID = 0;
var sClientIP = "234.16.296.31";
var iCookieDuration = 30;

var iMinPWDLength = 5;

var sPrizeName = null;
var sPrizeIndexNumer = null;

var sResultsAudioURLIndex = "";
var sAudioToggleState = "Mute";

var sClientGUID = "";
var sURL = null;

var iSpinTime = 3000;
var sSpinResults = "";
var sSpinResult = "";

var sTransactionHistory = null;
var iCurrentTransactionCharge = 0;

var sTransactionResult = "";
var sTransactionType = "";
var sTransactionValueType = "";
var flTransactionValueAmount = "";
var sTransactionUserMessage = "";

var divSpinResults = null;
var txtSpinCreditHistory = null;
var txtSweepsPointsHistory = null;
var txtSpinCost = null;

var MaxWinsPerUser = 0;
var bLockOutWinners = false;

var AwardID = "";
var PrizeID = "";
var PrizeName = "";
var PrizeDesc = "";
var PrizeURL = "";

var xmlDoc;

var iNumberOfSpinReels = 3;

var sValueType = "";
var sValueAmount = "";

var sSpinCreditBalance = 0;
var sSweepsPoints = 0;
var sCashBalance = 0;
var sSpinResults = null;

var prizes = "";

//var audioPath = "skins/standard/audio/";

// Indexed
// 0
var audioPreSpinURL = audioPath + "Pre_spin_option_1-slot_machine_bet_all_03.mp3";

// 1
var audioSpinURL = audioPath + "Spinning_option_1-gamemachine09.mp3";

// 2
var audioLoseURL = audioPath + "Win_option_2-slot_machine_win_jackpot_04.mp3";

// 3
var audioWinURL = audioPath + "Win_option_1-slot_machine_win_25.mp3";

// audio config
var AudioConfig = {
    "SPIN": {
        "container": "musicplayer_spin",
        "playerid": "mp3playerspin",
        "mp3": audioSpinURL
    },
    "WIN": {
        "container": "musicplayer_win",
        "playerid": "mp3playerwin",
        "mp3": audioWinURL
    },
    "LOSE": {
        "container": "musicplayer_lose",
        "playerid": "mp3playerlose",
        "mp3": audioLoseURL
    }
};

function preloadImages() {
    var siteAdr = "http://spindev.contestfactory.com";
    var bImagesArePreloaded = getCookie("bImagesArePreloaded");
    if (bImagesArePreloaded == null || bImagesArePreloaded == "" || bImagesArePreloaded == "") {
        // Image Counter
        var i = 0;

        // create object
        imageObj = new Image();

        // set image list
        images = new Array();

        // Page bg
        images[0] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Backgrounds/SpinZone_Background_Panel_Optimized.jpg";

        // Company Logo header
        images[1] = siteAdr + "/Sites/Defaults/Images/Logos/cf_logo_header.png";

        // Company Logo footer
        images[2] = siteAdr + "/Sites/Defaults/Images/Logos/poweredBy_Cropped.png";

        // Home icon
        images[3] = siteAdr + "/Sites/Defaults/Images/Icons/home_icon.png";

        // FaceBook icon
        images[4] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_FaceBook.png";

        // Twitter icon
        images[5] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Twitter.png";

        // Email icon
        images[6] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Email.png";

        // Submit promo code
        images[7] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Buttons/continue_off.png";

        // Header prize images
        images[8] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/grand_prize_top.png";
        images[9] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/nano.png";
        images[10] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/fandango.png";
        images[11] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/kindle.png";
        images[12] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/amazon.png";
        images[13] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/shuffle.png";
        images[14] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/touch.png";

        // Footer prize images
        images[15] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/grand_prize_footer.png";
        images[16] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/nano.png";
        images[17] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/fandango.png";
        images[18] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/kindle.png";
        images[19] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/amazon.png";
        images[20] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/shuffle.png";
        images[21] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/touch.png";

        // Footer-Screened prize images
        images[22] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/spinzone_white_text-screened.png";
        images[23] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/nano-Screened.png";
        images[24] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/fandango-Screened.png";
        images[25] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/kindle-Screened.png";
        images[26] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/amazon-Screened.png";
        images[27] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/shuffle-Screened.png";
        images[28] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Prizes/Thumbnails/touch-Screened.png";

        // Spin Button
        images[29] = siteAdr + "/Sites/SiteID_1/Campaigns/CampaignID_3/Images/Buttons/spin_off.png";

        // Social network sharing icons
        images[30] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Blogger.png";
        images[31] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Delicious.png";
        images[32] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Digg.png";
        images[33] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_FaceBook.png";
        images[34] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_FriendFeed.png";
        images[35] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Multiply.png";
        images[36] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_MySpace.png";
        images[37] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Plaxo.png";
        images[38] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Reddit.png";
        images[39] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_StumbleUpon.png";
        images[40] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Tumblr.png";
        images[41] = siteAdr + "/Sites/Defaults/Images/Icons/Social-Sharing-Icons/socialLogos_Twitter.png";

        // Example images
        images[42] = siteAdr + "/Images/ScreenCaps/sample_1.jpg";
        images[43] = siteAdr + "/Images/ScreenCaps/sample_2.jpg";
        images[44] = siteAdr + "/Images/ScreenCaps/sample_3.jpg";
        images[45] = siteAdr + "/Images/ScreenCaps/sample_4.jpg";
        images[46] = siteAdr + "/Images/ScreenCaps/sample_5.jpg";
        images[47] = siteAdr + "/Images/ScreenCaps/sample_6.jpg";
        images[46] = siteAdr + "/Images/ScreenCaps/sample_7.jpg";
        images[47] = siteAdr + "/Images/ScreenCaps/sample_8.jpg";

        // start preloading
        for (i = 0; i <= images.length; i++) {
            imageObj.src = images[i];
        }

        setCookie("bImagesArePreloaded", "true", iCookieDuration, "/", "", false);
    }
}

function validatePromocode() {

    var objPromocodeField = document.getElementById("txtPromotional_Code");

    if (objPromocodeField.value == null || objPromocodeField.value == "") {
        alert("Please enter your Promo Code in the required field.");
        objPromocodeField.focus();
        return false
    }
    else {
        return true;
    }
}

function validateLogin() {

    // Validate email syntax
    var bEmailValid = false;
    var objEmailField = document.getElementById("txtEmailLogin");
    if (objEmailField.value == null || objEmailField.value == "") {
        alert("Please enter your Email Address in the required field.");
        objEmailField.focus();
        return false
    }
    else {
        // If valid email syntax, then continue processing
        bEmailValid = validateEmail(objEmailField, "Email Address");
        if (!bEmailValid) {
            objEmailField.focus();
            return false;
        }
    }

    // Ensure they have entered at least 3 characters
    var bPWDValid = false;
    var objPWDField = document.getElementById("txtPWDLogin");
    if (objPWDField.value == null || objPWDField.value == "") {
        alert("Please enter your Password in the required field.");
        objPWDField.focus();
        return false
    }
    else {
        if (objPWDField.value.length < iMinPWDLength) {
            alert("Passwords are a minimum of " + iMinPWDLength + " characters in length.");
            objPWDField.focus();
            return false
        }
        else {
            bPWDValid = true;
        }
    }

    if (bEmailValid && bPWDValid) {
        return true;
    }
    else {
        return false;
    }
}

function validateContactForm() {
    var sName = document.getElementById("txtContactName");
    var sCompanyName = document.getElementById("txtContactCompanyName");
    var sEmail = document.getElementById("txtContactEmail");

    if (sName.value == null || sName.value == "") {
        alert("Please enter your Name in the required field.");
        sName.focus();
        return false
    }
    else {
        if (!validateName(sName, "Name")) {
            sName.focus();
            return false;
        }
    }

    if (sCompanyName.value == null || sCompanyName.value == "") {
        alert("Please enter your Company Name in the required field.");
        sCompanyName.focus();
        return false
    }
    else {
        if (!validateAlphaNumeric(sCompanyName, "Company Name")) {
            sCompanyName.focus();
            return false;
        }
    }

    if (sEmail.value == null || sEmail.value == "") {
        alert("Please enter your Email Address in the required field.");
        sEmail.focus();
        return false
    }
    else {
        // If valid email syntax, then continue processing
        var bEmailValid = validateEmail(sEmail, "Email Address");
        if (bEmailValid) {
            return true;
        }
        else {
            sEmail.focus();
            return false;
        }
    }

    var sPhone = document.getElementById("txtContactPhone");

    return true;
}

function setCursorPointer(objCursorObject, cursorType) {
    objCursorObject.style.cursor = cursorType;
}

function passwordReset() {
    var sPromptMessage = "";
    sPromptMessage += "Please be advised, this will reset the password on your account.";
    sPromptMessage += "\nOnce you have entered a valid email, the system will create a\nrandom password and send this to your email account momentarily.";
    sPromptMessage += "\n\nIf you wish to continue, please enter your valid email address\nand click OK. If not, simply click Cancel.\n\n";

    var UserEmailAddress = window.prompt(sPromptMessage)

    if (UserEmailAddress == null || UserEmailAddress == "") {
        // Don't do anything here. It's a cancel event
    }
    else {
        // Validate email address here. If it is valid, call the web service with password reset request
        var bEmailValid = validateEmail_jsPrompt(UserEmailAddress);
        if (bEmailValid) {
            var msgDiv = document.getElementById("lblSystemMessage_Text");

            var bSendResult = true;
            if (bSendResult) {
                msgDiv.innerHTML = "An email has been sent to " + UserEmailAddress + " with your new password. Please check your inbox for this email message.";
            }
            else {
                msgDiv.innerHTML = "<span style='color: #ff0000;'><b>Please be advised, " + UserEmailAddress + " is not registered on our system.</b></span><br />Ensure you are entering your correctly registered email address."
            }
        }
        else {
            passwordReset();
        }
    }
}

//var gNumberOfAvailablePrizes = 0;
function getSpinPrizesLoss() 
{

	var arrImageId = new Array();
	var imageId = null;
	var isUnique = false;
	var tempDistinct = false;

	// Need to determine the Winning prize index
	// or randomly generate the indexes for a loss making sure they don't match
	var spinResult = null;
	var i = 1;
	var iNumberOfSpinReels = 3; //document.getElementById("txtSpinReelsNumber").value;
	var iNumberOfAvailablePrizes = gNumberOfAvailablePrizes;

	var sPrizeCollectionIndexes = "";


	arrImageId[0] = getRandomNumber(iNumberOfAvailablePrizes);
	
	
	isUnique = false;
	do
	{
		arrImageId[1] = getRandomNumber(iNumberOfAvailablePrizes);
		if( arrImageId[1] != arrImageId[0] )
		{
			isUnique = true;
		}
	}
	while(!isUnique)
	
	isUnique = false;
	do
	{
		arrImageId[2] = getRandomNumber(iNumberOfAvailablePrizes);
		if( arrImageId[2] != arrImageId[1] & arrImageId[2] != arrImageId[0] )
		{
			isUnique = true;
		}
	}
	while(!isUnique)
	
	for(var i=0; i < arrImageId.length; i++ )
	{
		sPrizeCollectionIndexes += arrImageId[i];
		if (i != arrImageId.length - 1)
			sPrizeCollectionIndexes += "|";
	}
	
	/*
	sPrizeCollectionIndexes = sPrizeCollectionIndexes.split("|");
	
	if( sPrizeCollectionIndexes[0] == sPrizeCollectionIndexes[1] || sPrizeCollectionIndexes[1] == sPrizeCollectionIndexes[2] || sPrizeCollectionIndexes[2] == sPrizeCollectionIndexes[0])
		alert(sPrizeCollectionIndexes + "     Bug - Loose 2 prizes should be different!!!!");
	
	if(sPrizeCollectionIndexes[0] == sPrizeCollectionIndexes[1] &&  sPrizeCollectionIndexes[1] == sPrizeCollectionIndexes[2] )
		alert(sPrizeCollectionIndexes + "     Bug - Loose 3 prizes should be different!!!!");
	
	*/
	return sPrizeCollectionIndexes;
}

function getRandomNumber(iNumberOfAvailablePrizes) {

    randomnumber = Math.floor(Math.random() * iNumberOfAvailablePrizes);

    return randomnumber;
}

function isReady() {

    mute();
    return jsReady;
}

function checkForSocialSharingRedirection() {
    var sRedirectToURL = document.getElementById("txtRedirectToURL");

    // Popop referral window then set hidden field to empty
    if (sRedirectToURL.value != "") {
        window.open(sRedirectToURL.value);
        sRedirectToURL.value = "";
    }
}

function checkPersonalization() {
    var sCurrentPageState = document.getElementById("txtCurrentPageState").value;

    var sUserFirstName = document.getElementById("txtUserFirstName");
    var sUserLastName = document.getElementById("txtUserLastName");

    var sUserFullName = sUserFirstName.value + " " + sUserLastName.value;
    if (sUserFullName == " ") // This is an anonymous user
    {
        switch (sCurrentPageState) {
            case "CompanySignup":
                document.getElementById("txtContactName").value = "";
                break;
        }
    }
    else {
        switch (sCurrentPageState) {
            case "CompanySignup":
                document.getElementById("txtContactName").value = sUserFullName;
                break;

            case "ShareThis":
                document.getElementById("txtSenderName").value = sUserFullName;
                break;
        }
    }
}

function pageInit() {

    //alert("pageInit");
	//checkForSocialSharingRedirection();

    //checkPersonalization();

    //turnOnAudio(false);

    //if (Get_Cookie("sp") != "1")
    if(true) {
	
    preloadImages();


    var sQueryStringResult = getQueryStringKeyValue("networkName", null);
    if (sQueryStringResult != null && sQueryStringResult != "") {
        if (sQueryStringResult == "Email") {
            showEmailReferalsPanel();
        }
        if (sQueryStringResult == "AllSocial") {
            showSocialReferalsPanel();
        }
    }

    if (document.getElementById("btnContinue") != null) {
        document.getElementById("btnContinue").style.display = "none";
    }

    var txtCurrentPageState = document.getElementById("txtCurrentPageState");
    var objFooter = document.getElementById("divPrizeFooter");
    var objPoweredBy = document.getElementById("divPoweredBy");

    if (txtCurrentPageState != null) {
        if (txtCurrentPageState.value == "LoggedIn") {
            //objFooter.className = "divPrizeFooter-Raised";
            //objPoweredBy.className = "PoweredBy-Raised";

            // Hide the spin button if they don't have enough credits
			try{
	            var objSpinCredits = document.getElementById("txtSpinCreditHistory");
	            if (objSpinCredits != null) {
	                var iCreditsLeft = parseInt(objSpinCredits.value);
	                if (iCreditsLeft < 1) {
	                    document.getElementById("btnSpinTheWheel").style.display = "none";
	                    //$("#btnSpinTheWheel").attr("disabled", "disabled");

	                    var sCreditMsg = "";
						//alert("uFirstName: " + uFirstName);
						sCreditMsg += "<b>" + document.getElementById("txtUserFirstName").value + ", we are sorry to say that you are out of spins.</b>";
	                    sCreditMsg += "<br />";
	                    sCreditMsg += "Please refer a colleague to earn more spins.";
	                    sCreditMsg += "";

	                    document.getElementById("lblSystemMessage_Text").value = sCreditMsg;
	                }
	                else {
	                }
	            }
	            retrievePrizeCollection();
			}
			catch(e){}
        }
        else {
            //objFooter.className = "divPrizeFooter";
            //objPoweredBy.className = "PoweredBy";
        }


        // Set default form field focus
        var iCampaignID = document.getElementById("txtCampaignID").value;
        var sPageState = txtCurrentPageState.value;

        switch (sPageState) {
            case "Register":
                switch (iCampaignID) {
                    case "3":
                        if (document.getElementById("txtEmailAddress") != null)
                            document.getElementById("txtEmailAddress").focus();
                        break;

                    case "4":
                        if (document.getElementById("txtFirstName") != null)
                            document.getElementById("txtFirstName").focus();
                        break;
                }
                break

            case "RegistrationConfirmation":
                switch (iCampaignID) {
                    case "4":
                        break;

                    default:
                        // Do nothing
                        break;
                }
                break;
        }
    }

    jsReady = true;

//    if (document.getElementById("txtSystemErrorMessage").value != "") {
//        // Do not scroll page
//    }
//    else {
//        scroll(0, 0);
//    }
	flashLoaded();
	
	}
}

function showSpinButton() {
    var iUserCredits = parseInt(document.getElementById("txtSpinCreditHistory").value);
    if (iUserCredits < 1) {
        document.getElementById("btnSpinTheWheel").style.display = "none";
        //$("#btnSpinTheWheel").attr("disabled", "disabled");
    }
    else {
        document.getElementById("btnSpinTheWheel").style.display = "";
        //$("#btnSpinTheWheel").removeAttr("disabled");
    }
}

function getQueryStringKeyValue(key, default_) {
    if (default_ == null) default_ = "";
    key = key.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + key + "=([^&#]*)");
    var qs = regex.exec(window.location.href);
    if (qs == null)
        return default_;
    else
        return qs[1];
}

function checkEmpty(sField, sFieldName) {

    if (sField.value == "") {
        alert(" Please enter your " + sFieldName + " in the required field.");
        return true;
    }
    return false;
}

function validateFormFields(div) {

    div = typeof div === "string" ? document.getElementById(div) : div;
    var sFields = div.getElementsByTagName("*");
    var bValidated = true;
    var bRequiredField = "false";

    for (var i = 0, maxI = sFields.length; i < maxI; ++i) {
        var sField = sFields[i];
        bRequired = "false";
        if (bValidated == true) {

            if (sField.id != null && sField.id != "") {
                bRequiredField = document.getElementById(sField.id).getAttribute("isRequired");
            }

            switch (sField.id) {
                case "txtEmailAddress":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Email')) {
                            if (!validateEmail(sField, 'Email')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtEmailAddress_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtEmailConfirm":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Confirm Email')) {
                            if (!validateEmail(sField, 'Confirm Email')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 1);
                                bValidated = false;
                            }
                            else {
                                if (document.getElementById("txtEmailAddress").value.toLowerCase() != document.getElementById("txtEmailConfirm").value.toLowerCase()) {
                                    alert("Both Email address and Confirm Email address must be the same");
                                    tempField = sField;
                                    setTimeout("tempField.focus();", 1);
                                    bValidated = false;
                                }
                                else {
                                    bValidated = true;
                                }
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtPWD":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Password')) {
                            if (!validatePassword(sField, 'Password')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtPWD_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtConfirmPassword":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Confirm Password')) {
                            if (!validatePassword(sField, 'Confirm Password')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 1);
                                bValidated = false;
                            }
                            else {
                                if (document.getElementById("txtPWD").value != document.getElementById("txtPWDConfirm").value) {
                                    alert("Both Password and Confirm Password must be the same");
                                    tempField = sField;
                                    setTimeout("tempField.focus();", 1);
                                    bValidated = false;
                                }
                                else {
                                    bValidated = true;
                                }
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtOrganizationName":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Company')) {
                            if (!validateAlphaNumeric(sField, 'Company')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtOrganizationName_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtOrganizationJobTitle":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Job Title')) {
                            if (!validateAlphaNumeric(sField, 'Job Title')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtOrganizationJobTitle_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break; case "txtFirstName":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'First Name')) {
                            if (!validateName(sField, 'First Name')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtFirstName_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtLastName":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Last Name')) {
                            if (!validateAlphaNumeric(sField, 'Last Name')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtLastName_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtPhoneWork":
                    if (bRequiredField == "true") {
                        if (!validatePhone(sField, 'Phone')) {
                            tempField = sField;
                            setTimeout("tempField.focus();", 1);
                            bValidated = false;
                        }
                        else {
                            bValidated = true;
                            document.getElementById("txtPhoneWork_Hidden").value = sField.value;
                        }
                    }
                    else {
                        if (sField.value.length > 0) {
                            if (!validatePhone(sField, 'Phone')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 1);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtPhoneWork_Hidden").value = sField.value;
                            }
                        }
                    }
                    break;

                case "chkOver18":
                    if (bRequiredField == "true") {
                        if (sField.checked == false) {
                            alert("Please select check box: " + sField.value);
                            tempField = sField;
                            setTimeout("tempField.focus();", 1);
                            bValidated = false;
                        }
                        else {
                            bValidated = true;
                            document.getElementById("chkOver18_Hidden").value = sField.checked;
                        }
                    }
                    break;

                case "txtAddress1":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Address')) {
                            if (!validateAlphaNumeric(sField, 'Address')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtAddress1_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "txtAddress2":
                    document.getElementById("txtAddress2_Hidden").value = sField.value;
                    break;

                case "dlState":
                    if (bRequiredField == "true") {
                        {
                            if (sField.selectedIndex > 0) {
                                bValidate = true;
                                document.getElementById("txtState_Hidden").value = sField.value;
                            }
                            else {
                                alert("Please select a value in State field");
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                        }
                    }
                    break;

                case "txtCity":
                    if (bRequiredField == "true") {
                        {
                            if (!checkEmpty(sField, 'City')) {
                                if (!validateAlphabet(sField, 'City')) {
                                    tempField = sField;
                                    setTimeout("tempField.focus();", 5);
                                    bValidated = false;
                                }
                                else {
                                    bValidated = true;
                                    document.getElementById("txtCity_Hidden").value = sField.value;
                                }
                            }
                            else {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                        }
                    }
                    break;

                case "txtZip":
                    if (bRequiredField == "true") {
                        if (!checkEmpty(sField, 'Zip')) {
                            if (!validateZipCode(sField, 'Zip')) {
                                tempField = sField;
                                setTimeout("tempField.focus();", 5);
                                bValidated = false;
                            }
                            else {
                                bValidated = true;
                                document.getElementById("txtZip_Hidden").value = sField.value;
                            }
                        }
                        else {
                            tempField = sField;
                            setTimeout("tempField.focus();", 5);
                            bValidated = false;
                        }
                    }
                    break;

                case "chkNewsletterOptIn":
                    if (bRequiredField == "true") {
                        if (sField.checked == false) {
                            alert("Please select check box: " + sField.value);
                            tempField = sField;
                            setTimeout("tempField.focus();", 1);
                            bValidated = false;
                        }
                        else {
                            bValidated = true;
                            document.getElementById("chkNewsletterOptIn_Hidden").value = sField.checked;
                        }
                    }
                    break;
            }
        }
        else {
            return false;
            break;
        }
    }
    if (bValidated == false)
        return false;
}

function LTrim(value) {
    var re = /\s*((\S+\s*)*)/;
    return value.replace(re, "$1");
}

function RTrim(value) {
    var re = /((\s*\S+)*)\s*/;
    return value.replace(re, "$1");
}

function trim(value) {
    return LTrim(RTrim(value));
}

function validateEmail(sField, sFieldName) {
    sEmailAddress = sField.value;

    var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if (emailPattern.test(sEmailAddress) == false) {
        alert("Please enter a valid email in " + sFieldName + " field");
        return false;
    }
    return true;
}

function validateEmail_jsPrompt(sEmailAddress) {

    var emailPattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[(2([0-4]\d|5[0-5])|1?\d{1,2})(\.(2([0-4]\d|5[0-5])|1?\d{1,2})){3} \])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailPattern.test(sEmailAddress) == false) {
        return false;
    }
    return true;
}

function validateAlphabet(sField, sFieldName) {
    var sText = trim(sField.value);

    var textPattern = /^[a-zA-Z,\-\. ]+$/;
    if (textPattern.test(sText) == false) {
        alert("Please enter a valid value in " + sFieldName + " field");
        return false;
    }
    return true;
}
function validateAlphaNumeric(sField, sFieldName) {
    var sText = trim(sField.value);

    var textPattern = /^[0-9a-zA-Z#,\-'\. ]+$/;
    if (textPattern.test(sText) == false) {
        alert("Please enter a valid value in " + sFieldName + " field");
        return false;
    }
    return true;
}

function validateZipCode(sField, sFieldName) {
    var sText = trim(sField.value);

    var textPattern = /^\d{5}$/;
    if (textPattern.test(sText) == false || sText.length > 5) {
        alert("Please enter a valid 5 digit zip code");
        return false;
    }
    return true;
}

function validateName(sField, sFieldName) {
    var sName = trim(sField.value);

    var namePattern = /^[0-9a-zA-Z, \-']+$/;
    if (namePattern.test(sName) == false) {
        alert("Please enter a valid value in " + sFieldName + " field");
        return false;
    }
    else {
        if (sName.length > 19) {
            alert("The " + sFieldName + " field does not allow more than 20 characters.");
            return false;
        }
        else {
            return true;
        }
    }
    return true;
}

function validatePhone(sField, sFieldName) {
    sPhone = sField.value;
    var phoneNumberPattern = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;

    if (phoneNumberPattern.test(sPhone) == false) {
        alert("Please enter a valid value in " + sFieldName + " field\n(xxx) xxx-xxxx or xxx-xxx-xxxx");
        return false;
    }
    return true;
}

function validatePassword(sField, sFieldName) {
    sPwd = sField.value;
    if (sPwd.length < 5) {
        alert("Please enter a password with at least " + iMinPWDLength + " characters.")
        return false;
    }
    return true;
}

function validateEmailReferralForm() {

    var bEmailValid = false;

    var bFriendNameValid = false;
    var bFriendEmailValid = false;

    var iEntriesToProcess = 0;
    var iNumberOfEmailsSent = 0;

    // Validate sender name and email
    var txtSenderName = document.getElementById("txtSenderName");
    if (txtSenderName.value == null || txtSenderName.value == "") {
        alert("Please enter Name in the required field.");
        txtSenderName.focus();
        return false;
    }
    else {
        if (!validateName(txtSenderName, "Name")) {
            txtSenderName.focus();
            return false;
        }
    }


    var txtSenderEmail = document.getElementById("txtSenderEmail");
    if (txtSenderEmail.value == null || txtSenderEmail.value == "") {
        alert("Please enter your Email Address in the required field.");
        txtSenderEmail.focus();
        return false
    }
    else {
        if (!validateEmail(txtSenderEmail, "Email")) {
            txtSenderEmail.focus();
            return false;
        }
    }

    var sval = true;
    var sEmpty = false;
    var sAllEmpty = true;
    var txtFriendName;
    var txtFriendEmail;


    for (var i = 1; i <= 3; i++) {
        sval = false;
        if ((document.getElementById("txtFriendsName_" + i).value != "") || (document.getElementById("txtFriendsEmail_" + i).value != "")) {
            sAllEmpty = false;
            sval = true;
            break;
        }
    }

    if (!sAllEmpty) {
        for (i = 1; i <= 3; i++) {
            if ((document.getElementById("txtFriendsName_" + i).value != "") || (document.getElementById("txtFriendsEmail_" + i).value != "")) {
                if ((document.getElementById("txtFriendsName_" + i).value == "") || (document.getElementById("txtFriendsEmail_" + i).value == "")) {
                    sEmpty = true;
                    sval = false;
                }
            }
        }
    }

    if (sAllEmpty)
        alert("Please enter at least one Friend's Name and E-mail Address");
    if (sEmpty)
        alert("Please Enter both Friend's Name and E-mail Address");


    if (sval) {
        for (e = 1; e <= 3; e++) {
            sNameField = document.getElementById("txtFriendsName_" + e);
            sEmailField = document.getElementById("txtFriendsEmail_" + e);

            if (sNameField.value != "") {
                if (!validateName(sNameField, "Friend's Name")) {
                    sval = false
                    break;
                    return false;
                }
                else {
                    if (sEmailField.value != "") {
                        if (!validateEmail(sEmailField, "Friend's Email")) {
                            sval = false;
                            break;
                            return false;
                        }
                    }
                }
                iEntriesToProcess++;
            }

        }
    }

    if (sval) {
        for (j = 1; j <= 3; j++) {
            var userGUID = document.getElementById("txtUserGUID");

            sNameField = document.getElementById("txtFriendsName_" + j);
            sEmailField = document.getElementById("txtFriendsEmail_" + j);

            if (userGUID.value == null || userGUID.value == "") // This is an anonymous user
                userGUID.value = "11111111-1111-1111-1111-111111111111";

            var objSubmitButton = document.getElementById("btnSendEmailReferrals");
            objSubmitButton.style.visibility = "hidden";

            document.getElementById("txtExtendedProperties").value = sNameField.value + "|" + sEmailField.value + "|" + txtSenderName.value;

            var sSentStatus = callWebServices("502");
            if (userGUID.value == "11111111-1111-1111-1111-111111111111")
                userGUID.value = "";

            // Clear the currently completed referral
            document.getElementById("txtFriendsName_" + j).value = "";
            document.getElementById("txtFriendsEmail_" + j).value = "";
        }
        alert("Thank you for referring " + iEntriesToProcess + " of your colleagues to SpinZone.");
        objSubmitButton.style.visibility = "visible";
        return false;

    }
    else {
        objSubmitButton.style.visibility = "visible";
        return false;
    }
}


function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function validateEmailAdressSyntax(sEmailAddress) {

    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
    return emailPattern.test(sEmailAddress);
}

function showEmailReferalsPanel() {

    var objSendReferralsButton = document.getElementById("btnSendEmailReferrals");
    if (objSendReferralsButton == null) {
        alert("Why is the objSendReferralsButton null?");
    }
    else {
        objSendReferralsButton.style.display = "";
    }

    var objEmailReferalsPanel = document.getElementById("divEmailReferalsPanel");
    objEmailReferalsPanel.style.display = "";

    var objdivSocialReferralsPanel = document.getElementById("divSocialReferralsPanel");
    objdivSocialReferralsPanel.style.display = "none";

    document.getElementById("spanShowEmailReferalsPanel").className = "showEmailReferalsPanel-Focused";
    document.getElementById("spanShowSocialReferalsPanel").className = "showSocialReferalsPanel-Unfocused";

    document.getElementById("txtSenderName").focus();
}

function showSocialReferalsPanel() {

    var objSendReferralsButton = document.getElementById("btnSendEmailReferrals");
    if (objSendReferralsButton == null) {
        alert("Why is the objSendReferralsButton null?");
    }
    else {
        objSendReferralsButton.style.display = "none";
    }

    var objdivSocialReferralsPanel = document.getElementById("divSocialReferralsPanel");
    objdivSocialReferralsPanel.style.display = "";

    var objEmailReferalsPanel = document.getElementById("divEmailReferalsPanel");
    objEmailReferalsPanel.style.display = "none";

    document.getElementById("spanShowEmailReferalsPanel").className = "showEmailReferalsPanel-Unfocused";
    document.getElementById("spanShowSocialReferalsPanel").className = "showSocialReferalsPanel-Focused";
}

function setAudioMuteFromCookie() {
    if (document.cookie.indexOf('audioMuteState=') > -1) {
        var sAudioMuteStateCookie = document.cookie.split('audioMuteState='); // = "audioMuteState=" + sAudioToggleState + ",path=/";
        sAudioMuteStateCookie = sAudioMuteStateCookie[1].split(',path');
        sAudioToggleState = sAudioMuteStateCookie[0];
    }
    else {
        sAudioToggleState = "Mute";
    }
}

function hiliteObject(button) {
    var currCSSClass = button.className.split('-');
    var currCSSClassMode = currCSSClass[1];
    var newCSSClassName = currCSSClass[0];

    switch (currCSSClassMode) {
        case "Off":
            newCSSClassName += "-On";
            break;

        case "On":
            newCSSClassName += "-Off";
            break;
    }
    button.className = newCSSClassName;
}

function getCookie(cookieKey) {
    // first we'll split this cookie up into name/value pairs
    // note: document.cookie only returns name=value, not the other components
    var a_all_cookies = document.cookie.split(';');
    var a_temp_cookie = '';
    var cookie_name = '';
    var cookie_value = '';
    var b_cookie_found = false; // set boolean t/f default f

    for (i = 0; i < a_all_cookies.length; i++) {
        // now we'll split apart each name=value pair
        a_temp_cookie = a_all_cookies[i].split('=');


        // and trim left/right whitespace while we're at it
        cookie_name = a_temp_cookie[0].replace(/^\s+|\s+$/g, '');

        // if the extracted name matches passed check_name
        if (cookie_name == cookieKey) {
            b_cookie_found = true;
            // we need to handle case where cookie has no value but exists (no = sign, that is):
            if (a_temp_cookie.length > 1) {
                cookie_value = unescape(a_temp_cookie[1].replace(/^\s+|\s+$/g, ''));
            }
            // note that in cases where cookie is initialized but no value, null is returned
            return cookie_value;
            break;
        }
        a_temp_cookie = null;
        cookie_name = '';
    }
    if (!b_cookie_found) {
        return null;
    }
}

function setCookie(cookieKey, cookieValue, expires, path, domain, secure) {
    // set time, it's in milliseconds
    var today = new Date();
    today.setTime(today.getTime());

    /*
    if the expires variable is set, make the correct
    expires time, the current script below will set
    it for x number of days, to make it for hours,
    delete * 24, for minutes, delete * 60 * 24
    */
    if (expires) {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date(today.getTime() + (expires));

    document.cookie = cookieKey + "=" + escape(cookieValue) +
    ((expires) ? ";expires=" + expires_date.toGMTString() : "") +
    ((path) ? ";path=" + path : "") +
    ((domain) ? ";domain=" + domain : "") +
    ((secure) ? ";secure" : "");
}

function turnOnAudio(bEnable) {

    if (bEnable)
        Set_Cookie("ismute", "0");
    else
        Set_Cookie("ismute", "1");

    // ToDo: Fix this
    //setAudioMuteFromCookie();

    var objAudioOn = document.getElementById("btnAudioOn");
    var objAudioOff = document.getElementById("btnAudioOff");

    if (objAudioOn != null && objAudioOff != null) {

        switch (bEnable) {
            case true:
                document.getElementById("btnAudioOn").style.visibility = "hidden";
                document.getElementById("btnAudioOff").style.visibility = "";
                unmute();
                break;

            case false:
                document.getElementById("btnAudioOn").style.visibility = "";
                document.getElementById("btnAudioOff").style.visibility = "hidden";
                mute();
                break;

            default:
                document.getElementById("btnAudioOn").style.visibility = "";
                document.getElementById("btnAudioOff").style.visibility = "hidden";
                mute();
                break;
        }
    }
}

function mute() {
    //thisMovie("spinzone_flash").mute();
    for (x in AudioConfig) {
        player = AudioConfig[x].playerid;
        if (document.getElementById(player))
            document.getElementById(player).SetVariable("player:jsVolume", 0);
    }
}

function unmute() {
    //thisMovie("spinzone_flash").unmute();
    for (x in AudioConfig) {
        player = AudioConfig[x].playerid;
        if (document.getElementById(player))
            document.getElementById(player).SetVariable("player:jsVolume", 100);
    }
}

function retrievePrizeCollection() {
	//alert("retrievePrizeCollection");
    prizes += "<campaign>";
    prizes += "<URLDetails audioPreSpinURL='" + audioPreSpinURL + "' audioSpinURL='" + audioSpinURL + "' audioLoseURL='" + audioLoseURL + "' audioWinURL='" + audioWinURL + "'></URLDetails>";

    var divPrizeCollection = null;
    divPrizeCollection = document.getElementById("divPrizeCollection");

    if (divPrizeCollection) {
        // Mozilla
        if (window.DOMParser) {
            prizes += document.getElementById("divPrizeCollection").textContent;
        }

        // IE
        else {
            prizes += document.getElementById("divPrizeCollection").innerText;
        }
    }

    prizes += "</campaign>";
	//alert("prizes before escape: " + prizes);
    prizes = escape(prizes);
}

function flashLoaded() {
    sendPrizes();
    //turnOnAudio();
}

function loadSound(type) {
    so = new SWFObject(player_mp3_maxiUrl, AudioConfig[type].playerid, "1", "1", "9");
    so.addParam("allowFullScreen", "true");
    so.addParam("autoPlay", "true");
    so.addParam("menu", "false");
    so.addParam("wmode", "transparent");
    so.addVariable("mp3", AudioConfig[type].mp3);
    so.addVariable("autoPlay", "true");
    so.write(AudioConfig[type].container);
}

function playSound(type) {    
    
    //var player = "";
    //for (x in AudioConfig) {
    //    player = AudioConfig[x].playerid;
    //    if(document.getElementById(player))
    //        document.getElementById(player).SetVariable("player:jsStop", "");
    //}
    //var played = AudioConfig[type].playerid;
    //document.getElementById(played).SetVariable("player:jsPlay", "");
    //if (Get_Cookie("ismute") == "0")
	//	document.getElementById(played).SetVariable("player:jsVolume", 100);        
    //else
    //    document.getElementById(played).SetVariable("player:jsVolume", 0);
}

function thisMovie(movieName) {
    return document[movieName];
}

function sendToActionScript(value) {
    thisMovie("spinzone_flash").sendToActionScript(value);
}

function sendPrizes() {
    //alert("sendPrizes");
	iNumberOfSpinReels = document.getElementById("txtSpinReelsNumber").value;
    //thisMovie("spinzone_flash").sendPrizes(prizes, iNumberOfSpinReels);
	processPrizes(prizes);
}

function showSpinZone() {
    thisMovie("spinzone_flash").showSpinZone();
}

function continueSpin() {
    document.getElementById("spinzone_flash").style.visibility = "";
    document.getElementById("pnlSpinButton").style.visibility = "";
    document.getElementById("pnlAudioOn").style.visibility = "";
    document.getElementById("btnContinue").style.display = "none";
    document.getElementById("lblSystemMessage_Text").innerHTML
}


function showSystemTextMsg(bShowWidget) {
    alert("bShowCreditEmpty - " + bShowWidget);
    if (bShowWidget) {
        document.getElementById("pnlSpinWidget").style.visibility = "";
        document.getElementById("pnlCreditMsg").style.visibility = "hidden";
    }
    else {
        document.getElementById("pnlSpinWidget").style.visibility = "hidden";
        document.getElementById("pnlCreditMsg").style.visibility = "";
    }
}

function showSpinResult() {

    var sResultSummary = "";
    var sResultDetails = "";
    var iSpinCreditHistory = parseInt(document.getElementById("txtSpinCreditHistory").value);
    var objFirstName = "";// document.getElementById("txtUserFirstName").value;

    switch (sSpinResult) {
        case "SPIN_ZONE_LOSS":
            sResultDetails = SpinZone_Message_Loss;
            /*
            sResultDetails = "<span style='color: #fc6901; font-weight: bold'>Sorry " + objFirstName + ", unfortunately you did not win.</span>";
            sResultDetails += "<br />";
            sResultDetails += "<span style='color: #000000; font-weight: normal;'>Win or lose, you are already entered into the  Hawaii Dream sweepstakes drawing for a chance to win the Grand Prize.</span>";
            */

            sResultsAudioURLIndex = "2";
            playSound("LOSE");
            break;

        case "SPIN_ZONE_PREV_WIN":
            sResultDetails += SpinZone_Message_Pre_Win;
            //sResultDetails += "<b>You have exceeded the maximum number of wins (" + document.getElementById("txtMaxWinsPerUser").value + ") allowed per campaign.</b>";
            /*
            if (bLockOutWinners)
            {
            }
            else {
                sResultDetails += "<br /><span style='color: #000000; font-weight: normal;'>You are also entered into the SpinZone&#0153 sweepstakes drawing for a chance to win the Grand Prize.</span>";
            }
            */
            sResultsAudioURLIndex = "2";
            playSound("WIN");
            break;

        case "SPIN_ZONE_WIN":
            sResultDetails = SpinZone_Message_Win.replace("%PrizeName%", sPrizeName);
            //sResultDetails += "<br />";
            //sResultDetails += "<span style='color: #000000; font-weight: normal;'>Plus you have been entered into the SpinZone&#0153 sweepstakes drawing for a chance to win the Grand Prize.</span>";

            sResultsAudioURLIndex = "2";
            playSound("WIN");
            break;

        default:
            sResultDetails = sSpinResult;
            sResultsAudioURLIndex = "2";
            break;
    }
    document.getElementById("lblSystemMessage_Text").innerHTML = sResultDetails;

    // Check user balance here
    var iSpinCreditsLeft = txtSpinCreditHistory.value;
    if (iSpinCreditsLeft > 0) {
        document.getElementById("btnSpinTheWheel").style.display = "";
        //$("#btnSpinTheWheel").removeAttr("disabled");
    }
}

function URLDecode(psEncodeString) {
    // Create a regular expression to search all +s in the string
    var lsRegExp = /\+/g;
    // Return the decoded string
    return unescape(String(psEncodeString).replace(lsRegExp, " "));
}

function startSpin() {
	Set_Cookie("sp","1");

	//thisMovie("spinzone_flash").startSpin();
	
	startAnimation0();
	playSound("SPIN");

    callWebServices("50", "");

    var spinResultImages = null;
    var iNumberOfAvailablePrizes = 15;
    var sPrizeIndexes = null;
    var xmlPrizeCollectionDoc = null;
	
    switch (sSpinResult) {
        case "SPIN_ZONE_LOSS":
            sPrizeIndexes = getSpinPrizesLoss();
            break;

        case "SPIN_ZONE_PREV_WIN":
            sPrizeIndexes = getSpinPrizesLoss();
            break;

        case "SPIN_ZONE_WIN":
            var i = 0;
            var xmlDoc = null;
			
			for (var i = 0; i < prizeImgArr.length; i++)
			{
				if( prizeImgArr[i][3] == sPrizeName )
				{
					sPrizeIndexes = i + "|" + i + "|" + i;
					break;
				}
			}
			//alert("sPrizeIndexes: " + sPrizeIndexes);
			break;
			

        default:
            sPrizeIndexes = getSpinPrizesLoss();
            break;
    }

    sResultsAudioURLIndex = 0;
    setTimeout(function () {
        showSpinResult();
        $("#btnContinue").show();
		$("#btnContinueDiv").show();
    }, 5000);    
    UpdateSpin();
    stopSpin(sPrizeIndexes, iSpinTime, sResultsAudioURLIndex);
}

function stopSpin(spinResult, iSpinTime, sResultsAudioURLIndex) {
    //thisMovie("spinzone_flash").stopSpin(spinResult, iSpinTime, sResultsAudioURLIndex);
	setPrizes( spinResult.split("|") );
}

function callCompleted()
{
	alert("callCompleted");	
}

function callWebServices(sCallType, sExtendedPropoerties) {

    if (document.getElementById("txtUserGUID") != null) {

        sClientGUID = document.getElementById("txtUserGUID").value;
        iSiteID = document.getElementById("txtSiteID").value;
        iCampaignID = document.getElementById("txtCampaignID").value;
        
        sURL = WebserviceURL; //document.getElementById("txtServicesURL").value;
        sURL += "/wsOpusOne_SOAGateWay";

        sURL += "?bDebug=false";
        sURL += "&sTenantGUID=";
        sURL += "&sSiteGUID=";
        sURL += "&sCampaignGUID=";
        sURL += "&iSiteID=" + iSiteID;
        sURL += "&iCampaignID=" + iCampaignID;
        sURL += "&iSourceID=0";
        sURL += "&iCallType=" + sCallType;
        sURL += "&sNamePrefix=";
        sURL += "&sFirstName=";
        sURL += "&sMiddleName=";
        sURL += "&sLastName=";
        sURL += "&sNameSuffix=";
        sURL += "&sOver18=";
        sURL += "&sGender=";
        sURL += "&sAge=";
        sURL += "&sDateOfBirth=";
        sURL += "&sAddress1=";
        sURL += "&sAddress2=";
        sURL += "&sCity=";
        sURL += "&sState=";
        sURL += "&sZipCode=";
        sURL += "&sCountry=";
        sURL += "&sTimeZone=";
        sURL += "&sCallerID=";
        sURL += "&sPhoneHome=";
        sURL += "&sPhoneWork=";
        sURL += "&sPhoneMobile=";
        sURL += "&sPhoneFax=";
        sURL += "&sScreenName=";
        sURL += "&sUserGUID=" + sClientGUID;
        sURL += "&sEmailAddress=";
        sURL += "&sPwd=";
        sURL += "&sSecretQuestion1=";
        sURL += "&sSecretAnswer1=";
        sURL += "&sClientIP=" + sClientIP;

        var sExtendedProperties = "";
        sExtendedProperties = document.getElementById("txtExtendedProperties").value;

        sURL += "&sExtendedProperties=" + sExtendedProperties;
        sURL += "&sNewsletterOptIn=";

        //alert(sURL);

        var proxyURL = "fbproxy.aspx?rURL=" + encodeURIComponent(sURL);

        if (sCallType == "50") {
            if (document.getElementById("lblSystemMessage_Text") != null)
                document.getElementById("lblSystemMessage_Text").innerHTML = "<br /><b>Spin in Progress...</b>";
        }

        if (document.getElementById("btnSpinTheWheel") != null)
        {
            document.getElementById("btnSpinTheWheel").style.display = "none";
            //$("#btnSpinTheWheel").attr("disabled", "disabled");
        }

        txtSpinCreditHistory = document.getElementById("txtSpinCreditHistory");
        txtSweepsPointsHistory = document.getElementById("txtSweepsPointsHistory");
        txtSpinCost = document.getElementById("txtSpinCost");

        var browserCodeName = navigator.appCodeName;
        var browserName = navigator.appName;
        var browserVersion = navigator.appVersion;
        var browserCookiesEnabled = navigator.cookieEnabled;
        var browserPlatform = navigator.platform;
        var browserUserAgent = navigator.userAgent;

        // Browser detection and handling
        //if (window.DOMParser) {
        //var responseText = $.ajax({
        //    type: "GET",
        //    url: proxyURL,            
        //    async: false,
		//	onComplete: callCompleted
        //}).responseText;

        var responseText = "";

        var objRe = null;

        try {
            //objRe = eval("(" + responseText + ")");

            //objRe = {
            //    sSpinResult: "SPIN_ZONE_LOSS",
            //    MaxWinsPerUser: 0,
            //    bLockOutWinners: 0
            //};

            $.ajax({
                type: "POST",
                url: urlMakeSpin,
                async: false,
                success: function (data) {
                    objRe = data;
                }
            });
            
            switch (objRe.sSpinResult.toString()) {
                case "1":
                    objRe.sSpinResult = "SPIN_ZONE_WIN";
                    break;
                default:
                    objRe.sSpinResult = "SPIN_ZONE_LOSS";
                    objRe.AwardID = 0;
                    objRe.sPrizeName = "";
                    objRe.PrizeDesc = "";
                    objRe.MaxWinsPerUser = 0;
                    objRe.bLockOutWinners = 0;
                    break;
                //case -1:
                //    objRe.sSpinResult = "SPIN_ZONE_LOSS";
                //    break;
            }

            //objRe = {
            //    sSpinResult : "SPIN_ZONE_LOSS",
            //    MaxWinsPerUser : 0,
            //    bLockOutWinners : 0
            //};
            
            //objRe = {
            //    sSpinResult: "SPIN_ZONE_WIN",
            //    AwardID: 0,
            //    PrizeID: 1,
            //    sPrizeName: "PrizeName 1",
            //    PrizeDesc: "http://new.cfdevs.com/spinzone/sites/SiteID_1/Campaigns/CampaignID_10/Images/Prizes/Spinner-Prizes/iPodShuffle.png",
            //    MaxWinsPerUser: 0,
            //    bLockOutWinners: 0
            //};
            
        } catch (err) {
            objRe = {
                sSpinResult : "SPIN_ZONE_LOSS",
                 MaxWinsPerUser : 0,
                 bLockOutWinners : 0
            };
        }




//            try {
//                // This is standard Mozilla
//                xmlDoc = document.implementation.createDocument("", "", null);
//                xmlDoc.async = false;
//                xmlDoc.load(proxyURL);
//            }
//            catch (Error) {
//                // This is IE (IE9 now is reporting as Mozilla codename as well as Mozilla user agent.
//                xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
//                xmlDoc.async = "false";
//                xmlDoc.load(proxyURL);
//            }

            if (sCallType != "502") {

                sSpinResult = objRe.sSpinResult;

                // If we have a winner, get the prize data
                if (sSpinResult == "SPIN_ZONE_WIN") {
                    AwardID = objRe.AwardID;
                    PrizeID = objRe.PrizeID;
                    sPrizeName = objRe.sPrizeName;

                    PrizeDesc = objRe.PrizeDesc;
                    PrizeURL = objRe.PrizeDesc;
                    MaxWinsPerUser = objRe.MaxWinsPerUser;
                    bLockOutWinners = objRe.MaxWinsPerUser;
                }
                else {
                    MaxWinsPerUser = objRe.MaxWinsPerUser;
                    bLockOutWinners = objRe.MaxWinsPerUser;
                }                

                sTransactionResult = objRe.MaxWinsPerUser;
                sTransactionType = objRe.MaxWinsPerUser;
                sTransactionValueType = objRe.MaxWinsPerUser;
                flTransactionValueAmount = objRe.MaxWinsPerUser;

//                var Balances = sTransactionHistory(0).childNodes(0).childNodes;
//                for (var i = 0; i < Balances.length; i++) {

//                    sValueType = Balances(i).getAttribute("UnitType");
//                    sValueAmount = addCommas(trimFloatValue(parseFloat(Balances(i).getAttribute("Value"))));

//                    setCookie(sValueType, sValueAmount, iCookieDuration, "/", "", false);
//                }
                

                if (bLockOutWinners == "1")
                    bLockOutWinners = true;
                else
                    bLockOutWinners = false;

                txtSpinCreditHistory.value = parseInt(txtSpinCreditHistory.value) - 1;
            }
            // This is an email referral call
            else {
                var sShareID = "Sent";
                return sShareID;
            }
        //}

        // Get references to the header social share icons to update thier hrefs with the corrected user spincredits
        var oFaceBook = document.getElementById("linkFaceBook");
        if (oFaceBook != null) {

            var currentHref = oFaceBook.href;
            var arrHref = currentHref.split("&ub=");

            var iCreditsLeft = txtSpinCreditHistory.value;
            var sNewURL = arrHref[0] + "&ub=" + iCreditsLeft;

            oFaceBook.href = sNewURL;
        }

        var oTwitter = document.getElementById("linkTwitter");
        if (oTwitter != null) {

            var currentHref = oTwitter.href;
            var arrHref = currentHref.split("&ub=");

            var iCreditsLeft = txtSpinCreditHistory.value;
            var sNewURL = arrHref[0] + "&ub=" + iCreditsLeft;

            oTwitter.href = sNewURL;
        }
    }

    function trimFloatValue(floatValue) {
        // Trimming the Float value to two decimal points
        var iTemp = parseInt((floatValue * 100).toString()) / 100;
        iTemp = parseInt(floatValue);
        return iTemp;
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
}

/**
* SWFObject v1.5.1: Flash Player detection and embed - http://blog.deconcept.com/swfobject/
*
* SWFObject is (c) 2007 Geoff Stearns and is released under the MIT License:
* http://www.opensource.org/licenses/mit-license.php
*
*/
if (typeof deconcept == "undefined") var deconcept = {};
if (typeof deconcept.util == "undefined") deconcept.util = {};
if (typeof deconcept.SWFObjectUtil == "undefined") deconcept.SWFObjectUtil = {};
deconcept.SWFObject = function (swf, id, w, h, ver, c, quality, xiRedirectUrl, redirectUrl, detectKey) {
    if (!document.getElementById) { return; }
    this.DETECT_KEY = detectKey ? detectKey : 'detectflash';
    this.skipDetect = deconcept.util.getRequestParameter(this.DETECT_KEY);
    this.params = {};
    this.variables = {};
    this.attributes = [];
    if (swf) { this.setAttribute('swf', swf); }
    if (id) { this.setAttribute('id', id); }
    if (w) { this.setAttribute('width', w); }
    if (h) { this.setAttribute('height', h); }
    if (ver) { this.setAttribute('version', new deconcept.PlayerVersion(ver.toString().split("."))); }
    this.installedVer = deconcept.SWFObjectUtil.getPlayerVersion();
    if (!window.opera && document.all && this.installedVer.major > 7) {
        // only add the onunload cleanup if the Flash Player version supports External Interface and we are in IE
        // fixes bug in some fp9 versions see http://blog.deconcept.com/2006/07/28/swfobject-143-released/
        if (!deconcept.unloadSet) {
            deconcept.SWFObjectUtil.prepUnload = function () {
                __flash_unloadHandler = function () { };
                __flash_savedUnloadHandler = function () { };
                window.attachEvent("onunload", deconcept.SWFObjectUtil.cleanupSWFs);
            }
            window.attachEvent("onbeforeunload", deconcept.SWFObjectUtil.prepUnload);
            deconcept.unloadSet = true;
        }
    }
    if (c) { this.addParam('bgcolor', c); }
    var q = quality ? quality : 'high';
    this.addParam('quality', q);
    this.setAttribute('useExpressInstall', false);
    this.setAttribute('doExpressInstall', false);
    var xir = (xiRedirectUrl) ? xiRedirectUrl : window.location;
    this.setAttribute('xiRedirectUrl', xir);
    this.setAttribute('redirectUrl', '');
    if (redirectUrl) { this.setAttribute('redirectUrl', redirectUrl); }
}
deconcept.SWFObject.prototype = {
    useExpressInstall: function (path) {
        this.xiSWFPath = !path ? "expressinstall.swf" : path;
        this.setAttribute('useExpressInstall', true);
    },
    setAttribute: function (name, value) {
        this.attributes[name] = value;
    },
    getAttribute: function (name) {
        return this.attributes[name] || "";
    },
    addParam: function (name, value) {
        this.params[name] = value;
    },
    getParams: function () {
        return this.params;
    },
    addVariable: function (name, value) {
        this.variables[name] = value;
    },
    getVariable: function (name) {
        return this.variables[name] || "";
    },
    getVariables: function () {
        return this.variables;
    },
    getVariablePairs: function () {
        var variablePairs = [];
        var key;
        var variables = this.getVariables();
        for (key in variables) {
            variablePairs[variablePairs.length] = key + "=" + variables[key];
        }
        return variablePairs;
    },
    getSWFHTML: function () {
        var swfNode = "";
        if (navigator.plugins && navigator.mimeTypes && navigator.mimeTypes.length) { // netscape plugin architecture
            if (this.getAttribute("doExpressInstall")) {
                this.addVariable("MMplayerType", "PlugIn");
                this.setAttribute('swf', this.xiSWFPath);
            }
            swfNode = '<embed type="application/x-shockwave-flash" src="' + this.getAttribute('swf') + '" width="' + this.getAttribute('width') + '" height="' + this.getAttribute('height') + '" style="' + (this.getAttribute('style') || "") + '"';
            swfNode += ' id="' + this.getAttribute('id') + '" name="' + this.getAttribute('id') + '" ';
            var params = this.getParams();
            for (var key in params) { swfNode += [key] + '="' + params[key] + '" '; }
            var pairs = this.getVariablePairs().join("&");
            if (pairs.length > 0) { swfNode += 'flashvars="' + pairs + '"'; }
            swfNode += '/>';
        } else { // PC IE
            if (this.getAttribute("doExpressInstall")) {
                this.addVariable("MMplayerType", "ActiveX");
                this.setAttribute('swf', this.xiSWFPath);
            }
            swfNode = '<object id="' + this.getAttribute('id') + '" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + this.getAttribute('width') + '" height="' + this.getAttribute('height') + '" style="' + (this.getAttribute('style') || "") + '">';
            swfNode += '<param name="movie" value="' + this.getAttribute('swf') + '" />';
            var params = this.getParams();
            for (var key in params) {
                swfNode += '<param name="' + key + '" value="' + params[key] + '" />';
            }
            var pairs = this.getVariablePairs().join("&");
            if (pairs.length > 0) { swfNode += '<param name="flashvars" value="' + pairs + '" />'; }
            swfNode += "</object>";
        }
        return swfNode;
    },
    write: function (elementId) {
        if (this.getAttribute('useExpressInstall')) {
            // check to see if we need to do an express install
            var expressInstallReqVer = new deconcept.PlayerVersion([6, 0, 65]);
            if (this.installedVer.versionIsValid(expressInstallReqVer) && !this.installedVer.versionIsValid(this.getAttribute('version'))) {
                this.setAttribute('doExpressInstall', true);
                this.addVariable("MMredirectURL", escape(this.getAttribute('xiRedirectUrl')));
                document.title = document.title.slice(0, 47) + " - Flash Player Installation";
                this.addVariable("MMdoctitle", document.title);
            }
        }
        if (this.skipDetect || this.getAttribute('doExpressInstall') || this.installedVer.versionIsValid(this.getAttribute('version'))) {
            var n = (typeof elementId == 'string') ? document.getElementById(elementId) : elementId;
            n.innerHTML = this.getSWFHTML();
            return true;
        } else {
            if (this.getAttribute('redirectUrl') != "") {
                document.location.replace(this.getAttribute('redirectUrl'));
            }
        }
        return false;
    }
}

/* ---- detection functions ---- */
deconcept.SWFObjectUtil.getPlayerVersion = function () {
    var PlayerVersion = new deconcept.PlayerVersion([0, 0, 0]);
    if (navigator.plugins && navigator.mimeTypes.length) {
        var x = navigator.plugins["Shockwave Flash"];
        if (x && x.description) {
            PlayerVersion = new deconcept.PlayerVersion(x.description.replace(/([a-zA-Z]|\s)+/, "").replace(/(\s+r|\s+b[0-9]+)/, ".").split("."));
        }
    } else if (navigator.userAgent && navigator.userAgent.indexOf("Windows CE") >= 0) { // if Windows CE
        var axo = 1;
        var counter = 3;
        while (axo) {
            try {
                counter++;
                axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash." + counter);
                //				document.write("player v: "+ counter);
                PlayerVersion = new deconcept.PlayerVersion([counter, 0, 0]);
            } catch (e) {
                axo = null;
            }
        }
    } else { // Win IE (non mobile)
        // do minor version lookup in IE, but avoid fp6 crashing issues
        // see http://blog.deconcept.com/2006/01/11/getvariable-setvariable-crash-internet-explorer-flash-6/
        try {
            var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.7");
        } catch (e) {
            try {
                var axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash.6");
                PlayerVersion = new deconcept.PlayerVersion([6, 0, 21]);
                axo.AllowScriptAccess = "always"; // error if player version < 6.0.47 (thanks to Michael Williams @ Adobe for this code)
            } catch (e) {
                if (PlayerVersion.major == 6) {
                    return PlayerVersion;
                }
            }
            try {
                axo = new ActiveXObject("ShockwaveFlash.ShockwaveFlash");
            } catch (e) { }
        }
        if (axo != null) {
            PlayerVersion = new deconcept.PlayerVersion(axo.GetVariable("$version").split(" ")[1].split(","));
        }
    }
    return PlayerVersion;
}
deconcept.PlayerVersion = function (arrVersion) {
    this.major = arrVersion[0] != null ? parseInt(arrVersion[0]) : 0;
    this.minor = arrVersion[1] != null ? parseInt(arrVersion[1]) : 0;
    this.rev = arrVersion[2] != null ? parseInt(arrVersion[2]) : 0;
}
deconcept.PlayerVersion.prototype.versionIsValid = function (fv) {
    if (this.major < fv.major) return false;
    if (this.major > fv.major) return true;
    if (this.minor < fv.minor) return false;
    if (this.minor > fv.minor) return true;
    if (this.rev < fv.rev) return false;
    return true;
}
/* ---- get value of query string param ---- */
deconcept.util = {
    getRequestParameter: function (param) {
        var q = document.location.search || document.location.hash;
        if (param == null) { return q; }
        if (q) {
            var pairs = q.substring(1).split("&");
            for (var i = 0; i < pairs.length; i++) {
                if (pairs[i].substring(0, pairs[i].indexOf("=")) == param) {
                    return pairs[i].substring((pairs[i].indexOf("=") + 1));
                }
            }
        }
        return "";
    }
}

function Set_Cookie(name, value, expires, path, domain, secure) {
    // set time, it's in milliseconds
    var today = new Date();
    today.setTime(today.getTime());

    /*
	if the expires variable is set, make the correct 
	expires time, the current script below will set 
	it for x number of days, to make it for hours, 
	delete * 24, for minutes, delete * 60 * 24
	*/
    if (expires) {
        expires = expires * 1000 * 60 * 60 * 24;
    }
    var expires_date = new Date(today.getTime() + (expires));

    document.cookie = name + "=" + escape(value) +
	((expires) ? ";expires=" + expires_date.toGMTString() : "") +
	((path) ? ";path=" + path : "") +
	((domain) ? ";domain=" + domain : "") +
	((secure) ? ";secure" : "");
}

/* fix for video streaming bug */
deconcept.SWFObjectUtil.cleanupSWFs = function () {
    var objects = document.getElementsByTagName("OBJECT");
    for (var i = objects.length - 1; i >= 0; i--) {
        objects[i].style.display = 'none';
        for (var x in objects[i]) {
            if (typeof objects[i][x] == 'function') {
                objects[i][x] = function () { };
            }
        }
    }
}
/* add document.getElementById if needed (mobile IE < 5) */
if (!document.getElementById && document.all) { document.getElementById = function (id) { return document.all[id]; } }

/* add some aliases for ease of use/backwards compatibility */
var getQueryParamValue = deconcept.util.getRequestParameter;
var FlashObject = deconcept.SWFObject; // for legacy support
var SWFObject = deconcept.SWFObject;
