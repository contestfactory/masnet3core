﻿// need config when render Layout
//var _config = {
//    pageName: "contest/details",
//    queryParams: {
//        isWinner: false, // Request["IsSweepstakesWinner"] == "1"
//        isMySubmission: false // Request["showshare"] == "1"
//    },

//    deviceType: isMobileDevice.Any() ? "mobile" : "non-mobile",
//    userData: {
//        memberID: "",
//        zipCode: "",
//        state: ""
//    },

//    errorData: {
//        errorcode: "",
//        errormessage: ""
//    }
//}


// tracking helper object
var aarpTrackingObj = {
    getVar3: function (pageName){
        var result = "";
        switch (pageName) {
            case "contestRule": result = "rules"; break;
            case "contest/details": result = "details"; break;
            case "contest/submit": result = "submit"; break;
            case "publishedcontest/contestantentrydetail": result = "contest-entry"; break;
            case "contest/match": result = "match"; break;
            case "contestant/details": result = "contestant"; break;            
            default: break;
        }
        return result;
    },

    getVar4: function (pageName, params){
        var result = "";
        switch(pageName){            
            case "contest/match": result  = params.isWinner == "1" ? "sweepstakes-winner" : ""; break;
            case "contestant/details": result = params.isMySubmission == "1" ? "submissions" : "details"; break;
            default: break;
        }
        return result;
    }
}


/* Define data layer */
var AARP = {
    MetaInfo: {
        siteProperty: "", // required
        pagedata:{
            language: "en-us",
            accounttype: "contests",
            var1: "25days-of-heroes",
            var2: "contest",
            var3: aarpTrackingObj.getVar3(aarpTrackingConfig.pageName),
            var4: aarpTrackingObj.getVar4(aarpTrackingConfig.pageName, aarpTrackingConfig.queryParams),
            pagetype: "25daysofheroes.com",
            channel: "sweepstakes",
            eventtype: "event8"
        },
        userdata:{
            konnexid: aarpTrackingConfig.userData.memberID, //member ID
            zipcode: aarpTrackingConfig.userData.zipCode,
            state: aarpTrackingConfig.userData.state,
            devicetype: aarpTrackingConfig.deviceType
        },
        errors: aarpTrackingConfig.errorData
    }
}
