﻿var curConnection = null,
        curMatch = null;
var instance = null;

var DiagramRoundWidth = 200;
var DiagramMatchHeight = 85;
var zoom = 1;

var leftEndPoints = [[0, 0.3, -1, 0], [0, 0.5, -1, 0], [0, 0.7, -1, 0]],
        rightEndPoints = [[1, 0.3, 1, 0], [1, 0.5, 1, 0], [1, 0.7, 1, 0]];


function repaintDiagram() {
    if (!!instance)
        instance.repaintEverything();
}

function getRoundIndex(MId) {
    return $('#' + MId).parents('.roundctn').index();
}


function getEndPointIndex(str) {
    for (i = 0; i < 3; i++)
        if (rightEndPoints[i] + "" == str)
            return i + 1;
    for (i = 0; i < 3; i++)
        if (leftEndPoints[i] + "" == str)
            return i + 4;
    return 0;
}

function getEndPointName(idx) {
    if (idx >= 1 && idx <= 3)
        return rightEndPoints[idx - 1] + "";
    else if (idx >= 4 && idx <= 6)
        return leftEndPoints[idx - 4] + "";
    else
        return "";
}

// declare connector and connection style

var connectorList = [
    ["Flowchart", { gap: 5, cornerRadius: 5, alwaysRespectStubs: true, midpoint: 0.5 }],
    ["Flowchart", { gap: 5, cornerRadius: 5, alwaysRespectStubs: true, midpoint: 0.5 }],
    ["Bezier", {}],
    ["Straight", { stub: 0, gap: 0 }],
    ["StateMachine", { margin: 5, proximityLimit: 80 }]

]

var connectorType = connectorList[1];

// this is the paint style for the connecting lines..
var connectorPaintStyle = {
    lineWidth: 1,
    strokeStyle: "green",//"#61B7CF",
    //dashstyle: "5",
    joinstyle: "round",
    outlineColor: "white",
    outlineWidth: 2
},
// .. and this is the hover style.
    connectorHoverStyle = {
        lineWidth: 1,
        strokeStyle: "orange",//"#216477",
        outlineWidth: 2,
        outlineColor: "white"
    },
    endpointHoverStyle = {
        fillStyle: "#216477",
        strokeStyle: "#216477",
        radius: 5
    },
// the definition of source endpoints (the small blue ones)
    sourceEndpoint = {
        endpoint: "Dot",
        paintStyle: {
            //strokeStyle: "#7AB02C",
            fillStyle: "green",
            radius: 5,
            //lineWidth: 2
        },
        maxConnections: -1,
        isSource: true,
        connector: connectorType,
        //connector: [ "Bezier", {  } ],
        //connector: [ "Straight", { stub : 0, gap : 0  } ],
        //connector: [ "StateMachine", { margin: 5, proximityLimit : 80 } ],
        connectorStyle: connectorPaintStyle,
        hoverPaintStyle: endpointHoverStyle,
        connectorHoverStyle: connectorHoverStyle,
        dragOptions: {},
        overlays: [
            ["Label", {
                location: [0.5, 1.5],
                label: "",
                cssClass: "endpointSourceLabel"
            }]
        ]
    },
// the definition of target endpoints (will appear when the user drags a connection)
    targetEndpoint = {
        endpoint: "Dot",
        paintStyle: { fillStyle: "green", radius: 5 },
        hoverPaintStyle: endpointHoverStyle,
        maxConnections: -1,
        dropOptions: { hoverClass: "hover", activeClass: "active" },
        isTarget: true,
        overlays: [
            ["Label", { location: [0.5, -0.5], label: "", cssClass: "endpointTargetLabel" }]
        ]
    },
    init = function (connection) {
        //connection.getOverlay("label").setLabel(connection.sourceId.substring(15) + "-" + connection.targetId.substring(15));
        
    };

var basicType = {
    connector: connectorType,
    paintStyle: connectorPaintStyle,
    hoverPaintStyle: connectorHoverStyle,
    overlays: [

    ]
};
var errType = {
    connector: connectorType,
    paintStyle: { strokeStyle: "red", lineWidth: 4 },
    hoverPaintStyle: { strokeStyle: "blue" },
    overlays: [

    ]
};

var DiagramEvent = {
    onEndPointMouseOver: null,
    onEndPointMouseOut: null,
    onSourceEndPointClick: null,
    onTargetEndPointClick: null,
    onConnectionMouseOver: null,
    onConnectionMouseOut: null
};

var _addEndpoints = function (toId, sourceAnchors, targetAnchors) {

    var EndPointEnabled = ($('#' + toId).attr('data-isrematch') != '1');

    for (var i = 0; i < sourceAnchors.length; i++) {
        var sourceUUID = toId + '_' + getEndPointIndex(sourceAnchors[i] + '');
        
        sourceEndpoint.enabled = EndPointEnabled;

        var newSourcePoint = instance.addEndpoint(toId, sourceEndpoint, {
            anchor: sourceAnchors[i], uuid: sourceUUID
        });

        if (EndPointEnabled) {
            newSourcePoint.bind('click', function (endpoint) {
                //console.log('source point click');
                if (DiagramEvent.onSourceEndPointClick != null)
                    DiagramEvent.onSourceEndPointClick(endpoint);
            });

            newSourcePoint.bind('mouseover', function (endpoint, originalEvent) {
                if (DiagramEvent.onEndPointMouseOver != null)
                    DiagramEvent.onEndPointMouseOver(endpoint, originalEvent);
            });

            newSourcePoint.bind('mouseout', function (endpoint, originalEvent) {
                if (DiagramEvent.onEndPointMouseOut != null)
                    DiagramEvent.onEndPointMouseOut(endpoint, originalEvent);
            });
        }
    }
    for (var j = 0; j < targetAnchors.length; j++) {
        var targetUUID = toId + '_' + getEndPointIndex(targetAnchors[j] + '');

        targetEndpoint.enabled = EndPointEnabled;

        var newtargetPoint = instance.addEndpoint(toId, targetEndpoint, { anchor: targetAnchors[j], uuid: targetUUID });        
        if (EndPointEnabled) {
            newtargetPoint.bind('click', function (endpoint) {
                //console.log('target point click');
                if (DiagramEvent.onTargetEndPointClick != null)
                    DiagramEvent.onTargetEndPointClick(endpoint);
            });

            newtargetPoint.bind('mouseover', function (endpoint, originalEvent) {
                if (DiagramEvent.onEndPointMouseOver != null)
                    DiagramEvent.onEndPointMouseOver(endpoint, originalEvent);
            });

            newtargetPoint.bind('mouseout', function (endpoint, originalEvent) {
                if (DiagramEvent.onEndPointMouseOut != null)
                    DiagramEvent.onEndPointMouseOut(endpoint, originalEvent);
            });
        }
    }
};

function getHeighestRound() {
    var heighestRound = 0;
    $('.roundwrapper').each(function () {
        var uiRoundWrapper = $(this);
        if (uiRoundWrapper.height() > heighestRound) {
            heighestRound = uiRoundWrapper.height();
        }
    })

    return heighestRound;
}

function getAtMostMatchRound() {
    var atMostRound = 0;
    $('.roundwrapper').each(function () {
        var uiRoundWrapper = $(this);
        if (uiRoundWrapper.find('.window').length > atMostRound) {
            atMostRound = uiRoundWrapper.find('.window').length;
        }
    })

    return atMostRound;
}

function adjustRoundHeight() {
    var heighestRound = getHeighestRound();
    $('.roundwrapper').height(heighestRound);
}

function adjustMatchesMargin(uiRound) {
    var heighestRound = getHeighestRound();
    var matchHeight = DiagramMatchHeight;
    if ($('.window').length == 0) {
        return;
    }

    //if (uiRound.find('.roundwrapper .window').length == getAtMostMatchRound())
    //    return;
    

    var numofMatches = uiRound.find('.window').length;
    var remainSpace = heighestRound - (numofMatches * matchHeight);
    var matchMargin = remainSpace / numofMatches;

    uiRound.find(".window").css("margin", '0px auto ' + matchMargin + 'px auto');
    uiRound.find(".window:first").css("margin-top", (matchMargin / 2) + 'px');
    uiRound.find(".window:last").css("margin-bottom", (matchMargin / 2) + 'px');
}

function adjustMatchesMarginForAllRound() {
    $('.canvasslider .roundctn').each(function () {
        adjustMatchesMargin($(this));
    })
}


function adjustCanvasslider() {
    var slider = $('#canvas .canvasslider');

    var totalWidth = (slider.find('.roundctn').length * DiagramRoundWidth);

    slider.css("width", totalWidth + 'px');

    var canvas = $('#canvas');
    canvas.css("width", totalWidth + 'px');
}

function initConnector(cType, viewType) {
    //viewType- 1:full; 2:mini
    if (viewType == null)
        viewType = 1;

    connectorType = connectorList[cType];
    if (viewType == 1) {
        switch (cType) {
            case 0:
            case 1:                
                connectorType[1].stub = [5, 5];
                connectorType[1].gap = 2;
                break;
            case 2:
                connectorType[1].curviness = 75;
                connectorType[1].stub = 1;
                break;
            case 3:
                break;
            case 4:
                break;

            default:
        }
    }
    else if (viewType == 2) {
        switch (cType) {
            case 0:
            case 1:
                connectorType[1].gap = 1;
                connectorType[1].stub = [5, 5];
                break;
            case 2:
                connectorType[1].curviness = 15;
                connectorType[1].stub = 1;
                break;
            case 3:
                break;
            case 4:
                break;

            default:
        }
    }


    sourceEndpoint.connector = connectorType;
}

function changeConnector(cType) {
    connectorType = connectorList[cType];
    sourceEndpoint.connector = connectorType;
    var arrConn = instance.getConnections();
    for (var i = 0; i < arrConn.length; i++) {
        arrConn[i].setConnector(connectorType);
        arrConn[i].addOverlay(["Arrow", { location: 1}]);
    }
    
}

function changeEndPointConnector(cType) {
    connectorType = connectorList[cType];
    $('.window').each(function () {
        var uiMatch = $(this);
        for (var i = 0; i < 3; i++) {
            instance.getEndpoints(uiMatch.prop('id'))[i].connector = connectorType;
        }
    })
}

function zoomDiagram(dZoom) {

    zoom = dZoom;

    $('#canvas').css({
        '-moz-transform': 'scale(' + zoom + ')',
        '-moz-transform-origin': 'top left',
        '-o-transform': 'scale(' + zoom + ')',
        '-o-transform-origin': 'top left',
        '-webkit-transform': 'scale(' + zoom + ')',
        '-webkit-transform-origin': 'top left',
        'zoom': zoom
    })
}