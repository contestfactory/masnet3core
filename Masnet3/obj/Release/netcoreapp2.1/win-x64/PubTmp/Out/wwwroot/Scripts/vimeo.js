﻿var Vimeo = {

    ValidateUrl: function (url) {
        url = Vimeo.WrapURL(url);   
        var data = Vimeo.GetVimeoInfo(url);

        if (data == null) {
            return false;
        }

        return true;
    },
    GetVideoID: function (url) {
        url = Vimeo.WrapURL(url);
        var data = Vimeo.GetVimeoInfo(url);

        if (data == null) {
            return "";
        }

        return data.video_id;
    },

    GetVideoUrl: function (id) {
        return 'https://vimeo.com/' + id;
    },

    GetVimeoInfo: function (url) {
        url = Vimeo.WrapURL(url);
        var info = null;
        if (url != null && $.trim(url) != '') {
            $.ajax({
                type: "GET",
                url: "https://vimeo.com/api/oembed.json?url=" + encodeURI(url.toLowerCase()),
                //jsonp: 'callback',
                //dataType: 'jsonp',
                async: false,
                success: function (data) {
                    if (typeof data == 'object' || data instanceof Object) {
                        info = data;
                    }
                    else {
                        info = null;
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    info = null;
                    return info;
                }
            });
        } else
            return null;

        return info;
    },

    GetThumbnail: function (jsonData, width, height) {
        if (jsonData == null) {
            return "";
        }

        var data = jsonData.data;
        if (data == null) {
            data = Vimeo.GetVimeoInfo(jsonData.url);

            if (data == null) {
                return "";
            }
        }
        var thumbnail_url = data.thumbnail_url;
        var lastSlash = thumbnail_url.lastIndexOf("/");
        var thumbnail_directory = thumbnail_url.substring(0, lastSlash);        
        var thumbnail_path = thumbnail_url.substring(lastSlash + 1);        

        var thumbnail_extension = thumbnail_path.substring(thumbnail_path.lastIndexOf("."));
        var thumbnail_id = thumbnail_path.substring(0, thumbnail_path.indexOf("_"));

        if (width != null && width > 0) {
            thumbnail_id += "_" + width;

            if (height != null && height > 0) {
                thumbnail_id += "x" + height;
            }
        }

        return thumbnail_directory + "/" + thumbnail_id + thumbnail_extension;
    },

    GetThumbnailSmall: function (jsonData) {
        return Vimeo.GetThumbnail(jsonData, 100, 75);
    },

    WrapURL: function (url){
        return isURL(url) ? url : "https://" + url;    
    }
}