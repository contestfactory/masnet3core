﻿/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	//config.language = 'fa';
	// config.uiColor = '#AADC6E';
	config.skin = 'v2'; 
	config.enterMode = 2;
	
	config.toolbar_OPUS =
	[
		['Source','-','Preview','Print','-','Bold','Italic','Underline','Subscript','Superscript','SpecialChar','-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','Find','Replace','-','BidiLtr', 'BidiRtl'],
		'/',
		['Font','FontSize','TextColor','BGColor','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Image']
	];
	
	config.toolbar_OPUS2 =
	[
		['Source','-','Preview','Print','-','Bold','Italic','Underline','Subscript','Superscript','SpecialChar'],
		'/',
		['Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo','-','Find','Replace','-','Image']
	];

	config.toolbar_OPUS3 =
    [
        ['Source', '-', 'Preview', 'Print', '-', 'Bold', 'Italic', 'Underline', 'Subscript', 'Superscript', 'SpecialChar', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
		'/',
        ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'BidiLtr', 'BidiRtl'],
        '/',
		['Font', 'FontSize', 'TextColor', 'BGColor', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', 'Image']
    ];


	if(document.location.href.toLowerCase().indexOf("fan_register.asp") > -1)
		config.toolbar = 'OPUS2';
	else
		config.toolbar = 'OPUS';
	config.removePlugins = 'elementspath';
	config.extraPlugins = 'wordcount';
};
