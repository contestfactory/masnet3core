﻿var voteModel = {    
    languages: {}
};


loadLanguages("gVoted,Match_AlreadyVoted", SiteConfig.pageName, voteModel);


/*
** Load vote form and click submit automatically when VoteType is button
*/
var tempConfigs;
function CallVoteButton(configs) {
    /*
    configs:
    1. Button (JQuery)
    2. VoteFormContainer (JQuery)
    3. AutoClick (bool)
    4. onButtonClick
    5: SliderWidth
    6: TieBreak
    */
    var btn = configs.Button;
    var voteform = configs.VoteFormContainer;
    tempConfigs = configs;
    if (btn.attr('data-showPopup') === "1") {
        showDialog({
            title: "Vote",
            modal: true,
            dSize: "modal-sm",
            message: "<div class='text-center'><p>Please confirm your vote.</p><div><input class='btn btn-primary btn-sm' type='button' value='OK' onclick='$(\"\#dvDialog\").dialog(\"\close\"); confirmVote()' />&nbsp;&nbsp;&nbsp;<input class='btn btn-primary btn-sm' type='button' value='Cancel' onclick='$(\"\#dvDialog\").dialog(\"\close\");'/></div></div>"
        });
        return;
    }

    if (btn.attr('data-contestantid') == null) {
        $('#slidercontainervoter').css('visibility', 'visible');
        return;
    }

    $.ajax({
        type: "POST",
        url: SiteURL.Contestant_ContestantVoteForm,
        data: {
            AfterVoting: btn.attr('data-aftervoting'),
            ContestantID: btn.attr('data-contestantid'),
            SliderWidth: configs.SliderWidth,
            TieBreak: configs.TieBreak
        },
        success: function (msg) {            
            voteform.hide();
            voteform.html(msg);

            var votetype = voteform.find('.formconfig > .VoteType').val();
            var voteable = voteform.find('.formconfig > .Voteable').val();
            var hasvoted = voteform.find('.formconfig > .Hasvoted').val();
            var displayresultwhencompleted = voteform.find('.formconfig > .DisplayResultWhenCompleted').val();
            var isMultipleVote = voteform.find('.togglevote').size() > 0;
            //if (SiteConfig.AllowAnonymousVote) {
            //      if ($.cookies("vdate") == new Date().toLocaleDateString())
            //          voteable = false;
            //}

            if (voteable == '1') {
                if (!isMultipleVote && (votetype == '0' || votetype == '1')) {
                    voteform.hide();
                    voteform.find('form').submit();
                }
                else {
                    voteform.show();
                    InitContestantVoteSlider(voteform);
                }
            }
            else {
                btn.hide();                   
                if (hasvoted == '1' || displayresultwhencompleted == '1') {
                    voteform.show();
                    if (window.resizeImages)
                        resizeImages();
                }
                else {
                    voteform.hide();
                }
                if (hasvoted)
                    alert(voteModel.languages.Match_AlreadyVoted);
            }

            if (configs.onButtonClick != null) {
                configs.onButtonClick();
            }

            
            if (configs.callBack) configs.callBack();
            // after init vote holder button, trigger click event 
            if (isMultipleVote) {
                voteform.find('.togglevote').trigger("click");
            }
        }
    });
        
}


/*
** Just check and show vote form
*/

function getVoteText(votetype) {
    var lblVoted = voteModel.languages.gVoted;
    if (SiteConfig.SiteName.indexOf("collette") > -1 && SiteConfig.SiteName.indexOf("phase") == -1)
        lblVoted = votetype == "2" ? "Voted" : "Liked";
    return lblVoted;
}

function CheckVoteForContestant(configs){
    /*
    configs:
    1. Button (JQuery)
    2. VoteFormContainer (JQuery) 
    4. onButtonClick : when vote form is ready
    5: SliderWidth
    6: TieBreak
    */   
    var lblVoted = '';
    var btn = configs.Button;
    var voteform = configs.VoteFormContainer;

    if (btn.attr('data-contestantid') == null) {
        $('#slidercontainervoter').css('visibility','visible');
        return;
    }

    $.ajax({
        type: "POST",
        url: SiteURL.Contestant_ContestantVoteForm,
        data: {
            AfterVoting: btn.attr('data-aftervoting'),
            ContestantID: btn.attr('data-contestantid'),
            SliderWidth: configs.SliderWidth,
            TieBreak: configs.TieBreak
        },
    success: function (msg) {
        voteform.hide();
        voteform.html(msg);        

        var votetype = voteform.find('.formconfig > .VoteType').val();
        var voteable = voteform.find('.formconfig > .Voteable').val();
        var hasvoted = voteform.find('.formconfig > .Hasvoted').val();
        lblVoted = getVoteText(votetype);
        if (window.Contestant != null) {
            Contestant.Votable = voteable;
            Contestant.HasVoted = hasvoted;
            Contestant.VoteType = votetype;
        }

        var displayresultwhencompleted = voteform.find('.formconfig > .DisplayResultWhenCompleted').val();        

        /* Button = 0, ThumbUp = 1, Criterias = 2, ThumbUpAndDown = 3*/
        if (voteable == '1') {
            if (votetype == '0') {
                btn.show();
                voteform.hide();
                btn.unbind();

                if (SiteConfig.SiteName == "knoxchase" && IsFan == "1") {
                    
                    if ($.cookie("prv") != "1") {
                        
                        btn.after("<div class='voteTimer text-center hidden-xs' id='voteTimer'>Voting will be available after Knox Chase Trailer</div>");
                        $("#btnSkipContestant").after("<div  class='voteTimer text-center visible-xs' id='voteTimer'>Voting will be available after Knox Chase Trailer</div>");
                        btn.attr("disabled", "disabled");
                    }
                    if ($.cookie("prv") == "1" && !Contestant.MediaWatched) {
                        
                        btn.attr("disabled", "disabled");

                        //if (!Contestant.PressedPlay && isAppleDevice()) {
                        //    if ($(".voteTimer").length == 0) {
                        //        $("#btnVoteContestant").after("<div class='voteTimer text-center hidden-xs' id='voteTimer'></div>");
                        //        $("#btnSkipContestant").next().after("<div class='voteTimer text-center visible-xs' id='voteTimer'></div>");
                        //    }
                        //    $(".voteTimer").html("Voting will be available after viewing Video");                            
                        //}

                        writeLog("countDownToVote()");
                        countDownToVote();
                    }
                }
                
                /* btn.click(function () {

                    if (SiteConfig.SiteName == "knoxchase") {
                        if ($.cookie("prv") != "1" && IsFan == "1") {
                            
                            return;
                        }

                        if (!Contestant.MediaWatched) {
                           
                            return;

                        }
                    }
                    
                    var postedData = voteform.find('form').serialize();                   
                    var url = SiteURL.Contestant_ContestantVote + "?contestantId=" + Contestant.ContestantID;
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: postedData,
                        success: function (msg) {
                            
                            if (msg == "OK") {
                                //alert('Thank you for voting!');
                                //window.location.href = window.location.href;
                                //<div class="user-voted btn btn-primary" disabled=""><i class="icon-thumbs-o-up"></i> Voted</div>
                                var contestantID = btn.attr('data-contestantid');
                                var textVote = getVoteText();
                                //btn.replaceWith("<div class='user-voted btn btn-primary' disabled=''><i class='icon-thumbs-o-up'></i> Voted</div>");
                                btn.hide();

                                $.ajax({
                                    type: "POST",
                                    url: SiteURL.Contestant_ContestantVoteForm,
                                    data: {
                                        ContestantID: contestantID
                                        //SliderWidth: configs.SliderWidth,
                                        //TieBreak: configs.TieBreak
                                    },
                                    success: function (msg) {                                        
                                        //$("#dvVoteSection>div.user-voted.btn").hide();
                                        voteform.show();
                                        voteform.html(msg);
                                        if (voteform.is(":hidden")) {
                                            btn.replaceWith("<div class='user-voted btn btn-primary' onclick='ShowMessageVoted()'><i class='icon-thumbs-o-up'></i> " + textVote + "</div>");
                                        }
                                        var voteCountEl = $(".contestant-vote .voteCount");
                                        if (voteCountEl.size() == 1) {
                                            try{
                                                voteCount = voteform.find(".VoteCount:hidden").val();
                                                voteCountEl.html(voteCount);
                                            }
                                            catch (ex) {
                                            }
                                        }
                                    }
                                })

                                //GetNextContestant(Contestant.ContestantID, true);
                            }
                        }
                    });
                    //voteform.find('form').submit();                       

                })
                */

                btn.unbind()
                    .click(function () {
                    voteform.find('form').submit();
                })
                
            }
            else if (votetype == '1') {
                btn.hide();
                voteform.show();

                //btn.unbind()
                //   .click(function () {
                //       voteform.find('form').submit();
                //   })
            }
            else {
                btn.attr('data-votetype', votetype);
                btn.show();
                btn.unbind()
                   .click(function(){
                       btn.hide();
                       voteform.show();
                       InitContestantVoteSlider(voteform);
                   })
            }
        }
        else {
            if (SiteConfig.SiteName == "knoxchase") {
                if (hasvoted == '1' || displayresultwhencompleted == '1') {
                    if (votetype == "0") {
                        btn.hide();
                        btn.replaceWith("<div class='user-voted'></div>");
                    }
                    else {
                        btn.hide();
                        voteform.show();
                    }
                }
                else {
                    btn.hide();
                    voteform.hide();
                }
            }
            else {
                //btn.hide();
                if (hasvoted == '1') {
                    //if (displayresultwhencompleted == '1') {
                    //    //$("#dvVoteSection>div.user-voted.btn").hide();
                    //    voteform.show();
                    //}
                    //else if (votetype != "0") {
                    //    btn.replaceWith("<div class='user-voted btn btn-primary' onclick='ShowMessageVoted()'><i class='icon-thumbs-o-up'></i> " + lblVoted + "</div>");
                    //    //btn.replaceWith("<div class='user-voted btn btn-primary' disabled><i class='icon-thumbs-o-up'></i> Voted</div>");
                    //}
                    btn.hide();
                    voteform.show();
                }
                else {
                    voteform.show();
                }
            }
        }

        if (configs.onButtonClick != null) {
            configs.onButtonClick();
        }
    }
});
}


// show message voted when user click voted button
function ShowMessageVoted() {
    if ($("#dvMessageVoted").size() > 0) {
        if ($("#dvMessageVoted").text() != "") {
            $("#dvMessageVoted").show();
        }        
    }
}


function CheckAllowAnonymousVote() {
    /*if (SiteConfig.AllowAnonymousVote) {
        if ($.cookie("vdate") == new Date().toLocaleDateString())
            return false;
    }*/
    return true;
}

function UpdateAllowAnonymousVote() {  

    $.cookie("vdate", new Date().toLocaleDateString());
}

function InitVoteSlider() {

}

function InitContestantVoteSlider(container) {
    container.find(".votecriteria").each(function (i) {

        var slider = this;
        $(slider).slider({
            value: 0,
            min: parseInt($(slider).attr("slider-min")),
            max: parseInt($(slider).attr("slider-max")),
            step: 1,
            create: function (event, ui) {
                var value = $(slider).slider("value");
                $(slider).slider().find("a")
                    .css({ "text-align": "center", "text-decoration": "none" })
                    .html(value == 0 ? "" : value);
            },
            slide: function (event, ui) {               
                $(ui.handle).html(ui.value);
                if (ui.value != 0) {
                    $(ui.handle).parent().next().hide();
                }
                else {
                    $(slider).slider().find("a").html("");
                    if (SiteConfig.SiteName != "aarp" && SiteConfig.SiteName.indexOf("collette") < 0) {
                        $(ui.handle).parent().next().show();
                    }                    
                }
                $(ui.handle).parents(".dvVoteCriteria")
                            .find("#hdScore_" + $(ui.handle).parent().attr("id") + " > .hdScore")
                            .val(ui.value);

                var container = $(ui.handle).closest("form");
                if ($(".avgscore", container).length > 0) {
                    CalculateAvgScore(container);
                }

            }
        })
    });
    $('[data-toggle="tooltip"]').tooltip();
    $(".inputNumber").bind("keydown", inputNumber_OnKeyDown).bind("paste", inputNumber_OnPaste);
}

function onInputScoreChange(input) {
    var el = $(input);
    var value = el.val();
    var minValue = parseInt(el.attr("min"));
    var maxValue = parseInt(el.attr("max"));

    if (isNaN(value))
        value = minValue;
    if (value < minValue)
        value = minValue;
    if (value > maxValue)
        value = maxValue;
    el.val(value);

    var slider = $(input).parents(".dvVoteCriteria").find(".votecriteria").slider({ value: value });

    var aSlider = $(slider.get(0).children);
    
    if (value == "0") {
        value = "";
    }
    if (value == "") {
        slider.next(".invalidVote").show();
    }
    else {
        slider.next(".invalidVote").hide();
    }

    aSlider.html(value);

    //.find("a").html(value);

    CalculateAvgScore();
}

var TempVoteData = {
    ids: [],
    lstCriterias: [],
    html : [],
};

function addTempVote(obj){
    var frmVote = $(obj).parents('.frmVoteContestant');

    if(CheckVoteDetail(frmVote.prop('id'))){

        var contestantid = frmVote.attr('data-contestantid');
        var lstScores = frmVote.find('.hdScore');
        var VoteCriterias = [];
        var i = 0;

        for(var i=0;i<lstScores.length;i++){
            VoteCriterias.push({
                VoteCriteriaID: frmVote.find("input:hidden[name='[" + i + "].VoteCriteriaID']").val(),
                SelectedValue: frmVote.find(".hdScore[name='[" + i + "].SelectedValue']").val(),
                JudgeWeight : frmVote.find("input:hidden[name='[" + i + "].JudgeWeight']").val(),
                CriteriaName : frmVote.find("input:hidden[name='[" + i + "].VoteCriteriaID']").closest('.dvVoteCriteria').find('.voting_criteria > span').text()
            });
        }

        //html
        var dvContestant = $('.ContestantWithMedia[contestantid=' + contestantid + ']');
        var contestantImage = $(dvContestant.find('.contestantimage').parent().html());
        contestantImage.find('img').css('width', dvContestant.find('.contestantimage > img').width()*0.75);

        var contestantName = $('<div>' + dvContestant.find('.listTitle').parent().html() + '</div>');
        contestantName.find('.listTitle').css('margin','5px 0px');

        var html = {
            contestantImage : contestantImage,
            contestantName : contestantName
        }


        TempVoteData.ids.push(contestantid);
        TempVoteData.lstCriterias.push(VoteCriterias);
        TempVoteData.html.push(html);

        frmVote.parents('.sectionVote').css('visibility','hidden');
    }
}

function removeTempVote(contestantId){

    var removedIdx = TempVoteData.ids.indexOf(contestantId);
    TempVoteData.ids.splice(removedIdx,1);
    TempVoteData.lstCriterias.splice(removedIdx,1);
    TempVoteData.html.splice(removedIdx,1);


    var dvContestant = $('.ContestantWithMedia[contestantid=' + contestantId + ']');
    dvContestant.find('.sectionVote').css('visibility','visible');
}

function isTieAfterTiebreak(){
    var postedData = {
        contestantids: TempVoteData.ids,
        VoteCriterias: TempVoteData.lstCriterias,
        TieBreak : "true"
    };

    var istie = IsTieVote(postedData);
    if(istie){
        alert("The contest will be tied after voting.");
        return true;
    }

    return false;
}
function addTempVoteHTML(idx,contestantId,votedData,contestanthtml){
    var html = $('<tr>'                                                      +
                 '    <td>'+ (idx+1) +'</td>'                                +
                 '    <td class="tmpcontestantinfo" style="width:60%"></td>'                   +
                 '    <td class="tmpvoteinfo"></td>      '                   +
                 '    <td class="tmpaction"><a href="#" class="lnkDelete">Delete</a></td>    ' +
                 '<tr/>');
                
    html.find('.tmpcontestantinfo')
        .append(contestanthtml.contestantImage)
        .append(contestanthtml.contestantName);

    var lstVoted = $('<div></div>');        
    for (var j = 0; j < votedData.length; j++) {
        $('<div></div>')
            .html(votedData[j].CriteriaName + ': <span class="badge pull-right">' + votedData[j].SelectedValue + ' </span>')
            .appendTo(lstVoted);
    }

    html.find('.tmpvoteinfo').append(lstVoted);

    html.find('.tmpaction > .lnkDelete')
        .unbind('click')
        .click(function(){
            removeTempVote(contestantId);
            $(this).parents('tr').remove();
            return false;
        });

    return html;
}

function submitTieBreak(){
        
    var postedData = {
        contestantids: TempVoteData.ids,
        VoteCriterias: TempVoteData.lstCriterias,
        Security: $("#Security").val(),
        TieBreak : "true"
    };

    var istie = IsTieVote(postedData);
    if(istie){
        alert("The contest will be tied after voting.");
        return;
    }

    var url = SiteURL.Contestant_ContestantVoteMultiple;
    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(postedData),
        success: function (msg) {
            if (msg == "OK"){
                alert('Tiebreak successully!');
                window.location.href = window.location.href;
            }
        }
    });
}

function HideContestantInTempVote(){
    for (var i = 0; i < TempVoteData.ids.length; i++) {
        var contestantId = TempVoteData.ids[i];
        $('.ContestantWithMedia[contestantid=' + contestantId + ']').find('.sectionVote')
            .css('visibility','hidden');
    }
}

function showListOfTempVote(){

    if(TempVoteData.ids.length == 0){
        alert('Please vote at least one candidate to tie break.');
        return false;
    }

    var dvListOfTempVote = $('#dvListOfTempVote');
    var tbl = dvListOfTempVote.find('.tblListOfTempVote');
    var btnSubmit = dvListOfTempVote.find('#btnSubmitTieBreak');

    tbl.find('tbody > tr').remove();

    for (var i = 0; i < TempVoteData.ids.length; i++) {
        tbl.find('tbody')
           .append(addTempVoteHTML(i,TempVoteData.ids[i],TempVoteData.lstCriterias[i],TempVoteData.html[i]));
    }

    if(TempVoteData.ids.length == 0){
        btnSubmit.hide();
    }
    else{
        btnSubmit
            .show()
            .unbind('click')
            .click(function () {
                submitTieBreak();                
                return false;
            })
                
    }

    dvListOfTempVote.dialog({
        width : 700,
        height: 525,
        lgClass:true
    })
    dvListOfTempVote.dialog('open');
}

function GetNextContestant(contestantId, isRedirect, action, updateReview, isPartial, contestID) {
    if (updateReview) {
        $.ajax({
            type: "POST",
            url: SiteURL.Prefix + "/contestant/ResetInReview",
            data: {
                ContestantID: contestantId
            }
        });
    }
    var url = SiteURL.Prefix + "/contestant/GetNextPre"
    var btnId = action == "prev" ? "#btnPrevious" : "#btnSkipContestant"   

    if ($("#btnPrevious").size() > 0 && !$(btnId).attr("data-next-contestantid") && !action)
        btnId = "#btnPrevious";

    if ($(btnId).attr("data-next-contestantid")) {
        var nextId = $(btnId).attr("data-next-contestantid");
        var nextContestId = $(btnId).attr("data-next-contestid");
        if (!nextContestId)
        {
            nextContestId = contestID;
        }
        if (isPartial) {
            ContestantService.showContestantPopup({ contestantID: nextId, contestid: nextContestId });
            return;
        }

        if (isRedirect){
            var redirectUrl = SiteURL.Prefix + "/contestant/details/" + nextId;
            if (nextContestId)
                redirectUrl += ("?contestId=" + nextContestId);
            location.href = redirectUrl;
        }            
        else {
            if ($(btnId).attr("data-next-contestantid"))
                $(btnId).show();          
        }
    }
    else {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                ContestantID: contestantId,
                ContestID: Contestant.ContestID
            },
            success: function (msg) {
                var nextId = 0;
                var nextContestId = contestID;
                if (action == "prev") {
                    nextId = msg.PrevID;
                    nextContestId = msg.PrevContestID;
                }
                else {
                    nextId = msg.NextID;
                    nextContestId = msg.NextContestID;
                }
                if ($("#btnPrevious").size() > 0 && nextId == 0 && !action) {
                    nextId = msg.PrevID;
                    nextContestId = msg.PrevContestID;
                }
                
                if (msg.PrevID > 0){
                    $("#btnPrevious").attr("data-next-contestantid", msg.PrevID);
                    $("#btnPrevious").attr("data-next-contestid", msg.PrevContestID);
                    $("#btnPrevious").show();                    
                }
                else
                     $("#btnPrevious").hide();

                if (msg.NextID > 0){
                    $("#btnSkipContestant").attr("data-next-contestantid", msg.NextID);
                    $("#btnSkipContestant").attr("data-next-contestid", msg.NextContestID);
                    $("#btnSkipContestant").show();                    
                }
                else
                     $("#btnSkipContestant").hide();
                
                if (isPartial) {
                    ContestantService.showContestantPopup({ contestantID: nextId, contestid: nextContestId });
                    return;
                }

                if (nextId > 0) {
                    if (isRedirect)
                        location.href = SiteURL.Prefix + "/contestant/details/" + nextId + "?contestId=" + nextContestId;
                } 
                else{
                    if (isRedirect)
                        location.href = SiteURL.Prefix + "/contestant/details/-1" + "?contestId=" + nextContestId;
                }
            }
        });
    }
}

function countDownToVote() {
    Contestant.VoteWaitTime--;
    if (Contestant.VoteWaitTime > 0) {
        if ($(".voteTimer").length == 0) {
            $("#btnVoteContestant").after("<div class='voteTimer text-center hidden-xs' id='voteTimer'></div>");
            $("#btnSkipContestant").after("<div class='voteTimer text-center visible-xs' id='voteTimer'></div>");
            Contestant.VoteWaitTimer = $(".voteTimer");
        }
        
        $(".voteTimer").html("Voting available in " + pad(Contestant.VoteWaitTime, 2) + (Contestant.VoteWaitTime == 1 ? " second" : " seconds"));
        setTimeout(countDownToVote, 1000);        
    }
    else {        
        $('.voteTimer').attr('style', 'display:none !important');
        $("#btnVoteContestant").removeAttr("disabled");
        Contestant.MediaWatched = true;
    }
    
}

function confirmVote() {
    $('.btnVoteHolder ').attr("data-showpopup", "");
    CallVoteButton(tempConfigs);
}

function InitVoteHolder(param) {
    var option = $.extend({}, param);

    $('#lstContestants').on('click', '.btnVoteHolder', function () {
        var btn = $(this);

        CallVoteButton({
            Button: btn,
            VoteFormContainer: btn.next(),
            SliderWidth: 155,
            TieBreak: option.isTieBreak,
            onButtonClick: function () {
                if (option.contestantsLayout != '2' || option.isTieBreak) {
                    if (window.AdjustContestantImageHeight) window.AdjustContestantImageHeight();
                }
            },
            callBack: param.callBack
        });

        return false;
    });
}

