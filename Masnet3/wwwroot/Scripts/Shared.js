﻿// _Sharing
var GSharing = {
    CurrentSharedUrl: ""
}

String.prototype.getIndex = function (content) {
    return this.search(new RegExp(content, "i"));
}

String.prototype.hasContent = function (content) {
    return this.getIndex(content) > -1;
}

function isURL(path) {
    //return path.indexOf("http://") > -1 || path.indexOf("https://") > -1;
    return path.hasContent("http://") || path.hasContent("https://");
}

function wrapURL(path) {
    var resultUrl = '';
    if ($.trim(path) != '') {
        if (path.getIndex('https://') == 0 || path.getIndex('http://') == 0) {
            resultUrl = path;
        }
        else {
            if (path.getIndex('://') == 0)
                resultUrl = 'http' + path;
            else if (path.getIndex('//') == 0)
                resultUrl = 'http:' + path;
            else if (path.getIndex('/') == 0)
                resultUrl = 'http:/' + path;
            else
                resultUrl = 'http://' + path;
        }
    }

    return resultUrl;
}

function initFacebookAsync() {

    if (typeof window.fbAsyncInit == 'undefined') {
        if ($("#fb-root").length == 0)
            $("body").prepend("<div id='fb-root'></div>");

        window.fbAsyncInit = function () {
            FB.init({
                appId: SiteConfig.FBShareAppID,
                xfbml: true,
                status: true,
                cookie: true,
                version: 'v2.2'
            });

            resizeFBCanvasHeight();

            if (window.FBReady)
                FBReady();

            //if (window.registerLike)
            //    registerLike();


        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
}

function BindShareable(obj) {
    GSharing.CurrentSharedUrl = $(obj).attr("shared-url");
    var bInContentWrapper = ($(obj).attr("shared-outcontentwrapper") == null);
    var bNoPopup = ($(obj).attr("shared-nopopup") != null);

    var os = $(obj).offset();
    var h = $(obj).outerHeight();
    var wlink = $(obj).width();
    //if (h < 40)
    //    h = 37;
    ////mas site 
    //if ($(obj).height() == 23 || $(obj).height() == 28)
    //    h = 28;

    if (!bNoPopup) {
        //if ($("#dvSharing").length == 0) {
        if ($("#shareWrapper").length == 0) {
            //console.log('load share content');
            InitSharing((os.top + h - 15), os.left, bInContentWrapper, obj);
            initFacebookAsync();
            setShareActionStatusLinks(obj);
            //if ($('#dvSharing').is('.modal')) {
            //    $('#dvSharing div.modal-content').prepend('<button type="button" onclick="hideDivShare();" class="close" style="font-size: 18px !important; font-weight: 700; text-align:center;line-height: 1; color: #fff; text-shadow: none; opacity: 1; background: #be212d !important; border-radius: 50%; width: 20px; height: 20px;padding:0px!important;min-width:0px;"><span aria-hidden="true">×</span></button>');
            //}
            //else {
            //    $('#dvSharing > div').prepend('<button type="button" onclick="hideDivShare();" class="close" style="font-size: 18px !important; font-weight: 700; text-align:center;line-height: 1; color: #fff; text-shadow: none; opacity: 1; background: #be212d !important; border-radius: 50%; width: 20px; height: 20px;padding:0px!important;min-width:0px;"><span aria-hidden="true">×</span></button>');
            //}                       
        }
        else {
            InitSharing((os.top + h - 15), os.left, bInContentWrapper, obj);
            $("#dvSharing a").attr("shared-contestantid", $(obj).attr("contestantid"));
            $("#shareWrapper a").attr("shared-contestantid", $(obj).attr("contestantid"));
            $("#dvSharing").css({ top: (os.top + h - 15) + "px", left: SpecifySharing(os.left, bInContentWrapper) + "px" })
            //               .show();

            setShareActionStatusLinks(obj);
        }


        //if ($('#dvSharing').is('.modal')) {
        //    $('#dvSharing').css('top', 0).css('left', 0);
        //}


        //$('#dvSharing').dialog({ lgClass: 'modal-sm', hideTitle: true });
    }
    else {
        setShareActionStatusLinks(obj);
    }


    if ($("[shared-url] [data-actionid=1]").length > 0) {
        initFacebookAsync();
    }
}

function hideDivShare(obj) {
    //$("#dvSharing").hide();
    $(obj).closest('.popover').popover("hide");
    currentPopOverTrigger = null;
}

function EnableAfterLoadingButton() {
    $("[data-enableAfterLoading]").removeAttr("disabled");
}

$(document).ready(function () {
   
    //Force iframe session
    var reref = getUrlParameter(location.href, "reref");
    if (reref) {
        $.cookie("fs", "1", { path: "/" });
        location.href = reref;
    }

    if (isAppleDevice()) {

        var cookie = $.cookie("fs");
        if (parent != self && cookie == null) {
            $.cookie("fs", "1", { path: "/" });
            href = SiteConfig.gSiteAdrs + "/home/iframe?forcelogin=1"; //document.location.href;
            top.location.href = href + (href.indexOf('?') == -1 ? "?" : "&") + 'reref=' + encodeURIComponent(document.referrer);
        }
    }

    var previewHeaderOnload = getUrlParameter(location.href, "previewHeaderOnload");
    if (previewHeaderOnload) {
        if (parent["previewHeaderOnload"]) {
            parent["previewHeaderOnload"]();            
        };
    }

    //End Force iframe session

    //AARP DMI pixel
    if ($.cookie("tid")) {
        $("body").append('<iframe src="//cafetrack.com/p.ashx?o=12304&e=613&t=' + $.cookie("tid") + '" height="1" width="1" frameborder="0"></iframe>"');
        setTimeout(function () {
            $.cookie("tid", null, { path: '/' });
        }, 1000);        
    }

    //FB pixel
    //setTimeout(function () {
    //    if ($.cookie("fbt")) {
    //        eval($.cookie("fbt"));
    //        $.cookie("fbt", null, { path: '/' });
    //    }
    //}, 500);       
    //FB pixel

    //Focus on validate
    if (SiteConfig.IsAdmin) {
        formAfterValidateFail();
    }

    //Share menu
    $("*[shared-url][auto-init]").each(function () {
        var obj = $(this).attr("share-flag");
        if (obj == null || obj.length == 0)
            BindShareable(this);
    });
    $("*[shared-url]:not([auto-init])")
    .hover(function () { //hover
        var obj = $(this).attr("share-flag");
        if (obj == null || obj.length == 0)
            BindShareable(this);
    }, function () { //mouseout
        //$("#dvSharing").hide();

    })

    $("*[requirelogin]")
        .removeAttr("onclick")
        .unbind()
        .click(function () {
            $("#loginLink").trigger("click");
        });

    //Jquery custorm function
    if ($.validator) {
        $.validator.addMethod(
            "regex",
            function (value, element, regexp) {
                var re = new RegExp(regexp, 'i');
                return this.optional(element) || re.test(value);
            },
            "Please check your input."
        );
        $.validator.methods.email = function (value, element, param) {
            return !value || validateEmail(value);
        }

        $.validator.addMethod("minWord", function (value, element, min) {
            var textValue = value;
            if (typeof (CKEDITOR) != 'undefined' && CKEDITOR.instances[element.id]) {
                textValue = CKEDITOR.instances[element.id].getData();
            }
            var len = textValue.countWord();
            return (min == 0 && this.optional(element)) || len >= parseInt(min);
        }, jQuery.validator.format("Please enter at least {0} words"));

        $.validator.addMethod("maxWord", function (value, element, max) {
            var textValue = value;
            if (typeof (CKEDITOR) != 'undefined' && CKEDITOR.instances[element.id]) {
                textValue = CKEDITOR.instances[element.id].getData();
            }
            var len = textValue.countWord();
            return (max == 0 && this.optional(element)) || len <= parseInt(max);
        }, jQuery.validator.format("Please enter no more than {0} words"));

        $.validator.addMethod("ckRequired", function (value, element) {
            var isValid = false;
            if (typeof (CKEDITOR) != 'undefined' && CKEDITOR.instances[element.id]) {
                isValid = CKEDITOR.instances[element.id].getData().trim().length > 0;
            }
            else {
                isValid = $.validator.methods.required.call(this, value, element);
            }
            return isValid;
        }, $.validator.messages.required);

        $.validator.addMethod("POBoxValidate", function (value, element) {

            var patt = /(po box|p.o. box|post office box|PO \d+)/i ;
            var res = patt.test(value);
            return !res;

            /*
            var poBoxAddress = value.toLowerCase();
            if (poBoxAddress.indexOf("po box") > -1 ||
                poBoxAddress.indexOf("p.o. box") > -1 ||
                poBoxAddress.indexOf("post office box") > -1) {
                return false;
            }
            return true;
            */
            
        });
    }
    
    //Contenteditable Init
    if (SiteConfig.PreviewMode != "readonly") {
        $("[contenteditable]").each(function (index) {
            this.addEventListener('input', function () {
                $(this).attr("data-contentChanged", "1");
            });
            var handler = eval('(' + $(this).attr("handlers") + ')');
            if (handler.rules.hasOwnProperty("multiline") && !handler.rules["multiline"]) {
                $(this).off("keypress").on("keypress", function (e) {
                    if (e.keyCode == 13)
                        return false;
                });
                $(this).unbind("paste").bind("paste", function () { return false; });
            };
        });
    }

    EnableAfterLoadingButton();

    bigDataInit();

    //focus leftmenu
    $(".leftnav a,.dropdown-menu a").removeClass("active");
    if (SiteConfig.pageName != undefined) {
        $(".leftnav a[href*='" + SiteConfig.pageName.replace('_listview', '') + "']").addClass("active");
        $(".dropdown-menu a[href*='" + SiteConfig.pageName.replace('_listview', '') + "']").addClass("active");
    }

    //update Share
    if (SiteConfig.ShareID > 0) {
        var robots = SiteConfig.robots.split(",");
        for (var i = 0; i < robots.length; i++) {
            if (SiteConfig.RefererAgent.indexOf(robots[i]) > -1)
                break;

            updateShare({ TranID: SiteConfig.ShareID, status: 1, pt: 3 });
            //$.cookie("fr_" + SiteConfig.shareID, "1");

            // update share return
            WriteLog("source Shared.js, IP: " + SiteConfig.RefererIP + " , Agent: " + SiteConfig.RefererAgent + " URL:" + location.href);
            updateShareReturn();
        }

        
    }

})

function WriteLog(s) {
    $.ajax({
        type: "POST",
        url: SiteURL.Prefix + "/Home/WriteLog",
        data: {s : s}
    });
}

function bigDataInit() {
    if (IsBigData == true) {
        $("[data-bigdata-remove]").remove();
        $("[data-bigdata-hide]").hide();
    }
}

function setShareActionStatusLinks(obj) {

    $("#dvSharing, #dvSharing [data-actionid]").attr("shared-contestantentryid", $(obj).attr("shared-contestantentryid"));
    $("#dvSharing, #dvSharing [data-actionid]").attr("shared-contestantid", $(obj).attr("shared-contestantid"));
    $("#dvSharing, #dvSharing [data-actionid]").attr("shared-callback", $(obj).attr("shared-callback"));
    $("#dvSharing, #dvSharing [data-actionid]").attr("shared-url", GSharing.CurrentSharedUrl);
    if ($(obj).attr("shared-contestId") != null) {
        $("#dvSharing, #dvSharing [data-actionid]").attr("shared-contestId", $(obj).attr("shared-contestId"));
    }

    $("a[data-actionid]").each(function () {

        var actionId = $(this).attr("data-actionid");
        var status = $(this).attr("shared-status");

        switch (status) {
            case "disabled":
                $(this).unbind();
                $(this).attr("disabled", "disabled");

                if ($(this).hasClass("disabled-link") == false) {
                    $(this).addClass("disabled-link");
                }
                $(this).removeAttr("onclick");
                $(this).removeAttr("target");
                $(this).attr("href", "javascript:void(0);");
                break;
        }

    });
}

function LoadCkEditorScript() {
    $.ajax({
        url: SiteContent.CKEditor,
        dataType: "script",
        cache: true,
        async: false,
        success: function (msg) {
            CKEDITOR.basePath = SiteContent.CKEditorBasePath;
            CKEDITOR.config.contentsCss = SiteContent.CKEditorContentsCss;
        }
    });
}

function applyHTMLEditor(elementID) {
    if (window.CKEDITOR) {
        if (CKEDITOR.instances[elementID]) {
            try {
                CKEDITOR.instances[elementID].destroy(true);
            }
            catch (ex) { }
        }
            
        var isWordMode = $("textarea#" + elementID).attr("wordmode") == 1;
        var maxWordLength = $("textarea#" + elementID).attr("maxwordlength");
        CKEDITOR.replace(elementID, {
            language: "en", wordcount : {
                // Whether or not you want to show the Word Count
                showWordCount : isWordMode,
                // Whether or not you want to show the Char Count
                showCharCount: !isWordMode,
                // Whether or not to include Html chars in the Char Count
                countHTML: false,
                countSpacesAsChars: true
            }
        });
        CKEDITOR.instances[elementID].on('change', function () {
            $("textarea#" + elementID).val(CKEDITOR.instances[elementID].getData());
            $("textarea#" + elementID).valid();
        });

        CKEDITOR.instances[elementID].on('blur', function () {
            $("textarea#" + elementID).valid();
        });
    }
    else {
        //$("textarea#" + elementID).on('input propertychange', function () { $("textarea#" + elementID).valid(); });
    }
}

function HideHelp(sDialog) {
        $("#" +sDialog).hide();
}

function ShowHelp(sDialog, iWidth, iHeight, Type, Contain, Obj) {
    var os = $("#" + Contain).offset();
    var h = $("#" + Contain).height();
    if (Obj) {
        os = $(Obj).offset();
        h = $(Obj).height();
    }
    var pLeft = os.left;
    pLeft += 15;
    h -= 15;
    if ($("#" + sDialog).width() > $(".content-wrapper").width())
        $("#" + sDialog).width($(".content-wrapper").width())

    if ((pLeft + $("#" + sDialog).width()) > $(".content-wrapper").width()) {
        pLeft = (pLeft - $("#" + sDialog).width());
    }
    if (pLeft < 0)
        pLeft = 0;
    if (Type == 1) {
        $("#" + sDialog).appendTo(document.body);
        $("#" + sDialog).css({ top: (os.top + h) + "px", left: pLeft + "px" });
        $("#" + sDialog).show(200);


        $("#" + sDialog).hover(function () {
            $(this).show();
        }, function () {
            $(this).hide();
        })
    } else if (Type == 2) {
        $("#" + sDialog).appendTo(document.body);
        $("#" + sDialog).css({ top: (os.top + 15) + "px", left: pLeft + "px" });
        $("#" + sDialog).show(200);


        $("#" + sDialog).hover(function () {
            $(this).show();
        }, function () {
            $(this).hide();
        })
    } else {
        $("#" + sDialog).dialog({ autoOpen: true, width: iWidth, modal: true, zIndex: 35000 });
        $("#" + sDialog).css("max-height", iHeight);
        $("#" + sDialog).dialog("open");
    }
}

function ShowMediaAjax(mediaId) {
    $.ajax({
        url: SiteURL.UploadFile_UploadFile,
        data: {
            MediaId: mediaId
        },
        dataType: 'json',
        success: function (obj) {
            ShowMedia({
                mediaid: obj.MediaID,
                thumbnail: obj.Thumbnail,
                filepath: obj.Path,
                filetype: obj.MediaTypeID,
                title: obj.Title
            })
        }
    })
}

function ShowMediaPopup(obj, playerid, param) {
    if (!playerid)
        playerid = "media_player";

    if (param != null) {
        param.playerid = playerid;
    }
    else {
        param = {
            mediaid: obj.getAttribute("media-mediaid"),
            thumbnail: obj.getAttribute("media-thumbnail"),
            filepath: obj.getAttribute("media-filepath"),
            filetype: obj.getAttribute("media-filetype"),
            openEffect: obj.getAttribute("data-open-effect"),
            closeEffect: obj.getAttribute("data-close-effect"),
            playerid: playerid
        };
    }

    if (param.showmediastyle == null)
        param.showmediastyle = SiteConfig.ShowMediaStyle;

    if ($("#" + playerid).length == 0)
        $("body").append("<div id='" + playerid + "' style='display:none'></div>");

    ShowMedia(param);

    if (param.showmediastyle == "0") {
        //$("#" + playerid).dialog({
        //    modal: true,
        //    title: param && param.title ? param.title : "",
        //    width: 565,
        //    height: 385,
        //    lgClass: param.lgClass != undefined ? param.lgClass : false,
        //    stop: param.stop != undefined ? param.stop : false
        //});
        var options = $.extend({
            target: '',
            contentTarget: '',
            content: '',
            title: '',
            lgClass: false,
            autoOpen: false,
            openEffect: '',
            closeEffect: ''
        }, param)
        $("#" + playerid).dialog(options);

        $("#" + playerid).dialog("open");
        $("#" + playerid).find("video").get(0).play();
    }
}

function ShowMedia(param, obj) {
    /*
    param = {                
        mediaid
        thumbnail
        filepath
        filetype       
        playerid
        showmediastyle,
        videoId
    }
    */
    if (param.showmediastyle == null)
        param.showmediastyle = SiteConfig.ShowMediaStyle;

    if (param.showmediastyle == "0") {
        var w = 500;
        var url = "";
        var playerid = "";
        playerid = param.playerid == null ? "media_player" : param.playerid;
        if ($("#" + playerid).length == 0)
            $("body").append("<div id='" + playerid + "' style='display:none'></div>");

        if (obj) {
            $(obj).parent().find("img").css("border", "0px solid #ccc");
            $(obj).css("border", "2px solid #ccc");
        }


        if (param.filetype == '4') {
            if (param.filepath == null || param.filepath == "") {
                $("#" + playerid)
                    .css({
                        "word-wrap": "break-word"
                        //, "font-size": "1.2em"
                    })
                    .html($("#dvDesc_" + param.mediaid).html());

                if (SiteConfig.SiteName.toLowerCase().indexOf("collette") == -1) {
                    $("#" + playerid).css("font-size", "1.2em");
                }
            }
            else {
                var downloadpath = param.filepath;
                var file_display = "Download";
                file_display = param.filepath.substring(param.filepath.lastIndexOf('/') + 1);
                var html = "<div style='text-align:left;padding:15px 5px;' class=''  onclick=\"DownloadFile($(this).attr('downloadpath'));\" downloadpath='" + downloadpath + "'><a href='#' style='text-decoration:none' onclick=\"return false;\" ><i class='icon icon-download'></i> " + file_display + "</a></div>";

                $("#" + playerid).html(html);
            }
        }
        else if (param.filetype == '1') {
            var imgPath = param.filepath;
            if (param.filepath.indexOf("//") == -1) {
                imgPath = SiteConfig.VirtualUploadPath + SiteConfig.UploadFolder + "/" + param.filepath;
            }

            if (param.thumbnail.indexOf("//") == -1)
                url = imgPath;
            else
                url = param.thumbnail;
            var attrZoom = "";
            if (param.cfZoom == 1) {
                var attrOpenEffect = param.openEffect ? " data-open-effect='" + param.openEffect + "' " : "";
                var attrCloseEffect = param.closeEffect ? " data-close-effect='" + param.closeEffect + "' " : "";
                var attrImgUrl = " data-image-url='" + imgPath + "' ";
                attrZoom = "cf-zoom data-modal-large='1' " + attrOpenEffect + attrCloseEffect + attrImgUrl;
            }            
            $("#" + playerid).html("<img class='img-responsive thumbnail' src='" + url + "' border='0' style='width:100%; max-width: 800px;' " + SiteConfig.HandleMissing_L + attrZoom + "  >");
            if (param.cfZoom == 1) initPhotoZoom();
        }
        else {
            PlayMedia(param.filetype, param.filepath, param.thumbnail, playerid, param);
        }
    }
    else {
        if ($("#dvFullsizeLink").length > 0)
            $("#dvFullsizeLink").remove();

        $("body:first").append("<div id='dvFullsizeLink' class='thumbnail' style='display:none'></div>");
        $("#dvFullsizeLink").append("<img href='#' id='lnkFullScreen' class='fullsizable' alt='' />");
        $("#lnkFullScreen")
            .attr("media-mediaid", param.mediaid)
            .attr("media-thumbnail", param.thumbnail)
            .attr("media-filepath", param.filepath)
            .attr("media-filetype", param.filetype)
            .attr("media-shorttitle", (param.title || ""))
            .attr("title", (param.title || ""));

        FullSizableConfig.navigation = false;

        $.fullsizableOpen($("#lnkFullScreen"));
    }
}

function isAppleDevice() {
    var isIPhoneDetect = navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) ||
        (navigator.userAgent.match(/Safari/i) && navigator.userAgent.match(/Macintosh/i)) //MAC Safari
    ;
    return isIPhoneDetect;
}

function isHttpFile(dFile) {
    return (dFile.indexOf("http://") == 0 || dFile.indexOf("https://") == 0 || dFile.indexOf("//") == 0);
}

function makeSureURL(dFile) {
    if (isHttpFile(dFile)) {
        if (dFile.indexOf("//") == 0)
            return "https:" + dFile;
        else
            return dFile;
    }
    else {
        return dFile;
    }
}

function PlayMedia(filetype, url, thumbnail, playerid, param) {
    var isIPhoneDetect = true;
    //(navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i) || (navigator.userAgent.match(/iPad/i)));
    var uAdultContent = "0";
    var flvType;
    var flv;

    var photo = "";
    var flvUrl = "";
    var gVirtualUploadPath = SiteConfig.VirtualUploadPath + SiteConfig.UploadFolder + "/";

    if (!isHttpFile(thumbnail))
        photo = gVirtualUploadPath + thumbnail;
    else {
        photo = thumbnail;
    }

    if (!isHttpFile(url))
        flvUrl = makeSureURL(gVirtualUploadPath + url);
    else {
        flvUrl = makeSureURL(url);
    }

    if (filetype == '2') {	//audio
        photo = SiteContent.AudioImage;
        flvType = "audio";
    }
    else {
        flvType = "video";
    }

    if (flvUrl.toLowerCase().indexOf("vimeo.com") >= 0) // Vimeo
    {
        if (param && param.simpleHtml5) {
            var intPosition = flvUrl.lastIndexOf("/");
            flvUrl = flvUrl.substring(intPosition + 1);

            //$("#" + playerid).addClass("hidebutton");
            var html5 = "<div data-type='vimeo' data-video-id='" + flvUrl + "'></div>";
            $("#" + playerid).html(html5);
            plyr.setup();
            return;
        }
        else {
            loadJS(SiteURL.Root + 'Scripts/vimeo.js');
            // Vimeo Player

            var vimeo_player = SiteConfig.VimeoPlayerEmbed;
            var vimeo_videoId = Vimeo.GetVideoID(flvUrl);
            var vimeo_width = $("#" + playerid).width() - 3;
            var vimeo_height = $("#" + playerid).width() * 35 / 47;

            vimeo_player = vimeo_player.replace("VIDEO_ID", vimeo_videoId);
            vimeo_player = vimeo_player.replace("EMBED_WIDTH", vimeo_width);
            vimeo_player = vimeo_player.replace("EMBED_HEIGHT", vimeo_height);

            $("#" + playerid).html(vimeo_player);

            // When the player is ready, add listeners for pause, finish, and playProgress
            /*var iframe = $('#player1')[0];
            var player = $f(iframe);
            if (player!= null)
                player.addEvent('ready', function () {
    
                    if (!isAppleDevice())
                        player.api("play");
    
                    //player.addEvent('pause', onPause);
                    player.addEvent('play', onPlay);
                    //player.addEvent('finish', onFinish);
                    //player.addEvent('playProgress', onPlayProgress);
                });*/

            return;
        }
    }

    if (!isIPhoneDetect) {
        var pwidth = $("#" + playerid).width() - 3;
        var pheight = $("#" + playerid).width() * 35 / 47;

        $("#" + playerid).flash(
	        {
	            swf: SiteContent.FlvPlayer,
	            height: pheight,
	            width: "100%",
	            allowFullScreen: true,
	            wmode: 'transparent',
	            flashvars: {
	                siteURL: SiteURL.Root,
	                flvURL: flvUrl,
	                flvType: flvType,
	                adultContent: uAdultContent,
	                cName: "ContestFactory",
	                cPhotoUrl: photo,
	                style: "border:1px solid blue",
	                cUrl: ""
	            },
	            hasVersion: 9, // requires Flash 9
	            hasVersionFail: function (options) {
	                alert("A minimum version of the Flash plugin 9.0 is required.")
	                return false; // returning false means the expressInstaller document will not be used

	            }
	        }
        );
    }
    else {
        ShowIPhoneVideo(flvUrl, '100%', '100%', photo, playerid, flvType, param);
    }
}

function ShowIPhoneVideo(url, width, height, photo, controlName, fileType, param) {

    var player = $("#" + controlName);

    //if (!(param && param.isAARP))
    //{
    //    player.css("width", (parseInt(player.parent().css("width").replace("px", "")) - 10) + "px");
    //    player.css("height", parseInt(player.parent().css("width").replace("px", "")) * 9 / 16 + "px");
    //}

    var videoId = "", autoplay = "";
    if (param && param.videoId) {
        videoId = "  id='" + param.videoId + "'";
    }
    if (param && param.autoplay) {
        autoplay = " autoplay ";
    }

    if (url.toLowerCase().indexOf("youtube") == -1) {
        var fileName = url.substring(0, url.lastIndexOf("."));
        var html5 = "";
        if (fileType == "video") {

            var webmUrl = fileName + ".webm";
            url = fileName + ".mp4";

            html5 = "<video width='" + width + "' " + videoId + autoplay + "  height='" + height + "' controls='controls' poster='" + photo + "'>"
            html5 = "<video width='" + width + "' " + videoId + autoplay + "  height='" + height + "' controls='controls' poster='" + photo + "'>"
            if (param && param.showProcess == undefined) {
                html5 = html5 + "<source src='" + url + "' type='video/mp4'  />"
                html5 = html5 + "<source src='" + webmUrl + "' type='video/webm'  />"
            } else {
                html5 = html5 + "<source src='" + url + "' type='video/mp4'  />"
                html5 = html5 + "<source src='" + webmUrl + "' type='video/webm'  />"
            }
            html5 = html5 + "Your browser does not support the video tag.</video>"
        } else {
            var mp3Url = fileName + ".mp3";
            url = fileName + ".ogg";
            //html5 = "<video width='" + width + "'  height='" + height + "' controls='controls' poster='" + photo + "'>"
            html5 = "<video width='" + width + "'  height='" + height + "' controls='controls' poster='" + photo + "'>"
            if (param && param.showProcess == undefined) {
                html5 = html5 + "<source src='" + mp3Url + "' type='audio/mpeg'  />"
                html5 = html5 + "<source src='" + url + "' type='audio/ogg'  />"
            } else {
                html5 = html5 + "<source src='" + mp3Url + "' type='audio/mpeg'  />"
                html5 = html5 + "<source src='" + url + "' type='audio/ogg'  />"
            }
            html5 = html5 + "Your browser does not support the audio tag.</video>"
        }

        player.html(html5);

        if (param && param.simpleHtml5) {
            plyr.setup();
            if (param && param.stopAutoPlay == undefined) {
                player.find("video").get(0).play();
            }
            console.log(player);
            if (player.parents().hasClass('modal')) {
                hideFullScreenButton();
            }

            //player.find("video").get(0).addEventListener("error", function () {
            //    setTimeout(function () {
            //        location.reload(true);
            //    }, 4000);
            //}, true);
        }

        var errorHanlder;
        var media = SiteConfig.SiteName.toLowerCase() == "rantchamp" ? "Rant" : "video";

        player.find("video").get(0).addEventListener('progress', function () {
            player.find(".playererr").hide();
            clearInterval(errorHanlder);
        }, true);

    } else {
        if (param && param.simpleHtml5) {
            var intPosition = url.lastIndexOf("/");
            url = url.substring(intPosition + 1);
            player.addClass("hidebutton");
            var html5 = "<div data-type='youtube' data-video-id='" + url + "'></div>";
            player.html(html5);
            plyr.setup();
        }
        else {
            var strHtml = "<iframe id='ik_player_iframe' frameBorder='0' width='" + width + "'  height='" + height + "'" +
                        "src='" + url.replace("/v/", "/embed/").replace("http://", "https://") + "?enablejsapi=1&autoplay=1' ></iframe>";
            if (location.href.indexOf("knoxchase/contest/details/") > -1) {
                var strHtml = "<iframe id='ik_player_iframe' frameBorder='0' width='" + width + "'  height='" + height + "'" +
                            "src='" + url.replace("/v/", "/embed/").replace("http://", "https://") + "?enablejsapi=1&autoplay=0' ></iframe>";
            }

            //$("#" + controlName).html(strHtml);
            ytID = url.replace("/v/", "/embed/");

            if ($("#ik_player_iframe").length > 0) {
                ik_player.loadVideoByUrl(url.replace("/v/", "/embed/"));

                if (Contestant.Votable == '1' && Contestant.IsFan == '1') {
                    writeLog("countDownToVote() from ShowIPhoneVideo");
                    countDownToVote();
                }

            }
            else
                $("#" + controlName).html(strHtml);

            //This code loads the IFrame Player API code asynchronously.        
            loadJS("https://www.youtube.com/iframe_api");
        }
    }
}

function errVideo(srcObj, wrapperId) {

    $(srcObj).attr("hasError", 1);
    var playerWrapper = wrapperId ? $("#" + wrapperId) : $("#media_player");
    var videoObj = $(srcObj).parent();
    if (videoObj.find("source[hasError='1']").size() == 2) {
        var msg = (videoObj.attr("converted") == "1" || videoObj.attr("converted") == "2") ? "Processing Video failed." : "<i aria-hidden='true' style='' class='icon icon-spinner fa-spin'></i>" + SiteLanguage.gMedia_InvalidFile;
        playerWrapper.css("position", "relative");
        dvErrVideo = $("<div class='playererr' style='position:absolute;top:0px;left:0px;'></div>")
                        .html("<div style='position: relative; height: 100%;'><div style='position: absolute; top: 50%; left: 50%; -ms-transform: translate(-50%, -50%);transform: translate(-50%, -50%);'>" + msg + "<div></div>");
        if (playerWrapper.find(".playererr").length == 0)
            playerWrapper.append(dvErrVideo);

        var videoEl = videoObj.get(0);
        if (videoEl.oncanplay == null) {
            videoEl.oncanplay = function () {
                dvErrVideo.hide();
            };
        }

        setTimeout(function () {
            videoObj.find("source").attr("hasError", 0);
            videoObj.load();

        }, 30000);
    }
}

function RebindFullsizable() {
    if (SiteConfig.ShowMediaStyle == "1") {
        $(".fullsizable")
            .removeAttr("onclick")
            .unbind()
            .click(function () {
                $.fullsizableOpen($(this));
                return false;
            });
    }
}

function DownloadFile(dFile) {
    var url = SiteURL.UploadFile_Download + "?filepath=" + encodeURIComponent(dFile);
    if ($("#ifrDownloader").length > 0)
        $("#ifrDownloader").remove();

    if (isHttpFile(dFile)) {
        window.open(dFile);
    }
    else {
        $("body:first").append("<iframe id='ifrDownloader' src='" + url + "' style='display:none' width='1px' height='1px'></iframe>");
    }
}

function DownloadCSV(url) {
//    $("body:first").append("<iframe onload='onDownloadCSVDone(this)' src='" + url + "' style='display:none' width='1px' height='1px'></iframe>");

    //if ($("#hdLinkDownload").size() > 0) {
    //    $("#hdLinkDownload").remove();
    //}
    //$("body:first").append("<a id='hdLinkDownload' href='" + url + "' target='_self' style='display:none' download onload='onDownloadCSVDone(this)'></a>");
    //$("#hdLinkDownload").get(0).click();

    fadeIn();
    document.location.href = url;
    fadeOut()
}

function ImageURL(dThumnail, dWidth, dHeight, prefix) {
    if (!dWidth)
        dWidth = 70;
    if (!dHeight)
        dHeight = 70;

    if (dThumnail.toLowerCase().indexOf("http") == 0) {
        return dThumnail;
    }
    else {
        if (prefix) {
            var pos = dThumnail.lastIndexOf("/");
            prefix += "_";
            dThumnail = dThumnail.substring(0, pos + 1) + prefix + dThumnail.substring(pos + 1);
        }
        var src = SiteConfig.VirtualUploadPath + SiteConfig.UploadFolder + "/" + dThumnail;
        return src;
    }
}

function showLogonDialog() {
    if ($("#login_form").length == 0) {
        $("#login_dialog").html("Loading ...");
        $("#login_dialog").load(SiteURL.Home_Login,
                function () {
                    if (SiteConfig.IsPreview == true) {

                        Content.InitContenteditable();

                        if (SiteConfig.PreviewMode != "readonly") {
                            //Contenteditable Init
                            $("[contenteditable][id='btnHandlerLogin'],[id='lnkForgotPassword']").each(function (index) {

                                this.addEventListener('input', function () {
                                    $(this).attr("data-contentChanged", "1");
                                });
                                var handler = eval('(' + $(this).attr("handlers") + ')');
                                if (handler.rules.hasOwnProperty("multiline") && !handler.rules["multiline"]) {
                                    $(this).on("keypress", function (e) {
                                        if (e.keyCode == 13)
                                            return false;
                                    });
                                    $(this).bind("paste", function () { return false; });
                                };

                            });
                        }
                    }
                }
            );

    }
    else {
        $("#UsernameLogin, #PasswordLogin").val('');
        $(".field-validation-error").attr("style", "display:none");
        $("#UsernameLogin, #PasswordLogin").removeClass("input-validation-error");
    }

    //Dialog
    if (SiteConfig.LoginStyle == "0") {

        $("#login_dialog").dialog({
            autoOpen: false,
            height: 270,
            width: 350,
            modal: true,
            close: function (e, ui) {
                $("#login_dialog").dialog("close");
            }
        });
        $("#login_dialog").dialog("open");

    }
    else {

        if ($(".loginoverlay").length == 0) {
            $("body").append('<div class="loginoverlay" style="display:none"></div>');
        }

        $("#login_dialog").show(300);
        $(".loginoverlay").show();

        $(".loginoverlay")
            .unbind()
            .click(function () {
                $(this).hide();
                $("#login_dialog").hide();
            });
    }
}

var hasinitsharing = false;
var dvSharingContent = null;
var currentPopOverTrigger = null;

function InitSharing(dTop, dLeft, bInContentWrapper, obj) {
    if (dvSharingContent) {
        if ($("#dvSharing").size() == 0)
            $("#shareWrapper").append(dvSharingContent);
    }

    if (hasinitsharing) {
        if (obj.id === "topSharing") {
            if ($("#topSharing").prop('data-loaded') !== 1) {
                $("#topSharing").popover({
                    html: true,
                    content: function () {
                        return $("#shareWrapper").html();
                    }
                }).popover('show').prop('data-loaded', 1);
                $('#dvSharing a').css("padding", 0);
                $('#dvSharing a').css("font-size", "14px");
                $('#dvSharing a').css("width", "100%");
                $('#dvSharing a').css("padding-bottom", "5px");
            }
        }
        else if (obj.id === "popSharing") {
            if ($("#popSharing").prop('data-loaded') != 1) {
                $("#popSharing").popover({
                    html: true,
                    content: function () {
                        return $("#shareWrapper").html();
                    }
                }).popover('show').prop('data-loaded', 1);
            }
        }
        else {
            if ($(obj).prop('data-loaded') !== 1) {
                $(obj).popover({
                    html: true,
                    content: function () {
                        return $("#dvSharing");
                    },
                    trigger: 'manual',
                    placement: 'bottom'
                });
                currentPopOverTrigger = obj;
                $(obj).popover('show').prop('data-loaded', 1);
                $(obj).mouseover(function (e) {
                    if (currentPopOverTrigger !== obj) {
                        currentPopOverTrigger = obj;
                        $(obj).popover('show');
                    }
                    $('[shared-url][data-original-title]').not(currentPopOverTrigger).popover('hide');
                });
            }
        }

        return;
    }


    hasinitsharing = true;

    $("#loadingindicator")
        .css({ top: dTop + "px", left: dLeft + "px" })
        .show();

    var link = SiteURL.Action_GetShareList;

    $.ajax({
        type: "GET",
        url: link,
        data: {
            contestantID: $(obj).attr("contestantID")
            //, tagId: $(obj).attr("data-tagid")
            ,contestID: SiteConfig.ContestID
        },
        async: false,
        success: function (msg) {

            $($("body").get(0)).append(msg);
            $("#shareWrapper").hide();
            $("#loadingindicator").hide();

            //if (!$("#dvSharing").is('.modal')) {
            //    $("#dvSharing").css({ top: dTop + "px", left: SpecifySharing(dLeft, bInContentWrapper) + "px" })
            //}      
            dvSharingContent = $("#shareWrapper").html();
            if (obj.id === "topSharing") {
                $("#topSharing").popover({
                    html: true,
                    content: function () {
                        return $("#shareWrapper").html();
                    }
                    //,title: function () {
                    //    return $("#example-popover-2-title").html();
                    //}
                }).popover('show').prop('data-loaded', 1);
                $('#dvSharing a').css("padding", 0);
                $('#dvSharing a').css("font-size", "14px");
                $('#dvSharing a').css("width", "100%");
                $('#dvSharing a').css("padding-bottom", "5px");
            }
            else if (obj.id === "popSharing") {
                $("#popSharing").popover({
                    html: true,
                    content: function () {
                        return $("#shareWrapper").html();
                    }
                }).popover('show').prop('data-loaded', 1);
            }
            else {
                $(obj).popover({
                    html: true,
                    trigger: 'manual',
                    content: function () {
                        return $("#dvSharing");
                    },
                    placement: 'bottom'
                });
                currentPopOverTrigger = obj;
                $(obj).popover('show').prop('data-loaded', 1);
                $(obj).mouseover(function (e) {
                    if (currentPopOverTrigger !== obj) {
                        currentPopOverTrigger = obj;
                        $(obj).popover('show');
                    }
                    $('[shared-url][data-original-title]').not(currentPopOverTrigger).popover('hide');
                });
            }
            //$("#dvSharing").show(200);

            //$("#dvSharing").hover(function () {
            //    //$(this).show();
            //}, function () {
            //    //$(this).hide();
            //})

        }
    });
}

function SpecifySharing(pLeft, bInContentWrapper) {
    if (bInContentWrapper == null)
        bInContentWrapper = true;

    if (bInContentWrapper) {
        //if ((pLeft + $("#dvSharing").width()) > $(".content-wrapper").width()) {
        //    var dLeft = (pLeft - (pLeft + $("#dvSharing").width() - $(".content-wrapper").width()) - 5);
        //    if (dLeft < 0)
        //        dLeft = 0;
        //    return dLeft;
        //}
        return pLeft;
    }
    else {
        if ((pLeft + $("#dvSharing").width()) > $("body:first").width()) {
            var dLeft = (pLeft - (pLeft + $("#dvSharing").width() - $("body:first").width()) - 40);
            if (dLeft < 0)
                dLeft = 0;
            return dLeft;
        }
        return pLeft;
    }
}

function ShowRequireLogin() {
    var urlContestRegister = SiteURL.Customer_Register;
    var urlContestLogin = SiteURL.Home_CustomerLogin;
    if ($("#dvRequireLogin").length == 0) {
        $("<div id='dvRequireLogin' style='z-index:1050;display:none;position:absolute;background-color:#fff;color:black;padding:30px 5px;border:1px solid gray'></div>").appendTo($("body").first());
        $("<div>Please <a href='" + urlContestLogin + "' style='font-weight:bold'>Login</a> or <a href='" + urlContestRegister + "' style='font-weight:bold'>Register</a> to continue.</div>")
            .appendTo("#dvRequireLogin");
    }

    $("*[requirelogin],.requirelogin")
        .unbind()
        .hover(
            function () {

                var os = $(this).offset();
                var ileft = os.left + ($(this).outerWidth() / 2) - ($("#dvRequireLogin").width() / 2);
                if (ileft + $("#dvRequireLogin").width() > $(".content-wrapper").width())
                    ileft = $(".content-wrapper").width() - $("#dvRequireLogin").width() - 3

                if ($("#dvRequireLogin").width() > $(".content-wrapper").width())
                    ileft = $(".content-wrapper").width();

                if (ileft < 0)
                    ileft = 3
                $("#dvRequireLogin").css({
                    top: (os.top + $(this).outerHeight() - 5) + 'px',
                    left: ileft + 'px',
                }).show();
            }, function () {
                $("#dvRequireLogin").hide();
            })

    $("#dvRequireLogin")
        .unbind()
        .hover(
            function () { $(this).show() },
            function () { $(this).hide() }
        )


}

function changeLanguage(langCode) {
    var currentUrl = window.location.href;
    if (currentUrl.toLowerCase().indexOf("&lang") > 0) {
        window.location.href = currentUrl.replace(/&?lang=([^&]$|[^&]*)/i, "&lang=" + langCode)
    } else if (currentUrl.toLowerCase().indexOf("?lang") > 0)
        window.location.href = currentUrl.replace(/&?lang=([^&]$|[^&]*)/i, "lang=" + langCode)
    else {
        if (currentUrl.toLowerCase().indexOf("?") > 0)
            window.location.href = currentUrl + "&lang=" + langCode
        else
            window.location.href = currentUrl + "?lang=" + langCode
    }

}

function keypressEnterHandler(FormName, CallBack) {
    $("#" + FormName).keypress(function (event) {
        var charCode = event.keyCode || event.which;
        if (charCode == 13) {
            if (CallBack)
                CallBack();
        }
    })

}

function ShowTimeoutWarning() {

    SiteConfig.IsTimeOut = "1";
    /*
    if ($("#divTimeoutWarning").length == 0) {
        var dvTimeoutHtml = "";
        dvTimeoutHtml += "<div id='divTimeoutWarning' valign='middle' align='center' style='display:none'>"
        dvTimeoutHtml += "    <div id='timeoutmsg' class='authenticate_link' style='margin-top:10px;padding: 5px'> " + SiteLanguage.gSessionTimeoutMessage + " </div>"
        dvTimeoutHtml += "    <div style='margin-top:5px;;padding: 5px 5px 10px 5px'>"
        dvTimeoutHtml += "        <input type='button' value='Close'  id='btnHideTimeOutMsg' onclick='HideTimeoutWarning();return false;'/>"
        dvTimeoutHtml += "    </div>"
        dvTimeoutHtml += "</div>"

        $("body").append(dvTimeoutHtml);
    }
    if ($("#divTimeoutWarning").hasClass('ui-dialog-content') == false) {
        $("#divTimeoutWarning").dialog({ autoOpen: false, width: 350, height: 'auto', modal: false, zIndex: 35000, close: function (event, ui) { location.href = SiteURL.Home_Logout; $("#divTimeoutWarning").dialog("destroy"); } });
        $("#divTimeoutWarning").dialog("open");
    }
    */

    if ($("#divTimeoutWarning").length == 0) {
        var dvTimeoutHtml = "";
        dvTimeoutHtml += "<div id='divTimeoutWarning' class='modal fade' role='dialog'>"
        dvTimeoutHtml += "  <div class='modal-dialog modal-sm' style='background: white'>"
        dvTimeoutHtml += "      <div class='modal-body'>"
        dvTimeoutHtml += "          <div id='timeoutmsg' class='authenticate_link' style='margin-top:10px;padding: 5px'> " + SiteLanguage.gSessionTimeoutMessage + " </div>"
        dvTimeoutHtml += "</div>"
        dvTimeoutHtml += "      <div class='modal-footer'>"
        dvTimeoutHtml += "          <input data-dismiss='modal' type='button' class='btn btn-info' value='Close'  id='btnHideTimeOutMsg' onclick='HideTimeoutWarning();return false;' />"
        dvTimeoutHtml += "      </div>"
        dvTimeoutHtml += "  </div>"
        dvTimeoutHtml += "</div>"
        $("body").append(dvTimeoutHtml);
    }

    $('.modal').on('show.bs.modal', function () {
        var dialog = $("#divTimeoutWarning").find('.modal-dialog');
        dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 3));
    });
    $('#divTimeoutWarning').on('hidden.bs.modal', function () {
        location.href = SiteURL.Home_Logout;
    });

    $("#divTimeoutWarning").modal("show");
}

function HideTimeoutWarning() {
    //$("#divTimeoutWarning").dialog("close");
    $("#divTimeoutWarning").modal("close");
    location.href = SiteURL.Home_Logout;
}

var FullSizableConfig = null;

$(document).ready(function () {
    if (window.arrOnReady) {
        $.each(arrOnReady, function (idx, fn) {
            fn();
        });
    }

    var isIPhone = (navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i) || (navigator.userAgent.match(/iPad/i)));
    if (isIPhone) {
        $('*').on('click', function () { return true; });
    }
    $.ajaxSetup({ cache: false });

    // Ajax activity indicator bound to ajax start/stop document events
    $(document).ajaxStart(function () {
        
    })
    .ajaxSend(function (e, request, settings) {
        var isUrlParam = false;
        var jsonParam;
        var url;
        var param;
        var resetPaging = settings.data && getUrlParameter(settings.data, "resetPaging") ? 1 : 0;

        if (hasPaging() && false) {
            // if no "page" param found
            if (settings.url.indexOf("page=") == -1 && settings.data && settings.data.indexOf("page=") == -1) {
                if (settings.url.indexOf("?") == -1) {
                    settings.url += "?page=1"
                }
                else {
                    settings.url += "&page=1";
                }
            }

            // Keep current page if ajax callback
            if (settings.url.indexOf("page=") > -1) {
                isUrlParam = true;
                var urlSegments = settings.url.split('?');
                url = urlSegments[0];
                param = urlSegments[1];
            }
            if (settings.data && settings.data.indexOf("page=") > -1) {
                try {
                    param = settings.data;
                }
                catch (ex) { }
            }

            if (param) {
                var currentPageValue = getUrlParameter(param, "page");
                var pageValue = resetPaging ? 1 : getCurrentPageIndex(e, currentPageValue);

                if (isUrlParam) {
                    settings.url = url + '?' + updateQueryStringParameter(param, "page", pageValue);
                }
                else {
                    settings.data = updateQueryStringParameter(param, "page", pageValue);
                }
            }
        }

        // show loading icon if request is not in ignore action list
        var ignoreActions = [
            "contest/_getstatus",
            "home/keepalive",
            "customer/_campaignstat",
            "contestant/_contestantvoteform",
            "contestant/updatecount",
            "contestant/_linkedmedia",
            "contest/_matchblockbigsize",
            "tools/getadhocmailstatus",
            "application/copyapplication",
            "application/isuniqueapplicationname",
            "customercontest/copycampaign",
            "application/deletesite",
            "report/getqueue",
            "contestant/disableshare"
        ];
        var requestUrl = settings.url.toLowerCase();        
        var arr = jQuery.grep(ignoreActions, function (action) {
            return requestUrl.indexOf(action) > -1; // match action
        });
        var showLoadingIcon = arr.length == 0; // request action is ignore or not.
        if (showLoadingIcon) {
            fadeIn();
        }
    })
    .ajaxStop(function (response) {
        //fadeOut();
        //hideLoading();        
    });

    $(document).ajaxComplete(function (event, xhr, settings) {

        if (settings.url.indexOf("tools/getadhocmailstatus") == -1
            && settings.url.toLowerCase().indexOf("application/copyapplication") == -1
            && settings.url.toLowerCase().indexOf("application/isuniqueapplicationname") == -1
            && settings.url.toLowerCase().indexOf("customercontest/copycampaign") == -1
            && settings.url.toLowerCase().indexOf("application/deletesite") == -1
            && settings.url.toLowerCase().indexOf("report/getqueue") == -1
            && settings.url.toLowerCase().indexOf("contestant/importcsv") == -1
            ) {
            fadeOut();
            hideLoading();
        }

        try {
            if (settings.url.indexOf("contest/submit") != -1
            || settings.url.indexOf("fileextension/getfileextension") != -1
            || settings.url.indexOf("uploadfile/getrecordedmedia") != -1
            || settings.url.indexOf("uploadfile/getnewfilename") != -1
            || settings.url.indexOf("contest/_submitinfo") != -1
            || settings.url.indexOf("contest/_submitfee") != -1
            || settings.url.indexOf("uploadfile/upload") != -1
            || settings.url.indexOf("uploadfile/library") != -1
            || settings.url.indexOf("uploadfile/_youtube") != -1
            || settings.url.indexOf("uploadfile/_textMessage") != -1
            || settings.url.indexOf("uploadfile/library") != -1
            || settings.url.indexOf("uploadfile/_media") != -1
            || settings.url.indexOf("uploadfile/_media") != -1
            || settings.url.indexOf("contest/_submitpreview") != -1
            || settings.url.indexOf("contest/complete") != -1

                ) {
                if (SiteConfig.IsPreview == false) {
                    if (xhr && xhr.getResponseHeader("HasSession").toLowerCase() == "false")
                        ShowTimeoutWarning();
                }
            }
        } catch (e) { }

        $("#divLeftMenuHide").height($("#dashboard_page").height());

    });

    initUploadMediaDialog();

    if (SiteConfig.ShowMediaStyle == "1") {
        FullSizableConfig = $.fullsizableDynamic(".fullsizable", {
            detach_id: ".nonfullsizable,.content-wrapper",
            navigation: true,
            closeButton: true,
            clickBehaviour: "doNothing"
        });
    }

    if ($("#Topmenu li").length > 0)
        $("#Topmenu").show()

    $(".ui-dialog").on("dialogopen", function (event, ui) {
        if ($(".ui-dialog").width() > screen.width) {
            $(".ui-dialog").width(screen.width);
            $(".ui-dialog").position({
                my: "center",
                at: "center",
                of: window
            });
        }
    });
    ShowRequireLogin();

    if (SiteConfig.Comment_IntervalRefresh > 0) {
        window.setInterval(function () {
            $(".dvComments").each(function () {
                onCommentPageChange($(this).attr("param_referid"), $(this).attr("param_commenttype"), 1, $(this).attr("param_container"), null);
            })
        }, SiteConfig.Comment_IntervalRefresh * 1000);
    }

    $(document).on('show.bs.modal', '.modal.in', function (event) {
        var zIndex = Math.max.apply(null, Array.prototype.map.call($('*:not(.fade-in-processing)'), function (el) {
            return el.style.zIndex;
        })) + 10;
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').addClass('modal-stack');
            $('.modal-backdrop .modal-stack').css('z-index', zIndex - 1);
        }, 100);
    });

    $(document).on('hidden.bs.modal', '.modal', function () {
        $('.modal:visible').length && $(document.body).addClass('modal-open');
    });

});

function initUploadMediaDialog() {
    $("[data-upload-running]").removeAttr("data-upload-running");

    $("[data-media-upload]").click(function () {
        var index = 0;
        $(this).attr("data-upload-running", "1");
        var iframeId = $.trim($(this).attr("data-media-iframeId"));
        var dlgId = $.trim($(this).attr("data-media-dialogId"));
        if (dlgId == "") {
            dlgId = "dialogUploadMedia";
        }

        if ($("#" + dlgId).length == 0) {
            $("body").append("<div style='display:none' id='" + dlgId + "'></div>");
        }

        if (iframeId == '') {
            iframeId = "UploadControl";
        }

        var htmlDiv = "<div style='padding: 5px 0;'><i>" + SiteLanguage.Media_SelectFile + "</i></div>";
        var defaultAjaxSubmit = $.trim($(this).attr("data-default-ajax-submit")) == "1";
        htmlDiv += "<div><iframe id='" + iframeId + "' name='" + iframeId + "' scrolling='no' width='100%' style='width:100%;max-width:450px' height='100px' frameborder='no' src='" + SiteConfig.VirtualUploadPath + "/UploadControls.aspx?";

        //VirtualUploadDomain
        var virtualuploaddomain = $(this).attr("data-media-virtualuploaddomain");
        if (virtualuploaddomain == null || virtualuploaddomain.trim() == "") {
            virtualuploaddomain = SiteConfig.VirtualUploadDomain;
        }
        htmlDiv += "virtualuploaddomain=" + virtualuploaddomain + "&";

        //sessionkey
        var sessionkey = $(this).attr("data-media-sessionkey");
        if (sessionkey == null || sessionkey.trim() == "") {
            sessionkey = SiteConfig.NowTick;
        }
        htmlDiv += "SessionKey=" + sessionkey + "&";

        var callback = $(this).attr("data-media-callback");
        if (callback != null && callback.trim() != "") {            
            if (callback.indexOf("()") == -1) {
                htmlDiv += "callback=" + callback + "&";
            }            
        }

        //maxfilesize
        var maxfilesize = $(this).attr("data-media-max-file-size");
        if (maxfilesize == null || maxfilesize.trim() == "") {
            maxfilesize = "4294967296";
        }
        htmlDiv += "MaxFileSize=" + maxfilesize + "&";

        //MediaType
        if ($(this).attr("data-media-type")) {
            var mediaType = $(this).attr("data-media-type");
            if (mediaType.trim() != "") {
                htmlDiv += "MediaType=" + mediaType + "&";
            }
        }

        //FileFilter
        var fileFilter = $(this).attr("data-media-file-filter");
        htmlDiv += "FileFilter=" + fileFilter + "&";

        //SaveWithName
        var saveWithName = $(this).attr("data-media-save-with-name");
        if (saveWithName != null && saveWithName != '') {
            htmlDiv += "SaveWithName=" + saveWithName + "&";
        }

        //SaveOverwrite
        var saveOverwrite = $(this).attr("data-media-save-overwrite");
        if (saveOverwrite != null && saveOverwrite != '') {
            htmlDiv += "Overwrite=" + saveOverwrite + "&";
        }

        //lang
        var lang = $(this).attr("data-media-lang");
        if (lang == null || lang.trim() == "") {
            maxfilesize = "en";
        }
        htmlDiv += "Lang=" + lang + "&";

        //UserName
        var username = $(this).attr("data-media-username");
        // ignore the param will use in config
        if (username.length > 50) {
            htmlDiv += "UserName=" + SiteConfig.CurrentUserFolder + "&";
        }
        else {
            htmlDiv += "UserName=" + username + "&";
        }


        //AppName
        var appName = $(this).attr("data-media-appName");
        if (appName == null || appName.trim() == "") {
            appName = SiteConfig.ApplicationName;
        }
        htmlDiv += "AppName=" + appName + "&";

        htmlDiv += "'></iframe></div>";
        htmlDiv += "<div>";
        htmlDiv += "    <input type='button' id='btnSubmitFacebookImage' value='Submit' />";
        htmlDiv += "    <input type='button' value='Close' onclick=\"$('#" + dlgId + "').dialog('close');return false;\" />";
        htmlDiv += "</div>"

        //Title
        var dialogTilte = $(this).attr("data-media-title");
        if (dialogTilte == null || dialogTilte.trim() == "") {
            dialogTilte = "Upload";
        }

        $("object").each(function () {
            var orginal = $(this).parent().css("transform");
            $(this).parent().css("transform", "translateX(0)");
            $(this).parent().css("visibility", "hidden");
            $(this).parent().attr("data-parent-flash-transform", orginal);
        });

        $("#" + dlgId).dialog({
            width: 500,
            height: 280,
            title: dialogTilte,
            close: function (event, ui) {

                $("object").each(function () {
                    var orginal = $(this).parent().attr("data-parent-flash-transform");
                    $(this).parent().css("transform", orginal);
                    $(this).parent().removeAttr("data-parent-flash-transform");
                    $(this).parent().css("visibility", "visible");
                });

                var saveWithName = $.trim($("[data-upload-running]").attr("data-media-save-with-name"));
                if (saveWithName != '') {
                    $("[data-upload-running]").attr("data-media-save-with-name", $.now());
                }

                $("[data-upload-running]").removeAttr("data-upload-running");
            }
        });

        $("#" + dlgId).dialog("open");


        $("#" + dlgId).html(htmlDiv);
        $("#Labeldg" + dlgId).html(dialogTilte);

        $("#btnSubmitFacebookImage", "#" + dlgId)
            .unbind()
            .click(function () {
                if (defaultAjaxSubmit) {
                    var targetIframe = $('#' + iframeId).get(0).contentWindow;                    
                    if (targetIframe.fn_Validate && !targetIframe.fn_Validate())
                        return false;

                    if (targetIframe.fn_DoUpload) {
                        targetIframe.fn_DoUpload();
                    }
                }
                else {
                    ajaxSubmitMediaFile(iframeId);
                }
            });
        var callback = $(this).attr("data-media-initialCrop");
        if (callback != null && callback.trim() !== "") {
            $('#' + iframeId).load(function () {
                eval(callback);
            });
        }

    });//$("[data-media-upload]").click()
}

//function ajaxSubmitMediaFile() {
//    if (UploadControl.fn_Validate && !UploadControl.fn_Validate())
//        return false;

//    if (UploadControl.fn_DoUpload) {
//        UploadControl.fn_DoUpload();
//    }
//}
//End: ajaxSubmitMediaFile()
//function DoUpdatingDataAfterUploading(errorCode, resultfileName, mediaServer) {
//            if (errorCode == 0) {
//                var btnUploadThumbnail = $("[data-upload-running]").first();
//                $("#dialogUploadMedia").dialog("close");
//                $(btnUploadThumbnail).attr("data-upload-running", "1");
//                $(btnUploadThumbnail).attr("data-media-uploadedfileName", resultfileName);
//				$(btnUploadThumbnail).attr("data-media-server", mediaServer);
//                var callback = $(btnUploadThumbnail).attr("data-media-callback");
//                if (callback != null && callback.trim() != "") {
//                    eval(callback);
//                }
//            }
//            else if (errorCode == 1) alert("Please select upload file");
//            else if (errorCode == -1) alert("Upload Fail: An exception occurred.");
//}
//End: DoUpdatingDataAfterUploading(errorCode, resultfileName)

function SwitchUser(token, returnUrl, siteName) {
    $.ajax({
        type: "POST",
        url: UrlAction(SiteURL.Home_LoginByToken, { token_holder: token, return_Url: returnUrl, base_Url: window.location.href }, siteName),
        success: function (msg) {
            if (msg != null) {
                window.location.href = msg.ReturnUrl;
            }
            else {
                window.location.href = window.location.href;
            }
        }
    });
}

$(document).ready(function () {
    RequireLoginPopup();
    PressEnter();
    showCookieNotice();
})

function showCookieNotice() {
    var isUsingCookie = document.cookie && document.cookie != "";
    if (isUsingCookie) {

        var cookieName = "showNotice";
        var siteId = SiteConfig.SiteId;
        var contestId = SiteConfig.ContestId;

        if (siteId > 0 && contestId > 0) {
            var noticeName = cookieName.concat("_", siteId, "_", contestId);
            saveCookieNotice(noticeName);
            return;
        }

        if (siteId > 0) {
            var noticeName = cookieName.concat("_", siteId);
            saveCookieNotice(noticeName);
            return;
        }

        if (contestId > 0) {
            var noticeName = cookieName.concat("_", contestId);
            saveCookieNotice(noticeName);
            return;
        }
    }
}

function saveCookieNotice(noticeName) {
    if ($.cookie(noticeName) == null || !$.cookie(noticeName)) {
        $.cookie(noticeName, true, { path: "/" });

        if (SiteLanguage.CookieNotice != "")
            showDialog({
                title: "",
                modal: true,
                message: SiteLanguage.CookieNotice
            });
    }
}

function PressEnter() {
    $(".pressenter").unbind("keypress");
    $(".pressenter").keypress(function (event) {
        var charCode = event.keyCode || event.which;
        if (charCode == 13) {
            var handler = $(this).attr("enter-handler") != undefined ? $(this).attr("enter-handler") : $(this).attr("enterhandler");
            if (handler)
                eval(handler);
        }
    });
}

function searchLirary() {
    if (window.getData) {
        getData();
    }
    else {
        location.href = SiteConfig.gSiteAdrs + "/Contest/Match/" + SiteConfig.CampaignID + "?keyWord=" + $("#txtSearch").val();
    }
}

function onCommentPageChange(dReferID, dCommentType, dPage, dContainer, dControl) {
    if (dControl)
        $(dControl).parent().remove();

    var configsection = $("#dvComments_" + dContainer + "_Config");

    var url = UrlAction(SiteURL.Comment_GetList,
                            {
                                "ReferIDHolder": dReferID,
                                "CommentTypeHolder": dCommentType,
                                "pageHolder": dPage
                            });
    $.ajax({
        type: "POST",
        url: url,
        data: {
            Container: dContainer,
            CommentWidth: $("input[name='CommentWidth']", configsection).val()
        },
        success: function (msg) {

            $(".postmessagesection", $("#dvComments_" + dContainer)).remove();

            if (dPage == 1) {
                $("#dvComments_" + dContainer)
                .html(msg);
            }
            else {
                $("#dvComments_" + dContainer)
                .append(msg);
            }

        }
    });

    return false;
}

function postCommentByType(event, dControl, allowPost) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    var _allowPost = allowPost != undefined ? 1 : 0;
    if (keycode == 13 || _allowPost == 1) {
        var ctl = $(dControl);
        if ($.trim(ctl.val()) != "") {

            var postedData = {
                ReferID: ctl.attr("param_referid"),
                CommentType: ctl.attr("param_commenttype"),
                Comment: ctl.val(),
                Container: ctl.attr("param_container"),
                QuestionID: ctl.attr("param_questionid")
            };

            ctl.val("");

            $.ajax({
                type: "POST",
                url: SiteURL.Comment_Post,
                data: postedData,
                success: function (msg) {
                    if (msg == "Pending") {
                        alert("Your comment was posted. Please waiting for approval");
                    }

                    if (msg == "OK" || msg == "Pending") {
                        onCommentPageChange(postedData.ReferID, postedData.CommentType, 1, postedData.Container, null);
                    }
                    else if (msg == "CanNotPost") {
                        alert("You can not post the message.");
                    }
                }
            });

        }

        return false;
    }
}

function drawMyFiles(config) {
    /*
    config:
        FolderID,
        FileType,
        Container,
        FileRenderOnSuccess,
        SearchControl
    */

    var Container = null;
    if (typeof config.Container === "string")
        Container = $("#" + config.Container);
    else
        Container = $(config.Container);

    if (config.sortBy == null)
        config.sortBy = "CreatedDate";

    if (config.sortDirection == null)
        config.sortDirection = "DESC";

    var search = "";
    if (config.SearchControl != "") {
        search = $("#" + config.SearchControl).val();
    }

    $.ajax({
        type: "POST",
        url: SiteURL.UploadFile_MediaList,
        data: {
            fileType: config.FileType,
            FolderID: config.FolderID,
            UserID: config.UserID,
            FileRenderOnSuccess: config.FileRenderOnSuccess,
            search: search,
            page: 1,
            pageSize: 8,
            sortBy: config.sortBy,
            sortDirection: config.sortDirection
        },
        success: function (msg) {
            Container.html(msg);
            if (Container.find(".thumbnail").length > 0) {
                $("#dvMediaSearch").show();
            } else
                $("[id^='dvTitle']").hide();

            if (config.FileRenderOnSuccess != "") {
                eval(config.FileRenderOnSuccess + "(" + config.FolderID + ")");
            }
        }
    });
}

function isDateTime(dtStr) {
    var d = Date.parse(dtStr);
    var message = "Please enter valid date and time";
    if (d == null || isNaN(d)) {
        //alert(message);
        return false;
    }
    else {
        var parts = dtStr.split('/');
        if (parts.length < 3) {
            //alert(message);
            return false;
        }
        else {
            var day = parseInt(parts[1]);
            var month = parseInt(parts[0]);
            var year = parseInt(parts[2]);

            if (isNaN(day) || isNaN(month) || isNaN(year)) {
                //alert(message);
                return false;
            }
            if (day < 1 || year < 1) {
                //alert(message);
                return false;
            }
            if (month > 12 || month < 1) {
                //alert(message);
                return false;
            }

            if ((month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12) && day > 31) {
                //alert(message);
                return false;
            }
            if ((month == 4 || month == 6 || month == 9 || month == 11) && day > 30) {
                //alert(message);
                return false;
            }
            if (month == 2) {
                if (((year % 4) == 0 && (year % 100) != 0) || ((year % 400) == 0 && (year % 100) == 0)) {
                    if (day > 29) {
                        //alert(message);
                        return false;
                    }
                } else {
                    if (day > 28) {
                        //alert(message);
                        return false;
                    }
                }
            }


            var strtimer = parts[2].trim().split(' ');

            if (strtimer.length > 1) {
                var times = strtimer[1].trim().split(':');
                var hours = parseInt(times[0]);
                var mins = parseInt(times[1]);
                var seconds = parseInt(times[2]);
                if (isNaN(hours) || isNaN(mins)) {
                    //alert(message);
                    return false;
                }
                else {
                    if (times.length == 3) {
                        if (isNaN(seconds))
                            return false;
                        else if (seconds < 0 || seconds > 59)
                            return false;
                    }
                    if (hours >= 0 && hours <= 23 && mins >= 0 && mins <= 59) {
                        return true;
                    }
                    else {
                        //alert(message);
                        return false;
                    }
                }
            }

            return true;
        }
    }

    return true;
}

function site_campaignMenu(applicationId, campaignId) {
    $("#liSiteDetails").hide();
    $("#liSiteDesign").hide();
    $("#liSitePublish").hide();

    if (applicationId >= 0 && campaignId == 0) {
        $("#liSiteDetails").show();
        $("#liSiteDesign").show();
        $("#liSitePublish").show();
    }
}

function RequireLoginPopup() {
    if ($("#dvRequireLoginPopup").length == 0) {
        $("body")
            .first()
            .append("<div id='dvRequireLoginPopup' class='requireloginpopup' onmouseover='$(this).show()' onmouseout='$(this).hide();' style='display:none;position:absolute;z-index:1050;padding:15px 0px;'></div>");

        var lnkLogin = SiteURL.Home_CustomerLogin;
        var lnkRegister = SiteURL.Customer_Register;

        $("<span>Please <a href='" + lnkLogin + "'>Login</a> or <a href='" + lnkRegister + "'>Register</a></span>").appendTo("#dvRequireLoginPopup");
    }

    $(".requirelogin")
        .unbind()
        .hover(function () {
            var os = $(this).offset();
            var h = $(this).height();

            $("#dvRequireLoginPopup").css({
                top: (os.top + $(this).outerHeight() - 5) + 'px',
                left: (os.left + ($(this).outerWidth() / 2) - ($("#dvRequireLoginPopup").width() / 2)) + 'px'
            }).show(300);

        }, function () {
            $("#dvRequireLoginPopup").hide();
        })
}

function collapseLeftMenu(isCollapse) {
    $.ajax({
        method: "POST",
        url: SiteURL.Home_DisplayLeftMenu,
        async: false,
        success: function (data) {
            $("#dashboard_page").removeClass("open-left-login");
            $("#dashboard_page").removeClass("marg-left-override-login");
            $("#divLeftMenuHide").hide();
            $("#divLeftMenuShow").hide();

            if (isCollapse == 0) {
                $("#divLeftMenuShow").show();
                $("#dashboard_page").addClass("open-left-login");
            }
            else {
                $("#divLeftMenuHide").show();
                $("#dashboard_page").addClass("marg-left-override-login");
            }
            if ($('.wrapper1').length > 0)
                $('.wrapper1,.wrapper2').width($('.content-inner').width() - 8)
            if ($('.has-dblScroll').size() > 0)
                $('.has-dblScroll').doubleScroll();
        }
    });
}

function leftMenuCollapse(menuItem) {
    $(".accordion-heading [aria-expanded='true']").each(function () {
        if ("#" + menuItem != $(this).attr("href")) {
            $(this)
                .addClass("collapsed")
                .attr("aria-expanded", false);

            var bodyId = $(this).attr("href");
            $(bodyId)
                .removeClass("in")
                .attr("aria-expanded", false)
                .css("height", 0);
        }
    });
    //$.ajax({
    //    method: "POST",
    //    url: SiteURL.Home_DisplayLeftMenu,
    //    data: { menuItem: menuItem },
    //    async: false,
    //    success: function (data) {
    //    }
    //});
}

function UpdateContestStatus(contestId) {
    if ($(".contest_status").length > 0 &&
        $(".contest_status").css("display") != "none" && $(".contest_status").parent().css("display") != "none") {
        setInterval(function () {
            $.ajax({
                type: "POST",
                url: SiteURL.Contest_GetStatus,
                data: {
                    id: contestId
                },
                success: function (msg) {
                    $(".contest_status").parent().html(msg);
                }
            });
        }, 5000);
    }
}

if (SiteConfig.CurrentUserID > 0 && SiteConfig.ShowTimeout == "1") {

    var SessionTimeoutConfig = {
        t: null,  //keep timeout object ID
        iDefTimeout: SiteConfig.SessionTimeout,
        sessionTimeout: (SiteConfig.SessionTimeout * 60),
        sessionClock: "",
        iAlertSessionTimeOutInSecond: SiteConfig.ShowTimeOutInSeconds,
        orgMsg: 'Your session ends in {xxx} seconds. <br>Please click on Refresh to maintain your session',
        isShowTimeoutDiv: false,
        keepAliveTime: 60000 * 5,
        PreTimeoutEvent: {}
    }

    function showTimeoutDiv() {
        SessionTimeoutConfig.isShowTimeoutDiv = true;

        //if ($("#divTimeoutParent").length == 0) {
        //    var dvTimeoutHtml = "";
        //    dvTimeoutHtml += "<div id='divTimeoutParent' valign='middle' align='center' style='display:none'>"
        //    dvTimeoutHtml += "    <div id='timout-container'><div id='timeoutmsg' class='authenticate_link' style='margin-top:10px;padding: 5px'> <span id='spanCountDown'></span></div>"
        //    dvTimeoutHtml += "    <div style='margin-top:5px;;padding: 5px 5px 10px 5px'>"
        //    dvTimeoutHtml += "        <input type='button' value='Refresh' id='btnResetTimeout' onclick='resetSession();HideTimeout();return false;'/>"
        //    dvTimeoutHtml += "        <input type='button' value='Close'  id='btnHideTimeOutMsg' onclick='HideTimeout();return false;'/>"
        //    dvTimeoutHtml += "    </div> </div>"
        //    dvTimeoutHtml += "</div>"

        //    $("body").append(dvTimeoutHtml);
        //}

        //$("#divTimeoutParent").css({ width: "100%", height: "auto", overflow: "none" });
        //$("#divTimeoutParent").dialog({ autoOpen: false, width: 400, height: 'auto', modal: false, zIndex: 35000, close: function (event, ui) { } });
        //$("#divTimeoutParent").dialog("open");

        if ($("#divTimeoutParent").length == 0) {
            var dvTimeoutHtml = "";
            dvTimeoutHtml += "<div id='divTimeoutParent' class='modal fade' role='dialog'>";
            dvTimeoutHtml += "  <div class='modal-dialog modal-sm' style='background: white'>";
            dvTimeoutHtml += "      <div class='modal-body'>";
            dvTimeoutHtml += "          <div id='timout-container'><div id='timeoutmsg' class='authenticate_link' style='margin-top:10px;padding: 5px'> <span id='spanCountDown'></span></div></div>";
            dvTimeoutHtml += "      <div class='modal-footer'>";
            dvTimeoutHtml += "          <input type='button' class='btn btn-info' value='Refresh' id='btnResetTimeout' onclick='resetSession();HideTimeout();return false;'/>";
            dvTimeoutHtml += "          <input type='button' data-dismiss='modal' class='btn btn-info' value='Close'  id='btnHideTimeOutMsg' onclick='HideTimeout();return false;'/>";
            dvTimeoutHtml += "      </div>";
            dvTimeoutHtml += "  </div>";
            dvTimeoutHtml += "</div>";
            $("body").append(dvTimeoutHtml);
        }

        $('.modal').on('show.bs.modal', function () {
            var dialog = $("#divTimeoutParent").find('.modal-dialog');
            dialog.css("margin-top", Math.max(0, ($(window).height() - dialog.height()) / 3));
        });
        $('#divTimeoutParent').on('hidden.bs.modal', function () {
            location.href = SiteURL.Home_Logout;
        });

        $("#divTimeoutParent").modal("show");
    }

    function AbandonSession() {
        $.ajax({
            type: "POST",
            url: SiteURL.Home_Logout,
            data: {
                DoNotRedirect: "1"
            },
            success: function (msg) {
                if (msg == "OK") {
                    //show message box
                    $("#divTimeoutParent").find('#btnResetTimeout').hide();
                    $("#divTimeoutParent").find('#btnHideTimeOutMsg').val('OK');
                    $("#timout-container").html(SiteLanguage.gSessionTimeoutMessage);
                    //clear timeout
                    window.clearTimeout(SessionTimeoutConfig.t);
                }
            }
        });
    }
    function HasPreTimeoutEvent() {
        for (x in PreTimeoutEvent) {
            return true;
        }
        return false;
    }

    function DisplaySessionTimeout() {
        var now = new Date();
        var elapseSecond = parseInt((now.getTime() - $.cookie("sessionClock")) / 1000);

        if (elapseSecond >= SessionTimeoutConfig.iAlertSessionTimeOutInSecond) {
            if (SessionTimeoutConfig.isShowTimeoutDiv == false)
                showTimeoutDiv();
        }
        else {

            if ($("#divTimeoutParent").is(":visible"))
                //$("#divTimeoutParent").dialog("close");
                $("#divTimeoutParent").modal("close");
            SessionTimeoutConfig.isShowTimeoutDiv = false;
        }

        var remainTime = SessionTimeoutConfig.sessionTimeout - elapseSecond < 0 ? 0 : SessionTimeoutConfig.sessionTimeout - elapseSecond;

        if (remainTime == 1) {
            $("#spanCountDown").html(SessionTimeoutConfig.orgMsg.replace("{xxx}", remainTime).replace("seconds", "second"));
        }
        else {
            $("#spanCountDown").html(SessionTimeoutConfig.orgMsg.replace("{xxx}", remainTime));
        }

        //if session is not less than 0
        if (elapseSecond < SessionTimeoutConfig.sessionTimeout)
            SessionTimeoutConfig.t = window.setTimeout("DisplaySessionTimeout()", 1000); //call the function again after 1 second delay
        else {
            //$("#timout-container").html("Session has timed out. Please login again.");
            AbandonSession();
        }
    }

    $(document).ready(function () {
        var now = new Date();

        SessionTimeoutConfig.sessionClock = now.getTime();
        $.cookie("sessionClock", SessionTimeoutConfig.sessionClock, { path: "/" });

        $('#btnHideTimeOutMsg').click(function () {
            $("#divTimeoutParent").dialog("close");
        });

        $('#btnResetTimeout').click(function () {
            resetSession();
            $("#divTimeoutParent").dialog("close");
        });

        //call main function to display Time out message
        if (DisplaySessionTimeout)
            DisplaySessionTimeout();

        $(document).ajaxSend(function (evt, request, settings) {
            if (settings.url != SiteURL.Home_KeepAlive)
                resetClientSession();
        });
    });

    function HideTimeout() {
        //$("#divTimeoutParent").dialog("close");
        $("#divTimeoutParent").modal("close");
    }

    function resetSession() {
        $.ajax({
            type: "POST",
            url: SiteURL.Home_KeepAlive,
            success: function (data) {
                resetClientSession();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { }
        });
    }

    function resetClientSession() {
        if (window.SessionTimeoutConfig.sessionClock) {
            var now = new Date();
            SessionTimeoutConfig.sessionClock = now.getTime();
            $.cookie("sessionClock", SessionTimeoutConfig.sessionClock, { path: "/" });
        }
        SessionTimeoutConfig.isShowTimeoutDiv = false;
    }
}

function ShowVoteDetailAndComment(dContestantID, dContestantEntryID) {
    if (!dContestantEntryID)
        dContestantEntryID = 0;

    $.ajax({
        url: SiteURL.Contestant_VoteDetailAndComment,
        data: {
            ContestantID: dContestantID,
            ContestantEntryID: dContestantEntryID
        },
        success: function (html) {

            if ($("#dvShowVoteDetailAndComment").length == 0)
                $("body:first").append("<div id='dvShowVoteDetailAndComment' style='display:none' title='Vote Scores & Comments'></div>");

            $("#dvShowVoteDetailAndComment").html(html);

            $("#dvShowVoteDetailAndComment").dialog({
                modal: true,
                width: $(window).width() * 0.9,
                height: $(window).height() * 0.8
            });
            $("#dvShowVoteDetailAndComment").dialog("open");
        }
    });
}

function resizeCountdown(daysLen, idCounter) {
    //Resize countdown days width
    if (idCounter == null || idCounter == '') {
        idCounter = 'counter';
    }

    if (daysLen > 3) {
        var parentDiv = $("#" + idCounter).parent();
        var currentwidth = parentDiv.width();
        parentDiv.width(currentwidth + 20);
    }
}

function IsFacebookApp() {
    return (SiteConfig.IFrameMode == false && window.parent != window);
}

function resizeFBCanvasHeight() {
    if (window.FB)
        FB.Canvas.setSize({ width: 760, height: $("body").height() + 40 });
}

$(document).ready(function () {
    if (!(SiteConfig.IsPreview)) {
        if ($.trim($("#dvLeftMenu").html()) != '') {
            //$("#dvBodyContainer").attr("class", "container-fluid");
            //$("#dvHeader").attr("class", "container-fluid");
            //$("#dvFooter").attr("class", "container-fluid");
        }
        else {
            //$("#dvBodyContainer").attr("class", "container");
            //$("#dvHeader").attr("class", "container");
            //$("#dvFooter").attr("class", "container");
        }
    }
    $(".widget-footer").each(function () {
        if (isIE() < 9) {
            if ($(this).html() == '') {
                $(this).remove();
            }
        }
        else {
            if ($.trim($(this).html()) == '') {
                $(this).remove();
            }
        }

    });
    //fadeIn();



    $(".banner").each(function () {
        RotateBanner($(this));
    })

    $(".bannerscript").each(function () {

        var decoded = $.trim($(this).html())
                                    .replace(/&gt;/g, '>')
                                    .replace(/&lt;/g, '<');
        var str = "<script>";
        str += "<";
        str += "/script>";

        var newscript = $(str);
        newscript.append(decoded);
        newscript.appendTo("body:first");
    })


    $("#divLeftMenuShow").removeClass("hidden");
    $("#divLeftMenuHide").removeClass("hidden");
    if ($(".navbar-toggle").css("display") == "block") {
        if ($.trim($("#dvLeftMenu").html()).length > 0) {
            $("#divLeftMenuShow").hide();
            $("#divLeftMenuHide").show();
        }
    }

    if (isIE() <= 8) {
        if ($("#divLeftMenuShow").css("display") == "none") {
            //$("#dvLeftMenu").removeClass("col-xs-2");
            //$("#dvLeftMenu").addClass("col-xs-1");
            //$("#divMainContainer").addClass("col-xs-10");
        }
        else {
            //$("#dvLeftMenu").removeClass("col-xs-1");
            //$("#dvLeftMenu").addClass("col-xs-2");
        }

        $(".navbar-collapse").css("HEIGHT", "auto");



        if ($("#divLeftMenuShow").length == 0) {
            //$("#dvLeftMenu").removeClass("col-xs-2");
            //$("#dvLeftMenu").addClass("col-xs-1");
        }
    }

    $(".menuicon").click(function () {
        if ($(".pull-right").css("display") == "block") {
            $(".pull-right").hide();
        }
        else {
            $(".pull-right").show();
        }
    });

    $("#divLeftMenuHide").height($("#dashboard_page").height());
    $(".widget-header").each(function () {
        if (isIE() < 9) {
            if ($(this).html() == "") {
                $(this).css("padding", "0");
            }
        }
        else {
            if ($.trim($(this).html()) == "") {
                $(this).css("padding", "0");
            }
        }

    })

    //fadeOut();
    $("body").show();

    /*if (IsFacebookApp()) {

        if ($("fb-root").length == 0)
            $("body").prepend("<div id='fb-root'></div>");

        window.fbAsyncInit = function () {
            FB.init({
                appId: SiteConfig.FBShareAppID,
                xfbml: true,
                version: 'v2.2'
            });

            resizeFBCanvasHeight();

            if (window.FBReady)
                FBReady();
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) { return; }
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }*/
})

var bannerId = "";

function RotateBanner(objBanner) {
    if ($(".banneritem", $(objBanner)).length > 0) {
        var currentbanneritem = null, nextbanneritem = null;
        currentbanneritem = $(".banneritem:visible", $(objBanner));

        if (currentbanneritem.length == 0) { // first time 
            nextbanneritem = $(".banneritem:first", $(objBanner));

        }
        else {
            nextbanneritem = currentbanneritem.next();
            if (nextbanneritem.length == 0)
                nextbanneritem = $(".banneritem:first", $(objBanner));
        }

        var interval = parseInt(nextbanneritem.attr("banner-interval"));
        currentbanneritem.hide();
        nextbanneritem.show();

        var video = $("video", currentbanneritem);
        if (video.size() > 0)
            video.get(0).pause();

        window.setTimeout(RotateBanner, interval * 1000, objBanner);
    }
}

window.onload = function () {
    $("#divLeftMenuHide").height($("#dashboard_page").height());
    //fadeOut();
}

function isIE() {
    var myNav = navigator.userAgent.toLowerCase();
    if (myNav.indexOf('msie') != -1) {
        var ieVersion = parseInt(myNav.split('msie')[1]);

        if (ieVersion <= 7) {
            $("body").html("");
            var html = "";
            html += "<div style='position:absolute; top: 40%; width: 100%; text-align: center;'>";
            html += "   <h1>Click the icon to upgrade your browser</h1>";
            html += "   <a href='http://windows.microsoft.com/en-us/internet-explorer/download-ie'>";
            html += "       <img src='" + SiteContent.BrowserIE + "' style='width: 150px'>";
            html += "   </a>";
            html += "</div>"

            $("body").html(html);
        }
        return ieVersion;
    };

    return false;
}

function OpenInfoDetail(obj, title) {
    $("#" + obj).dialog({
        width: 800,
        height: 600,
        modal: true,
        autoOpen: false,
        close: function (e, u) {
            $("#" + obj).dialog("destroy");
        }
    });
    $("#" + obj).dialog("option", "title", title);
    $("#" + obj).dialog("open");
}

function ensureExist(id, cssConfig) {

    if ($("#" + id).length == 0) {
        var newE = $("<div></div>");
        newE.attr('id', id);
        if (!!cssConfig)
            newE.css(cssConfig);

        newE.appendTo($("body:first"));

        return newE;
    }
    else {
        return $("#" + id);
    }
}

function runLater(condition, callback, timeout) {
    if (condition() == true) {
        callback();
    }
    else {
        window.setTimeout(function () {
            runLater(condition, callback, timeout);
        }, timeout);
    }
}

function getLineHeight() {
    var testLineHeight = null;
    if ($('#testLineHeight').length > 0) {
        testLineHeight = $('#testLineHeight');
    }
    else {
        testLineHeight = $("<div id='testLineHeight' style='display:none'>T</div>");
        testLineHeight.appendTo('body');
    }

    return testLineHeight.height();
}

function appendTable(tableObj) {
    var $firstTable = $(tableObj).first();
    $(tableObj).not(":first").each(function () {
        $firstTable.find("tbody").append($(this).find("tbody").html());
        $(this).remove();
    });
}

function disableFeaturesForBigData(obj) {
    $(obj).find("table thead th, table thead td").each(function () {
        var aValue = $(this).find("a").html();
        if ($.trim(aValue) != "") {
            $(this).html(aValue);
        }
    });
}

//Holds a reference to the YouTube player
var ik_player;

//this function is called by the API
var ytID = "";
function onYouTubeIframeAPIReady() {
    //creates the player object
    ik_player = new YT.Player('ik_player_iframe');

    //console.log('onYouTubeIframeAPIReady');

    //subscribe to events
    ik_player.addEventListener("onReady", "onYouTubePlayerReady");
    ik_player.addEventListener("onStateChange", "onYouTubePlayerStateChange");
    //ik_player.addEventListener("onStateChange", function (event) {
    //    console.log(count);
    //    if (event.data == 0) {
    //        count++;
    //        $.cookie("prv", "1");
    //        PlayDefaultMedia(true);
    //        InitVote();
    //    }
    //});
}

function onYouTubePlayerReady(event) {
    //console.log('onYouTubePlayerReady');
    //if (!isAppleDevice())
    //    event.target.playVideo();   

}

function onYouTubePlayerPlaying(event) {
    //console.log(document.getElementById("ik_player_iframe").getCurrentTime());
    //console.log("onYouTubePlayerPlaying");
}

function onYouTubePlayerStateChange(event) {
    //console.log("onYouTubePlayerStateChange");
    var player = $("#ik_player_iframe");
    //console.log($("#ik_player_iframe").attr("src"));
    var Preroll = window.Contestant != null ? Contestant.Preroll : "BPISMQbro3k";

    if (ytID.indexOf(Preroll) > -1 && Preroll != "") {

        //console.log("onYouTubePlayerStateChange1");
        if (event.data == 0) {
            $.cookie("prv", "1");
            if (window.PlayDefaultMedia) {
                PlayDefaultMedia(true);
                InitVote();
            }
        }
    }
    //else {
    //    console.log("onYouTubePlayerStateChange2");
    //    if (Contestant.Votable == '1' && Contestant.IsFan == '1')
    //        if (!Contestant.PressedPlay) {
    //            Contestant.PressedPlay = true;
    //            countDownToVote();
    //        }
    //}
}

//Vimeo events
function onPause(id) {
    //status.text('paused');
}

function onPlay(id) {
    //console.log("played");
    //if (Contestant.Votable == '1' && Contestant.IsFan == '1')
    //    if (!Contestant.PressedPlay) {
    //        Contestant.PressedPlay = true;
    //        countDownToVote();
    //    }
}

function onFinish(id) {
    //status.text('finished');
}

function onPlayProgress(data, id) {
    //status.text(data.seconds + 's played');
}
//Vimeo events

function pad(num, size) {
    var s = num + "";
    while (s.length < size) s = "0" + s;
    return s;
}

function writeLog(s) {
    //if (SiteConfig.debug == "1") {
    //    if ($("txtDebug").length == 0)
    //        $("body").append("<div id='txtDebug' style='height:50px; position:fixed;left:0; top:0; border:1px solid gray'><button id='btnDebugClear'>Clear</button></div>");
    //    $("txtDebug").html(s + "<br/>" + $("txtDebug").html());
    //}
}

serialize = function (obj) {
    var str = [];
    for (var p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        }
    return str.join("&");
}

function ShowPopupFormReview_v2(displayItem, registerHeader, registerFormHeader, submitButton, resourceKey) {
    var dvFormReview = "<div id='dvFormReview' style='display:none;'></div>";
    if ($("#dvFormReview").length == 0)
        $("body").first().append(dvFormReview);

    $("#dvFormReview").html(displayItem);
    $("#txtTitle").text(registerHeader);
    $("#txtRegisterFormHeader").text(registerFormHeader);
    //$("#txtResourceKey").text(resourceKey);
    $("#btnRegister").text(submitButton);
    $("#dvFormReview")
        .dialog({
            modal: true,
            width: 600,
            height: 700,
            title: "Form Preview",
            buttons: {
                Close: {
                    text: 'Close',
                    click: function () {
                        $(this).dialog("close");
                    }
                }
            },
            close: function (event, ui) {
                $(".ui-dialog-titlebar-close").click();
            }
        });
}

function ShowFormPreview_v2(urlDisplayItem, urlResource) {

    var displayItem = "";
    var registerHeader = "";
    var registerFormHeader = "";
    var submitButton = "";
    var resourceKey = "";


    $.ajax({
        url: urlDisplayItem,
        type: 'GET',
        cache: false,
        success: function (result) {
            displayItem = result;

            $.ajax({
                url: urlResource,
                type: 'GET',
                cache: false,
                success: function (result) {
                    if (result != null) {
                        for (var i = 0; i < result.length; i++) {
                            if (result[i].ResourceKey == "ButtonRegister") {
                                submitButton = result[i].Value;
                            }
                            else if (result[i].ResourceKey == "Register_Instruction") {
                                registerFormHeader = result[i].Value;
                            }
                            else if (result[i].ResourceKey == "Register_Header") {
                                registerHeader = result[i].Value;
                                resourceKey = result[i].ResourceKey;
                            }
                        }

                        ShowPopupFormReview_v2(displayItem, registerHeader, registerFormHeader, submitButton, resourceKey);
                    }
                }
            });
        }
    });
}

function showChangePhotoDialog() {

    if ($("#changephoto_dialog").length == 0) {
        $("body").append("<div id=\"changephoto_dialog\" style=\"display:none\"></div>");
    }

    $("#changephoto_dialog").load(getRoot() + "/MyAccount/_ChangeMemberPhoto");
    $("#changephoto_dialog").dialog({
        autoOpen: false,
        height: 300,
        width: Math.min(600, $(document).width() - 20),
        modal: true
    });
    $("#changephoto_dialog").dialog("open");

    var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    if (isMobile) {
        $(".nav-togle-btn").click();
    }
}

function getRoot() {
    if (SiteConfig.gSiteAdrs.toLowerCase().indexOf(SiteConfig.SiteName.toLowerCase()) > -1)
        return SiteConfig.gSiteAdrs;
    else
        return SiteConfig.gSiteAdrs + SiteConfig.SiteName;
}

ContestantService = {
    showContestantPopup: function (params) {
        //params: { contestantID: 0, contestName: "" }
        $.ajax({
            url: SiteURL.Prefix + "/contestant/Details",
            data: { id: params.contestantID, isPartial: 1, contestid: params.contestid ? params.contestid : 0 },
            success: function (response) {
                var footerContent = '';
                if ($("#dvContestant").size() == 0) {
                    dvContestant = $("<div id='dvContestant' class='page-contest-details' style='display:none'></div>").appendTo("body");
                    footerContent = '<div class="row"><div class="col-xs-6 col-sm-6 col-md-6 text-left"><button type="button" class="btn btn-primary btn-xs" data-dismiss="modal">SEE ALL SUBMISSIONS</button></div><div class="col-xs-6 col-sm-6 col-md-6"><button type="button" class="btn btn-default btn-xs" data-dismiss="modal">CLOSE</button></div></div>';
                }
                $("#dvContestant").empty();
                $("#dvContestant").html(response);
                // modal is not opening.
                var modalContainer = $("#dvContestant").closest(".modal");
                if (modalContainer.size() == 0 || !modalContainer.hasClass("in")) {
                    $("#dvContestant").dialog({
                        lgClass: 1,
                        title: params.contestName,
                        customClass: "modal-box",
                        footerContent: footerContent
                    });
                }
                initPage();
                if (params.callBack) params.callBack();
            }
        });

    },

    showContestant: function (contestant) {

        var dvContestant = $("#dvContestant");
        var url = SiteConfig.gSiteAdrs + "/contestant/_details";

        if ($("#dvContestant").length == 0) {
            dvContestant = $("<div id='dvContestant' class='modal fade modal-winnerpopup'></div>").appendTo("body");
        }
        else {
            dvContestant.find('.modal-dialog').remove();
        }
        dvContestant.modal("show");
        $('body').addClass('scroll-fixed');

        $.ajax({
            type: "POST",
            url: url,
            async: true,
            data: {
                contestantID: contestant.ContestantID
            },
            success: function (html) {
                if ($("#dvContestant").length == 0 || $("#dvContestant").find('.modal-dialog').size() == 0) {
                    //$("body").append(html);
                    $("#dvContestant").append($(html).find('.modal-dialog'));
                }
                else {
                    var content = $(html).find('.modal-dialog');
                    $("#dvContestant").find('.modal-dialog').replaceWith(content);
                }

                dvContestant = $("#dvContestant");
                $('.modal-winnerpopup').on('hidden.bs.modal', function () {
                    $("iframe", "#dvContestant").remove();
                    $('#myVideo').remove();
                    $('#dvSponsor').css({
                        'overflow-x': 'hidden',
                        'overflow-y': 'auto',
                        'background': 'rgba(0,0,0,0.8)'
                    });
                    hideDivShare();
                    $('body').removeClass('scroll-fixed');
                });

                if ($("#myVideo").attr("src") != undefined && !isURL($("#player").attr("src"))) {
                    $("#playerWrapper").css("height", $("#player").css("height")).prepend("<div class='unavailable-message'>This Rant is not available</div>");
                    $("#player").remove();
                }
                var visible_modal = $("#dvContestant").is('.modal.in'); // modalID or undefined
                if (!visible_modal) { // modal is active
                    dvContestant.modal("show");
                }
            }
        });

        if ($("#player").length > 0 || $("#playerVimeo").length > 0) {
            $("#ViewCnt").html(parseInt($("#ViewCnt").html(), 10) + 1);
            console.log('here');
            this.updateCount({ ContestantID: contestant.ContestantID, CountType: 1 });
        }

    },
    videoPlaying: function (obj) {
        if (obj.currentTime >= obj.duration / 2 && $(obj).attr("data-viewed") == "0") {
            $(obj).attr("data-viewed", 1)
            $("#ViewCnt").html(parseInt($("#ViewCnt").html(), 10) + 1);
            this.updateCount({ ContestantID: $(obj).attr("data-ContestantID"), CountType: 1 })
        }
    },
    videoEnded: function (obj) {
        $(obj).attr("data-viewed", 0);
    },
    updateCount: function (contestant) {
        var countUrl = SiteConfig.gSiteAdrs + "/contestant/UpdateCount";

        $.ajax({
            type: "POST",
            url: countUrl,
            data: {
                contestantID: contestant.ContestantID,
                countType: contestant.CountType
            },
            success: function (msg) {
                $('.txtViewCount_' + contestant.ContestantID).html(msg + " Views");
            }
        });
    },

    vote: function (obj) {

        var contestID = $(obj).attr("contestid");
        var contestantId = $(obj).attr("contestantid");
        var actionid = $(obj).attr("actionid");

        if (SiteConfig.CurrentUserID == 0) {
            return;
        }

        $.ajax({
            type: "POST",
            url: SiteConfig.gSiteAdrs + "/contestant/Like",
            data: {
                ContestantID: contestantId,
                ActionID: actionid,
                ContestID: contestID
            },
            success: function (data) {
                if (data.result == "ok") {

                    var likeObj = $("#like_" + contestantId);
                    var dislikeObj = $("#dislike_" + contestantId);

                    likeObj.find('#FanCnt').text(data.data.FanCnt);
                    dislikeObj.find('#DislikeCnt').text(data.data.DislikeCnt);
                    if (data.data.userFanCnt > 0)
                        likeObj.find('.fa-thumbs-up').addClass("redIcon");
                    else
                        likeObj.find('.fa-thumbs-up').removeClass("redIcon");
                    if (data.data.userDislikeCnt > 0)
                        dislikeObj.find('.fa-thumbs-down').addClass("redIcon");
                    else
                        dislikeObj.find('.fa-thumbs-down').removeClass("redIcon");
                }
            }
        });
    },
    favorite: function (obj) {

        if (SiteConfig.CurrentUserID == 0) {
            return;
        }

        var newStatus = $(obj).attr("status") == "1" ? 0 : 1;
        $.ajax({
            type: "POST",
            url: $(obj).attr("url"),
            data: {
                contestantID: $(obj).attr("contestantID"),
                contestID: $(obj).attr("contestID"),
                adding: newStatus
            },
            success: function (data) {
                $(obj).attr("status", newStatus);
                $("[name='heart']").toggleClass("toggle-favorite");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    },
    shelterFavorite: function (elem, obj, callback) {

        if (SiteConfig.CurrentUserID == 0) {
            return;
        }

        $.ajax({
            type: "POST",
            url: obj.url,
            data: {
                applicationId: obj.applicationId,
                shelterId: obj.shelterId,
                favoriteId: obj.favoriteId
            },
            success: function (data) {
                if (data.result == 1) { //OK
                    if ($(elem)) {
                        $(elem).text("My Favorite");
                        $(elem).prop("onclick", null);
                    }
                    if (callback) callback();
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    },
    getNextPre: function (obj) {
        if (obj.ContestantID) {
            $.ajax({
                type: "POST",
                url: SiteConfig.gSiteAdrs + "/contestant/GetNextPre",
                async: false,
                data: {
                    contestID: obj.ContestID,
                    ParentID: obj.ParentID,
                    contestantID: obj.ContestantID
                },
                success: function (result) {
                    if (result) {
                        if (result.PrevID) {
                            if (obj.prevWrapper) {
                                obj.prevWrapper.data('contestantid', result.PrevID);
                                obj.prevWrapper.show();
                            }
                        }
                        else {
                            obj.prevWrapper.data('contestantid', 0);
                        }

                        if (result.NextID) {
                            if (obj.nextWrapper) {
                                obj.nextWrapper.data('contestantid', result.NextID);
                                obj.nextWrapper.show();
                            }
                        }
                        else {
                            obj.nextWrapper.data('contestantid', 0);
                        }
                    }
                }
            });
        }
    }
}

function showTIme(sec) {
    var secNum = parseInt(sec, 10); // don't forget the second param
    var hours = Math.floor(secNum / 3600);
    var minutes = Math.floor((secNum - (hours * 3600)) / 60);
    var seconds = secNum - (hours * 3600) - (minutes * 60);

    if (minutes < 10) { minutes = "0" + minutes; }
    if (seconds < 10) { seconds = "0" + seconds; }
    return minutes + ':' + seconds;
}

//Abuse report begin
function reportPartial(reportData) {
    var dvContestant = $("#myModal");
    $.ajax({
        type: "POST",
        url: reportData.urlGetPartial,
        async: false,
        cache: false,
        success: function (html) {
            //$("#myModal").replaceWith(html);
            $("body").append(html);
            $('#myNavbar #myModal').remove();

            //$("#myModal").replaceWith(html);
            //$("#myModal").detach().appendTo('body');
            dvContestant = $("#myModal");
            //$('#myModal').on('hidden.bs.modal', function () {               
            //    $("#myModal").css("z-index", 999998);
            //    $('.modal.in').css({
            //        'overflow-x': 'hidden',
            //        'overflow-y': 'auto',
            //        'background': 'rgba(0,0,0,1)'
            //    });
            //});
            //$("#myModal").css("z-index", 1000000);
            dvContestant.modal("show");
            //validationReport();
            $('#btnSubmit').click(function () {
                if ($("#frmReportAbuse").valid()) {
                    $('#btnSubmit').attr('disabled', 'true');
                    var postData = {
                        ApplicationID: reportData.applicationId,
                        ContestID: reportData.contestId,
                        ContestantEntryID: reportData.contestantId,
                        Email: $('#txtEmail').val(),
                        Description: $('#txtDescription').val(),
                        Url: window.location.href,
                        UserID: reportData.userId,
                        UseType: reportData.userType,
                        IsHideContestantEntry: reportData.IsHideContestantEntry != undefined ? reportData.IsHideContestantEntry : false
                    };
                    $.ajax({
                        dataType: "HTML",
                        type: "POST",
                        data: postData,
                        url: reportData.urlInsertReport,
                        success: function (result) {
                            if (result === "Success") {
                                if (reportData.IsHideContestantEntry === true) {
                                    $('#myModal').modal('hide');

                                    if ($('#btnSkipAirbnb').length > 0)
                                        $('#btnSkipAirbnb').trigger('click');
                                    else {
                                        if (window.resultCallBack)
                                            resultCallBack();
                                    }                                    

                                } else {
                                    $('#txtEmail').val(reportData.userEmail);
                                    $('#txtDescription').val("");
                                    $('#btnSubmit').prop("disabled", false);
                                    $('#btnSubmit').hide();
                                    $('#divMessage').show();
                                    $('#divReport').hide();
                                }
                                //$('#btnCancel').click();
                            }
                        },
                        error: function (err) {
                            //console.log(err.status + " - " + err.statusText);
                            $('#btnSubmit').prop("disabled", false);
                        }
                    });
                }
            });
            $('#btnCancel').click(function () {
                RemoveError('txtEmail');
                RemoveError('txtDescription');
                $('#txtEmail').val(reportData.userEmail);
                $('#txtDescription').val("");
                $('#btnSubmit').prop("disabled", false);
                // $('#dvContestant').css('z-index', '1000000');

                //$("#myModal").css("z-index", 999998);
                $('.modal.in').css({
                    'overflow-x': 'hidden',
                    'overflow-y': 'auto',
                    'background': 'rgba(0,0,0,1)'
                });
            });
            //$("#txtEmail").on('keyup', function () {
            //    RemoveError('txtEmail');
            //    if ($.trim($('#txtEmail').val()) === '') {
            //        ShowError('txtEmail', 'Please input Email.');
            //    } else if (!validateEmail($('#txtEmail').val())) {
            //        ShowError('txtEmail', 'Invalid Email.');
            //    }
            //});

            //$("#txtDescription").on('keyup', function () {
            //    RemoveError('txtDescription');
            //    if ($.trim($('#txtDescription').val()) === '') {
            //        ShowError('txtDescription', 'Please input Description.');
            //        $('#txtDescription').focus();
            //    };
            //});

        }
    });
}

function validateEmail(email) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(email);
}

function Validation() {
    var flag = true;
    RemoveError('txtEmail');
    RemoveError('txtDescription');
    if ($.trim($('#txtEmail').val()) === '') {
        ShowError('txtEmail', 'Please input Email.');
        $('#txtEmail').focus();
        flag = false;
    } else if (!validateEmail($('#txtEmail').val())) {
        ShowError('txtEmail', 'Invalid Email.');
        $('#txtEmail').focus();
        flag = false;
    }
    if ($.trim($('#txtDescription').val()) === '') {
        ShowError('txtDescription', 'Please input Description.');
        if (!$('#txtEmail').is(":focus"))
            $('#txtDescription').focus();
        flag = false;
    };
    return flag;
}

function ShowError(id, errorStr) {
    var html = ' <span class="error">' + errorStr + '<span class="tooltip-arrow"></span></span>';
    $('#' + id + '-error').addClass('post-input').append(html);
}

function RemoveError(id) {
    $('#' + id + '-error').removeClass('post-input').html('');
}
//End Abuse report

//Begin Share
function ActionShare(dActionID, obj, options) {

    if (obj) {
        if ($(obj).attr("shared-callback") != null && dActionID + '' !== '1') {
            //eval($(obj).attr("shared-callback"));
            setActionDisable(obj, options);
        }
    }

    var link = SiteURL.Action_Share;
    var shareUrl = $(obj).attr("shared-url") ? $(obj).attr("shared-url") : $(obj).closest("[shared-url]").attr("shared-url");
    link = link.replace("actionidholder", dActionID);
    //link = link.replace("sharedurlholder", encodeURIComponent(GSharing.CurrentSharedUrl));
    link = link.replace("sharedurlholder", encodeURIComponent(shareUrl));

    if ($(obj).attr("shared-contestantid") != null && link.toLowerCase().indexOf("contestant/details") == -1) {
        link += "&ContestantID=" + $(obj).attr("shared-contestantid")

        //position
        var span = $("span[contestantId='" + $(obj).attr("shared-contestantid") + "']");
        if (span != null && span.attr("position") == "Coach") {
            link += "&position=Coach";
        }

    }

    if ($(obj).attr("shared-contestantentryid") != null) {
        link += "&ContestantEntryID=" + $(obj).attr("shared-contestantentryid")
    }

    if ($(obj).attr("shared-notdeactive") != null) {
        link += "&isNotDeActive=" + $(obj).attr("shared-notdeactive");
    }

    if ($(obj).attr("shared-sourceID") != null) {
        link += "&sourceID=" + $(obj).attr("shared-sourceID");
    }

    if ($(obj).attr("shared-contestId") != null) {
        link += "&contestID=" + $(obj).attr("shared-contestId") ;
    }
    else {
        link += "&contestID=" + SiteConfig.ContestID;
    }

    var w = 800;
    var h = 400;
    var left = (screen.width / 2) - (w / 2);
    var top = (screen.height / 2) - (h / 2);

    switch (dActionID + '') {
        case '1':
            var returndata;
            $.ajax({
                type: "post",
                url: link,
                async: false,
                success: function (data) {
                    returndata = data;
                }
            });

            if (returndata.shareThumbnail != "" && returndata.shareThumbnail.indexOf("http") < 0) {
                returndata.shareThumbnail = "http:" + returndata.shareThumbnail;
            }

            //Disable imediate
            //setActionDisable(obj, options);

            FB.ui(
                {
                    method: 'feed',
                    name: returndata.shareName,
                    picture: returndata.shareThumbnail,
                    description: returndata.shareDescription,
                    link: returndata.shareUrl,
                    display: "popup"
                },
                function (response) {
                    
                   
                    //Posting completed
                    if (response && !response.error_code) {
                        setActionDisable(obj, options);
                        updateShare({ obj: obj, TranID: returndata.shareID, status: 1, pt: 1 });
                        
                    } else {
                        //console.log('Error while posting.');
                    }
                }
            );

            break;
        default:

            //Disable imediate
            setActionDisable(obj, options);

            window.open(link, "", 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
            break;
    }
}

function updateShare(params, callback)
{
	$.ajax({
		type: "POST",
		url: SiteURL.Action_Update_Share,
		data: { 
		    TranID: params.TranID,
			status: params.status,
		    pt:params.pt
		},
		success: function () {
			if (callback){				
				if (callback.name == "setActionDisable")
				{				    
				    if ($(params.obj).attr("shared-notdeactive") == null) {
						//Removed disable tempararily
				        setActionDisable(params.obj, params);
					}
				}				
				else{
					callback(params);
				}				
			}			
		}
	});
}

function updateShareReturn(params, callback) {
    $.ajax({
        type: "POST",
        url: SiteURL.Action_Update_ShareReturn,
        //data: null,
        success: function (response) {
            if (callback) {
                callback(response);
            }
        }
    });
}

function setActionDisable(obj) {
    $(obj).unbind();

    $(obj).attr("disabled", "disabled");
    if ($(obj).hasClass("shared-disabled") == false) {
        $(obj).addClass("shared-disabled");
    }
    $(obj).removeAttr("ontouchend");
    $(obj).removeAttr("onclick");
    $(obj).removeAttr("target");
    $(obj).attr("href", "javascript:void(0);");

    var iconObj = $(obj).find('img');
    if (iconObj.size() > 0)
        iconObj.attr("src", iconObj.attr("disabled-src"));
}

function ShowShareByEmail(dActionID, obj) {

    GSharing.CurrentSharedUrl = $(obj).attr("shared-url") != undefined ? $(obj).attr("shared-url") : GSharing.CurrentSharedUrl;

    //if (obj) {
    //    if ($(obj).attr("shared-callback") != null) {
    //        setActionDisable(obj);
    //    }
    //}

    var uid = $(obj).attr("uid");

    //Contestant share
    var container = "#dvShareByEmail_" + uid;
    var contestantId = $(obj).attr("shared-contestantid");
    var sem = $("#textSystemMessage", container)
    if (contestantId > 0 && sem.length > 0) {
        var span = $("span[contestantId='" + contestantId + "']");
        sem.html($("#textSystemMessage", container).html().replace("%ContestantName%", span.html()));
        
        if (span.attr("position") == "Coach")
            sem.html(sem.html().replace("%players%", "coaches"));
        else
            sem.html(sem.html().replace("%players%", "players"));
    }
    if (sem.length > 0)
        $("a", container).replaceWith($("a", container).html());

    $("#dvShareByEmail_" + uid).dialog({
        modal: true,
        // width: 600,
        height: 400,
        autoOpen: false,
        position: "center",
        title: "Share with your friend"
    }).dialog("open");
    //$("#dvSharing").hide();


    $("#dgdvShareByEmail_" + uid).on('hidden.bs.modal', function () {
        $('#dvContestant').css('z-index', '999999');
        $('.modal.in').css({
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
            'background': 'rgba(0,0,0,1)'
        });
    });

    $("#btnSend", "#dvShareByEmail_" + uid).unbind("click").click(function () {

        var isvalid = true;
        var sendtolist = {};
        var hasfriend = false;
        var hasfriendEmail = true;
        if ($.trim($("#txtYourEmail", "#dvShareByEmail_" + uid).val()) == "") {

            //alert(SiteLanguage.gRequireYourEmail)
            ShowPopup({ content: '<span>' + SiteLanguage.gRequireYourEmail + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });
            
            $("#txtYourEmail", "#dvShareByEmail_" + uid).focus();
            return false;
        }
        if (!validateEmail($("#txtYourEmail", "#dvShareByEmail_" + uid).val())) {

            //alert(ValidatorMessages.email)
            ShowPopup({ content: '<span>' + ValidatorMessages.email + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });

            $("#txtYourEmail", "#dvShareByEmail_" + uid).focus();
            return false;
        }
        var numOfFriend = $("[id^='txtFriendName']", "#dvShareByEmail_" + uid).length;
        $(".friendsemail > div", "#dvShareByEmail_" + uid).each(function (i) {
            var index = i;
            var fname = $.trim($("#txtFriendName" + index, $(this)).val());
            var femail = $.trim($("#txtFriendEmail" + index, $(this)).val());
            if (fname !== "" || femail !== "") {
                if (fname !== "" && femail === "") {
                    isvalid = false;
                    if (numOfFriend === 1)
                        //alert(SiteLanguage.gRequireFriendEmail);
                        ShowPopup({ content: '<span>' + SiteLanguage.gRequireFriendEmail + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });
                    else
                        //alert(SiteLanguage.gRequireFriendEmail + index);
                        ShowPopup({ content: '<span>' + SiteLanguage.gRequireFriendEmail + index + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });

                    $("#txtFriendEmail" + index, $(this)).focus();
                    hasfriendEmail = false;
                    return false;
                }

                if (fname === "" && femail !== "") {
                    isvalid = false;
                    if (numOfFriend === 1)
                        //alert(SiteLanguage.gRequireFriendName);
                        ShowPopup({ content: '<span>' + SiteLanguage.gRequireFriendName + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });
                    else
                        //alert(SiteLanguage.gRequireFriendName + index);
                        ShowPopup({ content: '<span>' + SiteLanguage.gRequireFriendName + index + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });

                    $("#txtFriendName" + index, $(this)).focus();
                    hasfriendEmail = false;
                    return false;
                }
                if (!validateEmail(femail)) {
                    isvalid = false;

                    //alert(ValidatorMessages.email);
                    ShowPopup({ content: '<span>' + ValidatorMessages.email + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });

                    $("#txtFriendEmail" + index, $(this)).focus();
                    hasfriendEmail = false;
                    return false;
                }
                hasfriend = true;
                sendtolist[i] = { FriendName: fname, FriendEmail: femail };
            }
        });

        if (!hasfriend && isvalid) {

            //alert(SiteLanguage.gRequireAtLeastFriend);
            ShowPopup({ content: '<span>' + SiteLanguage.gRequireAtLeastFriend + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });

            return false;
        }

        if (!hasfriendEmail) return false;
        var link = UrlAction(SiteURL.Action_ShareByEmail,
                            {
                                "actionidholder": 1,
                                "sharedurlholder":  GSharing.CurrentSharedUrl,
                                "fromemailholder": $("#txtYourEmail", "#dvShareByEmail_" + uid).val()
                            });

        if ($(obj).attr("shared-contestantid") != null) {
            link += "&ContestantID=" + $(obj).attr("shared-contestantid");

            //position
            var span = $("span[contestantId='" + contestantId + "']");
            if (span!= null && span.attr("position") == "Coach") {
                link += "&position=Coach";
            }

        }

        if ($(obj).attr("shared-contestantentryid") != null) {
            link += "&ContestantEntryID=" + $(obj).attr("shared-contestantentryid");
        }

        if ($(obj).attr("shared-contestId") != null) {
            link += "&contestID=" + $(obj).attr("shared-contestId");
        }

        //sourceID
        if ($(obj).attr("shared-sourceID") != null) {
            link += "&sourceID=" + $(obj).attr("shared-sourceID");
        }
       

        if ($("#txtPersonalMessage", "#dvShareByEmail_" + uid).size() > 0 && $("#txtPersonalMessage", "#dvShareByEmail_" + uid).val()) {
            sendtolist.personalMessage = $("#txtPersonalMessage", "#dvShareByEmail_" + uid).val()            
        }

        $.ajax({
            type: "POST",
            url: link,
            data: sendtolist,
            success: function (msg) {
                if (msg === "OK") {

                    ShowPopup({ content: '<span>' + SiteLanguage.gSentSuccessfully + '</span><br/><br/><button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>' });

                    $(".friendsemail input").val("");
                    $("#dvShareByEmail_" + uid).dialog("close")
                    $(".modal").css("background", "transparent");

                    if (obj) {
                        if ($(obj).attr("shared-callback") != null) {
                            //eval($(obj).attr("shared-callback"));
                            setActionDisable(obj);
                        }
                    }
                }
            }
        });
    });

    $("#btnClose", "#dvShareByEmail_" + uid).unbind("click").click(function () {
        $("#dvShareByEmail_" + uid).dialog("close");
    });
}

function ShowShareLink(dActionID, obj) {
    var uid = $(obj).attr("uid");
    var mediaId = $(obj).attr("media-id");
    var videoPath = $(obj).attr("video-path");
    var thumbnailUrl = $(obj).attr("thumbnail-url");
    var thumbnailDefaultUrl = $(obj).attr("thumbnail-default-url");
    var shareLink = $(obj).attr("share-link-url");
    var shareLinkType = $(obj).attr("share-link-type");
    var contestantId = $(obj).attr("shared-contestantid");

    var container = "#dvShareLink_" + uid;
    var shareLinkBox = container + " #taShareLink";

    if (dActionID == 71) { //Link
        $(shareLinkBox).val(shareLink);
    }
    if (dActionID == 72) { //Embed
        var link = BindEmbedText(mediaId, videoPath, shareLinkType, thumbnailUrl, thumbnailDefaultUrl);
        $(shareLinkBox).val(link);
    }

    $(shareLinkBox).click(function () {
        copyToClipboard($(this));
    });

    $(container).dialog({
        modal: true,
        width: 500,
        height: 300,
        autoOpen: false,
        position: "center",
        title: dActionID == 71 ? "Get Link" : "Get Embed Code"
    }).dialog("open");

    $("#dgdvShareLink_" + uid).on('hidden.bs.modal', function () {
        $('#dvContestant').css('z-index', '999999');
        $('.modal.in').css({
            'overflow-x': 'hidden',
            'overflow-y': 'auto',
            'background': 'rgba(0,0,0,1)'
        });
    });
}

function BindEmbedText(videoId, videoPath, shareLinkType, thumbnailPath, thumbnailDefaultPath) {
    if (shareLinkType == 3) { // Video 
        var embed = "";
        var gVirtualUploadPath = SiteConfig.VirtualUploadPath + SiteConfig.UploadFolder + "/";

        var thumbnailUrl = !isHttpFile(thumbnailPath) ? (gVirtualUploadPath + thumbnailPath) : thumbnailPath;
        var videoUrl = !isHttpFile(videoPath) ? makeSureURL(gVirtualUploadPath + videoPath) : makeSureURL(videoPath);

        if (videoUrl.toLowerCase().indexOf("vimeo.com") >= 0) {
            embed = GetEmbedVimeo(videoUrl);
        }
        else {
            embed = GetEmbedYoutubeOrVideo(videoId, videoUrl, thumbnailUrl);
        }
        return embed;
    }
    if (shareLinkType == 1) { // Photo
        return '<img class="img-responsive" src="' + thumbnailPath + '"' + thumbnailDefaultPath + '/>';
    }
}

function GetEmbedVimeo(videoUrl) {
    loadJS(SiteURL.Root + "Scripts/vimeo.js");

    var videoId = Vimeo.GetVideoID(videoUrl);
    var embed = SiteConfig.VimeoPlayerEmbed;

    embed = embed.replace("VIDEO_ID", videoId);
    embed = embed.replace("EMBED_WIDTH", "100%");
    embed = embed.replace("EMBED_HEIGHT", "100%");
    return embed;
}

function GetEmbedYoutubeOrVideo(videoId, videoUrl, thumbnailUrl) {
    if (videoUrl.toLowerCase().indexOf("youtube") == -1) { // Youtube
        var fileName = videoUrl.substring(0, videoUrl.lastIndexOf("."));
        var webmUrl = fileName + ".webm";
        var mp4Url = fileName + ".mp4";

        embed = '<video id="' + videoId + '" width="100%" height= "100%" controls="controls" poster="' + thumbnailUrl + '">';
        embed += '<source src="' + mp4Url + '" type="video/mp4" />';
        embed += '<source src="' + webmUrl + '" type="video/webm" />';
        embed += '</video>';
    }
    else { // Upload Video
        embed = '<iframe id="ik_player_iframe" frameBorder="0" width="100%" height="100%" ';
        embed += 'src="' + videoUrl.replace("/v/", "/embed/").replace("http://", "https://") + '?enablejsapi=1&autoplay=1"></iframe>';
    }
    return embed;
}

//End sharing

function copyToClipboard(element) {
    if (element) element = $(element);
    element.focus();
    element.select();
    document.execCommand("copy");
}

// Begin Input number only
// using: $("#Zip").bind("keydown", inputNumber_OnKeyDown).bind("paste", inputNumber_OnPaste);

function inputNumber_OnKeyDown(event) {
    var charCode = (event.which) ? event.which : event.keyCode;
    if (
            (!event.shiftKey && (charCode >= 48 && charCode <= 57)) // 0-9 in keyboards
            || (!event.shiftKey && (charCode >= 96 && charCode <= 105))  // 0-9 in numpad 

            || (event.ctrlKey && (charCode == 86)) // Ctrl-V OR Ctrl-v
            || (event.shiftKey && (charCode == 16 || charCode == 97 || charCode == 103)) // .,1,7 in numpad ~ del,end,home keys

            || (charCode >= 35 && charCode <= 40) // home, end, left, right, down, up keys
            || charCode == 8 //backspace
            || charCode == 9 //tab
            || charCode == 46  //delete
            || charCode == 116  //F5
        ) {
        return true;
    }
    else {
        event.preventDefault();
    }
    return false;
}

function inputNumber_OnPaste(event) {
    var clipboardData = event.clipboardData || event.originalEvent.clipboardData || window.clipboardData;
    if (clipboardData) {
        var pastedData = clipboardData.getData('Text');
        if (isNaN(pastedData))
            event.preventDefault();
    }
}

// End Input number only

// http://magentohostsolution.com/3-ways-detect-mobile-device-jquery/
var isMobileDevice = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iPad: function () {
        return navigator.userAgent.match(/iPad/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i);
    },
    Any: function () {
        return (isMobileDevice.Android() || isMobileDevice.BlackBerry() || isMobileDevice.iOS() || isMobileDevice.Opera() || isMobile.Windows());
    }
};

function loadLanguages(keys, pageName, obj) {
    $.ajax({
        url: SiteURL.Prefix + "/resource/getresources",
        async: false,
        data: {
            keys: keys,
            pageName: pageName
        },
        success: function (data) {
            obj.languages = data;
        }
    });
}

function showLanguageKey(isShow) {
    $.ajax({
        url: SiteURL.Prefix + "/resource/showlanguagekey",
        data: {
            showlanguages: isShow
        },
        success: function () {
            location.reload();
        }
    });
}

function changeCurrentLanguage(languageId) {
    $.ajax({
        type: "post",
        url: SiteURL.Prefix + "/Home/ChangeCurrentLanguage",
        data: { languageId: languageId },
        success: function (data) {
            window.location.href = window.location.href;
        }
    });
}

function showPriorityForm(modalID, param, callBackOnClose) {
    var data = $.extend({
        tableName: null,
        columnId: null,
        columnName: null,
        columnPriority: null,
        whereClause: null,
        CallbackJS: null
    }, param);

    $.ajax({
        type: "GET",
        url: SiteURL.Prefix + "/home/_sortable",
        data: data,
        success: function (html) {
            var modal = $("#" + modalID);
            modal.html(html);
            initSortable();
            modal.dialog({
                lgClass: true,
                title: "Set Priority",
                close: function () {
                    if (callBackOnClose) {
                        callBackOnClose();
                    }
                    else {
                        window.location.href = window.location.href;
                    }

                }
            });
            modal.dialog("open");
        }
    });// End: $.ajax()
}

function hideFullScreenButton() {
    if (isMobileDevice.iPad() != null && isMobileDevice.iPad().length) {
        $('.plyr--fullscreen-enabled [data-plyr=fullscreen]').hide();
    }
}

function rebuildDic() {
    $.ajax({
        type: "GET",
        url: SiteURL.Prefix + "/Application/RebuildDic",
        data: {},
        success: function (result) {
            return result == "OK";
        }
    });// End: $.ajax()
}

function ShowPopup(config) {
    var options = $.extend({
        target: '',
        contentTarget: '',
        content: '',
        title: '',
        lgClass: false,
        autoOpen: false,
        openEffect: '',
        closeEffect: ''
    }, config);

    if ($("#" + options.target).size() == 0) {
        if (!options.target) {
            options.target = 'dlgContainer';
        }
        $("body").append("<div id='" + options.target + "' style='display:none'>");
    }
    var container = $("#" + options.target);

    if (!options.contentTarget) {
        options.contentTarget = options.target;
    }

    var contentWrapper = $("#" + options.contentTarget);
    contentWrapper.html(options.content);

    container.dialog(options);
    container.dialog("open");
}

function initPhotoZoom() {
    var arrObj = $("[cf-zoom]");    
    if (arrObj.size() == 0) return;

    // declare params
    $.each(arrObj, function (idx, element) {
        var obj = $(element);
        var imgUrl = obj.attr("data-image-url");
        if (imgUrl) {
            obj.on("click", function () {
                var options = {
                    openEffect: obj.attr("data-open-effect"),
                    closeEffect: obj.attr("data-close-effect"),
                    lgClass: obj.attr("data-modal-large") == "1",
                    title: obj.attr("data-title")
                };
                options.content = "<div class='text-center' style='padding-bottom:20px;'><img class='img-responsize' style='max-width:100%' src='" + imgUrl + " '/></div>";
                ShowPopup(options);
            });
            obj.css("cursor", "zoom-in");
        }
    });
}

function UpdateSession(options, callback) {
    var params = $.extend({
        name: '',
        value: ''
    }, options);
    $.ajax({
        type: "POST",
        url: SiteURL.Prefix + "/Home/UpdateSession",
        data: params,
        success: function (response) {
            if (callback) callback(response);
        }
    });
}

function formAfterValidateFail() {
    $.each($("form"), function (idx, form) {
        if ($(form).validate) {
            $(form).bind('invalid-form.validate',
                function (form, validator) {
                    var errors = validator.numberOfInvalids();
                    if (errors) {
                        var firstInvalidElement = $(validator.errorList[0].element);
                        $('html,body').scrollTop(firstInvalidElement.offset().top);
                        firstInvalidElement.focus();
                    }
                }
            );
        }
    });
}

function hasPaging() {
    return $(".paging").size();
}

function getCurrentPageIndex(event, page) {
    var result = page ? page : 1;
    if (result == 1) {
        try {
            var _event = event ? event : window.event;
            //console.log(_event);
            if (_event.type != "click") {
                if (hasPaging()
                    // for firefox 
                    && _event.target.activeElement.type != "button"
                 ) {
                    result = $(".paging .current").text();
                    //console.log("result:");
                    //console.log($(".paging").size());
                }
            }
        }
        catch (ex) { }
        if (!result)
            result = 1;
    }
    return result;
}

function getUrlParameter(uri, key) {
    key = key.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + key + '=([^&#]*)');
    var results = regex.exec(uri);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
};

function updateQueryStringParameter(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

function getAge(birthDate) {
    var today = new Date();
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function isAgeUnder(birthDate, minAge) {
    if (!minAge)
        minAge = 13;
    if (!birthDate.getMonth) {
        birthDate = new Date(birthDate);
    }
    var age = getAge(birthDate);
    return age < minAge;
}

function updateFieldsRuleStatus(options) {
    if (!options.formId || options.fields) return;
    var status = options.status;

    var form = $("#" + formId);
    if (form.validate()) {
        for (var idx in options.fields) {
            var field = fields[idx];
            if ($("#" + field).size() > 0) {
                if (form.validate().settings.rules[field]) {
                    for (var rule in form.validate().settings.rules[field]) {
                        form.validate().settings.rules[field][rule] = status;
                    }
                }
            }
        }
    }
}

function isIOSDevice(){
    return /iPhone|iPad|iPod/i.test(navigator.userAgent);
}

jQuery.fn.extend({
    insertAtCaret: function (myValue) {
        return this.each(function (i) {
            if (document.selection) {
                //For browsers like Internet Explorer
                this.focus();
                sel = document.selection.createRange();
                sel.text = myValue;
                this.focus();
            }
            else if (this.selectionStart || this.selectionStart == '0') {
                //For browsers like Firefox and Webkit based
                var startPos = this.selectionStart;
                var endPos = this.selectionEnd;
                var scrollTop = this.scrollTop;
                this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
                this.focus();
                this.selectionStart = startPos + myValue.length;
                this.selectionEnd = startPos + myValue.length;
                this.scrollTop = scrollTop;
            } else {
                this.value += myValue;
                this.focus();
            }
        })
    }  
});

//NewGuid like cookies
(function ($) {
    $.NewGuid = function () {

        function _p8(s) {
            var p = (Math.random().toString(16) + "000000000").substr(2, 8);
            return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
        }
        return _p8() + _p8(true) + _p8(true) + _p8();
    };
})(jQuery);

function addToken(obj) {
    if (window.prevFocus) {
        var text = $(obj).text();
        if (window.prevFocus.type == 'ckeditor') {
            console.log(window.prevFocus.element.selectionStart);
            window.prevFocus.element.insertText(text);
        }
        else {
            window.prevFocus.element.insertAtCaret(text);
        }
    }
}

String.prototype.countWord = function () {
    //return this == '' ? 0 : $.trim(this.stripHTML()).replace(/[^\w ]/g, "").split(/\s+/).length;
    return this == '' ? 0 : $.trim(this.stripHTML().replace(/(\r\n|\n|\r)/gm, " ")).split(/\s+/).length;
}

function hasUploadTextMessageForm() {
    return $("#frmUploadMessage").size() > 0;
}

function isValidUploadTextMessageForm() {
    return !hasUploadTextMessageForm() || (hasUploadTextMessageForm() && $("#frmUploadMessage").valid());
}

function setUploadCallBack(upload, callback, precondition) {
    upload.data("fn_callback", callback);
    if (precondition) {
        upload.data("fn_precondition", precondition);
    }
}

function triggerUploadCallBack(upload, data) {
    var hasCallBack = false;
    if (upload) {
        if (upload.data("fn_callback")) {
            hasCallBack = true;
            (upload.data("fn_callback"))(data);
        }
    }
    return hasCallBack;
}

function triggerUploadPrecondition(upload) {
    if (upload) {
        if (upload.data("fn_precondition")) {
            hasCallBack = true;
            return (upload.data("fn_precondition"))();
        }
    }
}

String.prototype.stripHTML = function () {
    var tmp = document.createElement("div");
    $(tmp).html(this);
    return $(tmp).text();
}

function facebookTracking(fbPixelID, eventName) {
    if (!fbPixelID)
        return;

    !function (f, b, e, v, n, t, s) {
        if (f.fbq) return; n = f.fbq = function () {
            n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        }; if (!f._fbq) f._fbq = n;
        n.push = n; n.loaded = !0; n.version = '2.0'; n.queue = []; t = b.createElement(e); t.async = !0;
        t.src = v; s = b.getElementsByTagName(e)[0]; s.parentNode.insertBefore(t, s)
    }(window, document, 'script', '//connect.facebook.net/en_US/fbevents.js');	

    fbq('init', fbPixelID);
    fbq('track', eventName);
}

function openURLdialog(url, targetDiv, title) {
	
	if(!title)
		title = '';

	$(targetDiv).load(url, function() {
		$(targetDiv).dialog({
			modal: true,
			title: title
		});
	})

}


var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function (obj) {
    var self = obj instanceof this.constructor ?
        obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
    var container, timeout;

    originalLeave.call(this, obj);

    if (obj.currentTarget) {
        if ($(obj.currentTarget).attr("popup-hover") == "1") {
            container = $(obj.currentTarget).siblings('.popover')
            timeout = self.timeout;
            container.one('mouseenter', function () {
                //We entered the actual popover – call off the dogs
                clearTimeout(timeout);
                //Let's monitor popover content instead
                container.one('mouseleave', function () {
                    $.fn.popover.Constructor.prototype.leave.call(self, self);
                });
            })
        }
    }
};
