﻿ContestantEntry = {
    SetReloadStatus: function (shouldReload){
        if ($("#dvContestantEntryInfo").size() > 0) {
            $("#dvContestantEntryInfo").attr("data-reload", shouldReload ? "1" : "0");
        }
    },
    GetReloadStatus: function () {
        if ($("#dvContestantEntryInfo").size() > 0) {
            return $("#dvContestantEntryInfo").attr("data-reload") == "1";
        }
        return false;
    },

    ShowContestantEntryInfo: function (contestant) {
        var dv = $("#dvContestantEntryInfo");
        if (dv.length == 0) {
            $("body").append("<div id='dvContestantEntryInfo' style='display:none;' title='ContestantEntry Info'></div>");
            dv = $("#dvContestantEntryInfo");
        }        
        console.log(contestant);
        $.ajax({
            url: contestant.url,
            type: "GET",
            data: {
                Id: contestant.id,
                contestantentryID: contestant.ContestantID,
                editSiteId: contestant.editSiteId,
                editCampaignId: contestant.editCampaignId,
                isOverview: 2,               
                IsPartial: true,
                controllerName: contestant.controllerName
            },
            success: function (msg) {
                dv.html(msg);
                
                dv.dialog({
                    modal: true,
                    width: window.screen.availWidth * 0.8,
                    height: window.screen.availHeight * 0.85,
                    lgClass: true,
                    customClass: contestant.customClass ? contestant.customClass : "",
                    title: contestant.title ? contestant.title : "",
                    close: function () {
                        if (ContestantEntry.GetReloadStatus()) {
                            if (contestant.callback) {
                                contestant.callback();
                            }
                        }
                        dv.html("");
                        ContestantEntry.SetReloadStatus(false);
                    }
                }).dialog("open");

                $('#dgdvContestantEntryInfo')
                    .css('z-index', '50000')
                    .removeClass('in');

                if (window.PlayDefaultMedia) {
                    PlayDefaultMedia();
                }
            }
        })
    },

    ChangeStatus: function (contestant) {
        var comment = contestant.comment ? contestant.comment : ($('#txtRejectDescription').val() != undefined ? $('#txtRejectDescription').val() : "");

        $.ajax({
            type: "POST",
            url: contestant.url,
            data: {
                id: contestant.ContestantID,
                status: contestant.Status,
                editSiteId: contestant.editSiteId,
                editCampaignId: contestant.editCampaignId,
                comment: comment,
                saveHistory: true
            },
            success: function (msg) {
                ContestantEntry.SetReloadStatus(true);
                if (typeof contestant.callback === 'function') {
                    contestant.callback(msg)
                }
                else {
                    eval(contestant.callback + "()");
                }                    
            }
        });
    },

    ChangeDisplay:function (contestant, obj, status) {
        var url = contestant.url;
        if (!status) {
            if (obj)
            {
                status = ($(obj).text() == "Show" ? "1" : "0");
            }
            else {
                status = 0;
            }
        }
        $.ajax({
            type: "POST",
            data: {
                Id: contestant.ContestantID,
                IsDisplay: status
            },
            url: url,
            success: function (msg) {
                ContestantEntry.SetReloadStatus(true);
                if (typeof contestant.callback === 'function') {
                    contestant.callback(msg)
                }
                else {
                    eval(contestant.callback + "()");
                }
            }
        });
    },

    DeleteContestant: function (contestant) {
        if (confirm("Are you sure to delete this entry?")) {
            var url = contestant.url;
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    contestantId: contestant.ContestantID
                },
                success: function (msg) {
                    ContestantEntry.SetReloadStatus(true);
                    if (msg == "ok") {
                        alert("Deleted successfully.");
                        if (typeof contestant.callback === 'function') {
                            contestant.callback(msg)
                        }
                        else {
                            eval(contestant.callback + "()");
                        }

                    }
                }
            });
        }
    },

    UpdateFlag: function (contestant) {
        var url = SiteURL.Prefix + '/Contestant/UpdateFlag';
        $.ajax({
            type: "POST",
            url: url,
            data: contestant,
            success: function (msg) {
                ContestantEntry.SetReloadStatus(true);
                if (typeof contestant.callback === 'function') {
                    contestant.callback(msg)
                }
                else {
                    eval(contestant.callback + "()");
                }
            }
        });
    }
}

