/*
 * jquery-counter plugin
 *
 * Copyright (c) 2009 Martin Conte Mac Donell <Reflejo@gmail.com>
 * Dual licensed under the MIT and GPL licenses.
 * http://docs.jquery.com/License
 */
jQuery.fn.countdown = function(userOptions)
{
	// Default options
  	var options = {
    	stepTime: 1000,
		// startTime and format MUST follow the same format.
		// also you cannot specify a format unordered (e.g. hh:ss:mm is wrong)
		timeLeft: null,
		format: "dd:hh:mm:ss",
		startTime: "01:12:32:55",
		currentTime: new Date(),
		digitImages: 6,
		digitWidth: 53,
		digitHeight: 77,
		timerEnd: function(){},
		image: "digits.png",
        showDaysOnly: false
	};
  	var digits = []; 
	var interval;
	var __max = [];
	var divisionRemainder;
	
	// console.log('options.startTime: ' + options.startTime);
	
	var self = this;

	var parseDate = function()
	{
		// var timeLeft = Math.round((options.startTime - options.currentTime) / 1000);
		var secs;
		var mins;
		var hrs;		
		var days;
		
		if(timeLeft > 0)
		{
			mins = Math.floor(timeLeft / 60);
			hrs = Math.floor(mins / 60);
			days = Math.floor(hrs  / 24);
			secs = timeLeft - mins * 60;
			
			
			if (options.showDaysOnly){
				if(days > 0){
					$(self).find('.cnt-days').show();
					$(self).find('.cnt-regular').hide();
				} else {
					$(self).find('.cnt-days').hide();
					$(self).find('.cnt-regular').show();

					if( $(self).find('.cnt-digit-container').length == 4 )
						$(self).find('.cnt-digit-container')[0].remove();
					if( $(self).find('.cnt-separator').length == 3 )
						$(self).find('.cnt-separator')[0].remove();
				}
			}
			
			$(self).find('.cnt-days .days-value').text( days );
			if(days == 1){				
				$(self).find('.cnt-days .days-text').text( 'DAY' );
			}

			mins = mins - hrs * 60;
			hrs = hrs - days * 24;
			
			mins = addZero(mins);
			hrs = addZero(hrs);
			days = addZero(days);
			secs = addZero(secs);

			/*if(days > 0)
				options.startTime = days + ":" + hrs + ":" + mins + ":" + secs;
			else
			{
				options.startTime = hrs + ":" + mins + ":" + secs;
				$(".daysHours .days").css("display", "none");
			}*/

            options.startTime = days + ":" + hrs + ":" + mins + ":" + secs;
			
			$(".daysHours").css("display", "block");
			
			if (days > 99)
				options.format = "ddd:hh:mm:ss";
            //else if (days == 0 || SiteConfig.SiteName.toLowerCase() == "greatergood")
            //else if (SiteConfig.SiteName.toLowerCase() == "greatergood")
            //    options.format = "hh:mm:ss";
            else
			    options.format = "dd:hh:mm:ss";


			divisionRemainder = options.startTime.split(":")[0].length % 2;
			options.startTime = options.startTime.split("");
			options.format = options.format.split("");
		}
		else
        {
            //if (SiteConfig.SiteName.toLowerCase() == "greatergood")
            //    options.startTime = "00:00:00";
            //else
                options.startTime = "00:00:00:00";
		}
		
		//options.startTime = "205:09:59:25";
		//options.format = "ddd:hh:mm:ss";
	}
	
	//function converts 1 to 01
	var addZero = function(obj)
	{
		if(Number(obj) < 10)
			obj = String("0" + obj);
		return obj;
	}
	
  	// Draw digits in given container
  	var createDigits = function(where) 
  	{
		//alert("createDigits");
		var c = 0;
		// Iterate each startTime digit, if it is not a digit
		// we'll asume that it's a separator
		
		// divisionRemainder = options.startTime.split(":")[0].length % 2;
		
		// options.startTime = options.startTime.split("");
		// options.format = options.format.split("");
		//options.startTime = replaceStrings(options.startTime, ":");
		//options.format = replaceStrings(options.format, ":");		
		// alert(options.startTime.length);
		
		var divContainer = $('<div class="cnt-digit-container"></div>');
		var whereContainer;
		var cntRegular = $('<div class="cnt-regular" style="display: none;"></div>');
		var cntDays = $('<div class="cnt-days" style="display: none;"><p class="days-value"></p><p class="days-text">DAYS</p></div>');

		where.html("");

		cntRegular.append(divContainer);
  	    //where.append(divContainer);
		if (options.showDaysOnly)
		    where.append(cntDays);
		else
			$(cntRegular).show();

		where.append(cntRegular);
		//console.warn('where');
		//console.log(where.html());
		
		whereContainer = where.find(".cnt-digit-container");
		//console.warn('whereContainer');
		//console.log(whereContainer.html());

		where = cntRegular;		

		// console.log('options.startTime.length: ' + options.startTime.length);
		// console.log(options.startTime);

		for (var i = 0; i < options.startTime.length; i++)
		{
		    if (parseInt(options.startTime[i]) >= 0)
      		{
				//create html for one digit
				elem = $('<div id="cnt_' + c + '" class="cnt-digit"><div>').css({
					// background: 'url(\'' + options.image + '\')',
					float: 'left'
					});
				//add new div into digits array
				digits.push(elem);
		        //setting initial margin top
				
				whereContainer.append(elem);
				// margin(c, -((parseInt(options.startTime[i]) * options.digitHeight * options.digitImages)));
				margin(c, (parseInt(options.startTime[i])));
				
				// if (options.format[i] === 'd')
				//     console.log( whereContainer.html() );

				// Add max digits, for example, first digit of minutes (mm) has 
				// a max of 5. Conditional max is used when the left digit has reach
				// the max. For example second "hours" digit has a conditional max of 4 
				if (options.notShowDay === undefined) {
				    switch (options.format[i]) {
				        case 'h': //hrs mins secs
				            __max.push((c % 2 == divisionRemainder) ? 2 : 3);
				            if ($(whereContainer).children(".days").length == 0)
				                whereContainer.prepend($('<div class="days">HOURS</div>'));
				            break;
				        case 'd':
				            __max.push(9);
				            if ($(whereContainer).children(".days").length == 0)
				                whereContainer.prepend($('<div class="days">DAYS</div>'));
				            break;
				        case 'm'://minutes will get same __max as seconds
				            if ($(whereContainer).children(".days").length == 0)
				                whereContainer.prepend($('<div class="days">MINS</div>'));
				            break;
				        case 's':
				            __max.push((c % 2 == divisionRemainder) ? 5 : 9);
				            if ($(whereContainer).children(".days").length == 0)
				                whereContainer.prepend($('<div class="days">SECS</div>'));
				            break;
				    }
				}
				
				++c;
			}
			else 
			{
		        elem = $('<div class="cnt-separator"/>').css({ float: 'left' })
						.html(":");
						//.text(options.startTime[i]);
				where.append(elem);

				if (i < options.startTime.length)
		        {
				    where.append($('<div class="cnt-digit-container"></div>'));
				    whereContainer = $(where).children(".cnt-digit-container:last-child");
				}
			}
			// alert( "digits.length: " + digits.length );
		}
	};

	var updateText = function()
	{
		var digitsIndex = 0;
		for (var i = 0; i < options.startTime.length; i++)
		{
			if( options.startTime[i] >= 0 )
			{
				$(digits[digitsIndex]).text( options.startTime[i] );
				digitsIndex++;
			}
		}
	}
  
    // Set or get element margin
    var margin = function (elem, val) {
        var cntDigitObj = $("#cnt_" + elem);
        if (val !== undefined) {
            // var r = $(cntDigitObj).css({ 'background-position': "0 " + val + 'px' });
            var r = $(cntDigitObj).text(val);
            return r;
        }
        // var returnValue = parseInt($(cntDigitObj).css('background-position').replace('0% ', '').replace('0px ', '').replace('px', ''), 10);
        var returnValue = parseInt($(cntDigitObj).text());
        return returnValue;
    };

	this.startInterval = function()
	{
		interval = setInterval(moveStep(digits.length - 1), 1000);
	}
	this.stopInterval = function()
	{
		stopIntervalFunc();
	}
	var stopIntervalFunc = function()
	{
		if(interval)
			clearInterval(interval);
	}
	
	//init
	$.extend(options, userOptions);
	var timeLeft;
	if(options.timeLeft){
		// console.log( 'options.timeLeft: ' + options.timeLeft );
		timeLeft = options.timeLeft;
	}
	else{
	 	timeLeft = Math.round((options.startTime - options.currentTime) / 1000);
	}

	//this.css({height: options.digitHeight, overflow: 'hidden'});
	parseDate();		
	createDigits(this);
	$('#countdown').css("float", "none");


	var moveStepCF = function()
	{
		timeLeft--;

		parseDate();
		updateText();

		if(timeLeft <= 0)
		{	
			stopIntervalFunc();
			options.timerEnd();
		}
	}

	interval = setInterval(moveStepCF, 1000);

	return this;
};

