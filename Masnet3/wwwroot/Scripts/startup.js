﻿// start up javascript

// comment
function ClearComment(container) {
    $("#" + container).find("textarea").val("");
}
function CheckComment(container) {
    if ($.trim($("#" + container).find("textarea").val()) == "") {
        alert("Please enter your comment");
        return false;
    }
    return true;
}

//UrlAction
function UrlAction(dUrl, dParam, siteName) {
    if (siteName != undefined) {
        var split = dUrl.split("/");
        var flag = dUrl.indexOf("masnet3") > 0;
        dUrl = flag ? SiteURL.Root + siteName + "/" : "/" + siteName + "/";
        for (var i = flag ? 3 : 2; i < split.length; i++) {
            dUrl += i === split.length - 1 ? split[i] : split[i] + "/";
        }
    }
    var result = dUrl.replace(/&amp;/g, "&");
    for (x in dParam) {
        result = result.replace(x, encodeURIComponent(dParam[x]));
    }
    return result;
}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}

function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) {
        var urlparam = url[i].split('=');
        if (urlparam[0] == param) {
            return urlparam[1];
        }
    }
    return null;
}

function showLoading(){   

    if ($(".loader-container").length == 0) {
        $(".page-title").after("<div class='loader-container'><div class='loader--text'>Submission in Progress</div><div class='spinner'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div></div>");
    }

    $(".loader-container").show();
}

function hideLoading() {

    if ($(".loader-container").length == 0) {
        $(".page-title").after("<div class='loader-container'><div class='loader--text'>Submission in Progress</div><div class='spinner'><div class='rect1'></div><div class='rect2'></div><div class='rect3'></div><div class='rect4'></div><div class='rect5'></div></div></div>");
    }

    $(".loader-container").hide();
}


function fadeIn(immediate, processingData, isManual) {
    if (!immediate)
        immediate = false;
   
    var hasProcessingData = processingData && processingData.percent;
    if ($(".fade-in-processing").length == 0) {
        var dvLoading = "<div class='fade-in-processing' style='z-index:9999; position:fixed; width:100%;height:100%; padding-top: 30%;padding-left:50%;left: 0;top: 0;background-color:transparent; opacity:1;filter:alpha(opacity=1)'>";
        dvLoading += "<div class='loading-box'>";
        if (SiteConfig.SiteName.toLowerCase() != "rantchamp") {           
            dvLoading += "<i style='font-size: 3.0em; color:#333; left: 50%;top: 50%; transform: translate(-50%, -50%);' class='icon icon-spinner icon-spin'></i>";
        }
        dvLoading += "</div></div>";
        $("body").append(dvLoading);
    }

    if (hasProcessingData) {
        $(".fade-in-processing .loading-box").addClass("bg-lightgray");
    }
    else {
        $(".fade-in-processing .loading-box").removeClass("bg-lightgray");
    }

    if (!immediate)
        $(".fade-in-processing").fadeIn();
    else
        $(".fade-in-processing").show();

    if (processingData && processingData.percent) {
        if ($(".fade-in-processing .loading-box .process-info").size() == 0) {
            $(".fade-in-processing .loading-box").append("<div class='process-info'></div>");
        }
        $(".fade-in-processing .loading-box .process-info").html("Processing " + processingData.percent + "% - " + processingData.rowIndex + "/" + processingData.totalRow);
    }
    else {
        if ($(".fade-in-processing .loading-box .process-info").size() > 0) {
            $(".fade-in-processing .loading-box .process-info").remove();
        }
    }

    if (isManual) {
        window.isProcessing = true;
    }

    $(".ajaxloading").show();
}

function fadeOut(immediate, isManual) {
    if (window.isProcessing === true)
    {
        if (isManual !== true) return;
        window.isProcessing = false;
    }

    if (!immediate)
        immediate = false;

    if (!immediate)
        $(".fade-in-processing").fadeOut();
    else
        $(".fade-in-processing").hide();
 
    
    $(".ajaxloading").hide();

    if ($(".fade-in-processing .loading-box .process-info").size() > 0) {
        $(".fade-in-processing .loading-box .process-info").remove();
    }
}

$(document).ready(function () {
    if ($.validator) {
        $.validator.addMethod(
            "safe",
            function (value, element, regexp) {
                //var re = new RegExp("^[^<>\'\"\\/]+$");
                var re = new RegExp("^[^<>\"\\/]+$");
                return this.optional(element) || re.test(value);
            }
        );

        jQuery.validator.addClassRules('safe', {
            safe: true
        });
    }    
    try{
        //if (SiteConfig.IsAdmin) {
        //    $(document).scrollTop($("#dvHeader").height() + 40);
        //}   
    } catch (ex) { }
});

//Script untility 
loadCSS = function (href) {
    if ($("link[href*='" + href + "']").length == 0) {
        var cssLink = $("<link rel='stylesheet' type='text/css' href='" + href + "'>");
        $("head").append(cssLink);
    }
};

loadJS = function (src) {
    if ($("script[src*='" + src + "']").length == 0) {
        var jsLink = $("<script type='text/javascript' src='" + src + "'>");
        $("head").append(jsLink);
    }
};

jQuery.fn.extend( {
	dialog: function(options,attr,val) {  
		var id = this.selector;                
		dialog(id, options, attr, val);
		return $(id);
	}
});

function dialog(id, options, attr, val) {
	
    //if (!options) {
    //    options = { };
    //}

	var uWidth = (options && options.width) ? (";width:" + (options.width > window.screen.width ? window.screen.width - 13 : options.width) + "px") : "";	
	var uHeight = (options && options.height) ? (";height:" + options.height + "px") : "";
	var uTitle = (options && options.title) ? (options.title) : "";
	var uClose = (options && options.close) ? (options.close) : "";
	var dgID = "dg" + id.replace("#", "").replace(".", "");
	var footerContent = (options && options.footerContent) ? (options.footerContent) : "";
	var customClass = (options && options.customClass) ? (options.customClass) : "";
	var modalFooter = "";
	if (footerContent.length > 0) {
	    modalFooter = "<div class=\"modal-footer\" >" + footerContent + "</div>";
	}
	
	if (options == "destroy") {
	    $("#" + dgID).data("bs.modal", null)
	    $("#" + dgID).detach();
	   return;
	}

	var modalClass = (options && options.lgClass) ? "<div class=\"modal-dialog modal-lg\" > " : "<div class=\"modal-dialog " + (options && options.dSize ? options.dSize : "") + "\" > ";
	var fullScreenModalClass = (options && options.fullScreen) ? "modal-fullscreen": "";

	//if (typeof options.openEffect == 'undefined' || options.openEffect == null) {
	//    options.openEffect = "";
	//}

	if ($("#" + dgID).length == 0) {
	    var dg = $("<div class=\"modal " + customClass + " " + (options && options.openEffect ? " fade " : "") + fullScreenModalClass + "\" id=\"" + dgID + "\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">" +
		  modalClass+
			"<div class=\"modal-content\"  > " +
			  "<div class=\"modal-header\" >" +
				"<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>" +
				"<h4 class=\"modal-title\" id=\"Label" + dgID + "\">" + uTitle + "</h4>" +
                "<div style=\"clear:both\"></div>" +
			  "</div>" +
			  "<div class=\"modal-body\" >" +
			  "</div>" +
              modalFooter +
			"</div>" +
		  "</div>" +
		"</div>");
	    //$(id).replaceWith(dg);

	    $("body:first").append(dg);	    
	    $("#" + dgID + " .modal-body").wrapInner($(id));

		if (options == "option" && attr == "title" && val)
		    $("#Label" + dgID).html(val);

		$("#" + dgID).on('show.bs.modal', function (e) {
		    // fix ios safari address bar overlapping issue
		    //if ($("#" + dgID + " .modal-dialog").offset().top < 70) {
		    //    $("#" + dgID + " .modal-dialog").css("top", "70px");
		    //}
		    // end: fix ios safari address bar overlapping issue
		    if (options && options.closeEffect) {
		        $("#" + dgID + " .modal-dialog").removeClass(options.closeEffect);
		    }
		    if (options && options.openEffect) {
		        $("#" + dgID + " .modal-dialog")
                        .addClass(options.openEffect)
                        .addClass("animated");
		    }
		});

		$("#" + dgID).on('hide.bs.modal', function (e) {
		    if (options && options.openEffect) {
		        $("#" + dgID + " .modal-dialog").removeClass(options.openEffect);
		    }
		    if (options && options.closeEffect) {
		        $("#" + dgID + " .modal-dialog")
                    .addClass(options.closeEffect)
                    .addClass("animated");
		    }
		});

		$("#" + dgID).on('hidden.bs.modal', function () {
		    //$("#" + dgID).replaceWith($(id));
            // clear animate classes
		    //$("#" + dgID + " .modal-dialog")
                //.removeClass("zoomIn")
                //.removeClass("zoomOut")
		        //.removeClass("animated");

		    if (options && options.close)
		        options.close();
		    if (options && options.stop)
		        $(this).remove();
		    $(id).hide();
		    if (options && options.destroyOnClose) {
		        $("#" + dgID).data("bs.modal", null)
		        $("#" + dgID).detach();
		    }
		});

		//$("#" + dgID).on("shown.bs.modal", alignModal);

	    $("#" + dgID).modal({
		    backdrop: 'static',
		    keyboard: false,
	        show: false
		});
	}
	else {
	    if (options && options.title)
	        $('.modal-title', $("#" + dgID)).html(options.title);
	}

	$("[data-toggle='tooltip']", $("#" + dgID)).tooltip();

	if (options == "open" || !options || (options && options.autoOpen != false)) {
	    //$("#" + dgID).css("z-index", $(".modal.in").length * 10 + 5000);
	    if ($('.modal:visible').length) {
	        // has visible modal
	        var zIndex = Math.max.apply(null, Array.prototype.map.call(document.querySelectorAll('*'), function (el) {
	            return el.style.zIndex;
	        }));

	        if ($("#" + dgID).css('z-index') < zIndex) {
	            $("#" + dgID).css('z-index', zIndex + 10);
	        }

	        // Align modal when user resize the window
	        //$(window).on("resize", function () {
	        //    $(".modal:visible").each(alignModal);
	        //});
	    }
	    $("#" + dgID).modal('show');
	    $(id).show();
	    if (parent != self) parent.postMessage("scrollIframeTop", "*");

	    if(isAppleDevice()){
	        //$(".modal-dialog").css("top", $.cookie("off") + "px");
	        
	    }
	}
	if (options == "close") {
	    $("#" + dgID).modal('hide');
	    //$(id).hide();
	}
}

function alignModal() {
    var modalDialog = $(document).find(".modal-dialog");

    // Applying the top margin on modal dialog to align it vertically center
    modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
}

String.prototype.trim = function () {
    return this.replace(/^\s+|\s+$/g, "");
}

function left(value, maxlen) {
    value = $.trim(value);
    if(maxlen == null)
    {
        return value;
    }
    
    if (value.length <= maxlen) {
        return value;
    }

    return value.substring(0, Math.max(maxlen - 3, 3)) + "...";
}

function setTitle(obj, value, maxlen) {
    var leftValue = left(value, maxlen);

    $(obj)
        .html(leftValue)
        .attr("title", $.trim(value));
}

function getTitle(obj) {
    return $.trim($(obj).attr("title"));
}

function sortDropdownlistByText(obj) {
    if (obj != null) {
        var options = $(obj).find("option");
        if (options != null && options.length > 0) {
            var selectedValue = $(obj).val();
            options.detach().sort(function (a, b) {               // Detach from select, then Sort
                var at = $(a).text();
                var bt = $(b).text();
                return (at > bt) ? 1 : ((at < bt) ? -1 : 0);            // Tell the sort function how to order
            });
            options.appendTo(obj);

            $(obj).val(selectedValue);
        }
    }
}

function buildUploadControlIframe(obj) {
    var htmlDiv = "";
    if ($(obj).length > 0) {
        var iframeId = $.trim($(obj).attr("data-media-iframeId"));
        if (iframeId == '') {
            iframeId = "UploadControl";
        }
        htmlDiv += "<div><iframe id='" + iframeId + "' name='" + iframeId + "' scrolling='no' width='100%' style='width:100%;max-width:450px' height='100px' frameborder='no' src='" + SiteConfig.VirtualUploadPath + "/UploadControls.aspx?";

        //VirtualUploadDomain
        var virtualuploaddomain = $(obj).attr("data-media-virtualuploaddomain");
        if (virtualuploaddomain == null || virtualuploaddomain.trim() == "") {
            virtualuploaddomain = SiteConfig.VirtualUploadDomain;
        }
        htmlDiv += "virtualuploaddomain=" + virtualuploaddomain + "&";

        //sessionkey
        var sessionkey = $(obj).attr("data-media-sessionkey");
        if (sessionkey == null || sessionkey.trim() == "") {
            sessionkey = SiteConfig.NowTick;
        }
        htmlDiv += "SessionKey=" + sessionkey + "&";

        //maxfilesize
        var maxfilesize = $(obj).attr("data-media-max-file-size");
        if (maxfilesize == null || maxfilesize.trim() == "") {
            maxfilesize = "4294967296";
        }
        htmlDiv += "MaxFileSize=" + maxfilesize + "&";

        //MediaType
        if ($(this).attr("data-media-type")) {
            var mediaType = $(obj).attr("data-media-type");
            if (mediaType.trim() != "") {
                htmlDiv += "MediaType=" + mediaType + "&";
            }
        }

        //FileFilter
        var fileFilter = $(obj).attr("data-media-file-filter");
        htmlDiv += "FileFilter=" + fileFilter + "&";

        //SaveWithName
        var saveWithName = $(obj).attr("data-media-save-with-name");
        if (saveWithName != null && saveWithName != '') {
            htmlDiv += "SaveWithName=" + saveWithName + "&";
        }

        //SaveOverwrite
        var saveOverwrite = $(obj).attr("data-media-save-overwrite");
        if (saveOverwrite != null && saveOverwrite != '') {
            htmlDiv += "Overwrite=" + saveOverwrite + "&";
        }

        //lang
        var lang = $(this).attr("data-media-lang");
        if (lang == null || lang.trim() == "") {
            maxfilesize = "en";
        }
        htmlDiv += "Lang=" + lang + "&";

        //UserName
        var username = $(obj).attr("data-media-username");
        // ignore the param will use in config
        if (username.length > 50) {
            htmlDiv += "UserName=" + SiteConfig.CurrentUserFolder + "&";
        }
        else {
            htmlDiv += "UserName=" + username + "&";
        }


        //AppName
        var appName = $(obj).attr("data-media-appName");
        if (appName == null || appName.trim() == "") {
            appName = SiteConfig.ApplicationName;
        }
        htmlDiv += "AppName=" + appName + "&";

        htmlDiv += "'></iframe></div>";
    }//End: if ($(obj).length > 0)

    return htmlDiv;
}//End: function buildUploadControlIframe()

function showDialog(obj) {
    var containerID = obj.containerID ? obj.containerID : "dvDialog";
    if ($('#dvDialog').size() == 0) {
        $("<div id='" + containerID + "' class='text-center' style='display:none'></div>").appendTo("body");
    }
    else {
        $('#dvDialog').addClass("text-center");
    }

    $('#dvDialog').html(obj.message);
    if (obj.btnOK == "1" && $("#btnOK", $('#dvDialog')).size() == 0) {
        var btnOK = $("<br/><br/> <div class=\"text-center padding-top-lg padding-bottom-lg\"><input id='btnOK' type='button' value='OK' class='btn btn-primary' onclick='$(\"#dvDialog\").dialog(\"close\")'></div>");
        $('#dvDialog').append(btnOK);
    }
    $('#dvDialog').dialog(obj);
}

function getURL(url) {
    var result = null;
    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        async: false,
        dataType: 'json',
        success: function (responseData) {
            result = responseData;
        }
    });

    return result;
}

function getServerTime() {
    var url = SiteURL.Root + SiteConfig.SiteName + "/Home/GetServerTime";
    var dateString = getURL(url);
    var parsedDate = new Date(parseInt(dateString.replace(/\/Date\((-?\d+)\)\//gi, "$1")));
    return parsedDate;
}
