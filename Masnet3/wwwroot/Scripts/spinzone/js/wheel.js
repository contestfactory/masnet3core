// "Hello, World" class
function Wheel(wheelID)
{
	/*
	* PROPERTIES
	*/
	this._wheelID = wheelID; //string ID refernce to corresponding html object wheel

	if (PRIZE_SECTOR_HEIGHT <= 0) {
	    this.PRIZE_SECTOR_HEIGHT = 179;
	}
	else {
	    this.PRIZE_SECTOR_HEIGHT = PRIZE_SECTOR_HEIGHT;
	}
	
	this._speed = 0;
	this._speedMax = 20;
	this._speedIncr = .75;
	this._speedDecr = .75;
	this._lastY = 0; //holds y position of _container, when last addPrize, removePrize happened
	this._counts = 0; //holds value of how many removePrize happened, needed to adjust y position of added prize
	this._difference = 0;
	this.wheelTop = 0;
	
	this._countsTimer = 0;
	this._actionStatus = "start"; //start, stop, stopped
	
	this._pathBeforeStop = 0;
	this._countsBeforeStop = 0;
	this._winnerIndex = -1;
	this.totalReplacementsBeforeStop = 0;
	this._finalY;
	this._prizeIndexToremove = 3;


	this._speed = 0;
	this._timeStop;
	this.interval = 0;
	
	this.initDefault = function()
	{		
		this._actionStatus = "start"; //start, stop, stopped		
	}
	
	/*
	*
	* initAddPrizes
	*
	*/
	this.initAddPrizes = function ()
	{
		//alert(PRIZE_SECTOR_HEIGHT);
		for ( i = 0; i < 4; i++) 
		{
			$("#" + this._wheelID + "_prize" + i).css("top", (i - 1.5)* this.PRIZE_SECTOR_HEIGHT + "px");
			//alert("i: " + i + ", " + ((i - 1.5)* PRIZE_SECTOR_HEIGHT));
			//alert( ("#" + this._wheelID + "_prize" + i) );
		}	
	}
	/*
	*
	* setSpinPrize
	*
	*/
	this.setSpinPrize = function ( winnerIndex, timeSpin )
	{
		//Debugger.trace("setSpinPrize ->  winnerIndex: " + winnerIndex);
		this._winnerIndex = winnerIndex;
		this._setSpinPrizeFlag = true;
		
		//Debugger.trace( "_winnerIndex: " + _winnerIndex );
		this._timeStop = timeSpin;
		this._speed = this._speedMax;
		//if ( this._speed == this._speedMax )
		//{
			stopRotatingCore( this._wheelID, this._timeStop );
		//}
	}
	/*
	*
	* enterFrame
	*
	*/
	
	this.enterFrame =  function ()
	{	
		if ( this._winnerIndex > -1 && this._setSpinPrizeFlag ) 
		{
			this._setSpinPrizeFlag = false;
			//setTimeout( stopRotating, _timeStop);
		}
		
		//move container
		if ( this._actionStatus == "start" ) 
		{
			if ( this._speed + this._speedIncr < this._speedMax ) this._speed += this._speedIncr;
			else this._speed = this._speedMax;
		}
		else if( this._actionStatus == "stop" )
		{
			var oldSpeed = this._speed;		
			this._pathBeforeStop += this._speed;
			
			if ( this._speed - this._speedDecr > 0 ) 
			{
				this._speed -= this._speedDecr;
			}
			else 
			{
				this._speed = 0;
				//_spinMachine.spinFinished();
			}
		}
		this.wheelTop = this.wheelTop + this._speed;
		$("#" + this._wheelID ).css("top", this.wheelTop + "px"); 

		
		
		
		//check if need to remove or hide prizes
		if ( (this.wheelTop - this._lastY) >= this.PRIZE_SECTOR_HEIGHT)
		{
			if ( this._actionStatus == "start" ) this.addPrize();
			if ( this._actionStatus == "stop")
			{
				this._countsBeforeStop++;
				if ( this._countsBeforeStop == this.totalReplacementsBeforeStop - 1 )	
				{
					this.addWinnerPrize();
					//alert("addWinnerPrize");
				}
				else 
				{
					this.addPrize();
				}
			}
			
			this._counts++;
			this._lastY = this._counts * this.PRIZE_SECTOR_HEIGHT;	
			this._countsTimer = 0;
		}
		
		//alert( wheelTop );
		//if(lineChart) lineChart.drawData(_container.y);
		
	}
	/*
	*
	* addPrize
	*
	*/
	this.addPrize = function ()
	{
		$("#" + this._wheelID + "_prize" + this._prizeIndexToremove).html( getRandomPrize() );
		$("#" + this._wheelID + "_prize" + this._prizeIndexToremove).css("top", ((0 - 2.5 - this._counts) * (this.PRIZE_SECTOR_HEIGHT - 3) - this._counts * 3) + "px");// + i * prizeImg.height;

		if( this._prizeIndexToremove - 1 >=0 )
			this._prizeIndexToremove--;
		else
			this._prizeIndexToremove = 3;
	}
	/*
	*
	* addWinnerPrize
	*
	*/
	this.addWinnerPrize = function ()
	{
		$("#" + this._wheelID + "_prize" + this._prizeIndexToremove).html( getPrizeByID(this._winnerIndex) );
		$("#" + this._wheelID + "_prize" + this._prizeIndexToremove).css("top", ((0 - 2.5 - this._counts) * (this.PRIZE_SECTOR_HEIGHT - 3) - this._counts * 3) + "px");// + i * prizeImg.height;
		
		if( this._prizeIndexToremove - 1 >=0 )
			this._prizeIndexToremove--;
		else
			this._prizeIndexToremove = 3;
	}

	this.stopRotating = function()
	{
		/*	current speed
		 * 	_countsBeforeStop
		 * 
		 * 
		 * 
		 */
		//alert("stopRotating: " + this._speed);
		this._countsBeforeStop = 0;
		//var pathBeforeStop:Number = 0;
		var speed = this._speed;
		


		//checking y middle coordinates at the end of animation
		var correctedPathBeforeStop;
		if ( this.getTopPosition() % this.PRIZE_SECTOR_HEIGHT > 0 )
		{
			//correctedPathBeforeStop = 573 + (PRIZE_SECTOR_HEIGHT - _container.y % ( PRIZE_SECTOR_HEIGHT ) ) + _speed / 2 + 2;
			this.correctedPathBeforeStop = 14 + this.PRIZE_SECTOR_HEIGHT * 2 * 4 + (this.PRIZE_SECTOR_HEIGHT - this.getTopPosition() % ( this.PRIZE_SECTOR_HEIGHT ) );
			
			//NEED TO ADJUST _speedIncr
			this._speedDecr = (this._speed * this._speed) / (2 * this.correctedPathBeforeStop);
			
			speed = this._speed;
			_countsBeforeStop = 0;
			var tmpPath = 0;
			do
			{
				tmpPath += speed;
				
				this._countsBeforeStop++;
				
				if ( speed - this._speedDecr > 0 ) speed = speed - this._speedDecr;
				else speed = 0;
			}
			while ( speed > 0 )
		}
		
		this._finalY = this.getTopPosition() + tmpPath;
		this.totalReplacementsBeforeStop = 8;
	

									
		this._countsBeforeStop = 0;
		this._pathBeforeStop = 0;
		this._actionStatus = "stop";
		
		
		/*
		if ( this._wheelID == "wheel_0" ) 
		{
			//Debug.timerStart("Loop Execution Time");
			//setTimeout(MP3Loader.muteSpinAudio, 500);
		}*/

		
	}
	/*
	*
	* getWheelID
	*
	*/
	/*this.getWheelID = function()
	{
		return this._wheelID;	
	}*/
	this.getTopPosition = function()
	{
		return $("#" + this._wheelID ).css("top").replace("px", "");	
	}

}

/*
campaign ID = 3
*/