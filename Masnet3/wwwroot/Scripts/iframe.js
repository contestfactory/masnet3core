//post mess
var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
var eventer = window[eventMethod];
var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

// Listen to message from child window
eventer(messageEvent,function(e) {
    var key = e.message ? "message" : "data";
    var data = e[key];

    // alert(e.origin);

    switch( data )
    {
        case "orientationChanged":
            //SliderOrientationChanged();
            updatedHeight();
            break;
    }
},false);

// Listen to resize event and update iframe height
$( window ).resize(function() {
    updatedHeight();
});

$(function() {
    updatedHeight();
});
$(window).load(function () {
    console.log("$ window loaded");
    updatedHeight();
});

var updatedHeight = function()
{
    //console.log("body height: " + $("body").height())
    parent.postMessage("setIframeHeight:" + $("#bodywrapper").height(), "*");
}
window.updatedHeight = updatedHeight;