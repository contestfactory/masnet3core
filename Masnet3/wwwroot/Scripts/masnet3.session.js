﻿session = {
    isLogedIn: function () {
        var result = true;
        $.ajax({
            type: "POST",
            async: false,
            url: SiteURL.Home_KeepAlive,
            success: function (data) {
                result = data.IsLoggedIn == "1";
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) { }
        });

        return result;
    },
}

