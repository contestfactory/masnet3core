﻿
function saveCTAButton(obj) {
	var index = $(obj).attr("data-Position") - 1;
	//var data = {
	//	CTAButtonID:    $(obj).attr("data-CTAButtonID"),
	//	CTAButtonKeyID: $(obj).attr("data-CTAButtonKeyID"),
	//	PageID:         $(obj).attr("data-PageID"),
	//	Position:       $(obj).attr("data-Position"),
	//	Image:          $("[id*='" + index + "__Image']").val(),
	//	ButtonLabel:    $("[id*='" + index + "__ButtonLabel']").val(),
	//	Title:          $("[id*='" + index + "__Title']").val(),
	//	Link:           $("[id*='" + index + "__Link']").val(),
	//	TemplateID:     $(obj).attr("data-TemplateID"),
    //    ApplicationID:  $(obj).attr("data-ApplicationID"),
    //    CampaignID:     $(obj).attr("data-CampaignID"),
	//	Display:        $(obj).hasClass('bootstrap-switch-on') ? true : false 
	//}           
	var data = $("#frmCTAButton").serialize();
	if ($("#frmCTAButton").valid()) {
			$.ajax({
				type: "POST",
				url: SiteURL.Prefix + "/ContestPages/_SaveCTAButtons",
				data: data,
				success: function (data) {
					$(obj).attr("data-CTAButtonID", data.CTAButtonID);    
					$("[id*='" + index + "__CTAButtonID']").val(data.CTAButtonID);
					$("[id*='" + index + "__OldImage']").val( $("[id*='" + index + "__Image']").val());
			}
		});
	}
}

function saveCTAButtons() {
    if ($("#frmCTAButton").valid()) {
        $.ajax({
            type: "post",
            url: SiteURL.Prefix + "/ContestPages/_SaveCTAButtons",
            data: $("#frmCTAButton").serialize(),
            success: function (data) {
                if (window.goNext)
                    goNext();
                else
                    alert("Data saved!");
            }
        });
    }
}

function copyTo(index) {
    var pageIds = "";
    $("[id^='chkCopyToPage_" + index + "_']").each(function () {
        if ($(this).is(":checked")) {
            pageIds += $(this).attr("data-copytopage") + ",";
        }
        $(this).removeAttr("checked");
    });

    $.ajax({
        type: "post",
        url: SiteURL.Prefix + "/ContestPages/CopyCTAButtonToPage",        
        data: {
            Title: $("#CTAButtons_" + index + "__Title").val(),
            ButtonLabel: $("#CTAButtons_" + index + "__ButtonLabel").val(),
            Image: $("#CTAButtons_" + index + "__Image").val(),
            Link: $("#CTAButtons_" + index + "__Link").val(),
            ApplicationID: $("#CTAButtons_" + index + "__ApplicationID").val(),
            CampaignID: $("#CTAButtons_" + index + "__CampaignID").val(),
            Position: $("#CTAButtons_" + index + "__Position").val(),
            Display: $("#CTAButtons_" + index + "__Display").val(),
            pageIds: pageIds,
            SH_PromoBox: $("#CTAButtons_" + index + "__SH_PromoBox").val(),
            SH_Title: $("#CTAButtons_" + index + "__SH_Title").val(),
            SH_ButtonLabel: $("#CTAButtons_" + index + "__SH_ButtonLabel").val(),
            SH_Image: $("#CTAButtons_" + index + "__SH_Image").val(),
            SH_Link: $("#CTAButtons_" + index + "__SH_Link").val(),
            SH_Copy: $("#CTAButtons_" + index + "__SH_Copy").val(),
            TemplateID: $("#CTAButtons_" + index + "__TemplateID").val(),
            UseType: $("#CTAButtons_" + index + "__UseType").val()
        },
        success: function (data) {
            alert("Copied successfully.");
        }
    });
}

function ajaxSubmitMediaFile() {
    if (UploadControl.fn_Validate && !UploadControl.fn_Validate())
        return false;

    if (UploadControl.fn_DoUpload) {
        UploadControl.fn_DoUpload();
    }
} //End: ajaxSubmitMediaFile()

function DoUpdatingDataAfterUploading(errorCode, resultfileName, mediaServer) {
    if (errorCode == 0) {
        var btnUploadThumbnail = $("[data-upload-running]").first();
        $("#dialogUploadMedia").dialog("close");
        $(btnUploadThumbnail).attr("data-upload-running", "1");
        $(btnUploadThumbnail).attr("data-media-uploadedfileName", resultfileName);
        $(btnUploadThumbnail).attr("data-media-server", mediaServer);
        var callback = $(btnUploadThumbnail).attr("data-media-callback");
        if (callback != null && callback.trim() != "") {
            eval(callback);
        }
    } else if (errorCode == 1) alert('Please select a file');
    else if (errorCode == -1) alert("Upload Fail: An exception occurred.");
} //End: DoUpdatingDataAfterUploading(errorCode, resultfileName)

function uploadSuccess() {
    var btnUploadThumbnail = $("[data-upload-running]").first();
    var value_index = $(btnUploadThumbnail).attr("data-value-index");

    var CTAButtons_Image = $("#CTAButtons_" + value_index + "__Image")
    var uploadFileName = $(btnUploadThumbnail).attr("data-VirtualUploadPath") + "/" + SiteConfig.UploadFolder + "/" + $(btnUploadThumbnail).attr("data-media-appname") + "/" + $(btnUploadThumbnail).attr("data-media-username") + "/" + $(btnUploadThumbnail).attr("data-media-uploadedfileName");
    var mediaServer = $(btnUploadThumbnail).attr("data-media-server");      

    //Create f thumbnail            
    $.ajax({
        type: "POST",
        url: SiteURL.Prefix + "/Base/CreateThumbnail",   
        data: {
            filepath: uploadFileName,
            allThumbnails:true,
            prefix: "",
            MediaServer: mediaServer
        },
        success: function (data) {
            var thumbnailPreview = $(btnUploadThumbnail).attr("data-VirtualUploadPath") + "/" + SiteConfig.UploadFolder + "/" + $(btnUploadThumbnail).attr("data-media-appname") + "/" + $(btnUploadThumbnail).attr("data-media-username") + "/m_" + $(btnUploadThumbnail).attr("data-media-uploadedfileName");
            $(CTAButtons_Image).val(uploadFileName);
            $("#imgPreviewCTAButton_" + value_index).attr("src", thumbnailPreview);
            $("[data-upload-running]").attr("data-media-save-with-name", $.now());
            $("[data-upload-running]").removeAttr("data-upload-running");
            $("[id='Remove_" + value_index + "']").show();
        }
    });
}


function removeImage(obj) {
    var id = "#imgPreviewCTAButton_" + $(obj).attr("data-index");
    var idCTA = $("#CTAButtons_" + $(obj).attr("data-index") + "__Image");
    $(id).attr("src", $(obj).attr("data-DefaultImage"));
    $(idCTA).val("");
    $(obj).hide();
    return false;
}

