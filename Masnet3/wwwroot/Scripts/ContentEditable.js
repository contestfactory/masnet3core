﻿//Contenteditable Functions
Content = {
    UpdateText: function (obj) {        
        if ($(obj).attr("data-contentChanged") || true) {

            //validate
            var handler = eval('(' + $(obj).attr("handlers") + ')');
            var id = $(obj).attr("data-id");
            var value = ($(obj).prop("tagName") == "BUTTON" || $(obj).prop("tagName") == "A") ? $(obj).text() : $(obj).html().trim();
            //var value = ($(obj).prop("tagName") == "BUTTON" || $(obj).prop("tagName") == "A") ? $(obj).text() :
            //                    ($(obj).clone()    //clone the element
            //                        .children() //select all the children
            //                        .remove()   //remove all the children
            //                        .end()  //again go back to selected element
            //                        .text()
            //                        .trim());

            value = value.replace(/<br>$/, '').replace(/(\r\n|\n|\r)$/, "");
            if ($("#err_" + id).size() == 0) $(obj).after("<span id='" + "err_" + id + "' class='field-validation-error'></span>");
            var err = "";
            if (!handler.rules.hasOwnProperty("optional")) {
                if (!Content.ProceedRule("required", "true", obj)) {
                    $("#err_" + id).text("required");
                    return;
                }
            }
            for (var key in handler.rules) {
                if (handler.rules.hasOwnProperty(key)) {
                    if (!Content.ProceedRule(key, handler.rules[key], obj)) {
                        err = handler.messages.hasOwnProperty(key) ? handler.messages[key] : $.validator.messages.hasOwnProperty(key) ? $.validator.messages[key] : "Not allowed input";
                        $("#err_" + id).text(err.replace("{0}", handler.rules[key]));
                        return;
                    }
                }
            }
            $("#err_" + id).text("");

            //save  
            $.ajax({
                type: "POST",
                url: SiteConfig.gSiteAdrs + "/ContestTemplate/UpdateDisplay",
                data: {
                    id: $(obj).attr("data-id"),
                    name: $(obj).attr("data-contentField") ? $(obj).attr("data-contentField") : "Value",
                    value: value,
                    type: $(obj).attr("data-contentype") ? $(obj).attr("data-contentype") : 2,
                    idField: $(obj).attr("data-IDField") ? $(obj).attr("data-IDField") : "ID",
                    editSiteId: $(obj).attr("data-editSiteId"),
                    editCampaignId: $(obj).attr("data-editCampaignId")
                },
                success: function (msg) {
                    $(obj).removeAttr("data-contentChanged");
                    if ($(obj).attr("data-refresh")) {
                        if ($(obj).attr("data-refresh") == "1")
                            window.location.href = window.location.href;
                    }
                    if (msg != "" && id != msg && msg != "0")
                        $(obj).attr("data-id", msg);

                }
            });
        }
    },

    SelectText: function (obj) {
        if (window.parent) {
            if (window.parent.expandCustomCss) {
                window.parent.expandCustomCss($(obj).attr("data-editClass"));
            }
        }
    },

    ProceedRule: function (rule, param, obj) {
        var value = ($(obj).prop("tagName") == "BUTTON" || $(obj).prop("tagName") == "A") ? $(obj).text() : $(obj).html();
        value = value.replace(/<br>$/, '').replace(/(\r\n|\n|\r)$/, "");

        if (rule == "required") {
            return value != "";
        }
        else if (rule == "maxlength") {
            return value.length <= param;
        }
        else if (rule == "regex") {
            var re = new RegExp(param);
            return value == "" || re.test(value);
        }

        return true;
    },

    InitContenteditable: function () {

        $("input, button, a, select, img, [contenteditable], [csseditable]").each(function () {
            if ($(this).attr("data-ignore-contenteditable") == null) {
                $(this).unbind();
                $(this).removeAttr("onclick");
                $(this).removeProp("onclick");

                if ($(this).attr("shared-url") != null) {
                    $(this).removeAttr("shared-url");
                    $(this).removeProp("shared-url");
                }

                if ($(this).attr("target") != null) {
                    $(this).attr("target", "_self")
                }

                if ($(this).get(0).tagName == "SELECT") {
                    $(this).removeAttr("onchange");
                    $(this).removeProp("onchange");
                }

                $(this).click(function () {
                    return false;
                });
            }
        });

        if (SiteConfig.PreviewMode != "readonly") {

            $(".caret").each(function () {
                $(this)
                    .unbind()
                    .click(function () {
                        var dropdown = $(".caret").closest(".dropdown");

                        if ($(dropdown).hasClass("preview-mode")) {
                            dropdown.removeClass("preview-mode");
                        }
                        else {
                            dropdown.addClass("preview-mode");
                        }
                    });
            });

            $("[contenteditable], [csseditable]").each(function (index) {
                $(this).on("click", function (e) {
                    Content.SelectText(this);
                });
            });
        }

        if (window.parent.CallbackWhenPageReady)
            window.parent.CallbackWhenPageReady();
    }
}

$(window).load(function () {
    if (SiteConfig.IsPreview) {
        $("input, button, a, select, img,[contenteditable], [csseditable]").each(function () {
            if ($(this).attr("data-ignore-contenteditable") == null) {
                $(this).unbind();
                $(this).removeAttr("onclick");
                $(this).removeProp("onclick");

                if ($(this).attr("shared-url") != null) {
                    $(this).removeAttr("shared-url");
                    $(this).removeProp("shared-url");
                }

                if ($(this).attr("target") != null) {
                    $(this).attr("target", "_self")
                }

                if ($(this).get(0).tagName == "SELECT") {
                    $(this).removeAttr("onchange");
                    $(this).removeProp("onchange");
                }

                $(this).click(function () {
                    return false;
                });
            }
        });

        if (SiteConfig.PreviewMode != "readonly") {

            $(".caret").each(function () {
                $(this).click(function () {
                    var dropdown = $(".caret").closest(".dropdown");

                    if ($(dropdown).hasClass("preview-mode")) {
                        dropdown.removeClass("preview-mode");
                    }
                    else {
                        dropdown.addClass("preview-mode");
                    }
                });
            });

            $("[contenteditable], [csseditable]").each(function (index) {
                $(this).on("click", function (e) {
                    Content.SelectText(this);
                });
            });
        }

        if (window.parent.CallbackWhenPageReady)
            window.parent.CallbackWhenPageReady();
    }
})

