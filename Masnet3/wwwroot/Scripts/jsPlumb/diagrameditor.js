﻿function addMoreRound() {

    var txtRoundName = $('#txtRoundName'),
        txtNumofMatches = $('#txtNumofMatches'),
        slcRoundIndex = $('#slcRoundIndex');

    txtRoundName.val("");
    txtNumofMatches.val("");
    slcRoundIndex.html("");
    slcRoundIndex.closest('div').show();

    $('<option value="-2">First</option>').appendTo(slcRoundIndex);
    $('<option value="-1">Last</option>').appendTo(slcRoundIndex);
    for (var i = 0; i < $('.roundwrapper').length - 1; i++)
        slcRoundIndex.append('<option value=' + i + '>' + (i + 1) + '</option>');

    txtNumofMatches.parent().show();

    $("#dvAddRound")
        .dialog({
            width: 400,
            height: 350,
            title: "Round"
        })
        .dialog("open");

    $("#btnAddRound")
        .unbind('click')
        .click(function () {

            if ($("#dvAddRound > .frmPopup").valid()) {
                var title = txtRoundName.val();
                var numberofmatches = parseInt(txtNumofMatches.val());
                var insertedIndex = slcRoundIndex.val();

                if (numberofmatches <= 0) {
                    alert('Number of Matches must be greater than zero');
                    return false;
                }

                var slider = $('#canvas .canvasslider');                
                if (insertedIndex >= 0) {
                    if (slider.find('.roundctn:nth(' + insertedIndex + ')').attr('data-isrematch') == '1'
                        ||
                        slider.find('.roundctn:nth(' + (insertedIndex+1) + ')').attr('data-isrematch') == '1'
                       )
                    {
                        alert('Can not insert before or after rematch round.');
                        return false;
                    }
                }
                
                
                var r = addR(insertedIndex);
                setTitle(r.find('.roundTitle span'), title, 15);

                adjustCanvasslider();

                for (var i = 0 ; i < numberofmatches; i++) {
                    addM(r);
                }

                adjustRoundHeight();
                adjustMatchesMargin(r);
                repaintDiagram();


                $("#dvAddRound").dialog('close');
                $(".dvScrollTop").width($(".canvasslider").width());
                $(".wrapper1").scrollLeft($(".wrapper2").scrollLeft());

            }

            return false;
        });
}

function editRound(objRound) {

    objRound = $(objRound);

    var txtRoundName = $('#txtRoundName'),
        txtNumofMatches = $('#txtNumofMatches'),
        slcRoundIndex = $('#slcRoundIndex');

    txtRoundName.val(getTitle(objRound.find('.roundTitle span')));
    txtNumofMatches.val("");

    txtNumofMatches.parent().hide();
    slcRoundIndex.closest('div').hide();

    $("#dvAddRound")
        .dialog({
            width: 400,
            height: 350,
            title: "Round"
        })
        .dialog("open");

    $("#btnAddRound")
        .unbind('click')
        .click(function () {

            if ($("#dvAddRound > .frmPopup").valid()) {
                var title = txtRoundName.val();

                setTitle(objRound.find('.roundTitle span'), title, 15);
                $("#dvAddRound").dialog('close');
            }

            return false;
        });
}

function editMatch(objMatch) {
    var txtMatchName = $('#txtMatchName');
    var txtTopWinner = $('#txtTopWinner');

    txtMatchName.val(getTitle(objMatch.find('.matchTitle > span')));
    txtTopWinner.val(objMatch.attr('data-denowinners'));

    $("#dvEditMatch")
        .dialog({
            width: 400,
            height: 300,
            title: "Edit Match"
        })
        .dialog("open");


    $('#btnSaveEditMatch')
        .unbind('click')
        .click(function () {

            if ($("#dvEditMatch .frmPopup").valid()) {
                setTitle(objMatch.find('.matchTitle > span'), txtMatchName.val(), 8);
                objMatch.attr('data-denowinners', txtTopWinner.val());
                $('#dvEditMatch').dialog('close');
            }

            return false;
        });

}




function getHighestLinkRank(mId) {
    var arr = instance.getConnections({ source: mId });
    var hr = 0, label = '', tmp = null;
    for (var i = 0; i < arr.length; i++) {
        label = arr[i].getOverlay('label').getLabel();
        tmp = label.split('-');
        if (tmp.length == 2) {
            if (parseInt(tmp[1]) > hr) {
                hr = parseInt(tmp[1]);
            }
        }
    }
    return hr;
}


function insertMatches(objRound) {
    objRound = $(objRound);
    var txtInsertedNum = $('#txtInsertedNum'),
        slcIndex = $('#slcIndex');

    txtInsertedNum.val("");
    slcIndex.html("");

    $('<option value="-2">First</option>').appendTo(slcIndex);
    $('<option value="-1">Last</option>').appendTo(slcIndex);
    for (var i = 0; i < objRound.find('.roundwrapper .window').length - 1; i++)
        slcIndex.append('<option value=' + i + '>' + (i + 1) + '</option>');

    $("#dvAddMatch")
        .dialog({
            width: 400,
            height: 300,
            title: "Match"
        })
        .dialog("open");

    $('#btnAddMatch')
        .unbind()
        .click(function () {

            //$("div.window:hidden").remove();

            if ($("#dvAddMatch .frmPopup").valid()) {
                var insertedNum = parseInt(txtInsertedNum.val());
                var insertedIndex = parseInt(slcIndex.val());

                for (var i = 0; i < insertedNum; i++) {
                    addM(objRound, insertedIndex);                  

                    if (insertedIndex != -1 && insertedIndex != -2) {
                        insertedIndex++;
                    }
                }

                adjustRoundHeight();
                adjustMatchesMargin(objRound);
                repaintDiagram();               

                $("#dvAddMatch").dialog('close');
            }

            return false;
        })
}

function addR(idx) {
    var rNo = getNewRoundNumber() + '_' + parseInt(Math.random() * 10000000);
    var newR = $('<div class="roundctn" id="R' + rNo + '">'
                + '    <div class="roundTitle"><span title="R' + rNo + '">R' + rNo + '</span> <i class="roundEdit icon icon-gear"></i></div>'
                + '    <div class="roundwrapper">'
                + '    </div>'
                + '</div>');

    var slider = $('#canvas .canvasslider');

    if (idx == -1) {
    slider.append(newR);
    }
    else if (idx == -2) {        
        slider.prepend(newR);
    }
    else {
        slider.find('.roundctn:nth(' + idx + ')').after(newR);

        slider.find('.roundctn:nth(' + idx + ')').find('.window').each(function () {
            var uiMatch = $(this);
            var arr = instance.getConnections({ source: uiMatch.prop('id') });
            for (var i = 0; i < arr.length; i++) {
                instance.detach(arr[i]);
            }
        })

        if (slider.find('.roundctn:nth(' + (idx + 2) + ')').length > 0) {
            slider.find('.roundctn:nth(' + (idx + 2) + ')').find('.window').each(function () {
                var uiMatch = $(this);
                var arr = instance.getConnections({ target: uiMatch.prop('id') });
                for (var i = 0; i < arr.length; i++) {
                    instance.detach(arr[i]);
                }
            })
        }
    }

    return newR;
}
function addM(objRound, idx) {
    if (idx == null)
        idx = -1;

    objRound = $(objRound);

    var mNo = getNewMatchNumberByRound(objRound);
	var id = getNewId();

    //var newMatchId = 'R' + (objRound.index() + 1) + 'M' + mNo;    
    var newMatchId = 'R' + objRound.prop('id') + 'M' + id;
    var newMatchTitle = 'R' + (objRound.index() + 1) + 'M' + mNo;

    var newM = $('<div class="window jtk-node" id="' + newMatchId + '" data-isrematch="0" data-rematchallowvote="0" data-denowinners="1"><input type="text" class="ContestantNoOfMatch" title="Contestants number" value="1" maxlength="4" /><br /><div class="matchTitle"><span title="' + newMatchTitle + '">' + left(newMatchTitle, 8) + '</span> <i class="matchEdit icon icon-gear"></i></div></div>');

    if (idx == -1) {
        objRound.find('.roundwrapper').append(newM);
        repaintDiagram();
    }
    else if (idx == -2) {
        objRound.find('.roundwrapper').prepend(newM);
    }
    else {
        objRound.find('.roundwrapper .window:nth(' + idx + ')').after(newM);
        repaintDiagram();
    }

    _addEndpoints(newMatchId, rightEndPoints, leftEndPoints);

    return newM;
}

function removeM(mId) {
    if (!!instance) {
        instance.remove($('#' + mId));
    }
}
function removeAllMatches(objRound) {
    objRound = $(objRound);
    objRound.find('.roundwrapper .window').each(function (i) {
        removeM($(this).attr('id'));
    })
}

function getNewRoundNumber() {
    return $('.canvasslider .roundctn').length + 1;
}

function getNewMatchNumberByRound(rnd) {
    return $(rnd).find(".roundwrapper").find(".window").length + 1;	
}
function getNewId() {    
	var currentdate = new Date(); 	
    return currentdate.getTime();
}

var curEditConnection = null;
function editConnection(conn) {

    if (conn.sourceId == null) {
        conn = conn.connections[0];
    }

    curEditConnection = conn;

    var dvConnectionName = $('#dvConnectionName');
    var dvEditConnection = $('#dvEditConnection');

    var sourceEle = $('#' + conn.sourceId),
        targetEle = $('#' + conn.targetId);


    var rankBag = getConnectionRankBag(conn);
    dvConnectionName.text(getTitle(sourceEle.find('.matchTitle > span')) + ' - ' + getTitle(targetEle.find('.matchTitle > span')));
    dvEditConnection.find('#ConnectionName').text(getTitle(sourceEle.find('.matchTitle > span')) + ' to ' + getTitle(targetEle.find('.matchTitle > span')));
    dvEditConnection.find('.spTotalOutContestant').text(rankBag.Total);

    dvEditConnection.find('.ConnectionRuleContainer').html('');    
    var rules = getConnectionValue(conn);
    if (rules != null) {
        for (var i = 0; i < rules.length; i++) {
            addConnectionRule(rules[i]);
        }
    }
    

    $("#dvEditConnection")
        .dialog({
            width: 600,
            height: 420,
            title: "Edit Connection"
        })
        .dialog("open");
    
    $('#btnSaveEditConn')
        .unbind('click')
        .click(function () {

            if ($("#dvEditConnection .frmPopup").valid()) {

                var dvEditConnection = $('#dvEditConnection');

                var sourceNC = parseInt(sourceEle.find('.ContestantNoOfMatch').val());
                var targetNC = parseInt(targetEle.find('.ContestantNoOfMatch').val());

                var rules = [];

                dvEditConnection.find('.chkCR:checked').each(function () {
                    var cr = $(this).parent();
                    var a = cr.find('a');

                    rules.push({
                        r: a.data('r'),
                        t: a.data('t')
                    });
                });

                var tmpRB = buildRankBag(rules);

                if (sourceNC > 0) {
                    if (tmpRB.Total > sourceNC) {
                        alert('Invalid Number of contestants.');
                        return false;
                    }
                }

                if (tmpRB.Total > targetNC) {
                    alert('Invalid Number of contestants.');
                    return false;
                }

                if (tmpRB.IsDuplicate) {
                    alert('#' + tmpRB.FirstDuplicateRank + ' is duplicated!');
                    return false;
                }

                conn.setParameter('Value', rules);

                if (rules.length > 0)
                    conn.getOverlay("label").setLabel(getCRTitle(rules[0]));
                else
                    conn.getOverlay("label").setLabel("");
                $("#" + conn.targetId + " .ContestantNoOfMatch").attr("readonly", true);
                $('#dvEditConnection').dialog('close');
            }

            return false;
        });

    $('#btnDeleteConnection')
        .unbind("click")
        .click(function () {
            if (confirm('Are you sure delete the Connection?')) {
                instance.detach(conn);
                $("#" + conn.targetId + " .ContestantNoOfMatch").removeAttr("readonly");
                $('#dvEditConnection').dialog('close');
            }

            return false;
        });

    $('#btnAddCR')
        .unbind('click')
        .click(function () {

            var rb = getConnectionRankBag(conn);

            if (rb.LastRank == 0) {
                checkRank($(conn.source).prop('id'));
                var lastRank = $(conn.source).data('LastRank');
                editConnectionRule({ r: (lastRank + 1) + ',', t: 1 }, null);
            } else
                editConnectionRule({ r: (rb.LastRank + 1) + ',', t: 1 }, null);

            return false;
        })

    dvEditConnection.find('#lnkSelectAllCR')
        .unbind()
        .click(function () {
            dvEditConnection.find('.ConnectionRuleContainer .chkCR')
                .prop('checked', true);

            return false;
        })

    dvEditConnection.find('#lnkUnselectAllCR')
        .unbind()
        .click(function () {
            dvEditConnection.find('.ConnectionRuleContainer .chkCR')
                .prop('checked', false);

            return false;
        })
}

function getMatchConnections(matchId) {
    var result = {
        In: instance.getConnections({ target: matchId }),
        Out: instance.getConnections({ source: matchId })
    };

    return result;
}

function addConnectionRule(objCR, container) {

    var newCR = null;
    var isEdit = false;

    if (container == null) {
        newCR = $('<div class="connrule"><input type="checkbox" checked="checked" class="chkCR" />&nbsp;&nbsp;<a href="#" class="connruleinfo"></a></div>');
        isEdit = false;
    }
    else {
        newCR = container;
        isEdit = true;
    }

    var a = newCR.find('a');
    a.data('r', objCR.r);
    a.data('t', objCR.t);

    a.html(getCRTitle(objCR));

    if (!isEdit) {
        newCR.appendTo('.ConnectionRuleContainer');
    }

    return newCR;
}

function getConnectionValue(conn) {
    if (conn.getParameter('Value') == null)
        conn.setParameter('Value', []);

    return conn.getParameter('Value');
}

function getConnectionRule(conn, r) {
    var rules = getConnectionValue(conn);
    if (rules != null) {
        for (var i = 0; i < rules.length; i++) {
            if (rules[i].r == r) {
                return rules[i];
            }
        }
    }
    return null;
}

function findConnectionRule(rules, r) {
    if (rules != null) {
        for (var i = 0; i < rules.length; i++) {
            if (rules[i].r == r) {
                return rules[i];
            }
        }
    }
    return null;
}

function getConnectionRankBag(conn) {
    var rules = getConnectionValue(conn);
    return buildRankBag(rules);
}

function buildRankBag(rules) {
    var cr = null,
        RankBag = {
            Data: {},
            Total: 0,
            FirstRank: 1000000,
            LastRank: 0,
            IsDuplicate: false,
            FirstDuplicateRank: null
        };

    for (var i = 0; i < rules.length; i++) {
        cr = rules[i];
        if (cr.t == 1) {
            var tmpArr = cr.r.split(',');
            for (var j = parseInt(tmpArr[0]) ; j <= parseInt(tmpArr[1]) ; j++) {
                if (RankBag.Data[j] == null) {
                    RankBag.Data[j] = 1;
                    RankBag.Total++;
                }
                else {
                    RankBag.Data[j]++;
                    RankBag.IsDuplicate = true;
                    if (RankBag.FirstDuplicateRank == null)
                        RankBag.FirstDuplicateRank = j;
                }

                if (j < RankBag.FirstRank)
                    RankBag.FirstRank = j;

                if (j > RankBag.LastRank)
                    RankBag.LastRank = j;
            }
        }
        else {
            var tmpArr = cr.r.split(',');
            for (var j = 0; j < tmpArr.length; j++) {
                if (tmpArr[j] != "") {
                    var rank = parseInt(tmpArr[j]);
                    if (RankBag.Data[rank] == null) {
                        RankBag.Data[rank] = 1;
                        RankBag.Total++;
                    }
                    else {
                        RankBag.Data[rank]++;
                        RankBag.IsDuplicate = true;
                        if (RankBag.FirstDuplicateRank == null)
                            RankBag.FirstDuplicateRank = rank;
                    }

                    if (rank < RankBag.FirstRank)
                        RankBag.FirstRank = rank;

                    if (rank > RankBag.LastRank)
                        RankBag.LastRank = rank;
                }
            }
        }
    }

    return RankBag;
}


function getCRTitle(objCR) {
    if (objCR != null && objCR != "undefined") {
        if (objCR.t == 1) {
            var tmpArr = objCR.r.split(',');
            return tmpArr[0] + ' .. ' + tmpArr[1];
        } else {
            var tmpArr = objCR.r.split(',');
            var re = "";

            if (tmpArr.length > 0) {
                re = tmpArr[0];
                for (var i = 1; i < tmpArr.length; i++)
                    re += ", " + tmpArr[i];
            }
            return re;
        }
    }
}

function editConnectionRule(cr, ctl) {
    var dvContainer = $("#dvEditConnectionRule");
    var isEdit = (ctl != null);

    var txtCRFrom = dvContainer.find('#txtCRFrom'),
        txtCRTo = dvContainer.find('#txtCRTo'),
        txtCRRanks = dvContainer.find('#txtCRRanks');

    txtCRFrom.val('');
    txtCRTo.val('');
    txtCRRanks.val('');



    //load info
    dvContainer.find('[name=CRType][value=' + cr.t + ']').prop('checked', true);
    dvContainer.find('[name=CRType]:checked').trigger('change');

    if (cr.t == 1) {
        var tmpArr = cr.r.split(',');
        txtCRFrom.val(tmpArr[0]);
        txtCRTo.val(tmpArr[1])
    }
    else {
        txtCRRanks.val(cr.r);
    }

    dvContainer
        .dialog({
            width: 600,
            height: 400,
            title: "Connection Rule"
        })
        .dialog("open");


    dvContainer.find('.frmPopup').validate().resetForm();
    dvContainer.find('.frmPopup').find('input[text],textarea').removeClass('input-validation-error');

    dvContainer.find('#btnSaveEditCR')
        .unbind()
        .click(function () {

            txtCRRanks.val(txtCRRanks.val().replace(/ /g, ''));
            txtCRTo.val(txtCRTo.val().replace(/ /g, ''));
            txtCRFrom.val(txtCRFrom.val().replace(/ /g, ''));

            if (dvContainer.find('.frmPopup').valid()) {

                if (dvContainer.find('[name=CRType]:checked').val() == 1) {
                    if (parseInt(txtCRFrom.val()) > parseInt(txtCRTo.val())) {
                        alert('From rank must be less than To rank.');
                        return false;
                    }
                }
                else {                    
                    if (!isValidRankList(txtCRRanks.val())) {
                        alert('Invalid Ranks');
                        return false;
                    }
                }

                var obj=null,tmpobj = null;
                var currentListRule = (getConnectionValue(curEditConnection));

                var tmpListRule = JSON.parse(JSON.stringify(currentListRule));

                if (isEdit)
                    tmpobj = findConnectionRule(tmpListRule, cr.r);
                else
                    tmpobj = {};

                if (tmpobj != null) {
                    tmpobj.t = dvContainer.find('[name=CRType]:checked').val();
                    tmpobj.r = tmpobj.t == 1 ? txtCRFrom.val() + "," + txtCRTo.val() : txtCRRanks.val();
                }
                
                if (!isEdit) //add new element for checking     
                    tmpListRule.push(tmpobj);

                var rb = buildRankBag(tmpListRule);
                if (rb.IsDuplicate) {
                    alert('#' + rb.FirstDuplicateRank + ' is duplicated!');
                    return false;
                }

                var SourceNoOfContestant = parseInt($(curEditConnection.source).find('.ContestantNoOfMatch').val());
                if (SourceNoOfContestant > 0) {
                    if (rb.LastRank > SourceNoOfContestant) {
                        alert('Invalid rank.');
                        return false;
                    }
                }
                                
                if (isEdit)
                    obj = getConnectionRule(curEditConnection, cr.r);
                else { // add new rule
                    obj = {};
                    currentListRule.push(obj);
                }

                if (obj != null) {
                    obj.t = tmpobj.t;
                    obj.r = tmpobj.r;
                }

                addConnectionRule(obj, (isEdit ? ctl.parents('.connrule') : null));
                dvContainer.dialog('close');
            }

            return false;
        })
}

var regRankList = /[^0-9,]/;
function isValidRankList(val) {
    if (!regRankList.test(val) == false)
        return false;
    else {
        var tmpArr = val.split(','),curVal = '';
        for (var i = 0; i < tmpArr.length; i++) {
            curVal = tmpArr[i];
            if (!isDigit(curVal))
                return false;
            else if (parseInt(curVal) <= 0)
                return false;                
        }

        return true;
    }
}

function getConnectionInfo(conn) {

    var arrFromPoint = conn.endpoints[0].getUuid().split('_'),
        arrToPoint = conn.endpoints[1].getUuid().split('_');

    var objCon = {
        ID: 0,
        FromMatchID: 0,
        ToMatchID: 0,
        NumOfContestants: 0,
        FromRank: 0,
        FromPoint: arrFromPoint[arrFromPoint.length - 1],
        ToPoint: arrToPoint[arrToPoint.length - 1],
        FromMatchUIID: '',
        ToMatchUIID: '',
        Value: JSON.stringify(getConnectionValue(conn))
    };

    var uiFromMatch = $('#' + conn.sourceId),
        uiToMatch = $('#' + conn.targetId);

    objCon.FromMatchUIID = uiFromMatch.prop('id');

    if (uiFromMatch.attr('data-matchid') != null)
        objCon.FromMatchID = uiFromMatch.attr('data-matchid');

    objCon.ToMatchUIID = uiToMatch.prop('id');

    if (uiToMatch.attr('data-matchid') != null)
        objCon.ToMatchID = uiToMatch.attr('data-matchid');


    //get num of contestants info
    var rankBag = getConnectionRankBag(conn);
    objCon.NumOfContestants = rankBag.Total;

    return objCon;
}

function checkRank(matchId) {
    var arr = instance.getConnections({ source: matchId });
    var tmpArr = null,conn = null,rankBag = null;
    var SumRankBag = {};
    var LastRank = 0, TotalContestantsOut = 0;
    var uiMatch = $('#' + matchId);


    for (var i = 0; i < arr.length; i++) {
        conn = arr[i];
        rankBag = getConnectionRankBag(conn);
        if (rankBag.IsDuplicate) {
            return false;
        }

        for (var x in rankBag.Data) {
            if (SumRankBag[x] == null) {
                SumRankBag[x] = 1;
            }
            else {
                conn.setType('err');
                $(conn.canvas).css('z-index', 100);
                return false;
            }
        }

        if (rankBag.LastRank > LastRank) 
            LastRank = rankBag.LastRank;        

        TotalContestantsOut += rankBag.Total;
       
        conn.setType('basic');
        $(conn.canvas).css('z-index', 4);
    }

    uiMatch.data('LastRank', LastRank);
    uiMatch.data('TotalContestantOut', TotalContestantsOut);

    return true;
}


function hasAlreadyLinked(connection) {
    return instance.getConnections({ source: connection.sourceId, target: connection.targetId }).length > 1;
}

function hasInputLink(matchId) {
    return instance.getConnections({ target: matchId }).length > 0;
}

function hasOutputLink(matchId) {
    return instance.getConnections({ source: matchId }).length > 0;
}

var intRegex = /^\d+$/;
function isDigit(val) {
    if (val == "")
        return false;
    return intRegex.test(val);
}

function addRules() {
    var defaultNumOfRows = 5;
    $("#tblAddRules tbody").html("");

    var sourceMatches = getAddRuleSourceMatches();

    for (var index = 0; index < defaultNumOfRows; index++) {
        loadEmptyRuleItem(index + 1, sourceMatches);
    }

    $("#dvAddRules")
        .dialog({
            width: 800,
            height: 550,
            title: "Add Rules",
            autoOpen: false,
            modal: true
        })
        .dialog("open");
};//End: function addRules()

function loadMoreRule(num, sourceMatches) {
    if (num == null) {
        num = 5;
    }
    if (sourceMatches == null) {
        sourceMatches = getAddRuleSourceMatches();
    }

    for (var index = 0; index < num; index++) {
        loadEmptyRuleItem(null, sourceMatches);
    }

    $("#dvTableAddRules").scrollTop(10000);
}
function loadEmptyRuleItem(index, sourceMatches) {
    if (index == null) {
        index = $("#tblAddRules > tbody > tr").length + 1;
    }

    var id = index + "_" + Date.now();
    var html = "";
    html += "<tr id='tr_" + id + "' data-ruleid='" + id + "'>";
    html += "   <td data-rule='from' valign='top'>";
    html += "       <select class='form-control' style='width: 150px;' onchange='loadTargetMatches(this);' id='rule_from_" + id + "'data-ruleid='" + id + "'>";
    html += "           <option value=''></option>";
    html += "       </select>";
    html += "   </td>";
    html += "   <td data-rule='to' valign='top'>";
    html += "       <select class='form-control' style='width: 150px;' onchange='loadRules(this);' id='rule_to_" + id + "'data-ruleid='" + id + "'>";
    html += "           <option value=''></option>";
    html += "       </select>";
    html += "   </td>";
    html += "   <td data-rule='rank'>";
    html += "       <textarea class='form-control' rows=2 id='rule_rank_" + id + "'data-ruleid='" + id + "' style='height: 55px'></textarea>";
    //html += "       <p class='text-muted'>Example: 1..1; 2,4,6;</p>";
    html += "       <span class='field-validation-error' id='sprule_errorMessage_" + id + "'></span>";
    html += "   </td>";
    html += "</tr>";

    $("#tblAddRules tbody").append(html);
    if (sourceMatches == null) {
        sourceMatches = getAddRuleSourceMatches();
    }

    for (var index = 0; index < sourceMatches.length; index++) {
        var matchId = sourceMatches[index];
        var $matchId = $("#" + matchId);

        html = "<option data-rule-matchid='" + matchId + "' value='" + matchId + "' ";
        var roundid = $.trim($matchId.closest("[data-roundid]").attr("data-roundid"));
        if (roundid == '') {
            roundid = $matchId.closest(".roundctn").attr("id");
        }
        html += "data-roundid='" + roundid + "' ";
        html += ">";
        html += $.trim($matchId.closest(".roundctn").find(".roundTitle > span").attr("title")) + ": ";
        html += $matchId.find(".matchTitle span").attr("title");
        html += "</option>";
        $("#rule_from_" + id).append(html);
    }

    //sortDropdownlistByText("#rule_from_" + id);
};//End: function loadEmptyRuleItem()

function getAddRuleSourceMatches() {
    var arrSourceMatches = [];
    
    $(".canvasslider > .roundctn:not(:last-child)").each(function () {
        if ($(this).attr("data-isrematch") != "1" && $(this).next().attr("data-isrematch") != "1") {
            $(this).find(".roundwrapper > .window.jtk-node").each(function () {
                var matchId = $(this).attr("id");

                if (arrSourceMatches.indexOf(matchId) == -1) {
                    arrSourceMatches.push(matchId);
                }
            });
        }
    });

    return arrSourceMatches;
    //return arrSourceMatches.sort();
}

function loadTargetMatches(obj) {
    var selectedMatchId = $(obj).val();

    var data_ruleid = $(obj).attr("data-ruleid");
    $("#rule_rank_" + data_ruleid).val("");
    var $sprule_errorMessage = $("#sprule_errorMessage_" + data_ruleid);
    $sprule_errorMessage.html("");

    $("#rule_to_" + data_ruleid + " option[data-rule-matchid]").remove();

    var arrSelectedMatchId = [];
    $("#tblAddRules").find("[id^='rule_from_']").each(function () {
        if ($(this).val() != '') {
            arrSelectedMatchId.push($(this).val());
        }
    });

    $("#tblAddRules").find("[id^='rule_from_'] option").attr("style", "");

    if(selectedMatchId != '')
    {
        var roundId = $(obj).find("option[value='" + selectedMatchId + "']").attr("data-roundid");
        var arrtargetMatches = getAddRuleTargetMatches(roundId);
        
        for (var index = 0; index < arrtargetMatches.length; index++) {
            var matchId = arrtargetMatches[index];
            var $matchId = $("#" + matchId);

            var html = "<option data-rule-matchid='" + matchId + "' value='" + matchId + "' ";
            var roundid = $.trim($matchId.closest("[data-roundid]").attr("data-roundid"));
            if (roundid == '') {
                roundid = $matchId.closest(".roundctn").attr("id");
            }
            html += "data-roundid='" + roundid + "' ";
            html += ">";
            html += $.trim($matchId.closest(".roundctn").find(".roundTitle > span").attr("title")) + ": ";
            html += $matchId.find(".matchTitle span").attr("title");
            html += "</option>";

            $("#rule_to_" + data_ruleid).append(html);
        }

        //sortDropdownlistByText("#rule_to_" + data_ruleid);

        disableTargetOptions(selectedMatchId);
    }
}

function disableTargetOptions(selectedMatchId) {
    selectedMatchId = $.trim(selectedMatchId);

    if (selectedMatchId == '' || selectedMatchId === undefined) {
        return;
    }

    var selectedTargets = [];

    $("#tblAddRules").find("[id^='rule_from_']").each(function () {
        var fromMatchId = $.trim($(this).val());
        if (fromMatchId == selectedMatchId) {
            var ruleid = $(this).attr("data-ruleid");
            var $target = $("#rule_to_" + ruleid);
            $target.find("option").attr("style", "");

            var toMatchId = $.trim($target.val());
            if (toMatchId != "" && toMatchId !== undefined) {
                selectedTargets.push(toMatchId);
            }
        }
    });

    if (selectedTargets.length > 0) {
        $("#tblAddRules").find("[id^='rule_to_']").each(function () {
            var ruleid = $(this).attr("data-ruleid");
            var $source = $("#rule_from_" + ruleid);
            if ($.trim($source.val()) == selectedMatchId) {
                var target = $(this).val();
                for (var index = 0; index < selectedTargets.length; index++) {
                    var selectedTarget = selectedTargets[index];

                    if (target != selectedTarget) {
                        $(this).find("option[value='" + selectedTarget + "']").attr("style", "display:none");
                    }
                }
            }
        });
    }
}//End: function disableTargetOptions

function getAddRuleTargetMatches(roundId) {
    var arrtargetMatches = [];
    
    $("#Round_" + roundId + ", #" + roundId).next().find(".roundwrapper > .window.jtk-node").each(function () {
        var matchId = $(this).attr("id");

        if (arrtargetMatches.indexOf(matchId) == -1) {
            arrtargetMatches.push(matchId);
        }
    });

    //return arrtargetMatches.sort();
    return arrtargetMatches;
};//End: function getAddRuleTargetMatches(roundId)

function loadRules(obj) {
    var data_ruleid = $(obj).attr("data-ruleid");
    var targetId = $(obj).val();
    $("#rule_rank_" + data_ruleid).val("");
    var $sprule_errorMessage = $("#sprule_errorMessage_" + data_ruleid);
    $sprule_errorMessage.html("");

    var sourceId = $("#rule_from_" + data_ruleid).val();
    if (targetId != '') {
        var ruleValues = '';

        var conns = instance.getConnections({ source: sourceId, target: targetId });
        for (var index = 0; index < conns.length; index++) {
            var conn = conns[index];
            var rules = getConnectionValue(conns[0]);

            for (var idx = 0; idx < rules.length; idx++) {
                ruleValues += $.trim(getCRTitle(rules[idx])) + "; "
            }
        }

        $("#rule_rank_" + data_ruleid).val(ruleValues);
    }

    disableTargetOptions(sourceId);
};//End: function loadRules(obj)

function saveAddRules() {
    var isValid = true;
    $("#tblAddRules tbody tr").each(function () {
        var data_ruleid = $(this).attr("data-ruleid");

        var sourceId = $.trim($("#rule_from_" + data_ruleid).val());
        var targetId = $.trim($("#rule_to_" + data_ruleid).val());
        var ruleValues = $.trim($("#rule_rank_" + data_ruleid).val());
        var $sprule_errorMessage = $("#sprule_errorMessage_" + data_ruleid);
        $sprule_errorMessage.html("");

        if (sourceId != '' && targetId != '') {
            var conns = instance.getConnections({ source: sourceId, target: targetId });
            var rules = [];

            if (ruleValues == '') {
                if (conns == null || conns.length == 0) {
                    conns = [];
                    var conn = instance.connect({
                        uuids: [instance.getEndpoints(sourceId)[1].getUuid(), instance.getEndpoints(targetId)[4].getUuid()]
                        , editable: true
                    });

                    conn.sourceId = sourceId;
                    conn.targetId = targetId;

                    if (validateConnection(conn)) {
                        conns.push(conn);
                    }
                }

                for (var index = 0; index < conns.length; index++) {
                    var conn = conns[index];

                    conn.setParameter('Value', rules);
                    conn.getOverlay("label").setLabel("");
                }
            }
            else {
                var arrRuleValues = ruleValues.split(/[;\n\r]/)
                var errorMessage = "";
                var tmpListRule = [];

                var sourceNC = parseInt($("#" + sourceId).find('.ContestantNoOfMatch').val());
                var targetNC = parseInt($("#" + targetId).find('.ContestantNoOfMatch').val());

                for (var index = 0; index < arrRuleValues.length; index++) {
                    var ruleValue = $.trim(arrRuleValues[index]);
                    if (ruleValue != '') {
                        var tmpobj = {};
                        if (ruleValue.indexOf("..") >= 0) {
                            var rangeValues = ruleValue.split("..");
                            if (rangeValues.length != 2 || ($.trim(rangeValues[0])).match(/^\d+$/) == null || ($.trim(rangeValues[1])).match(/^\d+$/) == null) {
                                errorMessage += "<p><strong>" + ruleValue + ":</strong> Invalid value</p>";
                                isValid = false;
                                continue;
                            }
                            else if (parseInt(rangeValues[0]) > parseInt(rangeValues[1])) {
                                errorMessage += "<p><strong>" + ruleValue + ":</strong> From rank must be less than To rank</p>";
                                isValid = false;
                                continue;
                            }

                            tmpobj.t = 1;
                            tmpobj.r = $.trim(rangeValues[0]) + "," + $.trim(rangeValues[1]);
                        }
                        else {
                            if (!isValidRankList(ruleValue)) {
                                errorMessage += "<p><strong>" + ruleValue + ":</strong> Invalid Ranks</p>";
                                isValid = false;
                                continue;
                            }
                            else {
                                if (parseInt(ruleValue) > sourceNC) {
                                    errorMessage += "<p><strong>" + ruleValue + ":</strong> Invalid Ranks</p>";
                                    isValid = false;
                                    continue;
                                }
                            }

                            tmpobj.t = 2;
                            tmpobj.r = ruleValue;
                        }

                        tmpListRule.push(tmpobj);

                        var rb = buildRankBag(tmpListRule);

                        if (rb.IsDuplicate) {
                            errorMessage += "<p><strong>" + ruleValue + ":</strong> is duplicated</p>";
                            isValid = false;
                            continue;
                        }

                        rules.push(tmpobj);
                    }
                }

                if (errorMessage != '') {
                    $sprule_errorMessage.html(errorMessage);
                }
                else {
                    var tmpRB = buildRankBag(rules);

                    if (sourceNC > 0) {
                        if (tmpRB.Total > sourceNC) {
                            $sprule_errorMessage.html('<p>Invalid Number of contestants.</p>');
                            isValid = false;
                            return;
                        }
                    }

                    if (tmpRB.Total > targetNC) {
                        $sprule_errorMessage.html('Invalid Number of contestants.');
                        isValid = false;
                        return;
                    }

                    if (tmpRB.IsDuplicate) {
                        $sprule_errorMessage.html('#' + tmpRB.FirstDuplicateRank + ' is duplicated!');
                        isValid = false;
                        return;
                    }

                    if (conns == null || conns.length == 0) {
                        conns = [];
                        var conn = instance.connect({
                                                        uuids: [instance.getEndpoints(sourceId)[1].getUuid(), instance.getEndpoints(targetId)[4].getUuid()]
                                                        , editable: true
                                                    });

                        conn.sourceId = sourceId;
                        conn.targetId = targetId;

                        if (validateConnection(conn)) {
                            conns.push(conn);
                        }
                    }

                    if (conns != null && conns.length > 0) {
                        for (var indexConn = 0; indexConn < conns.length; indexConn++) {
                            var conn = conns[indexConn];
                            conn.setParameter('Value', rules);

                            if (rules.length > 0)
                                conn.getOverlay("label").setLabel(getCRTitle(rules[0]));
                            else
                                conn.getOverlay("label").setLabel("");
                        }
                    }
                }
            }
        }
    });

    if (isValid) {
        $('#dvAddRules').dialog('close');
    }
}//End: function saveAddRules();