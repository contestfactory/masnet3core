﻿function countContestRound(contestID) {

    var totalRound = 0;
    var url = SiteURL.Prefix + '/CustomerContest/CountContestRound';

    $.ajax({
        type: "POST",
        url: url,
        data: {
            ContestID: contestID
        },
        async: false,
        success: function (msg) {
            totalRound = parseInt(msg);
        }
    });

    return totalRound;
}