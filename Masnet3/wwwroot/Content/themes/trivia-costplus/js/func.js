(function ($) {
    "use strict";
    $(document).ready(function () {
        // Customize checkboxes
        var checkBox = $('input[type="checkbox"]');
        $(checkBox)
            .each(function () {
                if ($(this).is(':checked')) {
                    var parent = $(this).parent();
                    if (parent.hasClass('selected').toString() === 'false') {
                        $(this).parent().addClass('selected');
                    }
                }
            })
            .click(function () {
                $(this).parent().toggleClass('selected');
            });

        // Register
        $('.form-register').submit(function (e) {
            e.preventDefault();
            var isOK = true;
            $(this).find('input').each(function () {
                var parent = $(this).closest('.form-group');
                var value = $.trim($(this).val());
                if (value.length === 0) {
                    isOK = false;
                    parent.addClass('has-error');
                }
                else {
                    parent.removeClass('has-error');
                    if ($(this).attr('id') === 'email') {
                        if (!$.fn.validEmail(value)) {
                            isOK = false;
                            parent.addClass('has-error');
                        }
                    }
                }
            });
            if (isOK) {
                $.fn.notification('32');
                this.submit();
            }
        });

        // Subscriber
        $('.form-subscriber').submit(function (e) {
            e.preventDefault();
            var objEmail = $(this).find('#email');
            var email = $.trim(objEmail.val());
            var parent = objEmail.closest('.form-group');

            if (email.length === 0 || !$.fn.validEmail(email)) {
                parent.addClass('has-error');
                return false;
            }
            else {
                parent.removeClass('has-error');
                $.fn.notification('59');
                this.submit();
            }
        });

        // video
        if ($('#watch-video').length > 0) {
            videojs('watch-video');
        }

        // questions
        var questions = $('.form-questions');
        if (questions.length > 0) {
            var answer = questions.find('.options span');
            answer.click(function () {
                var parent = $(this).closest('.options');
                if (parent.find('.selected').length === 0) {
                    var answer_value = $(this).attr('data-value');
                    var answer_text = $(this).text();
                    if (answer_value === '3') {
                        $(this)
                            .addClass('correct')
                            .html('<label>Correct!</label>' + answer_text);
                    }
                    else {
                        $(this)
                            .addClass('incorrect')
                            .html('<label>Incorrect!</label>' + answer_text);
                        parent.find('li:eq(2) span').addClass('correct result');
                    }
                    $(this).addClass('selected');
                    answer.addClass('done');

                    $.fn.notification('80 and 85');
                }
            });
        }
    });

    $.fn.notification = function (line) {
        alert('[func.js]: Please replace your code at line ' + line + '!');
    };

    $.fn.validEmail = function (email) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(email);
    };
})(jQuery);