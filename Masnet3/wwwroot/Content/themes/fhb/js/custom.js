var responsiveImg;
$( document ).ready(function() {
	// Handler for .ready() called.
    $("#btn-login").click(function(){
        $("#login_dialog").toggle();
    });

    //bind click for 'Already Registered' button
    $('#btnAlreadyRegistered').bind('click', function(e)
    {
      /*var loginDiv;
      loginDiv = $("#dvRegisterLogin");
      $("#registerColumnsContainer").prepend(loginDiv);*/

      // var iframe = $('#frame1', window.parent.document);

      console.log("btnAlreadyRegistered clicked");

      var anchor = $(this);
      // window.parent.scrollPage( $(anchor.attr('href')).offset().top );
      parent.postMessage("scrollPage:" + $(anchor.attr('href')).offset().top, "*");
      
      e.preventDefault();

      $('#btnAlreadyRegistered').unbind('click');
    });

    //binding clicks on photos to enlrge them
    openPhoto();

});
/*function scrollToAnchor(anker) { 
    var childWindow =  document.getElementById("frame1").contentWindow;
    childWindow.scrollTo(0,childWindow.document.getElementById(anker).offsetTop);
}
*/

var activePhoto;
var closePhotoDiv = "<div class='close-photo'>X</div>";

function openPhoto()
{
  $(".gallery-container .entry-photo a").click(function() 
  {
    if( $( this ).hasClass("active") )//need to close
    {
      closePhoto();
    }
    else //open photo
    {
      //close previously opened photo
      closePhoto();

      //open clicked photo
      $(this).addClass("active");
      activePhoto = $(this);

      //append X close div
      $(this).prepend( closePhotoDiv );

      //bind close photo div
      // $( closePhotoDiv ).click(function() {
      //   closePhoto();
      // });  
    }

    

  });
}
function closePhoto()
{
  if( activePhoto )
  {
    $(activePhoto).removeClass("active");
    console.log( "length: " + $(activePhoto).children(".close-photo").length );
    $(activePhoto).children(".close-photo").remove();
  }
}

$(window).load(function()
{
    // $("body").css("background", "#fff");
    $("body").removeClass("loading");
    $("body, html").css("height", "auto");
    $(".page-wrapper, footer").show();
    
    resize();


    //set height of iframe
    
    // window.parent.setIframeHeight( $(window).height() );


    //send message to parent
    // console.log( "window.parent.postMessage" );
    // parent.postMessage("message", "*");


    setTimeout( function(){
      // console.log( "$(window).height(): " + $("body").height() );
      // console.log( "$(window).html(): " + $("body").html() );
      var h = Number($("body").height());
      // window.parent.setIframeHeight( $("body").height() );

      parent.postMessage("setIframeHeight:" + h, "*");

    }, 2000 );

    window.onscroll = function(){
        var scroll = window.scrollY;

        /*if(scroll < 0)
        {
          // $("footer img").css("opacity", 0);
        	$("footer img").css("display", "none");
        }
        else
        {
        	// $("footer img").css("opacity", 1);
          $("footer img").css("display", "block");
        }*/
    }
});
$( window ).resize(function() {
    resize();
});
var scale;
function resize()
{
  // alert( $('footer').height() );

  scale = $(window).width() / 1920;
  scale = ( scale > 1 ) ? 1 : scale ;
  if(scale > 0.3)
  {
  	$('.logo-header-container').css(
  		{
  			'width': 190 * scale,
  			'margin-left': - 190 / 2 * scale
  		}
  	);
  }	
  else
	$('.logo-header-container').css(
  		{
  			'width': 190 * 0.3,
  			'margin-left': - 190 / 2 * 0.3
  		}
  	);
	makeSameHeight(".entry-description");
  responsiveImg();
}
function responsiveImg()
{
  // console.log("responsiveImg");
  imgsArr = $("[data-src]");
  imgsArr.each(function(index) {
    if ($(this).css('display') !== 'none') {
      this.src = $(this).data('src');
      // imgsArr.splice(index, 1)
    }
  });
}
function makeSameHeight(selector)
{
	var maxHeight = 0;
	$( selector ).each(function( i ) {
	    $(this).css("height", "auto")
	});
	$( selector ).each(function( i ) {
	    if( $(this).height() > maxHeight )
	    	maxHeight = $(this).height();
	});
	$( selector ).each(function( i ) {
	    $(this).css("height", maxHeight + 16)
	});

	// console.log("maxHeight: " + maxHeight);
}