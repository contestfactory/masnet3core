<%
Server.ScriptTimeout = 400
%> 
<!-- #include file="base_common.asp" -->
<% call CheckAdminSession("admin_emails.asp") %>
<!-- #include file="base_tools.asp" -->
<!-- #include file="base_list.asp" -->
<!--#INCLUDE FILE="base_params.asp"-->
<!--#INCLUDE FILE="base_member.asp"-->
<!--#INCLUDE FILE="base_sweeps.asp"-->
<!-- #include file="base_fan.asp" -->
<!-- #include file="base_judge.asp" -->
<!-- #include file="base_members.asp" -->
<!-- #include file="admin_base_members.asp" -->
<!--#INCLUDE FILE="includes/TemplateHeader.asp"-->
<!--#INCLUDE FILE="admin_base_emails.asp"-->

<%

' METHODS
' ViewRequest, MainParams, PageName
' FanLoginRequest,

'************************************************************
'************************************************************

call ProcessActionRequest()

'************************************************************

Dim strLinkP, kDefault
Dim sPriority,sRoleType 
Dim arrTo, arrRoleTo, arrAdminRoleTo
Dim action, ID
Dim sToRoles, sCCRoles, sBCCRoles
Dim sToCat,sCCCat,sBCCCat
Dim sToInterest,sCCInterest,sBCCInterest, sCountries, sIsIncludeCountry, sAgeGroups, sGenders
Dim sToSubscribers
Dim mm_strSessionKey
Dim bSendOnlyLeader : bSendOnlyLeader = False

mm_strSessionKey = MemaCreateGUID(32)

If Request.QueryString("id")&"" <> "" Then
	ID = Request.QueryString("id")
End If 

strLinkP =""
kDefault = 0

If Not HasPermission(k_AddHoc) Then
	Response.End
End If

'Count Emails before sending
If Request("actionPage") = "3" Then
    Response.Clear
    Response.Write "{CntTo:" & GetCountEmails("To","count") & ",ToList:""" & GetCountEmails("To","getlist") &_
                   """,CntCC:" & GetCountEmails("CC","count") & ",CCList:""" & GetCountEmails("CC","getlist") &_ 
                   """,CntBCC:" & GetCountEmails("BCC","count") & ",BCCList:""" & GetCountEmails("BCC","getlist") & """}"
    Response.End
End If

dim uViewSlct, sAttachments
Dim arrFiles(5)
'**********************************************************
action = Request.Form("actionPage")

'Send Add Hoc
If (action = "1" Or action = "2") Then	
	
	'save uploaded files
	sSessionKey = Request.Form("SessionKey")
	
	If gVirtualUploadPath = "" Then
		strPathTmp = Server.MapPath("Tmp\Upload\") & "\"
	Else
		strPathTmp = gVirtualFolderPath & "Tmp\Upload\"
	End If
	
	Set File = Server.CreateObject("Scripting.FileSystemObject")
	
	'For idx = 1 To 5	
	'	arrFiles(idx) = Request.Form("Txt_File" & idx)
	'	If arrFiles(idx) <> "" And File.FileExists(strPathTmp & sSessionKey & "_file" & idx & "-" & arrFiles(idx)) Then 
	'		fileName = arrFiles(idx)
	'		fileExt = Mid(fileName, InstrRev(fileName, "."))
	'		add = 0
	'		While File.FileExists(PhysicalUploadDir() & "\ADHOCEmail\" & fileName)
	'			add = add + 1
	'			fileName = Left(arrFiles(idx), InStrRev(arrFiles(idx), ".") - 1) & " - " & add & fileExt
	'		WEnd
			
	'		File.MoveFile strPathTmp & sSessionKey & "_file" & idx & "-" & arrFiles(idx), PhysicalUploadDir() & "\ADHOCEmail\" & fileName
	'		arrFiles(idx) = fileName
			
	'		If sAttachments = "" Then
	'			sAttachments = PhysicalUploadDir() & "\ADHOCEmail\" & arrFiles(idx)
	'		Else	
	'			sAttachments = sAttachments & ";" & PhysicalUploadDir() & "\ADHOCEmail\" & arrFiles(idx)
	'		End If
	'		
	'	End If	
	'Next 

	uploadedFiles = Split(Request.Form("uploaded_file"), ";")
	
	For idx = 0 To UBound(uploadedFiles)
		uplFile = uploadedFiles(idx)
		
		If InStr(uplFile, ":") > 0 Then
			filename = Split(uplFile, ":")(1)
			
			If File.FileExists(strPathTmp & Replace(uplFile, ":", "-")) Then
				newFileName = filename
				fileExt = Mid(newFileName, InstrRev(newFileName, "."))
				
				add = 0
				While File.FileExists(PhysicalUploadDir() & "\ADHOCEmail\" & newFileName)
					add = add + 1
					newFileName = Left(filename, InStrRev(filename, ".") - 1) & " - " & add & fileExt
				WEnd
				
				File.MoveFile strPathTmp & Replace(uplFile, ":", "-"), PhysicalUploadDir() & "\ADHOCEmail\" & newFileName
				
				uplFile = newFileName
			Else
				uplFile = ""
			End If
		End If
		
		If sAttachments = "" Then
			sAttachments = PhysicalUploadDir() & "\ADHOCEmail\" & uplFile
		Else	
			sAttachments = sAttachments & ";" & PhysicalUploadDir() & "\ADHOCEmail\" & uplFile
		End If
	Next
		
	'Get CC, BCC 
	Dim sEmailCC,sEmailBCC, sList, sSentList
	sEmailCC = GetEmails("CC")
	sEmailBCC = GetEmails("BCC")
	sList = Request.Form("txtTo")
	sList = RemoveDisabledEmails(sList&"")

	If action = "1" Then	'send email out
	
		'SendNormalEmail()
		
		ID = ""		
		ID = SaveDraft(1)
		%>
		<script type="text/javascript">
			alert("Ad hoc email is saved to queue!");
			window.location.href = "admin_params_edit.asp?rCodeSlct=56&rMailType=3&id=<%=ID%>"
		</script>
		<%
	Else	'save as a draft
		
		ID = Request.Form("id")&""
		ID = SaveDraft(2)
		
		%>
		<script type="text/javascript">
			alert("Ad hoc email is saved!");
			window.location.href = "admin_params_edit.asp?rCodeSlct=56&rMailType=2&id=<%=ID%>"
		</script>
		<%
		
			
	End If
	
	
End If

Function SendNormalEmail()

'Get To
	sql = "  SELECT code,validationkey,fname,lname,r.memberid,Email,r.paypal_approved,r.IsPublicInfo, registeredfrom FROM regmem r "_
			& " INNER JOIN registration on registration.memberid = r.memberid "_
			& " WHERE registration.confirmed <> 'x' And r.send_me_info = 1 And r.newsletter = 1 And ISNULL(r.Email, '') <> ''" _
			& " AND ( 1 = 0 "
	If Request.Form("txtTo") <> "" Then
		sql = sql & " OR email in ( SELECT Col FROM dbo.ToTable(@email,',')) "	
	End If
	'>>(Replace(Request.Form("txtTo"),";","','"))
	If Request.Form("hidRoleTo") <> "" Then
		sql = sql & " OR Role_ID in (" + CLngList(Replace(Request.Form("hidRoleTo"),";",",")) + ")" 
	End If	
				
	If Request.Form("RoleTo_cat") <> "" Then 
            
        If Request("chkJustSendForLeader")&"" = "" Then

			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT fc.memberid " &_
                        " FROM	Fan_Category fc" &_
                        " INNER JOIN regmem reg ON fc.MemberID = reg.memberid" &_
                        " INNER JOIN registration r ON r.memberid = fc.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1 AND fc.[Status]=2" &_
                        " AND (" &_
	                    "    1=0"
	        For Each x In Split(Request.Form("RoleTo_cat"),",")
	            tmp = Split(Trim(x),":")	            
                sql = sql & " OR (CatID = " & CLng(tmp(0)) & " AND ISNULL(SubCatID,0) = " & CLng(tmp(1)) & ")	"
            Next
            sql = sql & " ) )"
        Else ' just send for leader
            sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT groupleader " &_
                        " FROM vwCategory fc" &_
                        " INNER JOIN regmem reg ON fc.groupleader = reg.memberid" &_
                        " INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE ISNULL(fc.groupleader,'') <> '' AND r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1 " &_
                        " AND (" &_
	                    "    1=0"
	        For Each x In Split(Request.Form("RoleTo_cat"),",")
	            tmp = Split(Trim(x),":")	            
                sql = sql & " OR (fc.CatID = " & CLng(tmp(0)) & " AND ISNULL(fc.SubCatID,0) = " & CLng(tmp(1)) & ")	"
            Next
            sql = sql & " ) )"
        End If
	End If
		
	If Request.Form("RoleTo_interest") <> "" Then 
		sql = sql & " OR r.MemberId IN (" & _
					" SELECT DISTINCT fi.memberid " &_
                    " FROM	Fan_Interest fi" &_
                    " INNER JOIN regmem reg ON fi.MemberID = reg.memberid" &_
                    " INNER JOIN registration r ON r.memberid = fi.memberid" &_
                    " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                    " AND (" &_
	                "    1=0"
	    For Each x In Split(Request.Form("RoleTo_interest"),",")
	        tmp = Split(Trim(x),":")	            
            sql = sql & " OR (interestCatId = " & CLng(tmp(0)) & " AND ISNULL(interestSubCatID,0) = " & CLng(tmp(1)) & ")	"
        Next
        sql = sql & " )) "
	End If
		
	If Request("RoleTo_AgeGroup") <> "" Then 
		sql = sql & " OR r.MemberId IN (" & _
					" SELECT DISTINCT reg.memberid " &_
                    " FROM	regmem reg " &_
					" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                    " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                    " AND agesno IN(" & CLng(Request("RoleTo_AgeGroup")) & ") )"
	End If
		
	If Request("RoleTo_Gender") <> "" And Request("RoleTo_Gender") <> "a" Then 
		sql = sql & " OR r.MemberId IN (" & _
					" SELECT DISTINCT reg.memberid " &_
                    " FROM	regmem reg " &_
					" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                    " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                    " AND gender = @gender )"
	End If
	'>>Request("RoleTo_Gender")
		
	If Request("RoleTo_Country") <> "" Then 
		If Request("Country") = "include" Then
			sql = sql & " OR r.MemberId IN (" & _
					" SELECT DISTINCT reg.memberid" &_
                    " FROM	regmem reg " &_
					" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                    " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                    " AND country IN (SELECT Col FROM dbo.ToTable(@country,','))) "
					'>>Replace(Request("RoleTo_Country"), ", ", "', '")
		Else
			sql = sql & " OR r.MemberId IN (" & _
					" SELECT DISTINCT reg.memberid " &_
                    " FROM	regmem reg " &_
					" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                    " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                    " AND country NOT IN (SELECT Col FROM dbo.ToTable(@country,','))) "
		End If
	End If
		
	'If Request("RoleTo_Country") = "" Or Request("Country") <> "exclude" Then 
		sql = sql & ")"
	'End If
		
	If Request.Form("hidAdminRoleTo") <> "" Then 
		sql = sql & " UNION SELECT 'D37BC41E' code, '' AS validationkey, fname,lname,superuser AS memberid,Email,'y', 1, '' FROM admin " &_
					" WHERE AdminType in (" + CLngList(Replace(Request.Form("hidAdminRoleTo"),";",",")) + ")" 		
	End If
		
	If Request.Form("chkSubscriberRoleTo") = "y" Then 
		If Request("rdLanguage") = "fr" or Request("rdLanguage") = "all" Then
			sql = sql & " UNION SELECT 'D37BC41E' code, '' AS validationkey,fname,lname, '' AS memberid, Email, 'y', 1, 'farsi' FROM Leave_contract"
		Else 
			sql = sql & " UNION SELECT 'D37BC41E' code,'' AS validationkey,fname,lname, '' AS memberid, Email, 'y', 1, 'eng' FROM Leave_contract"
		End If
	End If
		
set oDBEmail = new ADOHelper
oDBEmail.ConnString = conn		
sParams = array( _						  
	array("@email", adLongVarWchar, adParamInput, -1, (Replace(Request.Form("txtTo"),";",","))), _
	array("@gender", adVarWChar, adParamInput, 3, Request("RoleTo_Gender")),_
	array("@country", adVarWChar, adParamInput, 50,Request("RoleTo_Country")))
oDB.ClearParams()	
Set rsEmail = oDBEmail.RunSQLReturnRSAdvanced(sql, sParams)	
	
	
		
	sSentList = ""
	If Not rsEmail.EOF Then
		While Not rsEmail.EOF
				
			isshowname = (rsEmail("paypal_approved") & "" = "y" Or rsEmail("paypal_approved") & "" = "a")
			leadername = ShowMemberName(isshowname,rsEmail("fname"),rsEmail("lname"),rsEmail("memberid"),"")
						        
			sMemFirstName = rsEmail("fname")
			sMemLastName = rsEmail("lname")
			smeberId = leadername
			sEmail = rsEmail("Email")
			token = rsEmail("validationkey")
			memberCode = DecryptStrNew(rsEmail("code"))
				
			sBody = ""
			sSubject = ""		

			If Request.Form("rdLanguage") = "fr" Then
				If rsEmail("registeredfrom") = "farsi" Then 
					sBody = "<div dir='rtl'>" & Request.Form("txtBody_fr") & "</div>"
					sSubject = Request.Form("txtSubject_fr")
				End If
			ElseIf Request.Form("rdLanguage") = "en" Then
				If rsEmail("registeredfrom")&"" <> "farsi" Then 
					sBody = "<div>" & Request.Form("txtBody") & "</div>"
					sSubject = Request.Form("txtSubject")
				End If
			Else
				If rsEmail("registeredfrom") = "farsi" Then 
					sBody = "<div dir='rtl'>" & Request.Form("txtBody_fr") & "</div>"
					sSubject = Request.Form("txtSubject_fr")
				Else
					sBody = "<div>" & Request.Form("txtBody") & "</div>"
					sSubject = Request.Form("txtSubject")
				End If
			End If
				
			If sSubject <> "" And sBody <> "" Then
				Call SendAddHoc(Request.Form("txtFrom"), sEmail,sEmailCC, sEmailBCC, sBody, sSubject, sMemFirstName, sMemLastName, smeberId, sAttachments,token,rsEmail("registeredfrom"),memberCode)
			End If
				
			sSentList = IIf(sSentList = "", sEmail, sSentList & ";" & sEmail)				
				
			rsEmail.moveNext
		Wend
	End If
	Set rsEmail = Nothing
		
	'Send to Emails that not are Members
	If sList <> "" Then
		arrEmails = Split(sList, ";")
		sSentList = IIf(sSentList = "", "", ";" & sSentList & ";")
			
		If Request.Form("rdLanguage") = "fr" Then
			sBody = "<div dir='rtl'>" & Request.Form("txtBody_fr") & "</div>"
			sSubject = Request.Form("txtSubject_fr")
		ElseIf Request.Form("rdLanguage") = "en" Then
			sBody = "<div>" & Request.Form("txtBody") & "</div>"
			sSubject = Request.Form("txtSubject")
		Else
			sBody = "<div>" & Request.Form("txtBody_fr") & "</div>"
			sSubject = Request.Form("txtSubject_fr")
		End If
		sFrom = Request.Form("txtFrom")
			
		For idx = 0 To UBound(arrEmails)
			If InStr(sSentList, ";" & arrEmails(idx) & ";") <= 0 And Trim(arrEmails(idx)) <> "" Then
				sMemFirstName = ""
				sMemLastName = ""
				smeberId = ""
				sEmail = Trim(arrEmails(idx))

				Call SendAddHoc(sFrom, sEmail,sEmailCC, sEmailBCC, sBody, sSubject,sMemFirstName, sMemLastName, smeberId, sAttachments,"",IIf(Request.Form("rdLanguage") = "en","en","farsi"),"")
					
				sSentList = IIf(sSentList = "", sEmail, sSentList & sEmail & ";")
					
			End If
		Next
	End If

End Function

Function SaveDraft(dMailType)
	
	sToRole = Request.Form("hidRoleTo")&""
	If Request.Form("hidAdminRoleTo")&"" <> "" And sToRole <> "" Then
		sToRole = sToRole & ";" & Request.Form("hidAdminRoleTo")
	ElseIf Request.Form("hidAdminRoleTo")&"" <> "" Then
		sToRole = Request.Form("hidAdminRoleTo")
	End If
	
	sCCRole = Request.Form("hidRoleCC")&""
	If Request.Form("hidAdminRoleCC")&"" <> "" And sCCRole <> "" Then
		sCCRole = sCCRole & ";" & Request.Form("hidAdminRoleCC")
	ElseIf Request.Form("hidAdminRoleCC")&"" <> "" Then
		sCCRole = Request.Form("hidAdminRoleCC")
	End If
	
	sBCCRole = Request.Form("hidRoleBCC")&""
	If Request.Form("hidAdminRoleBCC")&"" <> "" And sBCCRole <> "" Then
		sBCCRole = sBCCRole & ";" & Request.Form("hidAdminRoleBCC")
	ElseIf Request.Form("hidAdminRoleBCC")&"" <> "" Then
		sBCCRole = Request.Form("hidAdminRoleBCC")
	End If
	
	sCountryType = Request("Country")
	If sCountryType = "include" Then
		sIncCountries = Request("RoleTo_Country")
		sExcCountries = ""
	Else
		sIncCountries = ""
		sExcCountries = Request("RoleTo_Country")
	End If
	
	If Request("chkSubscriberRoleTo") = "y" Then
		bToSubscribers = True
	Else
		bToSubscribers = False
	End If

    bSendOnlyLeader = False
    If Request("chkJustSendForLeader")&"" = "1" Then
		bSendOnlyLeader = True
	Else
		bSendOnlyLeader = False
	End If
	
	set oDBDC = new ADOHelper
	oDBDC.ConnString = conn

	If ID = "" Then
	    ' type = 3, pending status
   
        if(dMailType&"" ="1") Then
            dMailType = "3"
        End IF  
		sSQL = "usp_AdhocEmail_Autosave_Add"
		sParams = array( _
			array("@RETURN_VALUE", adInteger, adParamReturnValue, 0, RETURN_VALUE), _
			array("@From", adVarchar, adParamInput, 100, EmptyToNull(Request.Form("txtFrom"))), _
			array("@To", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtTo"))), _
			array("@ToRoles", adVarchar, adParamInput, 200, EmptyToNull(sToRole)), _
			array("@CC", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtCC"))), _
			array("@CCRoles", adVarchar, adParamInput, 100, EmptyToNull(sCCRole)), _
			array("@BCC", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtBCC"))), _
			array("@BCCRoles", adVarchar, adParamInput, 200, EmptyToNull(sBCCRole)), _
			array("@Subject", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtSubject"))), _
			array("@Subject_fr", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtSubject_fr"))), _
			array("@Body", adLongVarWChar, adParamInput, -1, EmptyToNull(Request.Form("txtBody"))), _
			array("@Body_fr", adLongVarWChar, adParamInput, -1, EmptyToNull(Request.Form("txtBody_fr"))), _
			array("@Attachments", adVarWchar, adParamInput, 2000, EmptyToNull(sAttachments)), _
			array("@SavedBy", adVarWchar, adParamInput, 50, EmptyToNull(uAdminId)), _
			array("@SavedTime", adDBTimeStamp, adParamInput, 0, now()),_			
			array("@ToCat", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleTo_cat"))), _
			array("@CCCat", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleCC_cat"))), _
			array("@BCCCat", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleBCC_cat"))), _			
			array("@ToInterest", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleTo_interest"))), _
			array("@CCInterest", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleCC_interest"))), _
			array("@BCCInterest", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleBCC_interest"))), _
			array("@Type", adInteger, adParamInput, 0, EmptyToNull(dMailType)),_
			array("@IncCountries", adLongVarWChar, adParamInput, -1, EmptyToNull(sIncCountries)), _
			array("@ExcCountries", adLongVarWChar, adParamInput, -1, EmptyToNull(sExcCountries)), _
			array("@AgeGroups", adVarchar, adParamInput, 1000, EmptyToNull(Request.Form("RoleTo_AgeGroup"))), _
			array("@Genders", adVarchar, adParamInput, 1, EmptyToNull(Request.Form("RoleTo_Gender"))), _
			array("@Language", adVarchar, adParamInput, 3, EmptyToNull(Request.Form("rdLanguage"))), _
			array("@ToSubscribers", adBoolean, adParamInput, 0, EmptyToNull(bToSubscribers)),_
			array("@CntTo", adInteger, adParamInput, 0, EmptyToNull(Request.Form("CntTo"))),_
            array("@SendOnlyLeader", adBoolean, adParamInput, 0, bSendOnlyLeader)_
			)
			
		oDBDC.ClearParams() 
		Set rs = oDBDC.RunSPReturnRS(sSQL, sParams, OutArray)
		If Not rs.EOF Then
			SaveDraft = rs(0)
		Else
			SaveDraft = 0
		End If
		rs.Close()
		Set rs = Nothing
	Else
		sSQL = "usp_AdhocEmail_Autosave_Update"
		sParams = array( _
			array("@RETURN_VALUE", adInteger, adParamReturnValue, 0, RETURN_VALUE), _
			array("@ID", adInteger, adParamInput, 0, EmptyToNull(ID)), _
			array("@From", adVarchar, adParamInput, 100, EmptyToNull(Request.Form("txtFrom"))), _
			array("@To", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtTo"))), _
			array("@ToRoles", adVarchar, adParamInput, 200, EmptyToNull(sToRole)), _
			array("@CC", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtCC"))), _
			array("@CCRoles", adVarchar, adParamInput, 100, EmptyToNull(sCCRole)), _
			array("@BCC", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtBCC"))), _
			array("@BCCRoles", adVarchar, adParamInput, 200, EmptyToNull(sBCCRole)), _
			array("@Subject", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtSubject"))), _
			array("@Subject_fr", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("txtSubject_fr"))), _
			array("@Body", adLongVarWChar, adParamInput, -1, EmptyToNull(Request.Form("txtBody"))), _
			array("@Body_fr", adLongVarWChar, adParamInput, -1, EmptyToNull(Request.Form("txtBody_fr"))), _
			array("@Attachments", adVarWchar, adParamInput, 2000, EmptyToNull(sAttachments)), _
			array("@SavedBy", adVarWchar, adParamInput, 50, EmptyToNull(uAdminId)), _
			array("@SavedTime", adDBTimeStamp, adParamInput, 0, now()),_
			array("@ToCat", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleTo_cat"))), _
			array("@CCCat", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleCC_cat"))), _
			array("@BCCCat", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleBCC_cat"))), _			
			array("@ToInterest", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleTo_interest"))), _
			array("@CCInterest", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleCC_interest"))), _
			array("@BCCInterest", adVarWchar, adParamInput, 4000, EmptyToNull(Request.Form("RoleBCC_interest"))), _
			array("@Type", adInteger, adParamInput, 0, EmptyToNull(dMailType)),_
			array("@IncCountries", adLongVarWChar, adParamInput, -1, EmptyToNull(sIncCountries)), _
			array("@ExcCountries", adLongVarWChar, adParamInput, -1, EmptyToNull(sExcCountries)), _
			array("@AgeGroups", adVarchar, adParamInput, 1000, EmptyToNull(Request.Form("RoleTo_AgeGroup"))), _
			array("@Genders", adVarchar, adParamInput, 1, EmptyToNull(Request.Form("RoleTo_Gender"))), _
			array("@Language", adVarchar, adParamInput, 3, EmptyToNull(Request.Form("rdLanguage"))), _
			array("@ToSubscribers", adBoolean, adParamInput, 0, EmptyToNull(bToSubscribers)), _
            array("@SendOnlyLeader", adBoolean, adParamInput, 0, bSendOnlyLeader)_
			)
		
		oDBDC.ClearParams() 
		Set rs = oDBDC.RunSPReturnRS(sSQL, sParams, OutArray)
		If Not rs.EOF Then
			SaveDraft = rs(0)
		Else
			SaveDraft = 0
		End If
		rs.Close()
		Set rs = Nothing
		
	End If
	
	
End Function

Function GetEmails(sEmailName)
	Dim sList: sList = Request.Form("txt" & sEmailName)
	Dim sEmailBCC: sEmailBCC = ""
	sList = RemoveDisabledEmails(sList&"")
	
	If sList <> "" OR Request("hidRole" & sEmailName) <> "" OR Request("hidAdminRole" & sEmailName) <> "" _ 
	    OR Request("Role" & sEmailName & "_cat") <> "" OR Request("Role" & sEmailName & "_interest") <> "" _
		OR Request("Role" & sEmailName & "_Country") <> "" OR Request("Role" & sEmailName & "_AgeGroup") <> "" _
		OR Request("Role" & sEmailName & "_Gender") <> "" Then
		sql = " SELECT fname,lname,r.memberid,Email FROM regmem r"_
			& " INNER JOIN registration on registration.memberid = r.memberid " _
			& " WHERE  registration.confirmed <> 'x' And r.send_me_info = 1 And r.newsletter = 1 And ISNULL(r.Email, '') <> ''" _
			& " AND (1 = 0 "
		If sList <> "" Then
		    sql = sql & " OR email in ('" + (Replace(sList,";","','")) + "') "	
		End If
		
		If Request.Form("hidRole" & sEmailName) <> "" Then
			sql = sql & " OR Role_ID in (" + CLngList(Replace(Request.Form("hidRole" & sEmailName),";",",")) + ")" 
		End If	
				
		If Request.Form("Role" & sEmailName & "_cat") <> "" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT fc.memberid " &_
                        " FROM	Fan_Category fc" &_
                        " INNER JOIN regmem reg ON fc.MemberID = reg.memberid" &_
                        " INNER JOIN registration r ON r.memberid = fc.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1 AND fc.[Status]=2" &_
                        " AND (" &_
	                    "    1=0"
	        For Each x In Split(Request.Form("Role" & sEmailName & "_cat"),",")
	            tmp = Split(Trim(x),":")	            
                sql = sql & " OR (CatID = " & CLng(tmp(0)) & " AND ISNULL(SubCatID,0) = " & CLng(tmp(1)) & ")	"
            Next
            sql = sql & " ) )"
		End If
		
		If Request.Form("Role" & sEmailName & "_interest") <> "" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT fi.memberid " &_
                        " FROM	Fan_Interest fi" &_
                        " INNER JOIN regmem reg ON fi.MemberID = reg.memberid" &_
                        " INNER JOIN registration r ON r.memberid = fi.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND (" &_
	                    "    1=0"
	        For Each x In Split(Request.Form("Role" & sEmailName & "_interest"),",")
	            tmp = Split(Trim(x),":")	            
                sql = sql & " OR (interestCatId = " & CLng(tmp(0)) & " AND ISNULL(interestSubCatID,0) = " & CLng(tmp(1)) & ")	"
            Next
            sql = sql & " )) "
		End If
		
		If Request("Role" & sEmailName & "_AgeGroup") <> "" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid " &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND agesno IN(" & CLngList(Request("Role" & sEmailName & "_AgeGroup")) & ") )"
		End If
		
		If Request("Role" & sEmailName & "_Gender") <> "" And Request("Role" & sEmailName & "_Gender") <> "a" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid " &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND gender = @gender )"
						'>>Request("Role" & sEmailName & "_Gender") 
		End If
		
		If Request("Role" & sEmailName & "_Country") <> "" Then 
			If Request("Country") = "include" Then
				sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid" &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND country IN (SELECT col FROM dbo.ToTable(@country,','))) "
						'>>Replace(Request("Role" & sEmailName & "_Country"), ", ", "', '")
			Else
				sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid " &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND country NOT IN (SELECT col FROM dbo.ToTable(@country,','))) "
			End If
		End If
		
		'If Request("Role" & sEmailName & "_Country") = "" Or Request("Country") <> "exclude" Then 
			sql = sql & ")"
		'End If
		
		If Request.Form("hidAdminRole" & sEmailName) <> "" Then 
			sql = sql & " UNION SELECT fname,lname,superuser AS memberid,Email,'y',1 FROM admin " &_
						" WHERE AdminType in (" + CLngList(Replace(Request.Form("hidAdminRole" & sEmailName),";",",")) + ")" 		
		End If
		
		sParams = array( _						  
		   array("@gender", adVarWChar, adParamInput, 50, Request("Role" & sEmailName & "_Gender")), _
		   array("@country", adLongVarWChar, adParamInput, -1, Request("Role" & sEmailName & "_Country")))
		oDB.ClearParams()	
		Set rsEmail = oDB.RunSQLReturnRSAdvanced(sql, sParams)
		
		If Not rsEmail.EOF Then
			While Not rsEmail.EOF
			    If len(rsEmail("Email")&"") > 0 and InStr(sList,rsEmail("Email")) <= 0 Then
					If sList = "" Then 
						sList = rsEmail("Email")
					else
						sList = sList & ";" & rsEmail("Email")
					End If
				End If 	
				rsEmail.moveNext	
			Wend
		End If
		Set rsEmail = Nothing			
	End If

	GetEmails = sList 
	
End Function

Function GetCountEmails(sEmailName,dReturnType)
	Dim sList: sList = Request("txt" & sEmailName)
	Dim sEmailBCC: sEmailBCC = ""	
	Dim re : re = 0
	Dim sEmailList : sEmailList = ""
	sList = RemoveDisabledEmails(sList&"")
	
	If sList <> "" OR Request("hidRole" & sEmailName) <> "" OR Request("hidAdminRole" & sEmailName) <> "" _ 
	    OR Request("Role" & sEmailName & "_cat") <> "" OR Request("Role" & sEmailName & "_interest") <> "" _
		OR Request("Role" & sEmailName & "_Country") <> "" OR Request("Role" & sEmailName & "_AgeGroup") <> "" _
		OR Request("Role" & sEmailName & "_Gender") <> "" Then
		
		sql = " SELECT fname,lname,r.memberid,Email, registeredfrom FROM regmem r"_
			& " INNER JOIN registration on registration.memberid = r.memberid " _
			& " WHERE  registration.confirmed <> 'x' And r.send_me_info = 1 And r.newsletter = 1 And ISNULL(r.Email, '') <> ''" _
			& " AND (1 = 0 "
		If sList <> "" Then
		    sql = sql & " OR email in (SELECT col FROM dbo.ToTable(@email,',')) "
		End If
		
		If Request.Form("hidRole" & sEmailName) <> "" Then
			sql = sql & " OR Role_ID in (" + CLNGList(Replace(Request.Form("hidRole" & sEmailName),";",",")) + ")" 
		End If	
				
		If Request.Form("Role" & sEmailName & "_cat") <> "" Then 
            
            If Request("chkJustSendForLeader")&"" = "" Then

			    sql = sql & " OR r.MemberId IN (" & _
						    " SELECT DISTINCT fc.memberid " &_
                            " FROM	Fan_Category fc" &_
                            " INNER JOIN regmem reg ON fc.MemberID = reg.memberid" &_
                            " INNER JOIN registration r ON r.memberid = fc.memberid" &_
                            " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1 AND fc.[Status]=2" &_
                            " AND (" &_
	                        "    1=0"
	            For Each x In Split(Request.Form("Role" & sEmailName & "_cat"),",")
	                tmp = Split(Trim(x),":")	            
                    sql = sql & " OR (CatID = " & CLng(tmp(0)) & " AND ISNULL(SubCatID,0) = " & CLng(tmp(1)) & ")	"
                Next
                sql = sql & " ) )"
            
            Else ' just send for leader

                sql = sql & " OR r.MemberId IN (" & _
						    " SELECT DISTINCT groupleader " &_
                            " FROM vwCategory fc" &_
                            " INNER JOIN regmem reg ON fc.groupleader = reg.memberid" &_
                            " INNER JOIN registration r ON r.memberid = reg.memberid" &_
                            " WHERE ISNULL(fc.groupleader,'') <> '' AND r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1 " &_
                            " AND (" &_
	                        "    1=0"
	            For Each x In Split(Request.Form("Role" & sEmailName & "_cat"),",")
	                tmp = Split(Trim(x),":")	            
                    sql = sql & " OR (fc.CatID = " & CLng(tmp(0)) & " AND ISNULL(fc.SubCatID,0) = " & CLng(tmp(1)) & ")	"
                Next
                sql = sql & " ) )"

            End If

		End If
		
		If Request.Form("Role" & sEmailName & "_interest") <> "" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT fi.memberid " &_
                        " FROM	Fan_Interest fi" &_
                        " INNER JOIN regmem reg ON fi.MemberID = reg.memberid" &_
                        " INNER JOIN registration r ON r.memberid = fi.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND (" &_
	                    "    1=0"
	        For Each x In Split(Request.Form("Role" & sEmailName & "_interest"),",")
	            tmp = Split(Trim(x),":")	            
                sql = sql & " OR (interestCatId = " & CLng(tmp(0)) & " AND ISNULL(interestSubCatID,0) = " & CLng(tmp(1)) & ")	"
            Next
            sql = sql & " )) "
		End If
		
		If Request("Role" & sEmailName & "_AgeGroup") <> "" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid " &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND agesno IN(" & ClngList(Request("Role" & sEmailName & "_AgeGroup")) & ") )"
		End If
		
		If Request("Role" & sEmailName & "_Gender") <> "" And Request("Role" & sEmailName & "_Gender") <> "a" Then 
			sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid " &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND gender = @gender )"
		End If
		
		If Request("Role" & sEmailName & "_Country") <> "" Then 
			If Request("Country") = "include" Then
				sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid" &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND country IN (SELECT Col FROM dbo.ToTable(@country,','))) "
			Else
				sql = sql & " OR r.MemberId IN (" & _
						" SELECT DISTINCT reg.memberid " &_
                        " FROM	regmem reg " &_
						" INNER JOIN registration r ON r.memberid = reg.memberid" &_
                        " WHERE r.confirmed <> 'x' AND reg.send_me_info = 1 AND reg.newsletter = 1" &_
                        " AND country NOT IN (SELECT Col FROM dbo.ToTable(@country,','))) "
			End If
		End If
		'If Request("Role" & sEmailName & "_Country") = "" Or Request("Country") <> "exclude" Then 
			sql = sql & ")"
		'End If
		
		If Request.Form("hidAdminRole" & sEmailName) <> "" Then 
			sql = sql & " UNION SELECT fname,lname,superuser AS memberid,Email, 1 FROM admin " &_
						" WHERE AdminType in (" + CLngList(Replace(Request.Form("hidAdminRole" & sEmailName),";",",")) + ")" 		
		End If
		
		If Request.Form("chkSubscriberRole"& sEmailName) = "y" Then 
			If Request("rdLanguage") = "fr"  or Request("rdLanguage") = "all" Then
				sql = sql & " UNION SELECT fname,lname, '' AS memberid, Email, 'farsi' FROM Leave_contract"
			Else
				sql = sql & " UNION SELECT fname,lname, '' AS memberid, Email, 'eng' FROM Leave_contract"
			End If
		End If
		
		sParams = array( _						  
		   array("@email", adLongVarWchar, adParamInput, -1, Replace(sList,";",",")), _
		   array("@gender", adVarWChar, adParamInput, 50, Request("Role" & sEmailName & "_Gender")),_
		   array("@country", adLongVarWchar, adParamInput, -1, Request("Role" & sEmailName & "_Country")))
		oDB.ClearParams()	
		Set rsEmail = oDB.RunSQLReturnRSAdvanced(sql, sParams)		
		'------------		
		re = 0				
		If Not rsEmail.EOF Then
			While Not rsEmail.EOF
			    If len(Trim(rsEmail("Email")&"")) > 0 Then
					If Request("rdLanguage") = "all" Or (Request("rdLanguage") = "fr" And rsEmail("registeredfrom") = "farsi") Or (Request("rdLanguage") = "en" And rsEmail("registeredfrom") <> "farsi") Then
						re = re + 1
						sEmailList = IIf(sEmailList = "", Trim(rsEmail("Email")&""), sEmailList & ";" & Trim(rsEmail("Email")&""))
					ElseIf sList <> "" Then
						sList = Replace(";" & sList & ";", ";" & Trim(rsEmail("Email")&"") & ";", "")
					End If
				End If 	
				rsEmail.moveNext	
			Wend
		End If
		Set rsEmail = Nothing		
		
		'Send to Emails that not are Members
		If sList <> "" Then
			arrEmails = Split(sList, ";")
			sEmailList = IIf(sEmailList = "", "", ";" & sEmailList & ";")
			
			For idx = 0 To UBound(arrEmails)
				If InStr(sEmailList, ";" & arrEmails(idx) & ";") <= 0 And Trim(arrEmails(idx)) <> "" Then
					sEmailList = IIf(sEmailList = "", ";" & arrEmails(idx) & ";", sEmailList & arrEmails(idx) & ";")
					re = re + 1
				End If
			Next
		Else
			sEmailList = IIf(sEmailList = "", "", ";" & sEmailList & ";")
		End If
	End If
    
    If dReturnType = "count" Then
        GetCountEmails = re       
    Else
		If sEmailList = "" Then
			GetCountEmails = ""
		Else
			GetCountEmails = Mid(sEmailList, 2, Len(sEmailList) - 2)
		End If
    End If       
End Function

Function RemoveDisabledEmails(ByVal EmailList)
	If Len(EmailList & "") > 0 Then	

		sql = " SELECT Email FROM regmem "_
			  &" INNER JOIN registration on registration.memberid = regmem.memberid "_
			  &"WHERE (registration.confirmed = 'x' Or ISNULL(regmem.send_me_info,0) <> 1 Or ISNULL(regmem.newsletter,0) <> 1 ) AND email in (SELECT Col FROM dbo.ToTable(@email,','))  " 
		sParams = array( _						  
		   array("@email", adLongVarWChar, adParamInput, -1, Replace(EmailList,";",",")))
		oDB.ClearParams()	
		Set rsEmail = oDB.RunSQLReturnRSAdvanced(sql, sParams)
	
		If Not rsEmail.EOF Then
			While Not rsEmail.EOF
				If rsEmail("Email")&"" <> "" Then 
					EmailList = Replace(EmailList&"",";"&rsEmail("Email")&";",";")
					EmailList = Replace(EmailList&"",";"&rsEmail("Email"),"")
					EmailList = Replace(EmailList&"",rsEmail("Email")&";","")
					EmailList = Replace(EmailList&"",rsEmail("Email"),"")
				End If
				rsEmail.moveNext	
			Wend
		End If
		Set rsEmail = Nothing			
	End If

	RemoveDisabledEmails = EmailList 
	
End Function

sub SendAddHoc(ByVal emailfrom, toEmail,dcc, dbcc,ByVal email_text,subject,dMemFirstName, dMemLastName, dmeberId, attachments, dtoken,dLanguage,ByVal pMemberCode)
        Dim strFirstName, resetLink
        Dim count
        pMemberCode = pMemberCode & ""
        strFirstName = ""
		resetLink = ""
        count = 0
        
        If dMemFirstName <> "" Then
            strFirstName = dMemFirstName
        End If
                
        If dMemFirstName & dMemLastName & "" = "" Then
            strFirstName = dmeberId 
        End If
		
		If dtoken <> "" Then
			resetLink = "<a href='" & gSiteAdrs & "/resetpw.asp?token=" & dtoken & "&lan=" & dLanguage & "'>" & gSiteAdrs & "/resetpw.asp?token=" & dtoken & "&lan=" & dLanguage & "</a>"
		End If

		' %sitename%: Site Name
		' %sitelink%: Site link
		' %UserName%: Username
		' %MemFirstName%: First Name
		' %MemLastName%: Last Name
		' %Email%: Email 
		' %password reset link%
		' %member-code% - _memberCode
        
		sql = "UPDATE registration SET resetPassword = 0 where memberid = ?"
		set oDBAdhoc = new ADOHelper
		oDBAdhoc.ConnString = conn
		oDBAdhoc.ClearParams()
		sParams = array( _
		   array("@memberid", adVarWChar, adParamInput, 50,dmeberId))	
		Call oDBAdhoc.RunSQL(sql, sParams)		
		oDBAdhoc.ClearParams()
		
		dim arr_fun(17,0)
	
		arr_fun(0,0)="UserName"
		arr_fun(1,0)= dmeberId
		arr_fun(2,0)="MemFirstName"
		arr_fun(3,0)= strFirstName
		arr_fun(4,0)="MemLastName"
		arr_fun(5,0)= dMemLastName
		arr_fun(6,0)="sitename"
		arr_fun(7,0)= gSiteName
		arr_fun(8,0)= "sitelink"
		arr_fun(9,0)= gSiteAdrs
		arr_fun(10,0)= "Email"
		arr_fun(11,0)= toEmail
		
		arr_fun(12,0)= "password reset link"
		arr_fun(13,0)= resetLink
		
		arr_fun(14,0)= "member-code"
		arr_fun(15,0)= pMemberCode		
		
		arr_fun(16,0)= "Text_YourCode"
		arr_fun(17,0)= GetLanguageByKeyAndPage("Text_YourCode",gPageName, IIf(dLanguage="farsi","Value_Fr","Value")) 
		If pMemberCode = "" Then
			arr_fun(17,0) = ""
		End if
		
		
		row = 17
		
		if 	isnull(email_text) or trim(email_text)="" then
		else
	 		for mk = 0 to row step 2'ubound(arr_fun) step 2
				if trim(arr_fun(mk+1,count))="" then
					val1=" "
				else
					val1=arr_fun(mk+1,count)
				end if
					
				'if trim(val1)<>"" then email_text = replace(email_text,"%"&arr_fun(mk,count)&"%",val1)
				email_text = replace(email_text,"%"&arr_fun(mk,count)&"%",val1 & "")
			next
		
			if trim(subject&"") <> "" then
				for mk = 0 to row step 2'ubound(arr_fun) step 2
					if trim(arr_fun(mk+1,count))="" then
						val1=" "
					else
						val1=arr_fun(mk+1,count)
					end if
						
					'if trim(val1)<>"" then email_text = replace(email_text,"%"&arr_fun(mk,count)&"%",val1)
					subject = replace(subject,"%"&arr_fun(mk,count)&"%",val1 & "")
				next
			 End if 	
			 
			if uOtherEmailText<>"" then email_text= email_text & "<br>" & uOtherEmailText
			
			newemailfrom = Replace(emailfrom,"%member-code%",pMemberCode)		
			
			If gUseMailQueue Then
			    Call AddMailQueue2(newemailfrom,toEmail,dcc,dbcc,Subject,email_text,attachments)
			Else
			    On Error Resume Next
			    Set mymail = CreateObject("CDO.Message")
			    mymail.BodyPart.CharSet = "utf-8"
			    mymail.Subject = Subject
			    mymail.From = newemailfrom
			    mymail.To = toEmail
			    if trim(dbcc)<>"" then		
				    mymail.bcc = dbcc 
			    end if	
			    if trim(dcc)<>"" then		
				    mymail.cc = dcc 
			    end if
    			
			    ' Response.Write("<br/> emailfrom" & emailfrom)
			    ' Response.Write("<br/> toEmail" & toEmail)
			    ' Response.Write("<br/> mymail.bcc" & mymail.bcc)
			    ' Response.Write("<br/> email_text" & email_text)
			    ' Response.Write("<br/> mymail.cc" & mymail.cc)
    			
			    
			    'mymail.TextBody = email_text
			    mymail.HTMLBody = email_text
    			
			    '--This section provides the configuration information for the remote SMTP server.
			    '--Normally you will only change the server name or IP.
			    mymail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/sendusing") = 2

			    'Name or IP of Remote SMTP Server
			    mymail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserver") = "mail.makeastar.net"

			    'Server port (typically 25)
			    mymail.Configuration.Fields.Item("http://schemas.microsoft.com/cdo/configuration/smtpserverport") = 25
    			
			    'Your UserID on the SMTP server
			    mymail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate") = 1 			
			    mymail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendusername") ="registration@makeastar.com"
			    mymail.Configuration.Fields.Item ("http://schemas.microsoft.com/cdo/configuration/sendpassword") ="regreg!"

			    mymail.Configuration.Fields.Update
			    '--End remote SMTP server configuration section==
    			
			    mymail.Send
    			
			    Set mymail = Nothing
			    On Error Goto 0
			End If	
		END IF				
	
end sub	
				
function form_action_href()
	form_action_href = fans_href(MainParams(), form_params())
end function
					
'**********************************************************
function SearchRequest()
	SearchRequest = ""
end function
'**********************************************************

sub DrawEmail()
	If ID <> "" Then
		
		sql = "SELECT * FROM AdhocEmail_Autosave WHERE Id = ? And SavedBy = ?"
		set oDBEmail = new ADOHelper
		oDBEmail.ConnString = conn		
		sParams = array( _						  
			array("@id", adInteger, adParamInput, 0, ID),_
			array("@SavedBy", adVarWChar, adParamInput, 50, uAdminId))
		oDBEmail.ClearParams()	
		Set rsEmail = oDBEmail.RunSQLReturnRSAdvanced(sql, sParams)	
	
		If Not rsEmail.EOF then									
			sFrom = rsEmail("from")&""
			sTo = rsEmail("to")&""
			sToRoles = rsEmail("toroles")&""
			sCC = rsEmail("cc")&""
			sCCRoles = rsEmail("CCroles")&""
			sBCC = rsEmail("bcc")&""
			sBCCRoles = rsEmail("BCCroles")&""
			sBody = rsEmail("body")&""
			sSubject = Server.HtmlEncode(rsEmail("subject")&"")
			sBody_fr = rsEmail("body_fr")&""
			sSubject_fr = Server.HtmlEncode(rsEmail("subject_fr")&"")
			sAttachments = rsEmail("attachments")&""
			sToCat       =   rsEmail("ToCat")&""
			sCCCat       =   rsEmail("CCCat")&""
			sBCCCat      =   rsEmail("BCCCat")&""
			sToInterest  =   rsEmail("ToInterest")&""
			sCCInterest  =   rsEmail("CCInterest")&""
			sBCCInterest =   rsEmail("BCCInterest")&""
			sCountries	 = 	 rsEmail("IncCountries") & rsEmail("ExcCountries") & ""
			If rsEmail("ExcCountries") & "" <> "" Then 				
				sIsIncludeCountry = False
			Else
				sIsIncludeCountry = True
			End If
			sAgeGroups	 = 	 rsEmail("AgeGroups")&""
			sGenders	 = 	 rsEmail("Genders")&""
			sLanguage	 =   rsEmail("Language")&""
			sToSubscribers = rsEmail("ToSubscribers")
            bSendOnlyLeader = rsEmail("SendOnlyLeader")

			'If sLanguage = "" Then sLanguage = "all"
		End If
		Set rsEmail = Nothing	
	Else
		sql = "SELECT * FROM email_title et, common_email em " &_
			  "WHERE et.email_title_id = em.email_title_id AND for_whom = 1 and title_text = N'Add Hoc'"
		Set rsEmail = con.execute(sql)
		If Not rsEmail.EOF then									
			sBody = rsEmail("email_text")
			sSubject = Server.HtmlEncode(rsEmail("email_subject"))
			
			sBody_fr = rsEmail("email_textfransi")
			sSubject_fr =  Server.HtmlEncode(rsEmail("email_subjectFransi"))
			
			sFrom = rsEmail("email_from")
			sTo = rsEmail("email_toAddress")
			sCC = rsEmail("email_ccAddress")
			sBCC = rsEmail("email_bccAddress")			
			sToCat       =   ""
			sCCCat       =   ""
			sBCCCat      =   ""
			sToInterest  =   ""
			sCCInterest  =   ""
			sBCCInterest =   ""
			sCountries	 = 	 ""
			sIsIncludeCountry = True
			sAgeGroups	 = 	 ""
			sGenders	 = 	 ""
			sLanguage	 =   ""
			sToSubscribers = False
		End If
		Set rsEmail = Nothing
	End If
	
%>
	<table CELLSPACING="4" CELLPADDING="2" BORDER="1" width="100%" align="center" ID="Table4">
		<form name="frmEmail" id="frmEmail" method="post">
		<tr>
		    <td align="left" colspan="2">
				<h2>Ad hoc email </h2>
			</td>						
		</tr>
		<tr>
		    <td align="left" width="150px">* From:</td>
			<td align="left">
				<input type="text" name="txtFrom" id="txtFrom" value="<%=sFrom%>" style="width:420px" maxlength="100"/>	
		    </td>
		</tr>
		<tr>
		    <td align="left" width="150px">* To:</td>
			<td align="left">
				<input type="text" name="txtTo" id="txtTo" value="<%=sTo%>" style="width:420px" maxlength="4000"/>
				<input type="button" value="Add" onClick="showSearchPopup('To')" id="btnSelect" name="btnSelect">
				<input type="button" value="Clear" onClick="$('#txtTo').val('')"  id="btnSelect" name="btnSelect">	
                <input type="button" value="Add Newsletter Subscribers" onClick="showSearchSubscribers();"  id="btnAddSubscribers" name="btnAddSubscribers">
		    </td>			
		</tr>	
		<%
			Call DrawRoles("RoleTo", sToRoles, "")			
		%>
		<tr style="display:none">
		    <td align="left">CC:</td>
			<td align="left">
				<input type="text" name="txtCC" id="txtCC" value="<%=sCC%>" style="width:420px" maxlength="4000"/>
				<input type="button" value="Add" onClick="showSearchPopup('CC')" id="btnAddCC" name="btnAddCC">
				<input type="button" value="Clear" onClick="$('#txtCC').val('')"  id="btnClearCC" name="btnClearCC">	
		    </td>			
		</tr>	
		<%
			'Call DrawRoles("RoleCC", sCCRoles , "none")
		%>
		<tr style="display:none">
		    <td align="left">BCC:</td>
			<td align="left">
				<input type="text" name="txtBCC" id="txtBCC" value="<%=sBCC%>" style="width:420px" maxlength="4000"/>
				<input type="button" value="Add" onClick="showSearchPopup('BCC')" id="btnAddBCC" name="btnAddBCC">
				<input type="button" value="Clear" onClick="$('#txtBCC').val('')"  id="btnClearBCC" name="btnClearBCC">
		    </td>			
		</tr>
		<%
			'Call DrawRoles("RoleBCC", sBCCRoles, "none")
		%>
		<tr>
		    <td align="left">Options:</td>
			<td align="left">
				<input type="radio" name="rdLanguage" id="rdLanguage" value="all" <%If sLanguage = "all" Then%>checked<%End If%> onclick="changeLanguage(this.value)"/> Send to all English & Farsi members<br/>
				<input type="radio" name="rdLanguage" id="rdLanguage" value="en" <%If sLanguage = "en" Then%>checked<%End If%> onclick="changeLanguage(this.value)"/> Do NOT send Farsi members<br/>
				<input type="radio" name="rdLanguage" id="rdLanguage" value="fr" <%If sLanguage = "fr" Then%>checked<%End If%> onclick="changeLanguage(this.value)"/> Do NOT send English members
				<script>
					function changeLanguage(lan){
						if(lan == "all"){	//send All
							$("#subject_en").show();
							$("#subject_fr").show();
							$("#body_en").show();
							$("#body_fr").show();
						}
						else if(lan == "en"){	//send Egnlish
							$("#subject_en").show();
							$("#subject_fr").hide();
							$("#body_en").show();
							$("#body_fr").hide();
						}
						else{	// send Farsi
							$("#subject_en").hide();
							$("#subject_fr").show();
							$("#body_en").hide();
							$("#body_fr").show();
						}
					}
					<%If sLanguage <> "" Then%>
					$(document).ready(function(){
						changeLanguage("<%=sLanguage%>");
					});
					<%End If%>
				</script>
		    </td>			
		</tr>		
		<tr id="subject_en">
		    <td align="left">* Subject in English:</td>
			<td align="left">
				<input type="text" name="txtSubject" id="txtSubject" value="<%=sSubject%>" style="width:420px" maxlength="1000"/>
		    </td>			
		</tr>		
		<tr id="subject_fr">
		    <td align="left">* Subject in Farsi:</td>
			<td align="left">
				<input type="text" name="txtSubject_fr" id="txtSubject_fr" value="<%=sSubject_fr%>" style="width:420px" maxlength="1000" dir="rtl"/>
		    </td>			
		</tr>
		<tr id="body_en">
		    <td align="left" valign="top">* Body in English:</td>
			<td align="left">
				<table width="800" border="0">
					<tr>
						<td width="656">
							<textarea name="txtBody" class="ckeditor" id="txtBody" rows="10" cols="50" maxlength="100000"><%=Server.HtmlEncode(sBody)%></textarea>
						</td>
						<td valign="top" >
							<b>Tokens:</b><br>
							<p align="left">
							
							<b>%sitename%</b>: Site Name  <br />
							<b>%sitelink%</b>: Site link  <br/>
							<b>%UserName%</b>: Username<br />
							<b>%MemFirstName%</b>: First Name<br />
							<b>%MemLastName%</b>: Last Name <br />
							<b>%Email%</b>: Email Address<br />
							<b>%member-code%</b>: Member Code<br />
							</p>
						</td>
					</tr>
				</table>
				
				<span>
				
				</span>
		    </td>			
		</tr>	
		<tr id="body_fr">
		    <td align="left" valign="top">* Body in Farsi:</td>
			<td align="left">
				<table width="800" border="0">
					<tr>
						<td width="656">
							<textarea name="txtBody_fr" class="ckeditor" lang="fa" id="txtBody_fr" rows="10" cols="50" maxlength="100000"><%=Server.HtmlEncode(sBody_fr)%></textarea>
							<script type="text/javascript">
							    function setMemberMessageEmail() {
							        try {
							            CKEDITOR.instances.txtBody_fr.config.language = "fa";
							        }
							        catch (err) {
							            window.setTimeout(function () { setMemberMessageEmail() }, 500);
							        }
							    }
							    //setMemberMessageEmail();
							    //setCKEditorLang("txtBody_fr", "fa"); 
							   
                                $(document).ready(function () {
							        //setMemberMessageEmail();
                                    setCKEditorLang("txtBody_fr", "fa"); 
							    });	
							</script>
						</td>
						<td valign="top" >
							<b>Tokens:</b><br>
							<p align="left">
							<b>%sitename%</b>: Site Name  <br />
							<b>%sitelink%</b>: Site link  <br/>
							<b>%UserName%</b>: Username<br />
							<b>%MemFirstName%</b>: First Name<br />
							<b>%MemLastName%</b>: Last Name <br />
							<b>%Email%</b>: Email Address<br />
							<b>%member-code%</b>: Member Code<br />
							</p>
						</td>
					</tr>
				</table>
				
				<span>
				
				</span>
		    </td>			
		</tr>
		<tr>
		    <td align="left">Attachment:</td>
			<td align="left">
				<div id="uploadedFiles">
					<input type="hidden" name="uploaded_file" id="uploaded_file" value="" />
				<%
				idx = 0
				If sAttachments <> "" Then
					arrAttachments = Split(sAttachments, ";")
					For idx = 0 To Ubound(arrAttachments)
						filePath = arrAttachments(idx)
						fileName = Right(filePath, Len(filePath) - InStrRev(filePath, "\"))
						%>
						<div id="uploaded_file<%=idx+1%>_div" class="uploadedfile">
						<!--<input type="hidden" name="uploaded_file" id="uploaded_file<%=idx+1%>" value="<%=filePath%>" />-->
						<img border="0" alt="Info on DekohArchitecture.jpg" src="skins/standard/images/attachment.png">
						<input type="checkbox" name="uploaded_files" id="chk_file<%=idx+1%>" value="<%=fileName%>" checked />
						<a href="<%=gVirtualUploadPath%>upload/ADHOCEmail/<%=fileName%>" target="_blank"><%=fileName%></a>&nbsp;&nbsp;<a href="#" onclick="removeUploadedFile('uploaded_file<%=idx+1%>');return false;" style="font-size:10px;font-style:italic;">Remove</a>
						</div>
						<%
					Next 
				End If
				
				iMaxFileSize = GetMaxFileSize("document")
				If iMaxFileSize<>"unlimited" Then
					iMaxFileSize = iMaxFileSize*1024*1024
				Else
					iMaxFileSize = 1073741824
				End If
				
				%>
				</div>
				<style>
				.file{
					height:70px;
					width:100%;
				}
				</style>
				<script>
					var UploadedFiles = <%=idx%>;
				</script>
				<input type="hidden" name="SessionKey" value="<%=mm_strSessionKey%>">
				<div id="file1_div" class="file" <%If idx = 5 Then%>style="display:none"<%End If%>>
					<table width="100%">
						<tr>
							<td width="500px">
								<iframe 
									id="uploadControl1" name="uploadControl1"
									scrolling="no" frameborder="0" 
									height="70px"
									width="100%" 
									src="<%=gVirtualUploadPath%>UploadControls.aspx?SessionKey=<%=mm_strSessionKey%>_file1&amp;MaxFileSize=<%=iMaxFileSize%>&amp;FileFilter=&amp;lang=<%If IsFarsi() Then%>fr<%Else%>en<%End If%>&amp;callback=UpdatingData1" 
									>
									Your browser does not support inline frames or is currently configured not to display inline frames
								</iframe>
							</td>
							<td>
								<input type="hidden" name="Txt_File1" id="Txt_File1" size="45" value="">
								<a href="#" id="r1" onclick="removeFile('file1');return false;" style="font-size:10px;font-style:italic;<%If idx = 0 Then%>display:none;<%End If%>">Remove</a>
							</td>
						</tr>
					</table>
				</div>
				<div id="file2_div" class="file" style="display:none">
					<table width="100%">
						<tr>
							<td width="500px">
								<iframe 
									id="uploadControl2" name="uploadControl2"
									scrolling="no" frameborder="0" 
									height="70px"
									width="100%" 
									src="<%=gVirtualUploadPath%>UploadControls.aspx?SessionKey=<%=mm_strSessionKey%>_file2&amp;MaxFileSize=<%=iMaxFileSize%>&amp;FileFilter=&amp;lang=<%If IsFarsi() Then%>fr<%Else%>en<%End If%>&amp;callback=UpdatingData2" 
									>
									Your browser does not support inline frames or is currently configured not to display inline frames
								</iframe>
							</td>
							<td>
								<input type="hidden" name="Txt_File2" id="Txt_File2" size="45" value="">
								<a href="#" id="r2" onclick="removeFile('file2');return false;" style="font-size:10px;font-style:italic;">Remove</a>
							</td>
						</tr>
					</table>
				</div>
				<div id="file3_div" class="file" style="display:none">
					<table width="100%">
						<tr>
							<td width="500px">
								<iframe 
									id="uploadControl3" name="uploadControl3"
									scrolling="no" frameborder="0" 
									height="70px"
									width="100%" 
									src="<%=gVirtualUploadPath%>UploadControls.aspx?SessionKey=<%=mm_strSessionKey%>_file3&amp;MaxFileSize=<%=iMaxFileSize%>&amp;FileFilter=&amp;lang=<%If IsFarsi() Then%>fr<%Else%>en<%End If%>&amp;callback=UpdatingData3" 
									>
									Your browser does not support inline frames or is currently configured not to display inline frames
								</iframe>
							</td>
							<td>
								<input type="hidden" name="Txt_File3" id="Txt_File3" size="45" value="">
								<a href="#" id="r3" onclick="removeFile('file3');return false;" style="font-size:10px;font-style:italic;">Remove</a>
							</td>
						</tr>
					</table>
				</div>
				<div id="file4_div" class="file" style="display:none">
					<table width="100%">
						<tr>
							<td width="500px">
								<iframe 
									id="uploadControl4" name="uploadControl4"
									scrolling="no" frameborder="0" 
									height="70px"
									width="100%" 
									src="<%=gVirtualUploadPath%>UploadControls.aspx?SessionKey=<%=mm_strSessionKey%>_file4&amp;MaxFileSize=<%=iMaxFileSize%>&amp;FileFilter=&amp;lang=<%If IsFarsi() Then%>fr<%Else%>en<%End If%>&amp;callback=UpdatingData4" 
									>
									Your browser does not support inline frames or is currently configured not to display inline frames
								</iframe>
							</td>
							<td>
								<input type="hidden" name="Txt_File4" id="Txt_File4" size="45" value="">
								<a href="#" id="r4" onclick="removeFile('file4');return false;" style="font-size:10px;font-style:italic;">Remove</a>
							</td>
						</tr>
					</table>
				</div>
				<div id="file5_div" class="file" style="display:none">
					<table width="100%">
						<tr>
							<td width="500px">
								<iframe 
									id="uploadControl5" name="uploadControl5"
									scrolling="no" frameborder="0" 
									height="70px"
									width="100%" 
									src="<%=gVirtualUploadPath%>UploadControls.aspx?SessionKey=<%=mm_strSessionKey%>_file5&amp;MaxFileSize=<%=iMaxFileSize%>&amp;FileFilter=&amp;lang=<%If IsFarsi() Then%>fr<%Else%>en<%End If%>&amp;callback=UpdatingData5" 
									>
									Your browser does not support inline frames or is currently configured not to display inline frames
								</iframe>
							</td>
							<td>
								<input type="hidden" name="Txt_File5" id="Txt_File5" size="45" value="">
								<a href="#" id="r5" onclick="removeFile('file5');return false;" style="font-size:10px;font-style:italic;">Remove</a>
							</td>
						</tr>
					</table>
				</div>
				<a id="addmore" href="#" onclick="addFile();return false;" <%If idx = 5 Then%>style="display:none"<%End If%>>Add more</a>
		    </td>			
		</tr>
		<tr>
		    <td  align="">		        
		    </td>
			 <td  align="">
				<input type="hidden" id="actionPage" name="actionPage" value="" />
				<input type="hidden" id="CntTo" name="CntTo" value="0" />
				<input type="hidden" id="id" name="id" value="<%=ID%>" />
		        <input type="button" value="Send Email" onClick="CountEmails(true);" id="btnSend" name="btnSend">
				<input type="button" value="Save Now" onClick="sendAddHoc('2')" id="btnSave" name="btnSave">
				<input type="button" value="Preview" onClick="CountEmails(false);" id="btnPreview" name="btnPreview">
				<a href="admin_params_edit.asp?rCodeSlct=56&rMailType=2">
				<input type="button" value="Cancel" id="btnCancel" name="btnCancel">
				</a>
		    </td>
		</tr>

		</form>
	</table>
<%
end sub

Function MemaCreateGUID(byval tmpLength)
  Randomize Timer
  Dim tmpCounter, tmpGUID
  Const strValid = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  For tmpCounter = 1 To tmpLength
		tmpGUID = tmpGUID & Mid(strValid, Int(Rnd(1) * Len(strValid)) + 1, 1)
  Next
  MemaCreateGUID = tmpGUID
End Function

Sub DrawRoles(sType, selectedRoles, sDisplay)
%>
	<tr style="display:<%= sDisplay %>" >
		<td align="left" valign="top"></td>
		<td align="left">		
		<input type="hidden" value="" id="hid<%=sType%>" name="hid<%=sType%>" />
		
		<div>
            <img border="0" style="margin-bottom:-3px;" id="img_<%= sType %>" onclick="ToggleRole('<%= sType %>');return false;" src="skins/standard/images/contestant/right_table/plus1.gif" />        
                &nbsp;<a href="#" onclick="ToggleRole('<%= sType %>');return false;" >Roles</a>
        </div>
		<div id="div_<%= sType %>" style="display:none">
		<table><tr>
		<%
		sql = "SELECT *	FROM UserRoles WHERE AdminRole = 0 And Template IS NULL"
		cnt = 0
		Set rsRole = con.execute(sql)
		If Not rsRole.EOF then									
			While Not rsRole.eof 
				sRoleId = rsRole("ID")	
				stRoleDes = rsRole("Description") 
				sRoleName = rsRole("RoleName")
				cnt = cnt + 1
		%>	
			<td>
			<input TYPE="checkbox" id="chk<%=sType%><%=sRoleId%>" <%If InStr(";"&selectedRoles&";", ";"&sRoleId&";") > 0 Then%>checked<%End If%> name="chk<%=sType%><%=sRoleId%>" value="<%=sRoleId%>" />&nbsp;<%=sRoleName%>
			</td>
			<%If cnt mod 4 = 0 Then%> </tr><tr> <%End If%>
		<%
				rsRole.moveNext
			Wend
		End If
		Set rsRole = Nothing	
		%>	
		</tr></table>
		</div>
		
		<div>
            <img border="0" style="margin-bottom:-3px;" id="img_Subscriber<%= sType %>" onclick="ToggleRole('Subscriber<%= sType %>');return false;" src="skins/standard/images/contestant/right_table/plus1.gif" />        
                &nbsp;<a href="#" onclick="ToggleRole('Subscriber<%= sType %>');return false;" >Newsletter Subscribers</a>
        </div>
		<div id="div_Subscriber<%= sType %>" style="display:none">
			<input TYPE="checkbox" id="chkSubscriber<%=sType%>" name="chkSubscriber<%=sType%>" <%If sToSubscribers Then%>checked<%End If%> value="y" />&nbsp;Send email to all newsletter subscribers
		</div>
		
		<input type="hidden" value="" id="hidAdmin<%=sType%>" name="hidAdmin<%=sType%>" />
		
		<% Call DrawCatSubCat(sType,"") %>		
		<% Call DrawInterest(sType,"") %>
		<% Call DrawCountries(sType) %>
		<% Call DrawAgeGroups(sType) %>
		<% Call DrawGenders(sType) %>
		</td>			
	</tr>	
<%
End Sub

'**********************************************************
%>
<script language="javascript" type="text/javascript">
	var arrUploadedFiles = new Array(5);
	var arrHasFiles = new Array(5);
	
	function resetStatus(){
		for(var i=1;i<=5;i++){
			arrUploadedFiles[i] = false;
			var uControl = eval("uploadControl"+i);
			if(uControl)
				arrHasFiles[i] = uControl.fn_HasFile();
			else
				arrHasFiles[i] = false;
		}	
	}
	
	function hasFiles(){
		for(var i=1;i<=5;i++){
			var uControl = eval("uploadControl"+i);
			if(uControl && uControl.fn_HasFile())
				return true;
		}
		return false;
	}
	
	function isUploaded(){
		for(var i=1;i<=5;i++){
			if(arrHasFiles[i] && arrUploadedFiles[i] == false)
				return false;
		}	
		return true;
	}
	function UpdateValue(){
		var chkFiles = $("input[name='uploaded_files']:checked");
		var value = "";
		for(var idx=0;idx<chkFiles.length;idx++){
			if(value == "")
				value = chkFiles[idx].value;
			else
				value = value + ";" + chkFiles[idx].value;
		}
		
		$("#uploaded_file").val(value);
	}
	function UpdatingData1(errorCode, resultfileName)
	{
		if (errorCode==0) 
		{
			arrUploadedFiles[1] = true;
			$("#Txt_File1").val(resultfileName);
			var i = $(".uploadedfile").length + 1;
						
			$("#uploadedFiles").append(
				"<div id=\"uploaded_file"+i+"_div\" class=\"uploadedfile\">" +
				"<img border=\"0\" src=\"skins/standard/images/attachment.png\">&nbsp;" +
				"<input type=\"checkbox\" name=\"uploaded_files\" id=\"chk_file"+i+"\" value=\"<%=mm_strSessionKey%>_file1:" + resultfileName + "\" checked />" +
				"<a href=\"<%=gVirtualUploadPath%>upload/ADHOCEmail/" + resultfileName + "\" target=\"_blank\">" + resultfileName + "</a>&nbsp;&nbsp;<a href=\"#\" onclick=\"removeUploadedFile('uploaded_file"+i+"');return false;\" style=\"font-size:10px;font-style:italic;\">Remove</a>" +
				"</div>"
			);
			removeFile('file1');
			
			if(isUploaded()){
				UpdateValue();
				$("#frmEmail").submit();
			}	
		}
		else if (errorCode==1) alert("Select one file Farsi to upload.");	
		else if (errorCode==2){ 
			alert("File type is invalid");
			uploadControl1.document.location.href = uploadControl1.document.location.href + "&error=" + (new Date()).getSeconds();
		}
		else if (errorCode==-1) alert("Upload Fail: An exception occurred.");
	}
	function UpdatingData2(errorCode, resultfileName)
	{
		if (errorCode==0) 
		{
			arrUploadedFiles[2] = true;
			$("#Txt_File2").val(resultfileName);
			var i = $(".uploadedfile").length + 1;
						
			$("#uploadedFiles").append(
				"<div id=\"uploaded_file"+i+"_div\" class=\"uploadedfile\">" +
				"<img border=\"0\" src=\"skins/standard/images/attachment.png\">&nbsp;" +
				"<input type=\"checkbox\" name=\"uploaded_files\" id=\"chk_file"+i+"\" value=\"<%=mm_strSessionKey%>_file2:" + resultfileName + "\" checked />" +
				"<a href=\"<%=gVirtualUploadPath%>upload/ADHOCEmail/" + resultfileName + "\" target=\"_blank\">" + resultfileName + "</a>&nbsp;&nbsp;<a href=\"#\" onclick=\"removeUploadedFile('uploaded_file"+i+"');return false;\" style=\"font-size:10px;font-style:italic;\">Remove</a>" +
				"</div>"
			);
			removeFile('file2');
			
			if(isUploaded()){
				UpdateValue();
				$("#frmEmail").submit();
			}	
		}
		else if (errorCode==1) alert("Select one file Farsi to upload.");	
		else if (errorCode==2){ 
			alert("File type is invalid");
			uploadControl2.document.location.href = uploadControl2.document.location.href + "&error=" + (new Date()).getSeconds();
		}
		else if (errorCode==-1) alert("Upload Fail: An exception occurred.");
	}
	function UpdatingData3(errorCode, resultfileName)
	{
		if (errorCode==0) 
		{
			arrUploadedFiles[3] = true;
			$("#Txt_File3").val(resultfileName);
			var i = $(".uploadedfile").length + 1;
						
			$("#uploadedFiles").append(
				"<div id=\"uploaded_file"+i+"_div\" class=\"uploadedfile\">" +
				"<img border=\"0\" src=\"skins/standard/images/attachment.png\">&nbsp;" +
				"<input type=\"checkbox\" name=\"uploaded_files\" id=\"chk_file"+i+"\" value=\"<%=mm_strSessionKey%>_file3:" + resultfileName + "\" checked />" +
				"<a href=\"<%=gVirtualUploadPath%>upload/ADHOCEmail/" + resultfileName + "\" target=\"_blank\">" + resultfileName + "</a>&nbsp;&nbsp;<a href=\"#\" onclick=\"removeUploadedFile('uploaded_file"+i+"');return false;\" style=\"font-size:10px;font-style:italic;\">Remove</a>" +
				"</div>"
			);
			removeFile('file3');
			
			if(isUploaded()){
				UpdateValue();
				$("#frmEmail").submit();
			}
		}
		else if (errorCode==1) alert("Select one file Farsi to upload.");	
		else if (errorCode==2){ 
			alert("File type is invalid");
			uploadControl3.document.location.href = uploadControl3.document.location.href + "&error=" + (new Date()).getSeconds();
		}
		else if (errorCode==-1) alert("Upload Fail: An exception occurred.");
	}
	function UpdatingData4(errorCode, resultfileName)
	{
		if (errorCode==0) 
		{
			arrUploadedFiles[4] = true;
			$("#Txt_File4").val(resultfileName);
			var i = $(".uploadedfile").length + 1;
						
			$("#uploadedFiles").append(
				"<div id=\"uploaded_file"+i+"_div\" class=\"uploadedfile\">" +
				"<img border=\"0\" src=\"skins/standard/images/attachment.png\">&nbsp;" +
				"<input type=\"checkbox\" name=\"uploaded_files\" id=\"chk_file"+i+"\" value=\"<%=mm_strSessionKey%>_file4:" + resultfileName + "\" checked />" +
				"<a href=\"<%=gVirtualUploadPath%>upload/ADHOCEmail/" + resultfileName + "\" target=\"_blank\">" + resultfileName + "</a>&nbsp;&nbsp;<a href=\"#\" onclick=\"removeUploadedFile('uploaded_file"+i+"');return false;\" style=\"font-size:10px;font-style:italic;\">Remove</a>" +
				"</div>"
			);
			removeFile('file4');
			if(isUploaded()){
				UpdateValue();
				$("#frmEmail").submit();
			}
		}
		else if (errorCode==1) alert("Select one file Farsi to upload.");	
		else if (errorCode==2){ 
			alert("File type is invalid");
			uploadControl4.document.location.href = uploadControl4.document.location.href + "&error=" + (new Date()).getSeconds();
		}
		else if (errorCode==-1) alert("Upload Fail: An exception occurred.");
	}
	function UpdatingData5(errorCode, resultfileName)
	{
		if (errorCode==0) 
		{
			arrUploadedFiles[5] = true;
			$("#Txt_File5").val(resultfileName);
			var i = $(".uploadedfile").length + 1;
						
			$("#uploadedFiles").append(
				"<div id=\"uploaded_file"+i+"_div\" class=\"uploadedfile\">" +
				"<img border=\"0\" src=\"skins/standard/images/attachment.png\">&nbsp;" +
				"<input type=\"checkbox\" name=\"uploaded_files\" id=\"chk_file"+i+"\" value=\"<%=mm_strSessionKey%>_file5:" + resultfileName + "\" checked />" +
				"<a href=\"<%=gVirtualUploadPath%>upload/ADHOCEmail/" + resultfileName + "\" target=\"_blank\">" + resultfileName + "</a>&nbsp;&nbsp;<a href=\"#\" onclick=\"removeUploadedFile('uploaded_file"+i+"');return false;\" style=\"font-size:10px;font-style:italic;\">Remove</a>" +
				"</div>"
			);
			removeFile('file5');
			if(isUploaded()){
				UpdateValue();
				$("#frmEmail").submit();
			}
		}
		else if (errorCode==1) alert("Select one file Farsi to upload.");	
		else if (errorCode==2){ 
			alert("File type is invalid");
			uploadControl5.document.location.href = uploadControl5.document.location.href + "&error=" + (new Date()).getSeconds();
		}
		else if (errorCode==-1) alert("Upload Fail: An exception occurred.");
	}
	
	function showSearchPopup(iEmailType)
    {
		var windowWidth = 1170;
		var windowHeight = 560;
		var centerWidth = (window.screen.width - windowWidth) / 2;
	    var centerHeight = (window.screen.height - windowHeight) / 2;
        
       	var url = "admin_fans.asp?rViewSlct=4&rEmailType=" + iEmailType;
			
        window.open(url, "Admin_Members", "left=" + centerWidth + ", top=" + centerHeight + ", width=" + windowWidth + ", height=" + windowHeight +",scrollbars=1" );

    }
    function showSearchSubscribers() {
        var windowWidth = 1170;
        var windowHeight = 560;
        var centerWidth = (window.screen.width - windowWidth) / 2;
        var centerHeight = (window.screen.height - windowHeight) / 2;

        var url = "report_emailsubscription.asp?rSelectOption=1&rviewtype=1";
        var options = "left=" + centerWidth + ", top=" + centerHeight + ", width=" + windowWidth + ", height=" + windowHeight + ",scrollbars=1";
        window.open(url, "SelectSubscribers", options);

    }

	function sendAddHoc(type)
    {	
		resetStatus();
		if (type=="1"){
			if (confirm("Do you want to send this Adhoc Email?")){
			    $("#btnPreview").removeAttr("onclick").unbind('click');  
				if(($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "en") && $("#txtSubject").val() == ""){
					alert("Please enter email subject in English!");
					$("#txtSubject").focus();
					return;
				}
				
				if(($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "fr") && $("#txtSubject_fr").val() == ""){
					alert("Please enter email subject in Farsi!");
					$("#txtSubject_fr").focus();
					return;
				}
				
				if ($("#txtFrom").val() == ""){
					alert("Please input From");
					$("#txtFrom").focus();
					return;
				}	

				if ($("#txtTo").val() == ""){
				    if ($("input[id^='chkRoleTo']:checked").length <= 0 && $("input[name^='RoleTo_cat']:checked").length <= 0 && $("input[name^='RoleTo_interest']:checked").length <= 0 && $("input[name^='RoleTo_Country']:checked").length <= 0 && $("input[name^='RoleTo_AgeGroup']:checked").length <= 0 && $("input[name^='RoleTo_Gender']:checked").get(0).value == "a" && $("input[id^='chkSubscriberRoleTo']:checked").length <= 0)
					 {
						alert("Please input To");
						$("#txtTo").focus();
						return;
					 }

				}
				if ( $("#txtTo").val() != "" )
				{
					if (!multiEmail($("#txtTo").val())) {
						alert('One or more to email addresses entered is invalid');
						$("#txtTo").focus();
						return;
					}
				}
				if ($("#txtCC").val() != "") {
					if (!multiEmail($("#txtCC").val())) {
						alert('One or more CC email addresses entered is invalid');
						$("#txtCC").focus();
						return;
					}
				}

				if ($("#txtBCC").val() != "") {
					if (!multiEmail($("#txtBCC").val())) {
						alert('One or more BCC email addresses entered is invalid');
						$("#txtBCC").focus();
						return;
					}
				}

				//Roles To
				var hidRoleTo = document.getElementById("hidRoleTo");
				hidRoleTo.value = "";
				$("input[name*='chkRoleTo']").each(function(){
					if (this.checked){				
						hidRoleTo.value += hidRoleTo.value==""?this.value:";" + this.value;
					}
				});	

				//Admin Roles To
				var hidAdminRoleTo = document.getElementById("hidAdminRoleTo");
				hidAdminRoleTo.value = "";
				$("input[name*='chkAdminRoleTo']").each(function(){
					if (this.checked){				
						hidAdminRoleTo.value += hidAdminRoleTo.value==""?this.value:";" + this.value;
					}			
				});	
				
				/*
				//Roles CC
				var hidRoleCC = document.getElementById("hidRoleCC");
				hidRoleCC.value = "";
				$("input[name*='chkRoleCC']").each(function(){
					if (this.checked){				
						hidRoleCC.value += hidRoleCC.value==""?this.value:";" + this.value;
					}
				});	

				//Admin Roles CC
				var hidAdminRoleCC = document.getElementById("hidAdminRoleCC");
				hidAdminRoleCC.value = "";
				$("input[name*='chkAdminRoleCC']").each(function(){
					if (this.checked){				
						hidAdminRoleCC.value += hidAdminRoleCC.value==""?this.value:";" + this.value;
					}			
				});
				
				//Roles BCC
				var hidRoleBCC = document.getElementById("hidRoleBCC");
				hidRoleBCC.value = "";
				$("input[name*='chkRoleBCC']").each(function(){
					if (this.checked){				
						hidRoleBCC.value += hidRoleBCC.value==""?this.value:";" + this.value;
					}
				});	

				//Admin Roles BCC
				var hidAdminRoleBCC = document.getElementById("hidAdminRoleBCC");
				hidAdminRoleBCC.value = "";
				$("input[name*='chkAdminRoleBCC']").each(function(){
					if (this.checked){				
						hidAdminRoleBCC.value += hidAdminRoleBCC.value==""?this.value:";" + this.value;
					}			
				});		
				*/
				
				var body = CKEDITOR.instances.txtBody.getData();
				var body_fr = CKEDITOR.instances.txtBody_fr.getData();
				//if($("#txtBody").val() == ""){
				if(($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "en") && body == ""){
					alert("Please enter email body in English!");
					//$("#txtBody").focus();
					return;
				}
				if(($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "fr") && body_fr == ""){
					alert("Please enter email body in Farsi!");
					//$("#txtBody_fr").focus();
					return;
				}
				
				$("#actionPage").val("1");	
				
				//submit form	
				if(!hasFiles()){
					UpdateValue();
					$("#frmEmail").submit();
					$("#btnDoSend").unbind('click'); 
				}	
				else{
					if(uploadControl1 && uploadControl1.fn_HasFile())
						uploadControl1.fn_DoUpload();
					if(uploadControl2 && uploadControl2.fn_HasFile())
						uploadControl2.fn_DoUpload();
					if(uploadControl3 && uploadControl3.fn_HasFile())
						uploadControl3.fn_DoUpload();
					if(uploadControl4 && uploadControl4.fn_HasFile())
						uploadControl4.fn_DoUpload();
					if(uploadControl5 && uploadControl5.fn_HasFile())
						uploadControl5.fn_DoUpload();
				}				
			}
		}
		else{
		    if (confirm("Do you want to save this Adhoc Email?")) {
		        $("#btnPreview").removeAttr("onclick").unbind('click'); 
				//Roles To
				var hidRoleTo = document.getElementById("hidRoleTo");
				hidRoleTo.value = "";
				$("input[name*='chkRoleTo']").each(function(){
					if (this.checked){				
						hidRoleTo.value += hidRoleTo.value==""?this.value:";" + this.value;
					}
				});	

				//Admin Roles To
				var hidAdminRoleTo = document.getElementById("hidAdminRoleTo");
				hidAdminRoleTo.value = "";
				$("input[name*='chkAdminRoleTo']").each(function(){
					if (this.checked){				
						hidAdminRoleTo.value += hidAdminRoleTo.value==""?this.value:";" + this.value;
					}			
				});	
				
				/*
				//Roles CC
				var hidRoleCC = document.getElementById("hidRoleCC");
				hidRoleCC.value = "";
				$("input[name*='chkRoleCC']").each(function(){
					if (this.checked){				
						hidRoleCC.value += hidRoleCC.value==""?this.value:";" + this.value;
					}
				});	

				//Admin Roles CC
				var hidAdminRoleCC = document.getElementById("hidAdminRoleCC");
				hidAdminRoleCC.value = "";
				$("input[name*='chkAdminRoleCC']").each(function(){
					if (this.checked){				
						hidAdminRoleCC.value += hidAdminRoleCC.value==""?this.value:";" + this.value;
					}			
				});
				
				//Roles BCC
				var hidRoleBCC = document.getElementById("hidRoleBCC");
				hidRoleBCC.value = "";
				$("input[name*='chkRoleBCC']").each(function(){
					if (this.checked){				
						hidRoleBCC.value += hidRoleBCC.value==""?this.value:";" + this.value;
					}
				});	

				//Admin Roles BCC
				var hidAdminRoleBCC = document.getElementById("hidAdminRoleBCC");
				hidAdminRoleBCC.value = "";
				$("input[name*='chkAdminRoleBCC']").each(function(){
					if (this.checked){				
						hidAdminRoleBCC.value += hidAdminRoleBCC.value==""?this.value:";" + this.value;
					}			
				});	
				*/
				
				//action type
				$("#actionPage").val("2");
				
				//submit form	
				if(!hasFiles()){
					UpdateValue();
					$("#frmEmail").submit();
				}
				else{
					if(uploadControl1 && uploadControl1.fn_HasFile())
						uploadControl1.fn_DoUpload();
					if(uploadControl2 && uploadControl2.fn_HasFile())
						uploadControl2.fn_DoUpload();
					if(uploadControl3 && uploadControl3.fn_HasFile())
						uploadControl3.fn_DoUpload();
					if(uploadControl4 && uploadControl4.fn_HasFile())
						uploadControl4.fn_DoUpload();
					if(uploadControl5 && uploadControl5.fn_HasFile())
						uploadControl5.fn_DoUpload();
				}
			}	
		}
		$("#dvReviewEmail").dialog("close");
    }
    function SelectUser(ddlUserType, pageRef, currentUserType)
    {
	    var usertype = ddlUserType.value;
	    pageRef = pageRef.replace("rUserType=" + currentUserType,"rUserType=" + usertype);
	    
	    window.location.href = pageRef;
    }
    function PaypalSave(dForm,dlink)
	{
		dForm.action =  dlink
		dForm.submit();
    }
    
    function LoadSelectedCat(dType,dSelectedCat) {
        var dvCat = $("#GroupName_" + dType);
        var arr = dSelectedCat.split(',');
        for (i = 0; i < arr.length; i++)
            $(":checkbox[value='" + $.trim(arr[i]) + "']", $(dvCat)).attr("checked", "checked");
    }

    function LoadSelectedOnlyLeader(dChecked) { 
        if(dChecked == "True")
            $("#chkJustSendForLeader").attr("checked","checked");
        else
            $("#chkJustSendForLeader").removeAttr("checked");
    }

    function LoadSelectedInterest(dType, dSelectedCat) {
        var dvCat = $("#Interest_" + dType);
        var arr = dSelectedCat.split(',');
        for (i = 0; i < arr.length; i++)
            $(":checkbox[value='" + $.trim(arr[i]) + "']", $(dvCat)).attr("checked", "checked");
    }
	function LoadSelectedContry(dType, dSelectedCountry) {
        var dvCat = $("#Country_" + dType);
        var arr = dSelectedCountry.split(',');
        for (i = 0; i < arr.length; i++)
            $(":checkbox[value='" + $.trim(arr[i]) + "']", $(dvCat)).attr("checked", "checked");
    }
	function LoadSelectedAgeGroup(dType, dSelectedAgeGroup) {
        var dvCat = $("#AgeGroup_" + dType);
        var arr = dSelectedAgeGroup.split(',');
        for (i = 0; i < arr.length; i++)
            $(":checkbox[value='" + $.trim(arr[i]) + "']", $(dvCat)).attr("checked", "checked");
    }
	function LoadSelectedGender(dType, dSelectedGender) {
        var dvCat = $("#Gender_" + dType);
        var arr = dSelectedGender.split(',');
        for (i = 0; i < arr.length; i++)
            $(":checkbox[value='" + $.trim(arr[i]) + "']", $(dvCat)).attr("checked", "checked");
    }
    function CountEmails(hasBottomBar) {
        //Roles To
        var hidRoleTo = document.getElementById("hidRoleTo");
        hidRoleTo.value = "";
        $("input[name*='chkRoleTo']").each(function() {
            if (this.checked) {
                hidRoleTo.value += hidRoleTo.value == "" ? this.value : ";" + this.value;
            }
        });

		/*
        //Roles CC
        var hidRoleCC = document.getElementById("hidRoleCC");
        hidRoleCC.value = "";
        $("input[name*='chkRoleCC']").each(function() {
            if (this.checked) {
                hidRoleCC.value += hidRoleCC.value == "" ? this.value : ";" + this.value;
            }
        });

        //Roles BCC
        var hidRoleBCC = document.getElementById("hidRoleBCC");
        hidRoleBCC.value = "";
        $("input[name*='chkRoleBCC']").each(function() {
            if (this.checked) {
                hidRoleBCC.value += hidRoleBCC.value == "" ? this.value : ";" + this.value;
            }
        });
		*/
		
		//check if admin selected options
		var selected = false;
        $("input[name='rdLanguage']").each(function() {
            if (this.checked) {
                selected = true;
            }
        });
		
		if(!selected){
			alert("Please Select Email Options");
			$("#rdLanguage").get(0).focus();
			
			return false;
		}
		
        $("#actionPage").val("3");
        var dataPOST = $("#frmEmail").serializeArray();

        $.ajax({
            type: "POST",
            url: "admin_emails.asp",
            data: dataPOST,
            success: function(msg) {
                var re = eval("(" + msg + ")");
                showReviewEmail(false, hasBottomBar, re);
            }
        });
    }

</script>
<!--******************************************************-->
<link href="js/jquery-ui-1.7.2.custom.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jquery-ui-1.7.2.custom.min.js"></script>
<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript">
var IdTemp = 0
$(document).ready(function() {
    $("#btnSubmit").click(function() {
        SubmitPromoCode(1);
    });
});		
function removeUploadedFile(id){
	$("#"+id+"_div").html("");
	$("#"+id+"_div").remove();
	
	if($(".uploadedfile").length==0){
		$("#r1").hide();
		if($(".file:visible").length==0)
			addFile();
	}	
		
	$("#addmore").show();
}
function removeFile(id){
	$("#"+id+"_div").html($("#"+id+"_div").html());
	$("#"+id+"_div").hide();
	$("#addmore").show();
}
function addFile(){
	var i = $(".uploadedfile").length + 1;
	for(;i<=5;i++){
		if(!$("#file"+i+"_div").is(":visible")){
			$("#file"+i+"_div").show();
			break;
		}
	}
	
	if($(".file:visible").length+ $(".uploadedfile").length >= 5) 
		$("#addmore").hide();
	else
		$("#addmore").show();
}
function multiEmail(email_field) {
    var email = email_field.split(';');
    for (var i = 0; i < email.length; i++) {
        if (!validateEmail($.trim(email[i]))) {           
            return false;
        }
    }
    return true;
}
function validateEmail(eml) {
    var RegExPattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    // /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$/;
    var errorMessage = 'Invalid';
    if ((eml.match(RegExPattern)) && (eml.value != '')) {
        return true;
    }
    else {
        return false;
    }
}
function GetAttachments() {
    var re = "";
	var chkFiles = $("input[name='uploaded_files']:checked");
	for(var idx=0;idx<chkFiles.length;idx++){
		re += chkFiles[idx].value + "<br />";
	}
    for (i = 1; i <= 5; i++) {
		var uControl = eval("uploadControl"+i);
		if(uControl && uControl.fn_HasFile())
			re += uControl.fn_GetFileName() + "<br />";
    }
    return re;
}
function showReviewEmail(isClose,hasBottomBar,obj) {
    if (!isClose) {

        var body = CKEDITOR.instances.txtBody.getData();
		var body_fr = CKEDITOR.instances.txtBody_fr.getData();
		
		$("#CntTo").val(obj.CntTo);
        var html = "<table>";
        html += "<tr><td>To:</td><td>" + obj.CntTo + "</td></tr>";
        html += "<tr><td>&nbsp;</td><td><textarea cols='120' rows='2' readonly='readonly'>" + obj.ToList + "</textarea></td></tr>";
        /*
		html += "<tr><td>CC:</td><td>" + obj.CntCC + "</td></tr>";
        html += "<tr><td>&nbsp;</td><td><textarea cols='120' rows='2' readonly='readonly'>" + obj.CCList + "</textarea></td></tr>";
        html += "<tr><td>BCC:</td><td>" + obj.CntBCC + "</td></tr>";
        html += "<tr><td>&nbsp;</td><td><textarea cols='120' rows='2' readonly='readonly'>" + obj.BCCList + "</textarea></td></tr>"; 
		*/
        html += "<tr><td valign='top'>Attachments</td><td valign='top'>" + GetAttachments() + "</td></tr>";
        html += "</table><div>&nbsp;<div>";
		
		if($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "en")
			html += "<div style='padding:5px 0px'><b>Subject English:</b><br/> " + $("#txtSubject").val() + "</div>";
			
		if($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "fr")	
			html += "<div style='padding:5px 0px'><b>Subject Farsi:</b><div dir='rtl' align='left'>" + $("#txtSubject_fr").val() + "</div></div>";
		
		if($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "en"){
			html += "<div style='padding:5px 0px'><b>Body English:</b></div>";
			html += "<div style='max-height:300px;overflow-y:auto;border:solid 1px #ccc;padding:5px;'>" + body + "</div>";
		}
		if($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "fr"){
			html += "<div style='padding:5px 0px'><b>Body Farsi:</b></div>";
			html += "<div style='max-height:300px;overflow-y:auto;border:solid 1px #ccc;padding:5px;' dir='rtl'>" + body_fr + "</div>";
		}
        if (hasBottomBar == true)
            html += "<div style='padding:5px 0px'><input type='button' value='Send' id='btnDoSend' onclick='sendAddHoc(\"1\")'> <input type='button' value='Cancel' onclick='showReviewEmail(true,false,null)'><div>";
                
        $("#dvReviewEmail").html(html);
        
        $("#dvReviewEmail").dialog({
            autoOpen: true,
            width: 720,
            minWidth: 425,
            minHeight: 300,
            modal: true,
            zIndex: 35000
        });
        $("#dvReviewEmail").dialog("option", "title", ($("#rdLanguage:checked").val() == "all" || $("#rdLanguage:checked").val() == "en")?$("#txtSubject").val():$("#txtSubject_fr").val());
        $("#dvReviewEmail").dialog("open");
    } else {
        $("#dvReviewEmail").dialog("close");
    }
}
</script>
<div id="promo_popup" title="Promotional code" style="display:none;">
<input type="hidden" id="hidSelectedMemberID" value="" />
<h3>Please enter promotional code</h3>
<input type="text" id="txt_PromoCode" value="" style="width:200" />
<input type="button" value="Submit" id="btnSubmit"  />
</div>
<!--******************************************************-->

<!--#INCLUDE FILE="includes/Global.asp"-->
<%
bOneColumn = True
m_sPageName = "Admin Members"

'************************************************************

const kTabColor_Orange			= 1
const kTabColor_Green			= 2
const kTabColor_yellow			= 3

const kTabTitles	= "Members|Contestants|Fans"
uTabTitles = Split(kTabTitles, "|")

InitMutiTab(2)

uTabColors(kTabSlct_Fans) = kTabColor_Green
uTabColors(kTabSlct_Contestants) = kTabColor_Orange
uTabColors(kTabSlct_Members) = kTabColor_yellow

call SetupMultiTab()
%>

<!--#INCLUDE FILE="includes/base_page.asp"-->

	<%	
		DrawTblAdmin("start")
		'DrawPanel() 
	%>
		<div id="dvReviewEmail"></div>		
		<table cellspacing="0" cellspacing="0"  border="0"  width="100%"  id="Table_cmain">
			<tr>
				<td>
					<% 
					call DrawEmail() 
					%>
				</td>
			</tr>
		</table>
	<%DrawTblAdmin("end")%>
	
<script type="text/javascript">
    $(document).ready(function () {
        LoadSelectedCat("RoleTo", "<%= sToCat %>");
        LoadSelectedOnlyLeader("<%= bSendOnlyLeader&"" %>")
        LoadSelectedInterest("RoleTo", "<%= sToInterest %>");
        /*
        LoadSelectedCat("RoleCC", "<%= sCCCat %>");
        LoadSelectedInterest("RoleCC", "<%= sCCInterest %>");
        LoadSelectedCat("RoleBCC", "<%= sBCCCat %>");
        LoadSelectedInterest("RoleBCC", "<%= sBCCInterest %>");
        */
        LoadSelectedContry("RoleTo", "<%= sCountries %>");
        LoadSelectedAgeGroup("RoleTo", "<%= sAgeGroups %>");
        LoadSelectedGender("RoleTo", "<%= sGenders %>");

        setInterval("resetSession()", keepAliveTime);
    });
</script>

	
<!--#INCLUDE FILE="includes/TemplateFooter.asp"-->
<script language="javascript" src="jsIncludes/validate_date.js"></script>
<script language="javascript" src="jsIncludes/adminfanPage.js"></script>
