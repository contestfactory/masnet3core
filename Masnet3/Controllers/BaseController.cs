﻿using Microsoft.AspNetCore.Mvc;
using System;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;
using Masnet3.Common.Helper;
using Masnet3.Common.Utilities;
using Masnet3.Model.ViewModel;
using MASNET.DataAccess.Model.Settings;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Common.Filters;
using System.Reflection;
using MASNET.DataAccess;
using Masnet3.Business.Repository.Users;
using Masnet3.Business.Repository.Contestants;

namespace Masnet3.Controllers
{
    public class BaseController : Controller
    {
        #region Cto
        public readonly IUserRepository userRepository;
        public readonly IContestantRepository contestantRepository;

        protected AppConfiguration appConfiguration;
        public BaseController(
            IUserRepository userRepository, 
            IContestantRepository contestantRepository,
            IOptions<AppConfiguration> appConfiguration)
        {
            this.userRepository = userRepository;
            this.contestantRepository = contestantRepository;
            this.appConfiguration = appConfiguration.Value;
        }

        public void ProcessLayout(object mapModel)
        {
            var currentUser = SessionUtilities.CurrentUser;
            LoggedUser currentAdmin = (LoggedUser)ViewBag.CurrentAdmin ?? SessionUtilities.SuperAdminAccount;

            var layoutViewModel = new LayoutViewModel();
            layoutViewModel.UseValidate = true;
            layoutViewModel.SiteInformation = SiteInformation;
            layoutViewModel.loggedUser = currentUser;
            layoutViewModel.appConfiguration = this.appConfiguration;

            layoutViewModel.SiteName = base.RouteData.Values["SiteName"].ToString();
            layoutViewModel.EditThemeId = 0;
            if (base.HttpContext.Items["EditThemeID"] != null)
            {
                layoutViewModel.EditThemeId = Convert.ToInt32(base.HttpContext.Items["EditThemeID"].ToString());
            }
            layoutViewModel.ApplicationID = GlobalVariables.SiteInformation.ApplicationID;
            if (base.HttpContext.Items["editSiteId"] != null)
            {
                layoutViewModel.ApplicationID = Convert.ToInt32(base.HttpContext.Items["editSiteId"].ToString());
            }
            layoutViewModel.ContestID = GlobalVariables.SiteInformation.GetContestID();
            if (base.HttpContext.Items["ContestID"] != null)
            {
                layoutViewModel.ContestID = Convert.ToInt32(base.HttpContext.Items["ContestID"].ToString());
            }
            else if (base.HttpContext.Items["editCampaignId"] != null)
            {
                layoutViewModel.ContestID = Convert.ToInt32(base.HttpContext.Items["editCampaignId"].ToString());
            }

            var rqIsCreate = Request.Query["isCreate"];
            layoutViewModel.ShowPreviewButton = string.IsNullOrWhiteSpace(rqIsCreate) && layoutViewModel.EditThemeId > 0 && (Commons.CanEditShowHide() || SessionUtilities.IsUserHasAdminPermission());

            layoutViewModel.Controller = base.RouteData.Values["Controller"].ToString();
            layoutViewModel.Action = base.RouteData.Values["Action"].ToString();
            layoutViewModel.PageName = $"{layoutViewModel.Controller}/{layoutViewModel.Action}".ToLower();
            layoutViewModel.IsLoggedIn = SessionUtilities.IsLoggedIn(currentUser);
            layoutViewModel.IsSuperAdmin = SessionUtilities.IsSuperAdmin(currentAdmin);
            if ((ViewBag.BodyContainer ?? "1") == "0" || (SessionUtilities.IsLoggedIn() && SessionUtilities.IsUserHasAdminPermission()))
            {
                layoutViewModel.MasterContainer = false;
            }

            layoutViewModel.LogoUrl = SiteInformation.LogoUrl;
            if (string.IsNullOrWhiteSpace(layoutViewModel.LogoUrl))
            {
                layoutViewModel.LogoUrl = Url.Action("Index", "Home");
            }

            //Two-FA
            var IsTwoFactor = Commons.Config["Login_TwoFA"];
            if (layoutViewModel.IsLoggedIn
                && currentUser.UserType == (int)Enums.RegistrationUserType.User
                && (IsTwoFactor == "1")
                && !SessionUtilities.IsPassedTwoFactor(IsTwoFactor) && layoutViewModel.PageName != "myaccount/verify")
            {
                Response.Redirect(Url.SZAction("Verify", "MyAccount"), true);
            }

            //
            bool isExpired = false;
            string seForcelogin = HttpContext.Session.Get<string>("forcelogin");
            var reForeLogin = HttpContext.Items["forcelogin"] ?? "";
            var reReref = HttpContext.Items["reref"] ?? "";
            var reShareID = Request.Query["ShareID"];
            var reEditSiteId = Request.Query["editSiteId"];
            var reEditCampaignId = Request.Query["editCampaignId"];
            layoutViewModel.ShareID = reShareID;
            layoutViewModel.EditSiteId = reEditSiteId;
            layoutViewModel.EditCampaignId = reEditCampaignId;

            if (SiteInformation.ApplicationID > 0)
            {
                isExpired = SiteInformation.Application.ExpireDate <= DateTime.Now;
            }
            else if (SiteInformation.CampaignID > 0)
            {
                isExpired = SiteInformation.Campaign.ExpireDate <= DateTime.Now;
            }
            if (SiteInformation.SiteStatus == Enums.SiteStatus.InvalidSite || (isExpired && !(seForcelogin == "1") && reForeLogin.ToString() != "1"))
            {
                Response.Redirect(Url.SZAction("Index", "Error"), true);
            }
            if (SiteInformation.SiteStatus == Enums.SiteStatus.Expired && !SessionUtilities.IsSuperAdmin())
            {
                Response.Redirect(Url.SZAction("Invalid", "Home"), true);
            }

            bool isValid = true;
            if (reForeLogin.ToString() != "1"
                && string.IsNullOrEmpty(reReref.ToString())
                && (SiteInformation.SiteStatus == Enums.SiteStatus.Unpublished
                || SiteInformation.SiteStatus == Enums.SiteStatus.Hidden
                || SiteInformation.SiteStatus == Enums.SiteStatus.Over) && !SiteInformation.IsPreview())
            {
                isValid = false;
                if (seForcelogin == "1" || SessionUtilities.IsSuperAdmin() || !string.IsNullOrWhiteSpace(reShareID.ToString()))
                {
                    isValid = true;
                }
                if (!(seForcelogin == "1") && layoutViewModel.PageName != "publishedcontest/contestantentrydetail" && !SessionUtilities.IsSuperAdmin())
                {
                    switch (layoutViewModel.Controller)
                    {
                        case "home":
                            if (!(layoutViewModel.Action == "countdown" || layoutViewModel.Action == "tips" || layoutViewModel.Action == "invalid" || layoutViewModel.Action == "contestended" || (layoutViewModel.Action == "customerlogin" && reForeLogin.ToString() == "1")))
                            {
                                Response.Redirect(Url.SZAction("Invalid", "Home"), true);
                            }
                            break;
                        case "contestant":
                            if (!(layoutViewModel.Action == "details"))
                            {
                                Response.Redirect(Url.SZAction("Invalid", "Home"), true);
                            }
                            break;
                        case "customer":
                            if (!(layoutViewModel.Action == "register" && reForeLogin.ToString() + "" == "1"))
                            {
                                bool isContestEnd = (SiteInformation.Campaign.Status == (int)Enums.ContestStatuses.Completed || SiteInformation.Campaign.Status == (int)Enums.ContestStatuses.Over || SiteInformation.SiteStatus != (int)Enums.SiteStatus.Valid);
                                if (isContestEnd && Commons.isSyngentaContext(SiteInformation))
                                {
                                    // do nothing
                                }
                                else
                                {
                                    //cause error for AARP if redirect after this.                        
                                    Response.Redirect(Url.SZAction("Invalid", "Home"), true);
                                }
                            }
                            break;
                        case "contest":
                            //Countdown
                            if (SiteInformation.SiteStatus == Enums.SiteStatus.Unpublished)
                            {
                                Response.Redirect(Url.SZAction("Invalid", "Home"), true);
                            } //EndPage
                            else
                            {
                                string ContestEndURL = Commons.Config["ContestEndURL"];
                                string[] arrContestEndValidPages = (Commons.Config["ContestEnd_ValidPages"] ?? "").Split(',');

                                if (!string.IsNullOrEmpty(ContestEndURL) && ContestEndURL.IndexOf(layoutViewModel.PageName, StringComparison.CurrentCultureIgnoreCase) == -1 && !arrContestEndValidPages.Contains(layoutViewModel.PageName))
                                {
                                    if (!(seForcelogin == "1") && layoutViewModel.PageName != "contest/match")
                                    {
                                        Response.Redirect(Url.SZAction("Invalid", "Home"), true);
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            layoutViewModel.IsValid = isValid;
            layoutViewModel.IsDebug = Request.Query["isCreate"] == "1";

            layoutViewModel.ShowLeftMenuCondition = Commons.Config["ShowAdminLeftMenu"].Trim() == "1" && isValid && !SiteInformation.IsPreview() && SessionUtilities.IsUserHasAdminPermission();
            if (layoutViewModel.ShowLeftMenuCondition)
            {
                if (string.IsNullOrWhiteSpace(SessionUtilities.UserPermission) && currentUser.UserType == (int)Enums.RegistrationUserType.User)
                {
                    layoutViewModel.ShowLeftMenuCondition = false;
                }
            }
            layoutViewModel.IsAdminPage = layoutViewModel.ShowLeftMenuCondition;
            if (layoutViewModel.IsAdminPage)
            {
                layoutViewModel.IsAdminPage = ((new string[] { "home/index", "contest/winner", "publishedcontest/judges", "home/media", "news/index", "myaccount/pointleader", "faq/index"
                , "contest/directory", "contest/match", "contestant/details", "contest/round", "contest/details", "contest/submit"
                , "publishedcontest/contestantentrydetail", "myaccount/edit", "myaccount/points", "myaccount/aboutme", "myaccount/changepassword"
                , "myaccount/media", "myaccount/entries", "myaccount/votes", "mystuff/news", "myaccount/reviewmedia", "uploadfile/details",
                "uploadfile/addnew", "uploadfile/submitfee"
                }).Contains(layoutViewModel.PageName)) == false;
            }
            layoutViewModel.IsLogin = SessionUtilities.IsLoggedIn();
            layoutViewModel.IsHeaderContainer = Convert.ToInt32(Commons.Config["HeaderContainerType"]) == 1;
            layoutViewModel.IsAdminSite = layoutViewModel.ShowLeftMenuCondition || SiteInformation.IsPreview();
            layoutViewModel.IsGreaterGoodContext = Commons.isGreaterGoodContext(SiteInformation);
            layoutViewModel.IsSyngentaContext = Commons.isSyngentaContext(SiteInformation);
            layoutViewModel.IsAARP2017Context = Commons.IsAARP2017Context(SiteInformation);
            layoutViewModel.IsFullWidth = layoutViewModel.IsAdminSite || layoutViewModel.IsGreaterGoodContext;
            layoutViewModel.UsingHeaderMenuLinks = this.appConfiguration.UsingHeaderMenuLinks == "1";
            layoutViewModel.DisplayLeftMenu = isValid && layoutViewModel.IsLoggedIn
                                                      && (currentUser.UserType == (int)Enums.RegistrationUserType.SuperAdmin || SessionUtilities.IsUserHasAdminPermission());


            layoutViewModel.IsContestEnd = (SiteInformation.Campaign.Status == (int)Enums.ContestStatuses.Completed || SiteInformation.Campaign.Status == (int)Enums.ContestStatuses.Over || SiteInformation.SiteStatus != (int)Enums.SiteStatus.Valid);
            layoutViewModel.IsPageContestended = Request.GetRawUrl().ToLower().Contains("/home/contestended");
            string itemKey;
            itemKey = !Commons.Config.TryGetValue("Contest_TriviaContest", out itemKey) ? "0" : "1";
            layoutViewModel.TriviaContest = itemKey == "1" && Commons.Config["Contest_TriviaContest"] == "1";

            layoutViewModel.CustomizeButtonConfig = Commons.GetPrivateConfig(layoutViewModel.ApplicationID, layoutViewModel.ContestID, (int)Enums.ConfigKeyID.Contest_ShowCustomizeButton, false, "Contest_ShowCustomizeButton");
            layoutViewModel.PageBannerDelivery = ViewBag.PageBanners ?? new PageBannerDelivery();
            if (layoutViewModel.appConfiguration.ShowBanner == "1" || layoutViewModel.SiteInformation.ApplicationID == 183)
            {

                if (ViewBag.PageBanners == null)
                {
                    layoutViewModel.PageBannerDelivery = new PageBannerDelivery();
                    layoutViewModel.PageBannerDelivery.LoadBannersByPage(layoutViewModel.Controller + "/" + layoutViewModel.Action);
                }
                //PageBanners.IncreaseViews();
            }
            CloneModelObject(mapModel, layoutViewModel);
        }

        private object CloneModelObject(object map, object value)
        {
            Type t = value.GetType();
            PropertyInfo[] properties = t.GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                try
                {
                    map.GetType().GetProperty(pi.Name).SetValue(map, pi.GetValue(value, null), null);
                }
                catch(Exception e)
                {
                    // do nothing
                }
            }
            return map;
        }


        #endregion
        public string SiteName
        {
            get
            {
                return this.RouteData.Values["SiteName"].ToString().ToLower();
            }
        }

        private SiteInformation _SiteInformation = null;
        public SiteInformation SiteInformation
        {
            get
            {
                if (_SiteInformation == null)
                {
                    _SiteInformation = GlobalVariables.SiteInformation;

                }
                return _SiteInformation;
            }
        }

        // Helper
        public string ProcessUrl(string url)
        {
            url = url.Replace("~", Url.Action("temp", "contest", null, RequestUtilities.Scheme()).ToLower().Replace("/contest/temp", ""));
            url = url.Replace("%contestid%", SiteInformation.CampaignID.ToString());
            return url;
        }

        public string ProcessReturnUrl(Registration registration)
        {
            return ProcessReturnUrl(registration.UserID, registration.UserType ?? 1);
        }
        public string ProcessReturnUrl(LoggedUser user)
        {
            return ProcessReturnUrl(user.UserID, user.UserType ?? 1);
        }
        public string ProcessReturnUrl(long UserID, int UserType)
        {
            string scheme = (Request.GetAbsoluteUriSelf().AbsoluteUri.StartsWith("https://", StringComparison.OrdinalIgnoreCase) || Request.Headers["HTTP_X_FORWARDED_PROTO"] == "https") ? "https" : "http";
            var key = string.Format("{0}RedirectAfterLogin", (Enums.RegistrationUserType)UserType);
            string redirectAfterLogin = "";
            var siteInfo = GlobalVariables.SiteInformation;
            bool isContestEnd = (SiteInformation.Campaign.Status == (int)Enums.ContestStatuses.Completed || SiteInformation.Campaign.Status == (int)Enums.ContestStatuses.Over || SiteInformation.SiteStatus != (int)Enums.SiteStatus.Valid);
            bool isSyngentaContext = Commons.isSyngentaContext(SiteInformation);

            if (key.StartsWith("User") && (siteInfo.Campaign.Status.Equals((byte)Enums.ContestStatuses.Open) || siteInfo.Campaign.Status.Equals((byte)Enums.ContestStatuses.OpenActive)))
            {
                redirectAfterLogin = Commons.Config.ContainsKey("UserRedirectAfterLoginForOpen") ? Commons.Config["UserRedirectAfterLoginForOpen"] : "~";
            }
            else
            {
                redirectAfterLogin = Commons.Config.ContainsKey(key) ? Commons.Config[key] : "~";
            }

            if (!string.IsNullOrWhiteSpace(redirectAfterLogin))
            {
                redirectAfterLogin = redirectAfterLogin.ToLower().Trim();
                redirectAfterLogin = redirectAfterLogin.Replace("~", Url.Action("temp", "contest", null, scheme).ToLower().Replace("contest/temp", ""));
                if (SessionUtilities.IsJudge() && redirectAfterLogin.Contains("%contestantid%"))
                {
                    var site = GlobalVariables.SiteInformation;
                    bool isJudge = SessionUtilities.IsJudge();
                    var isAirbnb = Commons.IsAirbnb(site);

                    int vote_MaxJudges = 0;
                    if (Commons.Config.Keys.Contains("Vote_MaxJudges"))
                    {
                        vote_MaxJudges = Convert.ToInt32(string.IsNullOrEmpty(Commons.Config["Vote_MaxJudges"]) ? "0" : Commons.Config["Vote_MaxJudges"]);
                    }
                    var objContestantDetail = this.contestantRepository.GetNextHasNotVoted(
                        SiteInformation.CampaignID, 
                        SessionUtilities.CurrentUser.UserID.ToIntOrDefault(),
                        site.ApplicationID,
                        site.SiteType,
                        isJudge,
                        Commons.Config["UseJudgeIAB"],
                        SessionUtilities.CurrentUser.IAB,
                        vote_MaxJudges,
                        isAirbnb
                        );
                    if (objContestantDetail != null)
                    {
                        redirectAfterLogin = redirectAfterLogin.Replace("%contestid%", objContestantDetail.ContestID.ToString());
                        redirectAfterLogin = redirectAfterLogin.Replace("%contestantid%", objContestantDetail.ContestantID.ToString());
                    }
                    else
                    {
                        redirectAfterLogin = redirectAfterLogin.Replace("%contestantid%", "-1");
                    }
                }

                redirectAfterLogin = redirectAfterLogin.Replace("%userid%", UserID.ToString());
                string editSiteId = Request.Query["editSiteId"];
                if (string.IsNullOrWhiteSpace(editSiteId))
                {
                    editSiteId = SiteInformation.ApplicationID.ToString();
                }
                redirectAfterLogin = redirectAfterLogin.Replace("%editsiteid%", editSiteId);

                string editCampaignId = Request.Query["editCampaignId"];
                if (string.IsNullOrWhiteSpace(editCampaignId))
                {
                    editCampaignId = SiteInformation.CampaignID.ToString();
                }
                redirectAfterLogin = redirectAfterLogin.Replace("%editcampaignid%", editCampaignId);

                redirectAfterLogin = redirectAfterLogin.Replace("%contestid%", SiteInformation.CampaignID.ToString());

            }
            return redirectAfterLogin;
        }

        protected void AutoLogin(Registration registration)
        {
            bool hasEnterPromoCode = false;
            SubmitFeeModel objSubmitFeeModel = null;
            if (HttpContext.Session.Get<string>("HasEnterPromoCode") != null)
            {
                if (HttpContext.Session.Get<SubmitFeeModel>("HasSelectedFee") != null)
                {
                    hasEnterPromoCode = true;
                    objSubmitFeeModel = HttpContext.Session.Get<SubmitFeeModel>("HasSelectedFee");
                }
            }
            HttpContext.Session.Clear();
            if (hasEnterPromoCode)
            {
                HttpContext.Session.Set<SubmitFeeModel>("HasSelectedFee", objSubmitFeeModel);
            }

            SessionUtilities.CurrentUser = new LoggedUser(registration);
            if (SessionUtilities.IsLoggedIn())
            {
                SessionUtilities.UserPermission = this.userRepository.SetPermissionsForCurrentUser(userType: SessionUtilities.CurrentUser.UserType.GetValueOrDefault(), userId: (int)SessionUtilities.CurrentUser.UserID);
            }
            CookieUtilities.Remove(Enums.CookieType.Login.ToString());

        }

    }
}
