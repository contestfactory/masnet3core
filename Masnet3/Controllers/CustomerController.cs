﻿using System;
using System.Net;
using System.Threading.Tasks;
using Masnet3.Common.Helper;
using Masnet3.Business.Repository.Fees;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Masnet3.Common;
using Masnet3.Common.Utilities;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Business.Repository.Contests;
using Masnet3.Model.ViewModel;
using Masnet3.Business.Repository.ExtensionFields;
using Masnet3.Business.Repository.Retailers;
using System.Linq;
using Masnet3.Business.Repository.Global;
using MASNET.DataAccess;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Masnet3.Business.Repository;
using Masnet3.Business.Repository.Layout;
using MASNET.DataAccess.Model.Settings;
using Microsoft.AspNetCore.Http;
using Masnet3.Business.Repository.Users;
using Masnet3.Business.Repository.Promotions;
using Masnet3.Business.Repository.Customer;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using Masnet3.Business.Repository.Periods;
using MASNET.DataAccess.Model.Custom;
using Masnet3.Business.Repository.Contestants;
using Microsoft.AspNetCore.Authorization;

namespace Masnet3.Controllers
{
    public class CustomerController : BaseController
    {
        private readonly IThemeRepository themeRepository;
        private readonly IRepositoryBasic repositoryBasic;
        private readonly IFeeRepository feeRepository;
        private readonly IContestRepository contestRepository;
        private readonly IExtensionFieldRepository extensionFieldRepository;
        private readonly IRetailerRepository retailerRepository;
        private readonly ICountryRepository countryRepository;
        private readonly IPromotionRepository promotionRepository;
        private readonly IRegistrationRepository registrationRepository;
        private readonly IPeriodRepository periodRepository;

        public CustomerController(
            IOptions<AppConfiguration> appConfiguration,
            IThemeRepository themeRepository,
            IRepositoryBasic repositoryBasic,
            IContestRepository contestRepository,
            IExtensionFieldRepository extensionFieldRepository,
            IRetailerRepository retailerRepository,
            ICountryRepository countryRepository,
            IFeeRepository feeRepository,
            IPromotionRepository promotionRepository,
            IRegistrationRepository registrationRepository,
            IPeriodRepository periodRepository,
            IUserRepository userRepository,
            IContestantRepository contestantRepository
            ) : base(userRepository, contestantRepository, appConfiguration)
        {
            this.themeRepository = themeRepository;
            this.repositoryBasic = repositoryBasic;
            this.countryRepository = countryRepository;
            this.extensionFieldRepository = extensionFieldRepository;
            this.contestRepository = contestRepository;
            this.feeRepository = feeRepository;
            this.retailerRepository = retailerRepository;
            this.promotionRepository = promotionRepository;
            this.registrationRepository = registrationRepository;
            this.periodRepository = periodRepository;
        }

        private void RegisterInit(RegisterViewModel model)
        {
            string pageName = "Customer/Register";
            List<DisplayItem> standardFields = CacheUilities.cacheManager.Get<List<DisplayItem>>(Request.GetRawUrl() + "/StandardFields"); ;
            List<CustomFields> customFields = CacheUilities.cacheManager.Get<List<CustomFields>>(Request.GetRawUrl() + "/CustomFields"); ;

            if (standardFields == null || customFields == null)
            {
                bool hasCustomFieldSite = this.repositoryBasic.CheckExistsItem("CustomFieldSite",
                    FilterUilities.GetFilter(SiteInformation.Application.ApplicationID, SiteInformation.CampaignID, 0),
                    new
                    {
                        ApplicationID = SiteInformation.Application.ApplicationID,
                        ContestID = SiteInformation.CampaignID
                    }) == 1;

                string joinClause = Commons.Join(new
                {
                    LeftTable = "DisplayItem",
                    Table = "DisplayItem",
                    IDField = "ID",
                    JoinFields = "PageID,ItemName,LanguageID",
                    editApplicationID = SiteInformation.ApplicationID,
                    editCampaignID = SiteInformation.CampaignID
                });
                var filter = string.Empty;

                if (hasCustomFieldSite)
                {
                    filter = FilterUilities.GetFilter("cfs.ApplicationID", "cfs.ContestID", "TemplateID", SiteInformation.Application.ApplicationID, SiteInformation.CampaignID, 0, "cfs.");
                }
                var GetFields = this.themeRepository.Get_DisplayItem_CustomFields_CustomFieldValues(hasCustomFieldSite,
                    joinClause, filter, pageName, SiteInformation.Application.ApplicationID, SiteInformation.CampaignID, SessionUtilities.CurrentLanguageID);

                standardFields = GetFields.Item1;
                customFields = GetFields.Item2;

                CacheUilities.cacheManager.Set(Request.GetRawUrl() + "/StandardFields", standardFields);
                CacheUilities.cacheManager.Set(Request.GetRawUrl() + "/CustomFields", customFields);

            }

            if (standardFields == null || standardFields.Count == 0)
            {
                standardFields = GlobalVariables.ProfileStandardFields;
            }

            foreach (var standardField in standardFields)
            {
                var elementAttributes = new Dictionary<string, object>();
                standardField.RuleItems = RuleUilities.Deserialize(standardField.Rules, SiteInformation.ApplicationID, SiteInformation.CampaignID);
                foreach (var item in standardField.RuleItems)
                {
                    item.ResourceMessage = string.Format(DbLanguage.GetText(item.ResourceKey), item.Value);
                }

                string fieldRequired = (standardField.IsRequire ?? false) ? "required" : "";
                model.ElementRequires.Add(standardField.ItemName);

                string ruleData = RuleUilities.Serialize(standardField.RuleItems, true);
                if (!string.IsNullOrEmpty(ruleData))
                {
                    model.ElementRules.Add(standardField.ItemName, ruleData);
                }

                elementAttributes.Add("Label", string.IsNullOrWhiteSpace(standardField.Description) ? standardField.ItemName : standardField.Description);
                elementAttributes.Add("ID", standardField.ID);
                elementAttributes.Add("HelpText", standardField.HelpText);
                elementAttributes.Add("SiteName", SiteInformation.ContextName.ToLower());
                elementAttributes.Add("PageID", standardField.PageID);

                if (standardField.RuleItems.Count(a => a.RuleKey.ToLower() == "required") > 0 || standardField.RuleItems.Count(a => a.RuleKey.ToLower() == "htmlrequired") > 0)
                {
                    elementAttributes.Add("IsRequired", true);
                }
                else
                {
                    elementAttributes.Add("IsRequired", standardField.IsRequire.GetValueOrDefault());
                }

                if (standardField.RuleItems.Count(a => a.RuleKey.ToLower() == "maxlength") > 0)
                {
                    elementAttributes.Add("MaxLength", int.Parse(standardField.RuleItems.FirstOrDefault(a => a.RuleKey.ToLower() == "maxlength").Value));
                }
                else if (standardField.MaxLength.GetValueOrDefault() > 0)
                {
                    elementAttributes.Add("MaxLength", standardField.MaxLength);
                }

                if (standardField.ItemName.ToLower().Contains("registration_field"))
                {
                    elementAttributes.Add("ItemName", standardField.ItemName);
                }

                model.StandardElements.Add(standardField.ItemName, elementAttributes);
            }

            model.keyStandardElements.FirstName = model.StandardElements.FirstOrDefault(x => x.Key == "FirstName");
            model.keyStandardElements.LastName = model.StandardElements.FirstOrDefault(x => x.Key == "LastName");
            model.keyStandardElements.City = model.StandardElements.FirstOrDefault(x => x.Key == "City");
            model.keyStandardElements.Provine = model.StandardElements.FirstOrDefault(x => x.Key == "Provine");
            model.keyStandardElements.Zip = model.StandardElements.FirstOrDefault(x => x.Key == "Zip");
            model.keyStandardElements.EmailAddress = model.StandardElements.FirstOrDefault(x => x.Key == "Email");
            model.keyStandardElements.Phone = model.StandardElements.FirstOrDefault(x => x.Key == "Registration_Field_5");
            model.keyStandardElements.Retailer = model.StandardElements.FirstOrDefault(x => x.Key.ToLower() == "registration_field_4" && x.Value["PageID"].ToInt() == 80);
            model.keyStandardElements.RetailerLocation = model.StandardElements.FirstOrDefault(x => x.Key.ToLower() == "registration_field_3" && x.Value["PageID"].ToInt() == 80);

            model.CustomFields = customFields;
            model.StandardFields = standardFields;

            //ViewBag
            string cachingKeyCountry = Request.GetRawUrl() + "/Countries";
            List<SelectListItem> countries = CacheUilities.cacheManager.Get<List<SelectListItem>>(cachingKeyCountry);
            if (countries == null || countries.Count == 0)
            {
                countries = GlobalVariables.Countries
                                      .Select(c => new SelectListItem()
                                      {
                                          Text = c.GetType().GetProperty(String.Format("CountryName_{0}", SessionUtilities.CurrentLanguageID)).GetValue(c, null).ToString(), //c.CountryName,
                                          Value = c.CountryID.ToString()
                                      }).ToList();
                if (countries.Count == 0)
                {
                    countries = GlobalVariables.Countries
                                      .Select(c => new SelectListItem()
                                      {
                                          Text = c.GetType().GetProperty(String.Format("CountryName_{0}", SessionUtilities.CurrentLanguageID)).GetValue(c, null).ToString(), //c.CountryName,
                                          Value = c.CountryID.ToString()
                                      }).ToList();
                }
                CacheUilities.cacheManager.Set(cachingKeyCountry, countries);
            }

            string selectedCountries = "";

            if (Commons.IsCollette100DaysContext(SiteInformation))
            {
                selectedCountries = ",14,39,230,231,";
            }
            else if (SiteInformation.ContextName.ToLower() == "knoxchase" || SiteInformation.ContextName.Contains("collette"))
            {
                selectedCountries = ",39,231,";
            }
            if (!string.IsNullOrEmpty(selectedCountries))
            {
                countries = countries.Where(c => (selectedCountries).Contains("," + c.Value.Trim() + ",")).ToList();
            }

            ViewBag.CountryList = countries;


            List<StateOfUSA> states = null;
            switch ((Enums.RestrictStateType)model.ExtensionField.RestrictStateType.GetValueOrDefault())
            {
                case Enums.RestrictStateType.AllStates:
                    states = GlobalVariables.States;
                    break;
                case Enums.RestrictStateType.ContiguosAllStates:
                    states = GlobalVariables.States.Where(s => s.Contiguous == true).ToList();
                    break;
                case Enums.RestrictStateType.SpecificStates:
                    states = this.countryRepository.GetStateOfUSAInStateID(string.IsNullOrWhiteSpace(model.ExtensionField.RestrictedStates) ? "0" : model.ExtensionField.RestrictedStates).ToList();
                    break;
                default:
                    states = GlobalVariables.States;
                    break;
            }

            ViewBag.States = states;
            ViewBag.StateList = states.Select(s => new SelectListItem()
            {
                Text = s.GetType().GetProperty(String.Format("State_{0}", SessionUtilities.CurrentLanguageID)).GetValue(s, null).ToString(), //System.Threading.Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(s.State.ToLower()),
                Value = s.StateID.ToString()
            }).ToList();
        }

        public async Task<IActionResult> Register(byte userType = 1, string returnURL = "", string forcelogin = "")
        {
            //NOT allow agent LIVE
            if (SiteInformation.SiteType == Enums.SiteType.Main && this.appConfiguration.SiteInfo_Type == "live")
            {
                Response.Redirect("../../400.html", true);
            }

            var scheme = HttpContext.Request.Scheme;
            var url = this.appConfiguration.AARP_LoginURL;
            var aarpServiceURL = this.appConfiguration.AARP_ServiceURL;

            var quizExtraSetting = Commons.Config["Quiz_ExtraSettings"];
            HttpContext.Session.SetStringObject("ExtraSetting", quizExtraSetting);

            SiteInformation site = SiteInformation;
            bool validLogin = site.SiteStatus == Enums.SiteStatus.Valid || true;

            var AARP_API_Version = Commons.Config["AARP_API_Version"];
            if (AARP_API_Version == "2")
            {
                string redirectUrl = Url.Action("registercallback", "customer", new { returnURL = returnURL }, scheme);
                AARPAPI aarpAPI = new AARPAPI(redirectUrl);
                return Redirect(aarpAPI.getAARPLoginUrl());
            }

            if ((AARP_API_Version == "1" || site.ContextName.Contains("aarp")) && validLogin && string.IsNullOrEmpty(forcelogin) && !string.IsNullOrEmpty(aarpServiceURL))
            {
                //getToken
                string urlToken = aarpServiceURL + "/providers/ContestFactory/token";
                string Provider_token = "78e017cf-5573-4680-b173-11067ef8f54a";
                WebHeaderCollection headers = new WebHeaderCollection();
                headers["Authentication"] = "PF0chtKKAbq5NlGSO93VUAXfumunPkOr7dA7+sf3fk+nF4RYorz6gO5zoJ12RQJqfUPTfFOU01lQFrUrArIgw614uIeCnxdSp1RhWwIA/BrhSJ04itW+QGX3Y5ygryJxCtSM750nFhgIF2Fewk8eqWqT8Bez3qAjSMvlY1RcXxs=";
                headers["Signature"] = "PevRIcM7GFJxja0yHcG/XJY9/uuFPLB4pT6sh+c3c9nzlfGSnObGwOqfpbqrk1c+GVaTUj/RPwT/OQmUAKjJZgneyjR1wJ8RglGWBcwuXALyW8qsTQXWMVQYOksJZdXyLOk0UxDyIuUypl2usONgOhYPtZXJxhs/AORseexStpw=";
                using (var response = RequestUtilities.MakeRequest(url, "POST", headers))
                {
                    Provider_token = response.Headers["Provider_token"];
                }
                SessionUtilities.CurrentUser.Token = Provider_token;

                //get loginFromProvider
                string referer = Url.Action("RegisterCallback", "customer", new { returnURL = returnURL }, scheme);
                url = aarpServiceURL + "/users/session?referrer=" + WebUtility.UrlEncode(referer);
                headers["Authentication"] = Provider_token;
                headers["Location"] = referer;
                using (var response = RequestUtilities.MakeRequest(url, "GET", headers))
                {
                    var location = response.Headers["Location"];
                    location = Commons.AddParameter(location, "custom", "DreamBuilders");
                    if (HttpContext.Items["isLogin"] != null)
                    {
                        if (HttpContext.Items["isLogin"].ToString() != "1")
                        {
                            location = location.Replace("signOn.action", "register/index.action", StringComparison.CurrentCultureIgnoreCase);
                        }
                    }
                    Response.Redirect(location, true);
                }
            }

            string customView = Commons.GetCustomView(SiteInformation.Theme.ID, (int)Enums.PageID.RegisterPage);

            ViewBag.ValidationKey = HttpContext.Items["code"];
            ViewBag.ShareId = HttpContext.Items["ShareId"];
            ViewBag.ReferId = HttpContext.Items["ReferId"];

            if (SessionUtilities.IsLoggedIn() && Commons.IsPreview == false && string.IsNullOrEmpty(HttpContext.Items["ShareID"].ToString()))
            {
                if (!customView.ToLower().Contains("cornerstore"))
                {
                    if (SessionUtilities.IsUserHasAdminPermission())
                    {
                        Response.Redirect(ProcessUrl(Url.Action("AdminRedirect", "Home")), true);
                    }
                    else
                    {
                        // do not thing
                    }
                }
            }

            RegisterViewModel model = new RegisterViewModel
            {
                ReturnUrl = returnURL,
                ShowTrackingCode = (Commons.Config["TrackingCode_Enable"] ?? "0") == "1",
                ShowLoginForm = Commons.Config["Registration_ShowLoginForm"] == "1",
                HasBeginForm = !string.IsNullOrEmpty(Commons.Config["Register_BeginForm"]),
                RegistrationAutoConfirm = Commons.Config["Registration_AutoConfirm"],
                varietyArray = Commons.GetSyngentaVarieties().Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem { Text = x, Value = x }),
                RetailerNameArray = this.retailerRepository.GetRetailerNameAutocomplete(SiteInformation.CampaignID),
                RetailerCityArray = this.retailerRepository.GetRetailerCityAutocomplete(SiteInformation.CampaignID, null),
                ProvinceArray = this.countryRepository.GetStateOfUSA().Select(x => new Microsoft.AspNetCore.Mvc.Rendering.SelectListItem { Text = x.StateCode, Value = x.StateID.ToString() }),
                DisplayLogin = true,
            };
            
            if (SiteInformation.CampaignID > 0 && SiteInformation.Campaign.TemplateTypeID == (int)Enums.ContestTemplateType.Sweepstakes)
            {
                model.DisplayLogin = SiteInformation.Campaign.IsMultiSubmissions;
            }
            model.RetailerLocation.retailerLocation = this.retailerRepository.GetRetailerLocations(SiteInformation.CampaignID);

            if (!(userType == (byte)Enums.RegistrationUserType.Customer || userType == (byte)Enums.RegistrationUserType.User))
            {
                userType = (byte)Enums.RegistrationUserType.User;
            }
            model.Registration.UserType = userType;
            //ExtensionField
            model.ExtensionField = ExtensionUilities.GetExtensionField(extensionFieldRepository, siteInfo: site);
            ViewData["MinAge"] = model.ExtensionField.MinAge.GetValueOrDefault();
            ViewData["registerEmail"] = HttpContext.Items["registerEmail"] ?? "";
            if (Commons.isSyngentaContext(site) || Commons.isAvonContext(site))
            {
                ViewBag.Retailers = retailerRepository.GetRetailers();
            }

            RegisterInit(model);
            // pas model to layout
            ProcessLayout(model);

            if (Commons.Config["SubmitContestant_ShowRegisterStep"] == "1")
            {
                var contestId = Convert.ToInt32(Commons.Config["DRTV_DefaultContestID"]);
                var lstFee = await this.feeRepository.GetFeesByContestID(contestId);
                ViewBag.DefaultContest = this.contestRepository.GetContestByContestId(contestId);
                ViewBag.ListFee = lstFee;
            }

            if (SiteInformation.Theme.ThemeName == "rantchamp")
            {
                return View("Register_RantChamp", model);
            }
            else if (SiteInformation.ContextName.ToLower().Contains("collette"))
            {
                return View("Register_Collette", model);
            }
            if (Commons.Config["Register_Rules"] == "1")
            {
                customView = "RegisterWithRules";
            }
            if (!string.IsNullOrWhiteSpace(customView))
            {
                return View(customView, model);
            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Register([FromBody]Registration registration, string returnURL = "", IFormFile file = null, int returnFormat = 0, string userTypeRequest = "")
        {
            string message = "";
            string superVisorEmail = "";
            string registration_AutoConfirm = Commons.Config["Registration_AutoConfirm"];
            Response.Headers.Append("Access-Control-Allow-Origin", "*");
            IntegrateResponse resultData = new IntegrateResponse() { Result = "ok", Message = "", Data = "" };
            var site = SiteInformation;

            #region //Secured Register
            var registerSecured = Commons.Config["Register_Secured"];
            var promoCode = Commons.isAvonContext(SiteInformation) ? registration.Registration_Field_3 : Request.Query["code"].ToString();
            registration.Code = promoCode;
            var repReg = new Registration();

            var promotionCode = new PromotionalCode
            {
                Code = registration.Code,
                ApplicationId = registration.ApplicationID,
                CampaignId = registration.ContestID
            };
            bool usePromo = !string.IsNullOrEmpty(promotionCode.Code);
            bool useRepID = Commons.Config.Keys.Contains("Register_UseRepID") && Commons.Config["Register_UseRepID"] == "1";

            if (useRepID)
            {
                resultData = IsValidRepID(registration, out repReg);
                if (resultData.Result.ToString() != "ok")
                    return Json(resultData);
            }

            if (usePromo)
            {
                resultData = IsValidPromo(ref promotionCode);
                if (resultData.Result.ToString() != "ok")
                    return Json(resultData);
            }

            if (repReg.UserID > 0 && useRepID)
            {
                var login = new LoginViewModel()
                {
                    username = registration.Email,
                    password = registration.Password,
                    Code = promoCode,
                    RepID = registration.RepID,
                    ContestID = site.CampaignID,
                    Application = site.ApplicationID,
                    PrizeID = promotionCode.PrizeId
                };
                if (LoginProcessMasNet3(registration.Email, registration.Password, login))
                {
                    HttpContext.Session.Set<PromotionalCode>("promoCode", promotionCode);
                    resultData.RedirectUrl = login.ReturnUrl;
                    resultData.Result = "ok";

                    return Json(resultData);
                }
                else
                {
                    resultData.Result = "error";
                    resultData.ErrorType = "RepID";
                    resultData.Message = login.Message;
                    return Json(resultData);
                }
            }

            //End Secured Register
            #endregion

            #region // loadtest
            //bool isLoadTest = LoadTestHelper.IsLoadTest(Request);
            //if (isLoadTest)
            //{
            //    if (System.Web.HttpContext.Current.Application["id"] == null)
            //        System.Web.HttpContext.Current.Application["id"] = 0;
            //    var index = (int)System.Web.HttpContext.Current.Application["id"];
            //    System.Web.HttpContext.Current.Application["id"] = index + 1;
            //    var oldEmail = registration.Email;
            //    registration.Email = registration.Email + index;
            //    if (registration.Email.Length > 255)
            //    {
            //        System.Web.HttpContext.Current.Application["id"] = 0;
            //        index = (int)System.Web.HttpContext.Current.Application["id"];
            //        registration.Email = oldEmail + index;
            //    }
            //}
            //end loadtest
            #endregion

            // Set RegistrationType
            registration.RegistrationType = registration.RegistrationType ?? (byte)Enums.RegistrationType.Site;
            if (Enum.IsDefined(typeof(Enums.RegistrationType), registration.RegistrationType) == false)
            {
                registration.RegistrationType = registration.RegistrationType ?? (int)Enums.RegistrationType.Site;
            }

            var categoryId = 0;
            if (Commons.ColletteFinal(SiteInformation) && registration.UserType == (byte)Enums.RegistrationUserType.Judge)
            {
                var contest = this.contestRepository.GetContestByContestId(site.CampaignID);
                if (contest.CategoryID != null) categoryId = (int)contest.CategoryID;
            }
            else
            {
                if (!(registration.UserType == (byte)Enums.RegistrationUserType.User || registration.UserType == (byte)Enums.RegistrationUserType.Customer))
                {
                    registration.UserType = (byte)Enums.RegistrationUserType.User;
                }

                if (SiteInformation.ApplicationID == 0 && SiteInformation.CampaignID == 0)
                {
                    registration.UserType = (byte)Enums.RegistrationUserType.Customer;
                }
                else
                {
                    if (registration.RegistrationType == (byte)Enums.RegistrationType.Mongo)
                    {
                        registration.UserType = (byte)Enums.RegistrationUserType.Customer;
                    }
                    else
                    {
                        registration.UserType = (byte)Enums.RegistrationUserType.User;
                    }
                }
            }

            string iPAddress = Request.Query["REMOTE_ADDR"];
            if (Request.IsLocal())
            {
                iPAddress = "127.0.0.1";
            }
            registration.IPAddress = iPAddress;

            Registration currentRegistration = null;
            if (registration.ApplicationID == 0)
            {
                registration.ApplicationID = SiteInformation.Application.ApplicationID;
                registration.ContestID = SiteInformation.CampaignID;
                //registration.Email = registration.Email;
                currentRegistration = this.registrationRepository.GetRegistrationByAppContestEmail(registration.ApplicationID, registration.ContestID, registration.Email);
            }

            string emailFromAddress = Commons.Config["Email_FromAddress"];
            if (string.IsNullOrWhiteSpace(registration.Username))
            {
                registration.Username = registration.Email;
            }
            bool isEdit;
            if (currentRegistration == null || currentRegistration.UserID == 0)
            {
                if (!(this.registrationRepository.EmailExists(registration.Email, 0, SiteInformation.ApplicationID, SiteInformation.CampaignID)))
                {
                    if (returnFormat == (byte)Enums.ReturnFormat.Json)
                    {
                        resultData.Result = "fail";
                        resultData.Message = DbLanguage.GetText("gValidationEmailExisted");
                        resultData.Data = new { UserID = registration.UserID, Username = registration.Username, MongoID = registration.MongoID };
                        return Ok(resultData);
                    }
                    if (returnFormat == (byte)Enums.ReturnFormat.JsonP)
                    {
                        resultData.Result = "fail";
                        resultData.Message = DbLanguage.GetText("gValidationEmailExisted");
                        resultData.Data = new { UserID = registration.UserID, Username = registration.Username, MongoID = registration.MongoID };
                        var sb = new StringBuilder();
                        sb.Append(Request.Query["callback"] + "(");
                        sb.Append(JsonConvert.SerializeObject(resultData));
                        sb.Append(");");
                        return Ok(sb);
                    }
                    else
                        return Content(DbLanguage.GetText("gValidationEmailExisted"));
                }

                if (file != null)
                {
                    registration.Photo = string.Format(@"{0}/{1}/Photo/{2}", site.MediaFolder, registration.Email, file.FileName);
                }

                if (Commons.isSyngentaContext(SiteInformation))
                {
                    registration.RetailerID = this.retailerRepository.GetRetailerId(
                            registration.Registration_Field_4,
                            registration.Registration_Field_3);
                }

                var appConfig = Commons.GetConfigFromAppsettings();

                registration.ApplicationID = site.Application.ApplicationID;
                registration.ContestID = site.CampaignID;
                registration.LanguageID = site.LanguageID;
                registration.UserType = registration.UserType ?? (int)Enums.RegistrationUserType.User;
                registration.RoleID = registration.UserType == (int)Enums.RegistrationUserType.Customer ? appConfig.AgentRoleID : GlobalVariables.DefaultRoleID;

                var registrationAutoConfirm = Commons.Config["Registration_AutoConfirm"];
                registration = this.registrationRepository.CreateRegistration(registration, registrationAutoConfirm, appConfig.IsBigData == "1");

                if (usePromo)
                {
                    HttpContext.Session.Set<PromotionalCode>("promoCode", promotionCode);
                }

                if (file != null)
                {
                    var folder = Path.Combine(Commons.Config["VirtualFolderPath"], Commons.UploadFolder, site.MediaFolder, registration.Email, "Photo");
                    var photo = string.Format(@"{0}/{1}/{2}/Photo/{3}", Commons.UploadFolder, site.MediaFolder, registration.Email, file.FileName);
                    var path = string.Format(@"{0}\{1}", folder, file.FileName);

                    if (!System.IO.Directory.Exists(folder))
                        System.IO.Directory.CreateDirectory(folder);
                    try
                    {
                        if (file != null)
                        {
                            using (var fileStream = new FileStream(path, FileMode.Create))
                            {
                                file.CopyTo(fileStream);
                            }
                            
                        }
                    }
                    catch (Exception ex)
                    {
                        //LogHelper.WriteLog(ex.Message);
                    }

                    //FlvTool objTool = new FlvTool();
                    //objTool.ProcessThumbnail(path);

                    //var converterBL = new FlvConverterBL();
                    //converterBL.InsertS3(new TblQueue_FLVConvert() { FilePath = Commons.RelativeS3Path(path) },
                    //    new UpdateCondition()
                    //    {
                    //        TableName = "Registration",
                    //        UpdateFields = "Photo",
                    //        IDField = "userID",
                    //        IDValue = registration.UserID.ToString()
                    //    }, null, site.ApplicationID, site.CampaignID
                    //);

                }
                // Update Used = 1 For PromotionalCode
                if (usePromo)
                {
                    this.promotionRepository.UpdateUsed(promotionCode);
                }

                isEdit = false;

                if (registration.RetailerID.GetValueOrDefault() > 0 && registration_AutoConfirm == "0")
                {
                    message = SendValidationEmailToSupervisor(registration, superVisorEmail);
                    this.registrationRepository.SendAdminRegistration(registration, site.ApplicationID, site.CampaignID, site.LanguageID, emailFromAddress, Commons.GetUseType());
                    resultData.Message = message; resultData.ReturnUrl = returnURL;
                    resultData.Data = new { registration.UserID };
                    return Json(resultData);
                }
            }
            else
            {
                isEdit = true;
            }

            ////After Register
            var activeperiod = PeriodUilities.GetActivePeriod();
            var sweepstake = SweepStakeUilities.GetByAction((int)Enums.Action.Registration);
            var isJudge = registration.UserType == (byte)Enums.RegistrationUserType.Judge;
            if (!isEdit)
            {
                //Register points
                var tran = new AccountDetail()
                {
                    UserID = registration.UserID,
                    ActionID = (int)Enums.Action.Registration,
                    TransactionDate = DateTime.Now,
                    Point = isJudge ? 0 : sweepstake.Point,
                    Amount = isJudge ? 0 : sweepstake.Amount,
                    ReferID = activeperiod.PeriodID,
                    ReferType = (byte)Enums.SweepType.Period,
                    Description = string.Format("{0} Registration", registration.Username),
                    ContestID = site.CampaignID,
                    ContestantID = null,
                    Transtype = (byte)Enums.TranType.Credit,
                    ApplicationID = site.ApplicationID,
                    DescriptionSecond = "",
                    TemplateToken = string.Empty,
                    Status = 1,
                    Code = registration.Code,
                    RepID = registration.RepID,
                    PrizeID = promotionCode.PrizeId
                };

                tran = this.userRepository.Insert(accountdetail: tran);
                //Register points

                //Share points                   
                int shareId = 0;
                if (HttpContext.Session.Get<string>("ShareID") != null)
                {
                    int.TryParse(HttpContext.Session.Get<string>("ShareID"), out shareId);
                }
                if (shareId > 0)
                {
                    var tranInfo = this.userRepository.GetByAccountDetailTranId(shareId);
                    if (tranInfo != null)
                    {
                        if (Commons.CanUpdateShare(site, tranInfo))
                        {
                            //Update Share                            
                            sweepstake = SweepStakeUilities.GetSweepstakeDirect(tranInfo.ActionID, site.ApplicationID, tranInfo.ContestID);
                            if (sweepstake != null && sweepstake.PointType == (int)Enums.PointType.FriendReturnAndRegister)
                            {
                                tranInfo.Status = 1;
                                tranInfo.pt = (int)Enums.PointType.FriendReturnAndRegister;
                                this.userRepository.UpdateAccountDetailStatus(tranInfo, sweepstake);
                            }
                        }

                        int newActionID = ActionUitilites.GetShareRegisterActionID(tranInfo.ActionID);
                        sweepstake = SweepStakeUilities.GetSweepstakeDirect(newActionID, site.ApplicationID, tran.ContestID);
                        if (sweepstake != null)
                        {
                            tran = new AccountDetail()
                            {
                                UserID = tranInfo.UserID,
                                ActionID = newActionID,
                                TransactionDate = DateTime.Now,
                                Point = sweepstake.Point,
                                Amount = sweepstake.Amount,
                                ReferID = activeperiod.PeriodID,
                                ReferType = (byte)Enums.SweepType.Period,
                                Description = string.Format("{0} : {1} Share Registration", registration.Username, (Enums.Action)tranInfo.ActionID),
                                ContestID = site.CampaignID,
                                ContestantID = null,
                                Transtype = (byte)Enums.TranType.Credit,
                                ApplicationID = site.ApplicationID,
                                DescriptionSecond = "",
                                TemplateToken = string.Empty,
                                Status = 1,
                                SourceID = tranInfo.TranID
                            };

                            int Frequency = 0;
                            int PointFrequencyType = 0;
                            if (sweepstake.ActionType == (int)Enums.ActionType.Share)
                            {
                                Frequency = Convert.ToInt32(Commons.Config["SharingFrequency"]);
                                PointFrequencyType = Convert.ToInt32(Commons.Config["PointFrequencyType"]);
                            }
                            else
                            {
                                Frequency = sweepstake.PointFrequency;
                            }
                            tran = this.userRepository.UpdateAccountDetailAllowPoint(tran, sweepstake, Frequency, PointFrequencyType);
                            this.userRepository.Insert(accountdetail: tran);
                        }

                        HttpContext.Session.Remove("ShareID");
                    }
                }
                //Share points
            }

            //Category member
            if (categoryId > 0)
            {
                this.registrationRepository.InsertMemberCat((int)registration.UserID, categoryId, 1);
            }
            //Category member

            //Admin SEM
            this.registrationRepository.SendAdminRegistration(registration, site.ApplicationID, site.CampaignID, site.LanguageID, emailFromAddress, Commons.GetUseType());

            //Result
            if (registration.Under13 == "1")
                registration_AutoConfirm = "0";

            if (returnFormat == (byte)Enums.ReturnFormat.Json)
            {
                string redirectAfterLogin = "";
                if (registration_AutoConfirm == "1")
                {
                    redirectAfterLogin = ProcessReturnUrl(registration);
                }

                registration.Token = Guid.NewGuid().ToString().Replace("-", "") + ((registration.UserID > 0) ? registration.UserID.ToString() : Commons.PasswordGenerator(3, false));
                registration.ExpiredDate = DateTime.Now.AddMinutes(GlobalVariables.SessionTimeout);
                this.registrationRepository.CreateRegistrationToken(registration);

                resultData.Result = "ok";
                resultData.Message = "ok";
                resultData.Data = new
                {
                    redirectAfterLogin,
                    ReturnUrl = redirectAfterLogin,
                    UserID = registration.UserID,
                    Username = registration.Username,
                    MongoID = registration.MongoID,
                    Token = registration.Token
                };
                return Ok(resultData);
            }

            if (returnFormat == (int)Enums.ReturnFormat.JsonP)
            {
                if (registration_AutoConfirm == "1")
                {
                    AutoLogin(registration);
                }
                string redirectAfterLogin = ProcessReturnUrl(registration);

                resultData.Result = "ok";
                resultData.Message = "ok";
                resultData.Data = new { redirectAfterLogin, UserID = registration.UserID, Username = registration.Username, MongoID = registration.MongoID };
                var sb = new StringBuilder();
                sb.Append(Request.Query["callback"] + "(");
                sb.Append(JsonConvert.SerializeObject(resultData));
                sb.Append(");");
                return Ok(sb);
            }

            //User SEM
            string activationLink = EmailUtilities.ExternalURL(Url.Action("Activation", "Customer", new { userName = registration.Username, validationKey = registration.ValidationKey }, Request.Scheme));
            CookieUtilities.Remove(string.Format("shareid_{0}_{1}", registration.ApplicationID ?? 0, registration.ContestID ?? 0));
            this.registrationRepository.SendActivationEmail(registration, site.ApplicationID, site.CampaignID, site.LanguageID, emailFromAddress,
                Commons.Config["CustomerService"], Commons.Config["UnsubscribeEmail"], Commons.GetUseType(),
                activationLink
                , Commons.Config["Registration_AutoConfirm"]);
            //User SEM

            //Auto login
            if (registration_AutoConfirm == "1")
            {
                SessionUtilities.CurrentUser = new LoggedUser(registration);
                SessionUtilities.UserPermission = this.userRepository.SetPermissionsForCurrentUser();

                string redirectAfterLogin = "";

                if (SiteInformation.ContextName.ToLower().Contains("collette100days") || Commons.isAvonContext(SiteInformation))
                {
                    var redirectAfteRegister = Commons.GetConfig(string.Format("{0}_RedirectAfteRegister", Enums.RegistrationUserType.User.ToString()));
                    redirectAfterLogin = GetRedirectUrl(redirectAfteRegister);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(returnURL))
                    {
                        redirectAfterLogin = returnURL;
                    }
                    else
                    {
                        redirectAfterLogin = ProcessReturnUrl(registration);
                    }
                }
                resultData.RedirectUrl = redirectAfterLogin;
            }
            //Auto login
            resultData.Data = new { registration.UserID };
            return Ok(resultData);
        }

        [HttpPost]
        public ActionResult RegisterBookingIntention([FromBody]List<BookingIntentionViewModel> model)
        {
            foreach (var item in model)
            {
                this.registrationRepository.CreateBookingIntention(new BookingIntention
                {
                    Amount = item.Amount,
                    UserId = item.UserId,
                    Variety = item.Variety
                });
            }
            return Json("OK");
        }

        public ActionResult RegisterConfirmation()
        {
            //Session ends
            bool autoConfirm = Commons.Config["Registration_AutoConfirm"] == "1";
            if (autoConfirm && !SessionUtilities.IsLoggedIn())
            {
                return RedirectToAction("Index", "Home");
            }

            var model = new LayoutViewModel();
            ProcessLayout(model);

            if (SessionUtilities.IsLoggedIn() && !SiteInformation.IsPreview())
            {
                string customView = Commons.GetCustomView(SiteInformation.Theme.ID, (int)Enums.PageID.RegisterThankPage);

                var promotionalCode = HttpContext.Session.Get<PromotionalCode>("promoCode");
                if (promotionalCode != null)
                {
                    ViewBag.PrizeName = SessionUtilities.CurrentLanguageID == (int)Enums.Language.English ? promotionalCode.Description : promotionalCode.Description_3;
                    ViewBag.Photo = promotionalCode.Photo;
                }

                if (!string.IsNullOrWhiteSpace(customView))
                {
                    return View(customView, model);
                }
                else if (SiteInformation.ContextName.ToLower().Contains("collette"))
                {
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }

        private bool LoginProcessMasNet3(string username, string password, LoginViewModel model)
        {
            ViewBag.ErrorMessage = string.Empty;
            Registration registration = new Registration();
            bool allowAnonymous = Commons.Config["Login_AllowAnonymous"] == "1";

            Enums.LoginStatus loginStatus = this.registrationRepository.LoginMasNet3(username, password, SiteInformation.Application.ApplicationID, SiteInformation.CampaignID, out registration, allowAnonymous, rememberLogin: model.RememberMeLogin);

            switch (loginStatus)
            {
                case Enums.LoginStatus.InvalidUserNameOrPassword:
                    ViewBag.ErrorMessage = DbLanguage.GetText("gMessage_InvalidUserName");
                    return false;
                case Enums.LoginStatus.Blocked:
                    ViewBag.ErrorMessage = DbLanguage.GetText("gMessage_AccountBlocked");
                    return false;
                case Enums.LoginStatus.Disabled:
                    ViewBag.ErrorMessage = DbLanguage.GetText("gMessage_AccountDeleted");
                    return false;
                case Enums.LoginStatus.Pending:
                    ViewBag.ErrorMessage = DbLanguage.GetText("gMessage_ActivateAccount");
                    return false;
                case Enums.LoginStatus.RequiredLogin:
                    ViewBag.ErrorMessage = "RequiredLogin";
                    return false;
                case Enums.LoginStatus.Success:

                    if(model.RememberMeLogin)
                    {
                        if (string.IsNullOrEmpty(registration.Token))
                        {
                            //registration.Token = Commons.PasswordGenerator(16, false);
                            registration.Token = Guid.NewGuid().ToString().Replace("-", "") + ((registration.UserID > 0) ? registration.UserID.ToString() : Commons.PasswordGenerator(3, false));
                            registration.ExpiredDate = DateTime.Now.AddMinutes(GlobalVariables.SessionTimeout);
                            this.registrationRepository.CreateRegistrationToken(registration);
                        }
                        SessionUtilities.SetUserTokenInCookie(registration.Token);
                    }

                    AutoLogin(registration);

                    if (string.IsNullOrWhiteSpace(model.ReturnUrl) || registration.UserType == (int)Enums.RegistrationUserType.Judge || registration.UserType == (int)Enums.RegistrationUserType.SuperAdmin)
                    {
                        string redirectAfterLogin = ProcessReturnUrl(registration);
                        model.ReturnUrl = redirectAfterLogin;
                    }

                    if (SiteInformation.CampaignID > 0 && SiteInformation.Campaign.TemplateTypeID == (int)Enums.ContestTemplateType.Sweepstakes)
                    {

                        if (!string.IsNullOrEmpty(model.Code))
                        {
                            this.promotionRepository.UpdateUsed(new PromotionalCode() { Code = model.Code, ApplicationId = SiteInformation.ApplicationID, CampaignId = SiteInformation.CampaignID });
                        }
                        this.userRepository.InsertAccountDetail(new AccountDetail()
                        {
                            UserID = registration.UserID,
                            ActionID = (int)Enums.Action.Login,
                            TransactionDate = DateTime.Now,
                            Point = 0,
                            Amount = 0,
                            Transtype = (int)Enums.TranType.Credit,
                            ApplicationID = SiteInformation.ApplicationID,
                            CampaignID = SiteInformation.CampaignID,
                            ContestID = SiteInformation.CampaignID,
                            Code = model.Code,
                            RepID = model.RepID,
                            PrizeID = model.PrizeID
                        });
                    }

                    var appConfig = Commons.GetConfigFromAppsettings();
                    string integrated = appConfig.Integrated;
                    string integrateGetToken = appConfig.IntegrateGetToken;
                    SessionUtilities.CurrentUser.IntergratedToken = "";

                    if (!string.IsNullOrWhiteSpace(integrated) && !string.IsNullOrWhiteSpace(integrateGetToken) && integrated.Trim() == "1")
                    {
                        IntegrationHelper.GetToken(registration);
                    }

                    break;
            }

            model.Message = ViewBag.ErrorMessage;

            return true;
        }

        IntegrateResponse IsValidRepID(Registration registration, out Registration reg)
        {
            reg = registration;
            var resultData = new IntegrateResponse() { Result = "ok" };
            string promoCode = registration.Code;
            var promotionCode = new PromotionalCode
            {
                Code = promoCode,
                ApplicationId = registration.ApplicationID,
                CampaignId = registration.ContestID
            };
            if (string.IsNullOrEmpty(registration.RepID))
            {
                resultData.Result = "missingRepID";
                resultData.ErrorType = "RepID";
                resultData.Message = DbLanguage.GetText("Register_MissingRepID");
                return (resultData);
            }
            var repReg = this.registrationRepository.GetRegistrationByRepID(registration.RepID);
            //Valid RepID
            if (repReg != null)
            {
                // //Login
                if (repReg.UserID > 0)
                {
                    var login = new LoginViewModel()
                    {
                        username = registration.Email,
                        password = registration.Password,
                        Code = promoCode
                    };

                    reg = repReg;
                }
            }
            else //Invalid RepID
            {
                resultData.Result = "invalidRepID";
                resultData.Message = DbLanguage.GetText("Register_InvalidRepID");
                return (resultData);
            }

            return resultData;
        }

        IntegrateResponse IsValidPromo(ref PromotionalCode promotionCode)
        {
            promotionCode.LanguageID = SessionUtilities.CurrentLanguageID;
            var resultData = new IntegrateResponse() { Result = "ok" };
            {
                promotionCode = this.promotionRepository.IsValidCode(promotionCode);
                if (promotionCode.IsValidCode == -1)
                {
                    resultData.Result = "expiredcode"; resultData.Message = DbLanguage.GetText("Register_ExpiredCode", pagename: "Customer/Register");
                    resultData.ErrorType = "promocode";
                }
                if (promotionCode.IsValidCode == -2)
                {
                    resultData.Result = "usedcode"; resultData.Message = DbLanguage.GetText("Register_UsedCode", pagename: "Customer/Register");
                    resultData.ErrorType = "promocode";
                }
                if (promotionCode.IsValidCode == -3)
                {
                    resultData.Result = "invalidcode"; resultData.Message = DbLanguage.GetText("Register_InvalidCode", pagename: "Customer/Register");
                    resultData.ErrorType = "promocode";
                }
                return (resultData);
            }
        }


        [AllowAnonymous]
        public JsonResult VerifyUserNameExists(string userName)
        {
            var IsUniqueUserName = this.registrationRepository.IsUniqueUserName(userName, SessionUtilities.CurrentApplicationID);
            return Json(IsUniqueUserName);
        }

        [AllowAnonymous]
        public JsonResult VerifyEmailExists(string email)
        {
            var IsUniqueEmail = this.registrationRepository.IsUniqueEmail(
                email,
                (int)SessionUtilities.CurrentUser.UserID,
                GlobalVariables.CurrentPublishedContestID,
                GlobalVariables.IsInPublishContest(),
                SessionUtilities.CurrentApplicationID);
            return Json(IsUniqueEmail);
        }

        [AllowAnonymous]
        public JsonResult VerifyEmail(string email)
        {
            string result = "Fail";
            var content = EmailUtilities.GetValidateByKickbox(email).Content;
            var response = JsonConvert.DeserializeObject<KickboxResponse>(content);
            if (response.result == "deliverable")
                result = "OK";
            else if (response.result == "undeliverable")
                result = "Fail";
            else //risky, unknown
            {
                if (response.reason == "invalid_email" ||
                    response.reason == "invalid_domain" ||
                    response.reason == "rejected_email" ||
                    response.reason == "invalid_smtp" ||
                    Convert.ToBoolean(response.disposable))
                    result = "Fail";
                else
                    result = "OK";
            }

            return Json(new { result });

        }

        [AllowAnonymous]
        public JsonResult EmailExists(string email, int? userId = null, int? editSiteId = null, int? editCampaignId = null)
        {
            var EmailExists = this.registrationRepository.EmailExists(email.Trim(), userId, editSiteId ?? SiteInformation.Application.ApplicationID, editCampaignId ?? SiteInformation.CampaignID);
            return Json(EmailExists);
        }

        private string SendValidationEmailToSupervisor(Registration reg, string supervisorEmail)
        {
            string message = "";
            //var templateBL = new TemplateBL();

            //string retailerName = new RetailerBL().GetRetailerName(reg.RetailerID.GetValueOrDefault());
            //string activationLink = Url.Action("ActivateBySupervisor", "Registration", new { userName = reg.Email, validationKey = reg.ValidationKey }, Request.Url.Scheme);

            //if (string.IsNullOrEmpty(message))
            //    message = DbLanguage.GetText("Register_ThanksMessage");

            //var template = templateBL.GetTemplateByNameDirect((int)Enums.TemplateKey.SEM_AdminPendingRegistration, reg.ApplicationID.GetValueOrDefault(), reg.ContestID.GetValueOrDefault());
            //if (template != null)
            //{
            //    if (string.IsNullOrEmpty(template.EmailTo))
            //        template.EmailTo = supervisorEmail;

            //    Dictionary<string, string> templateData = new Dictionary<string, string>()
            //    {
            //        {"%TemplateName%", "Register"},
            //        {"%UserID%", reg.UserID.ToString()},
            //        {"%ActivationLink%", activationLink},
            //        {"%RegistrationDate%", reg.RegisteredDate.ToString() + " PST" },
            //        {"%Email%", reg.Email},
            //        {"%FirstName%", (reg.FirstName ?? reg.Username)},
            //        {"%LastName%", reg.LastName},
            //        {"%Username%", reg.Username},
            //        {"%ValidationKey%", reg.ValidationKey},
            //        {"%RetailerName%", retailerName}
            //    };
            //    templateBL.SendEmail(template, Commons.Config["Email_FromAddress"], templateData);
            //}
            return message;
        }

        private string GetRedirectUrl(string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                url = url.ToLower().Trim();
                if (url == "~")
                {
                    url = url.Replace("~", Url.Action("Index", "Home").ToLower());
                }
                else if (url.Contains("~"))
                {
                    string homePage = Url.Action("Index", "Home").ToLower();
                    string rootUrl = homePage;
                    if (rootUrl.Contains(@"/home/"))
                    {
                        rootUrl = rootUrl.Remove(rootUrl.IndexOf(@"/home/"));
                    }

                    url = url.Replace("~", rootUrl);
                }


                url = url.Replace("{_userid_}", SessionUtilities.CurrentUser.UserID.ToString());
                url = url.Replace("{_editsiteid_}", Request.Query["editSiteId"]);
                url = url.Replace("{_editcampaignId_}", Request.Query["editCampaignId"]);
            }

            return url;
        }

    }
}