﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MASNET.DataAccess.Constants.Enums;
using Masnet3.Common;
using MASNET.DataAccess.Model.Settings;
using Masnet3.Business.Repository.Users;
using Masnet3.Business.Repository.Contestants;
using System;
using Masnet3.Business.Repository.Contests;
using System.Linq;
using Masnet3.Model.ViewModel;

namespace Masnet3.Controllers
{
    public class HomeController : BaseController
    {
        private readonly IContestRepository contestRepository;
        public HomeController(
            IContestRepository contestRepository,
            IUserRepository userRepository,
            IContestantRepository contestantRepository,
            IOptions<AppConfiguration> appConfiguration) : base(userRepository, contestantRepository, appConfiguration)
        {
            this.contestRepository = contestRepository;
        }

        public IActionResult Index()
        {
            if (SiteInformation.SiteType == Enums.SiteType.Campaign && Commons.Config["Contest_ShowInFE"] == "1")
            {
                return RedirectToAction("details", "contest", new { id = SiteInformation.CampaignID});
            }
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            int ContestID = 0;
            string DRTV_DefaultContestID =  Commons.GetConfig("DRTV_DefaultContestID");
            if (!string.IsNullOrWhiteSpace(DRTV_DefaultContestID))
            {
                ContestID = Convert.ToInt32(DRTV_DefaultContestID);
            }
            int CntContestFee = 0;
            if (ContestID > 0)
            {
                var lstContestFee = contestRepository.GetListContestFeeByContestId(ContestID).ToList();
                CntContestFee = lstContestFee.Count();
            }
            ViewBag.CntContestFee = CntContestFee;

            var model = new LayoutViewModel();
            ProcessLayout(model);

            string customView = Commons.GetCustomView(SiteInformation.Theme.ID, (int)Enums.PageID.HomePage);
            if (!string.IsNullOrWhiteSpace(customView))
            {
                return View(customView, model);
            }
            return View(model);
        }
    }
}
