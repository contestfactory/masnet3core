﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Masnet3.Common.Helper;
using Masnet3.Business.Repository.Fees;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Masnet3.Business.Repository.Applications;
using Masnet3.Business.Repository.Resources;
using Masnet3.Business.Repository.CustomViews;
using Masnet3.Business.Repository.Layout;
using Masnet3.Business.Repository.Contests;
using Masnet3.Business.Repository;
using Masnet3.Common;
using Masnet3.Common.Utilities;
using Masnet3.Business.Repository.CustomRouteSettings;
using Masnet3.Common.Statups;
using Masnet3.Business.Repository.Users;
using Masnet3.Business.Repository.ExtensionFields;
using Masnet3.Business.Repository.Retailers;
using Masnet3.Business.Repository.Global;
using Masnet3.Business.Repository.Rules;
using MASNET.DataAccess.Model.Settings;
using Masnet3.Business.Repository.Configs;
using Masnet3.Common.Filters;
using Masnet3.Business.Repository.Languages;
using Masnet3.Business.Repository.Periods;
using Masnet3.Business.Repository.Promotions;
using Masnet3.Business.Repository.Contestants;
using Masnet3.Business.Repository.Customer;
using Newtonsoft.Json.Serialization;
using Masnet3.Business.Repository.TblMailQueues;
using Masnet3.Business.Repository.MailArchives;

namespace Masnet3
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // add sesion
            services.AddSession();
            // add memory cache
            services.AddMemoryCache();
            services.AddMvc(options =>
            {
                options.Filters.Add<PermissionFilters>();
            })
            .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
            .AddSessionStateTempDataProvider();

            // add Configuration from AppSetting
            services.AddSingleton<IConfiguration>(Configuration);
            services.Configure<AppConfiguration>(Configuration.GetSection("AppConfiguration"));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            // add Repository
            services.AddSingleton<IRepositoryBasic, RepositoryBasic>();
            services.AddSingleton<IFeeRepository, FeeRepository>();
            services.AddSingleton<IMinifiedApplicationRepository, MinifiedApplicationRepository>();
            services.AddSingleton<IResourceRepository, ResourceRepository>();
            services.AddSingleton<ICustomViewRepository, CustomViewRepository>();
            services.AddSingleton<IThemeRepository, ThemeRepository>();
            services.AddSingleton<IContestRepository, ContestRepository>();
            services.AddSingleton<IApplicationRepository, ApplicationRepository>();
            services.AddSingleton<IApplicationLanguageRepository, ApplicationLanguageRepository>();
            services.AddSingleton<ICustomRouteSettingRepository, CustomRouteSettingRepository>();
            services.AddSingleton<IUserRepository, UserRepository>();
            services.AddSingleton<IExtensionFieldRepository, ExtensionFieldRepository>();
            services.AddSingleton<IRetailerRepository, RetailerRepository>();
            services.AddSingleton<ICountryRepository, CountryRepository>();
            services.AddSingleton<IRuleRepository, RuleRepository>();
            services.AddSingleton<IPageRepository, PageRepository>();
            services.AddSingleton<IConfigRepository, ConfigRepository>();
            services.AddSingleton<ILanguageRepostirory, LanguageRepostirory>();
            services.AddSingleton<IPeriodRepository, PeriodRepository>();
            services.AddSingleton<IPromotionRepository, PromotionRepository>();
            services.AddSingleton<IContestantRepository, ContestantRepository>();
            services.AddSingleton<IRegistrationRepository, RegistrationRepository>();
            services.AddSingleton<ITblMailQueueRepository, TblMailQueueRepository>(); 
            services.AddSingleton<IMailArchiveRepository, MailArchiveRepository>();

            services.AddScoped<PermissionFilters>();

            // Setup Static Class With DI
            var repositoryBasic = services.BuildServiceProvider().GetService<IRepositoryBasic>();
            var applicationLanguageRepository = services.BuildServiceProvider().GetService<IApplicationLanguageRepository>();
            var applicationRepository = services.BuildServiceProvider().GetService<IApplicationRepository>();
            var contestRepository = services.BuildServiceProvider().GetService<IContestRepository>();
            var customRouteSettingRepository = services.BuildServiceProvider().GetService<ICustomRouteSettingRepository>();
            var themeRepository = services.BuildServiceProvider().GetService<IThemeRepository>();
            var resourceRepository = services.BuildServiceProvider().GetService<IResourceRepository>();
            var userRepository = services.BuildServiceProvider().GetService<IUserRepository>();
            var countryRepository = services.BuildServiceProvider().GetService<ICountryRepository>();
            var ruleRepository = services.BuildServiceProvider().GetService<IRuleRepository>();
            var customViewRepository = services.BuildServiceProvider().GetService<ICustomViewRepository>();
            var configRepository = services.BuildServiceProvider().GetService<IConfigRepository>();
            var pageRepository = services.BuildServiceProvider().GetService<IPageRepository>();
            var periodRepository = services.BuildServiceProvider().GetService<IPeriodRepository>();
            var registrationRepository = services.BuildServiceProvider().GetService<IRegistrationRepository>();
            var languageRepostirory = services.BuildServiceProvider().GetService<ILanguageRepostirory>();

            // set up DI
            StartupStaticDI.repositoryBasic = repositoryBasic;
            StartupStaticDI.userRepository = userRepository;
            StartupStaticDI.customRouteSettingRepository = customRouteSettingRepository;
            StartupStaticDI.contestRepository = contestRepository;
            StartupStaticDI.applicationLanguageRepository = applicationLanguageRepository;
            StartupStaticDI.applicationRepository = applicationRepository;
            StartupStaticDI.contestRepository = contestRepository;
            StartupStaticDI.themeRepository = themeRepository;
            StartupStaticDI.resourceRepository = resourceRepository;
            StartupStaticDI.countryRepository = countryRepository;
            StartupStaticDI.ruleRepository = ruleRepository;
            StartupStaticDI.customViewRepository = customViewRepository;
            StartupStaticDI.configRepository = configRepository;
            StartupStaticDI.pageRepository = pageRepository;
            StartupStaticDI.periodRepository = periodRepository;
            StartupStaticDI.registrationRepository = registrationRepository;
            StartupStaticDI.languageRepostirory = languageRepostirory;

            //load default value
            StartupCommon.RebuildAppAndContestDictionary();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseSession();
            //app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                StartupRouteSetting.InitCustomRoute(routes);
                routes.MapRoute(
                    name: "MainSite",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

        }
    }
}
